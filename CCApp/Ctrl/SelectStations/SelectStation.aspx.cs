﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
namespace CCOA.Ctrl.SelectStations
{
    public partial class SelectStation : System.Web.UI.Page
    {
        private DataTable GetAllStations()
        {
            return BP.OA.GPM.GetAllStations();
        }
        private string rIn
        {
            get { return Convert.ToString(this.Request.QueryString["In"]); }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                BP.OA.UI.Dict.BindListCtrl(this.GetAllStations(), this.cblStations, "Name", "No", null, null, null);
                this.LoadSelected();
            }
        }
        private void LoadSelected()
        {
            if (String.IsNullOrEmpty(this.rIn)) return;
            foreach (ListItem li in this.cblStations.Items)
            {
                if (String.Format(",{0},", this.rIn).Contains(String.Format(",{0},", li.Value)))
                    li.Selected = true;
                else
                    li.Selected = false;
            }
        }
        private void SetValueNames()
        {
            string v = String.Empty;
            string t = String.Empty;
            foreach (ListItem li in this.cblStations.Items)
            {
                if (li.Selected)
                {
                    if (!String.IsNullOrEmpty(v)) v += ",";
                    v += String.Format("{0}", li.Value);
                    if (!String.IsNullOrEmpty(t)) t += ",";
                    t += String.Format("{0}", li.Text);
                }
            }
            this.txt_Stations.Value = t;
            this.txt_StationNames.Value = v;
        }

        protected void cblStations_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.SetValueNames();
        }
    }
}