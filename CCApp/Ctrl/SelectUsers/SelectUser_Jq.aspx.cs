﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
//using ULCode.QDA;
using System.Text;
namespace wwwroot.App_Ctrl
{
    public partial class SelectUser_Jq : System.Web.UI.Page
    {
        /// 对外6个接口
        private bool SelOnlyOne
        {
            get { return Request.QueryString["SelOnlyOne"] != null; }
        }
        private string SelUsers
        {
            get
            {
                return Convert.ToString(Request.QueryString["In"]);
            }
        }
        private DataTable GetAllDepts()
        {
            return BP.OA.GPM.GetAllDepts_By_Tree();
        }
        private DataTable GetAllStations()
        {
            return BP.OA.GPM.GetAllStations();
        }
        private DataTable GetAllEmps()
        {
            return BP.OA.GPM.GetAllEmps().Copy();
        }
        private DataTable GetSearchedEmps(string deptId, bool searchChildDept, string stationId, string name)
        {
            return BP.OA.GPM.GetSearchedEmps(deptId, searchChildDept, stationId, name);
        }
        /*初始化装载部分*/
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //BP.OA.UI.Dict.BindListCtrl(this.GetAllDepts(),this.ddlDept,null, "0#--所有部门--", null);
                BP.OA.UI.Dict.BindListCtrl(this.GetAllStations(), this.ddlStation, "Name", "No", null, "0#--所有职位--", null);
                //BP.OA.UI.Dict.BindListCtrl(this.GetAllEmps(), this.lbLeft, null, null, null);
                this.LoadSelectedEmployees();
                //this.RefreshLeftListState();
                //this.RefreshSelCount();
                //this.SetValueNames();
            }
        }
        private void LoadSelectedEmployees()
        {
            string selUsers = this.SelUsers;
            if (!String.IsNullOrEmpty(selUsers))
            {
                DataTable dt = this.GetAllEmps();
                if (dt != null)
                {
                    for (int i = dt.Rows.Count - 1; i >= 0; i--)
                    {
                        DataRow dr = dt.Rows[i];
                        if (!String.Format(",{0},", selUsers).Contains(String.Format(",{0},", dr["No"])))
                        {
                            dt.Rows.RemoveAt(i);
                            dr.Delete();
                        }
                    }
                }
                BP.OA.UI.Dict.BindListCtrl(dt, this.lbRight, "Name", "No", null, null, null);
            }
        }
    }
}