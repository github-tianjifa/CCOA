﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
//using ULCode.QDA;
using System.Text;
namespace wwwroot.App_Ctrl
{
	public partial class SelectPeople : System.Web.UI.Page
	{
        /// 对外6个接口
        private bool SelOnlyOne
        {
            get { return Request.QueryString["SelOnlyOne"] != null; }
        }
        private string SelUsers
        {
            get 
            {
                return Convert.ToString(Request.QueryString["In"]);
            }
        }
        private DataTable GetAllDepts()
        {
            return BP.OA.GPM.GetAllDepts_By_Tree();
        }
        private DataTable GetAllStations()
        {
            return BP.OA.GPM.GetAllStations();
        }
        private DataTable GetAllEmps()
        {
            return BP.OA.GPM.GetAllEmps();
        }
        private DataTable GetSearchedEmps(string deptId, bool searchChildDept, string stationId, string name)
        {
            return BP.OA.GPM.GetSearchedEmps(deptId, searchChildDept, stationId, name);
        }
        /*初始化装载部分*/
		protected void Page_Load(object sender, EventArgs e)
		{
            if (!IsPostBack)
            {
                BP.OA.UI.Dict.BindListCtrl(this.GetAllDepts(), this.ddlDept, "Name", "No", null, "0#--所有部门--", null);
                BP.OA.UI.Dict.BindListCtrl(this.GetAllStations(), this.ddlStation, "Name", "No", null, "0#--所有职位--", null);
                BP.OA.UI.Dict.BindListCtrl(this.GetAllEmps(), this.lbLeft, "Name", "No", null, null, null);
                this.LoadSelectedEmployees();
                this.RefreshLeftListState();
                this.RefreshSelCount();
            }
		}
        private void LoadSelectedEmployees()
        {
            string selUsers = this.SelUsers;
            if (!String.IsNullOrEmpty(selUsers))
            {
                DataTable dt = this.GetAllEmps();
                if (dt != null)
                {
                    for (int i = dt.Rows.Count - 1; i >= 0; i--)
                    {
                        DataRow dr = dt.Rows[i];
                        if (!String.Format(",{0},", selUsers).Contains(String.Format(",{0},", dr["No"])))
                        {
                            dt.Rows.RemoveAt(i);
                            dr.Delete();
                        }
                    }
                }
                BP.OA.UI.Dict.BindListCtrl(dt, this.lbRight, "Name", "No", null, null, null);                
            }
        }
        /// <summary>
        /// 如果已选，则将名前加个*
        /// </summary>
        private void RefreshLeftListState()
        {
            foreach (ListItem li in this.lbLeft.Items)
            {
                if (lbRight.Items.FindByValue(li.Value) != null)
                {
                    if (!li.Text.StartsWith("*"))
                        li.Text = String.Format("*{0}", li.Text);
                }
                else
                {
                    if (li.Text.StartsWith("*"))
                        li.Text = li.Text.Substring(1);
                }
            }
        }
        private void RefreshSelCount()
        {
            this.liSelCount.Text = this.lbRight.Items.Count.ToString();
        }
        /*事件部分*/
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            string dept = this.ddlDept.SelectedValue;
            string station = this.ddlStation.SelectedValue;
            string name = this.txtKeyword.Text.Trim();
            bool searchChildDept = this.cbContainChild.Checked;
            DataTable dtEmps = this.GetSearchedEmps(dept, searchChildDept, station, name);
            BP.OA.UI.Dict.BindListCtrl(dtEmps, this.lbLeft, "Name", "No", null, null, null);
            this.RefreshLeftListState();
        }
        protected void btnRight_Click(object sender, EventArgs e)
        {
            //单选限制
            if (this.SelOnlyOne)
            {
                if (lbRight.Items.Count == 1)
                {
                    BP.OA.Debug.Alert(this, "只允许添加一个人！");
                    return;
                }
            }
            //移动，可以多选所以遍历
            List<ListItem> li_Array = new List<ListItem>();
            foreach (ListItem li in this.lbLeft.Items)
            {
                if (li.Selected)
                {
                    if (lbRight.Items.FindByValue(li.Value) != null)
                    {
                        string text = li.Text;
                        if (text.StartsWith("*")) text = text.Substring(1);
                        BP.OA.Debug.Alert(this, String.Format("人员({0}) 已经存在，不能重复添加！", li.Text));
                    }
                    else
                    {
                        this.lbRight.Items.Add(li);
                        li_Array.Add(li);
                    }
                }
            }
           foreach (ListItem li in li_Array)
            {
                this.lbLeft.Items.Remove(li);
            }
           this.RefreshLeftListState();
           this.RefreshSelCount();
        }
        protected void btnLeft_Click(object sender, EventArgs e)
        {
            List<ListItem> li_Array = new List<ListItem>();
            foreach (ListItem li in this.lbRight.Items)
            {
                if (li.Selected)
                {
                    if (lbLeft.Items.FindByValue(li.Value) != null)
                    {
                        ;
                    }
                    else
                    {
                        this.lbLeft.Items.Add(li);
                    }
                    li_Array.Add(li);
                }
            }
            foreach (ListItem li in li_Array)
            {
                this.lbRight.Items.Remove(li);
            }
            this.RefreshLeftListState();
            this.RefreshSelCount();
        }
        protected void ClearSelAll(object sender, EventArgs e)
        {
            this.lbRight.Items.Clear();
        }        
    }
}