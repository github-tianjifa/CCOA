﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
namespace CCOA.Ctrl.SelectDepts
{
    public partial class SelectDept_zTree : System.Web.UI.Page
    {
        public string JsonTree
        {
            get
            {
                string sSql = "select [No],[Name],[ParentNo] from Port_Dept order by Idx";
                DataTable dt = CCPortal.DA.DBAccess.RunSQLReturnTable(sSql);
                if (dt == null) return String.Empty;
                StringBuilder sb = new StringBuilder();
                foreach (DataRow dr in dt.Rows)
                {
                    if (sb.Length > 0) sb.Append(",");
                    string f = String.Format("id: {0}, pId: {2}, name: \"{1}\", open: true {3} ", dr["No"], dr["Name"], dr["ParentNo"], this.rIn == Convert.ToInt32(dr["No"]) ? ",checked:true" : "");
                    sb.Append("{" + f + "}\r\n");
                }
                return sb.ToString();
            }
        }
        public int rIn
        {
            get
            {
                if (this.Request.QueryString["In"] == null || String.IsNullOrEmpty(this.Request.QueryString["In"].ToString()))
                    return 0;
                else
                    return Convert.ToInt32(this.Request.QueryString["In"]);
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {

            }
        }
    }
}