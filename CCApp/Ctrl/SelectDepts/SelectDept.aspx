﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SelectDept.aspx.cs" Inherits="CCOA.Ctrl.SelectDepts.SelectDept" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="/Js/jquery-1.7.2.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        function getReturnText() {
            return $("#txt_Stations").val();
        }
        function getReturnValue() {
            return $("#txt_StationNames").val();
        }
        function addToList(v, list) {
            if (list == null || list == '') {
                return v;
            }
            var s = new String();
            var g_v=","+v+",";
            var g_list=","+list+",";
            if (g_list.indexOf(g_v) == -1) {
                list = list + "," + v;
            }
            return list;
        }
        function removeFromList(v, list) {
            if (list == null || list == '') {
                return null;
            }
            var s = new String();
            var g_v = "," + v + ",";
            var g_list = "," + list + ",";
            if (g_list.indexOf(g_v) >= 0) {
                list = (',' + list).replace(',' + v, '');
                list = list.substr(1);
            }
            return list;
        }
        $(function () {
            $(":checkbox").click(function () {
                var checked = $(this).prop("checked");
                var t = $(this).prop("title");
                var v = $(this).val();
                var arr_v = v.split('├');
                v = arr_v[arr_v.length - 1];
                if (checked) {
                    $("#txt_StationNames").val(addToList(v, $("#txt_StationNames").val()));
                    $("#txt_Stations").val(addToList(t, $("#txt_Stations").val()));
                }
                else {
                    $("#txt_StationNames").val(removeFromList(v, $("#txt_StationNames").val()));
                    $("#txt_Stations").val(removeFromList(t, $("#txt_Stations").val()));
                }
            });
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:Repeater ID="rptDepts" runat="server">
            <ItemTemplate>
                <div>
                    <input type="checkbox" <%# GetSelected(Eval("ID")) %> value='<%# Eval("ID") %>' title='<%# Eval("Text") %>' /><%# Eval("Name") %>
                </div>
            </ItemTemplate>
        </asp:Repeater>
        <asp:HiddenField ID="txt_StationNames" runat="server" />
        <asp:HiddenField ID="txt_Stations" runat="server" />
    </div>
    </form>
</body>
</html>
