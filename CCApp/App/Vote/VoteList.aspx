﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="VoteList.aspx.cs" Inherits="CCOA.App.Vote.VoteList" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../../CSS/table.css" rel="stylesheet" type="text/css" />
    <script src="../../Js/jquery-1.7.2.min.js" type="text/javascript"></script>
    <script src="../../Js/zDialog/zDialog.js" type="text/javascript"></script>
    <script src="../../Js/zDialog/zDrag.js" type="text/javascript"></script>
    <script src="../../Js/Js_EasyUI/jquery.easyui.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        function Sure() {
            return confirm("确定要删除吗？");
        }
    </script>
</head>
<body>
    <form id="form1" runat="server" method="post">
    <table class="table">
        <tr>
            <th>
                编号
            </th>
            <th>
                投票主题
            </th>
            <th>
                是否多选
            </th>
            <th>
                是否匿名
            </th>
            <th>
                是否启用
            </th>
            <th>
                创建人
            </th>
            <th>
                创建时间
            </th>
            <th>
                开始时间
            </th>
            <th>
                结束时间
            </th>
            <th style="width: 180px">
                操作
            </th>
        </tr>
        <asp:Repeater ID="Repeater1" runat="server">
            <ItemTemplate>
                <tr>
                    <td>
                        <%#DataBinder.Eval(Container.DataItem,"OID" )%>
                    </td>
                    <td>
                        <%#DataBinder.Eval(Container.DataItem,"Title" )%>
                    </td>
                    <td>
                        <%#DataBinder.Eval(Container.DataItem,"VoteType" )%>
                    </td>
                    <td>
                        <%#Check(DataBinder.Eval(Container.DataItem, "IsAnonymous"))%>
                    </td>
                    <td>
                        <%#Check(DataBinder.Eval(Container.DataItem, "IsEnable"))%>
                    </td>
                    <td>
                        <%#DataBinder.Eval(Container.DataItem,"Name" )%>
                    </td>
                    <td>
                        <%#DateFormat( DataBinder.Eval(Container.DataItem,"CreateDate"))%>
                    </td>
                    <td>
                        <%#DataBinder.Eval(Container.DataItem,"DateFrom")%>
                    </td>
                    <td>
                        <%#DataBinder.Eval(Container.DataItem,"DateTo")%>
                    </td>
                    <td>
                        <div style="float: left">
                            <a href='VoteResult.aspx?id=<%#Eval("OID")%>'>查看</a>&nbsp;&nbsp; 
                        </div>
                        <div style='float: left; display: <%#CheckPermission(Eval("FK_Emp"))%>'>
                            <a href='CreateVote.aspx?m=edit&&id=<%#Eval("OID")%>'>编辑</a> &nbsp;&nbsp; <a href='?m=change&&id=<%#Eval("OID")%>&&state=<%#State(Eval("IsEnable"))%>'>
                                <%# CheckText(Eval("IsEnable"))%></a> &nbsp;&nbsp; <a href='?m=delete&&id=<%#Eval("OID")%>'
                                    onclick="return Sure()">删除</a>
                        </div>
                    </span> </th>
                </tr>
            </ItemTemplate>
        </asp:Repeater>
    </table>
    <input type="hidden" name="hidden1" value="" />
    </form>
</body>
</html>
