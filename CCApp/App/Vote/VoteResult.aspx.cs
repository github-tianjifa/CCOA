﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using BP.OA.Vote;
using System.Text;
using BP.DA;
using System.Web.UI.HtmlControls;
namespace CCOA.App.Vote
{
    public partial class VoteResult : System.Web.UI.Page
    {
        public BP.OA.Vote.OA_Vote en = new OA_Vote();
        /// <summary>
        /// 投票编号和投票数量
        /// </summary>
        public Dictionary<string, int> dics = new Dictionary<string, int>();
        //投票编号和投票人
        public Dictionary<string, string> dicEmp = new Dictionary<string, string>();
        /// <summary>
        /// 总票数
        /// </summary>
        public int Total = 0;
        public string data;
        public string title;
        public bool showPoll = true;
        public bool multiple = false;
        public string Uid = BP.Web.WebUser.No;
        public OA_VoteDetail vd;
        public string VoteId;
        public bool boolIsDoIt = true;
        protected void Page_Load(object sender, EventArgs e)
        {
            VoteId = Request.QueryString["id"];//投票编号
            if (!string.IsNullOrEmpty(VoteId))
            {
                DataTable dt = BP.OA.Vote.OA_Vote.GetVoteByOID(VoteId);
                if (dt != null && dt.Rows.Count == 1)
                {
                    en = Common.GetValue<BP.OA.Vote.OA_Vote>(dt.Rows[0]);
                    data = getVoteItemjson();
                    if (en.VoteType == "多投")
                    {
                        multiple = true;
                    }
                    if (en.IsEnable == "0")
                    {
                        showPoll = false;
                    }
                    Creator.InnerText = en.FK_Emp;
                    AddTime.InnerText = BP.OA.Main.TransDateTimeShort(en.CreateDate);
                    if (DateTime.Parse(en.DateFrom) > DateTime.Now)
                    {
                        IsValide.InnerText = "投票尚未开始";
                    }
                    else if (DateTime.Parse(en.DateTo) < DateTime.Now)
                    {
                        IsValide.InnerText = "投票已经结束";
                    }
                    else
                    {
                        IsValide.InnerText = "投票进行中....";
                    }
                    string shuxing = null;
                    if (en.IsEnable == "1")
                    {
                        shuxing += "已启用投票";
                    }
                    else
                    {
                        shuxing += "未启用投票";
                    }
                    if (en.IsAnonymous == "0")
                    {
                        shuxing += "  实名投票";
                    }
                    else
                    {
                        shuxing += "  匿名投票";
                    }
                    if (en.VoteType == "单投")
                    {
                        shuxing += "  单选投票";
                    }
                    else
                    {
                        shuxing += "  多选投票";
                    }
                    this.ShowUser.InnerText = shuxing;
                    //为空的判断
                    if (en.VoteEmpName.Length == 0)
                    {
                        this.namelist.InnerText = "全部人员";
                    }
                    else
                    {
                        this.namelist.InnerText = en.VoteEmpName.Substring(0, en.VoteEmpName.Length - 1);
                    }
                }
            }
            OA_VoteDetails vs = new OA_VoteDetails();
            vs.RetrieveAll();

            //改  当前登录人可以查看所有人的投票信息 Comment为空的例外
            string getAllEmps = string.Format("SELECT VComment,IsAnony,FK_Emp,FK_VoteOID,VoteDate FROM oa_votedetail WHERE FK_VoteOID ={0} AND VComment !=''", int.Parse(VoteId));

            DataTable AllEmpsArrayDt = DBAccess.RunSQLReturnTable(getAllEmps);

            string fkEmp = null;
            string isAnony = null;
            string getComment = null;
            string dTime = null;
            string notes = null;


            HtmlGenericControl span = new HtmlGenericControl();
            vd = (OA_VoteDetail)vs.Filter("FK_VoteOID", VoteId, "FK_Emp", BP.Web.WebUser.No);
            for (int i = 0; i < AllEmpsArrayDt.Rows.Count; i++)
            {
                //vd = (OA_VoteDetail)vs.Filter("FK_VoteOID", AllEmpsArrayDt.Rows[i]["FK_VoteOID"].ToString(), "FK_Emp", AllEmpsArrayDt.Rows[i]["FK_Emp"].ToString());
                if (vd != null)
                {
                    fkEmp = AllEmpsArrayDt.Rows[i]["FK_Emp"].ToString();
                    isAnony = AllEmpsArrayDt.Rows[i]["IsAnony"].ToString();
                    dTime = AllEmpsArrayDt.Rows[i]["VoteDate"].ToString();
                    if (isAnony == "1")
                    {
                        isAnony = "匿名用户";
                    }
                    else
                    {
                        string getTrueName = string.Format("SELECT NAME from port_emp  WHERE No='{0}'", fkEmp);
                        DataTable getTrueNameDt = DBAccess.RunSQLReturnTable(getTrueName);
                        if (getTrueNameDt.Rows.Count == 1)
                        {
                            isAnony = getTrueNameDt.Rows[0]["Name"].ToString();
                        }
                        else
                        {
                            isAnony = "非法用户";
                        }
                    }
                    getComment = AllEmpsArrayDt.Rows[i]["VComment"].ToString();
                    notes = "<span>" + getComment + " " + "&nbsp; &nbsp;" + "用户信息：" + isAnony + "&nbsp;&nbsp;时间：" + dTime + "</span><br>";

                    span.InnerHtml += notes;

                }
            }
            string isNullOfEmps = string.Format("SELECT VoteEmpNo FROM oa_vote WHERE OID={0}", int.Parse(VoteId));
            string isDoIt = null;
            if (string.IsNullOrEmpty(DBAccess.RunSQLReturnTable(isNullOfEmps).Rows[0]["VoteEmpNo"].ToString()))
            {
                isDoIt = string.Format("SELECT OID FROM oa_votedetail WHERE FK_Emp='{0}' and FK_VoteOID={1}", BP.Web.WebUser.No, int.Parse(VoteId));
                if (DBAccess.RunSQLReturnTable(isDoIt).Rows.Count == 0)
                {
                    boolIsDoIt = true;
                }
                else
                {
                    boolIsDoIt = false;
                }
            }
            else
            {
                isDoIt = string.Format("SELECT OID FROM oa_votedetail WHERE FK_Emp='{0}' and FK_VoteOID={1}", BP.Web.WebUser.No, int.Parse(VoteId));
                if (DBAccess.RunSQLReturnTable(isDoIt).Rows.Count >= 1)
                {
                    boolIsDoIt = false;
                }
            }
            //判断当前登录人是否已经投过票
            //string isDoIt = string.Format("SELECT OID FROM oa_votedetail WHERE FK_Emp='{0}' and FK_VoteOID={1}", BP.Web.WebUser.No, int.Parse(VoteId));

            //if (DBAccess.RunSQLReturnTable(isDoIt).Rows.Count >= 1)
            //{
            //    boolIsDoIt = false;
            //}
            this.notes.Controls.Add(span);

            //this.Response.Write(notes);
            //vd = (OA_VoteDetail)vs.Filter("FK_VoteOID", VoteId, "FK_Emp", BP.Web.WebUser.No);
            //if (vd != null)
            //{
            //    notes.InnerText += vd.Comment;

            //}
            //<span>管理员 &nbsp; &nbsp; --- 匿名用户 14-8-11 08:54 </span><br>
            //vd = (OA_VoteDetail)vs.Filter("FK_VoteOID", VoteId, "FK_Emp", BP.Web.WebUser.No);
            //if (vd != null)
            //{
            //    notes.InnerText += vd.Comment;

            //}
            //DataTable dt2 = BP.OA.Vote.OA_VoteDetail.GetVoteDetailByOID(id);//获得所有投票情况
            //if (dt2 != null)
            //{
            //    foreach (DataRow dr in dt2.Rows)
            //    {
            //        string[] strItems = Convert.ToString(dr["VoteItem"]).Split(',');
            //        AddToDics(strItems);
            //        AddToEmps(strItems, Convert.ToString(dr["Name"]));
            //        Total += strItems.Length;
            //    }
            //}
        }
        public string getVoteItemjson()
        {
            StringBuilder append = new StringBuilder();
            if (en != null)
            {
                int i = 0;
                string[] strs = en.Item.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
                append.Append("{root:[");
                foreach (string str in strs)
                {
                    if (string.IsNullOrEmpty(str))
                        continue;
                    append.Append("{");
                    i++;
                    string strValue = i.ToString();
                    append.Append("id" + ":'" + i + "',");
                    append.Append("name" + ":'" + str + "',");
                    string sql = String.Format("select * from OA_VoteDetail"
                        + " where (VoteItem like '%{0}%'  and FK_VoteOID={1}) ", i, VoteId);
                    int count = CCPortal.DA.DBAccess.RunSQLReturnCOUNT(sql);
                    append.Append("value" + ":'" + count + "'");
                    append.Append("},");
                }
                append = append.Remove(append.Length - 1, 1);
            }
            append.Append("]}");
            return append.ToString();
        }
        void AddToDics(string[] strItems)
        {
            foreach (string str in strItems)
            {
                if (dics.Keys.Contains(str))
                {
                    dics[str] += 1;
                }
                else
                {
                    dics.Add(str, 1);
                }
            }
        }
        void AddToEmps(string[] strItems, string Name)
        {
            foreach (string str in strItems)
            {
                if (dicEmp.Keys.Contains(str))
                {
                    dicEmp[str] += Name + "，";
                }
                else
                {
                    dicEmp.Add(str, Name + ",");
                }
            }
        }
        public string GetDetails(string key)
        {
            string strDetails = string.Empty;
            if (dicEmp.ContainsKey(key))
                strDetails = dicEmp[key];

            if (!string.IsNullOrEmpty(strDetails))
            {
                strDetails = strDetails.Substring(0, strDetails.Length - 1);
            }
            return strDetails;
        }
        public int GetVoteNum(string id)
        {
            if (dics.Keys.Contains(id))
            {
                return dics[id];
            }
            else
            {
                return 0;
            }
        }
    }
}