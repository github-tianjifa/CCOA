﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="VoteResult.aspx.cs" Inherits="CCOA.App.Vote.VoteResult" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <script src="js/vailidator.js" type="text/javascript"></script>
    <link href="js/InterFace.css" rel="stylesheet" />
    <link href="js/style.css" rel="stylesheet" />
    <script src="../../Js/jquery-1.7.2.min.js" type="text/javascript"></script>
    <script src="../../Js/Js_EasyUI/jquery.easyui.min.js" type="text/javascript"></script>
    <link href="js/jQuery.spider.poll.css" rel="stylesheet" />
    <script src="js/jQuery.spider.poll.js" type="text/javascript"></script>
    <title></title>
    <script type="text/javascript">
        var data="<%=data %>";
        var boolIsDoIt = "<%=boolIsDoIt%>";
        var sp
        if ("<%=showPoll%>"=="True") {
            sp=true
        }
        else {
            sp=false
        }
        if ("<%=multiple%>"=="True") {
            mtp=true;
        }
        else {
            mtp=false;
        }
        $(document).ready(function (){
            $("#poll_a").poll("poll1",{
                title: '<%=en==null?string.Empty:en.Title%>',
                titleColor:'red',
                width:'535px',
                data:data,
                showPoll:sp,
                multiple:mtp
            });
            if (boolIsDoIt=="False") {  //修改js
                $("#IsVote1").hide();
                if (mtp==true) {
                    $("#poll_a input[type='checkbox']").hide();
                }
                else {
                    $("#poll_a input[type='radio']").hide();
                }

                  }
              });
        function repayClick()
        {
            if(document.getElementById("replay").value=='在这里输入投票评论')
            {document.getElementById("replay").value='';}
        }
        function postVote(){
            var p = '';
            var u = '<%=Uid %>';
            var v = '<%=Request.QueryString["id"] %>';
            var n = $("#replay").val();
            $("#poll_a").getChecked().each(function (i,n){
                p += $(n).val() +',';
            });
            if(p != ''){
                if(confirm('确实要投票吗？')){
                    $.ajax({
                        type: "GET",
                        url: "vote.ashx",
                        dataType: 'text',
                        contentType: "application/x-www-form-urlencoded; charset=utf-8",
                        data: 'u=' + u + '&p=' + p + '&v=' + v + '&n=' + escape(n),
                        success: function (data) {
                            //alert(data);
                            if (data == "1") {
                                alert("投票成功，谢谢参与!");
                                window.location = 'VoteResult.aspx?id=' + v;
                            } else {
                                alert("网络错误，投票失败!");
                                window.location = 'VoteList.aspx';
                            }
                        } 
                    });
                }  
            }
            else {
               alert("您没选选中投票项!");
            }    
        }
    </script>
</head>
<body>
    <form id="form2" runat="server" onsubmit="return Validator.Validate(this.form,1);">
        <div id="interface_inside">
            <div id="interface_main">
                <div id="tabs_config" class="tabsbox">

                    <div class="clearboth"></div>
                    <table class="table subsubmenu">
                        <thead>
                            <tr>
                                <td>
                                    <a href="VoteList.aspx">投票列表</a>
                                </td>
                                <td style="text-align: right"></td>
                            </tr>
                        </thead>
                    </table>
                    <br />
                    <!-- 模块 -->
                    <div style="background: #fff; border: 1px solid #79B7E7; margin-left: 10px; margin-right: 10px;">
                        <div style="margin: 10px;">
                            <span style="font-weight: bold; color: #006600;">创建者</span>：<span runat="server" id="Creator" style="font-weight: bold;"></span> &nbsp;&nbsp;&nbsp;
                                <span style="font-weight: bold; color: #006600;">时 &nbsp;&nbsp;&nbsp;间</span>：<span runat="server" id="AddTime"></span> &nbsp;&nbsp;&nbsp;<br>
                            <span style="font-weight: bold; color: #006600;">状 &nbsp;&nbsp;&nbsp;态</span>：<span runat="server" id="IsValide"></span> &nbsp;&nbsp;&nbsp;
                                <span style="font-weight: bold; color: #006600;">属 &nbsp;&nbsp;&nbsp;性</span>：<span runat="server" id="ShowUser"></span> &nbsp;&nbsp;&nbsp;<br>
                            <span style="font-weight: bold; color: #006600;">参与人</span>：<span runat="server" id="namelist"></span> &nbsp;&nbsp;&nbsp;<br>
                            <hr />
                        </div>
                        <div id="poll_a"></div>
                        <br>
                        <div runat="server"  id="IsVote1" style="margin: 10px;">
                            <textarea id="replay" name="replay" rows="2" onclick="repayClick()" style="overflow: hidden; float: left; padding-left: 5px; width: 60%;">在这里输入投票评论</textarea>
                            <input type="button" value=' 我要投票 ' id="bt1" name="bt1" style="cursor: pointer; margin-left: 3px; height: 38px; border: 1px #999999 solid; background: #ebf0f4;" onclick='postVote()' />
                        </div>
                        <br>
                        <div style="margin: 10px;">
                            <span style="font-weight: bold; color: #006600;">评 &nbsp;&nbsp;&nbsp;论</span>：
                                <div runat="server" enableviewstate="false" id="notes" style="min-height: 70px; _height: 70px; margin: 10px 10px 1px 1px; border: 1px solid #E3E197; padding: 10px 10px 10px 10px; background-color: #FFFFFF;"></div>
                        </div>
                    </div>
                    <!-- 模块结束 -->
                </div>
            </div>
        </div>
    </form>
</body>
</html>
