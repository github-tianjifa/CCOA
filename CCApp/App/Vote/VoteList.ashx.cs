﻿using BP.En;
using BP.OA.Vote;
using BP.Sys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BP.Tools;

namespace CCOA.App.Vote
{
    /// <summary>
    /// VoteList1 的摘要说明
    /// </summary>
    public class VoteList1 : IHttpHandler
    {

      /// <summary>
        /// 封装有关个别 HTTP 请求的所有 HTTP 特定的信息
        /// </summary>
        HttpContext _Context = null;

        public string getUTF8ToString(string param)
        {
            return HttpUtility.UrlDecode(_Context.Request[param], System.Text.Encoding.UTF8);
        }

        public void ProcessRequest(HttpContext context)
        {
            _Context = context;

            if (BP.Web.WebUser.No == null)
                return;

            string method = string.Empty;
            //返回值
            string s_responsetext = string.Empty;
            if (!string.IsNullOrEmpty(_Context.Request["method"]))
                method = _Context.Request["method"].ToString();

            switch (method)
            {
                case "getnewslist"://获取投票列表
                    s_responsetext = GetNewsList();
                    break;
                case "newsdelete"://删除投票
                    s_responsetext = DelNews();
                    break;
            }
            if (string.IsNullOrEmpty(s_responsetext))
                s_responsetext = "";
            //组装ajax字符串格式,返回调用客户端
            _Context.Response.Charset = "UTF-8";
            _Context.Response.ContentEncoding = System.Text.Encoding.UTF8;
            _Context.Response.ContentType = "text/html";
            _Context.Response.Expires = 0;
            _Context.Response.Write(s_responsetext);
            _Context.Response.End();
        }
        ///// <summary>
        ///// 获取投票类别
        ///// </summary>
        ///// <returns></returns>
        //private string GetNewCatagory()
        //{

        //    ArticleCatagorys catagorys = new ArticleCatagorys();
        //    catagorys.RetrieveAll();
        //    return Entitis2Json.ConvertEntities2ListJson(catagorys);
        //}
        /// <summary>
        /// 获取投票列表
        /// </summary>
        /// <returns></returns>
        public string GetNewsList()
        {
            int totalCount = 0;
            string keyWords = getUTF8ToString("keyWords");
            //string cataValue = getUTF8ToString("newsCatagory");
            //当前页
            string pageNumber = getUTF8ToString("pageNumber");
            int iPageNumber = string.IsNullOrEmpty(pageNumber) ? 1 : Convert.ToInt32(pageNumber);
            //每页多少行
            string pageSize = getUTF8ToString("pageSize");
            int iPageSize = string.IsNullOrEmpty(pageSize) ? 9999 : Convert.ToInt32(pageSize);
            OA_Votes vts = new OA_Votes();

            QueryObject obj = new QueryObject(vts);
            obj.AddWhere("1=1");
            //if (cataValue != "0")
            //{
            //    obj.AddWhere(String.Format(" and FK_ArticleCatagory='{0}'", cataValue));
            //}
            if (!String.IsNullOrEmpty(keyWords))
            {
                obj.AddWhere(String.Format(" and Title LIKE '%{0}%' OR Item like '%{0}%' OR DateFrom like '%{0}%' "
                + " OR DateTo like '%{0}%' OR CreateDate like '%{0}%'", keyWords));
            }
            totalCount = obj.GetCount();
            obj.addOrderBy(OA_VoteAttr.CreateDate);
            obj.DoQuery(OA_VoteAttr.OID, iPageSize, iPageNumber);
            return Entitis2Json.ConvertEntitis2GridJsonOnlyData(vts, totalCount);
        }
        /// <summary>
        /// 删除投票
        /// </summary>
        /// <returns></returns>
        private string DelNews()
        {
            try
            {
                string newIds = getUTF8ToString("newIds");
                string[] idsArrary = newIds.Split(',');
                foreach (string str in idsArrary)
                {
                    OA_Vote article = new OA_Vote();
                    article.Delete(OA_VoteAttr.OID,str);
                }
                return "true";
            }
            catch (Exception ex)
            {
 
            }
            return "false";
        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}