﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BP.OA;
using BP.En;
using System.Data;

namespace CCOA.App.AddressList
{
    public partial class DeptList : System.Web.UI.Page
    {
        #region 属性.
        public int PageIdx
        {
            get
            {
                try
                {
                    return int.Parse(this.Request.QueryString["PageIdx"]);
                }
                catch
                {
                    return 1;
                }
            }
        }
        #endregion 属性.
        protected void Page_Load(object sender, EventArgs e)
        {

            int currPageIdx = this.PageIdx;
            int pageSize = 12; //页面记录条数.

            BP.OA.ALDepts ens = new ALDepts();
            BP.En.QueryObject qo = new QueryObject(ens);
            //qo.AddWhere(ALDeptAttr.Name,true);//查询条件
            qo.addOrderBy(ALDeptAttr.No);

            this.Pub1.Clear();
            this.Pub1.BindPageIdx(qo.GetCount(),
                pageSize, currPageIdx, "DeptList.aspx?1 =2&3=xx");
            qo.DoQuery("No", pageSize, currPageIdx);
            //qo.DoQuery();
            DataTable dt = ens.ToDataTableField();
            this.rep_List.DataSource = dt;

            this.rep_List.DataBind();




        }
    }
}