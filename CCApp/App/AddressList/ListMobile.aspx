﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App/Mobile.Master" AutoEventWireup="true" CodeBehind="ListMobile.aspx.cs" Inherits="CCOA.App.AddressList.ListMobile" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<script type="text/javascript">
    //公共方法
    function queryData(param, callback, scope, method, showErrMsg) {
        if (!method) method = 'GET';
        $.ajax({
            type: method, //使用GET或POST方法访问后台
            dataType: "text", //返回json格式的数据
            contentType: "application/json; charset=utf-8",
            url: "ListMobile.aspx", //要访问的后台地址
            data: param, //要发送的数据
            async: false,
            cache: false,
            complete: function () { }, //AJAX请求完成时隐藏loading提示
            error: function (XMLHttpRequest, errorThrown) {
                callback(XMLHttpRequest);
            },
            success: function (msg) {//msg为返回的数据，在这里做数据绑定
                var data = msg;
                callback(data, scope);
            }
        });
    }

    $(function () {
        LoadGridData(1, 20);
    });
    function LoadDataGridCallBack(js, scorp) {
        $("#pageloading").hide();
        if (js == "") js = "[]";

        //系统错误
        if (js.status && js.status == 500) {
            $("body").html("<b>访问页面出错，请联系管理员。<b>");
            return;
        }

        var pushData = eval('(' + js + ')');
        $('#newsGrid').datagrid({
            columns: [[
                    { field: 'NAME', title: '姓名', width: 100, align: 'center' },
                      { field: 'TEL', title: '手机号', width: 100, align: 'center' }

                ]],
            idField: 'NO',
            singleSelect: true,
            data: pushData,
            width: 'auto',
            height: 'auto',
            rownumbers: true,
            pagination: true,
            fitColumns: true,
            pageNumber: scorp.pageNumber,
            pageSize: scorp.pageSize,
            pageList: [20, 30, 40, 50],
            loadMsg: '数据加载中......'

        });
        //分页
        var pg = $("#newsGrid").datagrid("getPager");
        if (pg) {
            $(pg).pagination({
                onRefresh: function (pageNumber, pageSize) {
                    LoadGridData(pageNumber, pageSize);
                },
                onSelectPage: function (pageNumber, pageSize) {
                    LoadGridData(pageNumber, pageSize);
                }
            });
        }
    }
    //加载grid
    function LoadGridData(pageNumber, pageSize) {
        this.pageNumber = pageNumber;
        this.pageSize = pageSize;
        var IT_TXL = document.getElementById("IT_TXL").value;
        var params = {
            method: "getList",
            pageNumber: pageNumber,
            pageSize: pageSize,
            IT_TXL: IT_TXL
        };
        queryData(params, LoadDataGridCallBack, this);
    }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div data-role="page" data-theme="c">
        <div data-role="header" data-theme="b">
            <a href="../../CCMobile/Home.aspx" data-icon="black" data-role="button ">返回</a>
            <h2>
                我的通讯录</h2>
        </div>
        <div data-role="content">
            关键字查询：
            <input id="IT_TXL" type="text" data-role="none" style="border-style: solid; border-color: #aaaaaa;" />
            <a id="DoQueryByKey" href="#" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-search'"
                onclick="LoadGridData(1, 20)">查询</a>
        </div data-role="listview">
            <table id="newsGrid" class="easyui-datagrid">
            </table>
    </div>
</asp:Content>
