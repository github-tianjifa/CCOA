﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using BP.En;
namespace CCOA.App.WebLogs
{
    public partial class WebLogs1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                //清除即时消息
                BP.OA.ShortMsg.ReceivedInfo(null, "网络日志");
                #region 初始化页面
                BP.WebLog.LogTypes tps = new BP.WebLog.LogTypes();
                tps.RetrieveAll();
                //ListItem li = new ListItem("--请选择--", "");
                //selecttype.Items.Insert(0, li);
                selecttype.DataSource = tps;
                selecttype.DataTextField = "Name";
                selecttype.DataValueField = "No";
                selecttype.DataBind();
                #endregion
            }

        }
    }
}