﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App/AppMaster/AppSite.Master" AutoEventWireup="true" CodeBehind="WebLogs.aspx.cs" Inherits="CCOA.App.WebLogs.WebLogs1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
   
    <script language="javascript" type="text/javascript">
        //加载grid后回调函数
        function LoadDataGridCallBack(js, scorp) {
            $("#pageloading").hide();
            if (js == "") js = "[]";
            var pushData = eval('(' + js + ')');
            $('#newsGrid').datagrid({
                columns: [[
                    { field: 'OID', title: '编号', width: 60 },
                    { field: 'Doc', title: '内容', width: 100 },
                    { field: 'RecText', title: '作者', width: 120 },
                    { field: 'DRTypeText', title: '日志类型', width: 100 },
                    {
                        field: 'DRStaText', title: '日志状态', width: 120,
                        styler: function (value, row, index) {
                            if (value == "补写") {
                                return 'color:red;';
                            }
                            if (value == "正常") {
                                return 'color:green;';
                            }
                        }
                    },
                    { field: 'date', title: '日志时间', width: 120 },
                     { field: 'RDT', title: '提交时间', width: 120 },
                ]],
                frozenColumns: [[{ field: "chk", "checkbox": true }]],
                data: pushData,
                width: 'auto',
                height: 'auto',
                striped: true,
                rownumbers: true,
                singleSelect: true,
                pagination: true,
                remoteSort: false,
                fitColumns: true,
                pageNumber: scorp.pageNumber,
                pageSize: scorp.pageSize,
                pageList: [20, 30, 40, 50],
                onDblClickCell: function (index, field, value) {
                },
                loadMsg: '数据加载中......'
            });
            //分页
            var pg = $("#newsGrid").datagrid("getPager");
            if (pg) {
                $(pg).pagination({
                    onRefresh: function (pageNumber, pageSize) {
                        LoadGridData(pageNumber, pageSize);
                    },
                    onSelectPage: function (pageNumber, pageSize) {
                        LoadGridData(pageNumber, pageSize);
                    }
                });
            }
        }
        //加载grid
        function LoadGridData(pageNumber, pageSize) {
            var pageNumber = pageNumber;
            var pageSize = pageSize;
            var catagory = $("#NewCatagory").val();
            var keyWords = $("#TB_KeyWords").val();
            var fromdate = $('#start').datebox('getValue');
            var todate = $('#end').datebox('getValue');
            var params = {
                mtd: "getnewslist",
                keyWords: encodeURI(keyWords),
                newsCatagory: catagory,
                fromdate: fromdate,
                todate: todate
            };
            queryData(params, LoadDataGridCallBack, this);
        }
        //绑定下拉框
        function InitNewsCatagory() {
            var params = {
                mtd: "getnewcatagory"
            };
            queryData(params, function (js, scope) {
                var pushData = eval('(' + js + ')');
                var objSelect = document.getElementById("NewCatagory");
                var option = new Option("所有类型", "0");
                objSelect.options.add(option);
                for (var i = 0; i < pushData.length; i++) {
                    var varItem = new Option(pushData[i].Name, pushData[i].No);
                    objSelect.options.add(varItem);
                }
            }, this);
        }
        //初始化
        $(function () {
            $("#pageloading").show();
            InitNewsCatagory();
            var pg = $('#newsGrid').datagrid('getPager');
            var curPage = $(pg).pagination.pageNumber;
            LoadGridData(1, 20);
        });
        //公共方法
        function queryData(param, callback, scope, method, showErrMsg) {
            if (!method) method = 'GET';
            $.ajax({
                type: method, //使用GET或POST方法访问后台
                dataType: "text", //返回json格式的数据
                contentType: "application/json; charset=utf-8",
                url: "WebLoagsSevice.ashx", //要访问的后台地址
                data: param, //要发送的数据
                async: false,
                cache: false,
                complete: function () { }, //AJAX请求完成时隐藏loading提示
                error: function (XMLHttpRequest, errorThrown) {
                    $("body").html("<b>访问页面出错，请联系管理员。<b>");
                    //callback(XMLHttpRequest);
                },
                success: function (msg) {//msg为返回的数据，在这里做数据绑定
                    var data = msg;
                    callback(data, scope);
                }
            });
        }
        /////////////////////添加///////////////////
        function add_dg() {
            ////表单清空
            $("#fm_dg").form('reset');
            //显示
            $("#dd_dg").show();
            //日历默认时间
            var dt = new Date
            var mstring = parseInt(dt.getMonth()) + 1;
            var dtstring = dt.getFullYear() + "-" + mstring + "-" + dt.getDate();
            $('#date').datebox('setValue', dtstring);
            //以窗体的形式展示
            $("#dd_dg").dialog({
                title: "添加日志",//标题
                iconCls: "icon-add",//图标
                //width: 650,//窗体的宽度
                //height: 550,//窗体的高度
                modal: true, //遮罩层
                fit: true,
                resizable: true,
                //按钮集合
                buttons: [
                {
                    text: "添加",//添加按钮的文本值
                    iconCls: "icon-ok", //添加按钮的图标
                    handler: function () {
                        //将数据序列化
                        var parm = $("#fm_dg").serialize();
                        //中文格式转换
                        var pp = decodeURIComponent(parm, true);
                        //post异步提交
                        if ($("#fm_dg").form('validate')) {
                            $.post("WebLoagsSevice.ashx", { mtd: "add", data: pp }, function (data) {
                                $.messager.alert('提示', data);
                                //重新加载datagrid
                                var pg = $('#newsGrid').datagrid('getPager');
                                var curPage = $(pg).pagination.pageNumber;
                                LoadGridData(curPage, 20);
                                //关闭
                                $("#dd_dg").window('close');

                            });

                        }
                    }
                },
                  {
                      text: "取消",
                      iconCls: "icon-cancel",
                      handler: function () {
                          $("#dd_dg").window("close");
                      }
                  }
                ]
            });
        }
        //////////////////修改//////////////////
        function edit_dg() {
            $("#fm_dg").form('reset');
            //选中一行，获取这一行的属性的值
            var selected = $("#newsGrid").datagrid('getSelected');
            //判断是否选中
            if (selected != null) {
                $("#no").val(selected.OID);
                var text = document.getElementById("<%=selecttype.ClientID%>");
                for (var i = 0; i < text.options.length; i++) {
                    if (text.options[i].innerText = selected.DRTypeText) {
                        text.options[i].selected = true;
                    }
                    else {
                        text.options[i].selected = false;
                    }
                }
                document.getElementById("logdoc").innerText = selected.Doc
                //取值显示性别
                $("#dd_dg").show(); //显示修改窗体
                $("#dd_dg").dialog({
                    title: "编辑信息",
                    iconCls: "icon-edit",
                    modal: true,//遮罩层
                    fit: true,
                    resizable: true,
                    //height: 600,
                    //width: 625,
                    buttons: [
                    {
                        text: "编辑",
                        iconCls: "icon-edit",
                        handler: function () {
                            var parm = $("#fm_dg").serialize();
                            var pp = decodeURIComponent(parm, true);

                            if ($("#fm_dg").form('validate')) {
                                $.post("WebLoagsSevice.ashx", { mtd: "edit", data: pp }, function (data) {
                                    $.messager.show({
                                        title: "提示",
                                        msg: data
                                    });
                                    var pg = $('#newsGrid').datagrid('getPager');
                                    var curPage = $(pg).pagination.pageNumber;
                                    LoadGridData(curPage, 20);
                                    $("#dd_dg").window("close");

                                });
                            }
                        }
                    },
                     {
                         text: "取消",
                         iconCls: "icon-cancel",
                         handler: function () {
                             $("#dd_dg").window('close');
                         }
                     }
                    ]
                });
            } else {
                $.messager.alert('提示', '请选中一行在进行编辑');
            }
        }
        /////////////////////////删除/////////////////
        function delete_dg() {
            var selected = $("#newsGrid").datagrid('getSelected');
            if (selected != null) {
                $.messager.confirm('提示', '是否确定要删除？', function (y) {
                    if (y) {
                        var v = "";
                        var checked = $("#newsGrid").datagrid('getChecked');
                        $.each(checked, function (i, j) {
                            v += j.OID + ",";
                        })
                        v = v.substring(0, v.length - 1);
                        $.post("WebLoagsSevice.ashx", { mtd: "del", id: v }, function (data) {
                            $.messager.alert('提示', data);
                            var pg = $('#newsGrid').datagrid('getPager');
                            var curPage = $(pg).pagination.pageNumber;
                            LoadGridData(curPage, 20);
                        });
                    }
                })
            } else {
                $.messager.alert('提示', '您还没有选中一行数，请选中在删除！');
            }
        }
        /////////////////////////清空查询/////////////////
        function LoadClear() {
            $("#searchfrom").form('reset');
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="cc" class="easyui-layout" data-options="fit:true">
        <div data-options="region:'north',title:'日志查询',split:true" style="height: 150px;">
            <form id="searchfrom" method="post">
                <table id="tableform" style="width: 100%; height: 100%">
                    <tr>
                        <th style="text-align: right;">日志类型：</th>
                        <td>
                            <select id="NewCatagory" name="NewCatagory"></select>

                        </td>
                    </tr>
                    <tr>
                        <th style="text-align: right;">关键字查询：</th>
                        <td>
                            <input id="TB_KeyWords" type="text" style="width: 150px;" />
                        </td>
                    </tr>
                    <tr>
                        <th style="text-align: right;">时间段：</th>
                        <td>
                            <input id="start" name="dd" type="text" class="easyui-datebox" editable="false" />---
                     <input id="end" type="text " name="dd" class="easyui-datebox" editable="false" />&nbsp&nbsp
                            <a id="DoQueryByKey" href="#" class="easyui-linkbutton" onclick="LoadGridData(1, 20)">查询</a>&nbsp&nbsp
                            <a id="DoClear" href="#" class="easyui-linkbutton" onclick="LoadClear()">清空</a></td>
                    </tr>
                </table>
            </form>
        </div>
        <div data-options="region:'center'" border="false" style="margin: 0; padding: 0; overflow: hidden;">
            <table id="newsGrid"   toolbar="#tb" class="easyui-datagrid">
            </table>
            <div id="tb" style="padding: 6px; height: 28px;">
                <%--  <div class="datagrid-btn-separator">
                </div>--%>
                <a id="A1" href="#" style="float: left;" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-add'"
                    onclick="add_dg();">添加日志</a>
                <a id="A1" href="#" style="float: left;" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-edit'"
                    onclick="edit_dg();">编辑日志</a>
                <a id="A1" href="#" style="float: left;" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-delete'"
                    onclick="delete_dg();">删除日志</a>
            </div>
        </div>
    </div>
    <div id="dd_dg" style="display: none ;"  >
        <form id="fm_dg" style="padding:5% 5%";>
            <input type="text" id="no" name="no" style="display: none" />
           <strong> 日志类型：</strong>
            <select id="selecttype" class="easyui-combobox" name="logtype" runat="server">
            </select>
            <br />
            <br />
           <strong> 日志时间：</strong>
            <input id="date" name="date" type="text" class="easyui-datebox" editable="false" />
            <br />
            <br />
            <strong>日志内容：</strong>
            <br />
            <br />
            <textarea id="logdoc" name="Doc" cols="65" rows="10" style="border: 1px solid black;" class="easyui-validatebox" data-options="required:true,missingMessage:'内容不能为空！'"></textarea><br />
            <br />
        </form>
    </div>
</asp:Content>

