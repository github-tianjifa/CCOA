﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BP.OA;
using BP.En;
using BP.Port;

namespace CCOA.App.DailyRecord
{
    public partial class WebForm2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            BP.Port.Depts depts = new BP.Port.Depts();
            depts.RetrieveAll();

            BP.Port.Emps emps = new BP.Port.Emps();
            emps.RetrieveAll();

            BP.OA.DRShares drs = new DRShares();
            drs.Retrieve(DRShareAttr.Sharer, BP.Web.WebUser.No);

            string strs = "";
            foreach (DRShare item in drs)
            {
                strs += "[" + item.ShareTo + "]";
            }


            this.Pub1.AddTable("width=90%");
            foreach (Dept dept in depts)
            {
                this.Pub1.AddTRSum();
                this.Pub1.AddTDB(dept.Name);
                this.Pub1.AddTREnd();

                this.Pub1.AddTR();
                this.Pub1.AddTDBegin();
                foreach (Emp emp in emps)
                {
                    if (emp.FK_Dept != dept.No)
                        continue;
                    CheckBox cb = new CheckBox();
                    cb.ID = "CB_" + emp.No;
                    cb.Text = emp.Name;
                    cb.Checked = strs.Contains("[" + emp.No + "]");
                    this.Pub1.Add(cb);
                }
                this.Pub1.AddTDEnd();
                this.Pub1.AddTREnd();
            }
            this.Pub1.AddTableEnd();

            Button btn = new Button();
            btn.Text = "保存";
            btn.ID = "Btn_Save";
            btn.Click += new EventHandler(btn_Click);
            this.Pub1.Add(btn);

        }

        void btn_Click(object sender, EventArgs e)
        {
            //删除数据.
            BP.OA.DRShare en = new DRShare();
         
            en.Delete(DRShareAttr.Sharer, BP.Web.WebUser.No);

            //保存树数据.
            BP.Port.Emps emps = new BP.Port.Emps();
            emps.RetrieveAll();
            foreach (Emp emp in emps)
            {
                CheckBox cb = this.Pub1.GetCBByID("CB_" + emp.No);
                if (cb != null && cb.Checked == true)
                {
                    DRShare share = new DRShare();
                    
                    en.Sharer = BP.Web.WebUser.No;
                    en.ShareTo = emp.No;
                  
                    en.Insert();
                }
            }
            BP.Sys.PubClass.Alert("保存成功");
        }
         
    }
}