﻿//下载文档
function DownLoadMyDoc() {
    var row = $('#docGrid').datagrid('getSelected');
    if (row) {
        if (row.DocType == 0 || row.DocType == "undefined") {
            $.messager.alert('提示', '不支持文件夹下载！', 'info');
            return;
        }
        window.open("FilesUpLoadServer.ashx?UploadType=download&MyPK=" + row.MyPK);
    } else {
        $.messager.alert('提示', '请选择记录后再进行下载！', 'info');
    }
}

//文档属性
function ViewFileInfo() {
    var dialogTitle = "";
    var row = $('#docGrid').datagrid('getSelected');
    if (row) {
        //文件修改
        dialogTitle = "文档属性";

        $("#TB_FileName").val(row.Title);
        $("#TB_KeyWord").val(row.KeyWords);
        $("#TB_Doc").val(row.Doc);
        var fileSizeVal = row.FileSize;
        var fileSizeText = "0";

        if (fileSizeVal) {
            fileSizeText = fileSizeVal + "KB";
            if (fileSizeVal > 1024) {
                fileSizeVal = fileSizeVal / 1024;
                fileSizeText = fileSizeVal + "M";
            }
        }
        $("#TB_FileSize").val(fileSizeText);

        $("#TB_FileSize").attr("disabled", "disabled");
        $("#TB_FileName").attr("disabled", "disabled");
        $("#TB_KeyWord").attr("disabled", "disabled");
        $("#TB_Doc").attr("disabled", "disabled");
    }
    else {
        $.messager.alert('提示', '请选择记录后再进行查看！', 'info');
        return;
    }
    //修改阅读次数
    OA.data.ReadTimeEdit(row.MyPK, function (js) {
        //弹出窗体
        $('#newDocDialog').dialog({
            title: dialogTitle,
            width: 600,
            height: 500,
            closed: false,
            modal: true,
            iconCls: 'icon-save',
            resizable: true,
            onClose: function () {
                $("#TB_FileName").removeAttr("disabled");
                $("#TB_KeyWord").removeAttr("disabled");
                $("#TB_Doc").removeAttr("disabled");
            },
            buttons: [{
                text: '确定',
                iconCls: 'icon-ok',
                handler: function () {
                    $("#TB_FileName").val('');
                    $("#TB_KeyWord").val('');
                    $("#TB_Doc").val('');

                    $('#newDocDialog').dialog("close");
                }
            }]
        });
    }, this);
}

//最新文档
function LoadNewDocGrid() {
    var searchContent = $("#TB_Search").val();
    //工具栏
    var toolbar = [{ 'text': '属性', 'iconCls': 'icon-save-close', 'handler': 'ViewFileInfo' },
                   { 'text': '下载', 'iconCls': 'icon-down', 'handler': 'DownLoadMyDoc' }
                   ];
    //查询数据
    OA.data.getNewLyDoc(searchContent, function (js, scope) {
        if (js) {
            if (js == "") js = "[]";
            var pushData = eval('(' + js + ')');
            $('#docGrid').datagrid({
                data: pushData,
                width: 'auto',
                toolbar: toolbar,
                striped: true,
                rownumbers: true,
                singleSelect: true,
                loadMsg: '数据加载中......',
                columns: [[{ field: 'Title', title: '文件名', width: 260, formatter: function (value, rec) {
                    var url = url = "<img align='middle' alt='' src='../../Images/FileType/" + rec.FileExt + ".gif'/>" + value;
                    return url;
                } 
                },
                { field: 'FK_EmpText', title: '发布人', width: 100, align: 'left' },
                { field: 'FK_DeptText', title: '部门', width: 90, align: 'left' },
                { field: 'IsShare', title: '来源', width: 120, align: 'left', formatter: function (value, rec) {
                    var text = "知识库";
                    if (rec.IsShare == 1) {
                        text = "共享";
                    }
                    return text;
                }
                },
                { field: 'RDT', title: '创建日期', width: 120, align: 'left' },
                { field: 'EDTER', title: '修改人', width: 100 },
                { field: 'EDT', title: '修改日期', width: 120 },
                { field: 'ReadTimes', title: '阅读次数', width: 90 },
                { field: 'DownLoadTimes', title: '下载次数', width: 90 }
                ]]
            });
        }
    }, this);
}
//搜索
function MyDocSearch() {
    LoadNewDocGrid();
}
//初始化
$(function () {
    LoadNewDocGrid();
});