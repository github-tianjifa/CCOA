﻿DELETE FROM OA_NoticeCategory;
INSERT INTO OA_NoticeCategory (No,Name) Values('01','放假通知');
INSERT INTO OA_NoticeCategory (No,Name) Values('02','人事调整');
INSERT INTO OA_NoticeCategory (No,Name) Values('03','公司会议');
INSERT INTO OA_NoticeCategory (No,Name) Values('04','紧急通知');

DELETE FROM OA_ArticleCatagory;
INSERT INTO OA_ArticleCatagory (No,Name) Values('01','体育新闻');
INSERT INTO OA_ArticleCatagory (No,Name) Values('02','娱乐新闻');
INSERT INTO OA_ArticleCatagory (No,Name) Values('03','财经新闻');
INSERT INTO OA_ArticleCatagory (No,Name) Values('04','社会新闻');
INSERT INTO OA_ArticleCatagory (No,Name) Values('05','时政新闻');
INSERT INTO OA_ArticleCatagory (No,Name) Values('06','驰骋动态');


DELETE FROM KM_Tree;
INSERT INTO KM_Tree (No,Name) Values('01','体育新闻');
INSERT INTO KM_Tree (No,Name) Values('02','娱乐新闻');
INSERT INTO KM_Tree (No,Name) Values('03','财经新闻');
INSERT INTO KM_Tree (No,Name) Values('04','社会新闻');
INSERT INTO KM_Tree (No,Name) Values('05','时政新闻');
INSERT INTO KM_Tree (No,Name) Values('06','驰骋动态');



DELETE FROM OA_DSSort;
DELETE FROM OA_DSMain;

INSERT INTO OA_DSSort (No,Name) Values('01','文件档案管理类');
INSERT INTO OA_DSMain (No,Name,FK_Sort,Unit,Spec,NumOfBuy,NumOfTake,NumOfLeft,UnitPrice,Cost)
 Values('0001','有孔文件夹（两孔、三孔文件夹）','01','个','',100,0,100,1002.00,1324);
 
INSERT INTO OA_DSMain (No,Name,FK_Sort,Unit,Spec,NumOfBuy,NumOfTake,NumOfLeft,UnitPrice,Cost)
 Values('0002','无孔文件夹（单强力夹、双强力夹等）','01','个','',100,0,100,1002.00,1324);
 
 INSERT INTO OA_DSMain (No,Name,FK_Sort,Unit,Spec,NumOfBuy,NumOfTake,NumOfLeft,UnitPrice,Cost)
 Values('0003','报告夹','01','个','',100,0,100,1002.00,1324);
 
 INSERT INTO OA_DSMain (No,Name,FK_Sort,Unit,Spec,NumOfBuy,NumOfTake,NumOfLeft,UnitPrice,Cost)
 Values('0004','板夹','01','个','',100,0,100,1002.00,1324);
 
 INSERT INTO OA_DSMain (No,Name,FK_Sort,Unit,Spec,NumOfBuy,NumOfTake,NumOfLeft,UnitPrice,Cost)
 Values('0005','分类文件夹','01','个','',100,0,100,1002.00,1324);
 
 INSERT INTO OA_DSMain (No,Name,FK_Sort,Unit,Spec,NumOfBuy,NumOfTake,NumOfLeft,UnitPrice,Cost)
 Values('0006','挂劳夹','01','个','',100,0,100,1002.00,1324);
 
  INSERT INTO OA_DSMain (No,Name,FK_Sort,Unit,Spec,NumOfBuy,NumOfTake,NumOfLeft,UnitPrice,Cost)
 Values('0007','票据夹','01','个','',100,0,100,1002.00,1324);
 
  INSERT INTO OA_DSMain (No,Name,FK_Sort,Unit,Spec,NumOfBuy,NumOfTake,NumOfLeft,UnitPrice,Cost)
 Values('0008','档案盒','01','个','',100,0,100,1002.00,1324);
 
  INSERT INTO OA_DSMain (No,Name,FK_Sort,Unit,Spec,NumOfBuy,NumOfTake,NumOfLeft,UnitPrice,Cost)
 Values('0009','包胶档案夹','01','个','',100,0,100,1002.00,1324);
 
  INSERT INTO OA_DSMain (No,Name,FK_Sort,Unit,Spec,NumOfBuy,NumOfTake,NumOfLeft,UnitPrice,Cost)
 Values('0010','资料册','01','个','',100,0,100,1002.00,1324);
 
  INSERT INTO OA_DSMain (No,Name,FK_Sort,Unit,Spec,NumOfBuy,NumOfTake,NumOfLeft,UnitPrice,Cost)
 Values('0011','档案袋','01','个','',100,0,100,1002.00,1324);
 
   INSERT INTO OA_DSMain (No,Name,FK_Sort,Unit,Spec,NumOfBuy,NumOfTake,NumOfLeft,UnitPrice,Cost)
 Values('0012','文件套','01','个','',100,0,100,1002.00,1324);
 
   INSERT INTO OA_DSMain (No,Name,FK_Sort,Unit,Spec,NumOfBuy,NumOfTake,NumOfLeft,UnitPrice,Cost)
 Values('0013','名片盒/册','01','个','',100,0,100,1002.00,1324);
 
   INSERT INTO OA_DSMain (No,Name,FK_Sort,Unit,Spec,NumOfBuy,NumOfTake,NumOfLeft,UnitPrice,Cost)
 Values('0014','CD包/册','01','个','',100,0,100,1002.00,1324);
 
   INSERT INTO OA_DSMain (No,Name,FK_Sort,Unit,Spec,NumOfBuy,NumOfTake,NumOfLeft,UnitPrice,Cost)
 Values('0015','公事包','01','个','',100,0,100,1002.00,1324);
 
   INSERT INTO OA_DSMain (No,Name,FK_Sort,Unit,Spec,NumOfBuy,NumOfTake,NumOfLeft,UnitPrice,Cost)
 Values('0016','拉链袋','01','个','',100,0,100,1002.00,1324);
 
   INSERT INTO OA_DSMain (No,Name,FK_Sort,Unit,Spec,NumOfBuy,NumOfTake,NumOfLeft,UnitPrice,Cost)
 Values('0017','卡片袋','01','个','',100,0,100,1002.00,1324);
 
    INSERT INTO OA_DSMain (No,Name,FK_Sort,Unit,Spec,NumOfBuy,NumOfTake,NumOfLeft,UnitPrice,Cost)
 Values('0018','文件柜','01','个','',100,0,100,1002.00,1324);
 
    INSERT INTO OA_DSMain (No,Name,FK_Sort,Unit,Spec,NumOfBuy,NumOfTake,NumOfLeft,UnitPrice,Cost)
 Values('0019','资料架','01','个','',100,0,100,1002.00,1324);
 
    INSERT INTO OA_DSMain (No,Name,FK_Sort,Unit,Spec,NumOfBuy,NumOfTake,NumOfLeft,UnitPrice,Cost)
 Values('0020','文件篮','01','个','',100,0,100,1002.00,1324);
 
     INSERT INTO OA_DSMain (No,Name,FK_Sort,Unit,Spec,NumOfBuy,NumOfTake,NumOfLeft,UnitPrice,Cost)
 Values('0021','书立','01','个','',100,0,100,1002.00,1324);
 
   INSERT INTO OA_DSMain (No,Name,FK_Sort,Unit,Spec,NumOfBuy,NumOfTake,NumOfLeft,UnitPrice,Cost)
 Values('0022','相册','01','个','',100,0,100,1002.00,1324);
 
  INSERT INTO OA_DSMain (No,Name,FK_Sort,Unit,Spec,NumOfBuy,NumOfTake,NumOfLeft,UnitPrice,Cost)
 Values('0023','图纸夹','01','个','',100,0,100,1002.00,1324); 


INSERT INTO OA_DSMain (No,Name) Values('01','文件档案管理类');
INSERT INTO OA_DSSort (No,Name) Values('02','桌面用品');
INSERT INTO OA_DSSort (No,Name) Values('03','办公簿本');
INSERT INTO OA_DSSort (No,Name) Values('04','书写修正用品');
INSERT INTO OA_DSSort (No,Name) Values('05','财务用品');
INSERT INTO OA_DSSort (No,Name) Values('06','辅助用品');
INSERT INTO OA_DSSort (No,Name) Values('07','电脑周边用品');
INSERT INTO OA_DSSort (No,Name) Values('08','电子电器用品');

--会议管理相关数据初始化

--会议类型
DELETE FROM  OA_MeetingType ;
INSERT INTO  OA_MeetingType(No,Name ,Note)VALUES('001','研发计划','项目研发技术探讨/项目进展总结/客户沟通');
INSERT INTO  OA_MeetingType(No,Name ,Note)VALUES('002','销售计划','销售团队周/月/年销售总结及计划');
INSERT INTO  OA_MeetingType(No,Name ,Note)VALUES('003','面试','应聘人员接待');
INSERT INTO  OA_MeetingType(No,Name ,Note)VALUES('004','年终总结','全员年终总结');
INSERT INTO  OA_MeetingType(No,Name ,Note)VALUES('005','临时会议','临时性未被计划会议');
INSERT INTO  OA_MeetingType(No,Name ,Note)VALUES('006','客户接待','客户会见，培训人员通知。');
     

--会议室资源
DELETE FROM  OA_RoomResource ;
INSERT INTO  OA_RoomResource(No,Name)VALUES('001','投影仪');
INSERT INTO  OA_RoomResource(No,Name)VALUES('002','电脑');
INSERT INTO  OA_RoomResource(No,Name)VALUES('003','移动画板');
INSERT INTO  OA_RoomResource(No,Name)VALUES('004','办公桌10张');
INSERT INTO  OA_RoomResource(No,Name)VALUES('005','转向椅30把');
INSERT INTO  OA_RoomResource(No,Name)VALUES('006','可移动黑板3张');
INSERT INTO  OA_RoomResource(No,Name)VALUES('007','记事本50本');
INSERT INTO  OA_RoomResource(No,Name)VALUES('008','油性笔100支');


--会议室
DELETE FROM  OA_Room ;
INSERT INTO  OA_Room (No,Name,RoomSta,FK_Emp,Fk_EmpName,Capacity,RoomResource,ResourceName,Note,Authority )
     VALUES ('001','23层1会议室','0','admin','admin','20','','','会议室没有投影仪','')  ;
INSERT INTO  OA_Room(No,Name,RoomSta,FK_Emp,Fk_EmpName,Capacity,RoomResource,ResourceName,Note,Authority )
     VALUES ('002','23层2会议室','0','admin','admin','10','','','会议室没有网络','');
INSERT INTO  OA_Room(No,Name,RoomSta,FK_Emp,Fk_EmpName,Capacity,RoomResource,ResourceName,Note,Authority )
     VALUES ('003','面试会议室','0','zhoupeng,','周朋','10','003,','移动画板','面试应聘者','')	;
INSERT INTO  OA_Room(No,Name,RoomSta,FK_Emp,Fk_EmpName,Capacity,RoomResource,ResourceName,Note,Authority )
     VALUES ('004','销售会议室','0','zhoupeng,','周朋','15','001,002,003,004,005,006,007,008,','投影仪,电脑,移动画板,办公桌10张,转向椅30把,可移动黑板3张,记事本50本,油性笔100支	','由总经理召开的销售总结会议','');
INSERT INTO  OA_Room(No,Name,RoomSta,FK_Emp,Fk_EmpName,Capacity,RoomResource,ResourceName,Note,Authority )
     VALUES ('005','研发部会议室','0','zhoupeng,','周朋	','20','001,002,003,004,005,006,007,008,','投影仪,电脑,移动画板,办公桌10张,转向椅30把,可移动黑板3张,记事本50本,油性笔100支','研发部成员针对项目技术/进展/客户沟通使用','');
     
     
     


 




