﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BP.Sys;

public partial class WF_Admin_DBInstall_CCOA : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (BP.WF.Glo.OSModel == BP.Sys.OSModel.OneOne)
        {
            this.Pub1.AddFieldSetRed("错误:", "CCOA的运行模式必须为嵌入模式，请修改web.config的OSModel的设置。");
            return;
        }

        if (this.Request.QueryString["DoType"] == "OK")
        {
            this.Response.Redirect("/App/Default.htm", true);
            return;
        }

        if (BP.DA.DBAccess.IsExitsObject("OA_Article") == true)
        {
            this.Pub1.AddFieldSet("提示");
            this.Pub1.Add("数据已经安装，如果您要重新安装，您需要建立一个空白的数据库。");
            this.Pub1.AddFieldSetEnd();

            this.Pub1.AddFieldSet("修复数据表");
            this.Pub1.Add("把最新的版本的与当前的数据表结构，做一个自动修复, 修复内容：缺少列，缺少列注释，列注释不完整或者有变化。 <br> <a href='DBInstall.aspx?DoType=FixDB' >执行...</a>。");
            this.Pub1.AddFieldSetEnd();

            this.Pub1.AddFieldSet("其他连接");
            this.Pub1.AddUL();
            this.Pub1.AddLi("<a href='/WF/admin/Xap/Designer.aspx'>登录流程设计器</a>");
            this.Pub1.AddLi("<a href='/GPM/'>权限管理</a>");
            this.Pub1.AddLi("<a href='/Main/Login/Login.aspx'>登录CCOA</a>");
            this.Pub1.AddULEnd();


            this.Pub1.AddFieldSetEnd();


            if (this.Request.QueryString["DoType"] == "FixDB")
            {
                string rpt = BP.Sys.PubClass.DBRpt(BP.DA.DBCheckLevel.High);
                this.Pub1.AddMsgGreen("同步数据表结构成功, 部分错误不会影响系统运行.",
                    "执行成功，希望在系统每次升级后执行此功能，不会对你的数据库数据产生影响。<br> <a href='./XAP/Designer.aspx'>进入流程设计器.</a>");
            }
            return;
        }

        RadioButton rb = new RadioButton();
        CheckBox cb = new CheckBox();
        this.Pub1.AddFieldSet("选择数据库安装类型, 要修改此类型需要在web.config中配置.");
        rb = new RadioButton();
        rb.Text = "SQLServer2000,2005,2008";
        rb.ID = "MSSQL";
        if (BP.Sys.SystemConfig.AppCenterDBType == BP.DA.DBType.MSSQL)
            rb.Checked = true;

        rb.Enabled = false;
        rb.GroupName = "sd";
        rb.Enabled = false;
        this.Pub1.Add(rb);
        this.Pub1.AddBR();

        rb = new RadioButton();
        rb.Text = "Oracle,Oracle 10g";
        rb.ID = "RB_Oracle";
        rb.GroupName = "sd";
        if (BP.Sys.SystemConfig.AppCenterDBType == BP.DA.DBType.Oracle)
            rb.Checked = true;

        rb.Enabled = false;
        this.Pub1.Add(rb);
        this.Pub1.AddBR();

        rb = new RadioButton();
        rb.Text = "DB2";
        rb.ID = "RB_DB2";
        rb.GroupName = "sd";
        if (BP.Sys.SystemConfig.AppCenterDBType == BP.DA.DBType.DB2)
            rb.Checked = true;

        rb.Enabled = false;
        this.Pub1.Add(rb);
        this.Pub1.AddBR();

        rb = new RadioButton();
        rb.Text = "MySQL";
        rb.ID = "RB_MYSQL";
        rb.GroupName = "sd";
        if (BP.Sys.SystemConfig.AppCenterDBType == BP.DA.DBType.MySQL)
            rb.Checked = true;
        

        rb.Enabled = false;
        this.Pub1.Add(rb);
        this.Pub1.AddBR();

        //rrb = new CheckBox();
        //rb.Text = "是否安装演示数据";
        //rb.GroupName = "ssd";
        //this.Pub1.Add(rrb);
        //this.Pub1.AddBR();
        this.Pub1.AddFieldSetEnd();


        this.Pub1.AddFieldSet("装载演示流程模版?");
        rb = new RadioButton();
        rb.Text = "我是技术人员，我安装技术类的设计演示模版.(估计在<font color=red>8-15分钟</font>内安装完成)。";
        rb.ID = "RB_DemoOn0";
        rb.GroupName = "hjd";
        rb.Checked = true;
        this.Pub1.Add(rb);
        this.Pub1.AddBR();

        rb = new RadioButton();
        rb.Text = "我是业务人员，我想了解与使用常用的业务流程(估计在<font color=red>5-13分钟</font>内安装完成)。";
        rb.ID = "RB_DemoOn1";
        rb.GroupName = "hjd";
        rb.Checked = false;
        this.Pub1.Add(rb);
        this.Pub1.AddBR();

        rb = new RadioButton();
        rb.Text = "否:不安装demo，仅仅安装空白的ccbpm环境(估计在<font color=red >2-3分钟</font>内安装完成)。";
        rb.ID = "RB_DemoOn2";
        rb.GroupName = "hjd";
        this.Pub1.Add(rb);
        this.Pub1.AddBR();
        this.Pub1.AddFieldSetEndBR();

        
        #region 安装CCFlow

        //若不需要独立运行trunk /ccflow 安装ccflow数据流程，则把此处放开
        //this.Pub1.AddFieldSet("是否装载演示流程模板?");
        //rb = new RadioButton();
        //rb.Text = "是:我要安装demo流程模板、表单模板，以方便我学习ccflow与ccform.";
        //rb.ID = "RB_DemoOn";
        //rb.GroupName = "hjd";
        //rb.Checked = true;
        //this.Pub1.Add(rb);
        //this.Pub1.AddBR();
        //rb = new RadioButton();
        //rb.Text = "否:不安装。";
        //rb.ID = "RB_DemoOff";
        //rb.GroupName = "hjd";
        //this.Pub1.Add(rb);
        //this.Pub1.AddBR();
        //this.Pub1.AddFieldSetEndBR();

        #endregion

        Button btn = new Button();
        btn.ID = "Btn_s";
        btn.Text = "接受CCOA的GPL开源协议执行安装.";
        btn.UseSubmitBehavior = false;
        btn.OnClientClick = "this.disabled=true;";
        btn.Click += new EventHandler(btn_Click);
        this.Pub1.Add(btn);

    }
    void btn_Click(object sender, EventArgs e)
    {
        //首先安装GPM.
        BP.GPM.Glo.DoInstallDataBase("CN", "Inc");

        //.运行OA安装.
        BP.OA.Glo.DoInstallDataBase();

        //是否要安装demo.
        int demoTye = 0;
        if (this.Pub1.GetRadioButtonByID("RB_DemoOn0").Checked)
            demoTye = 0;
        if (this.Pub1.GetRadioButtonByID("RB_DemoOn1").Checked)
            demoTye = 1;
        if (this.Pub1.GetRadioButtonByID("RB_DemoOn2").Checked)
            demoTye = 2;

        //安装ccflow .
        BP.WF.Glo.DoInstallDataBase("CH", demoTye);

        //升级ccflow.
        BP.WF.Glo.UpdataCCFlowVer();

        //如果启用加密
        if (BP.Sys.SystemConfig.IsEnablePasswordEncryption)
        {
            //用户密码加密
            BP.Port.Emps emps = new BP.Port.Emps();
            emps.RetrieveAll();
            foreach (BP.Port.Emp emp in emps)
            {
                if (emp.Pass.Length < 16)
                {
                    emp.Pass = BP.Tools.Cryptography.EncryptString(emp.Pass);
                    emp.Update();
                }
            }
        }
        this.Response.Redirect("/Main/Login/Login.aspx", true);
    }
}