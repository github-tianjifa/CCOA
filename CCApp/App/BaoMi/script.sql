USE [ccoa]
GO
/****** Object:  Table [dbo].[Sys_WFSealData]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sys_WFSealData](
	[MyPK] [nvarchar](100) NOT NULL,
	[OID] [nvarchar](200) NULL,
	[FK_Node] [nvarchar](200) NULL,
	[FK_MapData] [nvarchar](300) NULL,
	[SealData] [nvarchar](4000) NULL,
	[RDT] [nvarchar](20) NULL,
 CONSTRAINT [Sys_WFSealDatapk] PRIMARY KEY CLUSTERED 
(
	[MyPK] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Sys_UserVar]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sys_UserVar](
	[No] [nvarchar](30) NOT NULL,
	[Name] [nvarchar](120) NULL,
	[Val] [nvarchar](120) NULL,
	[GroupKey] [nvarchar](120) NULL,
	[Note] [nvarchar](4000) NULL,
 CONSTRAINT [Sys_UserVarpk] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Sys_UserRegedit]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sys_UserRegedit](
	[MyPK] [nvarchar](100) NOT NULL,
	[FK_Emp] [nvarchar](30) NULL,
	[CfgKey] [nvarchar](200) NULL,
	[Vals] [nvarchar](2000) NULL,
	[GenerSQL] [nvarchar](2000) NULL,
	[Paras] [nvarchar](2000) NULL,
	[NumKey] [nvarchar](300) NULL,
	[OrderBy] [nvarchar](300) NULL,
	[OrderWay] [nvarchar](300) NULL,
	[SearchKey] [nvarchar](300) NULL,
	[MVals] [nvarchar](300) NULL,
	[IsPic] [int] NULL,
	[DTFrom] [nvarchar](20) NULL,
	[DTTo] [nvarchar](20) NULL,
 CONSTRAINT [Sys_UserRegeditpk] PRIMARY KEY CLUSTERED 
(
	[MyPK] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Sys_UserRegedit] ([MyPK], [FK_Emp], [CfgKey], [Vals], [GenerSQL], [Paras], [NumKey], [OrderBy], [OrderWay], [SearchKey], [MVals], [IsPic], [DTFrom], [DTTo]) VALUES (N'52two0pv2f5xgoc03d5rkmlk', N'52two0pv2f5xgoc03d5rkmlk', N'', N'', N'', N'', N'', N'', N'', N'', N'', 0, N'', N'')
INSERT [dbo].[Sys_UserRegedit] ([MyPK], [FK_Emp], [CfgKey], [Vals], [GenerSQL], [Paras], [NumKey], [OrderBy], [OrderWay], [SearchKey], [MVals], [IsPic], [DTFrom], [DTTo]) VALUES (N'52two0pv2f5xgoc03d5rkmlkBP.BM.JSJs_Group', N'52two0pv2f5xgoc03d5rkmlk', N'BP.BM.JSJs_Group', N'', N'', N'', N'', N'', N'', N'', N'', 0, N'', N'')
INSERT [dbo].[Sys_UserRegedit] ([MyPK], [FK_Emp], [CfgKey], [Vals], [GenerSQL], [Paras], [NumKey], [OrderBy], [OrderWay], [SearchKey], [MVals], [IsPic], [DTFrom], [DTTo]) VALUES (N'52two0pv2f5xgoc03d5rkmlkBP.BM.SBDtls_Group', N'52two0pv2f5xgoc03d5rkmlk', N'BP.BM.SBDtls_Group', N'SelectedGroupKey=@FK_Emp@SBSta@SBSort@SBMJ@YYLB@GuangQu@CZXT@StateNumKey@MyNum=SUM@', N'SELECT FK_Emp,SBSta,SBSort,SBMJ,YYLB,GuangQu,CZXT, round ( SUM(MyNum), 4) MyNum FROM BM_SBDtl WHERE  FK_Dept LIKE  @V_Dept+~%~   GROUP BY FK_Emp,SBSta,SBSort,SBMJ,YYLB,GuangQu,CZXT ORDER BY CZXT', N'@V_Dept=100', N'MyNum', N'CZXT', N'Up', N'', N'', 0, N'', N'')
INSERT [dbo].[Sys_UserRegedit] ([MyPK], [FK_Emp], [CfgKey], [Vals], [GenerSQL], [Paras], [NumKey], [OrderBy], [OrderWay], [SearchKey], [MVals], [IsPic], [DTFrom], [DTTo]) VALUES (N'adminBP.BM.AnQuanCPs_SearchAttrs', N'admin', N'BP.BM.AnQuanCPs_SearchAttrs', N'', N'', N'', N'', N'', N'', N'', N'', 0, N'', N'')
INSERT [dbo].[Sys_UserRegedit] ([MyPK], [FK_Emp], [CfgKey], [Vals], [GenerSQL], [Paras], [NumKey], [OrderBy], [OrderWay], [SearchKey], [MVals], [IsPic], [DTFrom], [DTTo]) VALUES (N'adminBP.BM.AQCPs_SearchAttrs', N'admin', N'BP.BM.AQCPs_SearchAttrs', N'', N'', N'', N'', N'', N'', N'', N'', 0, N'', N'')
INSERT [dbo].[Sys_UserRegedit] ([MyPK], [FK_Emp], [CfgKey], [Vals], [GenerSQL], [Paras], [NumKey], [OrderBy], [OrderWay], [SearchKey], [MVals], [IsPic], [DTFrom], [DTTo]) VALUES (N'adminBP.BM.CardDtls_SearchAttrs', N'admin', N'BP.BM.CardDtls_SearchAttrs', N'@DDL_FK_Dept=100@DDL_CardType=all@DDL_Sta=all', N'', N'', N'', N'', N'', N'', N'', 0, N'', N'')
INSERT [dbo].[Sys_UserRegedit] ([MyPK], [FK_Emp], [CfgKey], [Vals], [GenerSQL], [Paras], [NumKey], [OrderBy], [OrderWay], [SearchKey], [MVals], [IsPic], [DTFrom], [DTTo]) VALUES (N'adminBP.BM.CRJDtls_SearchAttrs', N'admin', N'BP.BM.CRJDtls_SearchAttrs', N'', N'', N'', N'', N'', N'', N'', N'', 0, N'', N'')
INSERT [dbo].[Sys_UserRegedit] ([MyPK], [FK_Emp], [CfgKey], [Vals], [GenerSQL], [Paras], [NumKey], [OrderBy], [OrderWay], [SearchKey], [MVals], [IsPic], [DTFrom], [DTTo]) VALUES (N'adminBP.BM.EduDtls_SearchAttrs', N'admin', N'BP.BM.EduDtls_SearchAttrs', N'@DDL_FK_Dept=mvals@DDL_XXLX=all@DDL_SJLY=all', N'', N'', N'', N'', N'', N'', N'@FK_Dept=.100..1001..1002..1003..1004..1005..1060..1061..1062..1063..1070..1071..1072..1073..1099.', 0, N'', N'')
INSERT [dbo].[Sys_UserRegedit] ([MyPK], [FK_Emp], [CfgKey], [Vals], [GenerSQL], [Paras], [NumKey], [OrderBy], [OrderWay], [SearchKey], [MVals], [IsPic], [DTFrom], [DTTo]) VALUES (N'adminBP.BM.Emps_SearchAttrs', N'admin', N'BP.BM.Emps_SearchAttrs', N'@DDL_XB=all@DDL_EmpSta=all@DDL_EmpMJ=all@DDL_XueLi=all', N'', N'', N'', N'', N'', N'', N'', 0, N'', N'')
INSERT [dbo].[Sys_UserRegedit] ([MyPK], [FK_Emp], [CfgKey], [Vals], [GenerSQL], [Paras], [NumKey], [OrderBy], [OrderWay], [SearchKey], [MVals], [IsPic], [DTFrom], [DTTo]) VALUES (N'adminBP.BM.HuiYiDtls_SearchAttrs', N'admin', N'BP.BM.HuiYiDtls_SearchAttrs', N'', N'', N'', N'', N'', N'', N'', N'', 0, N'', N'')
INSERT [dbo].[Sys_UserRegedit] ([MyPK], [FK_Emp], [CfgKey], [Vals], [GenerSQL], [Paras], [NumKey], [OrderBy], [OrderWay], [SearchKey], [MVals], [IsPic], [DTFrom], [DTTo]) VALUES (N'adminBP.BM.JSJs_SearchAttrs', N'admin', N'BP.BM.JSJs_SearchAttrs', N'@DDL_FK_Dept=100@DDL_SBLB=all@DDL_JSJType=all@DDL_SBZT=all', N'', N'', N'', N'', N'', N'', N'', 0, N'', N'')
INSERT [dbo].[Sys_UserRegedit] ([MyPK], [FK_Emp], [CfgKey], [Vals], [GenerSQL], [Paras], [NumKey], [OrderBy], [OrderWay], [SearchKey], [MVals], [IsPic], [DTFrom], [DTTo]) VALUES (N'adminBP.BM.SBDtls_SearchAttrs', N'admin', N'BP.BM.SBDtls_SearchAttrs', N'@DDL_FK_Dept=100@DDL_SBSort=all@DDL_SBMJ=2', N'', N'', N'', N'', N'', N'', N'', 0, N'', N'')
INSERT [dbo].[Sys_UserRegedit] ([MyPK], [FK_Emp], [CfgKey], [Vals], [GenerSQL], [Paras], [NumKey], [OrderBy], [OrderWay], [SearchKey], [MVals], [IsPic], [DTFrom], [DTTo]) VALUES (N'adminBP.BM.SFWDtls_SearchAttrs', N'admin', N'BP.BM.SFWDtls_SearchAttrs', N'', N'', N'', N'', N'', N'', N'', N'', 0, N'', N'')
INSERT [dbo].[Sys_UserRegedit] ([MyPK], [FK_Emp], [CfgKey], [Vals], [GenerSQL], [Paras], [NumKey], [OrderBy], [OrderWay], [SearchKey], [MVals], [IsPic], [DTFrom], [DTTo]) VALUES (N'adminBP.BM.USBs_SearchAttrs', N'admin', N'BP.BM.USBs_SearchAttrs', N'', N'', N'', N'', N'', N'', N'', N'', 0, N'', N'')
INSERT [dbo].[Sys_UserRegedit] ([MyPK], [FK_Emp], [CfgKey], [Vals], [GenerSQL], [Paras], [NumKey], [OrderBy], [OrderWay], [SearchKey], [MVals], [IsPic], [DTFrom], [DTTo]) VALUES (N'adminBP.BM.WLJFs_SearchAttrs', N'admin', N'BP.BM.WLJFs_SearchAttrs', N'', N'', N'', N'', N'', N'', N'', N'', 0, N'', N'')
INSERT [dbo].[Sys_UserRegedit] ([MyPK], [FK_Emp], [CfgKey], [Vals], [GenerSQL], [Paras], [NumKey], [OrderBy], [OrderWay], [SearchKey], [MVals], [IsPic], [DTFrom], [DTTo]) VALUES (N'adminBP.BM.XCBDDtls_SearchAttrs', N'admin', N'BP.BM.XCBDDtls_SearchAttrs', N'@DDL_FK_Dept=100@DDL_XXSort=all@DDL_MJ=all', N'', N'', N'', N'', N'', N'', N'', 0, N'', N'')
INSERT [dbo].[Sys_UserRegedit] ([MyPK], [FK_Emp], [CfgKey], [Vals], [GenerSQL], [Paras], [NumKey], [OrderBy], [OrderWay], [SearchKey], [MVals], [IsPic], [DTFrom], [DTTo]) VALUES (N'adminBP.BM.ZDSorts_SearchAttrs', N'admin', N'BP.BM.ZDSorts_SearchAttrs', N'', N'', N'', N'', N'', N'', N'', N'', 0, N'', N'')
INSERT [dbo].[Sys_UserRegedit] ([MyPK], [FK_Emp], [CfgKey], [Vals], [GenerSQL], [Paras], [NumKey], [OrderBy], [OrderWay], [SearchKey], [MVals], [IsPic], [DTFrom], [DTTo]) VALUES (N'adminBP.BM.ZhiDus_SearchAttrs', N'admin', N'BP.BM.ZhiDus_SearchAttrs', N'@DDL_FK_Sort=all@DDL_FK_ND=all', N'', N'', N'', N'', N'', N'', N'', 0, N'', N'')
INSERT [dbo].[Sys_UserRegedit] ([MyPK], [FK_Emp], [CfgKey], [Vals], [GenerSQL], [Paras], [NumKey], [OrderBy], [OrderWay], [SearchKey], [MVals], [IsPic], [DTFrom], [DTTo]) VALUES (N'adminBP.DS.DSBuys_SearchAttrs', N'admin', N'BP.DS.DSBuys_SearchAttrs', N'', N'', N'', N'', N'', N'', N'', N'', 0, N'', N'')
INSERT [dbo].[Sys_UserRegedit] ([MyPK], [FK_Emp], [CfgKey], [Vals], [GenerSQL], [Paras], [NumKey], [OrderBy], [OrderWay], [SearchKey], [MVals], [IsPic], [DTFrom], [DTTo]) VALUES (N'adminBP.DS.DSMains_SearchAttrs', N'admin', N'BP.DS.DSMains_SearchAttrs', N'', N'', N'', N'', N'', N'', N'', N'', 0, N'', N'')
INSERT [dbo].[Sys_UserRegedit] ([MyPK], [FK_Emp], [CfgKey], [Vals], [GenerSQL], [Paras], [NumKey], [OrderBy], [OrderWay], [SearchKey], [MVals], [IsPic], [DTFrom], [DTTo]) VALUES (N'adminBP.DS.DSTakes_SearchAttrs', N'admin', N'BP.DS.DSTakes_SearchAttrs', N'', N'', N'', N'', N'', N'', N'', N'', 0, N'', N'')
INSERT [dbo].[Sys_UserRegedit] ([MyPK], [FK_Emp], [CfgKey], [Vals], [GenerSQL], [Paras], [NumKey], [OrderBy], [OrderWay], [SearchKey], [MVals], [IsPic], [DTFrom], [DTTo]) VALUES (N'adminBP.GPM.DeptTypes_SearchAttrs', N'admin', N'BP.GPM.DeptTypes_SearchAttrs', N'', N'', N'', N'', N'', N'', N'', N'', 0, N'', N'')
INSERT [dbo].[Sys_UserRegedit] ([MyPK], [FK_Emp], [CfgKey], [Vals], [GenerSQL], [Paras], [NumKey], [OrderBy], [OrderWay], [SearchKey], [MVals], [IsPic], [DTFrom], [DTTo]) VALUES (N'adminBP.GPM.Dutys_SearchAttrs', N'admin', N'BP.GPM.Dutys_SearchAttrs', N'', N'', N'', N'', N'', N'', N'', N'', 0, N'', N'')
INSERT [dbo].[Sys_UserRegedit] ([MyPK], [FK_Emp], [CfgKey], [Vals], [GenerSQL], [Paras], [NumKey], [OrderBy], [OrderWay], [SearchKey], [MVals], [IsPic], [DTFrom], [DTTo]) VALUES (N'adminBP.GPM.Groups_SearchAttrs', N'admin', N'BP.GPM.Groups_SearchAttrs', N'', N'', N'', N'', N'', N'', N'', N'', 0, N'', N'')
INSERT [dbo].[Sys_UserRegedit] ([MyPK], [FK_Emp], [CfgKey], [Vals], [GenerSQL], [Paras], [NumKey], [OrderBy], [OrderWay], [SearchKey], [MVals], [IsPic], [DTFrom], [DTTo]) VALUES (N'adminBP.GPM.Stations_SearchAttrs', N'admin', N'BP.GPM.Stations_SearchAttrs', N'', N'', N'', N'', N'', N'', N'', N'', 0, N'', N'')
INSERT [dbo].[Sys_UserRegedit] ([MyPK], [FK_Emp], [CfgKey], [Vals], [GenerSQL], [Paras], [NumKey], [OrderBy], [OrderWay], [SearchKey], [MVals], [IsPic], [DTFrom], [DTTo]) VALUES (N'adminBP.GPM.StationTypes_SearchAttrs', N'admin', N'BP.GPM.StationTypes_SearchAttrs', N'', N'', N'', N'', N'', N'', N'', N'', 0, N'', N'')
INSERT [dbo].[Sys_UserRegedit] ([MyPK], [FK_Emp], [CfgKey], [Vals], [GenerSQL], [Paras], [NumKey], [OrderBy], [OrderWay], [SearchKey], [MVals], [IsPic], [DTFrom], [DTTo]) VALUES (N'adminBP.OA.Article.ArticleCatagorys_SearchAttrs', N'admin', N'BP.OA.Article.ArticleCatagorys_SearchAttrs', N'', N'', N'', N'', N'', N'', N'', N'', 0, N'', N'')
INSERT [dbo].[Sys_UserRegedit] ([MyPK], [FK_Emp], [CfgKey], [Vals], [GenerSQL], [Paras], [NumKey], [OrderBy], [OrderWay], [SearchKey], [MVals], [IsPic], [DTFrom], [DTTo]) VALUES (N'adminBP.Pub.NDs_SearchAttrs', N'admin', N'BP.Pub.NDs_SearchAttrs', N'', N'', N'', N'', N'', N'', N'', N'', 0, N'', N'')
INSERT [dbo].[Sys_UserRegedit] ([MyPK], [FK_Emp], [CfgKey], [Vals], [GenerSQL], [Paras], [NumKey], [OrderBy], [OrderWay], [SearchKey], [MVals], [IsPic], [DTFrom], [DTTo]) VALUES (N'zhoupengBP.DS.DSBuys_SearchAttrs', N'zhoupeng', N'BP.DS.DSBuys_SearchAttrs', N'', N'', N'', N'', N'', N'', N'', N'', 0, N'', N'')
INSERT [dbo].[Sys_UserRegedit] ([MyPK], [FK_Emp], [CfgKey], [Vals], [GenerSQL], [Paras], [NumKey], [OrderBy], [OrderWay], [SearchKey], [MVals], [IsPic], [DTFrom], [DTTo]) VALUES (N'zhoupengBP.DS.DSMains_SearchAttrs', N'zhoupeng', N'BP.DS.DSMains_SearchAttrs', N'', N'', N'', N'', N'', N'', N'', N'', 0, N'', N'')
INSERT [dbo].[Sys_UserRegedit] ([MyPK], [FK_Emp], [CfgKey], [Vals], [GenerSQL], [Paras], [NumKey], [OrderBy], [OrderWay], [SearchKey], [MVals], [IsPic], [DTFrom], [DTTo]) VALUES (N'zhoupengBP.DS.DSTakes_SearchAttrs', N'zhoupeng', N'BP.DS.DSTakes_SearchAttrs', N'', N'', N'', N'', N'', N'', N'', N'', 0, N'', N'')
INSERT [dbo].[Sys_UserRegedit] ([MyPK], [FK_Emp], [CfgKey], [Vals], [GenerSQL], [Paras], [NumKey], [OrderBy], [OrderWay], [SearchKey], [MVals], [IsPic], [DTFrom], [DTTo]) VALUES (N'zhoupengBP.GPM.DeptTypes_SearchAttrs', N'zhoupeng', N'BP.GPM.DeptTypes_SearchAttrs', N'', N'', N'', N'', N'', N'', N'', N'', 0, N'', N'')
INSERT [dbo].[Sys_UserRegedit] ([MyPK], [FK_Emp], [CfgKey], [Vals], [GenerSQL], [Paras], [NumKey], [OrderBy], [OrderWay], [SearchKey], [MVals], [IsPic], [DTFrom], [DTTo]) VALUES (N'zhoupengBP.GPM.Groups_SearchAttrs', N'zhoupeng', N'BP.GPM.Groups_SearchAttrs', N'', N'', N'', N'', N'', N'', N'', N'', 0, N'', N'')
INSERT [dbo].[Sys_UserRegedit] ([MyPK], [FK_Emp], [CfgKey], [Vals], [GenerSQL], [Paras], [NumKey], [OrderBy], [OrderWay], [SearchKey], [MVals], [IsPic], [DTFrom], [DTTo]) VALUES (N'zhoupengBP.GPM.StationTypes_SearchAttrs', N'zhoupeng', N'BP.GPM.StationTypes_SearchAttrs', N'', N'', N'', N'', N'', N'', N'', N'', 0, N'', N'')
INSERT [dbo].[Sys_UserRegedit] ([MyPK], [FK_Emp], [CfgKey], [Vals], [GenerSQL], [Paras], [NumKey], [OrderBy], [OrderWay], [SearchKey], [MVals], [IsPic], [DTFrom], [DTTo]) VALUES (N'zhoupengBP.OA.Article.ArticleCatagorys_SearchAttrs', N'zhoupeng', N'BP.OA.Article.ArticleCatagorys_SearchAttrs', N'', N'', N'', N'', N'', N'', N'', N'', 0, N'', N'')
/****** Object:  Table [dbo].[Sys_UserLogT]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sys_UserLogT](
	[MyPK] [nvarchar](100) NOT NULL,
	[FK_Emp] [nvarchar](30) NULL,
	[IP] [nvarchar](200) NULL,
	[LogFlag] [nvarchar](300) NULL,
	[Docs] [nvarchar](300) NULL,
	[RDT] [nvarchar](20) NULL,
 CONSTRAINT [Sys_UserLogTpk] PRIMARY KEY CLUSTERED 
(
	[MyPK] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Sys_UserLogT] ([MyPK], [FK_Emp], [IP], [LogFlag], [Docs], [RDT]) VALUES (N'01aacc94-c28f-4ef2-9273-54b550af407b', N'admin', N'127.0.0.1', N'SignIn', N'登录', N'2015-07-02 16:25')
INSERT [dbo].[Sys_UserLogT] ([MyPK], [FK_Emp], [IP], [LogFlag], [Docs], [RDT]) VALUES (N'060126ef-c1f5-433e-877d-7f2d72dedb02', N'admin', N'127.0.0.1', N'SignIn', N'登录', N'2015-07-04 12:56')
INSERT [dbo].[Sys_UserLogT] ([MyPK], [FK_Emp], [IP], [LogFlag], [Docs], [RDT]) VALUES (N'086c78a5-595b-4c8d-b9b2-904dbad1940b', N'zhoupeng', N'127.0.0.1', N'SignIn', N'登录', N'2015-07-04 12:51')
INSERT [dbo].[Sys_UserLogT] ([MyPK], [FK_Emp], [IP], [LogFlag], [Docs], [RDT]) VALUES (N'0b7dd13e-f286-4962-9278-85f993301be0', N'admin', N'127.0.0.1', N'SignIn', N'登录', N'2015-07-03 21:17')
INSERT [dbo].[Sys_UserLogT] ([MyPK], [FK_Emp], [IP], [LogFlag], [Docs], [RDT]) VALUES (N'0cc68a30-df82-4407-a916-8aa370ef081c', N'admin', N'127.0.0.1', N'SignIn', N'登录', N'2015-07-04 14:58')
INSERT [dbo].[Sys_UserLogT] ([MyPK], [FK_Emp], [IP], [LogFlag], [Docs], [RDT]) VALUES (N'0f62fed9-a7fe-4a38-96bc-0c401ac763ef', N'admin', N'127.0.0.1', N'SignIn', N'登录', N'2015-07-03 21:00')
INSERT [dbo].[Sys_UserLogT] ([MyPK], [FK_Emp], [IP], [LogFlag], [Docs], [RDT]) VALUES (N'20c35499-f564-4d59-86ac-c443d77486c2', N'admin', N'127.0.0.1', N'SignIn', N'登录', N'2015-07-04 14:58')
INSERT [dbo].[Sys_UserLogT] ([MyPK], [FK_Emp], [IP], [LogFlag], [Docs], [RDT]) VALUES (N'20de9e2f-704e-4db9-a94f-88a350e44fd2', N'admin', N'127.0.0.1', N'SignIn', N'登录', N'2015-05-11 10:40')
INSERT [dbo].[Sys_UserLogT] ([MyPK], [FK_Emp], [IP], [LogFlag], [Docs], [RDT]) VALUES (N'217604af-42be-43b7-8af7-ff2dc7034ae7', N'zhoupeng', N'127.0.0.1', N'SignIn', N'登录', N'2015-07-04 12:58')
INSERT [dbo].[Sys_UserLogT] ([MyPK], [FK_Emp], [IP], [LogFlag], [Docs], [RDT]) VALUES (N'2c51376c-ef78-41b1-92a0-b2d4559d3772', N'zhoupeng', N'127.0.0.1', N'SignIn', N'登录', N'2015-07-02 16:25')
INSERT [dbo].[Sys_UserLogT] ([MyPK], [FK_Emp], [IP], [LogFlag], [Docs], [RDT]) VALUES (N'308ff80c-b4ec-4b6f-b124-f9aa0c50d2da', N'admin', N'127.0.0.1', N'SignIn', N'登录', N'2015-07-06 09:09')
INSERT [dbo].[Sys_UserLogT] ([MyPK], [FK_Emp], [IP], [LogFlag], [Docs], [RDT]) VALUES (N'3445ca83-cc0e-4609-a5d8-59a35a0b4b42', N'admin', N'127.0.0.1', N'SignIn', N'登录', N'2015-07-04 15:16')
INSERT [dbo].[Sys_UserLogT] ([MyPK], [FK_Emp], [IP], [LogFlag], [Docs], [RDT]) VALUES (N'357cd911-e506-4302-9f37-12a25368f1eb', N'admin', N'127.0.0.1', N'SignIn', N'登录', N'2015-05-11 10:40')
INSERT [dbo].[Sys_UserLogT] ([MyPK], [FK_Emp], [IP], [LogFlag], [Docs], [RDT]) VALUES (N'36edc712-d860-40ce-a158-e320b7a096c8', N'zhoupeng', N'127.0.0.1', N'SignIn', N'登录', N'2015-07-03 20:59')
INSERT [dbo].[Sys_UserLogT] ([MyPK], [FK_Emp], [IP], [LogFlag], [Docs], [RDT]) VALUES (N'3b01f28f-1b25-42b7-aae1-f963ae065c3c', N'zhoupeng', N'127.0.0.1', N'SignIn', N'登录', N'2015-05-11 09:56')
INSERT [dbo].[Sys_UserLogT] ([MyPK], [FK_Emp], [IP], [LogFlag], [Docs], [RDT]) VALUES (N'4b20b8bb-e317-493c-b878-954832e71f11', N'admin', N'127.0.0.1', N'SignIn', N'登录', N'2015-07-04 15:16')
INSERT [dbo].[Sys_UserLogT] ([MyPK], [FK_Emp], [IP], [LogFlag], [Docs], [RDT]) VALUES (N'5f3f9ae4-bf92-48e4-8fa5-a420777ba9f8', N'admin', N'127.0.0.1', N'SignIn', N'登录', N'2015-07-03 21:01')
INSERT [dbo].[Sys_UserLogT] ([MyPK], [FK_Emp], [IP], [LogFlag], [Docs], [RDT]) VALUES (N'62201b24-0730-4731-864f-b1c1c6b6b412', N'admin', N'127.0.0.1', N'SignIn', N'登录', N'2015-07-04 14:59')
INSERT [dbo].[Sys_UserLogT] ([MyPK], [FK_Emp], [IP], [LogFlag], [Docs], [RDT]) VALUES (N'67c088eb-4b62-48f9-a210-4163dea82979', N'admin', N'127.0.0.1', N'SignIn', N'登录', N'2015-07-04 14:55')
INSERT [dbo].[Sys_UserLogT] ([MyPK], [FK_Emp], [IP], [LogFlag], [Docs], [RDT]) VALUES (N'689517ad-13e4-42ed-80e0-7d9140e2a746', N'admin', N'127.0.0.1', N'SignIn', N'登录', N'2015-07-04 15:18')
INSERT [dbo].[Sys_UserLogT] ([MyPK], [FK_Emp], [IP], [LogFlag], [Docs], [RDT]) VALUES (N'6d5d72ce-8f61-4137-9165-18535bf60855', N'admin', N'127.0.0.1', N'SignIn', N'登录', N'2015-05-11 09:45')
INSERT [dbo].[Sys_UserLogT] ([MyPK], [FK_Emp], [IP], [LogFlag], [Docs], [RDT]) VALUES (N'719b1775-86e7-49dd-bdf0-fa3a3de217ce', N'admin', N'127.0.0.1', N'SignIn', N'登录', N'2015-07-04 15:16')
INSERT [dbo].[Sys_UserLogT] ([MyPK], [FK_Emp], [IP], [LogFlag], [Docs], [RDT]) VALUES (N'73a267fb-02d5-4b0c-bbfa-d0278a741329', N'admin', N'127.0.0.1', N'SignIn', N'登录', N'2015-07-03 21:01')
INSERT [dbo].[Sys_UserLogT] ([MyPK], [FK_Emp], [IP], [LogFlag], [Docs], [RDT]) VALUES (N'77d11447-010b-4178-8325-a54b62eb54aa', N'admin', N'127.0.0.1', N'SignIn', N'登录', N'2015-07-04 15:16')
INSERT [dbo].[Sys_UserLogT] ([MyPK], [FK_Emp], [IP], [LogFlag], [Docs], [RDT]) VALUES (N'7b08be61-8073-464f-a73a-8865a7cd4513', N'admin', N'127.0.0.1', N'SignIn', N'登录', N'2015-07-03 21:01')
INSERT [dbo].[Sys_UserLogT] ([MyPK], [FK_Emp], [IP], [LogFlag], [Docs], [RDT]) VALUES (N'7bbd3162-4dfb-40a1-8ae3-b3478bc2b2b9', N'liyan', N'127.0.0.1', N'SignIn', N'登录', N'2015-05-11 09:53')
INSERT [dbo].[Sys_UserLogT] ([MyPK], [FK_Emp], [IP], [LogFlag], [Docs], [RDT]) VALUES (N'90419a48-668b-4c39-86fd-cbe19586cf0d', N'admin', N'127.0.0.1', N'SignIn', N'登录', N'2015-07-04 13:16')
INSERT [dbo].[Sys_UserLogT] ([MyPK], [FK_Emp], [IP], [LogFlag], [Docs], [RDT]) VALUES (N'90f9aebe-b766-4f89-bd3f-d17ee41e4cbd', N'admin', N'127.0.0.1', N'SignIn', N'登录', N'2015-07-04 13:16')
INSERT [dbo].[Sys_UserLogT] ([MyPK], [FK_Emp], [IP], [LogFlag], [Docs], [RDT]) VALUES (N'9ed284ec-43c6-4815-ac90-00c6fdd725e6', N'admin', N'127.0.0.1', N'SignIn', N'登录', N'2015-07-04 15:13')
INSERT [dbo].[Sys_UserLogT] ([MyPK], [FK_Emp], [IP], [LogFlag], [Docs], [RDT]) VALUES (N'a10b8218-c011-449c-959f-c23d58d8ecf0', N'admin', N'127.0.0.1', N'SignIn', N'登录', N'2015-07-03 21:17')
INSERT [dbo].[Sys_UserLogT] ([MyPK], [FK_Emp], [IP], [LogFlag], [Docs], [RDT]) VALUES (N'a1d9c537-05fb-499f-a481-673b37a1b072', N'admin', N'127.0.0.1', N'SignIn', N'登录', N'2015-07-04 14:59')
INSERT [dbo].[Sys_UserLogT] ([MyPK], [FK_Emp], [IP], [LogFlag], [Docs], [RDT]) VALUES (N'a2920e82-9758-4a22-87a1-1e554ff2651f', N'admin', N'127.0.0.1', N'SignIn', N'登录', N'2015-07-04 15:18')
INSERT [dbo].[Sys_UserLogT] ([MyPK], [FK_Emp], [IP], [LogFlag], [Docs], [RDT]) VALUES (N'a70f96f6-3ac9-424c-b7e4-2cb73c5ffd67', N'admin', N'127.0.0.1', N'SignIn', N'登录', N'2015-07-03 21:00')
INSERT [dbo].[Sys_UserLogT] ([MyPK], [FK_Emp], [IP], [LogFlag], [Docs], [RDT]) VALUES (N'b3e1edb0-26fa-4f08-b236-dd79bc0560e6', N'admin', N'127.0.0.1', N'SignIn', N'登录', N'2015-05-11 10:40')
INSERT [dbo].[Sys_UserLogT] ([MyPK], [FK_Emp], [IP], [LogFlag], [Docs], [RDT]) VALUES (N'b4883e4d-3e6b-4914-bb61-f5d84042bf3f', N'admin', N'127.0.0.1', N'SignIn', N'登录', N'2015-07-04 14:58')
INSERT [dbo].[Sys_UserLogT] ([MyPK], [FK_Emp], [IP], [LogFlag], [Docs], [RDT]) VALUES (N'c0d10b5d-11c0-4035-ae65-c73b3c13d5ec', N'admin', N'127.0.0.1', N'SignIn', N'登录', N'2015-07-04 15:17')
INSERT [dbo].[Sys_UserLogT] ([MyPK], [FK_Emp], [IP], [LogFlag], [Docs], [RDT]) VALUES (N'd9bbc148-b67a-487f-a20a-594ebf83a3e9', N'zhoupeng', N'127.0.0.1', N'SignIn', N'登录', N'2015-05-11 09:52')
INSERT [dbo].[Sys_UserLogT] ([MyPK], [FK_Emp], [IP], [LogFlag], [Docs], [RDT]) VALUES (N'e45a7d71-f222-432b-9203-cadded708116', N'admin', N'127.0.0.1', N'SignIn', N'登录', N'2015-07-04 12:55')
INSERT [dbo].[Sys_UserLogT] ([MyPK], [FK_Emp], [IP], [LogFlag], [Docs], [RDT]) VALUES (N'e4c2f635-3f89-491f-83a1-edc4fb4364df', N'admin', N'127.0.0.1', N'SignIn', N'登录', N'2015-07-03 21:17')
INSERT [dbo].[Sys_UserLogT] ([MyPK], [FK_Emp], [IP], [LogFlag], [Docs], [RDT]) VALUES (N'e7979ad8-a7fc-4628-a5f6-86a0c8fbf35b', N'admin', N'127.0.0.1', N'SignIn', N'登录', N'2015-07-05 09:58')
INSERT [dbo].[Sys_UserLogT] ([MyPK], [FK_Emp], [IP], [LogFlag], [Docs], [RDT]) VALUES (N'ee8c82b7-32c9-4ea1-a02e-3c46d27121f6', N'admin', N'127.0.0.1', N'SignIn', N'登录', N'2015-07-04 14:59')
/****** Object:  Table [dbo].[Sys_SMS]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sys_SMS](
	[MyPK] [nvarchar](100) NOT NULL,
	[Sender] [nvarchar](200) NULL,
	[SendTo] [nvarchar](200) NULL,
	[RDT] [nvarchar](50) NULL,
	[Mobile] [nvarchar](30) NULL,
	[MobileSta] [int] NULL,
	[MobileInfo] [nvarchar](1000) NULL,
	[Email] [nvarchar](200) NULL,
	[EmaiSta] [int] NULL,
	[EmailTitle] [nvarchar](3000) NULL,
	[EmailDoc] [nvarchar](4000) NULL,
	[SendDT] [nvarchar](50) NULL,
	[IsRead] [int] NULL,
	[IsAlert] [int] NULL,
	[MsgFlag] [nvarchar](200) NULL,
	[MsgType] [nvarchar](200) NULL,
 CONSTRAINT [Sys_SMSpk] PRIMARY KEY CLUSTERED 
(
	[MyPK] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Sys_SFTable]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sys_SFTable](
	[No] [nvarchar](20) NOT NULL,
	[Name] [nvarchar](30) NULL,
	[FK_Val] [nvarchar](50) NULL,
	[TableDesc] [nvarchar](50) NULL,
	[DefVal] [nvarchar](200) NULL,
	[IsTree] [int] NULL,
	[SFTableType] [int] NULL,
	[FK_SFDBSrc] [nvarchar](100) NULL,
	[SrcTable] [nvarchar](50) NULL,
	[ColumnValue] [nvarchar](50) NULL,
	[ColumnText] [nvarchar](50) NULL,
	[ParentValue] [nvarchar](50) NULL,
	[SelectStatement] [nvarchar](1000) NULL,
 CONSTRAINT [Sys_SFTablepk] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Sys_SFDBSrc]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sys_SFDBSrc](
	[No] [nvarchar](20) NOT NULL,
	[Name] [nvarchar](30) NULL,
	[UserID] [nvarchar](30) NULL,
	[Password] [nvarchar](30) NULL,
	[IP] [nvarchar](50) NULL,
	[DBName] [nvarchar](30) NULL,
	[DBSrcType] [int] NULL,
 CONSTRAINT [Sys_SFDBSrcpk] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Sys_Serial]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sys_Serial](
	[CfgKey] [nvarchar](100) NOT NULL,
	[IntVal] [int] NULL,
 CONSTRAINT [Sys_Serialpk] PRIMARY KEY CLUSTERED 
(
	[CfgKey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Sys_Serial] ([CfgKey], [IntVal]) VALUES (N'BP.GPM.Menu', 119)
INSERT [dbo].[Sys_Serial] ([CfgKey], [IntVal]) VALUES (N'OID', 197)
INSERT [dbo].[Sys_Serial] ([CfgKey], [IntVal]) VALUES (N'Ver', 201506131)
/****** Object:  Table [dbo].[Sys_RptTemplate]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sys_RptTemplate](
	[MyPK] [nvarchar](100) NOT NULL,
	[EnsName] [nvarchar](500) NULL,
	[FK_Emp] [nvarchar](20) NULL,
	[D1] [nvarchar](90) NULL,
	[D2] [nvarchar](90) NULL,
	[D3] [nvarchar](90) NULL,
	[AlObjs] [nvarchar](90) NULL,
	[Height] [int] NULL,
	[Width] [int] NULL,
	[IsSumBig] [int] NULL,
	[IsSumLittle] [int] NULL,
	[IsSumRight] [int] NULL,
	[PercentModel] [int] NULL,
 CONSTRAINT [Sys_RptTemplatepk] PRIMARY KEY CLUSTERED 
(
	[MyPK] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Sys_RptStation]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sys_RptStation](
	[FK_Rpt] [nvarchar](15) NOT NULL,
	[FK_Station] [nvarchar](100) NOT NULL,
 CONSTRAINT [Sys_RptStationpk] PRIMARY KEY CLUSTERED 
(
	[FK_Rpt] ASC,
	[FK_Station] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Sys_RptEmp]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sys_RptEmp](
	[FK_Rpt] [nvarchar](15) NOT NULL,
	[FK_Emp] [nvarchar](100) NOT NULL,
 CONSTRAINT [Sys_RptEmppk] PRIMARY KEY CLUSTERED 
(
	[FK_Rpt] ASC,
	[FK_Emp] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Sys_RptDept]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sys_RptDept](
	[FK_Rpt] [nvarchar](15) NOT NULL,
	[FK_Dept] [nvarchar](100) NOT NULL,
 CONSTRAINT [Sys_RptDeptpk] PRIMARY KEY CLUSTERED 
(
	[FK_Rpt] ASC,
	[FK_Dept] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Sys_MapM2M]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sys_MapM2M](
	[MyPK] [nvarchar](100) NOT NULL,
	[FK_MapData] [nvarchar](30) NULL,
	[NoOfObj] [nvarchar](20) NULL,
	[Name] [nvarchar](200) NULL,
	[DBOfLists] [nvarchar](4000) NULL,
	[DBOfObjs] [nvarchar](4000) NULL,
	[DBOfGroups] [nvarchar](4000) NULL,
	[H] [float] NULL,
	[W] [float] NULL,
	[X] [float] NULL,
	[Y] [float] NULL,
	[ShowWay] [int] NULL,
	[M2MType] [int] NULL,
	[RowIdx] [int] NULL,
	[GroupID] [int] NULL,
	[Cols] [int] NULL,
	[IsDelete] [int] NULL,
	[IsInsert] [int] NULL,
	[IsCheckAll] [int] NULL,
 CONSTRAINT [Sys_MapM2Mpk] PRIMARY KEY CLUSTERED 
(
	[MyPK] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Sys_MapFrame]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sys_MapFrame](
	[MyPK] [nvarchar](100) NOT NULL,
	[NoOfObj] [nvarchar](20) NULL,
	[Name] [nvarchar](200) NULL,
	[FK_MapData] [nvarchar](30) NULL,
	[URL] [nvarchar](3000) NULL,
	[W] [nvarchar](20) NULL,
	[H] [nvarchar](20) NULL,
	[IsAutoSize] [int] NULL,
	[RowIdx] [int] NULL,
	[GroupID] [int] NULL,
	[GUID] [nvarchar](128) NULL,
 CONSTRAINT [Sys_MapFramepk] PRIMARY KEY CLUSTERED 
(
	[MyPK] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Sys_MapExt]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sys_MapExt](
	[MyPK] [nvarchar](100) NOT NULL,
	[FK_MapData] [nvarchar](30) NULL,
	[ExtType] [nvarchar](30) NULL,
	[DoWay] [int] NULL,
	[AttrOfOper] [nvarchar](30) NULL,
	[AttrsOfActive] [nvarchar](900) NULL,
	[Doc] [nvarchar](4000) NULL,
	[Tag] [nvarchar](2000) NULL,
	[Tag1] [nvarchar](2000) NULL,
	[Tag2] [nvarchar](2000) NULL,
	[Tag3] [nvarchar](2000) NULL,
	[AtPara] [nvarchar](2000) NULL,
	[DBSrc] [nvarchar](20) NULL,
	[H] [int] NULL,
	[W] [int] NULL,
	[PRI] [int] NULL,
 CONSTRAINT [Sys_MapExtpk] PRIMARY KEY CLUSTERED 
(
	[MyPK] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Sys_MapDtl]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sys_MapDtl](
	[No] [nvarchar](80) NOT NULL,
	[Name] [nvarchar](200) NULL,
	[FK_MapData] [nvarchar](30) NULL,
	[PTable] [nvarchar](200) NULL,
	[GroupField] [nvarchar](300) NULL,
	[Model] [int] NULL,
	[ImpFixTreeSql] [nvarchar](500) NULL,
	[ImpFixDataSql] [nvarchar](500) NULL,
	[RowIdx] [int] NULL,
	[GroupID] [int] NULL,
	[RowsOfList] [int] NULL,
	[IsEnableGroupField] [int] NULL,
	[IsShowSum] [int] NULL,
	[IsShowIdx] [int] NULL,
	[IsCopyNDData] [int] NULL,
	[IsHLDtl] [int] NULL,
	[IsReadonly] [int] NULL,
	[IsShowTitle] [int] NULL,
	[IsView] [int] NULL,
	[IsInsert] [int] NULL,
	[IsDelete] [int] NULL,
	[IsUpdate] [int] NULL,
	[IsEnablePass] [int] NULL,
	[IsEnableAthM] [int] NULL,
	[IsEnableM2M] [int] NULL,
	[IsEnableM2MM] [int] NULL,
	[WhenOverSize] [int] NULL,
	[DtlOpenType] [int] NULL,
	[DtlShowModel] [int] NULL,
	[X] [float] NULL,
	[Y] [float] NULL,
	[H] [float] NULL,
	[W] [float] NULL,
	[FrmW] [float] NULL,
	[FrmH] [float] NULL,
	[MTR] [nvarchar](3000) NULL,
	[GUID] [nvarchar](128) NULL,
	[FK_Node] [int] NULL,
	[AtPara] [nvarchar](300) NULL,
	[IsExp] [int] NULL,
	[IsImp] [int] NULL,
	[IsEnableSelectImp] [int] NULL,
	[ImpSQLSearch] [nvarchar](500) NULL,
	[ImpSQLInit] [nvarchar](500) NULL,
	[ImpSQLFull] [nvarchar](500) NULL,
 CONSTRAINT [Sys_MapDtlpk] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Sys_MapData]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sys_MapData](
	[No] [nvarchar](20) NOT NULL,
	[Name] [nvarchar](500) NULL,
	[EnPK] [nvarchar](10) NULL,
	[PTable] [nvarchar](500) NULL,
	[Url] [nvarchar](500) NULL,
	[Dtls] [nvarchar](500) NULL,
	[FrmW] [int] NULL,
	[FrmH] [int] NULL,
	[TableCol] [int] NULL,
	[TableWidth] [int] NULL,
	[DBURL] [int] NULL,
	[Tag] [nvarchar](500) NULL,
	[FrmType] [int] NULL,
	[FK_FrmSort] [nvarchar](500) NULL,
	[FK_FormTree] [nvarchar](500) NULL,
	[AppType] [int] NULL,
	[Note] [nvarchar](500) NULL,
	[Designer] [nvarchar](500) NULL,
	[DesignerUnit] [nvarchar](500) NULL,
	[DesignerContact] [nvarchar](500) NULL,
	[AtPara] [nvarchar](4000) NULL,
	[Idx] [int] NULL,
	[GUID] [nvarchar](128) NULL,
	[Ver] [nvarchar](30) NULL,
	[OfficeOpenLab] [nvarchar](50) NULL,
	[OfficeOpenEnable] [int] NULL,
	[OfficeOpenTemplateLab] [nvarchar](50) NULL,
	[OfficeOpenTemplateEnable] [int] NULL,
	[OfficeSaveLab] [nvarchar](50) NULL,
	[OfficeSaveEnable] [int] NULL,
	[OfficeAcceptLab] [nvarchar](50) NULL,
	[OfficeAcceptEnable] [int] NULL,
	[OfficeRefuseLab] [nvarchar](50) NULL,
	[OfficeRefuseEnable] [int] NULL,
	[OfficeOverLab] [nvarchar](50) NULL,
	[OfficeOverEnable] [int] NULL,
	[OfficeMarksEnable] [int] NULL,
	[OfficePrintLab] [nvarchar](50) NULL,
	[OfficePrintEnable] [int] NULL,
	[OfficeSealLab] [nvarchar](50) NULL,
	[OfficeSealEnable] [int] NULL,
	[OfficeInsertFlowLab] [nvarchar](50) NULL,
	[OfficeInsertFlowEnable] [int] NULL,
	[OfficeNodeInfo] [int] NULL,
	[OfficeReSavePDF] [int] NULL,
	[OfficeDownLab] [nvarchar](50) NULL,
	[OfficeDownEnable] [int] NULL,
	[OfficeIsMarks] [int] NULL,
	[OfficeTemplate] [nvarchar](100) NULL,
	[OfficeIsParent] [int] NULL,
	[OfficeTHEnable] [int] NULL,
	[OfficeTHTemplate] [nvarchar](200) NULL,
	[FormRunType] [int] NULL,
	[FK_Flow] [nvarchar](50) NULL,
	[ParentMapData] [nvarchar](128) NULL,
	[RightViewWay] [int] NULL,
	[RightViewTag] [nvarchar](4000) NULL,
	[RightDeptWay] [int] NULL,
	[RightDeptTag] [nvarchar](4000) NULL,
 CONSTRAINT [Sys_MapDatapk] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Sys_MapData] ([No], [Name], [EnPK], [PTable], [Url], [Dtls], [FrmW], [FrmH], [TableCol], [TableWidth], [DBURL], [Tag], [FrmType], [FK_FrmSort], [FK_FormTree], [AppType], [Note], [Designer], [DesignerUnit], [DesignerContact], [AtPara], [Idx], [GUID], [Ver], [OfficeOpenLab], [OfficeOpenEnable], [OfficeOpenTemplateLab], [OfficeOpenTemplateEnable], [OfficeSaveLab], [OfficeSaveEnable], [OfficeAcceptLab], [OfficeAcceptEnable], [OfficeRefuseLab], [OfficeRefuseEnable], [OfficeOverLab], [OfficeOverEnable], [OfficeMarksEnable], [OfficePrintLab], [OfficePrintEnable], [OfficeSealLab], [OfficeSealEnable], [OfficeInsertFlowLab], [OfficeInsertFlowEnable], [OfficeNodeInfo], [OfficeReSavePDF], [OfficeDownLab], [OfficeDownEnable], [OfficeIsMarks], [OfficeTemplate], [OfficeIsParent], [OfficeTHEnable], [OfficeTHTemplate], [FormRunType], [FK_Flow], [ParentMapData], [RightViewWay], [RightViewTag], [RightDeptWay], [RightDeptTag]) VALUES (N'ND101', N'开始节点', N'', N'ND101', N'', N'', 900, 1200, 4, 600, 0, N'', 0, N'', N'', 1, N'', N'', N'', N'', N'@IsHaveCA=0', 100, N'', N'2015-07-04 14:59:08', N'打开本地', 0, N'打开模板', 0, N'保存', 1, N'接受修订', 0, N'拒绝修订', 0, N'套红按钮', 0, 1, N'打印按钮', 0, N'签章按钮', 0, N'插入流程', 0, 0, 0, N'下载', 0, 1, N'', 1, 0, N'', 1, N'', N'', 0, N'', 0, N'')
INSERT [dbo].[Sys_MapData] ([No], [Name], [EnPK], [PTable], [Url], [Dtls], [FrmW], [FrmH], [TableCol], [TableWidth], [DBURL], [Tag], [FrmType], [FK_FrmSort], [FK_FormTree], [AppType], [Note], [Designer], [DesignerUnit], [DesignerContact], [AtPara], [Idx], [GUID], [Ver], [OfficeOpenLab], [OfficeOpenEnable], [OfficeOpenTemplateLab], [OfficeOpenTemplateEnable], [OfficeSaveLab], [OfficeSaveEnable], [OfficeAcceptLab], [OfficeAcceptEnable], [OfficeRefuseLab], [OfficeRefuseEnable], [OfficeOverLab], [OfficeOverEnable], [OfficeMarksEnable], [OfficePrintLab], [OfficePrintEnable], [OfficeSealLab], [OfficeSealEnable], [OfficeInsertFlowLab], [OfficeInsertFlowEnable], [OfficeNodeInfo], [OfficeReSavePDF], [OfficeDownLab], [OfficeDownEnable], [OfficeIsMarks], [OfficeTemplate], [OfficeIsParent], [OfficeTHEnable], [OfficeTHTemplate], [FormRunType], [FK_Flow], [ParentMapData], [RightViewWay], [RightViewTag], [RightDeptWay], [RightDeptTag]) VALUES (N'ND102', N'节点2', N'', N'ND102', N'', N'', 900, 1200, 4, 600, 0, N'', 0, N'', N'', 1, N'', N'', N'', N'', N'@IsHaveCA=0', 100, N'', N'2015-07-04 14:59:07', N'打开本地', 0, N'打开模板', 0, N'保存', 1, N'接受修订', 0, N'拒绝修订', 0, N'套红按钮', 0, 1, N'打印按钮', 0, N'签章按钮', 0, N'插入流程', 0, 0, 0, N'下载', 0, 1, N'', 1, 0, N'', 1, N'', N'', 0, N'', 0, N'')
INSERT [dbo].[Sys_MapData] ([No], [Name], [EnPK], [PTable], [Url], [Dtls], [FrmW], [FrmH], [TableCol], [TableWidth], [DBURL], [Tag], [FrmType], [FK_FrmSort], [FK_FormTree], [AppType], [Note], [Designer], [DesignerUnit], [DesignerContact], [AtPara], [Idx], [GUID], [Ver], [OfficeOpenLab], [OfficeOpenEnable], [OfficeOpenTemplateLab], [OfficeOpenTemplateEnable], [OfficeSaveLab], [OfficeSaveEnable], [OfficeAcceptLab], [OfficeAcceptEnable], [OfficeRefuseLab], [OfficeRefuseEnable], [OfficeOverLab], [OfficeOverEnable], [OfficeMarksEnable], [OfficePrintLab], [OfficePrintEnable], [OfficeSealLab], [OfficeSealEnable], [OfficeInsertFlowLab], [OfficeInsertFlowEnable], [OfficeNodeInfo], [OfficeReSavePDF], [OfficeDownLab], [OfficeDownEnable], [OfficeIsMarks], [OfficeTemplate], [OfficeIsParent], [OfficeTHEnable], [OfficeTHTemplate], [FormRunType], [FK_Flow], [ParentMapData], [RightViewWay], [RightViewTag], [RightDeptWay], [RightDeptTag]) VALUES (N'ND1MyRpt', N'涉密人员入职报表', N'', N'ND1Rpt', N'', N'', 900, 1200, 4, 600, 0, N'', 0, N'', N'', 1, N'默认.', N'', N'', N'', N'@IsHaveCA=0', 100, N'', N'2015-07-04 14:59:08', N'打开本地', 0, N'打开模板', 0, N'保存', 1, N'接受修订', 0, N'拒绝修订', 0, N'套红按钮', 0, 1, N'打印按钮', 0, N'签章按钮', 0, N'插入流程', 0, 0, 0, N'下载', 0, 1, N'', 1, 0, N'', 1, N'001', N'ND1Rpt', 0, N'', 0, N'')
INSERT [dbo].[Sys_MapData] ([No], [Name], [EnPK], [PTable], [Url], [Dtls], [FrmW], [FrmH], [TableCol], [TableWidth], [DBURL], [Tag], [FrmType], [FK_FrmSort], [FK_FormTree], [AppType], [Note], [Designer], [DesignerUnit], [DesignerContact], [AtPara], [Idx], [GUID], [Ver], [OfficeOpenLab], [OfficeOpenEnable], [OfficeOpenTemplateLab], [OfficeOpenTemplateEnable], [OfficeSaveLab], [OfficeSaveEnable], [OfficeAcceptLab], [OfficeAcceptEnable], [OfficeRefuseLab], [OfficeRefuseEnable], [OfficeOverLab], [OfficeOverEnable], [OfficeMarksEnable], [OfficePrintLab], [OfficePrintEnable], [OfficeSealLab], [OfficeSealEnable], [OfficeInsertFlowLab], [OfficeInsertFlowEnable], [OfficeNodeInfo], [OfficeReSavePDF], [OfficeDownLab], [OfficeDownEnable], [OfficeIsMarks], [OfficeTemplate], [OfficeIsParent], [OfficeTHEnable], [OfficeTHTemplate], [FormRunType], [FK_Flow], [ParentMapData], [RightViewWay], [RightViewTag], [RightDeptWay], [RightDeptTag]) VALUES (N'ND1Rpt', N'涉密人员入职', N'', N'ND1Rpt', N'', N'', 900, 1200, 4, 600, 0, N'', 0, N'', N'', 1, N'', N'', N'', N'', N'@IsHaveCA=0', 100, N'', N'2015-07-04 14:59:08', N'打开本地', 0, N'打开模板', 0, N'保存', 1, N'接受修订', 0, N'拒绝修订', 0, N'套红按钮', 0, 1, N'打印按钮', 0, N'签章按钮', 0, N'插入流程', 0, 0, 0, N'下载', 0, 1, N'', 1, 0, N'', 1, N'', N'', 0, N'', 0, N'')
INSERT [dbo].[Sys_MapData] ([No], [Name], [EnPK], [PTable], [Url], [Dtls], [FrmW], [FrmH], [TableCol], [TableWidth], [DBURL], [Tag], [FrmType], [FK_FrmSort], [FK_FormTree], [AppType], [Note], [Designer], [DesignerUnit], [DesignerContact], [AtPara], [Idx], [GUID], [Ver], [OfficeOpenLab], [OfficeOpenEnable], [OfficeOpenTemplateLab], [OfficeOpenTemplateEnable], [OfficeSaveLab], [OfficeSaveEnable], [OfficeAcceptLab], [OfficeAcceptEnable], [OfficeRefuseLab], [OfficeRefuseEnable], [OfficeOverLab], [OfficeOverEnable], [OfficeMarksEnable], [OfficePrintLab], [OfficePrintEnable], [OfficeSealLab], [OfficeSealEnable], [OfficeInsertFlowLab], [OfficeInsertFlowEnable], [OfficeNodeInfo], [OfficeReSavePDF], [OfficeDownLab], [OfficeDownEnable], [OfficeIsMarks], [OfficeTemplate], [OfficeIsParent], [OfficeTHEnable], [OfficeTHTemplate], [FormRunType], [FK_Flow], [ParentMapData], [RightViewWay], [RightViewTag], [RightDeptWay], [RightDeptTag]) VALUES (N'ND201', N'开始节点', N'', N'ND201', N'', N'', 900, 1200, 4, 600, 0, N'', 0, N'', N'', 1, N'', N'', N'', N'', N'@IsHaveCA=0', 100, N'', N'2015-07-04 15:16:19', N'打开本地', 0, N'打开模板', 0, N'保存', 1, N'接受修订', 0, N'拒绝修订', 0, N'套红按钮', 0, 1, N'打印按钮', 0, N'签章按钮', 0, N'插入流程', 0, 0, 0, N'下载', 0, 1, N'', 1, 0, N'', 1, N'', N'', 0, N'', 0, N'')
INSERT [dbo].[Sys_MapData] ([No], [Name], [EnPK], [PTable], [Url], [Dtls], [FrmW], [FrmH], [TableCol], [TableWidth], [DBURL], [Tag], [FrmType], [FK_FrmSort], [FK_FormTree], [AppType], [Note], [Designer], [DesignerUnit], [DesignerContact], [AtPara], [Idx], [GUID], [Ver], [OfficeOpenLab], [OfficeOpenEnable], [OfficeOpenTemplateLab], [OfficeOpenTemplateEnable], [OfficeSaveLab], [OfficeSaveEnable], [OfficeAcceptLab], [OfficeAcceptEnable], [OfficeRefuseLab], [OfficeRefuseEnable], [OfficeOverLab], [OfficeOverEnable], [OfficeMarksEnable], [OfficePrintLab], [OfficePrintEnable], [OfficeSealLab], [OfficeSealEnable], [OfficeInsertFlowLab], [OfficeInsertFlowEnable], [OfficeNodeInfo], [OfficeReSavePDF], [OfficeDownLab], [OfficeDownEnable], [OfficeIsMarks], [OfficeTemplate], [OfficeIsParent], [OfficeTHEnable], [OfficeTHTemplate], [FormRunType], [FK_Flow], [ParentMapData], [RightViewWay], [RightViewTag], [RightDeptWay], [RightDeptTag]) VALUES (N'ND202', N'节点2', N'', N'ND202', N'', N'', 900, 1200, 4, 600, 0, N'', 0, N'', N'', 1, N'', N'', N'', N'', N'@IsHaveCA=0', 100, N'', N'2015-07-04 15:16:19', N'打开本地', 0, N'打开模板', 0, N'保存', 1, N'接受修订', 0, N'拒绝修订', 0, N'套红按钮', 0, 1, N'打印按钮', 0, N'签章按钮', 0, N'插入流程', 0, 0, 0, N'下载', 0, 1, N'', 1, 0, N'', 1, N'', N'', 0, N'', 0, N'')
INSERT [dbo].[Sys_MapData] ([No], [Name], [EnPK], [PTable], [Url], [Dtls], [FrmW], [FrmH], [TableCol], [TableWidth], [DBURL], [Tag], [FrmType], [FK_FrmSort], [FK_FormTree], [AppType], [Note], [Designer], [DesignerUnit], [DesignerContact], [AtPara], [Idx], [GUID], [Ver], [OfficeOpenLab], [OfficeOpenEnable], [OfficeOpenTemplateLab], [OfficeOpenTemplateEnable], [OfficeSaveLab], [OfficeSaveEnable], [OfficeAcceptLab], [OfficeAcceptEnable], [OfficeRefuseLab], [OfficeRefuseEnable], [OfficeOverLab], [OfficeOverEnable], [OfficeMarksEnable], [OfficePrintLab], [OfficePrintEnable], [OfficeSealLab], [OfficeSealEnable], [OfficeInsertFlowLab], [OfficeInsertFlowEnable], [OfficeNodeInfo], [OfficeReSavePDF], [OfficeDownLab], [OfficeDownEnable], [OfficeIsMarks], [OfficeTemplate], [OfficeIsParent], [OfficeTHEnable], [OfficeTHTemplate], [FormRunType], [FK_Flow], [ParentMapData], [RightViewWay], [RightViewTag], [RightDeptWay], [RightDeptTag]) VALUES (N'ND2MyRpt', N'密级变更报表', N'', N'ND2Rpt', N'', N'', 900, 1200, 4, 600, 0, N'', 0, N'', N'', 1, N'默认.', N'', N'', N'', N'@IsHaveCA=0', 100, N'', N'2015-07-04 15:16:20', N'打开本地', 0, N'打开模板', 0, N'保存', 1, N'接受修订', 0, N'拒绝修订', 0, N'套红按钮', 0, 1, N'打印按钮', 0, N'签章按钮', 0, N'插入流程', 0, 0, 0, N'下载', 0, 1, N'', 1, 0, N'', 1, N'002', N'ND2Rpt', 0, N'', 0, N'')
INSERT [dbo].[Sys_MapData] ([No], [Name], [EnPK], [PTable], [Url], [Dtls], [FrmW], [FrmH], [TableCol], [TableWidth], [DBURL], [Tag], [FrmType], [FK_FrmSort], [FK_FormTree], [AppType], [Note], [Designer], [DesignerUnit], [DesignerContact], [AtPara], [Idx], [GUID], [Ver], [OfficeOpenLab], [OfficeOpenEnable], [OfficeOpenTemplateLab], [OfficeOpenTemplateEnable], [OfficeSaveLab], [OfficeSaveEnable], [OfficeAcceptLab], [OfficeAcceptEnable], [OfficeRefuseLab], [OfficeRefuseEnable], [OfficeOverLab], [OfficeOverEnable], [OfficeMarksEnable], [OfficePrintLab], [OfficePrintEnable], [OfficeSealLab], [OfficeSealEnable], [OfficeInsertFlowLab], [OfficeInsertFlowEnable], [OfficeNodeInfo], [OfficeReSavePDF], [OfficeDownLab], [OfficeDownEnable], [OfficeIsMarks], [OfficeTemplate], [OfficeIsParent], [OfficeTHEnable], [OfficeTHTemplate], [FormRunType], [FK_Flow], [ParentMapData], [RightViewWay], [RightViewTag], [RightDeptWay], [RightDeptTag]) VALUES (N'ND2Rpt', N'密级变更', N'', N'ND2Rpt', N'', N'', 900, 1200, 4, 600, 0, N'', 0, N'', N'', 1, N'', N'', N'', N'', N'@IsHaveCA=0', 100, N'', N'2015-07-04 15:16:20', N'打开本地', 0, N'打开模板', 0, N'保存', 1, N'接受修订', 0, N'拒绝修订', 0, N'套红按钮', 0, 1, N'打印按钮', 0, N'签章按钮', 0, N'插入流程', 0, 0, 0, N'下载', 0, 1, N'', 1, 0, N'', 1, N'', N'', 0, N'', 0, N'')
INSERT [dbo].[Sys_MapData] ([No], [Name], [EnPK], [PTable], [Url], [Dtls], [FrmW], [FrmH], [TableCol], [TableWidth], [DBURL], [Tag], [FrmType], [FK_FrmSort], [FK_FormTree], [AppType], [Note], [Designer], [DesignerUnit], [DesignerContact], [AtPara], [Idx], [GUID], [Ver], [OfficeOpenLab], [OfficeOpenEnable], [OfficeOpenTemplateLab], [OfficeOpenTemplateEnable], [OfficeSaveLab], [OfficeSaveEnable], [OfficeAcceptLab], [OfficeAcceptEnable], [OfficeRefuseLab], [OfficeRefuseEnable], [OfficeOverLab], [OfficeOverEnable], [OfficeMarksEnable], [OfficePrintLab], [OfficePrintEnable], [OfficeSealLab], [OfficeSealEnable], [OfficeInsertFlowLab], [OfficeInsertFlowEnable], [OfficeNodeInfo], [OfficeReSavePDF], [OfficeDownLab], [OfficeDownEnable], [OfficeIsMarks], [OfficeTemplate], [OfficeIsParent], [OfficeTHEnable], [OfficeTHTemplate], [FormRunType], [FK_Flow], [ParentMapData], [RightViewWay], [RightViewTag], [RightDeptWay], [RightDeptTag]) VALUES (N'ND301', N'开始节点', N'', N'ND301', N'', N'', 900, 1200, 4, 600, 0, N'', 0, N'', N'', 1, N'', N'', N'', N'', N'@IsHaveCA=0', 100, N'', N'2015-07-04 15:16:34', N'打开本地', 0, N'打开模板', 0, N'保存', 1, N'接受修订', 0, N'拒绝修订', 0, N'套红按钮', 0, 1, N'打印按钮', 0, N'签章按钮', 0, N'插入流程', 0, 0, 0, N'下载', 0, 1, N'', 1, 0, N'', 1, N'', N'', 0, N'', 0, N'')
INSERT [dbo].[Sys_MapData] ([No], [Name], [EnPK], [PTable], [Url], [Dtls], [FrmW], [FrmH], [TableCol], [TableWidth], [DBURL], [Tag], [FrmType], [FK_FrmSort], [FK_FormTree], [AppType], [Note], [Designer], [DesignerUnit], [DesignerContact], [AtPara], [Idx], [GUID], [Ver], [OfficeOpenLab], [OfficeOpenEnable], [OfficeOpenTemplateLab], [OfficeOpenTemplateEnable], [OfficeSaveLab], [OfficeSaveEnable], [OfficeAcceptLab], [OfficeAcceptEnable], [OfficeRefuseLab], [OfficeRefuseEnable], [OfficeOverLab], [OfficeOverEnable], [OfficeMarksEnable], [OfficePrintLab], [OfficePrintEnable], [OfficeSealLab], [OfficeSealEnable], [OfficeInsertFlowLab], [OfficeInsertFlowEnable], [OfficeNodeInfo], [OfficeReSavePDF], [OfficeDownLab], [OfficeDownEnable], [OfficeIsMarks], [OfficeTemplate], [OfficeIsParent], [OfficeTHEnable], [OfficeTHTemplate], [FormRunType], [FK_Flow], [ParentMapData], [RightViewWay], [RightViewTag], [RightDeptWay], [RightDeptTag]) VALUES (N'ND302', N'节点2', N'', N'ND302', N'', N'', 900, 1200, 4, 600, 0, N'', 0, N'', N'', 1, N'', N'', N'', N'', N'@IsHaveCA=0', 100, N'', N'2015-07-04 15:16:34', N'打开本地', 0, N'打开模板', 0, N'保存', 1, N'接受修订', 0, N'拒绝修订', 0, N'套红按钮', 0, 1, N'打印按钮', 0, N'签章按钮', 0, N'插入流程', 0, 0, 0, N'下载', 0, 1, N'', 1, 0, N'', 1, N'', N'', 0, N'', 0, N'')
INSERT [dbo].[Sys_MapData] ([No], [Name], [EnPK], [PTable], [Url], [Dtls], [FrmW], [FrmH], [TableCol], [TableWidth], [DBURL], [Tag], [FrmType], [FK_FrmSort], [FK_FormTree], [AppType], [Note], [Designer], [DesignerUnit], [DesignerContact], [AtPara], [Idx], [GUID], [Ver], [OfficeOpenLab], [OfficeOpenEnable], [OfficeOpenTemplateLab], [OfficeOpenTemplateEnable], [OfficeSaveLab], [OfficeSaveEnable], [OfficeAcceptLab], [OfficeAcceptEnable], [OfficeRefuseLab], [OfficeRefuseEnable], [OfficeOverLab], [OfficeOverEnable], [OfficeMarksEnable], [OfficePrintLab], [OfficePrintEnable], [OfficeSealLab], [OfficeSealEnable], [OfficeInsertFlowLab], [OfficeInsertFlowEnable], [OfficeNodeInfo], [OfficeReSavePDF], [OfficeDownLab], [OfficeDownEnable], [OfficeIsMarks], [OfficeTemplate], [OfficeIsParent], [OfficeTHEnable], [OfficeTHTemplate], [FormRunType], [FK_Flow], [ParentMapData], [RightViewWay], [RightViewTag], [RightDeptWay], [RightDeptTag]) VALUES (N'ND3MyRpt', N'涉密人员注销报表', N'', N'ND3Rpt', N'', N'', 900, 1200, 4, 600, 0, N'', 0, N'', N'', 1, N'默认.', N'', N'', N'', N'@IsHaveCA=0', 100, N'', N'2015-07-04 15:16:34', N'打开本地', 0, N'打开模板', 0, N'保存', 1, N'接受修订', 0, N'拒绝修订', 0, N'套红按钮', 0, 1, N'打印按钮', 0, N'签章按钮', 0, N'插入流程', 0, 0, 0, N'下载', 0, 1, N'', 1, 0, N'', 1, N'003', N'ND3Rpt', 0, N'', 0, N'')
INSERT [dbo].[Sys_MapData] ([No], [Name], [EnPK], [PTable], [Url], [Dtls], [FrmW], [FrmH], [TableCol], [TableWidth], [DBURL], [Tag], [FrmType], [FK_FrmSort], [FK_FormTree], [AppType], [Note], [Designer], [DesignerUnit], [DesignerContact], [AtPara], [Idx], [GUID], [Ver], [OfficeOpenLab], [OfficeOpenEnable], [OfficeOpenTemplateLab], [OfficeOpenTemplateEnable], [OfficeSaveLab], [OfficeSaveEnable], [OfficeAcceptLab], [OfficeAcceptEnable], [OfficeRefuseLab], [OfficeRefuseEnable], [OfficeOverLab], [OfficeOverEnable], [OfficeMarksEnable], [OfficePrintLab], [OfficePrintEnable], [OfficeSealLab], [OfficeSealEnable], [OfficeInsertFlowLab], [OfficeInsertFlowEnable], [OfficeNodeInfo], [OfficeReSavePDF], [OfficeDownLab], [OfficeDownEnable], [OfficeIsMarks], [OfficeTemplate], [OfficeIsParent], [OfficeTHEnable], [OfficeTHTemplate], [FormRunType], [FK_Flow], [ParentMapData], [RightViewWay], [RightViewTag], [RightDeptWay], [RightDeptTag]) VALUES (N'ND3Rpt', N'涉密人员注销', N'', N'ND3Rpt', N'', N'', 900, 1200, 4, 600, 0, N'', 0, N'', N'', 1, N'', N'', N'', N'', N'@IsHaveCA=0', 100, N'', N'2015-07-04 15:16:34', N'打开本地', 0, N'打开模板', 0, N'保存', 1, N'接受修订', 0, N'拒绝修订', 0, N'套红按钮', 0, 1, N'打印按钮', 0, N'签章按钮', 0, N'插入流程', 0, 0, 0, N'下载', 0, 1, N'', 1, 0, N'', 1, N'', N'', 0, N'', 0, N'')
/****** Object:  Table [dbo].[Sys_MapAttr]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sys_MapAttr](
	[MyPK] [nvarchar](100) NOT NULL,
	[FK_MapData] [nvarchar](200) NULL,
	[KeyOfEn] [nvarchar](200) NULL,
	[Name] [nvarchar](200) NULL,
	[DefVal] [nvarchar](4000) NULL,
	[UIContralType] [int] NULL,
	[MyDataType] [int] NULL,
	[LGType] [int] NULL,
	[UIWidth] [float] NULL,
	[UIHeight] [float] NULL,
	[MinLen] [int] NULL,
	[MaxLen] [int] NULL,
	[UIBindKey] [nvarchar](100) NULL,
	[UIRefKey] [nvarchar](30) NULL,
	[UIRefKeyText] [nvarchar](30) NULL,
	[UIVisible] [int] NULL,
	[UIIsEnable] [int] NULL,
	[UIIsLine] [int] NULL,
	[Idx] [int] NULL,
	[GroupID] [int] NULL,
	[IsSigan] [int] NULL,
	[X] [float] NULL,
	[Y] [float] NULL,
	[GUID] [nvarchar](128) NULL,
	[Tag] [nvarchar](100) NULL,
	[EditType] [int] NULL,
	[ColSpan] [int] NULL,
	[AtPara] [nvarchar](4000) NULL,
 CONSTRAINT [Sys_MapAttrpk] PRIMARY KEY CLUSTERED 
(
	[MyPK] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND101_CDT', N'ND101', N'CDT', N'发起时间', N'@RDT', 0, 7, 0, 100, 23, 0, 300, N'', N'', N'', 0, 0, 0, 999, 101, 0, 5, 5, N'', N'1', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND101_Emps', N'ND101', N'Emps', N'Emps', N'', 0, 1, 0, 100, 23, 0, 400, N'', N'', N'', 0, 0, 0, 999, 101, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND101_FID', N'ND101', N'FID', N'FID', N'0', 0, 2, 0, 100, 23, 0, 300, N'', N'', N'', 0, 0, 0, 999, 101, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND101_FK_Dept', N'ND101', N'FK_Dept', N'操作员部门', N'', 0, 1, 0, 100, 23, 0, 20, N'', N'', N'', 0, 0, 0, 999, 101, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND101_FK_NY', N'ND101', N'FK_NY', N'年月', N'', 0, 1, 0, 100, 23, 0, 7, N'', N'', N'', 0, 0, 0, 999, 101, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND101_MyNum', N'ND101', N'MyNum', N'个数', N'1', 0, 2, 0, 100, 23, 0, 300, N'', N'', N'', 0, 0, 0, 999, 101, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND101_OID', N'ND101', N'OID', N'WorkID', N'0', 0, 2, 0, 100, 23, 0, 300, N'', N'', N'', 0, 0, 0, 999, 101, 0, 5, 5, N'', N'', 2, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND101_PRI', N'ND101', N'PRI', N'优先级', N'2', 1, 2, 1, 100, 23, 0, 200, N'PRI', N'', N'', 1, 1, 0, 999, 101, 0, 174.76, 56.19, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND101_RDT', N'ND101', N'RDT', N'接受时间', N'', 0, 7, 0, 100, 23, 0, 300, N'', N'', N'', 0, 0, 0, 999, 101, 0, 5, 5, N'', N'1', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND101_Rec', N'ND101', N'Rec', N'发起人', N'@WebUser.No', 0, 1, 0, 100, 23, 0, 20, N'', N'', N'', 0, 0, 0, 999, 101, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND101_Title', N'ND101', N'Title', N'标题', N'', 0, 1, 0, 251, 23, 0, 200, N'', N'', N'', 0, 0, 1, 999, 101, 0, 174.83, 54.4, N'', N'', 0, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND102_CDT', N'ND102', N'CDT', N'完成时间', N'@RDT', 0, 7, 0, 100, 23, 0, 300, N'', N'', N'', 0, 0, 0, 999, 0, 0, 5, 5, N'', N'1', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND102_Emps', N'ND102', N'Emps', N'Emps', N'', 0, 1, 0, 100, 23, 0, 400, N'', N'', N'', 0, 0, 0, 999, 0, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND102_FID', N'ND102', N'FID', N'FID', N'0', 0, 2, 0, 100, 23, 0, 300, N'', N'', N'', 0, 0, 0, 999, 0, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND102_FK_Dept', N'ND102', N'FK_Dept', N'操作员部门', N'', 0, 1, 0, 100, 23, 0, 32, N'', N'', N'', 0, 0, 0, 999, 0, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND102_OID', N'ND102', N'OID', N'WorkID', N'0', 0, 2, 0, 100, 23, 0, 300, N'', N'', N'', 0, 0, 0, 999, 0, 0, 5, 5, N'', N'', 2, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND102_RDT', N'ND102', N'RDT', N'接受时间', N'', 0, 7, 0, 100, 23, 0, 300, N'', N'', N'', 0, 0, 0, 999, 0, 0, 5, 5, N'', N'1', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND102_Rec', N'ND102', N'Rec', N'记录人', N'@WebUser.No', 0, 1, 0, 100, 23, 0, 20, N'', N'', N'', 0, 0, 0, 999, 0, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND1MyRpt_FK_Dept', N'ND1MyRpt', N'FK_Dept', N'操作员部门', N'', 1, 1, 2, 100, 23, 0, 100, N'BP.Port.Depts', N'', N'', 1, 0, 0, 999, 0, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND1MyRpt_FK_NY', N'ND1MyRpt', N'FK_NY', N'年月', N'', 1, 1, 2, 100, 23, 0, 7, N'BP.Pub.NYs', N'', N'', 1, 0, 0, 999, 0, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND1MyRpt_FlowDaySpan', N'ND1MyRpt', N'FlowDaySpan', N'跨度(天)', N'', 0, 8, 0, 100, 23, 0, 300, N'', N'', N'', 1, 1, 0, 999, 103, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND1MyRpt_FlowEmps', N'ND1MyRpt', N'FlowEmps', N'参与人', N'', 0, 1, 0, 100, 23, 0, 1000, N'', N'', N'', 1, 0, 1, 999, 103, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND1MyRpt_FlowEnder', N'ND1MyRpt', N'FlowEnder', N'结束人', N'', 1, 1, 2, 100, 23, 0, 20, N'BP.Port.Emps', N'', N'', 1, 0, 0, 999, 103, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND1MyRpt_FlowEnderRDT', N'ND1MyRpt', N'FlowEnderRDT', N'结束时间', N'', 0, 7, 0, 100, 23, 0, 300, N'', N'', N'', 1, 0, 0, 999, 103, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND1MyRpt_FlowEndNode', N'ND1MyRpt', N'FlowEndNode', N'结束节点', N'0', 0, 2, 0, 100, 23, 0, 300, N'', N'', N'', 1, 0, 0, 999, 103, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND1MyRpt_FlowStarter', N'ND1MyRpt', N'FlowStarter', N'发起人', N'', 1, 1, 2, 100, 23, 0, 20, N'BP.Port.Emps', N'', N'', 1, 0, 0, 999, 103, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND1MyRpt_FlowStartRDT', N'ND1MyRpt', N'FlowStartRDT', N'发起时间', N'', 0, 7, 0, 100, 23, 0, 300, N'', N'', N'', 1, 0, 0, 999, 103, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND1MyRpt_OID', N'ND1MyRpt', N'OID', N'WorkID', N'0', 0, 2, 0, 100, 23, 0, 300, N'', N'', N'', 0, 0, 0, 999, 101, 0, 5, 5, N'', N'', 2, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND1MyRpt_Title', N'ND1MyRpt', N'Title', N'标题', N'', 0, 1, 0, 251, 23, 0, 200, N'', N'', N'', 0, 0, 1, 999, 101, 0, 174.83, 54.4, N'', N'', 0, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND1MyRpt_WFSta', N'ND1MyRpt', N'WFSta', N'状态', N'', 1, 2, 1, 100, 23, 0, 1000, N'WFSta', N'', N'', 1, 0, 0, 999, 0, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND1MyRpt_WFState', N'ND1MyRpt', N'WFState', N'流程状态', N'', 1, 2, 1, 100, 23, 0, 1000, N'WFState', N'', N'', 1, 0, 0, 999, 103, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND1Rpt_AtPara', N'ND1Rpt', N'AtPara', N'参数', N'', 0, 1, 0, 100, 23, 0, 4000, N'', N'', N'', 0, 0, 0, 999, 0, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND1Rpt_BillNo', N'ND1Rpt', N'BillNo', N'单据编号', N'', 0, 1, 0, 100, 23, 0, 100, N'', N'', N'', 1, 0, 0, 999, 0, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND1Rpt_CDT', N'ND1Rpt', N'CDT', N'活动时间', N'', 0, 7, 0, 100, 23, 0, 300, N'', N'', N'', 0, 0, 0, 999, 101, 0, 5, 5, N'', N'1', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND1Rpt_CFlowNo', N'ND1Rpt', N'CFlowNo', N'延续流程编号', N'', 0, 1, 0, 100, 23, 0, 3, N'', N'', N'', 1, 0, 1, 999, 0, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND1Rpt_CWorkID', N'ND1Rpt', N'CWorkID', N'延续流程WorkID', N'0', 0, 2, 0, 100, 23, 0, 300, N'', N'', N'', 1, 0, 0, 999, 0, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND1Rpt_Emps', N'ND1Rpt', N'Emps', N'参与者', N'', 0, 1, 0, 100, 23, 0, 400, N'', N'', N'', 0, 0, 0, 999, 101, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND1Rpt_FID', N'ND1Rpt', N'FID', N'FID', N'0', 0, 2, 0, 100, 23, 0, 300, N'', N'', N'', 0, 0, 0, 999, 101, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND1Rpt_FK_Dept', N'ND1Rpt', N'FK_Dept', N'操作员部门', N'', 1, 1, 2, 100, 23, 0, 100, N'BP.Port.Depts', N'', N'', 1, 0, 0, 999, 103, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND1Rpt_FK_NY', N'ND1Rpt', N'FK_NY', N'年月', N'', 1, 1, 2, 100, 23, 0, 7, N'BP.Pub.NYs', N'', N'', 1, 0, 0, 999, 103, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND1Rpt_FlowDaySpan', N'ND1Rpt', N'FlowDaySpan', N'跨度(天)', N'', 0, 8, 0, 100, 23, 0, 300, N'', N'', N'', 1, 1, 0, 999, 103, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND1Rpt_FlowEmps', N'ND1Rpt', N'FlowEmps', N'参与人', N'', 0, 1, 0, 100, 23, 0, 1000, N'', N'', N'', 1, 0, 1, 999, 103, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND1Rpt_FlowEnder', N'ND1Rpt', N'FlowEnder', N'结束人', N'', 1, 1, 2, 100, 23, 0, 20, N'BP.Port.Emps', N'', N'', 1, 0, 0, 999, 103, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND1Rpt_FlowEnderRDT', N'ND1Rpt', N'FlowEnderRDT', N'结束时间', N'', 0, 7, 0, 100, 23, 0, 300, N'', N'', N'', 1, 0, 0, 999, 103, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND1Rpt_FlowEndNode', N'ND1Rpt', N'FlowEndNode', N'结束节点', N'0', 0, 2, 0, 100, 23, 0, 300, N'', N'', N'', 1, 0, 0, 999, 103, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND1Rpt_FlowNote', N'ND1Rpt', N'FlowNote', N'流程信息', N'', 0, 1, 0, 100, 23, 0, 500, N'', N'', N'', 1, 0, 1, 999, 0, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND1Rpt_FlowStarter', N'ND1Rpt', N'FlowStarter', N'发起人', N'', 1, 1, 2, 100, 23, 0, 20, N'BP.Port.Emps', N'', N'', 1, 0, 0, 999, 103, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND1Rpt_FlowStartRDT', N'ND1Rpt', N'FlowStartRDT', N'发起时间', N'', 0, 7, 0, 100, 23, 0, 300, N'', N'', N'', 1, 0, 0, 999, 103, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND1Rpt_GUID', N'ND1Rpt', N'GUID', N'GUID', N'', 0, 1, 0, 100, 23, 0, 32, N'', N'', N'', 0, 0, 0, 999, 0, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND1Rpt_MyNum', N'ND1Rpt', N'MyNum', N'个数', N'1', 0, 2, 0, 100, 23, 0, 300, N'', N'', N'', 0, 0, 0, 999, 103, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND1Rpt_OID', N'ND1Rpt', N'OID', N'WorkID', N'0', 0, 2, 0, 100, 23, 0, 300, N'', N'', N'', 0, 0, 0, 999, 101, 0, 5, 5, N'', N'', 2, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND1Rpt_PEmp', N'ND1Rpt', N'PEmp', N'调起子流程的人员', N'', 0, 1, 0, 100, 23, 0, 32, N'', N'', N'', 1, 0, 1, 999, 0, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND1Rpt_PFlowNo', N'ND1Rpt', N'PFlowNo', N'父流程编号', N'', 0, 1, 0, 100, 23, 0, 3, N'', N'', N'', 1, 0, 1, 999, 103, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND1Rpt_PNodeID', N'ND1Rpt', N'PNodeID', N'父流程启动的节点', N'0', 0, 2, 0, 100, 23, 0, 300, N'', N'', N'', 1, 0, 0, 999, 0, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND1Rpt_PRI', N'ND1Rpt', N'PRI', N'优先级', N'2', 1, 2, 1, 100, 23, 0, 200, N'PRI', N'', N'', 1, 0, 0, 999, 101, 0, 174.76, 56.19, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND1Rpt_PrjName', N'ND1Rpt', N'PrjName', N'项目名称', N'', 0, 1, 0, 100, 23, 0, 100, N'', N'', N'', 1, 0, 0, 999, 0, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND1Rpt_PrjNo', N'ND1Rpt', N'PrjNo', N'项目编号', N'', 0, 1, 0, 100, 23, 0, 100, N'', N'', N'', 1, 0, 0, 999, 0, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND1Rpt_PWorkID', N'ND1Rpt', N'PWorkID', N'父流程WorkID', N'0', 0, 2, 0, 100, 23, 0, 300, N'', N'', N'', 1, 0, 0, 999, 103, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND1Rpt_RDT', N'ND1Rpt', N'RDT', N'接受时间', N'', 0, 7, 0, 100, 23, 0, 300, N'', N'', N'', 0, 0, 0, 999, 101, 0, 5, 5, N'', N'1', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND1Rpt_Rec', N'ND1Rpt', N'Rec', N'发起人', N'', 0, 1, 0, 100, 23, 0, 20, N'', N'', N'', 0, 0, 0, 999, 101, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND1Rpt_Title', N'ND1Rpt', N'Title', N'标题', N'', 0, 1, 0, 251, 23, 0, 200, N'', N'', N'', 0, 0, 1, 999, 101, 0, 174.83, 54.4, N'', N'', 0, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND1Rpt_WFSta', N'ND1Rpt', N'WFSta', N'状态', N'', 1, 2, 1, 100, 23, 0, 1000, N'WFSta', N'', N'', 1, 0, 0, 999, 0, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND1Rpt_WFState', N'ND1Rpt', N'WFState', N'流程状态', N'', 1, 2, 1, 100, 23, 0, 1000, N'WFState', N'', N'', 1, 0, 0, 999, 103, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND201_CDT', N'ND201', N'CDT', N'发起时间', N'@RDT', 0, 7, 0, 100, 23, 0, 300, N'', N'', N'', 0, 0, 0, 999, 105, 0, 5, 5, N'', N'1', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND201_Emps', N'ND201', N'Emps', N'Emps', N'', 0, 1, 0, 100, 23, 0, 400, N'', N'', N'', 0, 0, 0, 999, 105, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND201_FID', N'ND201', N'FID', N'FID', N'0', 0, 2, 0, 100, 23, 0, 300, N'', N'', N'', 0, 0, 0, 999, 105, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND201_FK_Dept', N'ND201', N'FK_Dept', N'操作员部门', N'', 0, 1, 0, 100, 23, 0, 20, N'', N'', N'', 0, 0, 0, 999, 105, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND201_FK_NY', N'ND201', N'FK_NY', N'年月', N'', 0, 1, 0, 100, 23, 0, 7, N'', N'', N'', 0, 0, 0, 999, 105, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND201_MyNum', N'ND201', N'MyNum', N'个数', N'1', 0, 2, 0, 100, 23, 0, 300, N'', N'', N'', 0, 0, 0, 999, 105, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND201_OID', N'ND201', N'OID', N'WorkID', N'0', 0, 2, 0, 100, 23, 0, 300, N'', N'', N'', 0, 0, 0, 999, 105, 0, 5, 5, N'', N'', 2, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND201_PRI', N'ND201', N'PRI', N'优先级', N'2', 1, 2, 1, 100, 23, 0, 200, N'PRI', N'', N'', 1, 1, 0, 999, 105, 0, 174.76, 56.19, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND201_RDT', N'ND201', N'RDT', N'接受时间', N'', 0, 7, 0, 100, 23, 0, 300, N'', N'', N'', 0, 0, 0, 999, 105, 0, 5, 5, N'', N'1', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND201_Rec', N'ND201', N'Rec', N'发起人', N'@WebUser.No', 0, 1, 0, 100, 23, 0, 20, N'', N'', N'', 0, 0, 0, 999, 105, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND201_Title', N'ND201', N'Title', N'标题', N'', 0, 1, 0, 251, 23, 0, 200, N'', N'', N'', 0, 0, 1, 999, 105, 0, 174.83, 54.4, N'', N'', 0, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND202_CDT', N'ND202', N'CDT', N'完成时间', N'@RDT', 0, 7, 0, 100, 23, 0, 300, N'', N'', N'', 0, 0, 0, 999, 0, 0, 5, 5, N'', N'1', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND202_Emps', N'ND202', N'Emps', N'Emps', N'', 0, 1, 0, 100, 23, 0, 400, N'', N'', N'', 0, 0, 0, 999, 0, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND202_FID', N'ND202', N'FID', N'FID', N'0', 0, 2, 0, 100, 23, 0, 300, N'', N'', N'', 0, 0, 0, 999, 0, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND202_FK_Dept', N'ND202', N'FK_Dept', N'操作员部门', N'', 0, 1, 0, 100, 23, 0, 32, N'', N'', N'', 0, 0, 0, 999, 0, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND202_OID', N'ND202', N'OID', N'WorkID', N'0', 0, 2, 0, 100, 23, 0, 300, N'', N'', N'', 0, 0, 0, 999, 0, 0, 5, 5, N'', N'', 2, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND202_RDT', N'ND202', N'RDT', N'接受时间', N'', 0, 7, 0, 100, 23, 0, 300, N'', N'', N'', 0, 0, 0, 999, 0, 0, 5, 5, N'', N'1', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND202_Rec', N'ND202', N'Rec', N'记录人', N'@WebUser.No', 0, 1, 0, 100, 23, 0, 20, N'', N'', N'', 0, 0, 0, 999, 0, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND2MyRpt_FK_Dept', N'ND2MyRpt', N'FK_Dept', N'操作员部门', N'', 1, 1, 2, 100, 23, 0, 100, N'BP.Port.Depts', N'', N'', 1, 0, 0, 999, 0, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND2MyRpt_FK_NY', N'ND2MyRpt', N'FK_NY', N'年月', N'', 1, 1, 2, 100, 23, 0, 7, N'BP.Pub.NYs', N'', N'', 1, 0, 0, 999, 0, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND2MyRpt_FlowDaySpan', N'ND2MyRpt', N'FlowDaySpan', N'跨度(天)', N'', 0, 8, 0, 100, 23, 0, 300, N'', N'', N'', 1, 1, 0, 999, 107, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND2MyRpt_FlowEmps', N'ND2MyRpt', N'FlowEmps', N'参与人', N'', 0, 1, 0, 100, 23, 0, 1000, N'', N'', N'', 1, 0, 1, 999, 107, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND2MyRpt_FlowEnder', N'ND2MyRpt', N'FlowEnder', N'结束人', N'', 1, 1, 2, 100, 23, 0, 20, N'BP.Port.Emps', N'', N'', 1, 0, 0, 999, 107, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND2MyRpt_FlowEnderRDT', N'ND2MyRpt', N'FlowEnderRDT', N'结束时间', N'', 0, 7, 0, 100, 23, 0, 300, N'', N'', N'', 1, 0, 0, 999, 107, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND2MyRpt_FlowEndNode', N'ND2MyRpt', N'FlowEndNode', N'结束节点', N'0', 0, 2, 0, 100, 23, 0, 300, N'', N'', N'', 1, 0, 0, 999, 107, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND2MyRpt_FlowStarter', N'ND2MyRpt', N'FlowStarter', N'发起人', N'', 1, 1, 2, 100, 23, 0, 20, N'BP.Port.Emps', N'', N'', 1, 0, 0, 999, 107, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND2MyRpt_FlowStartRDT', N'ND2MyRpt', N'FlowStartRDT', N'发起时间', N'', 0, 7, 0, 100, 23, 0, 300, N'', N'', N'', 1, 0, 0, 999, 107, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND2MyRpt_OID', N'ND2MyRpt', N'OID', N'WorkID', N'0', 0, 2, 0, 100, 23, 0, 300, N'', N'', N'', 0, 0, 0, 999, 105, 0, 5, 5, N'', N'', 2, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND2MyRpt_Title', N'ND2MyRpt', N'Title', N'标题', N'', 0, 1, 0, 251, 23, 0, 200, N'', N'', N'', 0, 0, 1, 999, 105, 0, 174.83, 54.4, N'', N'', 0, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND2MyRpt_WFSta', N'ND2MyRpt', N'WFSta', N'状态', N'', 1, 2, 1, 100, 23, 0, 1000, N'WFSta', N'', N'', 1, 0, 0, 999, 0, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND2MyRpt_WFState', N'ND2MyRpt', N'WFState', N'流程状态', N'', 1, 2, 1, 100, 23, 0, 1000, N'WFState', N'', N'', 1, 0, 0, 999, 107, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND2Rpt_AtPara', N'ND2Rpt', N'AtPara', N'参数', N'', 0, 1, 0, 100, 23, 0, 4000, N'', N'', N'', 0, 0, 0, 999, 0, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND2Rpt_BillNo', N'ND2Rpt', N'BillNo', N'单据编号', N'', 0, 1, 0, 100, 23, 0, 100, N'', N'', N'', 1, 0, 0, 999, 0, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND2Rpt_CDT', N'ND2Rpt', N'CDT', N'活动时间', N'', 0, 7, 0, 100, 23, 0, 300, N'', N'', N'', 0, 0, 0, 999, 105, 0, 5, 5, N'', N'1', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND2Rpt_CFlowNo', N'ND2Rpt', N'CFlowNo', N'延续流程编号', N'', 0, 1, 0, 100, 23, 0, 3, N'', N'', N'', 1, 0, 1, 999, 0, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND2Rpt_CWorkID', N'ND2Rpt', N'CWorkID', N'延续流程WorkID', N'0', 0, 2, 0, 100, 23, 0, 300, N'', N'', N'', 1, 0, 0, 999, 0, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND2Rpt_Emps', N'ND2Rpt', N'Emps', N'参与者', N'', 0, 1, 0, 100, 23, 0, 400, N'', N'', N'', 0, 0, 0, 999, 105, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND2Rpt_FID', N'ND2Rpt', N'FID', N'FID', N'0', 0, 2, 0, 100, 23, 0, 300, N'', N'', N'', 0, 0, 0, 999, 105, 0, 5, 5, N'', N'', 1, 1, N'')
GO
print 'Processed 100 total records'
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND2Rpt_FK_Dept', N'ND2Rpt', N'FK_Dept', N'操作员部门', N'', 1, 1, 2, 100, 23, 0, 100, N'BP.Port.Depts', N'', N'', 1, 0, 0, 999, 107, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND2Rpt_FK_NY', N'ND2Rpt', N'FK_NY', N'年月', N'', 1, 1, 2, 100, 23, 0, 7, N'BP.Pub.NYs', N'', N'', 1, 0, 0, 999, 107, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND2Rpt_FlowDaySpan', N'ND2Rpt', N'FlowDaySpan', N'跨度(天)', N'', 0, 8, 0, 100, 23, 0, 300, N'', N'', N'', 1, 1, 0, 999, 107, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND2Rpt_FlowEmps', N'ND2Rpt', N'FlowEmps', N'参与人', N'', 0, 1, 0, 100, 23, 0, 1000, N'', N'', N'', 1, 0, 1, 999, 107, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND2Rpt_FlowEnder', N'ND2Rpt', N'FlowEnder', N'结束人', N'', 1, 1, 2, 100, 23, 0, 20, N'BP.Port.Emps', N'', N'', 1, 0, 0, 999, 107, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND2Rpt_FlowEnderRDT', N'ND2Rpt', N'FlowEnderRDT', N'结束时间', N'', 0, 7, 0, 100, 23, 0, 300, N'', N'', N'', 1, 0, 0, 999, 107, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND2Rpt_FlowEndNode', N'ND2Rpt', N'FlowEndNode', N'结束节点', N'0', 0, 2, 0, 100, 23, 0, 300, N'', N'', N'', 1, 0, 0, 999, 107, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND2Rpt_FlowNote', N'ND2Rpt', N'FlowNote', N'流程信息', N'', 0, 1, 0, 100, 23, 0, 500, N'', N'', N'', 1, 0, 1, 999, 0, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND2Rpt_FlowStarter', N'ND2Rpt', N'FlowStarter', N'发起人', N'', 1, 1, 2, 100, 23, 0, 20, N'BP.Port.Emps', N'', N'', 1, 0, 0, 999, 107, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND2Rpt_FlowStartRDT', N'ND2Rpt', N'FlowStartRDT', N'发起时间', N'', 0, 7, 0, 100, 23, 0, 300, N'', N'', N'', 1, 0, 0, 999, 107, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND2Rpt_GUID', N'ND2Rpt', N'GUID', N'GUID', N'', 0, 1, 0, 100, 23, 0, 32, N'', N'', N'', 0, 0, 0, 999, 0, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND2Rpt_MyNum', N'ND2Rpt', N'MyNum', N'个数', N'1', 0, 2, 0, 100, 23, 0, 300, N'', N'', N'', 0, 0, 0, 999, 107, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND2Rpt_OID', N'ND2Rpt', N'OID', N'WorkID', N'0', 0, 2, 0, 100, 23, 0, 300, N'', N'', N'', 0, 0, 0, 999, 105, 0, 5, 5, N'', N'', 2, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND2Rpt_PEmp', N'ND2Rpt', N'PEmp', N'调起子流程的人员', N'', 0, 1, 0, 100, 23, 0, 32, N'', N'', N'', 1, 0, 1, 999, 0, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND2Rpt_PFlowNo', N'ND2Rpt', N'PFlowNo', N'父流程编号', N'', 0, 1, 0, 100, 23, 0, 3, N'', N'', N'', 1, 0, 1, 999, 107, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND2Rpt_PNodeID', N'ND2Rpt', N'PNodeID', N'父流程启动的节点', N'0', 0, 2, 0, 100, 23, 0, 300, N'', N'', N'', 1, 0, 0, 999, 0, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND2Rpt_PRI', N'ND2Rpt', N'PRI', N'优先级', N'2', 1, 2, 1, 100, 23, 0, 200, N'PRI', N'', N'', 1, 0, 0, 999, 105, 0, 174.76, 56.19, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND2Rpt_PrjName', N'ND2Rpt', N'PrjName', N'项目名称', N'', 0, 1, 0, 100, 23, 0, 100, N'', N'', N'', 1, 0, 0, 999, 0, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND2Rpt_PrjNo', N'ND2Rpt', N'PrjNo', N'项目编号', N'', 0, 1, 0, 100, 23, 0, 100, N'', N'', N'', 1, 0, 0, 999, 0, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND2Rpt_PWorkID', N'ND2Rpt', N'PWorkID', N'父流程WorkID', N'0', 0, 2, 0, 100, 23, 0, 300, N'', N'', N'', 1, 0, 0, 999, 107, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND2Rpt_RDT', N'ND2Rpt', N'RDT', N'接受时间', N'', 0, 7, 0, 100, 23, 0, 300, N'', N'', N'', 0, 0, 0, 999, 105, 0, 5, 5, N'', N'1', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND2Rpt_Rec', N'ND2Rpt', N'Rec', N'发起人', N'', 0, 1, 0, 100, 23, 0, 20, N'', N'', N'', 0, 0, 0, 999, 105, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND2Rpt_Title', N'ND2Rpt', N'Title', N'标题', N'', 0, 1, 0, 251, 23, 0, 200, N'', N'', N'', 0, 0, 1, 999, 105, 0, 174.83, 54.4, N'', N'', 0, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND2Rpt_WFSta', N'ND2Rpt', N'WFSta', N'状态', N'', 1, 2, 1, 100, 23, 0, 1000, N'WFSta', N'', N'', 1, 0, 0, 999, 0, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND2Rpt_WFState', N'ND2Rpt', N'WFState', N'流程状态', N'', 1, 2, 1, 100, 23, 0, 1000, N'WFState', N'', N'', 1, 0, 0, 999, 107, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND301_CDT', N'ND301', N'CDT', N'发起时间', N'@RDT', 0, 7, 0, 100, 23, 0, 300, N'', N'', N'', 0, 0, 0, 999, 109, 0, 5, 5, N'', N'1', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND301_Emps', N'ND301', N'Emps', N'Emps', N'', 0, 1, 0, 100, 23, 0, 400, N'', N'', N'', 0, 0, 0, 999, 109, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND301_FID', N'ND301', N'FID', N'FID', N'0', 0, 2, 0, 100, 23, 0, 300, N'', N'', N'', 0, 0, 0, 999, 109, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND301_FK_Dept', N'ND301', N'FK_Dept', N'操作员部门', N'', 0, 1, 0, 100, 23, 0, 20, N'', N'', N'', 0, 0, 0, 999, 109, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND301_FK_NY', N'ND301', N'FK_NY', N'年月', N'', 0, 1, 0, 100, 23, 0, 7, N'', N'', N'', 0, 0, 0, 999, 109, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND301_MyNum', N'ND301', N'MyNum', N'个数', N'1', 0, 2, 0, 100, 23, 0, 300, N'', N'', N'', 0, 0, 0, 999, 109, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND301_OID', N'ND301', N'OID', N'WorkID', N'0', 0, 2, 0, 100, 23, 0, 300, N'', N'', N'', 0, 0, 0, 999, 109, 0, 5, 5, N'', N'', 2, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND301_PRI', N'ND301', N'PRI', N'优先级', N'2', 1, 2, 1, 100, 23, 0, 200, N'PRI', N'', N'', 1, 1, 0, 999, 109, 0, 174.76, 56.19, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND301_RDT', N'ND301', N'RDT', N'接受时间', N'', 0, 7, 0, 100, 23, 0, 300, N'', N'', N'', 0, 0, 0, 999, 109, 0, 5, 5, N'', N'1', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND301_Rec', N'ND301', N'Rec', N'发起人', N'@WebUser.No', 0, 1, 0, 100, 23, 0, 20, N'', N'', N'', 0, 0, 0, 999, 109, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND301_Title', N'ND301', N'Title', N'标题', N'', 0, 1, 0, 251, 23, 0, 200, N'', N'', N'', 0, 0, 1, 999, 109, 0, 174.83, 54.4, N'', N'', 0, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND302_CDT', N'ND302', N'CDT', N'完成时间', N'@RDT', 0, 7, 0, 100, 23, 0, 300, N'', N'', N'', 0, 0, 0, 999, 0, 0, 5, 5, N'', N'1', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND302_Emps', N'ND302', N'Emps', N'Emps', N'', 0, 1, 0, 100, 23, 0, 400, N'', N'', N'', 0, 0, 0, 999, 0, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND302_FID', N'ND302', N'FID', N'FID', N'0', 0, 2, 0, 100, 23, 0, 300, N'', N'', N'', 0, 0, 0, 999, 0, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND302_FK_Dept', N'ND302', N'FK_Dept', N'操作员部门', N'', 0, 1, 0, 100, 23, 0, 32, N'', N'', N'', 0, 0, 0, 999, 0, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND302_OID', N'ND302', N'OID', N'WorkID', N'0', 0, 2, 0, 100, 23, 0, 300, N'', N'', N'', 0, 0, 0, 999, 0, 0, 5, 5, N'', N'', 2, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND302_RDT', N'ND302', N'RDT', N'接受时间', N'', 0, 7, 0, 100, 23, 0, 300, N'', N'', N'', 0, 0, 0, 999, 0, 0, 5, 5, N'', N'1', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND302_Rec', N'ND302', N'Rec', N'记录人', N'@WebUser.No', 0, 1, 0, 100, 23, 0, 20, N'', N'', N'', 0, 0, 0, 999, 0, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND3MyRpt_FK_Dept', N'ND3MyRpt', N'FK_Dept', N'操作员部门', N'', 1, 1, 2, 100, 23, 0, 100, N'BP.Port.Depts', N'', N'', 1, 0, 0, 999, 0, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND3MyRpt_FK_NY', N'ND3MyRpt', N'FK_NY', N'年月', N'', 1, 1, 2, 100, 23, 0, 7, N'BP.Pub.NYs', N'', N'', 1, 0, 0, 999, 0, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND3MyRpt_FlowDaySpan', N'ND3MyRpt', N'FlowDaySpan', N'跨度(天)', N'', 0, 8, 0, 100, 23, 0, 300, N'', N'', N'', 1, 1, 0, 999, 111, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND3MyRpt_FlowEmps', N'ND3MyRpt', N'FlowEmps', N'参与人', N'', 0, 1, 0, 100, 23, 0, 1000, N'', N'', N'', 1, 0, 1, 999, 111, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND3MyRpt_FlowEnder', N'ND3MyRpt', N'FlowEnder', N'结束人', N'', 1, 1, 2, 100, 23, 0, 20, N'BP.Port.Emps', N'', N'', 1, 0, 0, 999, 111, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND3MyRpt_FlowEnderRDT', N'ND3MyRpt', N'FlowEnderRDT', N'结束时间', N'', 0, 7, 0, 100, 23, 0, 300, N'', N'', N'', 1, 0, 0, 999, 111, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND3MyRpt_FlowEndNode', N'ND3MyRpt', N'FlowEndNode', N'结束节点', N'0', 0, 2, 0, 100, 23, 0, 300, N'', N'', N'', 1, 0, 0, 999, 111, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND3MyRpt_FlowStarter', N'ND3MyRpt', N'FlowStarter', N'发起人', N'', 1, 1, 2, 100, 23, 0, 20, N'BP.Port.Emps', N'', N'', 1, 0, 0, 999, 111, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND3MyRpt_FlowStartRDT', N'ND3MyRpt', N'FlowStartRDT', N'发起时间', N'', 0, 7, 0, 100, 23, 0, 300, N'', N'', N'', 1, 0, 0, 999, 111, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND3MyRpt_OID', N'ND3MyRpt', N'OID', N'WorkID', N'0', 0, 2, 0, 100, 23, 0, 300, N'', N'', N'', 0, 0, 0, 999, 109, 0, 5, 5, N'', N'', 2, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND3MyRpt_Title', N'ND3MyRpt', N'Title', N'标题', N'', 0, 1, 0, 251, 23, 0, 200, N'', N'', N'', 0, 0, 1, 999, 109, 0, 174.83, 54.4, N'', N'', 0, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND3MyRpt_WFSta', N'ND3MyRpt', N'WFSta', N'状态', N'', 1, 2, 1, 100, 23, 0, 1000, N'WFSta', N'', N'', 1, 0, 0, 999, 0, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND3MyRpt_WFState', N'ND3MyRpt', N'WFState', N'流程状态', N'', 1, 2, 1, 100, 23, 0, 1000, N'WFState', N'', N'', 1, 0, 0, 999, 111, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND3Rpt_AtPara', N'ND3Rpt', N'AtPara', N'参数', N'', 0, 1, 0, 100, 23, 0, 4000, N'', N'', N'', 0, 0, 0, 999, 0, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND3Rpt_BillNo', N'ND3Rpt', N'BillNo', N'单据编号', N'', 0, 1, 0, 100, 23, 0, 100, N'', N'', N'', 1, 0, 0, 999, 0, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND3Rpt_CDT', N'ND3Rpt', N'CDT', N'活动时间', N'', 0, 7, 0, 100, 23, 0, 300, N'', N'', N'', 0, 0, 0, 999, 109, 0, 5, 5, N'', N'1', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND3Rpt_CFlowNo', N'ND3Rpt', N'CFlowNo', N'延续流程编号', N'', 0, 1, 0, 100, 23, 0, 3, N'', N'', N'', 1, 0, 1, 999, 0, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND3Rpt_CWorkID', N'ND3Rpt', N'CWorkID', N'延续流程WorkID', N'0', 0, 2, 0, 100, 23, 0, 300, N'', N'', N'', 1, 0, 0, 999, 0, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND3Rpt_Emps', N'ND3Rpt', N'Emps', N'参与者', N'', 0, 1, 0, 100, 23, 0, 400, N'', N'', N'', 0, 0, 0, 999, 109, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND3Rpt_FID', N'ND3Rpt', N'FID', N'FID', N'0', 0, 2, 0, 100, 23, 0, 300, N'', N'', N'', 0, 0, 0, 999, 109, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND3Rpt_FK_Dept', N'ND3Rpt', N'FK_Dept', N'操作员部门', N'', 1, 1, 2, 100, 23, 0, 100, N'BP.Port.Depts', N'', N'', 1, 0, 0, 999, 111, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND3Rpt_FK_NY', N'ND3Rpt', N'FK_NY', N'年月', N'', 1, 1, 2, 100, 23, 0, 7, N'BP.Pub.NYs', N'', N'', 1, 0, 0, 999, 111, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND3Rpt_FlowDaySpan', N'ND3Rpt', N'FlowDaySpan', N'跨度(天)', N'', 0, 8, 0, 100, 23, 0, 300, N'', N'', N'', 1, 1, 0, 999, 111, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND3Rpt_FlowEmps', N'ND3Rpt', N'FlowEmps', N'参与人', N'', 0, 1, 0, 100, 23, 0, 1000, N'', N'', N'', 1, 0, 1, 999, 111, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND3Rpt_FlowEnder', N'ND3Rpt', N'FlowEnder', N'结束人', N'', 1, 1, 2, 100, 23, 0, 20, N'BP.Port.Emps', N'', N'', 1, 0, 0, 999, 111, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND3Rpt_FlowEnderRDT', N'ND3Rpt', N'FlowEnderRDT', N'结束时间', N'', 0, 7, 0, 100, 23, 0, 300, N'', N'', N'', 1, 0, 0, 999, 111, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND3Rpt_FlowEndNode', N'ND3Rpt', N'FlowEndNode', N'结束节点', N'0', 0, 2, 0, 100, 23, 0, 300, N'', N'', N'', 1, 0, 0, 999, 111, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND3Rpt_FlowNote', N'ND3Rpt', N'FlowNote', N'流程信息', N'', 0, 1, 0, 100, 23, 0, 500, N'', N'', N'', 1, 0, 1, 999, 0, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND3Rpt_FlowStarter', N'ND3Rpt', N'FlowStarter', N'发起人', N'', 1, 1, 2, 100, 23, 0, 20, N'BP.Port.Emps', N'', N'', 1, 0, 0, 999, 111, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND3Rpt_FlowStartRDT', N'ND3Rpt', N'FlowStartRDT', N'发起时间', N'', 0, 7, 0, 100, 23, 0, 300, N'', N'', N'', 1, 0, 0, 999, 111, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND3Rpt_GUID', N'ND3Rpt', N'GUID', N'GUID', N'', 0, 1, 0, 100, 23, 0, 32, N'', N'', N'', 0, 0, 0, 999, 0, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND3Rpt_MyNum', N'ND3Rpt', N'MyNum', N'个数', N'1', 0, 2, 0, 100, 23, 0, 300, N'', N'', N'', 0, 0, 0, 999, 111, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND3Rpt_OID', N'ND3Rpt', N'OID', N'WorkID', N'0', 0, 2, 0, 100, 23, 0, 300, N'', N'', N'', 0, 0, 0, 999, 109, 0, 5, 5, N'', N'', 2, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND3Rpt_PEmp', N'ND3Rpt', N'PEmp', N'调起子流程的人员', N'', 0, 1, 0, 100, 23, 0, 32, N'', N'', N'', 1, 0, 1, 999, 0, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND3Rpt_PFlowNo', N'ND3Rpt', N'PFlowNo', N'父流程编号', N'', 0, 1, 0, 100, 23, 0, 3, N'', N'', N'', 1, 0, 1, 999, 111, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND3Rpt_PNodeID', N'ND3Rpt', N'PNodeID', N'父流程启动的节点', N'0', 0, 2, 0, 100, 23, 0, 300, N'', N'', N'', 1, 0, 0, 999, 0, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND3Rpt_PRI', N'ND3Rpt', N'PRI', N'优先级', N'2', 1, 2, 1, 100, 23, 0, 200, N'PRI', N'', N'', 1, 0, 0, 999, 109, 0, 174.76, 56.19, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND3Rpt_PrjName', N'ND3Rpt', N'PrjName', N'项目名称', N'', 0, 1, 0, 100, 23, 0, 100, N'', N'', N'', 1, 0, 0, 999, 0, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND3Rpt_PrjNo', N'ND3Rpt', N'PrjNo', N'项目编号', N'', 0, 1, 0, 100, 23, 0, 100, N'', N'', N'', 1, 0, 0, 999, 0, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND3Rpt_PWorkID', N'ND3Rpt', N'PWorkID', N'父流程WorkID', N'0', 0, 2, 0, 100, 23, 0, 300, N'', N'', N'', 1, 0, 0, 999, 111, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND3Rpt_RDT', N'ND3Rpt', N'RDT', N'接受时间', N'', 0, 7, 0, 100, 23, 0, 300, N'', N'', N'', 0, 0, 0, 999, 109, 0, 5, 5, N'', N'1', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND3Rpt_Rec', N'ND3Rpt', N'Rec', N'发起人', N'', 0, 1, 0, 100, 23, 0, 20, N'', N'', N'', 0, 0, 0, 999, 109, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND3Rpt_Title', N'ND3Rpt', N'Title', N'标题', N'', 0, 1, 0, 251, 23, 0, 200, N'', N'', N'', 0, 0, 1, 999, 109, 0, 174.83, 54.4, N'', N'', 0, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND3Rpt_WFSta', N'ND3Rpt', N'WFSta', N'状态', N'', 1, 2, 1, 100, 23, 0, 1000, N'WFSta', N'', N'', 1, 0, 0, 999, 0, 0, 5, 5, N'', N'', 1, 1, N'')
INSERT [dbo].[Sys_MapAttr] ([MyPK], [FK_MapData], [KeyOfEn], [Name], [DefVal], [UIContralType], [MyDataType], [LGType], [UIWidth], [UIHeight], [MinLen], [MaxLen], [UIBindKey], [UIRefKey], [UIRefKeyText], [UIVisible], [UIIsEnable], [UIIsLine], [Idx], [GroupID], [IsSigan], [X], [Y], [GUID], [Tag], [EditType], [ColSpan], [AtPara]) VALUES (N'ND3Rpt_WFState', N'ND3Rpt', N'WFState', N'流程状态', N'', 1, 2, 1, 100, 23, 0, 1000, N'WFState', N'', N'', 1, 0, 0, 999, 111, 0, 5, 5, N'', N'', 1, 1, N'')
/****** Object:  Table [dbo].[Sys_M2M]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sys_M2M](
	[MyPK] [nvarchar](100) NOT NULL,
	[FK_MapData] [nvarchar](20) NULL,
	[M2MNo] [nvarchar](20) NULL,
	[EnOID] [int] NULL,
	[DtlObj] [nvarchar](20) NULL,
	[Doc] [nvarchar](4000) NULL,
	[ValsName] [nvarchar](4000) NULL,
	[ValsSQL] [nvarchar](4000) NULL,
	[NumSelected] [int] NULL,
	[GUID] [nvarchar](128) NULL,
 CONSTRAINT [Sys_M2Mpk] PRIMARY KEY CLUSTERED 
(
	[MyPK] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Sys_GroupField]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sys_GroupField](
	[OID] [int] NOT NULL,
	[Lab] [nvarchar](500) NULL,
	[EnName] [nvarchar](200) NULL,
	[Idx] [int] NULL,
	[GUID] [nvarchar](128) NULL,
 CONSTRAINT [Sys_GroupFieldpk] PRIMARY KEY CLUSTERED 
(
	[OID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Sys_GroupField] ([OID], [Lab], [EnName], [Idx], [GUID]) VALUES (101, N'开始节点', N'ND101', 1, N'')
INSERT [dbo].[Sys_GroupField] ([OID], [Lab], [EnName], [Idx], [GUID]) VALUES (103, N'流程信息', N'ND1Rpt', 1, N'')
INSERT [dbo].[Sys_GroupField] ([OID], [Lab], [EnName], [Idx], [GUID]) VALUES (105, N'开始节点', N'ND201', 1, N'')
INSERT [dbo].[Sys_GroupField] ([OID], [Lab], [EnName], [Idx], [GUID]) VALUES (107, N'流程信息', N'ND2Rpt', 1, N'')
INSERT [dbo].[Sys_GroupField] ([OID], [Lab], [EnName], [Idx], [GUID]) VALUES (109, N'开始节点', N'ND301', 1, N'')
INSERT [dbo].[Sys_GroupField] ([OID], [Lab], [EnName], [Idx], [GUID]) VALUES (111, N'流程信息', N'ND3Rpt', 1, N'')
/****** Object:  Table [dbo].[Sys_GroupEnsTemplate]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sys_GroupEnsTemplate](
	[OID] [int] NOT NULL,
	[EnName] [nvarchar](500) NULL,
	[Name] [nvarchar](500) NULL,
	[EnsName] [nvarchar](90) NULL,
	[OperateCol] [nvarchar](90) NULL,
	[Attrs] [nvarchar](90) NULL,
	[Rec] [nvarchar](90) NULL,
 CONSTRAINT [Sys_GroupEnsTemplatepk] PRIMARY KEY CLUSTERED 
(
	[OID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Sys_GloVar]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sys_GloVar](
	[No] [nvarchar](30) NOT NULL,
	[Name] [nvarchar](120) NULL,
	[Val] [nvarchar](120) NULL,
	[GroupKey] [nvarchar](120) NULL,
	[Note] [nvarchar](4000) NULL,
 CONSTRAINT [Sys_GloVarpk] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Sys_GloVar] ([No], [Name], [Val], [GroupKey], [Note]) VALUES (N'ColsOfApp', N'SSO界面应用系统列数', N'2', N'APP', N'SSO界面应用系统列数。')
INSERT [dbo].[Sys_GloVar] ([No], [Name], [Val], [GroupKey], [Note]) VALUES (N'ColsOfSSO', N'SSO界面信息列数', N'3', N'SSO', N'SSO界面信息列数,单点登陆界面中的列数。')
INSERT [dbo].[Sys_GloVar] ([No], [Name], [Val], [GroupKey], [Note]) VALUES (N'UnitFullName', N'单位全称', N'济南驰骋信息技术有限公司', N'Glo', N'')
INSERT [dbo].[Sys_GloVar] ([No], [Name], [Val], [GroupKey], [Note]) VALUES (N'UnitSimpleName', N'单位简称', N'驰骋软件', N'Glo', N'')
/****** Object:  Table [dbo].[Sys_FrmSln]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sys_FrmSln](
	[MyPK] [nvarchar](100) NOT NULL,
	[FK_Flow] [nvarchar](4) NULL,
	[FK_Node] [int] NULL,
	[FK_MapData] [nvarchar](100) NULL,
	[KeyOfEn] [nvarchar](200) NULL,
	[Name] [nvarchar](500) NULL,
	[EleType] [nvarchar](20) NULL,
	[UIIsEnable] [int] NULL,
	[UIVisible] [int] NULL,
	[IsSigan] [int] NULL,
	[IsNotNull] [int] NULL,
	[RegularExp] [nvarchar](500) NULL,
	[IsWriteToFlowTable] [int] NULL,
	[DefVal] [nvarchar](200) NULL,
 CONSTRAINT [Sys_FrmSlnpk] PRIMARY KEY CLUSTERED 
(
	[MyPK] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Sys_FrmRpt]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sys_FrmRpt](
	[No] [nvarchar](20) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[FK_MapData] [nvarchar](30) NULL,
	[PTable] [nvarchar](30) NULL,
	[SQLOfColumn] [nvarchar](300) NULL,
	[SQLOfRow] [nvarchar](300) NULL,
	[RowIdx] [int] NULL,
	[GroupID] [int] NULL,
	[IsShowSum] [int] NULL,
	[IsShowIdx] [int] NULL,
	[IsCopyNDData] [int] NULL,
	[IsHLDtl] [int] NULL,
	[IsReadonly] [int] NULL,
	[IsShowTitle] [int] NULL,
	[IsView] [int] NULL,
	[IsExp] [int] NULL,
	[IsImp] [int] NULL,
	[IsInsert] [int] NULL,
	[IsDelete] [int] NULL,
	[IsUpdate] [int] NULL,
	[IsEnablePass] [int] NULL,
	[IsEnableAthM] [int] NULL,
	[IsEnableM2M] [int] NULL,
	[IsEnableM2MM] [int] NULL,
	[WhenOverSize] [int] NULL,
	[DtlOpenType] [int] NULL,
	[DtlShowModel] [int] NULL,
	[X] [float] NULL,
	[Y] [float] NULL,
	[H] [float] NULL,
	[W] [float] NULL,
	[FrmW] [float] NULL,
	[FrmH] [float] NULL,
	[MTR] [nvarchar](3000) NULL,
	[GUID] [nvarchar](128) NULL,
 CONSTRAINT [Sys_FrmRptpk] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Sys_FrmRePortField]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sys_FrmRePortField](
	[MyPK] [nvarchar](100) NOT NULL,
	[FK_MapData] [nvarchar](30) NULL,
	[KeyOfEn] [nvarchar](100) NULL,
	[Name] [nvarchar](200) NULL,
	[UIWidth] [nvarchar](100) NULL,
	[UIVisible] [int] NULL,
	[Idx] [int] NULL,
 CONSTRAINT [Sys_FrmRePortFieldpk] PRIMARY KEY CLUSTERED 
(
	[MyPK] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Sys_FrmRB]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sys_FrmRB](
	[MyPK] [nvarchar](100) NOT NULL,
	[FK_MapData] [nvarchar](30) NULL,
	[KeyOfEn] [nvarchar](30) NULL,
	[EnumKey] [nvarchar](30) NULL,
	[Lab] [nvarchar](90) NULL,
	[IntKey] [int] NULL,
	[X] [float] NULL,
	[Y] [float] NULL,
	[GUID] [nvarchar](128) NULL,
 CONSTRAINT [Sys_FrmRBpk] PRIMARY KEY CLUSTERED 
(
	[MyPK] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Sys_FrmLink]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sys_FrmLink](
	[MyPK] [nvarchar](100) NOT NULL,
	[FK_MapData] [nvarchar](30) NULL,
	[Text] [nvarchar](500) NULL,
	[URL] [nvarchar](500) NULL,
	[Target] [nvarchar](20) NULL,
	[X] [float] NULL,
	[Y] [float] NULL,
	[FontSize] [int] NULL,
	[FontColor] [nvarchar](50) NULL,
	[FontName] [nvarchar](50) NULL,
	[FontStyle] [nvarchar](50) NULL,
	[IsBold] [int] NULL,
	[IsItalic] [int] NULL,
	[GUID] [nvarchar](128) NULL,
 CONSTRAINT [Sys_FrmLinkpk] PRIMARY KEY CLUSTERED 
(
	[MyPK] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Sys_FrmLine]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sys_FrmLine](
	[MyPK] [nvarchar](100) NOT NULL,
	[FK_MapData] [nvarchar](30) NULL,
	[X] [float] NULL,
	[Y] [float] NULL,
	[X1] [float] NULL,
	[Y1] [float] NULL,
	[X2] [float] NULL,
	[Y2] [float] NULL,
	[BorderWidth] [float] NULL,
	[BorderColor] [nvarchar](30) NULL,
	[GUID] [nvarchar](128) NULL,
 CONSTRAINT [Sys_FrmLinepk] PRIMARY KEY CLUSTERED 
(
	[MyPK] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Sys_FrmLine] ([MyPK], [FK_MapData], [X], [Y], [X1], [Y1], [X2], [Y2], [BorderWidth], [BorderColor], [GUID]) VALUES (N'LE_1_ND101', N'ND101', 5, 5, 281.82, 81.82, 281.82, 121.82, 2, N'Black', N'')
INSERT [dbo].[Sys_FrmLine] ([MyPK], [FK_MapData], [X], [Y], [X1], [Y1], [X2], [Y2], [BorderWidth], [BorderColor], [GUID]) VALUES (N'LE_1_ND201', N'ND201', 5, 5, 281.82, 81.82, 281.82, 121.82, 2, N'Black', N'')
INSERT [dbo].[Sys_FrmLine] ([MyPK], [FK_MapData], [X], [Y], [X1], [Y1], [X2], [Y2], [BorderWidth], [BorderColor], [GUID]) VALUES (N'LE_1_ND301', N'ND301', 5, 5, 281.82, 81.82, 281.82, 121.82, 2, N'Black', N'')
INSERT [dbo].[Sys_FrmLine] ([MyPK], [FK_MapData], [X], [Y], [X1], [Y1], [X2], [Y2], [BorderWidth], [BorderColor], [GUID]) VALUES (N'LE_2_ND101', N'ND101', 5, 5, 360, 80.91, 360, 120.91, 2, N'Black', N'')
INSERT [dbo].[Sys_FrmLine] ([MyPK], [FK_MapData], [X], [Y], [X1], [Y1], [X2], [Y2], [BorderWidth], [BorderColor], [GUID]) VALUES (N'LE_2_ND201', N'ND201', 5, 5, 360, 80.91, 360, 120.91, 2, N'Black', N'')
INSERT [dbo].[Sys_FrmLine] ([MyPK], [FK_MapData], [X], [Y], [X1], [Y1], [X2], [Y2], [BorderWidth], [BorderColor], [GUID]) VALUES (N'LE_2_ND301', N'ND301', 5, 5, 360, 80.91, 360, 120.91, 2, N'Black', N'')
INSERT [dbo].[Sys_FrmLine] ([MyPK], [FK_MapData], [X], [Y], [X1], [Y1], [X2], [Y2], [BorderWidth], [BorderColor], [GUID]) VALUES (N'LE_3_ND101', N'ND101', 5, 5, 158.82, 41.82, 158.82, 482.73, 2, N'Black', N'')
INSERT [dbo].[Sys_FrmLine] ([MyPK], [FK_MapData], [X], [Y], [X1], [Y1], [X2], [Y2], [BorderWidth], [BorderColor], [GUID]) VALUES (N'LE_3_ND201', N'ND201', 5, 5, 158.82, 41.82, 158.82, 482.73, 2, N'Black', N'')
INSERT [dbo].[Sys_FrmLine] ([MyPK], [FK_MapData], [X], [Y], [X1], [Y1], [X2], [Y2], [BorderWidth], [BorderColor], [GUID]) VALUES (N'LE_3_ND301', N'ND301', 5, 5, 158.82, 41.82, 158.82, 482.73, 2, N'Black', N'')
INSERT [dbo].[Sys_FrmLine] ([MyPK], [FK_MapData], [X], [Y], [X1], [Y1], [X2], [Y2], [BorderWidth], [BorderColor], [GUID]) VALUES (N'LE_4_ND101', N'ND101', 5, 5, 81.55, 80, 718.82, 80, 2, N'Black', N'')
INSERT [dbo].[Sys_FrmLine] ([MyPK], [FK_MapData], [X], [Y], [X1], [Y1], [X2], [Y2], [BorderWidth], [BorderColor], [GUID]) VALUES (N'LE_4_ND201', N'ND201', 5, 5, 81.55, 80, 718.82, 80, 2, N'Black', N'')
INSERT [dbo].[Sys_FrmLine] ([MyPK], [FK_MapData], [X], [Y], [X1], [Y1], [X2], [Y2], [BorderWidth], [BorderColor], [GUID]) VALUES (N'LE_4_ND301', N'ND301', 5, 5, 81.55, 80, 718.82, 80, 2, N'Black', N'')
INSERT [dbo].[Sys_FrmLine] ([MyPK], [FK_MapData], [X], [Y], [X1], [Y1], [X2], [Y2], [BorderWidth], [BorderColor], [GUID]) VALUES (N'LE_5_ND101', N'ND101', 5, 5, 81.82, 40, 81.82, 480.91, 2, N'Black', N'')
INSERT [dbo].[Sys_FrmLine] ([MyPK], [FK_MapData], [X], [Y], [X1], [Y1], [X2], [Y2], [BorderWidth], [BorderColor], [GUID]) VALUES (N'LE_5_ND201', N'ND201', 5, 5, 81.82, 40, 81.82, 480.91, 2, N'Black', N'')
INSERT [dbo].[Sys_FrmLine] ([MyPK], [FK_MapData], [X], [Y], [X1], [Y1], [X2], [Y2], [BorderWidth], [BorderColor], [GUID]) VALUES (N'LE_5_ND301', N'ND301', 5, 5, 81.82, 40, 81.82, 480.91, 2, N'Black', N'')
INSERT [dbo].[Sys_FrmLine] ([MyPK], [FK_MapData], [X], [Y], [X1], [Y1], [X2], [Y2], [BorderWidth], [BorderColor], [GUID]) VALUES (N'LE_6_ND101', N'ND101', 5, 5, 81.82, 481.82, 720, 481.82, 2, N'Black', N'')
INSERT [dbo].[Sys_FrmLine] ([MyPK], [FK_MapData], [X], [Y], [X1], [Y1], [X2], [Y2], [BorderWidth], [BorderColor], [GUID]) VALUES (N'LE_6_ND201', N'ND201', 5, 5, 81.82, 481.82, 720, 481.82, 2, N'Black', N'')
INSERT [dbo].[Sys_FrmLine] ([MyPK], [FK_MapData], [X], [Y], [X1], [Y1], [X2], [Y2], [BorderWidth], [BorderColor], [GUID]) VALUES (N'LE_6_ND301', N'ND301', 5, 5, 81.82, 481.82, 720, 481.82, 2, N'Black', N'')
INSERT [dbo].[Sys_FrmLine] ([MyPK], [FK_MapData], [X], [Y], [X1], [Y1], [X2], [Y2], [BorderWidth], [BorderColor], [GUID]) VALUES (N'LE_7_ND101', N'ND101', 5, 5, 83.36, 40.91, 717.91, 40.91, 2, N'Black', N'')
INSERT [dbo].[Sys_FrmLine] ([MyPK], [FK_MapData], [X], [Y], [X1], [Y1], [X2], [Y2], [BorderWidth], [BorderColor], [GUID]) VALUES (N'LE_7_ND201', N'ND201', 5, 5, 83.36, 40.91, 717.91, 40.91, 2, N'Black', N'')
INSERT [dbo].[Sys_FrmLine] ([MyPK], [FK_MapData], [X], [Y], [X1], [Y1], [X2], [Y2], [BorderWidth], [BorderColor], [GUID]) VALUES (N'LE_7_ND301', N'ND301', 5, 5, 83.36, 40.91, 717.91, 40.91, 2, N'Black', N'')
INSERT [dbo].[Sys_FrmLine] ([MyPK], [FK_MapData], [X], [Y], [X1], [Y1], [X2], [Y2], [BorderWidth], [BorderColor], [GUID]) VALUES (N'LE_8_ND101', N'ND101', 5, 5, 83.36, 120.91, 717.91, 120.91, 2, N'Black', N'')
INSERT [dbo].[Sys_FrmLine] ([MyPK], [FK_MapData], [X], [Y], [X1], [Y1], [X2], [Y2], [BorderWidth], [BorderColor], [GUID]) VALUES (N'LE_8_ND201', N'ND201', 5, 5, 83.36, 120.91, 717.91, 120.91, 2, N'Black', N'')
INSERT [dbo].[Sys_FrmLine] ([MyPK], [FK_MapData], [X], [Y], [X1], [Y1], [X2], [Y2], [BorderWidth], [BorderColor], [GUID]) VALUES (N'LE_8_ND301', N'ND301', 5, 5, 83.36, 120.91, 717.91, 120.91, 2, N'Black', N'')
INSERT [dbo].[Sys_FrmLine] ([MyPK], [FK_MapData], [X], [Y], [X1], [Y1], [X2], [Y2], [BorderWidth], [BorderColor], [GUID]) VALUES (N'LE_9_ND101', N'ND101', 5, 5, 719.09, 40, 719.09, 482.73, 2, N'Black', N'')
INSERT [dbo].[Sys_FrmLine] ([MyPK], [FK_MapData], [X], [Y], [X1], [Y1], [X2], [Y2], [BorderWidth], [BorderColor], [GUID]) VALUES (N'LE_9_ND201', N'ND201', 5, 5, 719.09, 40, 719.09, 482.73, 2, N'Black', N'')
INSERT [dbo].[Sys_FrmLine] ([MyPK], [FK_MapData], [X], [Y], [X1], [Y1], [X2], [Y2], [BorderWidth], [BorderColor], [GUID]) VALUES (N'LE_9_ND301', N'ND301', 5, 5, 719.09, 40, 719.09, 482.73, 2, N'Black', N'')
/****** Object:  Table [dbo].[Sys_FrmLab]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sys_FrmLab](
	[MyPK] [nvarchar](100) NOT NULL,
	[FK_MapData] [nvarchar](30) NULL,
	[Text] [nvarchar](3900) NULL,
	[X] [float] NULL,
	[Y] [float] NULL,
	[FontSize] [int] NULL,
	[FontColor] [nvarchar](50) NULL,
	[FontName] [nvarchar](50) NULL,
	[FontStyle] [nvarchar](50) NULL,
	[FontWeight] [nvarchar](50) NULL,
	[IsBold] [int] NULL,
	[IsItalic] [int] NULL,
	[GUID] [nvarchar](128) NULL,
 CONSTRAINT [Sys_FrmLabpk] PRIMARY KEY CLUSTERED 
(
	[MyPK] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Sys_FrmLab] ([MyPK], [FK_MapData], [Text], [X], [Y], [FontSize], [FontColor], [FontName], [FontStyle], [FontWeight], [IsBold], [IsItalic], [GUID]) VALUES (N'LB_1_ND101', N'ND101', N'优先级', 109.05, 58.1, 11, N'black', N'Portable User Interface', N'Normal', N'normal', 0, 0, N'')
INSERT [dbo].[Sys_FrmLab] ([MyPK], [FK_MapData], [Text], [X], [Y], [FontSize], [FontColor], [FontName], [FontStyle], [FontWeight], [IsBold], [IsItalic], [GUID]) VALUES (N'LB_1_ND201', N'ND201', N'优先级', 109.05, 58.1, 11, N'black', N'Portable User Interface', N'Normal', N'normal', 0, 0, N'')
INSERT [dbo].[Sys_FrmLab] ([MyPK], [FK_MapData], [Text], [X], [Y], [FontSize], [FontColor], [FontName], [FontStyle], [FontWeight], [IsBold], [IsItalic], [GUID]) VALUES (N'LB_1_ND301', N'ND301', N'优先级', 109.05, 58.1, 11, N'black', N'Portable User Interface', N'Normal', N'normal', 0, 0, N'')
INSERT [dbo].[Sys_FrmLab] ([MyPK], [FK_MapData], [Text], [X], [Y], [FontSize], [FontColor], [FontName], [FontStyle], [FontWeight], [IsBold], [IsItalic], [GUID]) VALUES (N'LB_2_ND101', N'ND101', N'发起人', 106.48, 96.08, 11, N'black', N'Portable User Interface', N'Normal', N'normal', 0, 0, N'')
INSERT [dbo].[Sys_FrmLab] ([MyPK], [FK_MapData], [Text], [X], [Y], [FontSize], [FontColor], [FontName], [FontStyle], [FontWeight], [IsBold], [IsItalic], [GUID]) VALUES (N'LB_2_ND201', N'ND201', N'发起人', 106.48, 96.08, 11, N'black', N'Portable User Interface', N'Normal', N'normal', 0, 0, N'')
INSERT [dbo].[Sys_FrmLab] ([MyPK], [FK_MapData], [Text], [X], [Y], [FontSize], [FontColor], [FontName], [FontStyle], [FontWeight], [IsBold], [IsItalic], [GUID]) VALUES (N'LB_2_ND301', N'ND301', N'发起人', 106.48, 96.08, 11, N'black', N'Portable User Interface', N'Normal', N'normal', 0, 0, N'')
INSERT [dbo].[Sys_FrmLab] ([MyPK], [FK_MapData], [Text], [X], [Y], [FontSize], [FontColor], [FontName], [FontStyle], [FontWeight], [IsBold], [IsItalic], [GUID]) VALUES (N'LB_3_ND101', N'ND101', N'发起时间', 307.64, 95.17, 11, N'black', N'Portable User Interface', N'Normal', N'normal', 0, 0, N'')
INSERT [dbo].[Sys_FrmLab] ([MyPK], [FK_MapData], [Text], [X], [Y], [FontSize], [FontColor], [FontName], [FontStyle], [FontWeight], [IsBold], [IsItalic], [GUID]) VALUES (N'LB_3_ND201', N'ND201', N'发起时间', 307.64, 95.17, 11, N'black', N'Portable User Interface', N'Normal', N'normal', 0, 0, N'')
INSERT [dbo].[Sys_FrmLab] ([MyPK], [FK_MapData], [Text], [X], [Y], [FontSize], [FontColor], [FontName], [FontStyle], [FontWeight], [IsBold], [IsItalic], [GUID]) VALUES (N'LB_3_ND301', N'ND301', N'发起时间', 307.64, 95.17, 11, N'black', N'Portable User Interface', N'Normal', N'normal', 0, 0, N'')
INSERT [dbo].[Sys_FrmLab] ([MyPK], [FK_MapData], [Text], [X], [Y], [FontSize], [FontColor], [FontName], [FontStyle], [FontWeight], [IsBold], [IsItalic], [GUID]) VALUES (N'LB_4_ND101', N'ND101', N'新建节点(请修改标题)', 294.67, 8.27, 23, N'Blue', N'Portable User Interface', N'Normal', N'normal', 0, 0, N'')
INSERT [dbo].[Sys_FrmLab] ([MyPK], [FK_MapData], [Text], [X], [Y], [FontSize], [FontColor], [FontName], [FontStyle], [FontWeight], [IsBold], [IsItalic], [GUID]) VALUES (N'LB_4_ND201', N'ND201', N'新建节点(请修改标题)', 294.67, 8.27, 23, N'Blue', N'Portable User Interface', N'Normal', N'normal', 0, 0, N'')
INSERT [dbo].[Sys_FrmLab] ([MyPK], [FK_MapData], [Text], [X], [Y], [FontSize], [FontColor], [FontName], [FontStyle], [FontWeight], [IsBold], [IsItalic], [GUID]) VALUES (N'LB_4_ND301', N'ND301', N'新建节点(请修改标题)', 294.67, 8.27, 23, N'Blue', N'Portable User Interface', N'Normal', N'normal', 0, 0, N'')
INSERT [dbo].[Sys_FrmLab] ([MyPK], [FK_MapData], [Text], [X], [Y], [FontSize], [FontColor], [FontName], [FontStyle], [FontWeight], [IsBold], [IsItalic], [GUID]) VALUES (N'LB_5_ND101', N'ND101', N'说明:以上内容是ccflow自动产生的，您可以修改/删除它。@为了更方便您的设计您可以到http://ccflow.org官网下载表单模板.@因为当前技术问题与silverlight开发工具使用特别说明如下:@@1,改变控件位置: @  所有的控件都支持 wasd, 做为方向键用来移动控件的位置， 部分控件支持方向键. @2, 增加textbox, 从表, dropdownlistbox, 的宽度 shift+ -> 方向键增加宽度 shift + <- 减小宽度.@3, 保存 windows键 + s.  删除 delete.  复制 ctrl+c   粘帖: ctrl+v.@4, 支持全选，批量移动， 批量放大缩小字体., 批量改变线的宽度.@5, 改变线的长度： 选择线，点绿色的圆点，拖拉它。.@6, 放大或者缩小　label 的字体 , 选择一个多个label , 按 A+ 或者　A－　按钮.@7, 改变线或者标签的颜色， 选择操作对象，点工具栏上的调色板.', 168.24, 163.7, 11, N'Red', N'Portable User Interface', N'Normal', N'normal', 0, 0, N'')
INSERT [dbo].[Sys_FrmLab] ([MyPK], [FK_MapData], [Text], [X], [Y], [FontSize], [FontColor], [FontName], [FontStyle], [FontWeight], [IsBold], [IsItalic], [GUID]) VALUES (N'LB_5_ND201', N'ND201', N'说明:以上内容是ccflow自动产生的，您可以修改/删除它。@为了更方便您的设计您可以到http://ccflow.org官网下载表单模板.@因为当前技术问题与silverlight开发工具使用特别说明如下:@@1,改变控件位置: @  所有的控件都支持 wasd, 做为方向键用来移动控件的位置， 部分控件支持方向键. @2, 增加textbox, 从表, dropdownlistbox, 的宽度 shift+ -> 方向键增加宽度 shift + <- 减小宽度.@3, 保存 windows键 + s.  删除 delete.  复制 ctrl+c   粘帖: ctrl+v.@4, 支持全选，批量移动， 批量放大缩小字体., 批量改变线的宽度.@5, 改变线的长度： 选择线，点绿色的圆点，拖拉它。.@6, 放大或者缩小　label 的字体 , 选择一个多个label , 按 A+ 或者　A－　按钮.@7, 改变线或者标签的颜色， 选择操作对象，点工具栏上的调色板.', 168.24, 163.7, 11, N'Red', N'Portable User Interface', N'Normal', N'normal', 0, 0, N'')
INSERT [dbo].[Sys_FrmLab] ([MyPK], [FK_MapData], [Text], [X], [Y], [FontSize], [FontColor], [FontName], [FontStyle], [FontWeight], [IsBold], [IsItalic], [GUID]) VALUES (N'LB_5_ND301', N'ND301', N'说明:以上内容是ccflow自动产生的，您可以修改/删除它。@为了更方便您的设计您可以到http://ccflow.org官网下载表单模板.@因为当前技术问题与silverlight开发工具使用特别说明如下:@@1,改变控件位置: @  所有的控件都支持 wasd, 做为方向键用来移动控件的位置， 部分控件支持方向键. @2, 增加textbox, 从表, dropdownlistbox, 的宽度 shift+ -> 方向键增加宽度 shift + <- 减小宽度.@3, 保存 windows键 + s.  删除 delete.  复制 ctrl+c   粘帖: ctrl+v.@4, 支持全选，批量移动， 批量放大缩小字体., 批量改变线的宽度.@5, 改变线的长度： 选择线，点绿色的圆点，拖拉它。.@6, 放大或者缩小　label 的字体 , 选择一个多个label , 按 A+ 或者　A－　按钮.@7, 改变线或者标签的颜色， 选择操作对象，点工具栏上的调色板.', 168.24, 163.7, 11, N'Red', N'Portable User Interface', N'Normal', N'normal', 0, 0, N'')
/****** Object:  Table [dbo].[Sys_FrmImgAthDB]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sys_FrmImgAthDB](
	[MyPK] [nvarchar](100) NOT NULL,
	[FK_MapData] [nvarchar](30) NULL,
	[FK_FrmImgAth] [nvarchar](50) NULL,
	[RefPKVal] [nvarchar](50) NULL,
	[FileFullName] [nvarchar](700) NULL,
	[FileName] [nvarchar](500) NULL,
	[FileExts] [nvarchar](50) NULL,
	[FileSize] [float] NULL,
	[RDT] [nvarchar](50) NULL,
	[Rec] [nvarchar](50) NULL,
	[RecName] [nvarchar](50) NULL,
	[MyNote] [nvarchar](4000) NULL,
 CONSTRAINT [Sys_FrmImgAthDBpk] PRIMARY KEY CLUSTERED 
(
	[MyPK] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Sys_FrmImgAth]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sys_FrmImgAth](
	[MyPK] [nvarchar](100) NOT NULL,
	[FK_MapData] [nvarchar](30) NULL,
	[CtrlID] [nvarchar](200) NULL,
	[X] [float] NULL,
	[Y] [float] NULL,
	[H] [float] NULL,
	[W] [float] NULL,
	[IsEdit] [int] NULL,
	[GUID] [nvarchar](128) NULL,
 CONSTRAINT [Sys_FrmImgAthpk] PRIMARY KEY CLUSTERED 
(
	[MyPK] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Sys_FrmImg]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sys_FrmImg](
	[MyPK] [nvarchar](100) NOT NULL,
	[FK_MapData] [nvarchar](30) NULL,
	[ImgAppType] [int] NULL,
	[X] [float] NULL,
	[Y] [float] NULL,
	[H] [float] NULL,
	[W] [float] NULL,
	[ImgURL] [nvarchar](200) NULL,
	[ImgPath] [nvarchar](200) NULL,
	[LinkURL] [nvarchar](200) NULL,
	[LinkTarget] [nvarchar](200) NULL,
	[GUID] [nvarchar](128) NULL,
	[Tag0] [nvarchar](500) NULL,
	[SrcType] [int] NULL,
	[IsEdit] [int] NULL,
	[Name] [nvarchar](500) NULL,
	[EnPK] [nvarchar](500) NULL,
 CONSTRAINT [Sys_FrmImgpk] PRIMARY KEY CLUSTERED 
(
	[MyPK] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Sys_FrmImg] ([MyPK], [FK_MapData], [ImgAppType], [X], [Y], [H], [W], [ImgURL], [ImgPath], [LinkURL], [LinkTarget], [GUID], [Tag0], [SrcType], [IsEdit], [Name], [EnPK]) VALUES (N'Img_1_ND101', N'ND101', 0, 577.26, 3.45, 40, 137, N'/ccform;component/Img/LogoBig.png', N'', N'http://ccflow.org', N'_blank', N'', N'', 0, 0, N'', N'')
INSERT [dbo].[Sys_FrmImg] ([MyPK], [FK_MapData], [ImgAppType], [X], [Y], [H], [W], [ImgURL], [ImgPath], [LinkURL], [LinkTarget], [GUID], [Tag0], [SrcType], [IsEdit], [Name], [EnPK]) VALUES (N'Img_1_ND201', N'ND201', 0, 577.26, 3.45, 40, 137, N'/ccform;component/Img/LogoBig.png', N'', N'http://ccflow.org', N'_blank', N'', N'', 0, 0, N'', N'')
INSERT [dbo].[Sys_FrmImg] ([MyPK], [FK_MapData], [ImgAppType], [X], [Y], [H], [W], [ImgURL], [ImgPath], [LinkURL], [LinkTarget], [GUID], [Tag0], [SrcType], [IsEdit], [Name], [EnPK]) VALUES (N'Img_1_ND301', N'ND301', 0, 577.26, 3.45, 40, 137, N'/ccform;component/Img/LogoBig.png', N'', N'http://ccflow.org', N'_blank', N'', N'', 0, 0, N'', N'')
/****** Object:  Table [dbo].[Sys_FrmEvent]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sys_FrmEvent](
	[MyPK] [nvarchar](100) NOT NULL,
	[FK_Event] [nvarchar](400) NULL,
	[FK_MapData] [nvarchar](400) NULL,
	[DoType] [int] NULL,
	[DoDoc] [nvarchar](400) NULL,
	[MsgOK] [nvarchar](400) NULL,
	[MsgError] [nvarchar](400) NULL,
	[MsgCtrl] [int] NULL,
	[MsgMailEnable] [int] NULL,
	[MailTitle] [nvarchar](200) NULL,
	[MailDoc] [nvarchar](4000) NULL,
	[SMSEnable] [int] NULL,
	[SMSDoc] [nvarchar](4000) NULL,
	[MobilePushEnable] [int] NULL,
 CONSTRAINT [Sys_FrmEventpk] PRIMARY KEY CLUSTERED 
(
	[MyPK] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Sys_FrmEleDB]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sys_FrmEleDB](
	[MyPK] [nvarchar](100) NOT NULL,
	[FK_MapData] [nvarchar](30) NULL,
	[EleID] [nvarchar](50) NULL,
	[RefPKVal] [nvarchar](50) NULL,
	[Tag1] [nvarchar](4000) NULL,
	[Tag2] [nvarchar](4000) NULL,
	[Tag3] [nvarchar](4000) NULL,
	[Tag4] [nvarchar](4000) NULL,
 CONSTRAINT [Sys_FrmEleDBpk] PRIMARY KEY CLUSTERED 
(
	[MyPK] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Sys_FrmEle]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sys_FrmEle](
	[MyPK] [nvarchar](100) NOT NULL,
	[FK_MapData] [nvarchar](30) NULL,
	[EleType] [nvarchar](50) NULL,
	[EleID] [nvarchar](50) NULL,
	[EleName] [nvarchar](200) NULL,
	[X] [float] NULL,
	[Y] [float] NULL,
	[H] [float] NULL,
	[W] [float] NULL,
	[IsEnable] [int] NULL,
	[Tag1] [nvarchar](50) NULL,
	[Tag2] [nvarchar](50) NULL,
	[Tag3] [nvarchar](50) NULL,
	[Tag4] [nvarchar](50) NULL,
	[GUID] [nvarchar](128) NULL,
 CONSTRAINT [Sys_FrmElepk] PRIMARY KEY CLUSTERED 
(
	[MyPK] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Sys_FrmBtn]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sys_FrmBtn](
	[MyPK] [nvarchar](100) NOT NULL,
	[FK_MapData] [nvarchar](30) NULL,
	[Text] [nvarchar](3900) NULL,
	[X] [float] NULL,
	[Y] [float] NULL,
	[IsView] [int] NULL,
	[IsEnable] [int] NULL,
	[BtnType] [int] NULL,
	[UAC] [int] NULL,
	[UACContext] [nvarchar](3900) NULL,
	[EventType] [int] NULL,
	[EventContext] [nvarchar](3900) NULL,
	[MsgOK] [nvarchar](500) NULL,
	[MsgErr] [nvarchar](500) NULL,
	[GUID] [nvarchar](128) NULL,
 CONSTRAINT [Sys_FrmBtnpk] PRIMARY KEY CLUSTERED 
(
	[MyPK] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Sys_FrmAttachmentDB]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sys_FrmAttachmentDB](
	[MyPK] [nvarchar](100) NOT NULL,
	[FK_MapData] [nvarchar](30) NULL,
	[FK_FrmAttachment] [nvarchar](500) NULL,
	[RefPKVal] [nvarchar](50) NULL,
	[Sort] [nvarchar](200) NULL,
	[FileFullName] [nvarchar](700) NULL,
	[FileName] [nvarchar](500) NULL,
	[FileExts] [nvarchar](50) NULL,
	[FileSize] [float] NULL,
	[RDT] [nvarchar](50) NULL,
	[Rec] [nvarchar](50) NULL,
	[RecName] [nvarchar](50) NULL,
	[MyNote] [nvarchar](4000) NULL,
	[NodeID] [nvarchar](50) NULL,
	[IsRowLock] [int] NULL,
	[Idx] [int] NULL,
	[UploadGUID] [nvarchar](50) NULL,
 CONSTRAINT [Sys_FrmAttachmentDBpk] PRIMARY KEY CLUSTERED 
(
	[MyPK] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Sys_FrmAttachment]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sys_FrmAttachment](
	[MyPK] [nvarchar](100) NOT NULL,
	[FK_MapData] [nvarchar](30) NULL,
	[NoOfObj] [nvarchar](50) NULL,
	[FK_Node] [int] NULL,
	[Name] [nvarchar](50) NULL,
	[Exts] [nvarchar](50) NULL,
	[SaveTo] [nvarchar](150) NULL,
	[Sort] [nvarchar](500) NULL,
	[X] [float] NULL,
	[Y] [float] NULL,
	[W] [float] NULL,
	[H] [float] NULL,
	[IsUpload] [int] NULL,
	[IsDelete] [int] NULL,
	[IsDownload] [int] NULL,
	[IsOrder] [int] NULL,
	[IsAutoSize] [int] NULL,
	[IsNote] [int] NULL,
	[IsShowTitle] [int] NULL,
	[UploadType] [int] NULL,
	[CtrlWay] [int] NULL,
	[AthUploadWay] [int] NULL,
	[AtPara] [nvarchar](3000) NULL,
	[RowIdx] [int] NULL,
	[GroupID] [int] NULL,
	[GUID] [nvarchar](128) NULL,
 CONSTRAINT [Sys_FrmAttachmentpk] PRIMARY KEY CLUSTERED 
(
	[MyPK] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Sys_FormTree]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sys_FormTree](
	[No] [nvarchar](10) NOT NULL,
	[Name] [nvarchar](100) NULL,
	[ParentNo] [nvarchar](100) NULL,
	[TreeNo] [nvarchar](100) NULL,
	[IsDir] [int] NULL,
	[Idx] [int] NULL,
 CONSTRAINT [Sys_FormTreepk] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Sys_FormTree] ([No], [Name], [ParentNo], [TreeNo], [IsDir], [Idx]) VALUES (N'0', N'表单库', N'', N'0', 1, 0)
INSERT [dbo].[Sys_FormTree] ([No], [Name], [ParentNo], [TreeNo], [IsDir], [Idx]) VALUES (N'01', N'表单类别1', N'0', N'', 0, 0)
/****** Object:  Table [dbo].[Sys_FileManager]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sys_FileManager](
	[OID] [int] NOT NULL,
	[AttrFileName] [nvarchar](50) NULL,
	[AttrFileNo] [nvarchar](50) NULL,
	[EnName] [nvarchar](50) NULL,
	[RefVal] [nvarchar](50) NULL,
	[WebPath] [nvarchar](100) NULL,
	[MyFileName] [nvarchar](100) NULL,
	[MyFilePath] [nvarchar](100) NULL,
	[MyFileExt] [nvarchar](10) NULL,
	[MyFileH] [int] NULL,
	[MyFileW] [int] NULL,
	[MyFileSize] [float] NULL,
	[RDT] [nvarchar](50) NULL,
	[Rec] [nvarchar](50) NULL,
	[Doc] [nvarchar](4000) NULL,
 CONSTRAINT [Sys_FileManagerpk] PRIMARY KEY CLUSTERED 
(
	[OID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Sys_EnumMain]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sys_EnumMain](
	[No] [nvarchar](40) NOT NULL,
	[Name] [nvarchar](40) NULL,
	[CfgVal] [nvarchar](1500) NULL,
	[Lang] [nvarchar](10) NULL,
 CONSTRAINT [Sys_EnumMainpk] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Sys_Enum]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sys_Enum](
	[MyPK] [nvarchar](100) NOT NULL,
	[Lab] [nvarchar](80) NULL,
	[EnumKey] [nvarchar](40) NULL,
	[IntKey] [int] NULL,
	[Lang] [nvarchar](10) NULL,
 CONSTRAINT [Sys_Enumpk] PRIMARY KEY CLUSTERED 
(
	[MyPK] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'ActionType_CH_0', N'GET', N'ActionType', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'ActionType_CH_1', N'POST', N'ActionType', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'AlertType_CH_0', N'短信', N'AlertType', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'AlertType_CH_1', N'邮件', N'AlertType', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'AlertType_CH_2', N'邮件与短信', N'AlertType', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'AlertType_CH_3', N'系统(内部)消息', N'AlertType', 3, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'AlertWay_CH_0', N'不接收', N'AlertWay', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'AlertWay_CH_1', N'短信', N'AlertWay', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'AlertWay_CH_2', N'邮件', N'AlertWay', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'AlertWay_CH_3', N'内部消息', N'AlertWay', 3, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'AlertWay_CH_4', N'QQ消息', N'AlertWay', 4, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'AlertWay_CH_5', N'RTX消息', N'AlertWay', 5, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'AlertWay_CH_6', N'MSN消息', N'AlertWay', 6, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'AnQuanCPMJ_CH_0', N'非秘', N'AnQuanCPMJ', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'AnQuanCPMJ_CH_1', N'内部', N'AnQuanCPMJ', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'AnQuanCPMJ_CH_2', N'秘密', N'AnQuanCPMJ', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'AnQuanCPMJ_CH_3', N'机密', N'AnQuanCPMJ', 3, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'AnQuanCPMJ_CH_4', N'绝密', N'AnQuanCPMJ', 4, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'AppModel_CH_0', N'BS系统', N'AppModel', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'AppModel_CH_1', N'CS系统', N'AppModel', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'AppType_CH_0', N'外部Url连接', N'AppType', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'AppType_CH_1', N'本地可执行文件', N'AppType', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'AskCate_CH_0', N'病假', N'AskCate', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'AskCate_CH_1', N'婚假', N'AskCate', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'AskCate_CH_2', N'产假', N'AskCate', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'AskCate_CH_3', N'公事外出', N'AskCate', 3, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'AskCate_CH_4', N'其他', N'AskCate', 4, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'AuthorWay_CH_0', N'不授权', N'AuthorWay', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'AuthorWay_CH_1', N'全部流程授权', N'AuthorWay', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'AuthorWay_CH_2', N'指定流程授权', N'AuthorWay', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'BaoXianGongSi_CH_0', N'中国平安', N'BaoXianGongSi', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'BaoXianGongSi_CH_1', N'太平洋保险', N'BaoXianGongSi', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'BaoXianGongSi2_CH_0', N'中国平安', N'BaoXianGongSi2', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'BaoXianGongSi2_CH_1', N'太平洋保险', N'BaoXianGongSi2', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'BaoXianXianZhong_CH_0', N'交强险', N'BaoXianXianZhong', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'BaoXianXianZhong_CH_1', N'车辆损失险', N'BaoXianXianZhong', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'BaoXianXianZhong_CH_2', N'车上人员责任险', N'BaoXianXianZhong', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'BarType_CH_0', N'标题消息列表(Tag1', N'BarType', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'BarType_CH_1', N'其它', N'BarType', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'BatchRole_CH_0', N'不可以批处理', N'BatchRole', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'BatchRole_CH_1', N'批量审核', N'BatchRole', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'BatchRole_CH_2', N'分组批量审核', N'BatchRole', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'BlockModel_CH_0', N'不阻塞', N'BlockModel', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'BlockModel_CH_1', N'当前节点的所有未完成的子流程', N'BlockModel', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'BlockModel_CH_2', N'按约定格式阻塞未完成子流程', N'BlockModel', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'BlockModel_CH_3', N'按照SQL阻塞', N'BlockModel', 3, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'BlockModel_CH_4', N'按照表达式阻塞', N'BlockModel', 4, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'CancelRole_CH_0', N'上一步可以撤销', N'CancelRole', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'CancelRole_CH_1', N'不能撤销', N'CancelRole', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'CancelRole_CH_2', N'上一步与开始节点可以撤销', N'CancelRole', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'CancelRole_CH_3', N'指定的节点可以撤销', N'CancelRole', 3, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'CarCardType_CH_0', N'停车卡', N'CarCardType', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'CarCardType_CH_1', N'油卡', N'CarCardType', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'CarCardType_CH_2', N'Etc卡', N'CarCardType', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'CardType_CH_0', N'港澳通行证', N'CardType', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'CardType_CH_1', N'台湾通行证', N'CardType', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'CardType_CH_2', N'因私护照', N'CardType', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'CCRole_CH_0', N'不能抄送', N'CCRole', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'CCRole_CH_1', N'手工抄送', N'CCRole', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'CCRole_CH_2', N'自动抄送', N'CCRole', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'CCRole_CH_3', N'手工与自动', N'CCRole', 3, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'CCRole_CH_4', N'按表单SysCCEmps字段计算', N'CCRole', 4, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'CCRole_CH_5', N'在发送前打开抄送窗口', N'CCRole', 5, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'CCWriteTo_CH_0', N'写入抄送列表', N'CCWriteTo', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'CCWriteTo_CH_1', N'写入待办', N'CCWriteTo', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'CCWriteTo_CH_2', N'写入待办与抄送列表', N'CCWriteTo', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'CertificatesState_CH_0', N'未归还', N'CertificatesState', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'CertificatesState_CH_1', N'完整', N'CertificatesState', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'CertificatesState_CH_2', N'损坏', N'CertificatesState', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'CFlowWay_CH_0', N'无:非延续类流程', N'CFlowWay', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'CFlowWay_CH_1', N'按照参数', N'CFlowWay', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'CFlowWay_CH_2', N'按照字段配置', N'CFlowWay', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'ChartType_CH_0', N'几何图形', N'ChartType', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'ChartType_CH_1', N'肖像图片', N'ChartType', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'ChuCheFanWei_CH_0', N'市内', N'ChuCheFanWei', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'ChuCheFanWei_CH_1', N'市外', N'ChuCheFanWei', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'CHWay_CH_0', N'不考核', N'CHWay', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'CHWay_CH_1', N'按时效', N'CHWay', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'CHWay_CH_2', N'按工作量', N'CHWay', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'CLZT_CH_0', N'已受理', N'CLZT', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'CLZT_CH_1', N'处理完', N'CLZT', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'CondModel_CH_0', N'由连接线条件控制', N'CondModel', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'CondModel_CH_1', N'让用户手工选择', N'CondModel', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'ConnJudgeWay_CH_0', N'or', N'ConnJudgeWay', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'ConnJudgeWay_CH_1', N'and', N'ConnJudgeWay', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'CPLB_CH_0', N'单导', N'CPLB', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'CPLB_CH_1', N'涉密碎纸机', N'CPLB', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'CPLB_CH_2', N'电磁隔离插座', N'CPLB', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'CPLB_CH_3', N'视频干扰器4', N'CPLB', 3, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'CtrlWay_CH_0', N'按岗位', N'CtrlWay', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'CtrlWay_CH_1', N'按部门', N'CtrlWay', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'CtrlWay_CH_2', N'按人员', N'CtrlWay', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'CtrlWay_CH_3', N'按SQL', N'CtrlWay', 3, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'CZXT_CH_0', N'XP', N'CZXT', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'CZXT_CH_1', N'Win7', N'CZXT', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'CZXT_CH_2', N'linux', N'CZXT', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'CZXT_CH_3', N'其他', N'CZXT', 3, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'DataStoreModel_CH_0', N'数据轨迹模式', N'DataStoreModel', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'DataStoreModel_CH_1', N'数据合并模式', N'DataStoreModel', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'DBSrcType_CH_0', N'应用系统主数据库', N'DBSrcType', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'DBSrcType_CH_1', N'SQLServer', N'DBSrcType', 1, N'CH')
GO
print 'Processed 100 total records'
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'DBSrcType_CH_2', N'Oracle', N'DBSrcType', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'DBSrcType_CH_3', N'MySQL', N'DBSrcType', 3, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'DBSrcType_CH_4', N'Infomix', N'DBSrcType', 4, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'DelEnable_CH_0', N'不能删除', N'DelEnable', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'DelEnable_CH_1', N'逻辑删除', N'DelEnable', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'DelEnable_CH_2', N'记录日志方式删除', N'DelEnable', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'DelEnable_CH_3', N'彻底删除', N'DelEnable', 3, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'DelEnable_CH_4', N'让用户决定删除方式', N'DelEnable', 4, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'DeliveryWay_CH_0', N'01.按当前操作员所属组织结构逐级查找岗位', N'DeliveryWay', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'DeliveryWay_CH_1', N'02.按节点绑定的部门计算', N'DeliveryWay', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'DeliveryWay_CH_10', N'11.按绑定的岗位计算并且以绑定的部门集合为纬度', N'DeliveryWay', 10, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'DeliveryWay_CH_100', N'17.按ccflow的BPM模式处理', N'DeliveryWay', 100, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'DeliveryWay_CH_11', N'12.按指定节点的人员岗位计算', N'DeliveryWay', 11, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'DeliveryWay_CH_12', N'13.按SQL确定子线程接受人与数据源', N'DeliveryWay', 12, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'DeliveryWay_CH_13', N'14.由上一节点的明细表来决定子线程的接受人', N'DeliveryWay', 13, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'DeliveryWay_CH_14', N'15.仅按绑定的岗位计算', N'DeliveryWay', 14, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'DeliveryWay_CH_15', N'16.由FEE来决定', N'DeliveryWay', 15, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'DeliveryWay_CH_2', N'03.按设置的SQL获取接受人计算', N'DeliveryWay', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'DeliveryWay_CH_3', N'04.按节点绑定的人员计算', N'DeliveryWay', 3, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'DeliveryWay_CH_4', N'05.由上一节点发送人通过“人员选择器”选择接受人', N'DeliveryWay', 4, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'DeliveryWay_CH_5', N'06.按上一节点表单指定的字段值作为本步骤的接受人', N'DeliveryWay', 5, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'DeliveryWay_CH_6', N'07.与上一节点处理人员相同', N'DeliveryWay', 6, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'DeliveryWay_CH_7', N'08.与开始节点处理人相同', N'DeliveryWay', 7, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'DeliveryWay_CH_8', N'09.与指定节点处理人相同', N'DeliveryWay', 8, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'DeliveryWay_CH_9', N'10.按绑定的岗位与部门交集计算', N'DeliveryWay', 9, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'DocType_CH_0', N'正式公文', N'DocType', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'DocType_CH_1', N'便函', N'DocType', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'DPType_CH_0', N'飞机票', N'DPType', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'DPType_CH_1', N'火车票', N'DPType', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'Draft_CH_0', N'无(不设草稿)', N'Draft', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'Draft_CH_1', N'保存到待办', N'Draft', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'Draft_CH_2', N'保存到草稿箱', N'Draft', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'DRShe_CH_3', N'周总结', N'DRShe', 3, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'DRShe_CH_4', N'月总结', N'DRShe', 4, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'DRShe_CH_5', N'年度总结', N'DRShe', 5, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'DRSta_CH_0', N'正常', N'DRSta', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'DRSta_CH_1', N'补写', N'DRSta', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'DsBuyZT_CH_0', N'审核中', N'DsBuyZT', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'DsBuyZT_CH_1', N'通过', N'DsBuyZT', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'DsBuyZT_CH_2', N'未通过', N'DsBuyZT', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'DsTakeZT_CH_0', N'审核中', N'DsTakeZT', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'DsTakeZT_CH_1', N'通过', N'DsTakeZT', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'DsTakeZT_CH_2', N'未通过', N'DsTakeZT', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'DtlOpenType_CH_0', N'操作员', N'DtlOpenType', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'DtlOpenType_CH_1', N'工作ID', N'DtlOpenType', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'DtlOpenType_CH_2', N'流程ID', N'DtlOpenType', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'DtlShowModel_CH_0', N'表格', N'DtlShowModel', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'DtlShowModel_CH_1', N'卡片(自由模式)', N'DtlShowModel', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'DtlShowModel_CH_2', N'卡片(傻瓜模式)', N'DtlShowModel', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'DTSearchWay_CH_0', N'不启用', N'DTSearchWay', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'DTSearchWay_CH_1', N'按日期', N'DTSearchWay', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'DTSearchWay_CH_2', N'按日期时间', N'DTSearchWay', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'DTSField_CH_0', N'字段名相同', N'DTSField', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'DTSField_CH_1', N'按设置的字段匹配', N'DTSField', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'DTSField_CH_2', N'以上两者都使用', N'DTSField', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'DTSTime_CH_0', N'所有的节点发送后', N'DTSTime', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'DTSTime_CH_1', N'指定的节点发送后', N'DTSTime', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'DTSTime_CH_2', N'当流程结束时', N'DTSTime', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'DTSWay_CH_0', N'不同步', N'DTSWay', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'DTSWay_CH_1', N'按照业务表指定的WorkID计算', N'DTSWay', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'DTSWay_CH_2', N'按照业务表主键字段计算', N'DTSWay', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'EditerType_CH_0', N'无', N'EditerType', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'EditerType_CH_1', N'sina编辑器', N'EditerType', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'EditerType_CH_2', N'FKCEditer', N'EditerType', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'EditerType_CH_3', N'KindEditor', N'EditerType', 3, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'EditerType_CH_4', N'UEditor', N'EditerType', 4, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'EmpMJ_CH_0', N'一般', N'EmpMJ', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'EmpMJ_CH_1', N'重要', N'EmpMJ', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'EmpMJ_CH_2', N'核心', N'EmpMJ', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'EmpSta_CH_0', N'审核中', N'EmpSta', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'EmpSta_CH_1', N'在岗', N'EmpSta', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'EmpSta_CH_2', N'离岗', N'EmpSta', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'FeiYongMingCheng_CH_0', N'保养', N'FeiYongMingCheng', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'FeiYongMingCheng_CH_1', N'出险', N'FeiYongMingCheng', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'FeiYongMingCheng_CH_2', N'入保', N'FeiYongMingCheng', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'FeiYongMingCheng_CH_3', N'充值', N'FeiYongMingCheng', 3, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'FeiYongMingCheng_CH_4', N'年检', N'FeiYongMingCheng', 4, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'FeiYongMingCheng_CH_5', N'维修', N'FeiYongMingCheng', 5, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'FileStatus_CH_0', N'私有', N'FileStatus', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'FileStatus_CH_1', N'共享给全体人员', N'FileStatus', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'FileStatus_CH_2', N'共享给指定人', N'FileStatus', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'FileStatus_CH_3', N'删除', N'FileStatus', 3, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'FJOpen_CH_0', N'关闭附件', N'FJOpen', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'FJOpen_CH_1', N'操作员', N'FJOpen', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'FJOpen_CH_2', N'工作ID', N'FJOpen', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'FJOpen_CH_3', N'流程ID', N'FJOpen', 3, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'FlowAppType_CH_0', N'业务流程', N'FlowAppType', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'FlowAppType_CH_1', N'工程类(项目组流程)', N'FlowAppType', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'FlowAppType_CH_2', N'公文流程(VSTO)', N'FlowAppType', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'FlowRunWay_CH_0', N'手工启动', N'FlowRunWay', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'FlowRunWay_CH_1', N'指定人员按时启动', N'FlowRunWay', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'FlowRunWay_CH_2', N'数据集按时启动', N'FlowRunWay', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'FlowRunWay_CH_3', N'触发式启动', N'FlowRunWay', 3, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'FLRole_CH_0', N'按接受人', N'FLRole', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'FLRole_CH_1', N'按部门', N'FLRole', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'FLRole_CH_2', N'按岗位', N'FLRole', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'FormRunType_CH_0', N'傻瓜表单', N'FormRunType', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'FormRunType_CH_1', N'自由表单', N'FormRunType', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'FormRunType_CH_2', N'自定义表单', N'FormRunType', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'FormRunType_CH_4', N'Silverlight表单', N'FormRunType', 4, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'FP_CH_0', N'否', N'FP', 0, N'CH')
GO
print 'Processed 200 total records'
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'FP_CH_1', N'是', N'FP', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'FrmType_CH_0', N'自由表单', N'FrmType', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'FrmType_CH_1', N'傻瓜表单', N'FrmType', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'FrmType_CH_2', N'自定义表单', N'FrmType', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'FrmType_CH_3', N'表单树', N'FrmType', 3, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'FrmUrlShowWay_CH_0', N'不显示', N'FrmUrlShowWay', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'FrmUrlShowWay_CH_1', N'自动大小', N'FrmUrlShowWay', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'FrmUrlShowWay_CH_2', N'指定大小', N'FrmUrlShowWay', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'FrmUrlShowWay_CH_3', N'新窗口', N'FrmUrlShowWay', 3, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'FWCAth_CH_0', N'不启用', N'FWCAth', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'FWCAth_CH_1', N'多附件', N'FWCAth', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'FWCAth_CH_2', N'单附件(暂不支持)', N'FWCAth', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'FWCAth_CH_3', N'图片附件(暂不支持)', N'FWCAth', 3, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'FWCShowModel_CH_0', N'表格方式', N'FWCShowModel', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'FWCShowModel_CH_1', N'自由模式', N'FWCShowModel', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'FWCShowModel_CH_2', N'签章模式', N'FWCShowModel', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'FWCSta_CH_0', N'禁用', N'FWCSta', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'FWCSta_CH_1', N'启用', N'FWCSta', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'FWCSta_CH_2', N'只读', N'FWCSta', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'FWCType_CH_0', N'审核组件', N'FWCType', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'FWCType_CH_1', N'日志组件', N'FWCType', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'FWCType_CH_2', N'周报组件', N'FWCType', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'FWCType_CH_3', N'月报组件', N'FWCType', 3, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'Gcgm_CH_1', N'矿井', N'Gcgm', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'Gcgm_CH_2', N'选煤', N'Gcgm', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'Gcgm_CH_3', N'矿井总体', N'Gcgm', 3, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'GCLB_CH_1', N'总包', N'GCLB', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'GCLB_CH_2', N'设计', N'GCLB', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'GCLB_CH_3', N'运营', N'GCLB', 3, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'Gender_CH_0', N'男', N'Gender', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'Gender_CH_1', N'女', N'Gender', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'GetBadZT_CH_0', N'审核中', N'GetBadZT', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'GetBadZT_CH_1', N'通过', N'GetBadZT', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'GetBadZT_CH_2', N'未通过', N'GetBadZT', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'GetUseZT_CH_0', N'审核中', N'GetUseZT', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'GetUseZT_CH_1', N'通过', N'GetUseZT', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'GetUseZT_CH_2', N'未通过', N'GetUseZT', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'GiveBackZT_CH_0', N'审核中', N'GiveBackZT', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'GiveBackZT_CH_1', N'通过', N'GiveBackZT', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'GiveBackZT_CH_2', N'未通过', N'GiveBackZT', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'GQ_CH_0', N'无', N'GQ', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'GQ_CH_1', N'只读', N'GQ', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'GQ_CH_2', N'刻录', N'GQ', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'GuangQu_CH_0', N'无', N'GuangQu', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'GuangQu_CH_1', N'只读', N'GuangQu', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'GuangQu_CH_2', N'刻录', N'GuangQu', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'GuDingQiXian_CH_0', N'有', N'GuDingQiXian', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'GuDingQiXian_CH_1', N'无', N'GuDingQiXian', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'HDFS_CH_0', N'摇号', N'HDFS', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'HDFS_CH_1', N'变更', N'HDFS', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'HungUpWay_CH_0', N'无限挂起', N'HungUpWay', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'HungUpWay_CH_1', N'按指定的时间解除挂起并通知我自己', N'HungUpWay', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'HungUpWay_CH_2', N'按指定的时间解除挂起并通知所有人', N'HungUpWay', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'HYSort_CH_0', N'涉密会议', N'HYSort', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'HYSort_CH_1', N'机密会议', N'HYSort', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'HYSort_CH_2', N'其他', N'HYSort', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'Importance_CH_0', N'普通', N'Importance', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'Importance_CH_1', N'比较重要', N'Importance', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'Importance_CH_2', N'很重要', N'Importance', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'IsAnony_CH_0', N'实名', N'IsAnony', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'IsAnony_CH_1', N'匿名', N'IsAnony', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'IsAnonymous_CH_0', N'不允许', N'IsAnonymous', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'IsAnonymous_CH_1', N'允许', N'IsAnonymous', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'ISBM_CH_0', N' 否', N'ISBM', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'ISBM_CH_1', N' 是', N'ISBM', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'IsEnable_CH_0', N'禁用', N'IsEnable', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'IsEnable_CH_1', N'启用', N'IsEnable', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'IsTack_CH_0', N'否', N'IsTack', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'IsTack_CH_1', N'是', N'IsTack', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'JHType_CH_0', N'审核未通过', N'JHType', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'JHType_CH_1', N'审核通过', N'JHType', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'JHType_CH_2', N'审核中', N'JHType', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'JMCD_CH_0', N'一般', N'JMCD', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'JMCD_CH_1', N'保密', N'JMCD', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'JMCD_CH_2', N'秘密', N'JMCD', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'JMCD_CH_3', N'机密', N'JMCD', 3, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'JSJMJ_CH_0', N'非秘', N'JSJMJ', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'JSJMJ_CH_1', N'内部', N'JSJMJ', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'JSJMJ_CH_2', N'秘密', N'JSJMJ', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'JSJMJ_CH_3', N'机密', N'JSJMJ', 3, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'JSJMJ_CH_4', N'绝密', N'JSJMJ', 4, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'JSJType_CH_0', N'连网机', N'JSJType', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'JSJType_CH_1', N'涉密单机', N'JSJType', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'JSJType_CH_2', N'涉密笔记本', N'JSJType', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'JSJType_CH_3', N'非密办公计算机', N'JSJType', 3, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'JSJType_CH_4', N'上网计算机', N'JSJType', 4, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'JSLX_CH_1', N'总包', N'JSLX', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'JSLX_CH_2', N'设计', N'JSLX', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'JSLX_CH_3', N'运营', N'JSLX', 3, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'JumpWay_CH_0', N'不能跳转', N'JumpWay', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'JumpWay_CH_1', N'只能向后跳转', N'JumpWay', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'JumpWay_CH_2', N'只能向前跳转', N'JumpWay', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'JumpWay_CH_3', N'任意节点跳转', N'JumpWay', 3, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'JumpWay_CH_4', N'按指定规则跳转', N'JumpWay', 4, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'KMTreeCtrlWay_CH_0', N'任何人', N'KMTreeCtrlWay', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'KMTreeCtrlWay_CH_1', N'继承', N'KMTreeCtrlWay', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'KMTreeCtrlWay_CH_2', N'按岗位', N'KMTreeCtrlWay', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'KMTreeCtrlWay_CH_3', N'按部门', N'KMTreeCtrlWay', 3, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'KMTreeCtrlWay_CH_4', N'按人员', N'KMTreeCtrlWay', 4, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'KMTreeCtrlWay_CH_5', N'按岗位与部门的交集', N'KMTreeCtrlWay', 5, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'KMTreeCtrlWay_CH_6', N'按SQL', N'KMTreeCtrlWay', 6, N'CH')
GO
print 'Processed 300 total records'
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'LB_CH_0', N'固定资产维修', N'LB', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'LB_CH_1', N'后勤设施维修', N'LB', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'LCZT_CH_0', N'申请', N'LCZT', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'LCZT_CH_1', N'流程中', N'LCZT', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'LCZT_CH_2', N'流程结束', N'LCZT', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'LeaveType_CH_0', N'出差', N'LeaveType', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'LeaveType_CH_1', N'请假', N'LeaveType', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'LeaveType_CH_2', N'外出', N'LeaveType', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'LGType_CH_0', N'普通', N'LGType', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'LGType_CH_1', N'枚举', N'LGType', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'LGType_CH_2', N'外键', N'LGType', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'LX_CH_1', N'总包', N'LX', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'LX_CH_2', N'设计', N'LX', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'LX_CH_3', N'运营', N'LX', 3, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'MenuType_CH_3', N'目录', N'MenuType', 3, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'MenuType_CH_4', N'功能', N'MenuType', 4, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'MenuType_CH_5', N'功能控制点', N'MenuType', 5, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'MJ_CH_0', N'秘密', N'MJ', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'MJ_CH_1', N'机密', N'MJ', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'MJ_CH_2', N'绝密', N'MJ', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'Model_CH_0', N'普通', N'Model', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'Model_CH_1', N'固定行', N'Model', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'MoveToShowWay_CH_0', N'不显示', N'MoveToShowWay', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'MoveToShowWay_CH_1', N'下拉列表', N'MoveToShowWay', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'MoveToShowWay_CH_2', N'平铺', N'MoveToShowWay', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'MPad_SrcModel_CH_0', N'强制横屏', N'MPad_SrcModel', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'MPad_SrcModel_CH_1', N'强制竖屏', N'MPad_SrcModel', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'MPad_SrcModel_CH_2', N'由重力感应决定', N'MPad_SrcModel', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'MPad_WorkModel_CH_0', N'原生态', N'MPad_WorkModel', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'MPad_WorkModel_CH_1', N'浏览器', N'MPad_WorkModel', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'MPad_WorkModel_CH_2', N'禁用', N'MPad_WorkModel', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'MPhone_SrcModel_CH_0', N'强制横屏', N'MPhone_SrcModel', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'MPhone_SrcModel_CH_1', N'强制竖屏', N'MPhone_SrcModel', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'MPhone_SrcModel_CH_2', N'由重力感应决定', N'MPhone_SrcModel', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'MPhone_WorkModel_CH_0', N'原生态', N'MPhone_WorkModel', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'MPhone_WorkModel_CH_1', N'浏览器', N'MPhone_WorkModel', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'MPhone_WorkModel_CH_2', N'禁用', N'MPhone_WorkModel', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'MsgCtrl_CH_0', N'不发送', N'MsgCtrl', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'MsgCtrl_CH_1', N'按设置的发送范围自动发送', N'MsgCtrl', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'MsgCtrl_CH_2', N'由本节点表单系统字段(IsSendEmail,IsSendSMS)来决定', N'MsgCtrl', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'MsgCtrl_CH_3', N'由SDK开发者参数(IsSendEmail,IsSendSMS)来决定', N'MsgCtrl', 3, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'MyDataType_CH_1', N'字符串String', N'MyDataType', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'MyDataType_CH_2', N'整数类型Int', N'MyDataType', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'MyDataType_CH_3', N'浮点类型AppFloat', N'MyDataType', 3, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'MyDataType_CH_4', N'判断类型Boolean', N'MyDataType', 4, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'MyDataType_CH_5', N'双精度类型Double', N'MyDataType', 5, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'MyDataType_CH_6', N'日期型Date', N'MyDataType', 6, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'MyDataType_CH_7', N'时间类型Datetime', N'MyDataType', 7, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'MyDataType_CH_8', N'金额类型AppMoney', N'MyDataType', 8, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'NewGuDingQiXian_CH_0', N'有', N'NewGuDingQiXian', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'NewGuDingQiXian_CH_1', N'无', N'NewGuDingQiXian', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'NianFen_CH_0', N'2010', N'NianFen', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'NianFen_CH_1', N'2011', N'NianFen', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'NianFen_CH_2', N'2012', N'NianFen', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'NianFen_CH_3', N'2013', N'NianFen', 3, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'NianFen_CH_4', N'2014', N'NianFen', 4, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'NianFen_CH_5', N'2015', N'NianFen', 5, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'NodeFormType_CH_0', N'傻瓜表单(ccflow6取消支持)', N'NodeFormType', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'NodeFormType_CH_1', N'自由表单', N'NodeFormType', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'NodeFormType_CH_100', N'禁用(对多表单流程有效)', N'NodeFormType', 100, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'NodeFormType_CH_2', N'自定义表单', N'NodeFormType', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'NodeFormType_CH_3', N'SDK表单', N'NodeFormType', 3, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'NodeFormType_CH_4', N'SL表单(ccflow6取消支持)', N'NodeFormType', 4, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'NodeFormType_CH_5', N'表单树', N'NodeFormType', 5, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'NodeFormType_CH_6', N'动态表单树', N'NodeFormType', 6, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'NodeFormType_CH_7', N'公文表单(WebOffice)', N'NodeFormType', 7, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'NodeFormType_CH_8', N'Excel表单(测试中)', N'NodeFormType', 8, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'NodeFormType_CH_9', N'Word表单(测试中)', N'NodeFormType', 9, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'NoticeSta_CH_0', N'自动', N'NoticeSta', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'NoticeSta_CH_1', N'打开', N'NoticeSta', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'NoticeSta_CH_2', N'关闭', N'NoticeSta', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'OldGuDingQiXian_CH_0', N'有', N'OldGuDingQiXian', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'OldGuDingQiXian_CH_1', N'无', N'OldGuDingQiXian', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'OpenWay_CH_0', N'新窗口', N'OpenWay', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'OpenWay_CH_1', N'本窗口', N'OpenWay', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'OpenWay_CH_2', N'覆盖新窗口', N'OpenWay', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'OutTimeDeal_CH_0', N'不处理', N'OutTimeDeal', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'OutTimeDeal_CH_1', N'自动向下运动', N'OutTimeDeal', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'OutTimeDeal_CH_2', N'自动跳转指定的节点', N'OutTimeDeal', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'OutTimeDeal_CH_3', N'自动移交给指定的人员', N'OutTimeDeal', 3, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'OutTimeDeal_CH_4', N'向指定的人员发消息', N'OutTimeDeal', 4, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'OutTimeDeal_CH_5', N'删除流程', N'OutTimeDeal', 5, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'OutTimeDeal_CH_6', N'执行SQL', N'OutTimeDeal', 6, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'PanelType_CH_0', N'图片，轮换图', N'PanelType', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'PanelType_CH_1', N'外部链接', N'PanelType', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'PanelType_CH_2', N'列表', N'PanelType', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'PanelType_CH_3', N'表格式', N'PanelType', 3, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'PanelType_CH_4', N'图表', N'PanelType', 4, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'PopValFormat_CH_0', N'No(仅编号)', N'PopValFormat', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'PopValFormat_CH_1', N'Name(仅名称)', N'PopValFormat', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'PopValFormat_CH_2', N'No,Name(编号与名称,比如zhangsan,张三;lisi,李四;)', N'PopValFormat', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'PRI_CH_0', N'低', N'PRI', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'PRI_CH_1', N'中', N'PRI', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'PRI_CH_2', N'高', N'PRI', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'PrintDocEnable_CH_0', N'不打印', N'PrintDocEnable', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'PrintDocEnable_CH_1', N'打印网页', N'PrintDocEnable', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'PrintDocEnable_CH_2', N'打印RTF模板', N'PrintDocEnable', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'PrintDocEnable_CH_3', N'打印Word模版', N'PrintDocEnable', 3, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'PrivPlanItemType_CH_0', N'可选任务', N'PrivPlanItemType', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'PrivPlanItemType_CH_1', N'主要任务', N'PrivPlanItemType', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'PushWay_CH_0', N'按照指定节点的工作人员', N'PushWay', 0, N'CH')
GO
print 'Processed 400 total records'
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'PushWay_CH_1', N'按照指定的工作人员', N'PushWay', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'PushWay_CH_2', N'按照指定的工作岗位', N'PushWay', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'PushWay_CH_3', N'按照指定的部门', N'PushWay', 3, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'PushWay_CH_4', N'按照指定的SQL', N'PushWay', 4, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'PushWay_CH_5', N'按照系统指定的字段', N'PushWay', 5, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'ReadReceipts_CH_0', N'不回执', N'ReadReceipts', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'ReadReceipts_CH_1', N'自动回执', N'ReadReceipts', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'ReadReceipts_CH_2', N'由上一节点表单字段决定', N'ReadReceipts', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'ReadReceipts_CH_3', N'由SDK开发者参数决定', N'ReadReceipts', 3, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'RepairZT_CH_0', N'审核中', N'RepairZT', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'RepairZT_CH_1', N'通过', N'RepairZT', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'RepairZT_CH_2', N'未通过', N'RepairZT', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'ReturnRole_CH_0', N'不能退回', N'ReturnRole', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'ReturnRole_CH_1', N'只能退回上一个节点', N'ReturnRole', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'ReturnRole_CH_2', N'可退回以前任意节点', N'ReturnRole', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'ReturnRole_CH_3', N'可退回指定的节点', N'ReturnRole', 3, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'ReturnRole_CH_4', N'由流程图设计的退回路线决定', N'ReturnRole', 4, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'RoomSta_CH_0', N'正常', N'RoomSta', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'RoomSta_CH_1', N'维护中', N'RoomSta', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'RunModel_CH_0', N'普通', N'RunModel', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'RunModel_CH_1', N'合流', N'RunModel', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'RunModel_CH_2', N'分流', N'RunModel', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'RunModel_CH_3', N'分合流', N'RunModel', 3, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'RunModel_CH_4', N'子线程', N'RunModel', 4, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'RYLX_CH_0', N'正式', N'RYLX', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'RYLX_CH_1', N'挂职', N'RYLX', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'RYLX_CH_2', N'借调', N'RYLX', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'RYLX_CH_3', N'工勤', N'RYLX', 3, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'RYLX_CH_4', N'返聘', N'RYLX', 4, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'SaveModel_CH_0', N'仅节点表', N'SaveModel', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'SaveModel_CH_1', N'节点表与Rpt表', N'SaveModel', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'SBLB_CH_0', N'台式机', N'SBLB', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'SBLB_CH_1', N'笔记本', N'SBLB', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'SBMJ_CH_0', N'非密', N'SBMJ', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'SBMJ_CH_1', N'内部', N'SBMJ', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'SBMJ_CH_2', N'秘密', N'SBMJ', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'SBMJ_CH_3', N'机密', N'SBMJ', 3, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'SBMJ_CH_4', N'绝密', N'SBMJ', 4, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'SBSort_CH_0', N'计算机', N'SBSort', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'SBSort_CH_1', N'服务器', N'SBSort', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'SBSort_CH_2', N'网络设备', N'SBSort', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'SBSort_CH_3', N'打印机', N'SBSort', 3, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'SBSort_CH_4', N'复印机', N'SBSort', 4, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'SBSort_CH_5', N'扫描仪', N'SBSort', 5, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'SBSort_CH_6', N'多功能一体机', N'SBSort', 6, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'SBSta_CH_0', N'在库', N'SBSta', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'SBSta_CH_1', N'领用', N'SBSta', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'SBSta_CH_2', N'在用', N'SBSta', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'SBSta_CH_3', N'停用', N'SBSta', 3, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'SBSta_CH_4', N'报废', N'SBSta', 4, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'SBSta_CH_5', N'销毁', N'SBSta', 5, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'SBZT_CH_1', N'在库', N'SBZT', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'SBZT_CH_2', N'领用', N'SBZT', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'SBZT_CH_3', N'在用', N'SBZT', 3, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'SBZT_CH_4', N'停用', N'SBZT', 4, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'SBZT_CH_5', N'报废', N'SBZT', 5, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'SBZT_CH_6', N'销毁', N'SBZT', 6, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'SCLZT_CH_0', N'未处理', N'SCLZT', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'SCLZT_CH_1', N'已受理', N'SCLZT', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'SCLZT_CH_2', N'处理完成', N'SCLZT', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'SealType_CH_0', N'公章', N'SealType', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'SealType_CH_2', N'财务章', N'SealType', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'SealType_CH_3', N'财务章', N'SealType', 3, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'SelectAccepterEnable_CH_0', N'不启用', N'SelectAccepterEnable', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'SelectAccepterEnable_CH_1', N'单独启用', N'SelectAccepterEnable', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'SelectAccepterEnable_CH_2', N'在发送前打开', N'SelectAccepterEnable', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'SelectAccepterEnable_CH_3', N'转入新页面', N'SelectAccepterEnable', 3, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'SelectorDBShowWay_CH_0', N'表格显示', N'SelectorDBShowWay', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'SelectorDBShowWay_CH_1', N'树形显示', N'SelectorDBShowWay', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'SelectorModel_CH_0', N'按岗位', N'SelectorModel', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'SelectorModel_CH_1', N'按部门', N'SelectorModel', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'SelectorModel_CH_2', N'按人员', N'SelectorModel', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'SelectorModel_CH_3', N'按SQL', N'SelectorModel', 3, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'SelectorModel_CH_4', N'自定义Url', N'SelectorModel', 4, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'SetMes_CH_0', N'不推送', N'SetMes', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'SetMes_CH_1', N'站内消息', N'SetMes', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'SFTableType_CH_0', N'NoName类型', N'SFTableType', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'SFTableType_CH_1', N'NoNameTree类型', N'SFTableType', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'SFTableType_CH_2', N'NoName行政区划类型', N'SFTableType', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'SharingType_CH_0', N'共享', N'SharingType', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'SharingType_CH_1', N'私有', N'SharingType', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'ShowWhere_CH_0', N'树形表单', N'ShowWhere', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'ShowWhere_CH_1', N'工具栏', N'ShowWhere', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'SJLY_CH_0', N'系统增加', N'SJLY', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'SJLY_CH_1', N'审批增加', N'SJLY', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'SJLY_CH_2', N'手动增加', N'SJLY', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'Sort_CH_0', N'宣传报道', N'Sort', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'Sort_CH_1', N'信息发布', N'Sort', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'Sort_CH_2', N'其他', N'Sort', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'SSHY_CH_1', N'软件1', N'SSHY', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'SSHY_CH_2', N'软件2', N'SSHY', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'SSHY_CH_3', N'软件3', N'SSHY', 3, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'SSOType_CH_0', N'SID验证', N'SSOType', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'SSOType_CH_1', N'连接', N'SSOType', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'SSOType_CH_2', N'表单提交', N'SSOType', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'SSOType_CH_3', N'不传值', N'SSOType', 3, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'StaGrade_CH_1', N'高层岗', N'StaGrade', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'StaGrade_CH_2', N'中层岗', N'StaGrade', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'StaGrade_CH_3', N'执行岗', N'StaGrade', 3, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'StartGuideWay_CH_0', N'无', N'StartGuideWay', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'StartGuideWay_CH_1', N'按系统的URL-(父子流程)单条模式', N'StartGuideWay', 1, N'CH')
GO
print 'Processed 500 total records'
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'StartGuideWay_CH_10', N'按自定义的Url', N'StartGuideWay', 10, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'StartGuideWay_CH_11', N'按用户输入参数', N'StartGuideWay', 11, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'StartGuideWay_CH_2', N'按系统的URL-(子父流程)多条模式', N'StartGuideWay', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'StartGuideWay_CH_3', N'按系统的URL-(实体记录,未完成)单条模式', N'StartGuideWay', 3, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'StartGuideWay_CH_4', N'按系统的URL-(实体记录,未完成)多条模式', N'StartGuideWay', 4, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'StartGuideWay_CH_5', N'从开始节点Copy数据', N'StartGuideWay', 5, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'StartLimitRole_CH_0', N'不限制', N'StartLimitRole', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'StartLimitRole_CH_1', N'每人每天一次', N'StartLimitRole', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'StartLimitRole_CH_2', N'每人每周一次', N'StartLimitRole', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'StartLimitRole_CH_3', N'每人每月一次', N'StartLimitRole', 3, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'StartLimitRole_CH_4', N'每人每季一次', N'StartLimitRole', 4, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'StartLimitRole_CH_5', N'每人每年一次', N'StartLimitRole', 5, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'StartLimitRole_CH_6', N'发起的列不能重复,(多个列可以用逗号分开)', N'StartLimitRole', 6, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'StartLimitRole_CH_7', N'设置的SQL数据源为空,或者返回结果为零时可以启动.', N'StartLimitRole', 7, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'StartLimitRole_CH_8', N'设置的SQL数据源为空,或者返回结果为零时不可以启动.', N'StartLimitRole', 8, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'SubFlowCtrlRole_CH_0', N'无', N'SubFlowCtrlRole', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'SubFlowCtrlRole_CH_1', N'不可以删除子流程', N'SubFlowCtrlRole', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'SubFlowCtrlRole_CH_2', N'可以删除子流程', N'SubFlowCtrlRole', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'SubFlowStartWay_CH_0', N'不启动', N'SubFlowStartWay', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'SubFlowStartWay_CH_1', N'指定的字段启动', N'SubFlowStartWay', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'SubFlowStartWay_CH_2', N'按明细表启动', N'SubFlowStartWay', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'SubThreadType_CH_0', N'同表单', N'SubThreadType', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'SubThreadType_CH_1', N'异表单', N'SubThreadType', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'Target_CH_0', N'新窗口', N'Target', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'Target_CH_1', N'本窗口', N'Target', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'Target_CH_2', N'父窗口', N'Target', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'TaskSta_CH_0', N'未开始', N'TaskSta', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'TaskSta_CH_1', N'进行中', N'TaskSta', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'TaskSta_CH_2', N'完成', N'TaskSta', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'TaskSta_CH_3', N'推迟', N'TaskSta', 3, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'TBModel_CH_0', N'单行文本', N'TBModel', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'TBModel_CH_1', N'多行文本', N'TBModel', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'TBModel_CH_2', N'富文本', N'TBModel', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'TBZT_CH_1', N'投标', N'TBZT', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'TBZT_CH_2', N'中标', N'TBZT', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'TBZT_CH_3', N'未中标', N'TBZT', 3, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'ThreadKillRole_CH_0', N'不能删除', N'ThreadKillRole', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'ThreadKillRole_CH_1', N'手工删除', N'ThreadKillRole', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'ThreadKillRole_CH_2', N'自动删除', N'ThreadKillRole', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'TimelineRole_CH_0', N'按节点(由节点属性来定义)', N'TimelineRole', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'TimelineRole_CH_1', N'按发起人(开始节点SysSDTOfFlow字段计算)', N'TimelineRole', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'TodolistModel_CH_0', N'抢办模式', N'TodolistModel', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'TodolistModel_CH_1', N'协作模式', N'TodolistModel', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'TodolistModel_CH_2', N'队列模式', N'TodolistModel', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'TodolistModel_CH_3', N'共享模式', N'TodolistModel', 3, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'TurnToDeal_CH_0', N'提示ccflow默认信息', N'TurnToDeal', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'TurnToDeal_CH_1', N'提示指定信息', N'TurnToDeal', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'TurnToDeal_CH_2', N'转向指定的url', N'TurnToDeal', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'TurnToDeal_CH_3', N'按照条件转向', N'TurnToDeal', 3, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'TXGS_CH_0', N'移动公司', N'TXGS', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'TXGS_CH_1', N'联通公司', N'TXGS', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'TXGS_CH_2', N'电信公司', N'TXGS', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'UIRowStyleGlo_CH_0', N'无风格', N'UIRowStyleGlo', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'UIRowStyleGlo_CH_1', N'交替风格', N'UIRowStyleGlo', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'UIRowStyleGlo_CH_2', N'鼠标移动', N'UIRowStyleGlo', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'UIRowStyleGlo_CH_3', N'交替并鼠标移动', N'UIRowStyleGlo', 3, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'USBMJ_CH_0', N'非秘', N'USBMJ', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'USBMJ_CH_1', N'内部', N'USBMJ', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'USBMJ_CH_2', N'秘密', N'USBMJ', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'USBMJ_CH_3', N'机密', N'USBMJ', 3, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'USBMJ_CH_4', N'绝密', N'USBMJ', 4, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'USBType_CH_0', N'U盘', N'USBType', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'USBType_CH_1', N'移动硬盘', N'USBType', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'USBType_CH_2', N'光盘', N'USBType', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'USBType_CH_3', N'专用U盘', N'USBType', 3, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'WaterSet_CH_0', N'不添加水印', N'WaterSet', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'WaterSet_CH_1', N'文字水印', N'WaterSet', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'WaterSet_CH_2', N'图片水印', N'WaterSet', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'WebOfficeEnable_CH_0', N'不启用', N'WebOfficeEnable', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'WebOfficeEnable_CH_1', N'按钮方式', N'WebOfficeEnable', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'WebOfficeEnable_CH_2', N'标签页方式', N'WebOfficeEnable', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'WFSta_CH_0', N'运行中', N'WFSta', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'WFSta_CH_1', N'已完成', N'WFSta', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'WFSta_CH_2', N'其他', N'WFSta', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'WFState_CH_0', N'空白', N'WFState', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'WFState_CH_1', N'草稿', N'WFState', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'WFState_CH_10', N'批处理', N'WFState', 10, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'WFState_CH_2', N'运行中', N'WFState', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'WFState_CH_3', N'已完成', N'WFState', 3, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'WFState_CH_4', N'挂起', N'WFState', 4, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'WFState_CH_5', N'退回', N'WFState', 5, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'WFState_CH_6', N'转发', N'WFState', 6, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'WFState_CH_7', N'删除', N'WFState', 7, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'WFState_CH_8', N'加签', N'WFState', 8, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'WFState_CH_9', N'冻结', N'WFState', 9, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'WFStateApp_CH_10', N'批处理', N'WFStateApp', 10, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'WFStateApp_CH_2', N'运行中', N'WFStateApp', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'WFStateApp_CH_3', N'已完成', N'WFStateApp', 3, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'WFStateApp_CH_4', N'挂起', N'WFStateApp', 4, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'WFStateApp_CH_5', N'退回', N'WFStateApp', 5, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'WFStateApp_CH_6', N'转发', N'WFStateApp', 6, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'WFStateApp_CH_7', N'删除', N'WFStateApp', 7, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'WFStateApp_CH_8', N'加签', N'WFStateApp', 8, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'WFStateApp_CH_9', N'冻结', N'WFStateApp', 9, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'WhenNoWorker_CH_0', N'提示错误', N'WhenNoWorker', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'WhenNoWorker_CH_1', N'自动转到下一步', N'WhenNoWorker', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'WhenOverSize_CH_0', N'不处理', N'WhenOverSize', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'WhenOverSize_CH_1', N'向下顺增行', N'WhenOverSize', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'WhenOverSize_CH_2', N'次页显示', N'WhenOverSize', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'WhoExeIt_CH_0', N'操作员执行', N'WhoExeIt', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'WhoExeIt_CH_1', N'机器执行', N'WhoExeIt', 1, N'CH')
GO
print 'Processed 600 total records'
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'WhoExeIt_CH_2', N'混合执行', N'WhoExeIt', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'WhoIsPK_CH_0', N'工作ID是主键', N'WhoIsPK', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'WhoIsPK_CH_1', N'流程ID是主键', N'WhoIsPK', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'WhoIsPK_CH_2', N'父流程ID是主键', N'WhoIsPK', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'WhoIsPK_CH_3', N'延续流程ID是主键', N'WhoIsPK', 3, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'WLJFMJ_CH_0', N'非秘', N'WLJFMJ', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'WLJFMJ_CH_1', N'内部', N'WLJFMJ', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'WLJFMJ_CH_2', N'秘密', N'WLJFMJ', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'WLJFMJ_CH_3', N'机密', N'WLJFMJ', 3, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'WLJFMJ_CH_4', N'绝密', N'WLJFMJ', 4, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'WLLB_CH_0', N'内网', N'WLLB', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'WLLB_CH_1', N'外网', N'WLLB', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'WLLB_CH_2', N'单机', N'WLLB', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'WLSBLB_CH_0', N'服务器', N'WLSBLB', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'WLSBLB_CH_1', N'网络设备', N'WLSBLB', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'WLSBLB_CH_2', N'安全设备', N'WLSBLB', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'WLSBLB_CH_3', N'密码设备', N'WLSBLB', 3, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'WorkYearCount_CH_0', N'应届毕业生', N'WorkYearCount', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'WorkYearCount_CH_1', N'实习生', N'WorkYearCount', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'WorkYearCount_CH_2', N'1年以下', N'WorkYearCount', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'WorkYearCount_CH_3', N'1年', N'WorkYearCount', 3, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'WorkYearCount_CH_4', N'1.5年', N'WorkYearCount', 4, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'WorkYearCount_CH_5', N'2年', N'WorkYearCount', 5, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'WorkYearCount_CH_6', N'3-5年', N'WorkYearCount', 6, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'WorkYearCount_CH_7', N'5年以上', N'WorkYearCount', 7, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'XB_CH_0', N'女', N'XB', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'XB_CH_1', N'男', N'XB', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'XMLX_CH_0', N'1', N'XMLX', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'XueLi_CH_0', N'博士以上', N'XueLi', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'XueLi_CH_1', N'博士', N'XueLi', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'XueLi_CH_2', N'硕士', N'XueLi', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'XueLi_CH_3', N'本科', N'XueLi', 3, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'XueLi_CH_4', N'大专', N'XueLi', 4, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'XueLi_CH_5', N'中专', N'XueLi', 5, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'XXLX_CH_0', N'在线练习', N'XXLX', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'XXLX_CH_1', N'在线考试', N'XXLX', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'XXLX_CH_2', N'视频学习', N'XXLX', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'XXLX_CH_3', N'部门培训', N'XXLX', 3, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'XXLX_CH_4', N'公司培训', N'XXLX', 4, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'XXLX_CH_5', N'外部培训', N'XXLX', 5, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'XXSort_CH_0', N'宣传报道', N'XXSort', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'XXSort_CH_1', N'信息发布', N'XXSort', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'XXSort_CH_2', N'其他', N'XXSort', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'YueFen_CH_0', N'1', N'YueFen', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'YueFen_CH_1', N'2', N'YueFen', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'YueFen_CH_10', N'11', N'YueFen', 10, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'YueFen_CH_11', N'12', N'YueFen', 11, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'YueFen_CH_2', N'3', N'YueFen', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'YueFen_CH_3', N'4', N'YueFen', 3, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'YueFen_CH_4', N'5', N'YueFen', 4, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'YueFen_CH_5', N'6', N'YueFen', 5, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'YueFen_CH_6', N'7', N'YueFen', 6, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'YueFen_CH_7', N'8', N'YueFen', 7, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'YueFen_CH_8', N'9', N'YueFen', 8, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'YueFen_CH_9', N'10', N'YueFen', 9, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'YYLB_CH_0', N'内网', N'YYLB', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'YYLB_CH_1', N'外网', N'YYLB', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'YYLB_CH_2', N'单机', N'YYLB', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'YYLB_CH_3', N'其他', N'YYLB', 3, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'ZBZT_CH_0', N'未用', N'ZBZT', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'ZBZT_CH_1', N'已用', N'ZBZT', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'ZCZT_CH_0', N'闲置', N'ZCZT', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'ZCZT_CH_1', N'使用中', N'ZCZT', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'ZCZT_CH_2', N'报废', N'ZCZT', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'ZCZT_CH_3', N'维修中', N'ZCZT', 3, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'ZhuangTai_CH_0', N'在职', N'ZhuangTai', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'ZhuangTai_CH_1', N'离职', N'ZhuangTai', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'ZZMM_CH_0', N'党员', N'ZZMM', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'ZZMM_CH_1', N'团员', N'ZZMM', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'ZZMM_CH_2', N'其他党派人士', N'ZZMM', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'ZZMM_CH_3', N'无党派', N'ZZMM', 3, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'ZZMM_CH_4', N'群众', N'ZZMM', 4, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'ZZSta_CH_0', N'在办', N'ZZSta', 0, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'ZZSta_CH_1', N'在存', N'ZZSta', 1, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'ZZSta_CH_2', N'借出', N'ZZSta', 2, N'CH')
INSERT [dbo].[Sys_Enum] ([MyPK], [Lab], [EnumKey], [IntKey], [Lang]) VALUES (N'ZZSta_CH_3', N'作废', N'ZZSta', 3, N'CH')
/****** Object:  Table [dbo].[Sys_EnsAppCfg]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sys_EnsAppCfg](
	[MyPK] [nvarchar](100) NOT NULL,
	[EnsName] [nvarchar](100) NULL,
	[CfgKey] [nvarchar](100) NULL,
	[CfgVal] [nvarchar](200) NULL,
 CONSTRAINT [Sys_EnsAppCfgpk] PRIMARY KEY CLUSTERED 
(
	[MyPK] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.AnQuanCPs@EditerType', N'BP.BM.AnQuanCPs', N'EditerType', N'0')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.AnQuanCPs@FocusField', N'BP.BM.AnQuanCPs', N'FocusField', N'Name')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.AnQuanCPs@IsEnableDouclickGlo', N'BP.BM.AnQuanCPs', N'IsEnableDouclickGlo', N'1')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.AnQuanCPs@IsEnableFocusField', N'BP.BM.AnQuanCPs', N'IsEnableFocusField', N'1')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.AnQuanCPs@IsEnableOpenICON', N'BP.BM.AnQuanCPs', N'IsEnableOpenICON', N'1')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.AnQuanCPs@IsEnableRefFunc', N'BP.BM.AnQuanCPs', N'IsEnableRefFunc', N'1')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.AnQuanCPs@UIRowStyleGlo', N'BP.BM.AnQuanCPs', N'UIRowStyleGlo', N'2')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.AnQuanCPs@WinCardH', N'BP.BM.AnQuanCPs', N'WinCardH', N'480')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.AnQuanCPs@WinCardW', N'BP.BM.AnQuanCPs', N'WinCardW', N'820')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.AQCPs@FocusField', N'BP.BM.AQCPs', N'FocusField', N'Name')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.AQCPs@IsEnableDouclickGlo', N'BP.BM.AQCPs', N'IsEnableDouclickGlo', N'1')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.AQCPs@IsEnableFocusField', N'BP.BM.AQCPs', N'IsEnableFocusField', N'1')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.AQCPs@IsEnableOpenICON', N'BP.BM.AQCPs', N'IsEnableOpenICON', N'1')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.AQCPs@IsEnableRefFunc', N'BP.BM.AQCPs', N'IsEnableRefFunc', N'1')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.AQCPs@UIRowStyleGlo', N'BP.BM.AQCPs', N'UIRowStyleGlo', N'2')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.AQCPs@WinCardH', N'BP.BM.AQCPs', N'WinCardH', N'480')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.AQCPs@WinCardW', N'BP.BM.AQCPs', N'WinCardW', N'820')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.CardDtls@EditerType', N'BP.BM.CardDtls', N'EditerType', N'0')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.CardDtls@FocusField', N'BP.BM.CardDtls', N'FocusField', N'Title')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.CardDtls@IsEnableDouclickGlo', N'BP.BM.CardDtls', N'IsEnableDouclickGlo', N'1')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.CardDtls@IsEnableFocusField', N'BP.BM.CardDtls', N'IsEnableFocusField', N'1')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.CardDtls@IsEnableOpenICON', N'BP.BM.CardDtls', N'IsEnableOpenICON', N'1')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.CardDtls@IsEnableRefFunc', N'BP.BM.CardDtls', N'IsEnableRefFunc', N'1')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.CardDtls@UIRowStyleGlo', N'BP.BM.CardDtls', N'UIRowStyleGlo', N'2')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.CardDtls@WinCardH', N'BP.BM.CardDtls', N'WinCardH', N'480')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.CardDtls@WinCardW', N'BP.BM.CardDtls', N'WinCardW', N'820')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.CRJDtls@EditerType', N'BP.BM.CRJDtls', N'EditerType', N'0')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.CRJDtls@FocusField', N'BP.BM.CRJDtls', N'FocusField', N'Title')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.CRJDtls@IsEnableDouclickGlo', N'BP.BM.CRJDtls', N'IsEnableDouclickGlo', N'1')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.CRJDtls@IsEnableFocusField', N'BP.BM.CRJDtls', N'IsEnableFocusField', N'1')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.CRJDtls@IsEnableOpenICON', N'BP.BM.CRJDtls', N'IsEnableOpenICON', N'1')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.CRJDtls@IsEnableRefFunc', N'BP.BM.CRJDtls', N'IsEnableRefFunc', N'1')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.CRJDtls@UIRowStyleGlo', N'BP.BM.CRJDtls', N'UIRowStyleGlo', N'2')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.CRJDtls@WinCardH', N'BP.BM.CRJDtls', N'WinCardH', N'480')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.CRJDtls@WinCardW', N'BP.BM.CRJDtls', N'WinCardW', N'820')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.EduDtls@EditerType', N'BP.BM.EduDtls', N'EditerType', N'0')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.EduDtls@FocusField', N'BP.BM.EduDtls', N'FocusField', N'Title')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.EduDtls@IsEnableDouclickGlo', N'BP.BM.EduDtls', N'IsEnableDouclickGlo', N'1')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.EduDtls@IsEnableFocusField', N'BP.BM.EduDtls', N'IsEnableFocusField', N'1')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.EduDtls@IsEnableOpenICON', N'BP.BM.EduDtls', N'IsEnableOpenICON', N'1')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.EduDtls@IsEnableRefFunc', N'BP.BM.EduDtls', N'IsEnableRefFunc', N'1')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.EduDtls@UIRowStyleGlo', N'BP.BM.EduDtls', N'UIRowStyleGlo', N'2')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.EduDtls@WinCardH', N'BP.BM.EduDtls', N'WinCardH', N'480')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.EduDtls@WinCardW', N'BP.BM.EduDtls', N'WinCardW', N'820')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.Emps@EditerType', N'BP.BM.Emps', N'EditerType', N'0')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.Emps@FocusField', N'BP.BM.Emps', N'FocusField', N'Name')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.Emps@IsEnableDouclickGlo', N'BP.BM.Emps', N'IsEnableDouclickGlo', N'1')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.Emps@IsEnableFocusField', N'BP.BM.Emps', N'IsEnableFocusField', N'1')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.Emps@IsEnableOpenICON', N'BP.BM.Emps', N'IsEnableOpenICON', N'1')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.Emps@IsEnableRefFunc', N'BP.BM.Emps', N'IsEnableRefFunc', N'1')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.Emps@UIRowStyleGlo', N'BP.BM.Emps', N'UIRowStyleGlo', N'2')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.Emps@WinCardH', N'BP.BM.Emps', N'WinCardH', N'480')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.Emps@WinCardW', N'BP.BM.Emps', N'WinCardW', N'820')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.HuiYiDtls@EditerType', N'BP.BM.HuiYiDtls', N'EditerType', N'0')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.HuiYiDtls@FocusField', N'BP.BM.HuiYiDtls', N'FocusField', N'Title')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.HuiYiDtls@IsEnableDouclickGlo', N'BP.BM.HuiYiDtls', N'IsEnableDouclickGlo', N'1')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.HuiYiDtls@IsEnableFocusField', N'BP.BM.HuiYiDtls', N'IsEnableFocusField', N'1')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.HuiYiDtls@IsEnableOpenICON', N'BP.BM.HuiYiDtls', N'IsEnableOpenICON', N'1')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.HuiYiDtls@IsEnableRefFunc', N'BP.BM.HuiYiDtls', N'IsEnableRefFunc', N'1')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.HuiYiDtls@UIRowStyleGlo', N'BP.BM.HuiYiDtls', N'UIRowStyleGlo', N'2')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.HuiYiDtls@WinCardH', N'BP.BM.HuiYiDtls', N'WinCardH', N'480')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.HuiYiDtls@WinCardW', N'BP.BM.HuiYiDtls', N'WinCardW', N'820')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.JSJs@EditerType', N'BP.BM.JSJs', N'EditerType', N'0')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.JSJs@FocusField', N'BP.BM.JSJs', N'FocusField', N'Name')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.JSJs@IsEnableDouclickGlo', N'BP.BM.JSJs', N'IsEnableDouclickGlo', N'1')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.JSJs@IsEnableFocusField', N'BP.BM.JSJs', N'IsEnableFocusField', N'1')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.JSJs@IsEnableOpenICON', N'BP.BM.JSJs', N'IsEnableOpenICON', N'1')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.JSJs@IsEnableRefFunc', N'BP.BM.JSJs', N'IsEnableRefFunc', N'1')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.JSJs@UIRowStyleGlo', N'BP.BM.JSJs', N'UIRowStyleGlo', N'2')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.JSJs@WinCardH', N'BP.BM.JSJs', N'WinCardH', N'480')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.JSJs@WinCardW', N'BP.BM.JSJs', N'WinCardW', N'820')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.SBDtls@EditerType', N'BP.BM.SBDtls', N'EditerType', N'0')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.SBDtls@FocusField', N'BP.BM.SBDtls', N'FocusField', N'Title')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.SBDtls@IsEnableDouclickGlo', N'BP.BM.SBDtls', N'IsEnableDouclickGlo', N'1')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.SBDtls@IsEnableFocusField', N'BP.BM.SBDtls', N'IsEnableFocusField', N'1')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.SBDtls@IsEnableOpenICON', N'BP.BM.SBDtls', N'IsEnableOpenICON', N'1')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.SBDtls@IsEnableRefFunc', N'BP.BM.SBDtls', N'IsEnableRefFunc', N'1')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.SBDtls@UIRowStyleGlo', N'BP.BM.SBDtls', N'UIRowStyleGlo', N'2')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.SBDtls@WinCardH', N'BP.BM.SBDtls', N'WinCardH', N'480')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.SBDtls@WinCardW', N'BP.BM.SBDtls', N'WinCardW', N'820')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.SFWDtls@FocusField', N'BP.BM.SFWDtls', N'FocusField', N'Title')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.SFWDtls@IsEnableDouclickGlo', N'BP.BM.SFWDtls', N'IsEnableDouclickGlo', N'1')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.SFWDtls@IsEnableFocusField', N'BP.BM.SFWDtls', N'IsEnableFocusField', N'1')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.SFWDtls@IsEnableOpenICON', N'BP.BM.SFWDtls', N'IsEnableOpenICON', N'1')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.SFWDtls@IsEnableRefFunc', N'BP.BM.SFWDtls', N'IsEnableRefFunc', N'1')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.SFWDtls@UIRowStyleGlo', N'BP.BM.SFWDtls', N'UIRowStyleGlo', N'2')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.SFWDtls@WinCardH', N'BP.BM.SFWDtls', N'WinCardH', N'480')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.SFWDtls@WinCardW', N'BP.BM.SFWDtls', N'WinCardW', N'820')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.USBs@EditerType', N'BP.BM.USBs', N'EditerType', N'0')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.USBs@FocusField', N'BP.BM.USBs', N'FocusField', N'Name')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.USBs@IsEnableDouclickGlo', N'BP.BM.USBs', N'IsEnableDouclickGlo', N'1')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.USBs@IsEnableFocusField', N'BP.BM.USBs', N'IsEnableFocusField', N'1')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.USBs@IsEnableOpenICON', N'BP.BM.USBs', N'IsEnableOpenICON', N'1')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.USBs@IsEnableRefFunc', N'BP.BM.USBs', N'IsEnableRefFunc', N'1')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.USBs@UIRowStyleGlo', N'BP.BM.USBs', N'UIRowStyleGlo', N'2')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.USBs@WinCardH', N'BP.BM.USBs', N'WinCardH', N'480')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.USBs@WinCardW', N'BP.BM.USBs', N'WinCardW', N'820')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.WLJFs@EditerType', N'BP.BM.WLJFs', N'EditerType', N'0')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.WLJFs@FocusField', N'BP.BM.WLJFs', N'FocusField', N'Name')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.WLJFs@IsEnableDouclickGlo', N'BP.BM.WLJFs', N'IsEnableDouclickGlo', N'1')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.WLJFs@IsEnableFocusField', N'BP.BM.WLJFs', N'IsEnableFocusField', N'1')
GO
print 'Processed 100 total records'
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.WLJFs@IsEnableOpenICON', N'BP.BM.WLJFs', N'IsEnableOpenICON', N'1')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.WLJFs@IsEnableRefFunc', N'BP.BM.WLJFs', N'IsEnableRefFunc', N'1')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.WLJFs@UIRowStyleGlo', N'BP.BM.WLJFs', N'UIRowStyleGlo', N'2')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.WLJFs@WinCardH', N'BP.BM.WLJFs', N'WinCardH', N'480')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.WLJFs@WinCardW', N'BP.BM.WLJFs', N'WinCardW', N'820')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.XCBDDtls@EditerType', N'BP.BM.XCBDDtls', N'EditerType', N'0')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.XCBDDtls@FocusField', N'BP.BM.XCBDDtls', N'FocusField', N'Title')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.XCBDDtls@IsEnableDouclickGlo', N'BP.BM.XCBDDtls', N'IsEnableDouclickGlo', N'1')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.XCBDDtls@IsEnableFocusField', N'BP.BM.XCBDDtls', N'IsEnableFocusField', N'1')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.XCBDDtls@IsEnableOpenICON', N'BP.BM.XCBDDtls', N'IsEnableOpenICON', N'1')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.XCBDDtls@IsEnableRefFunc', N'BP.BM.XCBDDtls', N'IsEnableRefFunc', N'1')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.XCBDDtls@UIRowStyleGlo', N'BP.BM.XCBDDtls', N'UIRowStyleGlo', N'2')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.XCBDDtls@WinCardH', N'BP.BM.XCBDDtls', N'WinCardH', N'480')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.XCBDDtls@WinCardW', N'BP.BM.XCBDDtls', N'WinCardW', N'820')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.ZDSorts@FocusField', N'BP.BM.ZDSorts', N'FocusField', N'Name')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.ZDSorts@IsEnableDouclickGlo', N'BP.BM.ZDSorts', N'IsEnableDouclickGlo', N'1')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.ZDSorts@IsEnableFocusField', N'BP.BM.ZDSorts', N'IsEnableFocusField', N'1')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.ZDSorts@IsEnableOpenICON', N'BP.BM.ZDSorts', N'IsEnableOpenICON', N'1')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.ZDSorts@IsEnableRefFunc', N'BP.BM.ZDSorts', N'IsEnableRefFunc', N'1')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.ZDSorts@UIRowStyleGlo', N'BP.BM.ZDSorts', N'UIRowStyleGlo', N'2')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.ZDSorts@WinCardH', N'BP.BM.ZDSorts', N'WinCardH', N'480')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.ZDSorts@WinCardW', N'BP.BM.ZDSorts', N'WinCardW', N'820')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.ZhiDus@EditerType', N'BP.BM.ZhiDus', N'EditerType', N'0')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.ZhiDus@FocusField', N'BP.BM.ZhiDus', N'FocusField', N'Name')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.ZhiDus@IsEnableDouclickGlo', N'BP.BM.ZhiDus', N'IsEnableDouclickGlo', N'1')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.ZhiDus@IsEnableFocusField', N'BP.BM.ZhiDus', N'IsEnableFocusField', N'1')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.ZhiDus@IsEnableOpenICON', N'BP.BM.ZhiDus', N'IsEnableOpenICON', N'1')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.ZhiDus@IsEnableRefFunc', N'BP.BM.ZhiDus', N'IsEnableRefFunc', N'1')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.ZhiDus@UIRowStyleGlo', N'BP.BM.ZhiDus', N'UIRowStyleGlo', N'2')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.ZhiDus@WinCardH', N'BP.BM.ZhiDus', N'WinCardH', N'480')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.BM.ZhiDus@WinCardW', N'BP.BM.ZhiDus', N'WinCardW', N'820')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.DS.DSBuys@IsEnableDouclickGlo', N'BP.DS.DSBuys', N'IsEnableDouclickGlo', N'')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.DS.DSBuys@IsEnableFocusField', N'BP.DS.DSBuys', N'IsEnableFocusField', N'')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.DS.DSBuys@IsEnableOpenICON', N'BP.DS.DSBuys', N'IsEnableOpenICON', N'')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.DS.DSBuys@IsEnableRefFunc', N'BP.DS.DSBuys', N'IsEnableRefFunc', N'')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.DS.DSBuys@UIRowStyleGlo', N'BP.DS.DSBuys', N'UIRowStyleGlo', N'')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.DS.DSBuys@WinCardH', N'BP.DS.DSBuys', N'WinCardH', N'')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.DS.DSBuys@WinCardW', N'BP.DS.DSBuys', N'WinCardW', N'')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.DS.DSMains@IsEnableDouclickGlo', N'BP.DS.DSMains', N'IsEnableDouclickGlo', N'')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.DS.DSMains@IsEnableFocusField', N'BP.DS.DSMains', N'IsEnableFocusField', N'')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.DS.DSMains@IsEnableOpenICON', N'BP.DS.DSMains', N'IsEnableOpenICON', N'')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.DS.DSMains@IsEnableRefFunc', N'BP.DS.DSMains', N'IsEnableRefFunc', N'')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.DS.DSMains@UIRowStyleGlo', N'BP.DS.DSMains', N'UIRowStyleGlo', N'')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.DS.DSMains@WinCardH', N'BP.DS.DSMains', N'WinCardH', N'')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.DS.DSMains@WinCardW', N'BP.DS.DSMains', N'WinCardW', N'')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.DS.DSSorts@WinCardH', N'BP.DS.DSSorts', N'WinCardH', N'')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.DS.DSSorts@WinCardW', N'BP.DS.DSSorts', N'WinCardW', N'')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.DS.DSTakes@IsEnableDouclickGlo', N'BP.DS.DSTakes', N'IsEnableDouclickGlo', N'')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.DS.DSTakes@IsEnableFocusField', N'BP.DS.DSTakes', N'IsEnableFocusField', N'')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.DS.DSTakes@IsEnableOpenICON', N'BP.DS.DSTakes', N'IsEnableOpenICON', N'')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.DS.DSTakes@IsEnableRefFunc', N'BP.DS.DSTakes', N'IsEnableRefFunc', N'')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.DS.DSTakes@UIRowStyleGlo', N'BP.DS.DSTakes', N'UIRowStyleGlo', N'')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.DS.DSTakes@WinCardH', N'BP.DS.DSTakes', N'WinCardH', N'')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.DS.DSTakes@WinCardW', N'BP.DS.DSTakes', N'WinCardW', N'')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.OA.Article.ArticleCatagorys@IsEnableDouclickGlo', N'BP.OA.Article.ArticleCatagorys', N'IsEnableDouclickGlo', N'')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.OA.Article.ArticleCatagorys@IsEnableFocusField', N'BP.OA.Article.ArticleCatagorys', N'IsEnableFocusField', N'')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.OA.Article.ArticleCatagorys@IsEnableOpenICON', N'BP.OA.Article.ArticleCatagorys', N'IsEnableOpenICON', N'')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.OA.Article.ArticleCatagorys@IsEnableRefFunc', N'BP.OA.Article.ArticleCatagorys', N'IsEnableRefFunc', N'')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.OA.Article.ArticleCatagorys@UIRowStyleGlo', N'BP.OA.Article.ArticleCatagorys', N'UIRowStyleGlo', N'')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.OA.Article.ArticleCatagorys@WinCardH', N'BP.OA.Article.ArticleCatagorys', N'WinCardH', N'')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.OA.Article.ArticleCatagorys@WinCardW', N'BP.OA.Article.ArticleCatagorys', N'WinCardW', N'')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.Pub.NDs@FocusField', N'BP.Pub.NDs', N'FocusField', N'Name')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.Pub.NDs@IsEnableDouclickGlo', N'BP.Pub.NDs', N'IsEnableDouclickGlo', N'1')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.Pub.NDs@IsEnableFocusField', N'BP.Pub.NDs', N'IsEnableFocusField', N'1')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.Pub.NDs@IsEnableOpenICON', N'BP.Pub.NDs', N'IsEnableOpenICON', N'1')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.Pub.NDs@IsEnableRefFunc', N'BP.Pub.NDs', N'IsEnableRefFunc', N'1')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.Pub.NDs@UIRowStyleGlo', N'BP.Pub.NDs', N'UIRowStyleGlo', N'2')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.Pub.NDs@WinCardH', N'BP.Pub.NDs', N'WinCardH', N'480')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.Pub.NDs@WinCardW', N'BP.Pub.NDs', N'WinCardW', N'820')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.WF.Template.Ext.FlowSheets@EditerType', N'BP.WF.Template.Ext.FlowSheets', N'EditerType', N'0')
INSERT [dbo].[Sys_EnsAppCfg] ([MyPK], [EnsName], [CfgKey], [CfgVal]) VALUES (N'BP.WF.Template.Selectors@EditerType', N'BP.WF.Template.Selectors', N'EditerType', N'0')
/****** Object:  Table [dbo].[Sys_EnCfg]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sys_EnCfg](
	[No] [nvarchar](100) NOT NULL,
	[GroupTitle] [nvarchar](2000) NULL,
	[FJSavePath] [nvarchar](100) NULL,
	[FJWebPath] [nvarchar](100) NULL,
	[Datan] [nvarchar](200) NULL,
 CONSTRAINT [Sys_EnCfgpk] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Sys_EnCfg] ([No], [GroupTitle], [FJSavePath], [FJWebPath], [Datan]) VALUES (N'BP.BM.Emp', N'@No=基本配置@GA_QFD=港澳通行证,最多支持3个通行证的输入.@TA_QFD=台湾通行证,最多支持3个通行证的输入.@HZ_QFD=因私护照,最多支持3个通行证的输入.', NULL, NULL, NULL)
INSERT [dbo].[Sys_EnCfg] ([No], [GroupTitle], [FJSavePath], [FJWebPath], [Datan]) VALUES (N'BP.Sys.MapDataExt', N'@No=基本属性@Designer=设计者信息', NULL, NULL, NULL)
INSERT [dbo].[Sys_EnCfg] ([No], [GroupTitle], [FJSavePath], [FJWebPath], [Datan]) VALUES (N'BP.WF.Template.Ext.FlowSheet', N'@No=基本配置@FlowRunWay=启动方式,配置工作流程如何自动发起，该选项要与流程服务一起工作才有效.@StartLimitRole=启动限制规则@StartGuideWay=发起前置导航@CFlowWay=延续流程@DTSWay=流程数据与业务数据同步', NULL, NULL, NULL)
INSERT [dbo].[Sys_EnCfg] ([No], [GroupTitle], [FJSavePath], [FJWebPath], [Datan]) VALUES (N'BP.WF.Template.Ext.NodeSheet', N'@NodeID=基本配置@FormType=表单@FWCSta=审核组件,适用于sdk表单审核组件与ccform上的审核组件属性设置.@SendLab=按钮权限,控制工作节点可操作按钮.@RunModel=运行模式,分合流,父子流程@AutoJumpRole0=跳转,自动跳转规则当遇到该节点时如何让其自动的执行下一步.@MPhone_WorkModel=移动,与手机平板电脑相关的应用设置.@WarningDays=考核,时效考核,质量考核.@OfficeOpenLab=公文按钮,只有当该节点是公文流程时候有效', NULL, NULL, NULL)
/****** Object:  Table [dbo].[Sys_Domain]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sys_Domain](
	[No] [nvarchar](30) NOT NULL,
	[Name] [nvarchar](30) NULL,
	[DBLink] [nvarchar](130) NULL,
 CONSTRAINT [Sys_Domainpk] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Sys_DocFile]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sys_DocFile](
	[MyPK] [nvarchar](100) NOT NULL,
	[FileName] [nvarchar](200) NULL,
	[FileSize] [int] NULL,
	[FileType] [nvarchar](50) NULL,
	[D1] [nvarchar](4000) NULL,
	[D2] [nvarchar](4000) NULL,
	[D3] [nvarchar](4000) NULL,
	[D4] [nvarchar](4000) NULL,
	[D5] [nvarchar](4000) NULL,
	[D6] [nvarchar](4000) NULL,
	[D7] [nvarchar](4000) NULL,
	[D8] [nvarchar](4000) NULL,
	[D9] [nvarchar](4000) NULL,
	[D10] [nvarchar](4000) NULL,
	[D11] [nvarchar](4000) NULL,
	[D12] [nvarchar](4000) NULL,
	[D13] [nvarchar](4000) NULL,
	[D14] [nvarchar](4000) NULL,
	[D15] [nvarchar](4000) NULL,
	[D16] [nvarchar](4000) NULL,
	[D17] [nvarchar](4000) NULL,
	[D18] [nvarchar](4000) NULL,
	[D19] [nvarchar](4000) NULL,
	[D20] [nvarchar](4000) NULL,
 CONSTRAINT [Sys_DocFilepk] PRIMARY KEY CLUSTERED 
(
	[MyPK] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Sys_DefVal]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sys_DefVal](
	[No] [nvarchar](50) NOT NULL,
	[ParentNo] [nvarchar](50) NULL,
	[IsParent] [int] NULL,
	[WordsSort] [int] NULL,
	[FK_MapData] [nvarchar](50) NULL,
	[NodeAttrKey] [nvarchar](50) NULL,
	[IsHisWords] [int] NULL,
	[FK_Emp] [nvarchar](100) NULL,
	[CurValue] [nvarchar](4000) NULL,
 CONSTRAINT [Sys_DefValpk] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Sys_DataRpt]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sys_DataRpt](
	[MyPK] [nvarchar](100) NOT NULL,
	[ColCount] [nvarchar](50) NULL,
	[RowCount] [nvarchar](50) NULL,
	[Val] [float] NULL,
	[RefOID] [float] NULL,
 CONSTRAINT [Sys_DataRptpk] PRIMARY KEY CLUSTERED 
(
	[MyPK] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Sys_Contrast]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sys_Contrast](
	[MyPK] [nvarchar](100) NOT NULL,
	[ContrastKey] [nvarchar](20) NULL,
	[KeyVal1] [nvarchar](20) NULL,
	[KeyVal2] [nvarchar](20) NULL,
	[SortBy] [nvarchar](20) NULL,
	[KeyOfNum] [nvarchar](20) NULL,
	[GroupWay] [int] NULL,
	[OrderWay] [int] NULL,
 CONSTRAINT [Sys_Contrastpk] PRIMARY KEY CLUSTERED 
(
	[MyPK] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Sys_CField]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sys_CField](
	[EnsName] [nvarchar](100) NOT NULL,
	[FK_Emp] [nvarchar](100) NOT NULL,
	[Attrs] [nvarchar](4000) NULL,
 CONSTRAINT [Sys_CFieldpk] PRIMARY KEY CLUSTERED 
(
	[EnsName] ASC,
	[FK_Emp] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[sch]    Script Date: 07/06/2015 09:28:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sch]
   @TableName varchar(50),
   @Cols      int
AS
BEGIN
   if(@Cols<=0)return;
   
   declare @col nvarchar(500);   
   set @col='';
   if(@Cols & 1 = 1) set @col=@col+',name';
   if(@Cols & 2 = 2) set @col=@col+',''ui_''+name as UI变量';
   if(@Cols & 4 = 4) set @col=@col+',''<asp:TextBox ID="ui_''+name+''" runat="server" Width="97%"></asp:TextBox>'' as UI控件';
   if(@Cols & 8 = 8) set @col=@col+',''String ''+name+'' = this.ui_''+name+''.Text;'' as 获取变量';
   if(@Cols & 16 = 16) set @col=@col+','''+@TableName+'''+''.''+name+'' = ''+name+'';'' as 对象属性赋值';
   if(@Cols & 32 = 32) set @col=@col+',''this.ui_''+name+''.Text = ''+'''+@TableName+'''+''.''+name+'';'' as 控件赋值';
 

   declare @sql nvarchar(2000);
   set @sql='select '+RIGHT(@col,LEN(@col)-1);
   set @sql=@sql+' from syscolumns ';
   set @sql=@sql+' where id=(select id from sysobjects where name='''+@TableName+''');';
   exec(@sql);
END
GO
/****** Object:  Table [dbo].[Pub_YF]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Pub_YF](
	[No] [nvarchar](30) NOT NULL,
	[Name] [nvarchar](60) NULL,
 CONSTRAINT [Pub_YFpk] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Pub_NY]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Pub_NY](
	[No] [nvarchar](30) NOT NULL,
	[Name] [nvarchar](60) NULL,
 CONSTRAINT [Pub_NYpk] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Pub_ND]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Pub_ND](
	[No] [nvarchar](30) NOT NULL,
	[Name] [nvarchar](60) NULL,
 CONSTRAINT [Pub_NDpk] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Pub_ND] ([No], [Name]) VALUES (N'001', N'2015')
INSERT [dbo].[Pub_ND] ([No], [Name]) VALUES (N'002', N'2016')
/****** Object:  Table [dbo].[Prj_Prj]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Prj_Prj](
	[No] [nvarchar](4) NOT NULL,
	[Name] [nvarchar](60) NULL,
 CONSTRAINT [Prj_Prjpk] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PRJ_FileData]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PRJ_FileData](
	[My_PK] [nvarchar](10) NOT NULL,
	[OID] [int] NULL,
	[FileName] [nvarchar](60) NULL,
	[AbsolutionPath] [nvarchar](60) NULL,
	[FileFormat] [nvarchar](60) NULL,
	[FileSize] [nvarchar](60) NULL,
	[UpLoadDate] [nvarchar](50) NULL,
	[UpLoadPerson] [nvarchar](60) NULL,
	[NodeAttrbute] [nvarchar](60) NULL,
	[FlowID] [int] NULL,
 CONSTRAINT [PRJ_FileDatapk] PRIMARY KEY CLUSTERED 
(
	[My_PK] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Prj_EmpPrjStation]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Prj_EmpPrjStation](
	[FK_EmpPrj] [nvarchar](20) NOT NULL,
	[FK_Station] [nvarchar](100) NOT NULL,
	[FK_Emp] [nvarchar](20) NULL,
	[FK_Prj] [nvarchar](20) NULL,
 CONSTRAINT [Prj_EmpPrjStationpk] PRIMARY KEY CLUSTERED 
(
	[FK_EmpPrj] ASC,
	[FK_Station] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Prj_EmpPrj]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Prj_EmpPrj](
	[MyPK] [nvarchar](20) NULL,
	[Name] [nvarchar](3000) NULL,
	[FK_Emp] [nvarchar](100) NOT NULL,
	[FK_Prj] [nvarchar](100) NOT NULL,
	[StationStrs] [nvarchar](4000) NULL,
 CONSTRAINT [Prj_EmpPrjpk] PRIMARY KEY CLUSTERED 
(
	[FK_Emp] ASC,
	[FK_Prj] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Port_StationType]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Port_StationType](
	[No] [nvarchar](2) NOT NULL,
	[Name] [nvarchar](50) NULL,
 CONSTRAINT [Port_StationTypepk] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Port_StationType] ([No], [Name]) VALUES (N'1', N'高层')
INSERT [dbo].[Port_StationType] ([No], [Name]) VALUES (N'2', N'中层')
INSERT [dbo].[Port_StationType] ([No], [Name]) VALUES (N'3', N'基层')
/****** Object:  Table [dbo].[Port_Station]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Port_Station](
	[No] [nvarchar](20) NOT NULL,
	[Name] [nvarchar](100) NULL,
	[DutyReq] [nvarchar](4000) NULL,
	[Makings] [nvarchar](4000) NULL,
	[FK_StationType] [nvarchar](100) NULL,
	[StaGrade] [int] NULL,
 CONSTRAINT [Port_Stationpk] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Port_Station] ([No], [Name], [DutyReq], [Makings], [FK_StationType], [StaGrade]) VALUES (N'01', N'总经理', NULL, NULL, N'1', 0)
INSERT [dbo].[Port_Station] ([No], [Name], [DutyReq], [Makings], [FK_StationType], [StaGrade]) VALUES (N'02', N'市场部经理', NULL, NULL, N'2', 0)
INSERT [dbo].[Port_Station] ([No], [Name], [DutyReq], [Makings], [FK_StationType], [StaGrade]) VALUES (N'03', N'研发部经理', NULL, NULL, N'2', 0)
INSERT [dbo].[Port_Station] ([No], [Name], [DutyReq], [Makings], [FK_StationType], [StaGrade]) VALUES (N'04', N'客服部经理', NULL, NULL, N'2', 0)
INSERT [dbo].[Port_Station] ([No], [Name], [DutyReq], [Makings], [FK_StationType], [StaGrade]) VALUES (N'05', N'财务部经理', NULL, NULL, N'2', 0)
INSERT [dbo].[Port_Station] ([No], [Name], [DutyReq], [Makings], [FK_StationType], [StaGrade]) VALUES (N'06', N'人力资源部经理', NULL, NULL, N'2', 0)
INSERT [dbo].[Port_Station] ([No], [Name], [DutyReq], [Makings], [FK_StationType], [StaGrade]) VALUES (N'07', N'销售人员岗', NULL, NULL, N'3', 0)
INSERT [dbo].[Port_Station] ([No], [Name], [DutyReq], [Makings], [FK_StationType], [StaGrade]) VALUES (N'08', N'程序员岗', NULL, NULL, N'3', 0)
INSERT [dbo].[Port_Station] ([No], [Name], [DutyReq], [Makings], [FK_StationType], [StaGrade]) VALUES (N'09', N'技术服务岗', NULL, NULL, N'3', 0)
INSERT [dbo].[Port_Station] ([No], [Name], [DutyReq], [Makings], [FK_StationType], [StaGrade]) VALUES (N'10', N'出纳岗', NULL, NULL, N'3', 0)
INSERT [dbo].[Port_Station] ([No], [Name], [DutyReq], [Makings], [FK_StationType], [StaGrade]) VALUES (N'11', N'人力资源助理岗', NULL, NULL, N'3', 0)
INSERT [dbo].[Port_Station] ([No], [Name], [DutyReq], [Makings], [FK_StationType], [StaGrade]) VALUES (N'12', N'外来人员岗', NULL, NULL, N'3', 0)
/****** Object:  Table [dbo].[Port_EmpStation]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Port_EmpStation](
	[FK_Emp] [nvarchar](15) NOT NULL,
	[FK_Station] [nvarchar](100) NOT NULL,
 CONSTRAINT [Port_EmpStationpk] PRIMARY KEY CLUSTERED 
(
	[FK_Emp] ASC,
	[FK_Station] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Port_EmpDept]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Port_EmpDept](
	[FK_Emp] [nvarchar](15) NOT NULL,
	[FK_Dept] [nvarchar](100) NOT NULL,
 CONSTRAINT [Port_EmpDeptpk] PRIMARY KEY CLUSTERED 
(
	[FK_Emp] ASC,
	[FK_Dept] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Port_Emp]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Port_Emp](
	[No] [nvarchar](20) NOT NULL,
	[EmpNo] [nvarchar](20) NULL,
	[Name] [nvarchar](200) NULL,
	[Pass] [nvarchar](100) NULL,
	[FK_Dept] [nvarchar](100) NULL,
	[FK_Duty] [nvarchar](20) NULL,
	[Leader] [nvarchar](50) NULL,
	[SID] [nvarchar](36) NULL,
	[Tel] [nvarchar](20) NULL,
	[Email] [nvarchar](100) NULL,
	[NumOfDept] [int] NULL,
	[Idx] [int] NULL,
 CONSTRAINT [Port_Emppk] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Port_Emp] ([No], [EmpNo], [Name], [Pass], [FK_Dept], [FK_Duty], [Leader], [SID], [Tel], [Email], [NumOfDept], [Idx]) VALUES (N'admin', NULL, N'admin', N'pub', N'100', N'01', N'admin', N'lsrhqxybhn5grfdu1uo2ctzb', N'0531-82374939', N'zhoupeng@ccflow.org', 1, NULL)
INSERT [dbo].[Port_Emp] ([No], [EmpNo], [Name], [Pass], [FK_Dept], [FK_Duty], [Leader], [SID], [Tel], [Email], [NumOfDept], [Idx]) VALUES (N'fuhui', NULL, N'福惠', N'pub', N'1003', N'04', N'guoxiangbin', NULL, N'0531-82374939', N'fuhui@ccflow.org', 1, NULL)
INSERT [dbo].[Port_Emp] ([No], [EmpNo], [Name], [Pass], [FK_Dept], [FK_Duty], [Leader], [SID], [Tel], [Email], [NumOfDept], [Idx]) VALUES (N'guobaogeng', NULL, N'郭宝庚', N'pub', N'1004', N'04', N'yangyilei', NULL, N'0531-82374939', N'guobaogeng@ccflow.org', 1, NULL)
INSERT [dbo].[Port_Emp] ([No], [EmpNo], [Name], [Pass], [FK_Dept], [FK_Duty], [Leader], [SID], [Tel], [Email], [NumOfDept], [Idx]) VALUES (N'guoxiangbin', NULL, N'郭祥斌', N'pub', N'1003', N'03', N'zhoupeng', NULL, N'0531-82374939', N'guoxiangbin@ccflow.org', 1, NULL)
INSERT [dbo].[Port_Emp] ([No], [EmpNo], [Name], [Pass], [FK_Dept], [FK_Duty], [Leader], [SID], [Tel], [Email], [NumOfDept], [Idx]) VALUES (N'liping', NULL, N'李萍', N'pub', N'1005', N'03', N'zhoupeng', NULL, N'0531-82374939', N'liping@ccflow.org', 1, NULL)
INSERT [dbo].[Port_Emp] ([No], [EmpNo], [Name], [Pass], [FK_Dept], [FK_Duty], [Leader], [SID], [Tel], [Email], [NumOfDept], [Idx]) VALUES (N'liyan', NULL, N'李言', N'pub', N'1005', N'04', N'liping', NULL, N'0531-82374939', N'liyan@ccflow.org', 1, NULL)
INSERT [dbo].[Port_Emp] ([No], [EmpNo], [Name], [Pass], [FK_Dept], [FK_Duty], [Leader], [SID], [Tel], [Email], [NumOfDept], [Idx]) VALUES (N'qifenglin', NULL, N'祁凤林', N'pub', N'1002', N'03', N'zhoupeng', NULL, N'0531-82374939', N'qifenglin@ccflow.org', 1, NULL)
INSERT [dbo].[Port_Emp] ([No], [EmpNo], [Name], [Pass], [FK_Dept], [FK_Duty], [Leader], [SID], [Tel], [Email], [NumOfDept], [Idx]) VALUES (N'yangyilei', NULL, N'杨依雷', N'pub', N'1004', N'03', N'zhoupeng', NULL, N'0531-82374939', N'yangyilei@ccflow.org', 1, NULL)
INSERT [dbo].[Port_Emp] ([No], [EmpNo], [Name], [Pass], [FK_Dept], [FK_Duty], [Leader], [SID], [Tel], [Email], [NumOfDept], [Idx]) VALUES (N'zhanghaicheng', NULL, N'张海成', N'pub', N'1001', N'03', N'zhoupeng', NULL, N'0531-82374939', N'zhanghaicheng@ccflow.org', 1, NULL)
INSERT [dbo].[Port_Emp] ([No], [EmpNo], [Name], [Pass], [FK_Dept], [FK_Duty], [Leader], [SID], [Tel], [Email], [NumOfDept], [Idx]) VALUES (N'zhangyifan', NULL, N'张一帆', N'pub', N'1001', N'04', N'zhanghaicheng', NULL, N'0531-82374939', N'zhangyifan@ccflow.org', 1, NULL)
INSERT [dbo].[Port_Emp] ([No], [EmpNo], [Name], [Pass], [FK_Dept], [FK_Duty], [Leader], [SID], [Tel], [Email], [NumOfDept], [Idx]) VALUES (N'zhoupeng', NULL, N'周朋', N'pub', N'100', N'02', N'admin', N'fhtbmbh205dcfyxwrjtdxpre', N'0531-82374939', N'zhoupeng@ccflow.org', 1, NULL)
INSERT [dbo].[Port_Emp] ([No], [EmpNo], [Name], [Pass], [FK_Dept], [FK_Duty], [Leader], [SID], [Tel], [Email], [NumOfDept], [Idx]) VALUES (N'zhoushengyu', NULL, N'周升雨', N'pub', N'1001', N'04', N'zhanghaicheng', NULL, N'0531-82374939', N'zhoushengyu@ccflow.org', 1, NULL)
INSERT [dbo].[Port_Emp] ([No], [EmpNo], [Name], [Pass], [FK_Dept], [FK_Duty], [Leader], [SID], [Tel], [Email], [NumOfDept], [Idx]) VALUES (N'zhoutianjiao', NULL, N'周天娇', N'pub', N'1002', N'04', N'qifenglin', NULL, N'0531-82374939', N'zhoutianjiao@ccflow.org', 1, NULL)
/****** Object:  Table [dbo].[Port_Duty]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Port_Duty](
	[No] [nvarchar](2) NOT NULL,
	[Name] [nvarchar](50) NULL,
 CONSTRAINT [Port_Dutypk] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Port_Duty] ([No], [Name]) VALUES (N'01', N'董事长')
INSERT [dbo].[Port_Duty] ([No], [Name]) VALUES (N'02', N'总经理')
INSERT [dbo].[Port_Duty] ([No], [Name]) VALUES (N'03', N'科长')
INSERT [dbo].[Port_Duty] ([No], [Name]) VALUES (N'04', N'科员')
INSERT [dbo].[Port_Duty] ([No], [Name]) VALUES (N'05', N'分公司总经理')
INSERT [dbo].[Port_Duty] ([No], [Name]) VALUES (N'20', N'其他')
/****** Object:  Table [dbo].[Port_DeptType]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Port_DeptType](
	[No] [nvarchar](2) NOT NULL,
	[Name] [nvarchar](50) NULL,
 CONSTRAINT [Port_DeptTypepk] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Port_DeptType] ([No], [Name]) VALUES (N'01', N'集团')
INSERT [dbo].[Port_DeptType] ([No], [Name]) VALUES (N'02', N'集团部门')
INSERT [dbo].[Port_DeptType] ([No], [Name]) VALUES (N'03', N'分公司')
INSERT [dbo].[Port_DeptType] ([No], [Name]) VALUES (N'04', N'分公司部门')
/****** Object:  Table [dbo].[Port_DeptStation]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Port_DeptStation](
	[FK_Dept] [nvarchar](15) NOT NULL,
	[FK_Station] [nvarchar](100) NOT NULL,
 CONSTRAINT [Port_DeptStationpk] PRIMARY KEY CLUSTERED 
(
	[FK_Dept] ASC,
	[FK_Station] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Port_DeptStation] ([FK_Dept], [FK_Station]) VALUES (N'100', N'01')
INSERT [dbo].[Port_DeptStation] ([FK_Dept], [FK_Station]) VALUES (N'1001', N'07')
INSERT [dbo].[Port_DeptStation] ([FK_Dept], [FK_Station]) VALUES (N'1002', N'08')
INSERT [dbo].[Port_DeptStation] ([FK_Dept], [FK_Station]) VALUES (N'1003', N'09')
INSERT [dbo].[Port_DeptStation] ([FK_Dept], [FK_Station]) VALUES (N'1004', N'10')
INSERT [dbo].[Port_DeptStation] ([FK_Dept], [FK_Station]) VALUES (N'1005', N'11')
/****** Object:  Table [dbo].[Port_DeptSearchScorp]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Port_DeptSearchScorp](
	[FK_Emp] [nvarchar](50) NOT NULL,
	[FK_Dept] [nvarchar](100) NOT NULL,
 CONSTRAINT [Port_DeptSearchScorppk] PRIMARY KEY CLUSTERED 
(
	[FK_Emp] ASC,
	[FK_Dept] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Port_DeptEmpStation]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Port_DeptEmpStation](
	[MyPK] [nvarchar](100) NOT NULL,
	[FK_Dept] [nvarchar](50) NULL,
	[FK_Station] [nvarchar](50) NULL,
	[FK_Emp] [nvarchar](50) NULL,
 CONSTRAINT [Port_DeptEmpStationpk] PRIMARY KEY CLUSTERED 
(
	[MyPK] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Port_DeptEmpStation] ([MyPK], [FK_Dept], [FK_Station], [FK_Emp]) VALUES (N'100_zhoupeng_01', N'100', N'01', N'zhoupeng')
INSERT [dbo].[Port_DeptEmpStation] ([MyPK], [FK_Dept], [FK_Station], [FK_Emp]) VALUES (N'1001_zhanghaicheng_02', N'1001', N'02', N'zhanghaicheng')
INSERT [dbo].[Port_DeptEmpStation] ([MyPK], [FK_Dept], [FK_Station], [FK_Emp]) VALUES (N'1001_zhangyifan_07', N'1001', N'07', N'zhangyifan')
INSERT [dbo].[Port_DeptEmpStation] ([MyPK], [FK_Dept], [FK_Station], [FK_Emp]) VALUES (N'1001_zhoushengyu_07', N'1001', N'07', N'zhoushengyu')
INSERT [dbo].[Port_DeptEmpStation] ([MyPK], [FK_Dept], [FK_Station], [FK_Emp]) VALUES (N'1002_qifenglin_03', N'1002', N'03', N'qifenglin')
INSERT [dbo].[Port_DeptEmpStation] ([MyPK], [FK_Dept], [FK_Station], [FK_Emp]) VALUES (N'1002_zhoutianjiao_08', N'1002', N'08', N'zhoutianjiao')
INSERT [dbo].[Port_DeptEmpStation] ([MyPK], [FK_Dept], [FK_Station], [FK_Emp]) VALUES (N'1003_fuhui_09', N'1003', N'09', N'fuhui')
INSERT [dbo].[Port_DeptEmpStation] ([MyPK], [FK_Dept], [FK_Station], [FK_Emp]) VALUES (N'1003_guoxiangbin_04', N'1003', N'04', N'guoxiangbin')
INSERT [dbo].[Port_DeptEmpStation] ([MyPK], [FK_Dept], [FK_Station], [FK_Emp]) VALUES (N'1004_guobaogeng_10', N'1004', N'10', N'guobaogeng')
INSERT [dbo].[Port_DeptEmpStation] ([MyPK], [FK_Dept], [FK_Station], [FK_Emp]) VALUES (N'1004_yangyilei_05', N'1004', N'05', N'yangyilei')
INSERT [dbo].[Port_DeptEmpStation] ([MyPK], [FK_Dept], [FK_Station], [FK_Emp]) VALUES (N'1005_liping_06', N'1005', N'06', N'liping')
INSERT [dbo].[Port_DeptEmpStation] ([MyPK], [FK_Dept], [FK_Station], [FK_Emp]) VALUES (N'1005_liyan_11', N'1005', N'11', N'liyan')
INSERT [dbo].[Port_DeptEmpStation] ([MyPK], [FK_Dept], [FK_Station], [FK_Emp]) VALUES (N'1099_Guest_12', N'1005', N'12', N'Guest')
/****** Object:  Table [dbo].[Port_DeptEmp]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Port_DeptEmp](
	[MyPK] [nvarchar](100) NOT NULL,
	[FK_Emp] [nvarchar](50) NULL,
	[FK_Dept] [nvarchar](50) NULL,
	[FK_Duty] [nvarchar](50) NULL,
	[DutyLevel] [int] NULL,
	[Leader] [nvarchar](50) NULL,
 CONSTRAINT [Port_DeptEmppk] PRIMARY KEY CLUSTERED 
(
	[MyPK] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Port_DeptEmp] ([MyPK], [FK_Emp], [FK_Dept], [FK_Duty], [DutyLevel], [Leader]) VALUES (N'100_zhoupeng', N'zhoupeng', N'100', N'02', 10, N'zhoupeng')
INSERT [dbo].[Port_DeptEmp] ([MyPK], [FK_Emp], [FK_Dept], [FK_Duty], [DutyLevel], [Leader]) VALUES (N'1001_zhanghaicheng', N'zhanghaicheng', N'1001', N'03', 20, N'zhoupeng')
INSERT [dbo].[Port_DeptEmp] ([MyPK], [FK_Emp], [FK_Dept], [FK_Duty], [DutyLevel], [Leader]) VALUES (N'1001_zhangyifan', N'zhangyifan', N'1001', N'04', 20, N'zhanghaicheng')
INSERT [dbo].[Port_DeptEmp] ([MyPK], [FK_Emp], [FK_Dept], [FK_Duty], [DutyLevel], [Leader]) VALUES (N'1001_zhoushengyu', N'zhoushengyu', N'1001', N'04', 20, N'zhanghaicheng')
INSERT [dbo].[Port_DeptEmp] ([MyPK], [FK_Emp], [FK_Dept], [FK_Duty], [DutyLevel], [Leader]) VALUES (N'1002_qifenglin', N'qifenglin', N'1002', N'03', 20, N'zhoupeng')
INSERT [dbo].[Port_DeptEmp] ([MyPK], [FK_Emp], [FK_Dept], [FK_Duty], [DutyLevel], [Leader]) VALUES (N'1002_zhoutianjiao', N'zhoutianjiao', N'1002', N'04', 20, N'qifenglin')
INSERT [dbo].[Port_DeptEmp] ([MyPK], [FK_Emp], [FK_Dept], [FK_Duty], [DutyLevel], [Leader]) VALUES (N'1003_fuhui', N'fuhui', N'1003', N'04', 20, N'guoxiangbin')
INSERT [dbo].[Port_DeptEmp] ([MyPK], [FK_Emp], [FK_Dept], [FK_Duty], [DutyLevel], [Leader]) VALUES (N'1003_guoxiangbin', N'guoxiangbin', N'1003', N'03', 20, N'zhoupeng')
INSERT [dbo].[Port_DeptEmp] ([MyPK], [FK_Emp], [FK_Dept], [FK_Duty], [DutyLevel], [Leader]) VALUES (N'1004_guobaogeng', N'guobaogeng', N'1004', N'04', 20, N'yangyilei')
INSERT [dbo].[Port_DeptEmp] ([MyPK], [FK_Emp], [FK_Dept], [FK_Duty], [DutyLevel], [Leader]) VALUES (N'1004_yangyilei', N'yangyilei', N'1004', N'03', 20, N'zhoupeng')
INSERT [dbo].[Port_DeptEmp] ([MyPK], [FK_Emp], [FK_Dept], [FK_Duty], [DutyLevel], [Leader]) VALUES (N'1005_liping', N'liping', N'1005', N'03', 20, N'zhoupeng')
INSERT [dbo].[Port_DeptEmp] ([MyPK], [FK_Emp], [FK_Dept], [FK_Duty], [DutyLevel], [Leader]) VALUES (N'1005_liyan', N'liyan', N'1005', N'04', 20, N'liping')
/****** Object:  Table [dbo].[Port_DeptDuty]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Port_DeptDuty](
	[FK_Dept] [nvarchar](15) NOT NULL,
	[FK_Duty] [nvarchar](100) NOT NULL,
 CONSTRAINT [Port_DeptDutypk] PRIMARY KEY CLUSTERED 
(
	[FK_Dept] ASC,
	[FK_Duty] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Port_DeptDuty] ([FK_Dept], [FK_Duty]) VALUES (N'100', N'01')
INSERT [dbo].[Port_DeptDuty] ([FK_Dept], [FK_Duty]) VALUES (N'100', N'02')
INSERT [dbo].[Port_DeptDuty] ([FK_Dept], [FK_Duty]) VALUES (N'1001', N'04')
INSERT [dbo].[Port_DeptDuty] ([FK_Dept], [FK_Duty]) VALUES (N'1002', N'04')
INSERT [dbo].[Port_DeptDuty] ([FK_Dept], [FK_Duty]) VALUES (N'1003', N'04')
INSERT [dbo].[Port_DeptDuty] ([FK_Dept], [FK_Duty]) VALUES (N'1004', N'04')
INSERT [dbo].[Port_DeptDuty] ([FK_Dept], [FK_Duty]) VALUES (N'1005', N'04')
/****** Object:  Table [dbo].[Port_Dept]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Port_Dept](
	[No] [nvarchar](50) NOT NULL,
	[Name] [nvarchar](100) NULL,
	[NameOfPath] [nvarchar](300) NULL,
	[ParentNo] [nvarchar](100) NULL,
	[TreeNo] [nvarchar](100) NULL,
	[Leader] [nvarchar](100) NULL,
	[Tel] [nvarchar](100) NULL,
	[Idx] [int] NULL,
	[IsDir] [int] NULL,
	[FK_DeptType] [nvarchar](100) NULL,
 CONSTRAINT [Port_Deptpk] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Port_Dept] ([No], [Name], [NameOfPath], [ParentNo], [TreeNo], [Leader], [Tel], [Idx], [IsDir], [FK_DeptType]) VALUES (N'100', N'集团总部', NULL, N'0', NULL, N'zhoupeng', NULL, NULL, 0, N'01')
INSERT [dbo].[Port_Dept] ([No], [Name], [NameOfPath], [ParentNo], [TreeNo], [Leader], [Tel], [Idx], [IsDir], [FK_DeptType]) VALUES (N'1001', N'集团市场部', NULL, N'100', NULL, N'zhanghaicheng', NULL, NULL, 1, N'02')
INSERT [dbo].[Port_Dept] ([No], [Name], [NameOfPath], [ParentNo], [TreeNo], [Leader], [Tel], [Idx], [IsDir], [FK_DeptType]) VALUES (N'1002', N'集团研发部', NULL, N'100', NULL, N'qifenglin', NULL, NULL, 1, N'02')
INSERT [dbo].[Port_Dept] ([No], [Name], [NameOfPath], [ParentNo], [TreeNo], [Leader], [Tel], [Idx], [IsDir], [FK_DeptType]) VALUES (N'1003', N'集团服务部', NULL, N'100', NULL, N'zhanghaicheng', NULL, NULL, 1, N'02')
INSERT [dbo].[Port_Dept] ([No], [Name], [NameOfPath], [ParentNo], [TreeNo], [Leader], [Tel], [Idx], [IsDir], [FK_DeptType]) VALUES (N'1004', N'集团财务部', NULL, N'100', NULL, N'yangyilei', NULL, NULL, 1, N'02')
INSERT [dbo].[Port_Dept] ([No], [Name], [NameOfPath], [ParentNo], [TreeNo], [Leader], [Tel], [Idx], [IsDir], [FK_DeptType]) VALUES (N'1005', N'集团人力资源部', NULL, N'100', NULL, N'liping', NULL, NULL, 1, N'02')
INSERT [dbo].[Port_Dept] ([No], [Name], [NameOfPath], [ParentNo], [TreeNo], [Leader], [Tel], [Idx], [IsDir], [FK_DeptType]) VALUES (N'1060', N'南方分公司', NULL, N'100', NULL, N'wangwenying', NULL, NULL, 0, N'03')
INSERT [dbo].[Port_Dept] ([No], [Name], [NameOfPath], [ParentNo], [TreeNo], [Leader], [Tel], [Idx], [IsDir], [FK_DeptType]) VALUES (N'1061', N'市场部', NULL, N'1006', NULL, N'ranqingxin', NULL, NULL, 1, N'04')
INSERT [dbo].[Port_Dept] ([No], [Name], [NameOfPath], [ParentNo], [TreeNo], [Leader], [Tel], [Idx], [IsDir], [FK_DeptType]) VALUES (N'1062', N'财务部', NULL, N'1006', NULL, N'randun', NULL, NULL, 1, N'04')
INSERT [dbo].[Port_Dept] ([No], [Name], [NameOfPath], [ParentNo], [TreeNo], [Leader], [Tel], [Idx], [IsDir], [FK_DeptType]) VALUES (N'1063', N'销售部', NULL, N'1006', NULL, N'randun', NULL, NULL, 1, N'04')
INSERT [dbo].[Port_Dept] ([No], [Name], [NameOfPath], [ParentNo], [TreeNo], [Leader], [Tel], [Idx], [IsDir], [FK_DeptType]) VALUES (N'1070', N'北方分公司', NULL, N'100', NULL, N'lining', NULL, NULL, 0, N'03')
INSERT [dbo].[Port_Dept] ([No], [Name], [NameOfPath], [ParentNo], [TreeNo], [Leader], [Tel], [Idx], [IsDir], [FK_DeptType]) VALUES (N'1071', N'市场部', NULL, N'1070', NULL, N'lichao', NULL, NULL, 1, N'04')
INSERT [dbo].[Port_Dept] ([No], [Name], [NameOfPath], [ParentNo], [TreeNo], [Leader], [Tel], [Idx], [IsDir], [FK_DeptType]) VALUES (N'1072', N'财务部', NULL, N'1070', NULL, N'linyangyang', NULL, NULL, 1, N'04')
INSERT [dbo].[Port_Dept] ([No], [Name], [NameOfPath], [ParentNo], [TreeNo], [Leader], [Tel], [Idx], [IsDir], [FK_DeptType]) VALUES (N'1073', N'销售部', NULL, N'1070', NULL, N'tianyi', NULL, NULL, 1, N'04')
INSERT [dbo].[Port_Dept] ([No], [Name], [NameOfPath], [ParentNo], [TreeNo], [Leader], [Tel], [Idx], [IsDir], [FK_DeptType]) VALUES (N'1099', N'外来单位', NULL, N'100', NULL, N'Guest', NULL, NULL, 1, N'02')
/****** Object:  Table [dbo].[Port_ALDept]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Port_ALDept](
	[No] [nvarchar](50) NOT NULL,
	[Name] [nvarchar](100) NULL,
	[FK_ALDeptType] [nvarchar](100) NULL,
	[Leader] [nvarchar](100) NULL,
	[NameOfPath] [nvarchar](100) NULL,
	[Tel] [nvarchar](100) NULL,
 CONSTRAINT [Port_ALDeptpk] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OP_Dept]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OP_Dept](
	[No] [nvarchar](100) NOT NULL,
	[Name] [nvarchar](100) NULL,
 CONSTRAINT [OP_Deptpk] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_ZJGLCertificate]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_ZJGLCertificate](
	[No] [nvarchar](3) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[CertificatesID] [nvarchar](3) NULL,
	[FK_CName] [nvarchar](100) NULL,
	[Unit] [nvarchar](100) NULL,
	[KeepManID] [nvarchar](100) NULL,
	[OperatorName] [nvarchar](100) NULL,
	[RegistrationDate] [nvarchar](50) NULL,
	[CertificatesState] [int] NULL,
 CONSTRAINT [OA_ZJGLCertificatepk] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_YJGLSeal]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_YJGLSeal](
	[No] [nvarchar](3) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[SealType] [int] NULL,
	[SealName] [nvarchar](100) NULL,
	[Unit] [nvarchar](100) NULL,
	[KeepManID] [nvarchar](100) NULL,
	[OperatorName] [nvarchar](100) NULL,
	[RegistrationDate] [nvarchar](50) NULL,
 CONSTRAINT [OA_YJGLSealpk] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_WaiXieHui]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_WaiXieHui](
	[No] [nvarchar](100) NOT NULL,
	[MingCheng] [nvarchar](100) NULL,
	[LianXiRen] [nvarchar](100) NULL,
	[LianXiDiZhi] [nvarchar](100) NULL,
	[LianXiDianHua] [nvarchar](100) NULL,
	[ChuanZhen] [nvarchar](100) NULL,
	[Email] [nvarchar](100) NULL,
	[BeiZhu] [nvarchar](4000) NULL,
 CONSTRAINT [OA_WaiXieHuipk] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_VoteDetail]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_VoteDetail](
	[OID] [int] NOT NULL,
	[FK_VoteOID] [int] NULL,
	[VoteItem] [nvarchar](100) NULL,
	[VComment] [nvarchar](500) NULL,
	[IsAnony] [int] NULL,
	[FK_Emp] [nvarchar](10) NULL,
	[VoteDate] [nvarchar](50) NULL,
 CONSTRAINT [OA_VoteDetailpk] PRIMARY KEY CLUSTERED 
(
	[OID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_Vote]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_Vote](
	[OID] [int] NOT NULL,
	[Title] [nvarchar](50) NULL,
	[Item] [nvarchar](500) NULL,
	[FK_Emp] [nvarchar](100) NOT NULL,
	[VoteEmpNo] [nvarchar](4000) NULL,
	[VoteType] [nvarchar](100) NULL,
	[IsAnonymous] [int] NULL,
	[IsEnable] [int] NULL,
	[DateFrom] [nvarchar](50) NULL,
	[DateTo] [nvarchar](50) NULL,
	[CreateDate] [nvarchar](50) NULL,
	[VoteEmpName] [nvarchar](4000) NULL,
 CONSTRAINT [OA_Votepk] PRIMARY KEY CLUSTERED 
(
	[OID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_UserConfig]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_UserConfig](
	[OID] [int] NOT NULL,
	[ConfigGroup] [nvarchar](50) NULL,
	[ConfigItem] [nvarchar](50) NULL,
	[AddTime] [nvarchar](50) NULL,
	[Config] [nvarchar](4000) NULL,
	[State] [int] NULL,
 CONSTRAINT [OA_UserConfigpk] PRIMARY KEY CLUSTERED 
(
	[OID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_Unit]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_Unit](
	[No] [nvarchar](3) NOT NULL,
	[Name] [nvarchar](10) NULL,
 CONSTRAINT [OA_Unitpk] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_TrainingPlans]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_TrainingPlans](
	[OID] [int] NOT NULL,
	[Year] [nvarchar](100) NULL,
	[Dept] [nvarchar](100) NULL,
	[CreateUser] [nvarchar](100) NULL,
	[RecordDT] [nvarchar](50) NULL,
	[Type] [int] NULL,
	[Name] [nvarchar](100) NULL,
	[DataTime] [nvarchar](50) NULL,
	[DuiXiang] [int] NULL,
	[FeiYong] [float] NULL,
	[BeiZhu] [int] NULL,
 CONSTRAINT [OA_TrainingPlanspk] PRIMARY KEY CLUSTERED 
(
	[OID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_TenderInfo]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_TenderInfo](
	[No] [nvarchar](3) NOT NULL,
	[Name] [nvarchar](100) NULL,
	[Tbh] [nvarchar](10) NULL,
	[FK_Xmmc] [nvarchar](100) NULL,
	[FK_Dwmc] [nvarchar](100) NULL,
	[Kbrq] [nvarchar](50) NULL,
	[Zbrq] [nvarchar](50) NULL,
	[TBZT] [int] NULL,
	[Bzj] [float] NULL,
	[Fwf] [float] NULL,
	[Jbr] [nvarchar](10) NULL,
	[FP] [int] NULL,
	[Skr] [nvarchar](10) NULL,
	[Tbxs] [nvarchar](10) NULL,
	[Tbje] [nvarchar](10) NULL,
	[Tbrq] [nvarchar](50) NULL,
	[Bz] [nvarchar](4000) NULL,
 CONSTRAINT [OA_TenderInfopk] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_Supplier]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_Supplier](
	[No] [nvarchar](3) NOT NULL,
	[Name] [nvarchar](100) NULL,
 CONSTRAINT [OA_Supplierpk] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_SponsorMeeting]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_SponsorMeeting](
	[OID] [int] NOT NULL,
	[Title] [nvarchar](200) NULL,
	[Type] [nvarchar](5) NULL,
	[Fk_Emp] [nvarchar](100) NULL,
	[CompereEmpNo] [nvarchar](50) NULL,
	[SponsorDeptNo] [nvarchar](50) NULL,
	[ParticipantsEmpNo] [nvarchar](4000) NULL,
	[MeetingRecorder] [nvarchar](200) NULL,
	[MeetingAbstract] [nvarchar](4000) NULL,
	[Content] [nvarchar](4000) NULL,
	[Attachment] [nvarchar](500) NULL,
	[FK_SponsorOID] [int] NULL,
 CONSTRAINT [OA_SponsorMeetingpk] PRIMARY KEY CLUSTERED 
(
	[OID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_ShouKuanJiHua]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_ShouKuanJiHua](
	[OID] [int] NOT NULL,
	[JiHuaBianHao] [nvarchar](100) NULL,
	[JiHuaRen] [nvarchar](100) NULL,
	[JuHuaDate] [nvarchar](50) NULL,
	[BeiZhu] [nvarchar](4000) NULL,
 CONSTRAINT [OA_ShouKuanJiHuapk] PRIMARY KEY CLUSTERED 
(
	[OID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_ShouJuDengJi]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_ShouJuDengJi](
	[OID] [int] NOT NULL,
	[HeTongBianHao] [nvarchar](100) NULL,
	[HeTongJE] [float] NULL,
	[ShouJuHao] [nvarchar](100) NULL,
	[XiangMuMingCheng] [nvarchar](100) NULL,
	[ShouJuJE] [float] NULL,
	[CaiWuShiShouJE] [float] NULL,
	[JingBanRen] [nvarchar](100) NULL,
	[KeHuMingCheng] [nvarchar](100) NULL,
	[JingBanDate] [nvarchar](50) NULL,
	[BeiZhu] [nvarchar](4000) NULL,
 CONSTRAINT [OA_ShouJuDengJipk] PRIMARY KEY CLUSTERED 
(
	[OID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_ShortMsg]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_ShortMsg](
	[OID] [int] NOT NULL,
	[FK_UserNo] [nvarchar](50) NULL,
	[FK_SendToUserNo] [nvarchar](50) NULL,
	[AddTime] [nvarchar](50) NULL,
	[Doc] [nvarchar](4000) NULL,
	[AttachFile] [nvarchar](4000) NULL,
	[AttachId] [nvarchar](10) NULL,
	[SubjectTitle] [nvarchar](100) NULL,
	[GroupRedirect] [nvarchar](500) NULL,
	[GroupRedirectFormat] [nvarchar](500) NULL,
	[SingleRedirect] [nvarchar](500) NULL,
	[SingleRedirectFormat] [nvarchar](500) NULL,
	[Received] [int] NULL,
	[IsWap] [int] NULL,
 CONSTRAINT [OA_ShortMsgpk] PRIMARY KEY CLUSTERED 
(
	[OID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_SealUse]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_SealUse](
	[OID] [int] NOT NULL,
	[Name] [nvarchar](100) NULL,
	[IsTack] [int] NULL,
	[FK_Seal] [nvarchar](100) NULL,
	[FK_Dept] [nvarchar](100) NULL,
	[FK_Emp] [nvarchar](100) NULL,
	[BeginDate] [nvarchar](50) NULL,
	[EndDate] [nvarchar](50) NULL,
	[Note] [nvarchar](4000) NULL,
	[WorkID] [int] NULL,
 CONSTRAINT [OA_SealUsepk] PRIMARY KEY CLUSTERED 
(
	[OID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_SBQK]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_SBQK](
	[No] [nvarchar](100) NOT NULL,
	[Name] [nvarchar](100) NULL,
	[Num] [nvarchar](100) NULL,
	[SBDate] [nvarchar](50) NULL,
	[YCQK] [nvarchar](100) NULL,
	[Place] [nvarchar](100) NULL,
	[SBR] [nvarchar](100) NULL,
	[DJDate] [nvarchar](50) NULL,
	[SCLZT] [int] NULL,
	[WCSJ] [nvarchar](50) NULL,
	[Note] [nvarchar](100) NULL,
 CONSTRAINT [OA_SBQKpk] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_RoomResource]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_RoomResource](
	[No] [nvarchar](3) NOT NULL,
	[Name] [nvarchar](100) NULL,
 CONSTRAINT [OA_RoomResourcepk] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_RoomOrding]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_RoomOrding](
	[OID] [int] NOT NULL,
	[Title] [nvarchar](100) NULL,
	[Fk_Emp] [nvarchar](100) NOT NULL,
	[Fk_Dept] [nvarchar](100) NOT NULL,
	[Fk_Room] [nvarchar](100) NOT NULL,
	[DateFrom] [nvarchar](50) NULL,
	[DateTo] [nvarchar](50) NULL,
	[MTResource] [nvarchar](500) NULL,
	[State] [nvarchar](1) NULL,
	[Note] [nvarchar](500) NULL,
 CONSTRAINT [OA_RoomOrdingpk] PRIMARY KEY CLUSTERED 
(
	[OID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_Room]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_Room](
	[No] [nvarchar](3) NOT NULL,
	[Name] [nvarchar](100) NULL,
	[RoomSta] [int] NULL,
	[FK_Emp] [nvarchar](100) NULL,
	[Fk_EmpName] [nvarchar](100) NULL,
	[Capacity] [nvarchar](5) NULL,
	[RoomResource] [nvarchar](100) NULL,
	[ResourceName] [nvarchar](100) NULL,
	[Note] [nvarchar](100) NULL,
	[Authority] [nvarchar](100) NULL,
 CONSTRAINT [OA_Roompk] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_Repair]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_Repair](
	[OID] [int] NOT NULL,
	[FixNum] [nvarchar](100) NULL,
	[FK_FixMan] [nvarchar](100) NULL,
	[FK_FixAssBigCatagory] [nvarchar](100) NULL,
	[FK_FixAssBigCatSon] [nvarchar](100) NULL,
	[ZipSpec] [nvarchar](100) NULL,
	[WhoRes] [nvarchar](100) NULL,
	[RepMney] [float] NULL,
	[WhoWantRep] [nvarchar](100) NULL,
	[FixManRepairDept] [nvarchar](100) NULL,
	[FixManRepairDate] [nvarchar](50) NULL,
	[RepairZT] [int] NULL,
	[WhyRepair] [nvarchar](100) NULL,
	[WorkID] [int] NULL,
 CONSTRAINT [OA_Repairpk] PRIMARY KEY CLUSTERED 
(
	[OID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_PWGL]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_PWGL](
	[No] [nvarchar](100) NOT NULL,
	[Name] [nvarchar](100) NULL,
	[CHDate] [nvarchar](50) NULL,
	[DPType] [int] NULL,
	[MDD] [nvarchar](100) NULL,
	[SJLD] [nvarchar](100) NULL,
	[DPAdmin] [nvarchar](100) NULL,
	[HBH] [nvarchar](100) NULL,
	[JG] [float] NULL,
	[QFDate] [nvarchar](50) NULL,
	[DPDate] [nvarchar](50) NULL,
	[FK_NY] [nvarchar](100) NULL,
	[Note] [nvarchar](4000) NULL,
 CONSTRAINT [OA_PWGLpk] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_ProjectWeihu]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_ProjectWeihu](
	[No] [nvarchar](3) NOT NULL,
	[XMLX] [int] NULL,
	[Xmbh] [nvarchar](10) NULL,
	[Lxsj] [nvarchar](50) NULL,
	[SSHY] [int] NULL,
	[Szsf] [nvarchar](100) NULL,
	[Szcs] [nvarchar](100) NULL,
	[Khmc] [nvarchar](100) NULL,
	[GCLB] [int] NULL,
	[Gcgm] [nvarchar](100) NULL,
	[JSXZ] [int] NULL,
	[Sz] [nvarchar](100) NULL,
	[XMZT] [int] NULL,
	[YFXM] [int] NULL,
	[Zlzs] [nvarchar](100) NULL,
	[Rdh] [nvarchar](100) NULL,
 CONSTRAINT [OA_ProjectWeihupk] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_Projects]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_Projects](
	[No] [nvarchar](3) NOT NULL,
	[Name] [nvarchar](100) NULL,
 CONSTRAINT [OA_Projectspk] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_Project]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_Project](
	[No] [nvarchar](3) NOT NULL,
	[Xmbh] [nvarchar](10) NULL,
	[Name] [nvarchar](10) NULL,
	[XMLX] [int] NULL,
	[GCLB] [int] NULL,
 CONSTRAINT [OA_Projectpk] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_ProcurementPlan]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_ProcurementPlan](
	[No] [nvarchar](3) NOT NULL,
	[Name] [nvarchar](100) NULL,
	[JHType] [int] NULL,
	[FK_Project] [nvarchar](100) NULL,
	[JHDate] [nvarchar](50) NULL,
	[Note] [nvarchar](4000) NULL,
	[FK_NY] [nvarchar](100) NULL,
 CONSTRAINT [OA_ProcurementPlanpk] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_PrivPlanItem]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_PrivPlanItem](
	[OID] [int] NOT NULL,
	[FK_PrivPlan] [int] NULL,
	[AddTime] [nvarchar](50) NULL,
	[Score] [int] NULL,
	[Hours] [int] NULL,
	[Sort] [int] NULL,
	[Succeed] [int] NULL,
	[Task] [nvarchar](4000) NULL,
	[Reason] [nvarchar](4000) NULL,
	[Improve] [nvarchar](4000) NULL,
	[PrivPlanItemType] [int] NULL,
 CONSTRAINT [OA_PrivPlanItempk] PRIMARY KEY CLUSTERED 
(
	[OID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_PrivPlanEmp]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_PrivPlanEmp](
	[PK_UserNo] [nvarchar](50) NOT NULL,
	[AddTime] [nvarchar](50) NULL,
	[Score] [nvarchar](50) NULL,
 CONSTRAINT [OA_PrivPlanEmppk] PRIMARY KEY CLUSTERED 
(
	[PK_UserNo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_PrivPlan]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_PrivPlan](
	[OID] [int] NOT NULL,
	[FK_UserNo] [nvarchar](50) NULL,
	[PlanDate] [nvarchar](20) NULL,
	[AddTime] [nvarchar](50) NULL,
	[Score] [nvarchar](50) NULL,
	[Checked] [int] NULL,
	[CheckTime] [nvarchar](50) NULL,
	[MyDoc] [nvarchar](4000) NULL,
 CONSTRAINT [OA_PrivPlanpk] PRIMARY KEY CLUSTERED 
(
	[OID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_NoticeReader]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_NoticeReader](
	[MyPK] [nvarchar](100) NOT NULL,
	[FK_UserNo] [nvarchar](50) NULL,
	[FK_NoticeNo] [nvarchar](50) NULL,
	[AddTime] [nvarchar](50) NULL,
	[ReadTime] [nvarchar](50) NULL,
	[AdviceValue] [nvarchar](50) NULL,
	[AdviceRemark] [nvarchar](4000) NULL,
 CONSTRAINT [OA_NoticeReaderpk] PRIMARY KEY CLUSTERED 
(
	[MyPK] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_NoticeCategory]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_NoticeCategory](
	[No] [nvarchar](2) NOT NULL,
	[Name] [nvarchar](100) NULL,
 CONSTRAINT [OA_NoticeCategorypk] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[OA_NoticeCategory] ([No], [Name]) VALUES (N'01', N'放假通知')
INSERT [dbo].[OA_NoticeCategory] ([No], [Name]) VALUES (N'02', N'人事调整')
INSERT [dbo].[OA_NoticeCategory] ([No], [Name]) VALUES (N'03', N'公司会议')
INSERT [dbo].[OA_NoticeCategory] ([No], [Name]) VALUES (N'04', N'紧急通知')
/****** Object:  Table [dbo].[OA_Notice]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_Notice](
	[OID] [int] NOT NULL,
	[Title] [nvarchar](300) NULL,
	[Doc] [nvarchar](4000) NULL,
	[AttachFile] [nvarchar](4000) NULL,
	[FK_NoticeCategory] [nvarchar](100) NULL,
	[RDT] [nvarchar](50) NULL,
	[FK_Dept] [nvarchar](100) NULL,
	[FK_UserNo] [nvarchar](100) NULL,
	[SendToParts] [int] NULL,
	[SendToUsers] [nvarchar](4000) NULL,
	[SendToDepts] [nvarchar](4000) NULL,
	[SendToStations] [nvarchar](4000) NULL,
	[SetTop] [nvarchar](50) NULL,
	[Importance] [int] NULL,
	[GetAdvices] [int] NULL,
	[AdviceDesc] [nvarchar](4000) NULL,
	[AdviceItems] [nvarchar](4000) NULL,
	[NoticeSta] [int] NULL,
	[StartTime] [nvarchar](50) NULL,
	[StopTime] [nvarchar](50) NULL,
	[SetMes] [int] NULL,
 CONSTRAINT [OA_Noticepk] PRIMARY KEY CLUSTERED 
(
	[OID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_Notepaper]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_Notepaper](
	[No] [nvarchar](10) NOT NULL,
	[Name] [nvarchar](200) NULL,
	[FK_Emp] [nvarchar](100) NULL,
	[RDT] [nvarchar](50) NULL,
	[NoteContent] [nvarchar](4000) NULL,
 CONSTRAINT [OA_Notepaperpk] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_MessageSendBox]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_MessageSendBox](
	[OID] [int] NOT NULL,
	[Receiver] [nvarchar](1000) NULL,
	[FK_SendUserNo] [nvarchar](50) NULL,
	[FK_MsgNo] [int] NULL,
	[SendTime] [nvarchar](50) NULL,
 CONSTRAINT [OA_MessageSendBoxpk] PRIMARY KEY CLUSTERED 
(
	[OID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_MessageRecycleBox]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_MessageRecycleBox](
	[OID] [int] NOT NULL,
	[FK_MsgNo] [int] NULL,
	[FK_UserNo] [nvarchar](50) NULL,
	[AddTime] [nvarchar](50) NULL,
	[ReceivedTime] [nvarchar](50) NULL,
	[Received] [int] NULL,
	[FromType] [int] NULL,
 CONSTRAINT [OA_MessageRecycleBoxpk] PRIMARY KEY CLUSTERED 
(
	[OID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_MessageInBox]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_MessageInBox](
	[OID] [int] NOT NULL,
	[FK_MsgNo] [int] NULL,
	[FK_ReceiveUserNo] [nvarchar](50) NULL,
	[Sender] [nvarchar](50) NULL,
	[Doc] [nvarchar](4000) NULL,
	[AddTime] [nvarchar](50) NULL,
	[ReceiveTime] [nvarchar](20) NULL,
	[Received] [int] NULL,
	[IsCopy] [int] NULL,
	[Title] [nvarchar](2000) NULL,
	[AttachFile] [nvarchar](4000) NULL,
 CONSTRAINT [OA_MessageInBoxpk] PRIMARY KEY CLUSTERED 
(
	[OID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_MessageDraftBox]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_MessageDraftBox](
	[OID] [int] NOT NULL,
	[FK_MsgNo] [int] NULL,
	[AddTime] [nvarchar](50) NULL,
	[FK_UserNo] [nvarchar](50) NULL,
 CONSTRAINT [OA_MessageDraftBoxpk] PRIMARY KEY CLUSTERED 
(
	[OID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_Message]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_Message](
	[OID] [int] NOT NULL,
	[AddTime] [nvarchar](50) NULL,
	[Title] [nvarchar](500) NULL,
	[AttachFile] [nvarchar](4000) NULL,
	[Doc] [nvarchar](4000) NULL,
	[FK_UserNo] [nvarchar](50) NULL,
	[MessageState] [int] NULL,
	[SendToUsers] [nvarchar](4000) NULL,
	[CopyToUsers] [nvarchar](4000) NULL,
 CONSTRAINT [OA_Messagepk] PRIMARY KEY CLUSTERED 
(
	[OID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_MeetingType]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_MeetingType](
	[No] [nvarchar](3) NOT NULL,
	[Name] [nvarchar](100) NULL,
	[Note] [nvarchar](200) NULL,
 CONSTRAINT [OA_MeetingTypepk] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_MeetingSummary]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_MeetingSummary](
	[OID] [int] NOT NULL,
	[FK_OID] [nvarchar](10) NULL,
	[Content] [nvarchar](4000) NULL,
	[SenderEmps] [nvarchar](4000) NULL,
	[SenderDepts] [nvarchar](4000) NULL,
	[Attachment] [nvarchar](4000) NULL,
	[Recorder] [nvarchar](10) NULL,
	[FileName] [nvarchar](200) NULL,
	[SendDate] [nvarchar](50) NULL,
 CONSTRAINT [OA_MeetingSummarypk] PRIMARY KEY CLUSTERED 
(
	[OID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_LogType]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_LogType](
	[No] [nvarchar](3) NOT NULL,
	[Name] [nvarchar](100) NULL,
 CONSTRAINT [OA_LogTypepk] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_LeaderDtl]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_LeaderDtl](
	[OID] [int] NOT NULL,
	[FK_Leader] [nvarchar](10) NULL,
	[DTFrom] [nvarchar](70) NULL,
	[DTTo] [nvarchar](70) NULL,
	[Title] [nvarchar](4000) NULL,
 CONSTRAINT [OA_LeaderDtlpk] PRIMARY KEY CLUSTERED 
(
	[OID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_Leader]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_Leader](
	[No] [nvarchar](3) NOT NULL,
	[Name] [nvarchar](100) NULL,
	[Duty] [nvarchar](100) NULL,
	[XB] [int] NULL,
	[Age] [int] NULL,
	[JianLi] [nvarchar](4000) NULL,
	[MyFileName] [nvarchar](100) NULL,
	[MyFilePath] [nvarchar](100) NULL,
	[MyFileExt] [nvarchar](10) NULL,
	[WebPath] [nvarchar](200) NULL,
	[MyFileH] [int] NULL,
	[MyFileW] [int] NULL,
	[MyFileSize] [float] NULL,
 CONSTRAINT [OA_Leaderpk] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_KeHuDangAn]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_KeHuDangAn](
	[No] [nvarchar](100) NOT NULL,
	[KeHuMingCheng] [nvarchar](100) NULL,
	[LianXiRen] [nvarchar](100) NULL,
	[LianXiDianHua] [nvarchar](100) NULL,
	[LianXiDiZhi] [nvarchar](100) NULL,
	[ChuanZhen] [nvarchar](100) NULL,
	[Email] [nvarchar](100) NULL,
	[DongShiZhang] [nvarchar](100) NULL,
	[BeiZhu] [nvarchar](4000) NULL,
 CONSTRAINT [OA_KeHuDangAnpk] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_JingLi]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_JingLi](
	[OID] [int] NOT NULL,
	[FK_Name] [nvarchar](100) NULL,
	[WorkYearCount] [int] NULL,
	[TimeStart] [nvarchar](50) NULL,
	[TimeTo] [nvarchar](50) NULL,
	[WorkUnit] [nvarchar](100) NULL,
	[Duty] [nvarchar](50) NULL,
	[MainResults] [nvarchar](4000) NULL,
	[LeavingReason] [nvarchar](4000) NULL,
 CONSTRAINT [OA_JingLipk] PRIMARY KEY CLUSTERED 
(
	[OID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_HuiYiShiWeiXiuView]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_HuiYiShiWeiXiuView](
	[No] [nvarchar](100) NOT NULL,
	[FK_HuiYiShi] [nvarchar](100) NULL,
	[Name] [nvarchar](100) NULL,
	[QSSJ] [nvarchar](50) NULL,
	[ZZSJ] [nvarchar](50) NULL,
	[isWeiXiu] [int] NULL,
	[BZ] [nvarchar](4000) NULL,
 CONSTRAINT [OA_HuiYiShiWeiXiuViewpk] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_HuiYiShiShenQing]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_HuiYiShiShenQing](
	[No] [nvarchar](100) NOT NULL,
	[Name] [nvarchar](100) NULL,
	[FK_HuiYiShi] [nvarchar](100) NULL,
	[HYZT] [nvarchar](4000) NULL,
	[HYLX] [nvarchar](100) NULL,
	[SQBM] [nvarchar](100) NULL,
	[FQR] [nvarchar](100) NULL,
	[JLR] [nvarchar](100) NULL,
	[DJRQ] [nvarchar](50) NULL,
	[CHRS] [float] NULL,
	[QSSJ] [nvarchar](50) NULL,
	[ZZSJ] [nvarchar](50) NULL,
	[BZYQ] [nvarchar](100) NULL,
	[BZZT] [nvarchar](100) NULL,
	[HYJL] [nvarchar](4000) NULL,
	[BZ] [nvarchar](4000) NULL,
	[isWeiXiu] [int] NULL,
 CONSTRAINT [OA_HuiYiShiShenQingpk] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_HuiYiShi]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_HuiYiShi](
	[No] [nvarchar](100) NOT NULL,
	[Name] [nvarchar](100) NULL,
	[GLBM] [nvarchar](100) NULL,
	[FZR] [nvarchar](100) NULL,
	[RNRS] [float] NULL,
	[FWMJ] [nvarchar](100) NULL,
	[SS] [nvarchar](100) NULL,
	[BZ] [nvarchar](4000) NULL,
 CONSTRAINT [OA_HuiYiShipk] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_HRTransfer]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_HRTransfer](
	[No] [nvarchar](4) NOT NULL,
	[Name] [nvarchar](100) NULL,
	[DiaoDongDT] [nvarchar](50) NULL,
	[FK_OldDept] [nvarchar](100) NULL,
	[OldZhiWu] [nvarchar](100) NULL,
	[FK_NewDept] [nvarchar](100) NULL,
	[NewZhiWu] [nvarchar](100) NULL,
	[DiaoDongWhy] [nvarchar](4000) NULL,
	[Note] [nvarchar](4000) NULL,
 CONSTRAINT [OA_HRTransferpk] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_HRRecord]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_HRRecord](
	[No] [nvarchar](4) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[InCompanyDT] [nvarchar](50) NULL,
	[Gender] [int] NULL,
	[BirthDT] [nvarchar](50) NULL,
	[MinZu] [nvarchar](50) NULL,
	[JiGuan] [nvarchar](200) NULL,
	[HuKouAddr] [nvarchar](200) NULL,
	[IDCard] [nvarchar](18) NULL,
	[JiaRuDT] [nvarchar](50) NULL,
	[ZhuangTai] [int] NULL,
	[FK_Dept] [nvarchar](100) NULL,
	[FK_Duty] [nvarchar](100) NULL,
	[ZhiCheng] [nvarchar](100) NULL,
	[CongShiZhuanYe] [nvarchar](100) NULL,
	[WenHuaChengDu] [nvarchar](20) NULL,
	[ZhengZhiMianMao] [nvarchar](20) NULL,
	[BiYeSchool] [nvarchar](100) NULL,
	[SuoXueZhuanYe] [nvarchar](100) NULL,
	[BiYeDT] [nvarchar](50) NULL,
	[WorkDT] [nvarchar](50) NULL,
	[YuanCompany] [nvarchar](200) NULL,
	[Note] [nvarchar](4000) NULL,
 CONSTRAINT [OA_HRRecordpk] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_HRContractRemove]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_HRContractRemove](
	[OID] [int] NOT NULL,
	[FK_HRContract] [nvarchar](50) NULL,
	[FK_HRRecord] [nvarchar](100) NULL,
	[DengJiDT] [nvarchar](50) NULL,
	[GuDingQiXian] [int] NULL,
	[StartDT] [nvarchar](50) NULL,
	[EndDT] [nvarchar](50) NULL,
	[RemoveDT] [nvarchar](50) NULL,
	[Note] [nvarchar](4000) NULL,
 CONSTRAINT [OA_HRContractRemovepk] PRIMARY KEY CLUSTERED 
(
	[OID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_HRContractModify]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_HRContractModify](
	[OID] [int] NOT NULL,
	[FK_HRContract] [nvarchar](50) NULL,
	[FK_HRRecord] [nvarchar](100) NULL,
	[DengJiDT] [nvarchar](50) NULL,
	[OldGuDingQiXian] [int] NULL,
	[OldStartDT] [nvarchar](50) NULL,
	[OldEndDT] [nvarchar](50) NULL,
	[NewGuDingQiXian] [int] NULL,
	[NewStartDT] [nvarchar](50) NULL,
	[NewEndDT] [nvarchar](50) NULL,
	[Note] [nvarchar](4000) NULL,
 CONSTRAINT [OA_HRContractModifypk] PRIMARY KEY CLUSTERED 
(
	[OID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_HRContract]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_HRContract](
	[No] [nvarchar](4) NOT NULL,
	[FK_HRRecord] [nvarchar](100) NULL,
	[DengJiDT] [nvarchar](50) NULL,
	[QianShuDT] [nvarchar](50) NULL,
	[StartDT] [nvarchar](50) NULL,
	[EndDT] [nvarchar](50) NULL,
	[GuDingQiXian] [int] NULL,
	[Note] [nvarchar](4000) NULL,
 CONSTRAINT [OA_HRContractpk] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_HRAskForLeave]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_HRAskForLeave](
	[OID] [int] NOT NULL,
	[Fk_HrRecord] [nvarchar](1000) NULL,
	[AskCate] [int] NULL,
	[Fk_NY] [nvarchar](50) NULL,
	[Fk_Dept] [nvarchar](1000) NULL,
	[StartDT] [nvarchar](50) NULL,
	[EndDT] [nvarchar](50) NULL,
	[Hours] [nvarchar](1000) NULL,
	[AskWhy] [nvarchar](4000) NULL,
 CONSTRAINT [OA_HRAskForLeavepk] PRIMARY KEY CLUSTERED 
(
	[OID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_HouQinGuanLi]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_HouQinGuanLi](
	[No] [nvarchar](100) NOT NULL,
	[Num] [nvarchar](100) NULL,
	[SBDate] [nvarchar](50) NULL,
	[YCQK] [nvarchar](100) NULL,
	[Place] [nvarchar](100) NULL,
	[SBR] [nvarchar](100) NULL,
	[DJDate] [nvarchar](50) NULL,
	[SBBZ] [nvarchar](4000) NULL,
	[LB] [int] NULL,
	[HQSName] [nvarchar](100) NULL,
	[HQSNo] [nvarchar](100) NULL,
	[WXFZR] [nvarchar](100) NULL,
	[YY] [nvarchar](100) NULL,
	[FZHF] [nvarchar](100) NULL,
	[CLYJ] [nvarchar](100) NULL,
	[VXSC] [float] NULL,
	[WCDate] [nvarchar](50) NULL,
	[CSFY] [float] NULL,
	[CLZT] [int] NULL,
	[Note] [nvarchar](4000) NULL,
 CONSTRAINT [OA_HouQinGuanLipk] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_GoodType]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_GoodType](
	[No] [nvarchar](3) NOT NULL,
	[Name] [nvarchar](100) NULL,
 CONSTRAINT [OA_GoodTypepk] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_Goods]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_Goods](
	[OID] [int] NOT NULL,
	[Name] [nvarchar](100) NULL,
	[FK_GT] [nvarchar](100) NULL,
	[FK_PP] [nvarchar](100) NULL,
	[FK_GS] [nvarchar](100) NULL,
	[GGXH] [nvarchar](200) NULL,
	[GoodNumber] [int] NULL,
	[Sale] [float] NULL,
	[Note] [nvarchar](4000) NULL,
 CONSTRAINT [OA_Goodspk] PRIMARY KEY CLUSTERED 
(
	[OID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_GongSiZiZhi]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_GongSiZiZhi](
	[No] [nvarchar](100) NOT NULL,
	[ZiZhiMingCheng] [nvarchar](100) NULL,
	[FaZhengJiGuan] [nvarchar](100) NULL,
	[ZiZhiDengJi] [nvarchar](100) NULL,
	[BanBuDate] [nvarchar](50) NULL,
	[YouXiaoDate] [nvarchar](50) NULL,
	[BeiZhu] [nvarchar](4000) NULL,
 CONSTRAINT [OA_GongSiZiZhipk] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_GongJiJin]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_GongJiJin](
	[OID] [int] NOT NULL,
	[FK_DanWei] [nvarchar](50) NULL,
	[FK_Dept] [nvarchar](100) NULL,
	[FK_Name] [nvarchar](100) NULL,
	[NianFen] [int] NULL,
	[YueFen] [int] NULL,
	[XinJiShu] [float] NULL,
	[DanWeiJiaoCun] [float] NULL,
	[GeRenJiaoCun] [float] NULL,
	[HeJi] [float] NULL,
	[GeRenZhangHao] [nvarchar](50) NULL,
	[RDT] [nvarchar](50) NULL,
	[BeiZhu] [nvarchar](4000) NULL,
 CONSTRAINT [OA_GongJiJinpk] PRIMARY KEY CLUSTERED 
(
	[OID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_GiveBack]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_GiveBack](
	[OID] [int] NOT NULL,
	[FixNum] [nvarchar](100) NULL,
	[FK_FixMan] [nvarchar](100) NULL,
	[FK_FixAssBigCatagory] [nvarchar](100) NULL,
	[FK_FixAssBigCatSon] [nvarchar](100) NULL,
	[ZipSpec] [nvarchar](100) NULL,
	[WhoGiveBack] [nvarchar](100) NULL,
	[GiveBackDept] [nvarchar](100) NULL,
	[GiveBackDate] [nvarchar](50) NULL,
	[GiveBackZT] [int] NULL,
	[GiveBackNote] [nvarchar](100) NULL,
	[WorkID] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_GetBad]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_GetBad](
	[OID] [int] NOT NULL,
	[FixNum] [nvarchar](100) NULL,
	[FK_FixMan] [nvarchar](100) NULL,
	[FK_FixAssBigCatagory] [nvarchar](100) NULL,
	[FK_FixAssBigCatSon] [nvarchar](100) NULL,
	[ZipSpec] [nvarchar](100) NULL,
	[WhoFixManGetBad] [nvarchar](100) NULL,
	[FixManGetBadDept] [nvarchar](100) NULL,
	[FixManGetBadDate] [nvarchar](50) NULL,
	[WhyGetBad] [nvarchar](100) NULL,
	[GetBadZT] [int] NULL,
	[WorkID] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_FuLi]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_FuLi](
	[OID] [int] NOT NULL,
	[FK_DanWei] [nvarchar](50) NULL,
	[FK_Dept] [nvarchar](100) NULL,
	[FK_Name] [nvarchar](100) NULL,
	[NianFen] [int] NULL,
	[YueFen] [int] NULL,
	[XinSheBaoJiShu] [float] NULL,
	[XinYiBaoJiShu] [float] NULL,
	[YanLaoGeRenJiaoFei] [float] NULL,
	[YanLaoDanWeiJiaoFei] [float] NULL,
	[ShiYeGeRenJiaoFei] [float] NULL,
	[ShiYeDanWeiJiaoFei] [float] NULL,
	[YiLiaoGeRenJiaoFei] [float] NULL,
	[YiLiaoDanWeiJiaoFei] [float] NULL,
	[GongShangDanWeiJiaoFei] [float] NULL,
	[ShengYuDanWeiJiaoFei] [float] NULL,
	[YanLaoHeJi] [float] NULL,
	[ShiYeHeJi] [float] NULL,
	[YiLiaoHeJi] [float] NULL,
	[DanWeiJiaoFeiHeJi] [float] NULL,
	[GeRenJiaoFeiHeJi] [float] NULL,
	[HeJi] [nvarchar](50) NULL,
	[BeiZhu] [nvarchar](4000) NULL,
 CONSTRAINT [OA_FuLipk] PRIMARY KEY CLUSTERED 
(
	[OID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_FixMan]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_FixMan](
	[No] [nvarchar](4) NOT NULL,
	[Name] [nvarchar](100) NULL,
	[FK_FixAssBigCatagory] [nvarchar](100) NULL,
	[FK_FixAssBigCatSon] [nvarchar](100) NULL,
	[CertifiNum] [nvarchar](100) NULL,
	[ZipSpec] [nvarchar](100) NULL,
	[InvoicePri] [float] NULL,
	[InvoiceDat] [nvarchar](50) NULL,
	[Year] [nvarchar](100) NULL,
	[DepreBeginTime] [nvarchar](50) NULL,
	[HowManyGetOld] [float] NULL,
	[ZCZT] [int] NULL,
	[WhoUse] [nvarchar](100) NULL,
	[WhereIt] [nvarchar](100) NULL,
	[BeiZhu] [nvarchar](100) NULL,
 CONSTRAINT [OA_FixManpk] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_FixGetUse]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_FixGetUse](
	[OID] [int] NOT NULL,
	[FixNum] [nvarchar](100) NULL,
	[FK_FixMan] [nvarchar](100) NULL,
	[FK_FixAssBigCatagory] [nvarchar](100) NULL,
	[FK_FixAssBigCatSon] [nvarchar](100) NULL,
	[ZipSpec] [nvarchar](100) NULL,
	[WhoGetUse] [nvarchar](100) NULL,
	[GetUseDept] [nvarchar](100) NULL,
	[GetUseDate] [nvarchar](50) NULL,
	[GetUseZT] [int] NULL,
	[WorkID] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_FixAssBigCatSon]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_FixAssBigCatSon](
	[No] [nvarchar](4) NOT NULL,
	[FK_FixAssBigCatagory] [nvarchar](100) NULL,
	[Name] [nvarchar](100) NULL,
 CONSTRAINT [OA_FixAssBigCatSonpk] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_FixAssBigCatagory]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_FixAssBigCatagory](
	[No] [nvarchar](3) NOT NULL,
	[Name] [nvarchar](100) NULL,
 CONSTRAINT [OA_FixAssBigCatagorypk] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_DutyRecord]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_DutyRecord](
	[OID] [int] NOT NULL,
	[RecordDate] [nvarchar](50) NULL,
	[LeaveType] [int] NULL,
	[StartTime] [nvarchar](50) NULL,
	[EndTime] [nvarchar](50) NULL,
	[BeiZhu] [nvarchar](500) NULL,
	[DiDian] [nvarchar](30) NULL,
	[Recorder] [nvarchar](20) NULL,
 CONSTRAINT [OA_DutyRecordpk] PRIMARY KEY CLUSTERED 
(
	[OID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_DSTake]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_DSTake](
	[OID] [int] NOT NULL,
	[FK_DSMain] [nvarchar](100) NULL,
	[FK_Sort] [nvarchar](100) NULL,
	[Unit] [nvarchar](20) NULL,
	[Spec] [nvarchar](100) NULL,
	[NumOfWant] [float] NULL,
	[NumOfCan] [float] NULL,
	[NumOfTake] [float] NULL,
	[RDT] [nvarchar](50) NULL,
	[Taker] [nvarchar](500) NULL,
	[TakerDep] [nvarchar](500) NULL,
	[WorkID] [int] NULL,
	[DoIt] [nvarchar](500) NULL,
	[DsTakeZT] [int] NULL,
	[FK_NY] [nvarchar](100) NULL,
	[Note] [nvarchar](4000) NULL,
 CONSTRAINT [OA_DSTakepk] PRIMARY KEY CLUSTERED 
(
	[OID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_DSSort]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_DSSort](
	[No] [nvarchar](3) NOT NULL,
	[Name] [nvarchar](100) NULL,
 CONSTRAINT [OA_DSSortpk] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_DSMain]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_DSMain](
	[No] [nvarchar](4) NOT NULL,
	[Name] [nvarchar](100) NULL,
	[FK_Sort] [nvarchar](100) NULL,
	[Unit] [nvarchar](20) NULL,
	[Spec] [nvarchar](100) NULL,
	[NumOfBuy] [float] NULL,
	[NumOfTake] [float] NULL,
	[NumOfLeft] [float] NULL,
	[UnitPrice] [float] NULL,
	[Cost] [float] NULL,
 CONSTRAINT [OA_DSMainpk] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_DSBuy]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_DSBuy](
	[OID] [int] NOT NULL,
	[FK_DSMain] [nvarchar](100) NULL,
	[FK_Sort] [nvarchar](100) NULL,
	[Unit] [nvarchar](20) NULL,
	[Spec] [nvarchar](100) NULL,
	[NumOfPlan] [float] NULL,
	[NumOfBuy] [float] NULL,
	[UnitPrice] [float] NULL,
	[UnitPYCan] [float] NULL,
	[JE] [float] NULL,
	[LasTJE] [float] NULL,
	[WhoWant] [nvarchar](500) NULL,
	[Buyer] [nvarchar](500) NULL,
	[RDT] [nvarchar](50) NULL,
	[DsBuyZT] [int] NULL,
	[FK_NY] [nvarchar](100) NULL,
	[Note] [nvarchar](4000) NULL,
	[WorkID] [int] NULL,
 CONSTRAINT [OA_DSBuypk] PRIMARY KEY CLUSTERED 
(
	[OID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_DRSheet]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_DRSheet](
	[OID] [int] NOT NULL,
	[Title] [nvarchar](300) NULL,
	[DRSta] [int] NULL,
	[DRShe] [int] NULL,
	[Doc] [nvarchar](4000) NULL,
	[Rec] [nvarchar](100) NULL,
	[RDT] [nvarchar](50) NULL,
	[EDT] [nvarchar](50) NULL,
 CONSTRAINT [OA_DRSheetpk] PRIMARY KEY CLUSTERED 
(
	[OID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_DRShare]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_DRShare](
	[Sharer] [nvarchar](100) NOT NULL,
	[ShareTo] [nvarchar](100) NOT NULL,
 CONSTRAINT [OA_DRSharepk] PRIMARY KEY CLUSTERED 
(
	[Sharer] ASC,
	[ShareTo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_DRComment]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_DRComment](
	[OID] [int] NOT NULL,
	[FK_COM] [nvarchar](300) NULL,
	[Comm] [nvarchar](4000) NULL,
	[Critic] [nvarchar](100) NULL,
	[CommDate] [nvarchar](50) NULL,
 CONSTRAINT [OA_DRCommentpk] PRIMARY KEY CLUSTERED 
(
	[OID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_DHGL]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_DHGL](
	[No] [nvarchar](100) NOT NULL,
	[Dept] [nvarchar](100) NULL,
	[Station] [nvarchar](100) NULL,
	[Name] [nvarchar](100) NULL,
	[Tel] [nvarchar](100) NULL,
	[HTHao] [nvarchar](100) NULL,
	[TXGS] [int] NULL,
	[TSDV] [nvarchar](100) NULL,
	[SDV] [nvarchar](100) NULL,
	[JBR] [nvarchar](100) NULL,
	[BTBZ] [nvarchar](100) NULL,
	[HF] [int] NULL,
	[CE] [int] NULL,
	[DHSYZT] [nvarchar](100) NULL,
	[Note] [nvarchar](4000) NULL,
 CONSTRAINT [OA_DHGLpk] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_DesktopStation]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_DesktopStation](
	[OID] [nvarchar](10) NOT NULL,
	[FK_Station] [nvarchar](100) NULL,
	[FK_PID] [nvarchar](200) NULL,
	[Idx] [int] NULL,
 CONSTRAINT [OA_DesktopStationpk] PRIMARY KEY CLUSTERED 
(
	[OID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_DesktopPerson]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_DesktopPerson](
	[OID] [nvarchar](10) NOT NULL,
	[FK_Emp] [nvarchar](100) NULL,
	[FK_PID] [nvarchar](200) NULL,
	[Idx] [int] NULL,
 CONSTRAINT [OA_DesktopPersonpk] PRIMARY KEY CLUSTERED 
(
	[OID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_DesktopPanel]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_DesktopPanel](
	[No] [nvarchar](10) NOT NULL,
	[Name] [nvarchar](200) NULL,
	[PanelType] [int] NULL,
	[PanelIcon] [nvarchar](200) NULL,
	[PTable] [nvarchar](500) NULL,
	[FK_MyPK] [nvarchar](500) NULL,
	[TitleColumn] [nvarchar](500) NULL,
	[OrderColumn] [nvarchar](500) NULL,
	[AllowRows] [nvarchar](500) NULL,
	[ColumnName] [nvarchar](1000) NULL,
	[ColumnDesc] [nvarchar](1000) NULL,
	[ExtUrl] [nvarchar](500) NULL,
	[EDT] [nvarchar](500) NULL,
	[ImgUrl] [nvarchar](500) NULL,
	[OutHref] [nvarchar](1000) NULL,
	[IsEnabled] [int] NULL,
	[Idx] [int] NULL,
 CONSTRAINT [OA_DesktopPanelpk] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_DesktopConfig]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_DesktopConfig](
	[No] [nvarchar](10) NOT NULL,
	[Name] [nvarchar](200) NULL,
	[FK_Emp] [nvarchar](100) NULL,
 CONSTRAINT [OA_DesktopConfigpk] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_DailyRecord]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_DailyRecord](
	[No] [nvarchar](20) NOT NULL,
	[DailyRecordNo] [nvarchar](20) NULL,
	[Name] [nvarchar](100) NULL,
	[Pass] [nvarchar](20) NULL,
	[FK_Dept] [nvarchar](20) NULL,
	[FK_Duty] [nvarchar](20) NULL,
	[SID] [nvarchar](32) NULL,
	[Tel] [nvarchar](20) NULL,
	[Email] [nvarchar](100) NULL,
 CONSTRAINT [OA_DailyRecordpk] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_Contract]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_Contract](
	[No] [nvarchar](3) NOT NULL,
	[Htbh] [nvarchar](100) NULL,
	[Htmc] [nvarchar](100) NULL,
	[FK_XM] [nvarchar](100) NULL,
	[LX] [int] NULL,
	[Ze] [float] NULL,
	[Jaje] [float] NULL,
	[Sjje] [float] NULL,
	[Sbje] [float] NULL,
	[Qtje] [float] NULL,
	[Qsrq] [nvarchar](50) NULL,
	[Htwj] [nvarchar](100) NULL,
	[JSLX] [int] NULL,
 CONSTRAINT [OA_Contractpk] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_CertificatesUse]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_CertificatesUse](
	[OID] [int] NOT NULL,
	[Name] [nvarchar](100) NULL,
	[FK_Certificates] [nvarchar](100) NULL,
	[FK_Dept] [nvarchar](100) NULL,
	[FK_Emp] [nvarchar](100) NULL,
	[CType] [nvarchar](100) NULL,
	[BeginDate] [nvarchar](50) NULL,
	[EndDate] [nvarchar](50) NULL,
	[UseTime] [float] NULL,
	[UseReason] [nvarchar](4000) NULL,
	[WorkID] [int] NULL,
 CONSTRAINT [OA_CertificatesUsepk] PRIMARY KEY CLUSTERED 
(
	[OID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_CarZhiBiao]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_CarZhiBiao](
	[No] [nvarchar](3) NOT NULL,
	[Name] [nvarchar](100) NULL,
	[YCPH] [nvarchar](100) NULL,
	[DJR] [nvarchar](100) NULL,
	[DJRQ] [nvarchar](50) NULL,
	[ZBYXRQ] [nvarchar](50) NULL,
	[ZBDQRQ] [nvarchar](50) NULL,
	[HDFS] [int] NULL,
	[ZBZT] [int] NULL,
	[ZBBH] [nvarchar](100) NULL,
	[SSDW] [nvarchar](100) NULL,
	[JBR] [nvarchar](100) NULL,
	[BZ] [nvarchar](4000) NULL,
 CONSTRAINT [OA_CarZhiBiaopk] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_CarWeiZhang]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_CarWeiZhang](
	[OID] [int] NOT NULL,
	[FK_car] [nvarchar](100) NULL,
	[JiLuDanHao] [nvarchar](100) NULL,
	[FK_JiaShiYuan] [nvarchar](100) NULL,
	[ZeRenRen] [nvarchar](100) NULL,
	[WeiZhangLeiXing] [nvarchar](100) NULL,
	[WeiZhangWeiZhi] [nvarchar](100) NULL,
	[KouFen] [float] NULL,
	[FaKuan] [float] NULL,
	[WenZhangShiJian] [nvarchar](50) NULL,
	[BeiZhu] [nvarchar](4000) NULL,
 CONSTRAINT [OA_CarWeiZhangpk] PRIMARY KEY CLUSTERED 
(
	[OID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_CarWeiXiu]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_CarWeiXiu](
	[OID] [int] NOT NULL,
	[FK_Car] [nvarchar](100) NULL,
	[WXR] [nvarchar](100) NULL,
	[WXRQ] [nvarchar](50) NULL,
	[WSFY] [float] NULL,
	[WXGS] [nvarchar](100) NULL,
	[Note] [nvarchar](4000) NULL,
 CONSTRAINT [OA_CarWeiXiupk] PRIMARY KEY CLUSTERED 
(
	[OID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_CarRuBao]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_CarRuBao](
	[OID] [int] NOT NULL,
	[FK_car] [nvarchar](100) NULL,
	[BaoXianGongSi] [int] NULL,
	[BaoXianXianZhong] [int] NULL,
	[BaoXiaoJE] [float] NULL,
	[BaoXianShengXiaoDate] [nvarchar](50) NULL,
	[BaoXianDaoQiDate] [nvarchar](50) NULL,
	[BanLiDate] [nvarchar](50) NULL,
	[JinBanRen] [nvarchar](100) NULL,
	[ZeRenRen] [nvarchar](100) NULL,
	[BeiZhu] [nvarchar](4000) NULL,
 CONSTRAINT [OA_CarRuBaopk] PRIMARY KEY CLUSTERED 
(
	[OID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_CarRecord]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_CarRecord](
	[OID] [int] NOT NULL,
	[FK_car] [nvarchar](100) NULL,
	[YongCheRen] [nvarchar](100) NULL,
	[YongCheShenPiRen] [nvarchar](100) NULL,
	[PiZhuiRen] [nvarchar](100) NULL,
	[FK_JiaShiYuan] [nvarchar](100) NULL,
	[ChuCheShiYou] [nvarchar](100) NULL,
	[ChuCheFanWei] [int] NULL,
	[YongCheKaiShiShiJian] [nvarchar](50) NULL,
	[YongCheJieShuShiJian] [nvarchar](50) NULL,
	[ChuCheQianLiCheng] [float] NULL,
	[ChuCheHouLiCheng] [float] NULL,
	[YouHao] [float] NULL,
	[BeiZhu] [nvarchar](4000) NULL,
 CONSTRAINT [OA_CarRecordpk] PRIMARY KEY CLUSTERED 
(
	[OID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_CarNianJian]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_CarNianJian](
	[OID] [int] NOT NULL,
	[FK_Car] [nvarchar](100) NULL,
	[JE] [float] NULL,
	[NJRQ] [nvarchar](50) NULL,
	[NextDT] [nvarchar](50) NULL,
	[NJR] [nvarchar](100) NULL,
	[ZRR] [nvarchar](100) NULL,
	[BZ] [nvarchar](4000) NULL,
	[WorkID] [int] NULL,
 CONSTRAINT [OA_CarNianJianpk] PRIMARY KEY CLUSTERED 
(
	[OID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_CarInfo]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_CarInfo](
	[No] [nvarchar](3) NOT NULL,
	[Name] [nvarchar](100) NULL,
	[FK_CPH] [nvarchar](100) NULL,
	[CX] [nvarchar](100) NULL,
	[CJH] [nvarchar](100) NULL,
	[FDJH] [nvarchar](100) NULL,
	[GLS] [float] NULL,
	[NJRQ] [nvarchar](50) NULL,
	[BYRQ] [nvarchar](50) NULL,
	[BXDQR] [nvarchar](50) NULL,
	[GCRQ] [nvarchar](50) NULL,
	[ZYBM] [nvarchar](100) NULL,
	[ZYR] [nvarchar](100) NULL,
	[ZRR] [nvarchar](100) NULL,
	[Note] [nvarchar](4000) NULL,
 CONSTRAINT [OA_CarInfopk] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_CarFullMoney]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_CarFullMoney](
	[OID] [int] NOT NULL,
	[FK_Car] [nvarchar](100) NULL,
	[CarCardType] [int] NULL,
	[CardNo] [nvarchar](100) NULL,
	[HZDW] [nvarchar](100) NULL,
	[BLDD] [nvarchar](100) NULL,
	[CZQJE] [float] NULL,
	[JE] [float] NULL,
	[CZRQ] [nvarchar](50) NULL,
	[CZGLS] [int] NULL,
	[JBR] [nvarchar](100) NULL,
	[Note] [nvarchar](4000) NULL,
 CONSTRAINT [OA_CarFullMoneypk] PRIMARY KEY CLUSTERED 
(
	[OID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_CarFeiYongGuanLi]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_CarFeiYongGuanLi](
	[OID] [int] NOT NULL,
	[FK_car] [nvarchar](100) NULL,
	[JingBanRen] [nvarchar](100) NULL,
	[FeiYongMingCheng] [int] NULL,
	[FeiYongJE] [float] NULL,
	[FeiYongRiQi] [nvarchar](50) NULL,
	[SuoShuNY] [nvarchar](100) NULL,
	[BeiZhu] [nvarchar](4000) NULL,
	[MyFK] [nvarchar](100) NULL,
 CONSTRAINT [OA_CarFeiYongGuanLipk] PRIMARY KEY CLUSTERED 
(
	[OID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_CarDriver]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_CarDriver](
	[No] [nvarchar](3) NOT NULL,
	[Name] [nvarchar](100) NULL,
	[ZJCX] [nvarchar](100) NULL,
	[LZRQ] [nvarchar](50) NULL,
	[JZDQR] [nvarchar](50) NULL,
	[CCCS] [int] NULL,
	[WZCS] [int] NULL,
	[FKZJE] [float] NULL,
	[BZ] [nvarchar](4000) NULL,
 CONSTRAINT [OA_CarDriverpk] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_CarChuBao]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_CarChuBao](
	[OID] [int] NOT NULL,
	[FK_car] [nvarchar](100) NULL,
	[BaoXianGongSi2] [int] NULL,
	[LiPeiJE] [float] NULL,
	[LiPeiDate] [nvarchar](50) NULL,
	[ShiGuDiDian] [nvarchar](100) NULL,
	[ShiGuDate] [nvarchar](50) NULL,
	[FK_JiaShiYuan] [nvarchar](100) NULL,
	[JinBanRen] [nvarchar](100) NULL,
	[ZeRenRen] [nvarchar](100) NULL,
	[ZeRenFang] [nvarchar](100) NULL,
	[ChuLiZhuangTai] [nvarchar](100) NULL,
	[BeiZhu] [nvarchar](4000) NULL,
 CONSTRAINT [OA_CarChuBaopk] PRIMARY KEY CLUSTERED 
(
	[OID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_CarBaoYang]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_CarBaoYang](
	[OID] [int] NOT NULL,
	[FK_Car] [nvarchar](100) NULL,
	[BYDH] [nvarchar](100) NULL,
	[BYRQ] [nvarchar](50) NULL,
	[XCBYRQ] [nvarchar](50) NULL,
	[BYJE] [float] NULL,
	[BYGS] [nvarchar](100) NULL,
	[BYGLS] [float] NULL,
	[DQBYLC] [float] NULL,
	[XCBYLC] [float] NULL,
	[JSZT] [nvarchar](100) NULL,
	[JBR] [nvarchar](100) NULL,
	[ZRR] [nvarchar](100) NULL,
	[BZ] [nvarchar](4000) NULL,
 CONSTRAINT [OA_CarBaoYangpk] PRIMARY KEY CLUSTERED 
(
	[OID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_CalendarTaskCatagory]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_CalendarTaskCatagory](
	[No] [nvarchar](2) NOT NULL,
	[Name] [nvarchar](100) NULL,
 CONSTRAINT [OA_CalendarTaskCatagorypk] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_CalendarTask]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_CalendarTask](
	[OID] [int] NOT NULL,
	[FK_UserNo] [nvarchar](50) NULL,
	[CalendarCatagory] [int] NULL,
	[Title] [nvarchar](100) NULL,
	[StartDate] [nvarchar](50) NULL,
	[EndDate] [nvarchar](50) NULL,
	[Location] [nvarchar](10) NULL,
	[Notes] [nvarchar](100) NULL,
	[Url] [nvarchar](100) NULL,
	[IsAllDay] [int] NULL,
	[Reminder] [nvarchar](100) NULL,
	[IsNew] [int] NULL,
 CONSTRAINT [OA_CalendarTaskpk] PRIMARY KEY CLUSTERED 
(
	[OID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_BBSUserInfo]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_BBSUserInfo](
	[No] [nvarchar](4) NOT NULL,
	[FK_Emp] [nvarchar](30) NULL,
	[UserImg] [nvarchar](100) NULL,
	[Exp] [int] NULL,
	[Integral] [int] NULL,
	[Gold] [int] NULL,
	[ThemeCount] [int] NULL,
	[ReplayCount] [int] NULL,
	[RegDate] [nvarchar](50) NULL,
	[LastDate] [nvarchar](50) NULL,
	[UserGroup] [int] NULL,
	[UserLevel] [int] NULL,
 CONSTRAINT [OA_BBSUserInfopk] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_BBSReplay]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_BBSReplay](
	[No] [nvarchar](2) NOT NULL,
	[Name] [nvarchar](200) NULL,
	[FK_Motif] [int] NULL,
	[Contents] [nvarchar](2000) NULL,
	[FK_Emp] [nvarchar](100) NULL,
	[Ranking] [int] NULL,
	[AddTime] [nvarchar](50) NULL,
	[LastEditTime] [nvarchar](50) NULL,
 CONSTRAINT [OA_BBSReplaypk] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_BBSMotif]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_BBSMotif](
	[No] [nvarchar](2) NOT NULL,
	[Name] [nvarchar](1000) NULL,
	[FK_Class] [nvarchar](100) NULL,
	[Contents] [nvarchar](4000) NULL,
	[FK_Emp] [nvarchar](100) NULL,
	[RDT] [nvarchar](50) NULL,
	[Hits] [int] NULL,
	[Replays] [int] NULL,
	[LastReTime] [nvarchar](50) NULL,
	[LastReEmp] [nvarchar](100) NULL,
	[IsAllTop] [int] NULL,
	[IsTop] [int] NULL,
	[IsWonderful] [int] NULL,
 CONSTRAINT [OA_BBSMotifpk] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_BBSConfig]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_BBSConfig](
	[OID] [int] NOT NULL,
	[WebName] [nvarchar](200) NULL,
	[WebSite] [nvarchar](200) NULL,
	[KeyWords] [nvarchar](200) NULL,
	[Filter] [nvarchar](200) NULL,
	[F_exp] [int] NULL,
	[F_integral] [int] NULL,
	[F_gold] [int] NULL,
	[R_exp] [int] NULL,
	[R_integral] [int] NULL,
	[R_gold] [int] NULL,
	[WaterSet] [int] NULL,
	[WaterTxt] [nvarchar](200) NULL,
	[Description] [nvarchar](2000) NULL,
 CONSTRAINT [OA_BBSConfigpk] PRIMARY KEY CLUSTERED 
(
	[OID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_BBSClass]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_BBSClass](
	[No] [nvarchar](4) NOT NULL,
	[Name] [nvarchar](100) NULL,
	[ParentNo] [nvarchar](100) NULL,
	[TreeNo] [nvarchar](100) NULL,
	[FK_Emp] [nvarchar](100) NULL,
	[RDT] [nvarchar](50) NULL,
	[ImgUrl] [nvarchar](1000) NULL,
	[ThemeCount] [int] NULL,
	[ReplyCount] [int] NULL,
	[ReMark] [nvarchar](4000) NULL,
	[Idx] [int] NULL,
	[IsDir] [int] NULL,
 CONSTRAINT [OA_BBSClasspk] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_ArticleImage]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_ArticleImage](
	[OID] [int] NOT NULL,
	[AddTime] [nvarchar](50) NULL,
	[Path] [nvarchar](200) NULL,
 CONSTRAINT [OA_ArticleImagepk] PRIMARY KEY CLUSTERED 
(
	[OID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_ArticleCatagory]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_ArticleCatagory](
	[No] [nvarchar](2) NOT NULL,
	[Name] [nvarchar](100) NULL,
 CONSTRAINT [OA_ArticleCatagorypk] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[OA_ArticleCatagory] ([No], [Name]) VALUES (N'01', N'体育新闻')
INSERT [dbo].[OA_ArticleCatagory] ([No], [Name]) VALUES (N'02', N'娱乐新闻')
INSERT [dbo].[OA_ArticleCatagory] ([No], [Name]) VALUES (N'03', N'财经新闻')
INSERT [dbo].[OA_ArticleCatagory] ([No], [Name]) VALUES (N'04', N'社会新闻')
INSERT [dbo].[OA_ArticleCatagory] ([No], [Name]) VALUES (N'05', N'时政新闻')
INSERT [dbo].[OA_ArticleCatagory] ([No], [Name]) VALUES (N'06', N'驰骋动态')
/****** Object:  Table [dbo].[OA_Article]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_Article](
	[OID] [int] NOT NULL,
	[FK_UserNo] [nvarchar](50) NULL,
	[FK_ArticleCatagory] [nvarchar](100) NULL,
	[AddTime] [nvarchar](50) NULL,
	[Title] [nvarchar](100) NULL,
	[KeyWords] [nvarchar](100) NULL,
	[ArticleSource] [nvarchar](100) NULL,
	[BrowseCount] [int] NULL,
	[AttachFile] [nvarchar](4000) NULL,
	[Doc] [nvarchar](4000) NULL,
	[SetTop] [nvarchar](50) NULL,
 CONSTRAINT [OA_Articlepk] PRIMARY KEY CLUSTERED 
(
	[OID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OA_ALEmp]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OA_ALEmp](
	[FK_Emp] [nvarchar](20) NULL,
	[No] [nvarchar](50) NOT NULL,
	[Name] [nvarchar](100) NULL,
	[Email] [nvarchar](100) NULL,
	[Tel] [nvarchar](20) NULL,
	[FaxNo] [nvarchar](20) NULL,
	[Address] [nvarchar](20) NULL,
	[Birthday] [nvarchar](50) NULL,
	[MyHomePage] [nvarchar](20) NULL,
	[Organization] [nvarchar](20) NULL,
	[Dept] [nvarchar](20) NULL,
	[PosiTion] [nvarchar](20) NULL,
	[Role] [nvarchar](20) NULL,
	[Remarks] [nvarchar](4000) NULL,
 CONSTRAINT [OA_ALEmppk] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Notice]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Notice](
	[OID] [int] NOT NULL,
	[FQR] [nvarchar](100) NULL,
	[SSBM] [nvarchar](100) NULL,
	[FQSJ] [nvarchar](50) NULL,
	[BT] [nvarchar](100) NULL,
	[GGNR] [nvarchar](1000) NULL,
	[ISBM] [int] NULL,
	[BMBH] [nvarchar](5) NULL,
	[WorkId] [nvarchar](100) NULL,
 CONSTRAINT [Noticepk] PRIMARY KEY CLUSTERED 
(
	[OID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ND3Track]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ND3Track](
	[MyPK] [int] NOT NULL,
	[ActionType] [int] NULL,
	[ActionTypeText] [nvarchar](30) NULL,
	[FID] [int] NULL,
	[WorkID] [int] NULL,
	[NDFrom] [int] NULL,
	[NDFromT] [nvarchar](300) NULL,
	[NDTo] [int] NULL,
	[NDToT] [nvarchar](999) NULL,
	[EmpFrom] [nvarchar](20) NULL,
	[EmpFromT] [nvarchar](30) NULL,
	[EmpTo] [nvarchar](2000) NULL,
	[EmpToT] [nvarchar](2000) NULL,
	[RDT] [nvarchar](20) NULL,
	[WorkTimeSpan] [float] NULL,
	[Msg] [nvarchar](4000) NULL,
	[NodeData] [nvarchar](4000) NULL,
	[Tag] [nvarchar](300) NULL,
	[Exer] [nvarchar](200) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ND3Rpt]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ND3Rpt](
	[AtPara] [nvarchar](4000) NULL,
	[BillNo] [nvarchar](100) NULL,
	[CDT] [nvarchar](50) NULL,
	[CFlowNo] [nvarchar](3) NULL,
	[CWorkID] [int] NULL,
	[Emps] [nvarchar](400) NULL,
	[FID] [int] NULL,
	[FK_Dept] [nvarchar](100) NULL,
	[FK_NY] [nvarchar](100) NULL,
	[FlowDaySpan] [float] NULL,
	[FlowEmps] [nvarchar](1000) NULL,
	[FlowEnder] [nvarchar](100) NULL,
	[FlowEnderRDT] [nvarchar](50) NULL,
	[FlowEndNode] [int] NULL,
	[FlowNote] [nvarchar](500) NULL,
	[FlowStarter] [nvarchar](100) NULL,
	[FlowStartRDT] [nvarchar](50) NULL,
	[GUID] [nvarchar](32) NULL,
	[MyNum] [int] NULL,
	[OID] [int] NOT NULL,
	[PEmp] [nvarchar](32) NULL,
	[PFlowNo] [nvarchar](3) NULL,
	[PNodeID] [int] NULL,
	[PRI] [int] NULL,
	[PrjName] [nvarchar](100) NULL,
	[PrjNo] [nvarchar](100) NULL,
	[PWorkID] [int] NULL,
	[RDT] [nvarchar](50) NULL,
	[Rec] [nvarchar](20) NULL,
	[Title] [nvarchar](200) NULL,
	[WFSta] [int] NULL,
	[WFState] [int] NULL,
 CONSTRAINT [ND3Rptpk] PRIMARY KEY CLUSTERED 
(
	[OID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ND302]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ND302](
	[CDT] [nvarchar](50) NULL,
	[Emps] [nvarchar](400) NULL,
	[FID] [int] NULL,
	[FK_Dept] [nvarchar](32) NULL,
	[OID] [int] NOT NULL,
	[RDT] [nvarchar](50) NULL,
	[Rec] [nvarchar](20) NULL,
 CONSTRAINT [ND302pk] PRIMARY KEY CLUSTERED 
(
	[OID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ND2Track]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ND2Track](
	[MyPK] [int] NOT NULL,
	[ActionType] [int] NULL,
	[ActionTypeText] [nvarchar](30) NULL,
	[FID] [int] NULL,
	[WorkID] [int] NULL,
	[NDFrom] [int] NULL,
	[NDFromT] [nvarchar](300) NULL,
	[NDTo] [int] NULL,
	[NDToT] [nvarchar](999) NULL,
	[EmpFrom] [nvarchar](20) NULL,
	[EmpFromT] [nvarchar](30) NULL,
	[EmpTo] [nvarchar](2000) NULL,
	[EmpToT] [nvarchar](2000) NULL,
	[RDT] [nvarchar](20) NULL,
	[WorkTimeSpan] [float] NULL,
	[Msg] [nvarchar](4000) NULL,
	[NodeData] [nvarchar](4000) NULL,
	[Tag] [nvarchar](300) NULL,
	[Exer] [nvarchar](200) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ND2Rpt]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ND2Rpt](
	[AtPara] [nvarchar](4000) NULL,
	[BillNo] [nvarchar](100) NULL,
	[CDT] [nvarchar](50) NULL,
	[CFlowNo] [nvarchar](3) NULL,
	[CWorkID] [int] NULL,
	[Emps] [nvarchar](400) NULL,
	[FID] [int] NULL,
	[FK_Dept] [nvarchar](100) NULL,
	[FK_NY] [nvarchar](100) NULL,
	[FlowDaySpan] [float] NULL,
	[FlowEmps] [nvarchar](1000) NULL,
	[FlowEnder] [nvarchar](100) NULL,
	[FlowEnderRDT] [nvarchar](50) NULL,
	[FlowEndNode] [int] NULL,
	[FlowNote] [nvarchar](500) NULL,
	[FlowStarter] [nvarchar](100) NULL,
	[FlowStartRDT] [nvarchar](50) NULL,
	[GUID] [nvarchar](32) NULL,
	[MyNum] [int] NULL,
	[OID] [int] NOT NULL,
	[PEmp] [nvarchar](32) NULL,
	[PFlowNo] [nvarchar](3) NULL,
	[PNodeID] [int] NULL,
	[PRI] [int] NULL,
	[PrjName] [nvarchar](100) NULL,
	[PrjNo] [nvarchar](100) NULL,
	[PWorkID] [int] NULL,
	[RDT] [nvarchar](50) NULL,
	[Rec] [nvarchar](20) NULL,
	[Title] [nvarchar](200) NULL,
	[WFSta] [int] NULL,
	[WFState] [int] NULL,
 CONSTRAINT [ND2Rptpk] PRIMARY KEY CLUSTERED 
(
	[OID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ND29Rpt]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ND29Rpt](
	[OID] [int] NOT NULL,
	[BT] [nvarchar](100) NULL,
	[QQR] [nvarchar](100) NULL,
	[OP_Dept] [nvarchar](100) NULL,
	[WFState] [nvarchar](100) NULL,
	[FlowEndNode] [nvarchar](100) NULL,
	[LCZT] [int] NULL,
 CONSTRAINT [ND29Rptpk] PRIMARY KEY CLUSTERED 
(
	[OID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ND202]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ND202](
	[CDT] [nvarchar](50) NULL,
	[Emps] [nvarchar](400) NULL,
	[FID] [int] NULL,
	[FK_Dept] [nvarchar](32) NULL,
	[OID] [int] NOT NULL,
	[RDT] [nvarchar](50) NULL,
	[Rec] [nvarchar](20) NULL,
 CONSTRAINT [ND202pk] PRIMARY KEY CLUSTERED 
(
	[OID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ND1Track]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ND1Track](
	[MyPK] [int] NOT NULL,
	[ActionType] [int] NULL,
	[ActionTypeText] [nvarchar](30) NULL,
	[FID] [int] NULL,
	[WorkID] [int] NULL,
	[NDFrom] [int] NULL,
	[NDFromT] [nvarchar](300) NULL,
	[NDTo] [int] NULL,
	[NDToT] [nvarchar](999) NULL,
	[EmpFrom] [nvarchar](20) NULL,
	[EmpFromT] [nvarchar](30) NULL,
	[EmpTo] [nvarchar](2000) NULL,
	[EmpToT] [nvarchar](2000) NULL,
	[RDT] [nvarchar](20) NULL,
	[WorkTimeSpan] [float] NULL,
	[Msg] [nvarchar](4000) NULL,
	[NodeData] [nvarchar](4000) NULL,
	[Tag] [nvarchar](300) NULL,
	[Exer] [nvarchar](200) NULL,
 CONSTRAINT [WF_Trackpk] PRIMARY KEY CLUSTERED 
(
	[MyPK] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ND1Rpt]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ND1Rpt](
	[AtPara] [nvarchar](4000) NULL,
	[BillNo] [nvarchar](100) NULL,
	[CDT] [nvarchar](50) NULL,
	[CFlowNo] [nvarchar](3) NULL,
	[CWorkID] [int] NULL,
	[Emps] [nvarchar](400) NULL,
	[FID] [int] NULL,
	[FK_Dept] [nvarchar](100) NULL,
	[FK_NY] [nvarchar](100) NULL,
	[FlowDaySpan] [float] NULL,
	[FlowEmps] [nvarchar](1000) NULL,
	[FlowEnder] [nvarchar](100) NULL,
	[FlowEnderRDT] [nvarchar](50) NULL,
	[FlowEndNode] [int] NULL,
	[FlowNote] [nvarchar](500) NULL,
	[FlowStarter] [nvarchar](100) NULL,
	[FlowStartRDT] [nvarchar](50) NULL,
	[GUID] [nvarchar](32) NULL,
	[MyNum] [int] NULL,
	[OID] [int] NOT NULL,
	[PEmp] [nvarchar](32) NULL,
	[PFlowNo] [nvarchar](3) NULL,
	[PNodeID] [int] NULL,
	[PRI] [int] NULL,
	[PrjName] [nvarchar](100) NULL,
	[PrjNo] [nvarchar](100) NULL,
	[PWorkID] [int] NULL,
	[RDT] [nvarchar](50) NULL,
	[Rec] [nvarchar](20) NULL,
	[Title] [nvarchar](200) NULL,
	[WFSta] [int] NULL,
	[WFState] [int] NULL,
 CONSTRAINT [ND1Rptpk] PRIMARY KEY CLUSTERED 
(
	[OID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ND102]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ND102](
	[CDT] [nvarchar](50) NULL,
	[Emps] [nvarchar](400) NULL,
	[FID] [int] NULL,
	[FK_Dept] [nvarchar](32) NULL,
	[OID] [int] NOT NULL,
	[RDT] [nvarchar](50) NULL,
	[Rec] [nvarchar](20) NULL,
 CONSTRAINT [ND102pk] PRIMARY KEY CLUSTERED 
(
	[OID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[KM_TreeStation]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[KM_TreeStation](
	[RefTreeNo] [nvarchar](100) NOT NULL,
	[FK_Station] [nvarchar](100) NOT NULL,
 CONSTRAINT [KM_TreeStationpk] PRIMARY KEY CLUSTERED 
(
	[RefTreeNo] ASC,
	[FK_Station] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[KM_TreeEmp]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[KM_TreeEmp](
	[RefTreeNo] [nvarchar](100) NOT NULL,
	[FK_Emp] [nvarchar](100) NOT NULL,
 CONSTRAINT [KM_TreeEmppk] PRIMARY KEY CLUSTERED 
(
	[RefTreeNo] ASC,
	[FK_Emp] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[KM_TreeDept]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[KM_TreeDept](
	[RefTreeNo] [nvarchar](100) NOT NULL,
	[FK_Dept] [nvarchar](100) NOT NULL,
 CONSTRAINT [KM_TreeDeptpk] PRIMARY KEY CLUSTERED 
(
	[RefTreeNo] ASC,
	[FK_Dept] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[KM_Tree]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[KM_Tree](
	[No] [nvarchar](4) NOT NULL,
	[Name] [nvarchar](100) NULL,
	[ParentNo] [nvarchar](100) NULL,
	[TreeNo] [nvarchar](100) NULL,
	[FK_Emp] [nvarchar](100) NULL,
	[RDT] [nvarchar](50) NULL,
	[Idx] [int] NULL,
	[IsDir] [int] NULL,
	[KMTreeCtrlWay] [int] NULL,
	[CtrlWayPara] [nvarchar](4000) NULL,
	[NumOfInfos] [int] NULL,
	[IsEnable] [int] NULL,
	[FileStatus] [int] NULL,
	[IsShare] [int] NULL,
	[FileSize] [float] NULL,
 CONSTRAINT [KM_Treepk] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[KM_FileInfo]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[KM_FileInfo](
	[MyPK] [nvarchar](100) NOT NULL,
	[RefTreeNo] [nvarchar](100) NULL,
	[TreeNo] [nvarchar](100) NULL,
	[Title] [nvarchar](100) NULL,
	[Doc] [nvarchar](4000) NULL,
	[KeyWords] [nvarchar](2000) NULL,
	[FK_Emp] [nvarchar](100) NULL,
	[FK_Dept] [nvarchar](100) NULL,
	[RDT] [nvarchar](50) NULL,
	[EDTER] [nvarchar](100) NULL,
	[EDT] [nvarchar](50) NULL,
	[Idx] [int] NULL,
	[ReadTimes] [int] NULL,
	[DownLoadTimes] [int] NULL,
	[IsDownload] [int] NULL,
	[UploadPath] [nvarchar](2000) NULL,
	[FileExt] [nvarchar](10) NULL,
	[FileSize] [float] NULL,
	[FileStatus] [int] NULL,
	[IsShare] [int] NULL,
	[IsEnable] [int] NULL,
	[KMTreeCtrlWay] [int] NULL,
 CONSTRAINT [KM_FileInfopk] PRIMARY KEY CLUSTERED 
(
	[MyPK] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[HR_ZDSort]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HR_ZDSort](
	[No] [nvarchar](2) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[IDX] [int] NULL,
 CONSTRAINT [HR_ZDSortpk] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[HR_ZDSort] ([No], [Name], [IDX]) VALUES (N'01', N'国家法律法规', 0)
INSERT [dbo].[HR_ZDSort] ([No], [Name], [IDX]) VALUES (N'02', N'上级制度', 0)
INSERT [dbo].[HR_ZDSort] ([No], [Name], [IDX]) VALUES (N'03', N'单位制度', 0)
INSERT [dbo].[HR_ZDSort] ([No], [Name], [IDX]) VALUES (N'04', N'二级制度', 0)
/****** Object:  Table [dbo].[GPM_UserMenu]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GPM_UserMenu](
	[FK_Emp] [nvarchar](50) NOT NULL,
	[FK_Menu] [nvarchar](50) NOT NULL,
	[IsChecked] [int] NULL,
 CONSTRAINT [GPM_UserMenupk] PRIMARY KEY CLUSTERED 
(
	[FK_Emp] ASC,
	[FK_Menu] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'admin', N'100', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'admin', N'101', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'admin', N'1010', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'admin', N'1011', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'admin', N'1012', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'admin', N'1013', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'admin', N'1014', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'admin', N'1015', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'admin', N'1016', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'admin', N'1017', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'admin', N'1018', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'admin', N'1019', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'admin', N'1020', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'admin', N'1021', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'admin', N'1022', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'admin', N'1024', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'admin', N'1025', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'admin', N'1026', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'admin', N'1027', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'admin', N'1028', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'admin', N'1029', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'admin', N'1030', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'admin', N'1031', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'admin', N'1032', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'admin', N'1033', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'admin', N'1034', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'admin', N'1036', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'admin', N'1037', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'admin', N'1038', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'admin', N'1039', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'admin', N'1040', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'admin', N'106', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'admin', N'108', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'admin', N'110', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'admin', N'111', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'admin', N'112', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'admin', N'113', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'admin', N'114', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'admin', N'115', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'admin', N'2000', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'admin', N'2002', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'admin', N'2004', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'admin', N'2005', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'admin', N'2006', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'admin', N'2007', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'admin', N'2008', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'admin', N'2010', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'admin', N'2011', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'admin', N'2101', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'admin', N'2111', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'admin', N'2112', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'admin', N'2113', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'admin', N'2114', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'zhoupeng', N'100', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'zhoupeng', N'101', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'zhoupeng', N'1010', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'zhoupeng', N'1011', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'zhoupeng', N'1012', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'zhoupeng', N'1013', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'zhoupeng', N'1014', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'zhoupeng', N'1015', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'zhoupeng', N'1016', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'zhoupeng', N'1017', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'zhoupeng', N'1018', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'zhoupeng', N'1019', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'zhoupeng', N'1020', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'zhoupeng', N'1021', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'zhoupeng', N'1022', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'zhoupeng', N'1024', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'zhoupeng', N'1025', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'zhoupeng', N'1026', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'zhoupeng', N'1027', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'zhoupeng', N'1028', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'zhoupeng', N'1029', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'zhoupeng', N'1030', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'zhoupeng', N'1031', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'zhoupeng', N'1032', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'zhoupeng', N'1033', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'zhoupeng', N'1034', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'zhoupeng', N'1036', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'zhoupeng', N'1037', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'zhoupeng', N'1038', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'zhoupeng', N'1039', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'zhoupeng', N'1040', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'zhoupeng', N'106', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'zhoupeng', N'107', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'zhoupeng', N'108', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'zhoupeng', N'2000', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'zhoupeng', N'2002', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'zhoupeng', N'2004', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'zhoupeng', N'2005', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'zhoupeng', N'2006', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'zhoupeng', N'2007', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'zhoupeng', N'2008', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'zhoupeng', N'2010', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'zhoupeng', N'2011', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'zhoupeng', N'2101', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'zhoupeng', N'2111', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'zhoupeng', N'2112', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'zhoupeng', N'2113', 1)
INSERT [dbo].[GPM_UserMenu] ([FK_Emp], [FK_Menu], [IsChecked]) VALUES (N'zhoupeng', N'2114', 1)
GO
print 'Processed 100 total records'
/****** Object:  Table [dbo].[GPM_SystemMenuLog]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GPM_SystemMenuLog](
	[OID] [int] NOT NULL,
	[FK_Emp] [nvarchar](100) NULL,
	[FK_App] [nvarchar](50) NULL,
	[FK_Menu] [nvarchar](50) NULL,
	[RContent] [nvarchar](3900) NULL,
	[CreateTime] [nvarchar](50) NULL,
 CONSTRAINT [GPM_SystemMenuLogpk] PRIMARY KEY CLUSTERED 
(
	[OID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[GPM_SystemLoginLog]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GPM_SystemLoginLog](
	[OID] [int] NOT NULL,
	[FK_Emp] [nvarchar](20) NULL,
	[FK_App] [nvarchar](20) NULL,
	[RContent] [nvarchar](2000) NULL,
	[LoginDateTime] [nvarchar](50) NULL,
	[IP] [nvarchar](200) NULL,
 CONSTRAINT [GPM_SystemLoginLogpk] PRIMARY KEY CLUSTERED 
(
	[OID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[GPM_StationMenu]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GPM_StationMenu](
	[FK_Station] [nvarchar](50) NOT NULL,
	[FK_Menu] [nvarchar](50) NOT NULL,
	[IsChecked] [int] NULL,
 CONSTRAINT [GPM_StationMenupk] PRIMARY KEY CLUSTERED 
(
	[FK_Station] ASC,
	[FK_Menu] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[GPM_PerSetting]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GPM_PerSetting](
	[MyPK] [nvarchar](100) NOT NULL,
	[FK_Emp] [nvarchar](200) NULL,
	[FK_App] [nvarchar](200) NULL,
	[UserNo] [nvarchar](200) NULL,
	[UserPass] [nvarchar](200) NULL,
	[Idx] [int] NULL,
 CONSTRAINT [GPM_PerSettingpk] PRIMARY KEY CLUSTERED 
(
	[MyPK] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[GPM_Menu]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GPM_Menu](
	[No] [nvarchar](4) NOT NULL,
	[Name] [nvarchar](300) NULL,
	[ParentNo] [nvarchar](100) NULL,
	[TreeNo] [nvarchar](60) NULL,
	[IsDir] [int] NULL,
	[Idx] [int] NULL,
	[MenuType] [int] NULL,
	[FK_App] [nvarchar](100) NULL,
	[Url] [nvarchar](3900) NULL,
	[IsEnable] [int] NULL,
	[OpenWay] [int] NULL,
	[Flag] [nvarchar](500) NULL,
	[Tag1] [nvarchar](500) NULL,
	[Tag2] [nvarchar](500) NULL,
	[Tag3] [nvarchar](500) NULL,
	[WebPath] [nvarchar](200) NULL,
	[MyFileName] [nvarchar](100) NULL,
	[MyFilePath] [nvarchar](100) NULL,
	[MyFileExt] [nvarchar](10) NULL,
	[MyFileH] [int] NULL,
	[MyFileW] [int] NULL,
	[MyFileSize] [float] NULL,
 CONSTRAINT [GPM_Menupk] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[GPM_Menu] ([No], [Name], [ParentNo], [TreeNo], [IsDir], [Idx], [MenuType], [FK_App], [Url], [IsEnable], [OpenWay], [Flag], [Tag1], [Tag2], [Tag3], [WebPath], [MyFileName], [MyFilePath], [MyFileExt], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'100', N'BPM系统', N'2000', N'0110', 1, 0, 2, N'CCFlowBPM', N'', 1, 0, N'', N'', N'', N'', N'/WF/Img/FileType/IE.gif', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_Menu] ([No], [Name], [ParentNo], [TreeNo], [IsDir], [Idx], [MenuType], [FK_App], [Url], [IsEnable], [OpenWay], [Flag], [Tag1], [Tag2], [Tag3], [WebPath], [MyFileName], [MyFilePath], [MyFileExt], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1000', N'济南驰骋信息技术有限公司', N'0', N'01', 1, NULL, 0, N'UnitFullName', N'', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[GPM_Menu] ([No], [Name], [ParentNo], [TreeNo], [IsDir], [Idx], [MenuType], [FK_App], [Url], [IsEnable], [OpenWay], [Flag], [Tag1], [Tag2], [Tag3], [WebPath], [MyFileName], [MyFilePath], [MyFileExt], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'101', N'功能目录1', N'100', N'011010', 1, 0, 3, N'CCFlowBPM', N'', 1, 0, N'', N'', N'', N'', N'/WF/Img/FileType/IE.gif', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_Menu] ([No], [Name], [ParentNo], [TreeNo], [IsDir], [Idx], [MenuType], [FK_App], [Url], [IsEnable], [OpenWay], [Flag], [Tag1], [Tag2], [Tag3], [WebPath], [MyFileName], [MyFilePath], [MyFileExt], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1010', N'我的工作', N'2002', N'0101', 1, 0, 3, N'CCOA', N'', 1, NULL, N'', N'', N'', N'', N'', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_Menu] ([No], [Name], [ParentNo], [TreeNo], [IsDir], [Idx], [MenuType], [FK_App], [Url], [IsEnable], [OpenWay], [Flag], [Tag1], [Tag2], [Tag3], [WebPath], [MyFileName], [MyFilePath], [MyFileExt], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1011', N'我的计划', N'1010', N'010101', 0, 2, 4, N'CCOA', N'/App/PrivPlan/MyPlan.aspx', 1, NULL, N'', N'', N'', N'', N'//DataUser/BP.GPM.Menu/1011.gif', N'files.gif', N'', N'gif', 16, 16, 0)
INSERT [dbo].[GPM_Menu] ([No], [Name], [ParentNo], [TreeNo], [IsDir], [Idx], [MenuType], [FK_App], [Url], [IsEnable], [OpenWay], [Flag], [Tag1], [Tag2], [Tag3], [WebPath], [MyFileName], [MyFilePath], [MyFileExt], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1012', N'公告通知', N'2002', N'0102', 1, 0, 3, N'CCOA', N'', 1, NULL, N'', N'', N'', N'', N'', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_Menu] ([No], [Name], [ParentNo], [TreeNo], [IsDir], [Idx], [MenuType], [FK_App], [Url], [IsEnable], [OpenWay], [Flag], [Tag1], [Tag2], [Tag3], [WebPath], [MyFileName], [MyFilePath], [MyFileExt], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1013', N'公告管理', N'1012', N'010201', 1, 0, 4, N'CCOA', N'/App/Notice/NoticeList.aspx', 1, NULL, N'', N'', N'', N'', N'//DataUser/BP.GPM.Menu/1013.gif', N'framework.gif', N'', N'gif', 16, 16, 0)
INSERT [dbo].[GPM_Menu] ([No], [Name], [ParentNo], [TreeNo], [IsDir], [Idx], [MenuType], [FK_App], [Url], [IsEnable], [OpenWay], [Flag], [Tag1], [Tag2], [Tag3], [WebPath], [MyFileName], [MyFilePath], [MyFileExt], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1014', N'添加公告', N'1012', N'10202', 0, 0, 4, N'CCOA', N'/App/Notice/NewNotice.aspx', 1, NULL, N'', N'', N'', N'', N'//DataUser/BP.GPM.Menu/1014.gif', N'new3.gif', N'', N'gif', 16, 16, 0)
INSERT [dbo].[GPM_Menu] ([No], [Name], [ParentNo], [TreeNo], [IsDir], [Idx], [MenuType], [FK_App], [Url], [IsEnable], [OpenWay], [Flag], [Tag1], [Tag2], [Tag3], [WebPath], [MyFileName], [MyFilePath], [MyFileExt], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1015', N'我的公告', N'1012', N'10203', 0, 0, 4, N'CCOA', N'/App/Notice/MyNoticeList.aspx', 1, NULL, N'', N'', N'', N'', N'//DataUser/BP.GPM.Menu/1015.gif', N'data3.gif', N'', N'gif', 16, 16, 0)
INSERT [dbo].[GPM_Menu] ([No], [Name], [ParentNo], [TreeNo], [IsDir], [Idx], [MenuType], [FK_App], [Url], [IsEnable], [OpenWay], [Flag], [Tag1], [Tag2], [Tag3], [WebPath], [MyFileName], [MyFilePath], [MyFileExt], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1016', N'邮件管理', N'2002', N'0103', 1, 0, 3, N'CCOA', N'', 1, NULL, N'', N'', N'', N'', N'', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_Menu] ([No], [Name], [ParentNo], [TreeNo], [IsDir], [Idx], [MenuType], [FK_App], [Url], [IsEnable], [OpenWay], [Flag], [Tag1], [Tag2], [Tag3], [WebPath], [MyFileName], [MyFilePath], [MyFileExt], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1017', N'写邮件', N'1016', N'010301', 0, 0, 4, N'CCOA', N'/App/Message/SendMsg.aspx', 1, NULL, N'', N'', N'', N'', N'//DataUser/BP.GPM.Menu/1017.png', N'add.png', N'', N'png', 16, 16, 0)
INSERT [dbo].[GPM_Menu] ([No], [Name], [ParentNo], [TreeNo], [IsDir], [Idx], [MenuType], [FK_App], [Url], [IsEnable], [OpenWay], [Flag], [Tag1], [Tag2], [Tag3], [WebPath], [MyFileName], [MyFilePath], [MyFileExt], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1018', N'收件箱', N'1016', N'10302', 0, 0, 4, N'CCOA', N'/app/message/InBox.aspx', 1, NULL, N'', N'', N'', N'', N'//DataUser/BP.GPM.Menu/1018.gif', N'calendar1.gif', N'', N'gif', 16, 16, 0)
INSERT [dbo].[GPM_Menu] ([No], [Name], [ParentNo], [TreeNo], [IsDir], [Idx], [MenuType], [FK_App], [Url], [IsEnable], [OpenWay], [Flag], [Tag1], [Tag2], [Tag3], [WebPath], [MyFileName], [MyFilePath], [MyFileExt], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1019', N'发件箱', N'1016', N'10303', 0, 0, 4, N'CCOA', N'/app/Message/SendBox.aspx', 1, NULL, N'', N'', N'', N'', N'//DataUser/BP.GPM.Menu/1019.png', N'aremove.png', N'', N'png', 16, 16, 0)
INSERT [dbo].[GPM_Menu] ([No], [Name], [ParentNo], [TreeNo], [IsDir], [Idx], [MenuType], [FK_App], [Url], [IsEnable], [OpenWay], [Flag], [Tag1], [Tag2], [Tag3], [WebPath], [MyFileName], [MyFilePath], [MyFileExt], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'102', N'xxx管理1', N'101', N'01101010', 1, 0, 4, N'CCFlowBPM', N'http://ccflow.org', 1, 0, N'', N'', N'', N'', N'/WF/Img/FileType/IE.gif', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_Menu] ([No], [Name], [ParentNo], [TreeNo], [IsDir], [Idx], [MenuType], [FK_App], [Url], [IsEnable], [OpenWay], [Flag], [Tag1], [Tag2], [Tag3], [WebPath], [MyFileName], [MyFilePath], [MyFileExt], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1020', N'新闻管理', N'2002', N'0104', 1, 0, 3, N'CCOA', N'', 1, NULL, N'', N'', N'', N'', N'', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_Menu] ([No], [Name], [ParentNo], [TreeNo], [IsDir], [Idx], [MenuType], [FK_App], [Url], [IsEnable], [OpenWay], [Flag], [Tag1], [Tag2], [Tag3], [WebPath], [MyFileName], [MyFilePath], [MyFileExt], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1021', N'添加新闻', N'1020', N'010401', 0, 0, 4, N'CCOA', N'/app/News/NewsAdd.aspx', 1, NULL, N'', N'', N'', N'', N'//DataUser/BP.GPM.Menu/1021.gif', N'pen.gif', N'', N'gif', 16, 16, 0)
INSERT [dbo].[GPM_Menu] ([No], [Name], [ParentNo], [TreeNo], [IsDir], [Idx], [MenuType], [FK_App], [Url], [IsEnable], [OpenWay], [Flag], [Tag1], [Tag2], [Tag3], [WebPath], [MyFileName], [MyFilePath], [MyFileExt], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1022', N'新闻管理', N'1020', N'10402', 0, 0, 4, N'CCOA', N'/app/News/NewsList.aspx', 1, NULL, N'', N'', N'', N'', N'//DataUser/BP.GPM.Menu/1022.gif', N'ico_news.gif', N'', N'gif', 16, 16, 0)
INSERT [dbo].[GPM_Menu] ([No], [Name], [ParentNo], [TreeNo], [IsDir], [Idx], [MenuType], [FK_App], [Url], [IsEnable], [OpenWay], [Flag], [Tag1], [Tag2], [Tag3], [WebPath], [MyFileName], [MyFilePath], [MyFileExt], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1024', N'新闻栏目', N'1020', N'10404', 0, 0, 4, N'CCOA', N'/WF/Comm/Search.aspx?EnsName=BP.OA.Article.ArticleCatagorys', 1, NULL, N'', N'', N'', N'', N'//DataUser/BP.GPM.Menu/1024.gif', N'sharedir.gif', N'', N'gif', 16, 16, 0)
INSERT [dbo].[GPM_Menu] ([No], [Name], [ParentNo], [TreeNo], [IsDir], [Idx], [MenuType], [FK_App], [Url], [IsEnable], [OpenWay], [Flag], [Tag1], [Tag2], [Tag3], [WebPath], [MyFileName], [MyFilePath], [MyFileExt], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1025', N'流程管理', N'2002', N'0105', 1, 0, 3, N'CCOA', N'', 1, NULL, N'', N'', N'', N'', N'', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_Menu] ([No], [Name], [ParentNo], [TreeNo], [IsDir], [Idx], [MenuType], [FK_App], [Url], [IsEnable], [OpenWay], [Flag], [Tag1], [Tag2], [Tag3], [WebPath], [MyFileName], [MyFilePath], [MyFileExt], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1026', N'发起', N'1025', N'010501', 1, 0, 3, N'CCOA', N'/WF/Start.aspx', 1, NULL, N'', N'', N'', N'', N'//DataUser/BP.GPM.Menu/1026.gif', N'data1.gif', N'', N'gif', 16, 16, 0)
INSERT [dbo].[GPM_Menu] ([No], [Name], [ParentNo], [TreeNo], [IsDir], [Idx], [MenuType], [FK_App], [Url], [IsEnable], [OpenWay], [Flag], [Tag1], [Tag2], [Tag3], [WebPath], [MyFileName], [MyFilePath], [MyFileExt], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1027', N'待办', N'1025', N'10502', 0, 0, 3, N'CCOA', N'/WF/EmpWorks.aspx', 1, NULL, N'', N'', N'', N'', N'//DataUser/BP.GPM.Menu/1027.png', N'search1.png', N'', N'png', 16, 16, 0)
INSERT [dbo].[GPM_Menu] ([No], [Name], [ParentNo], [TreeNo], [IsDir], [Idx], [MenuType], [FK_App], [Url], [IsEnable], [OpenWay], [Flag], [Tag1], [Tag2], [Tag3], [WebPath], [MyFileName], [MyFilePath], [MyFileExt], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1028', N'抄送', N'1025', N'10503', 0, 0, 3, N'CCOA', N'/WF/CC.aspx', 1, NULL, N'', N'', N'', N'', N'//DataUser/BP.GPM.Menu/1028.gif', N'data1.gif', N'', N'gif', 16, 16, 0)
INSERT [dbo].[GPM_Menu] ([No], [Name], [ParentNo], [TreeNo], [IsDir], [Idx], [MenuType], [FK_App], [Url], [IsEnable], [OpenWay], [Flag], [Tag1], [Tag2], [Tag3], [WebPath], [MyFileName], [MyFilePath], [MyFileExt], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1029', N'挂起', N'1025', N'10504', 0, 0, 3, N'CCOA', N'/WF/HungupList.aspx', 1, NULL, N'', N'', N'', N'', N'//DataUser/BP.GPM.Menu/1029.gif', N'data5.gif', N'', N'gif', 16, 16, 0)
INSERT [dbo].[GPM_Menu] ([No], [Name], [ParentNo], [TreeNo], [IsDir], [Idx], [MenuType], [FK_App], [Url], [IsEnable], [OpenWay], [Flag], [Tag1], [Tag2], [Tag3], [WebPath], [MyFileName], [MyFilePath], [MyFileExt], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'103', N'查看', N'102', N'0110101010', 0, 0, 5, N'CCFlowBPM', N'', 1, 0, N'', N'', N'', N'', N'/WF/Img/FileType/IE.gif', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_Menu] ([No], [Name], [ParentNo], [TreeNo], [IsDir], [Idx], [MenuType], [FK_App], [Url], [IsEnable], [OpenWay], [Flag], [Tag1], [Tag2], [Tag3], [WebPath], [MyFileName], [MyFilePath], [MyFileExt], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1030', N'在途', N'1025', N'10505', 0, 0, 3, N'CCOA', N'/WF/Runing.aspx', 1, NULL, N'', N'', N'', N'', N'//DataUser/BP.GPM.Menu/1030.gif', N'data4.gif', N'', N'gif', 16, 16, 0)
INSERT [dbo].[GPM_Menu] ([No], [Name], [ParentNo], [TreeNo], [IsDir], [Idx], [MenuType], [FK_App], [Url], [IsEnable], [OpenWay], [Flag], [Tag1], [Tag2], [Tag3], [WebPath], [MyFileName], [MyFilePath], [MyFileExt], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1031', N'查询', N'1025', N'10506', 0, 0, 3, N'CCOA', N'/WF/FlowSearch.aspx', 1, NULL, N'', N'', N'', N'', N'//DataUser/BP.GPM.Menu/1031.gif', N'ico_search.gif', N'', N'gif', 16, 16, 0)
INSERT [dbo].[GPM_Menu] ([No], [Name], [ParentNo], [TreeNo], [IsDir], [Idx], [MenuType], [FK_App], [Url], [IsEnable], [OpenWay], [Flag], [Tag1], [Tag2], [Tag3], [WebPath], [MyFileName], [MyFilePath], [MyFileExt], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1032', N'关键字查询', N'1025', N'10507', 0, 0, 3, N'CCOA', N'/WF/KeySearch.aspx', 1, NULL, N'', N'', N'', N'', N'//DataUser/BP.GPM.Menu/1032.gif', N'cache.gif', N'', N'gif', 16, 16, 0)
INSERT [dbo].[GPM_Menu] ([No], [Name], [ParentNo], [TreeNo], [IsDir], [Idx], [MenuType], [FK_App], [Url], [IsEnable], [OpenWay], [Flag], [Tag1], [Tag2], [Tag3], [WebPath], [MyFileName], [MyFilePath], [MyFileExt], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1033', N'取消审批', N'1025', N'10508', 0, 0, 3, N'CCOA', N'/WF/GetTask.aspx', 1, NULL, N'', N'', N'', N'', N'//DataUser/BP.GPM.Menu/1033.gif', N'color.gif', N'', N'gif', 16, 16, 0)
INSERT [dbo].[GPM_Menu] ([No], [Name], [ParentNo], [TreeNo], [IsDir], [Idx], [MenuType], [FK_App], [Url], [IsEnable], [OpenWay], [Flag], [Tag1], [Tag2], [Tag3], [WebPath], [MyFileName], [MyFilePath], [MyFileExt], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1034', N'成员', N'1025', N'10509', 0, 0, 3, N'CCOA', N'/WF/Emps.aspx', 1, NULL, N'', N'', N'', N'', N'//DataUser/BP.GPM.Menu/1034.gif', N'data3.gif', N'', N'gif', 16, 16, 0)
INSERT [dbo].[GPM_Menu] ([No], [Name], [ParentNo], [TreeNo], [IsDir], [Idx], [MenuType], [FK_App], [Url], [IsEnable], [OpenWay], [Flag], [Tag1], [Tag2], [Tag3], [WebPath], [MyFileName], [MyFilePath], [MyFileExt], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1036', N'设置', N'1025', N'10511', 0, 0, 3, N'CCOA', N'/WF/Tools.aspx', 1, NULL, N'', N'', N'', N'', N'//DataUser/BP.GPM.Menu/1036.gif', N'ico_html.gif', N'', N'gif', 16, 16, 0)
INSERT [dbo].[GPM_Menu] ([No], [Name], [ParentNo], [TreeNo], [IsDir], [Idx], [MenuType], [FK_App], [Url], [IsEnable], [OpenWay], [Flag], [Tag1], [Tag2], [Tag3], [WebPath], [MyFileName], [MyFilePath], [MyFileExt], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1037', N'我的日程', N'1010', N'010102', 0, 1, 4, N'CCOA', N'/app/Calendar/MyCalendar.aspx', 1, NULL, N'', N'', N'', N'', N'//DataUser/BP.GPM.Menu/1037.png', N'calendar.png', N'', N'png', 16, 16, 0)
INSERT [dbo].[GPM_Menu] ([No], [Name], [ParentNo], [TreeNo], [IsDir], [Idx], [MenuType], [FK_App], [Url], [IsEnable], [OpenWay], [Flag], [Tag1], [Tag2], [Tag3], [WebPath], [MyFileName], [MyFilePath], [MyFileExt], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1038', N'创建流程', N'1025', N'10512', 0, 0, 3, N'CCOA', N'/WF/Admin/XAP/Designer.aspx', 1, NULL, N'', N'', N'', N'', N'//DataUser/BP.GPM.Menu/1038.gif', N'com.gif', N'', N'gif', 16, 16, 0)
INSERT [dbo].[GPM_Menu] ([No], [Name], [ParentNo], [TreeNo], [IsDir], [Idx], [MenuType], [FK_App], [Url], [IsEnable], [OpenWay], [Flag], [Tag1], [Tag2], [Tag3], [WebPath], [MyFileName], [MyFilePath], [MyFileExt], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1039', N'草稿箱', N'1016', N'10304', 0, 0, 4, N'CCOA', N'/app/Message/DraftBox.aspx', 1, NULL, N'', N'', N'', N'', N'//DataUser/BP.GPM.Menu/1039.gif', N'Rpt.gif', N'', N'gif', 16, 16, 0)
INSERT [dbo].[GPM_Menu] ([No], [Name], [ParentNo], [TreeNo], [IsDir], [Idx], [MenuType], [FK_App], [Url], [IsEnable], [OpenWay], [Flag], [Tag1], [Tag2], [Tag3], [WebPath], [MyFileName], [MyFilePath], [MyFileExt], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'104', N'增加', N'102', N'110101011', 0, 0, 5, N'CCFlowBPM', N'', 1, 0, N'', N'', N'', N'', N'/WF/Img/FileType/IE.gif', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_Menu] ([No], [Name], [ParentNo], [TreeNo], [IsDir], [Idx], [MenuType], [FK_App], [Url], [IsEnable], [OpenWay], [Flag], [Tag1], [Tag2], [Tag3], [WebPath], [MyFileName], [MyFilePath], [MyFileExt], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1040', N'垃圾箱', N'1016', N'10305', 0, 0, 4, N'CCOA', N'/App/Message/RecycleBox.aspx', 1, NULL, N'', N'', N'', N'', N'//DataUser/BP.GPM.Menu/1040.gif', N'admin.gif', N'', N'gif', 17, 19, 0)
INSERT [dbo].[GPM_Menu] ([No], [Name], [ParentNo], [TreeNo], [IsDir], [Idx], [MenuType], [FK_App], [Url], [IsEnable], [OpenWay], [Flag], [Tag1], [Tag2], [Tag3], [WebPath], [MyFileName], [MyFilePath], [MyFileExt], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'105', N'删除', N'102', N'110101012', 0, 0, 5, N'CCFlowBPM', N'', 1, 0, N'', N'', N'', N'', N'/WF/Img/FileType/IE.gif', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_Menu] ([No], [Name], [ParentNo], [TreeNo], [IsDir], [Idx], [MenuType], [FK_App], [Url], [IsEnable], [OpenWay], [Flag], [Tag1], [Tag2], [Tag3], [WebPath], [MyFileName], [MyFilePath], [MyFileExt], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'106', N'人员管理', N'2002', N'0106', 1, 0, 3, N'CCOA', N'', 1, 0, N'', N'', N'', N'', N'/WF/Img/FileType/IE.gif', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_Menu] ([No], [Name], [ParentNo], [TreeNo], [IsDir], [Idx], [MenuType], [FK_App], [Url], [IsEnable], [OpenWay], [Flag], [Tag1], [Tag2], [Tag3], [WebPath], [MyFileName], [MyFilePath], [MyFileExt], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'108', N'人员台帐', N'106', N'010610', 0, 1, 4, N'CCOA', N'/WF/Comm/Search.aspx?EnsName=BP.BM.Emps', 1, 1, N'', N'', N'', N'', N'/WF/Img/FileType/IE.gif', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_Menu] ([No], [Name], [ParentNo], [TreeNo], [IsDir], [Idx], [MenuType], [FK_App], [Url], [IsEnable], [OpenWay], [Flag], [Tag1], [Tag2], [Tag3], [WebPath], [MyFileName], [MyFilePath], [MyFileExt], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'110', N'新建目录', N'2008', N'', 1, 0, 3, N'SSO', N'', 1, 0, N'', N'', N'', N'', N'/WF/Img/FileType/IE.gif', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_Menu] ([No], [Name], [ParentNo], [TreeNo], [IsDir], [Idx], [MenuType], [FK_App], [Url], [IsEnable], [OpenWay], [Flag], [Tag1], [Tag2], [Tag3], [WebPath], [MyFileName], [MyFilePath], [MyFileExt], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'111', N'涉密设备管理', N'2002', N'0107', 1, 0, 3, N'CCOA', N'', 1, 0, N'', N'', N'', N'', N'/WF/Img/FileType/IE.gif', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_Menu] ([No], [Name], [ParentNo], [TreeNo], [IsDir], [Idx], [MenuType], [FK_App], [Url], [IsEnable], [OpenWay], [Flag], [Tag1], [Tag2], [Tag3], [WebPath], [MyFileName], [MyFilePath], [MyFileExt], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'112', N'计算机网路设备台帐', N'111', N'010710', 0, 0, 4, N'CCOA', N'', 1, 0, N'', N'', N'', N'', N'/WF/Img/FileType/IE.gif', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_Menu] ([No], [Name], [ParentNo], [TreeNo], [IsDir], [Idx], [MenuType], [FK_App], [Url], [IsEnable], [OpenWay], [Flag], [Tag1], [Tag2], [Tag3], [WebPath], [MyFileName], [MyFilePath], [MyFileExt], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'113', N'办公信息设备', N'111', N'10711', 0, 0, 4, N'CCOA', N'/WF/Comm/Search.aspx?EnsName=BP.BM.AQCPs', 1, 0, N'', N'', N'', N'', N'/WF/Img/FileType/IE.gif', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_Menu] ([No], [Name], [ParentNo], [TreeNo], [IsDir], [Idx], [MenuType], [FK_App], [Url], [IsEnable], [OpenWay], [Flag], [Tag1], [Tag2], [Tag3], [WebPath], [MyFileName], [MyFilePath], [MyFileExt], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'114', N'涉密载体台帐', N'111', N'10712', 0, 0, 4, N'CCOA', N'', 1, 0, N'', N'', N'', N'', N'/WF/Img/FileType/IE.gif', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_Menu] ([No], [Name], [ParentNo], [TreeNo], [IsDir], [Idx], [MenuType], [FK_App], [Url], [IsEnable], [OpenWay], [Flag], [Tag1], [Tag2], [Tag3], [WebPath], [MyFileName], [MyFilePath], [MyFileExt], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'115', N'安全产品台帐', N'111', N'10713', 0, 0, 4, N'CCOA', N'/WF/Comm/Search.aspx?EnsName=BP.BM.AQCPs', 1, 0, N'', N'', N'', N'', N'/WF/Img/FileType/IE.gif', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_Menu] ([No], [Name], [ParentNo], [TreeNo], [IsDir], [Idx], [MenuType], [FK_App], [Url], [IsEnable], [OpenWay], [Flag], [Tag1], [Tag2], [Tag3], [WebPath], [MyFileName], [MyFilePath], [MyFileExt], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'116', N'教育培训台帐', N'106', N'10611', 0, 2, 4, N'CCOA', N'', 1, 0, N'', N'', N'', N'', N'/WF/Img/FileType/IE.gif', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_Menu] ([No], [Name], [ParentNo], [TreeNo], [IsDir], [Idx], [MenuType], [FK_App], [Url], [IsEnable], [OpenWay], [Flag], [Tag1], [Tag2], [Tag3], [WebPath], [MyFileName], [MyFilePath], [MyFileExt], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'117', N'出国境台帐', N'106', N'10612', 0, 4, 3, N'CCOA', N'', 1, 0, N'', N'', N'', N'', N'/WF/Img/FileType/IE.gif', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_Menu] ([No], [Name], [ParentNo], [TreeNo], [IsDir], [Idx], [MenuType], [FK_App], [Url], [IsEnable], [OpenWay], [Flag], [Tag1], [Tag2], [Tag3], [WebPath], [MyFileName], [MyFilePath], [MyFileExt], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'118', N'宣传报道台帐', N'106', N'10613', 0, 5, 3, N'CCOA', N'', 1, 0, N'', N'', N'', N'', N'/WF/Img/FileType/IE.gif', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_Menu] ([No], [Name], [ParentNo], [TreeNo], [IsDir], [Idx], [MenuType], [FK_App], [Url], [IsEnable], [OpenWay], [Flag], [Tag1], [Tag2], [Tag3], [WebPath], [MyFileName], [MyFilePath], [MyFileExt], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'119', N'出国证照台帐', N'106', N'10614', 0, 3, 3, N'CCOA', N'', 1, 0, N'', N'', N'', N'', N'/WF/Img/FileType/IE.gif', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_Menu] ([No], [Name], [ParentNo], [TreeNo], [IsDir], [Idx], [MenuType], [FK_App], [Url], [IsEnable], [OpenWay], [Flag], [Tag1], [Tag2], [Tag3], [WebPath], [MyFileName], [MyFilePath], [MyFileExt], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2000', N'业务系统', N'1000', N'0101', 1, NULL, 1, N'AppSort', N'', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[GPM_Menu] ([No], [Name], [ParentNo], [TreeNo], [IsDir], [Idx], [MenuType], [FK_App], [Url], [IsEnable], [OpenWay], [Flag], [Tag1], [Tag2], [Tag3], [WebPath], [MyFileName], [MyFilePath], [MyFileExt], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2001', N'办公系统', N'1000', N'0102', 1, NULL, 1, N'AppSort', N'', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[GPM_Menu] ([No], [Name], [ParentNo], [TreeNo], [IsDir], [Idx], [MenuType], [FK_App], [Url], [IsEnable], [OpenWay], [Flag], [Tag1], [Tag2], [Tag3], [WebPath], [MyFileName], [MyFilePath], [MyFileExt], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2002', N'驰骋OA', N'2000', N'0103', 1, NULL, 2, N'CCOA', N'', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[GPM_Menu] ([No], [Name], [ParentNo], [TreeNo], [IsDir], [Idx], [MenuType], [FK_App], [Url], [IsEnable], [OpenWay], [Flag], [Tag1], [Tag2], [Tag3], [WebPath], [MyFileName], [MyFilePath], [MyFileExt], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2003', N'Tester', N'2001', N'0104', 1, NULL, 2, N'Tester', N'', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[GPM_Menu] ([No], [Name], [ParentNo], [TreeNo], [IsDir], [Idx], [MenuType], [FK_App], [Url], [IsEnable], [OpenWay], [Flag], [Tag1], [Tag2], [Tag3], [WebPath], [MyFileName], [MyFilePath], [MyFileExt], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2004', N'工作流程', N'2000', N'0105', 1, NULL, 2, N'CCFlow', N'', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[GPM_Menu] ([No], [Name], [ParentNo], [TreeNo], [IsDir], [Idx], [MenuType], [FK_App], [Url], [IsEnable], [OpenWay], [Flag], [Tag1], [Tag2], [Tag3], [WebPath], [MyFileName], [MyFilePath], [MyFileExt], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2005', N'即时通讯', N'2000', N'0106', 1, NULL, 2, N'CCIM', N'', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[GPM_Menu] ([No], [Name], [ParentNo], [TreeNo], [IsDir], [Idx], [MenuType], [FK_App], [Url], [IsEnable], [OpenWay], [Flag], [Tag1], [Tag2], [Tag3], [WebPath], [MyFileName], [MyFilePath], [MyFileExt], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2006', N'公共信息网', N'2000', N'0107', 1, NULL, 2, N'GGXXW', N'', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[GPM_Menu] ([No], [Name], [ParentNo], [TreeNo], [IsDir], [Idx], [MenuType], [FK_App], [Url], [IsEnable], [OpenWay], [Flag], [Tag1], [Tag2], [Tag3], [WebPath], [MyFileName], [MyFilePath], [MyFileExt], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2007', N'权限管理', N'2000', N'0108', 1, NULL, 2, N'GPM', N'', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[GPM_Menu] ([No], [Name], [ParentNo], [TreeNo], [IsDir], [Idx], [MenuType], [FK_App], [Url], [IsEnable], [OpenWay], [Flag], [Tag1], [Tag2], [Tag3], [WebPath], [MyFileName], [MyFilePath], [MyFileExt], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2008', N'单点登陆', N'2000', N'0109', 1, NULL, 2, N'SSO', N'', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[GPM_Menu] ([No], [Name], [ParentNo], [TreeNo], [IsDir], [Idx], [MenuType], [FK_App], [Url], [IsEnable], [OpenWay], [Flag], [Tag1], [Tag2], [Tag3], [WebPath], [MyFileName], [MyFilePath], [MyFileExt], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2010', N'发送短消息', N'1010', N'10103', 0, 3, 3, N'CCOA', N'/Main/Sys/SendShortMsg.aspx', 1, NULL, N'', N'', N'', N'', N'//DataUser/BP.GPM.Menu/2010.gif', N'new1.gif', N'', N'gif', 16, 16, 0)
INSERT [dbo].[GPM_Menu] ([No], [Name], [ParentNo], [TreeNo], [IsDir], [Idx], [MenuType], [FK_App], [Url], [IsEnable], [OpenWay], [Flag], [Tag1], [Tag2], [Tag3], [WebPath], [MyFileName], [MyFilePath], [MyFileExt], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2011', N'公告类型', N'1012', N'10204', 0, 0, 4, N'CCOA', N'/WF/Comm/Search.aspx?EnsName=BP.OA.NoticeCategorys', 1, NULL, N'', N'', N'', N'', N'//DataUser/BP.GPM.Menu/1015.png', N'data3.png', N'', N'png', 16, 16, 0)
INSERT [dbo].[GPM_Menu] ([No], [Name], [ParentNo], [TreeNo], [IsDir], [Idx], [MenuType], [FK_App], [Url], [IsEnable], [OpenWay], [Flag], [Tag1], [Tag2], [Tag3], [WebPath], [MyFileName], [MyFilePath], [MyFileExt], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2101', N'办公用品管理', N'2002', N'0105', 1, 0, 3, N'CCOA', N'', 1, NULL, N'', N'', N'', N'', N'', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_Menu] ([No], [Name], [ParentNo], [TreeNo], [IsDir], [Idx], [MenuType], [FK_App], [Url], [IsEnable], [OpenWay], [Flag], [Tag1], [Tag2], [Tag3], [WebPath], [MyFileName], [MyFilePath], [MyFileExt], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2111', N'类别维护', N'2101', N'10103', 0, 3, 3, N'CCOA', N'/WF/Comm/Ens.aspx?EnsName=BP.DS.DSSorts', 1, NULL, N'', N'', N'', N'', N'//DataUser/BP.GPM.Menu/2010.gif', N'new1.gif', N'', N'gif', 16, 16, 0)
INSERT [dbo].[GPM_Menu] ([No], [Name], [ParentNo], [TreeNo], [IsDir], [Idx], [MenuType], [FK_App], [Url], [IsEnable], [OpenWay], [Flag], [Tag1], [Tag2], [Tag3], [WebPath], [MyFileName], [MyFilePath], [MyFileExt], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2112', N'库存台帐', N'2101', N'10103', 0, 3, 3, N'CCOA', N'/WF/Comm/Search.aspx?EnsName=BP.DS.DSMains', 1, NULL, N'', N'', N'', N'', N'//DataUser/BP.GPM.Menu/2010.gif', N'new1.gif', N'', N'gif', 16, 16, 0)
INSERT [dbo].[GPM_Menu] ([No], [Name], [ParentNo], [TreeNo], [IsDir], [Idx], [MenuType], [FK_App], [Url], [IsEnable], [OpenWay], [Flag], [Tag1], [Tag2], [Tag3], [WebPath], [MyFileName], [MyFilePath], [MyFileExt], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2113', N'采购台帐', N'2101', N'10103', 0, 3, 3, N'CCOA', N'/WF/Comm/Search.aspx?EnsName=BP.DS.DSBuys', 1, NULL, N'', N'', N'', N'', N'//DataUser/BP.GPM.Menu/2010.gif', N'new1.gif', N'', N'gif', 16, 16, 0)
INSERT [dbo].[GPM_Menu] ([No], [Name], [ParentNo], [TreeNo], [IsDir], [Idx], [MenuType], [FK_App], [Url], [IsEnable], [OpenWay], [Flag], [Tag1], [Tag2], [Tag3], [WebPath], [MyFileName], [MyFilePath], [MyFileExt], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2114', N'领用台帐', N'2101', N'10103', 0, 3, 3, N'CCOA', N'/WF/Comm/Search.aspx?EnsName=BP.DS.DSTakes', 1, NULL, N'', N'', N'', N'', N'//DataUser/BP.GPM.Menu/2010.gif', N'new1.gif', N'', N'gif', 16, 16, 0)
/****** Object:  Table [dbo].[GPM_LinkSort]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GPM_LinkSort](
	[No] [nvarchar](2) NOT NULL,
	[Name] [nvarchar](3900) NULL,
	[Url] [nvarchar](3900) NULL,
	[OpenWay] [int] NULL,
	[Idx] [int] NULL,
 CONSTRAINT [GPM_LinkSortpk] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[GPM_LinkSort] ([No], [Name], [Url], [OpenWay], [Idx]) VALUES (N'01', N'驰骋产品', NULL, NULL, NULL)
INSERT [dbo].[GPM_LinkSort] ([No], [Name], [Url], [OpenWay], [Idx]) VALUES (N'02', N'门户网站', NULL, NULL, NULL)
INSERT [dbo].[GPM_LinkSort] ([No], [Name], [Url], [OpenWay], [Idx]) VALUES (N'03', N'相关链接', NULL, NULL, NULL)
/****** Object:  Table [dbo].[GPM_Link]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GPM_Link](
	[No] [nvarchar](2) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[Url] [nvarchar](200) NULL,
	[OpenWay] [int] NULL,
	[FK_Sort] [nvarchar](100) NULL,
	[Idx] [int] NULL,
 CONSTRAINT [GPM_Linkpk] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[GPM_Link] ([No], [Name], [Url], [OpenWay], [FK_Sort], [Idx]) VALUES (N'01', N'驰骋工作流程管理系统', N'http://ccflow.org', 0, N'01', NULL)
INSERT [dbo].[GPM_Link] ([No], [Name], [Url], [OpenWay], [FK_Sort], [Idx]) VALUES (N'02', N'驰骋办公自动化', N'http://www.chichengoa.org/', 0, N'01', NULL)
INSERT [dbo].[GPM_Link] ([No], [Name], [Url], [OpenWay], [FK_Sort], [Idx]) VALUES (N'04', N'驰骋关系型表单设计器', N'http://ccflow.org', 0, N'01', NULL)
INSERT [dbo].[GPM_Link] ([No], [Name], [Url], [OpenWay], [FK_Sort], [Idx]) VALUES (N'10', N'新浪网sina', N'http://sina.com.cn', 0, N'02', NULL)
INSERT [dbo].[GPM_Link] ([No], [Name], [Url], [OpenWay], [FK_Sort], [Idx]) VALUES (N'11', N'百度baidu', N'http://baidu.com', 0, N'02', NULL)
INSERT [dbo].[GPM_Link] ([No], [Name], [Url], [OpenWay], [FK_Sort], [Idx]) VALUES (N'12', N'谷歌google', N'http://google.com.cn', 0, N'02', NULL)
INSERT [dbo].[GPM_Link] ([No], [Name], [Url], [OpenWay], [FK_Sort], [Idx]) VALUES (N'20', N'国家总局', N'http://ccflow.org', 0, N'03', NULL)
INSERT [dbo].[GPM_Link] ([No], [Name], [Url], [OpenWay], [FK_Sort], [Idx]) VALUES (N'21', N'xxxx省局', N'http://ccflow.org', 0, N'03', NULL)
/****** Object:  Table [dbo].[GPM_InfoPush]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GPM_InfoPush](
	[No] [nvarchar](30) NOT NULL,
	[Name] [nvarchar](3900) NULL,
	[Url] [nvarchar](3900) NULL,
	[OpenWay] [int] NULL,
	[Idx] [int] NULL,
	[GetSQL] [nvarchar](4000) NULL,
	[CtrlObjs] [nvarchar](4000) NULL,
	[MyFileName] [nvarchar](100) NULL,
	[MyFilePath] [nvarchar](100) NULL,
	[MyFileExt] [nvarchar](10) NULL,
	[WebPath] [nvarchar](200) NULL,
	[MyFileH] [int] NULL,
	[MyFileW] [int] NULL,
	[MyFileSize] [float] NULL,
 CONSTRAINT [GPM_InfoPushpk] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[GPM_InfoPush] ([No], [Name], [Url], [OpenWay], [Idx], [GetSQL], [CtrlObjs], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'Email', N'新邮件', N'http://mail.google.com', NULL, NULL, N'SELECT COUNT(*) FROM PORT_EMP', NULL, NULL, NULL, NULL, N'Img/email.png', NULL, NULL, NULL)
INSERT [dbo].[GPM_InfoPush] ([No], [Name], [Url], [OpenWay], [Idx], [GetSQL], [CtrlObjs], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'Msg', N'系统消息', N'http://mail.google.com', NULL, NULL, N'SELECT COUNT(*) FROM PORT_EMP', NULL, NULL, NULL, NULL, N'Img/msg.png', NULL, NULL, NULL)
INSERT [dbo].[GPM_InfoPush] ([No], [Name], [Url], [OpenWay], [Idx], [GetSQL], [CtrlObjs], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'MyWork', N'待办工作', N'http://localhost/ccflow/WF/EmpWorks.aspx', NULL, NULL, N'SELECT COUNT(*) FROM PORT_EMP', NULL, NULL, NULL, NULL, N'/DataUser/BP.GPM.InfoPush/MyWork.gif', NULL, NULL, NULL)
INSERT [dbo].[GPM_InfoPush] ([No], [Name], [Url], [OpenWay], [Idx], [GetSQL], [CtrlObjs], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'News', N'未读新闻', N'http://sina.com.cn', NULL, NULL, N'SELECT COUNT(*) FROM PORT_EMP', NULL, NULL, NULL, NULL, N'/DataUser/BP.GPM.InfoPush/News.gif', NULL, NULL, NULL)
INSERT [dbo].[GPM_InfoPush] ([No], [Name], [Url], [OpenWay], [Idx], [GetSQL], [CtrlObjs], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'Notice', N'未读公告', N'http://sina.com.cn', NULL, NULL, N'SELECT COUNT(*) FROM PORT_EMP', NULL, NULL, NULL, NULL, N'/DataUser/BP.GPM.InfoPush/Notice.gif', NULL, NULL, NULL)
INSERT [dbo].[GPM_InfoPush] ([No], [Name], [Url], [OpenWay], [Idx], [GetSQL], [CtrlObjs], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'Ontheway', N'在途工作', N'http://sina.com.cn', NULL, NULL, N'SELECT COUNT(*) FROM PORT_EMP', NULL, NULL, NULL, NULL, N'/DataUser/BP.GPM.InfoPush/Ontheway.gif', NULL, NULL, NULL)
/****** Object:  Table [dbo].[GPM_GroupStation]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GPM_GroupStation](
	[FK_Group] [nvarchar](50) NOT NULL,
	[FK_Station] [nvarchar](100) NOT NULL,
 CONSTRAINT [GPM_GroupStationpk] PRIMARY KEY CLUSTERED 
(
	[FK_Group] ASC,
	[FK_Station] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[GPM_GroupMenu]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GPM_GroupMenu](
	[FK_Group] [nvarchar](50) NOT NULL,
	[FK_Menu] [nvarchar](50) NOT NULL,
	[IsChecked] [int] NULL,
 CONSTRAINT [GPM_GroupMenupk] PRIMARY KEY CLUSTERED 
(
	[FK_Group] ASC,
	[FK_Menu] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[GPM_GroupEmp]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GPM_GroupEmp](
	[FK_Group] [nvarchar](50) NOT NULL,
	[FK_Emp] [nvarchar](100) NOT NULL,
 CONSTRAINT [GPM_GroupEmppk] PRIMARY KEY CLUSTERED 
(
	[FK_Group] ASC,
	[FK_Emp] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[GPM_Group]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GPM_Group](
	[No] [nvarchar](3) NOT NULL,
	[Name] [nvarchar](300) NULL,
	[Idx] [int] NULL,
 CONSTRAINT [GPM_Grouppk] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[GPM_EmpMenu]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GPM_EmpMenu](
	[MyPK] [nvarchar](100) NOT NULL,
	[FK_Emp] [nvarchar](30) NULL,
	[FK_Menu] [nvarchar](30) NULL,
	[Name] [nvarchar](3900) NULL,
	[ParentNo] [nvarchar](30) NULL,
	[TreeNo] [nvarchar](30) NULL,
	[Url] [nvarchar](3900) NULL,
	[MenuType] [int] NULL,
	[FK_App] [nvarchar](30) NULL,
	[MyFileName] [nvarchar](100) NULL,
	[MyFilePath] [nvarchar](100) NULL,
	[MyFileExt] [nvarchar](10) NULL,
	[WebPath] [nvarchar](200) NULL,
	[MyFileH] [int] NULL,
	[MyFileW] [int] NULL,
	[MyFileSize] [float] NULL,
 CONSTRAINT [GPM_EmpMenupk] PRIMARY KEY CLUSTERED 
(
	[MyPK] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1000_admin', N'admin', N'1000', N'济南驰骋信息技术有限公司', N'0', N'01', N'', 0, N'UnitFullName', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1000_fuhui', N'fuhui', N'1000', N'济南驰骋信息技术有限公司', N'0', N'01', N'', 0, N'UnitFullName', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1000_guobaogeng', N'guobaogeng', N'1000', N'济南驰骋信息技术有限公司', N'0', N'01', N'', 0, N'UnitFullName', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1000_guoxiangbin', N'guoxiangbin', N'1000', N'济南驰骋信息技术有限公司', N'0', N'01', N'', 0, N'UnitFullName', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1000_liping', N'liping', N'1000', N'济南驰骋信息技术有限公司', N'0', N'01', N'', 0, N'UnitFullName', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1000_liyan', N'liyan', N'1000', N'济南驰骋信息技术有限公司', N'0', N'01', N'', 0, N'UnitFullName', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1000_qifenglin', N'qifenglin', N'1000', N'济南驰骋信息技术有限公司', N'0', N'01', N'', 0, N'UnitFullName', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1000_yangyilei', N'yangyilei', N'1000', N'济南驰骋信息技术有限公司', N'0', N'01', N'', 0, N'UnitFullName', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1000_zhanghaicheng', N'zhanghaicheng', N'1000', N'济南驰骋信息技术有限公司', N'0', N'01', N'', 0, N'UnitFullName', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1000_zhangyifan', N'zhangyifan', N'1000', N'济南驰骋信息技术有限公司', N'0', N'01', N'', 0, N'UnitFullName', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1000_zhoupeng', N'zhoupeng', N'1000', N'济南驰骋信息技术有限公司', N'0', N'01', N'', 0, N'UnitFullName', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1000_zhoushengyu', N'zhoushengyu', N'1000', N'济南驰骋信息技术有限公司', N'0', N'01', N'', 0, N'UnitFullName', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1000_zhoutianjiao', N'zhoutianjiao', N'1000', N'济南驰骋信息技术有限公司', N'0', N'01', N'', 0, N'UnitFullName', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1010_admin', N'admin', N'1010', N'我的工作', N'2002', N'0101', N'', 3, N'CCOA', N'', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1010_fuhui', N'fuhui', N'1010', N'我的工作', N'2002', N'0101', N'', 3, N'CCOA', N'', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1010_guobaogeng', N'guobaogeng', N'1010', N'我的工作', N'2002', N'0101', N'', 3, N'CCOA', N'', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1010_guoxiangbin', N'guoxiangbin', N'1010', N'我的工作', N'2002', N'0101', N'', 3, N'CCOA', N'', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1010_liping', N'liping', N'1010', N'我的工作', N'2002', N'0101', N'', 3, N'CCOA', N'', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1010_liyan', N'liyan', N'1010', N'我的工作', N'2002', N'0101', N'', 3, N'CCOA', N'', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1010_qifenglin', N'qifenglin', N'1010', N'我的工作', N'2002', N'0101', N'', 3, N'CCOA', N'', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1010_yangyilei', N'yangyilei', N'1010', N'我的工作', N'2002', N'0101', N'', 3, N'CCOA', N'', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1010_zhanghaicheng', N'zhanghaicheng', N'1010', N'我的工作', N'2002', N'0101', N'', 3, N'CCOA', N'', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1010_zhangyifan', N'zhangyifan', N'1010', N'我的工作', N'2002', N'0101', N'', 3, N'CCOA', N'', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1010_zhoupeng', N'zhoupeng', N'1010', N'我的工作', N'2002', N'0101', N'', 3, N'CCOA', N'', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1010_zhoushengyu', N'zhoushengyu', N'1010', N'我的工作', N'2002', N'0101', N'', 3, N'CCOA', N'', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1010_zhoutianjiao', N'zhoutianjiao', N'1010', N'我的工作', N'2002', N'0101', N'', 3, N'CCOA', N'', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1011_admin', N'admin', N'1011', N'我的计划', N'1010', N'010101', N'/App/PrivPlan/MyPlan.aspx', 4, N'CCOA', N'files.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1011.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1011_fuhui', N'fuhui', N'1011', N'我的计划', N'1010', N'010101', N'/App/PrivPlan/MyPlan.aspx', 4, N'CCOA', N'files.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1011.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1011_guobaogeng', N'guobaogeng', N'1011', N'我的计划', N'1010', N'010101', N'/App/PrivPlan/MyPlan.aspx', 4, N'CCOA', N'files.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1011.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1011_guoxiangbin', N'guoxiangbin', N'1011', N'我的计划', N'1010', N'010101', N'/App/PrivPlan/MyPlan.aspx', 4, N'CCOA', N'files.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1011.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1011_liping', N'liping', N'1011', N'我的计划', N'1010', N'010101', N'/App/PrivPlan/MyPlan.aspx', 4, N'CCOA', N'files.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1011.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1011_liyan', N'liyan', N'1011', N'我的计划', N'1010', N'010101', N'/App/PrivPlan/MyPlan.aspx', 4, N'CCOA', N'files.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1011.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1011_qifenglin', N'qifenglin', N'1011', N'我的计划', N'1010', N'010101', N'/App/PrivPlan/MyPlan.aspx', 4, N'CCOA', N'files.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1011.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1011_yangyilei', N'yangyilei', N'1011', N'我的计划', N'1010', N'010101', N'/App/PrivPlan/MyPlan.aspx', 4, N'CCOA', N'files.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1011.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1011_zhanghaicheng', N'zhanghaicheng', N'1011', N'我的计划', N'1010', N'010101', N'/App/PrivPlan/MyPlan.aspx', 4, N'CCOA', N'files.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1011.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1011_zhangyifan', N'zhangyifan', N'1011', N'我的计划', N'1010', N'010101', N'/App/PrivPlan/MyPlan.aspx', 4, N'CCOA', N'files.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1011.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1011_zhoupeng', N'zhoupeng', N'1011', N'我的计划', N'1010', N'010101', N'/App/PrivPlan/MyPlan.aspx', 4, N'CCOA', N'files.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1011.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1011_zhoushengyu', N'zhoushengyu', N'1011', N'我的计划', N'1010', N'010101', N'/App/PrivPlan/MyPlan.aspx', 4, N'CCOA', N'files.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1011.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1011_zhoutianjiao', N'zhoutianjiao', N'1011', N'我的计划', N'1010', N'010101', N'/App/PrivPlan/MyPlan.aspx', 4, N'CCOA', N'files.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1011.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1012_admin', N'admin', N'1012', N'公告通知', N'2002', N'0102', N'', 3, N'CCOA', N'', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1012_fuhui', N'fuhui', N'1012', N'公告通知', N'2002', N'0102', N'', 3, N'CCOA', N'', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1012_guobaogeng', N'guobaogeng', N'1012', N'公告通知', N'2002', N'0102', N'', 3, N'CCOA', N'', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1012_guoxiangbin', N'guoxiangbin', N'1012', N'公告通知', N'2002', N'0102', N'', 3, N'CCOA', N'', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1012_liping', N'liping', N'1012', N'公告通知', N'2002', N'0102', N'', 3, N'CCOA', N'', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1012_liyan', N'liyan', N'1012', N'公告通知', N'2002', N'0102', N'', 3, N'CCOA', N'', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1012_qifenglin', N'qifenglin', N'1012', N'公告通知', N'2002', N'0102', N'', 3, N'CCOA', N'', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1012_yangyilei', N'yangyilei', N'1012', N'公告通知', N'2002', N'0102', N'', 3, N'CCOA', N'', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1012_zhanghaicheng', N'zhanghaicheng', N'1012', N'公告通知', N'2002', N'0102', N'', 3, N'CCOA', N'', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1012_zhangyifan', N'zhangyifan', N'1012', N'公告通知', N'2002', N'0102', N'', 3, N'CCOA', N'', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1012_zhoupeng', N'zhoupeng', N'1012', N'公告通知', N'2002', N'0102', N'', 3, N'CCOA', N'', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1012_zhoushengyu', N'zhoushengyu', N'1012', N'公告通知', N'2002', N'0102', N'', 3, N'CCOA', N'', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1012_zhoutianjiao', N'zhoutianjiao', N'1012', N'公告通知', N'2002', N'0102', N'', 3, N'CCOA', N'', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1013_admin', N'admin', N'1013', N'公告管理', N'1012', N'010201', N'/App/Notice/NoticeList.aspx', 4, N'CCOA', N'framework.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1013.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1013_fuhui', N'fuhui', N'1013', N'公告管理', N'1012', N'010201', N'/App/Notice/NoticeList.aspx', 4, N'CCOA', N'framework.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1013.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1013_guobaogeng', N'guobaogeng', N'1013', N'公告管理', N'1012', N'010201', N'/App/Notice/NoticeList.aspx', 4, N'CCOA', N'framework.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1013.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1013_guoxiangbin', N'guoxiangbin', N'1013', N'公告管理', N'1012', N'010201', N'/App/Notice/NoticeList.aspx', 4, N'CCOA', N'framework.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1013.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1013_liping', N'liping', N'1013', N'公告管理', N'1012', N'010201', N'/App/Notice/NoticeList.aspx', 4, N'CCOA', N'framework.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1013.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1013_liyan', N'liyan', N'1013', N'公告管理', N'1012', N'010201', N'/App/Notice/NoticeList.aspx', 4, N'CCOA', N'framework.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1013.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1013_qifenglin', N'qifenglin', N'1013', N'公告管理', N'1012', N'010201', N'/App/Notice/NoticeList.aspx', 4, N'CCOA', N'framework.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1013.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1013_yangyilei', N'yangyilei', N'1013', N'公告管理', N'1012', N'010201', N'/App/Notice/NoticeList.aspx', 4, N'CCOA', N'framework.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1013.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1013_zhanghaicheng', N'zhanghaicheng', N'1013', N'公告管理', N'1012', N'010201', N'/App/Notice/NoticeList.aspx', 4, N'CCOA', N'framework.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1013.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1013_zhangyifan', N'zhangyifan', N'1013', N'公告管理', N'1012', N'010201', N'/App/Notice/NoticeList.aspx', 4, N'CCOA', N'framework.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1013.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1013_zhoupeng', N'zhoupeng', N'1013', N'公告管理', N'1012', N'010201', N'/App/Notice/NoticeList.aspx', 4, N'CCOA', N'framework.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1013.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1013_zhoushengyu', N'zhoushengyu', N'1013', N'公告管理', N'1012', N'010201', N'/App/Notice/NoticeList.aspx', 4, N'CCOA', N'framework.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1013.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1013_zhoutianjiao', N'zhoutianjiao', N'1013', N'公告管理', N'1012', N'010201', N'/App/Notice/NoticeList.aspx', 4, N'CCOA', N'framework.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1013.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1014_admin', N'admin', N'1014', N'添加公告', N'1012', N'10202', N'/App/Notice/NewNotice.aspx', 4, N'CCOA', N'new3.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1014.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1014_fuhui', N'fuhui', N'1014', N'添加公告', N'1012', N'10202', N'/App/Notice/NewNotice.aspx', 4, N'CCOA', N'new3.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1014.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1014_guobaogeng', N'guobaogeng', N'1014', N'添加公告', N'1012', N'10202', N'/App/Notice/NewNotice.aspx', 4, N'CCOA', N'new3.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1014.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1014_guoxiangbin', N'guoxiangbin', N'1014', N'添加公告', N'1012', N'10202', N'/App/Notice/NewNotice.aspx', 4, N'CCOA', N'new3.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1014.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1014_liping', N'liping', N'1014', N'添加公告', N'1012', N'10202', N'/App/Notice/NewNotice.aspx', 4, N'CCOA', N'new3.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1014.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1014_liyan', N'liyan', N'1014', N'添加公告', N'1012', N'10202', N'/App/Notice/NewNotice.aspx', 4, N'CCOA', N'new3.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1014.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1014_qifenglin', N'qifenglin', N'1014', N'添加公告', N'1012', N'10202', N'/App/Notice/NewNotice.aspx', 4, N'CCOA', N'new3.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1014.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1014_yangyilei', N'yangyilei', N'1014', N'添加公告', N'1012', N'10202', N'/App/Notice/NewNotice.aspx', 4, N'CCOA', N'new3.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1014.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1014_zhanghaicheng', N'zhanghaicheng', N'1014', N'添加公告', N'1012', N'10202', N'/App/Notice/NewNotice.aspx', 4, N'CCOA', N'new3.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1014.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1014_zhangyifan', N'zhangyifan', N'1014', N'添加公告', N'1012', N'10202', N'/App/Notice/NewNotice.aspx', 4, N'CCOA', N'new3.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1014.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1014_zhoupeng', N'zhoupeng', N'1014', N'添加公告', N'1012', N'10202', N'/App/Notice/NewNotice.aspx', 4, N'CCOA', N'new3.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1014.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1014_zhoushengyu', N'zhoushengyu', N'1014', N'添加公告', N'1012', N'10202', N'/App/Notice/NewNotice.aspx', 4, N'CCOA', N'new3.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1014.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1014_zhoutianjiao', N'zhoutianjiao', N'1014', N'添加公告', N'1012', N'10202', N'/App/Notice/NewNotice.aspx', 4, N'CCOA', N'new3.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1014.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1015_admin', N'admin', N'1015', N'我的公告', N'1012', N'10203', N'/App/Notice/MyNoticeList.aspx', 4, N'CCOA', N'data3.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1015.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1015_fuhui', N'fuhui', N'1015', N'我的公告', N'1012', N'10203', N'/App/Notice/MyNoticeList.aspx', 4, N'CCOA', N'data3.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1015.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1015_guobaogeng', N'guobaogeng', N'1015', N'我的公告', N'1012', N'10203', N'/App/Notice/MyNoticeList.aspx', 4, N'CCOA', N'data3.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1015.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1015_guoxiangbin', N'guoxiangbin', N'1015', N'我的公告', N'1012', N'10203', N'/App/Notice/MyNoticeList.aspx', 4, N'CCOA', N'data3.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1015.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1015_liping', N'liping', N'1015', N'我的公告', N'1012', N'10203', N'/App/Notice/MyNoticeList.aspx', 4, N'CCOA', N'data3.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1015.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1015_liyan', N'liyan', N'1015', N'我的公告', N'1012', N'10203', N'/App/Notice/MyNoticeList.aspx', 4, N'CCOA', N'data3.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1015.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1015_qifenglin', N'qifenglin', N'1015', N'我的公告', N'1012', N'10203', N'/App/Notice/MyNoticeList.aspx', 4, N'CCOA', N'data3.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1015.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1015_yangyilei', N'yangyilei', N'1015', N'我的公告', N'1012', N'10203', N'/App/Notice/MyNoticeList.aspx', 4, N'CCOA', N'data3.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1015.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1015_zhanghaicheng', N'zhanghaicheng', N'1015', N'我的公告', N'1012', N'10203', N'/App/Notice/MyNoticeList.aspx', 4, N'CCOA', N'data3.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1015.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1015_zhangyifan', N'zhangyifan', N'1015', N'我的公告', N'1012', N'10203', N'/App/Notice/MyNoticeList.aspx', 4, N'CCOA', N'data3.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1015.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1015_zhoupeng', N'zhoupeng', N'1015', N'我的公告', N'1012', N'10203', N'/App/Notice/MyNoticeList.aspx', 4, N'CCOA', N'data3.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1015.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1015_zhoushengyu', N'zhoushengyu', N'1015', N'我的公告', N'1012', N'10203', N'/App/Notice/MyNoticeList.aspx', 4, N'CCOA', N'data3.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1015.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1015_zhoutianjiao', N'zhoutianjiao', N'1015', N'我的公告', N'1012', N'10203', N'/App/Notice/MyNoticeList.aspx', 4, N'CCOA', N'data3.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1015.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1016_admin', N'admin', N'1016', N'邮件管理', N'2002', N'0103', N'', 3, N'CCOA', N'', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1016_fuhui', N'fuhui', N'1016', N'邮件管理', N'2002', N'0103', N'', 3, N'CCOA', N'', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1016_guobaogeng', N'guobaogeng', N'1016', N'邮件管理', N'2002', N'0103', N'', 3, N'CCOA', N'', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1016_guoxiangbin', N'guoxiangbin', N'1016', N'邮件管理', N'2002', N'0103', N'', 3, N'CCOA', N'', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1016_liping', N'liping', N'1016', N'邮件管理', N'2002', N'0103', N'', 3, N'CCOA', N'', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1016_liyan', N'liyan', N'1016', N'邮件管理', N'2002', N'0103', N'', 3, N'CCOA', N'', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1016_qifenglin', N'qifenglin', N'1016', N'邮件管理', N'2002', N'0103', N'', 3, N'CCOA', N'', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1016_yangyilei', N'yangyilei', N'1016', N'邮件管理', N'2002', N'0103', N'', 3, N'CCOA', N'', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1016_zhanghaicheng', N'zhanghaicheng', N'1016', N'邮件管理', N'2002', N'0103', N'', 3, N'CCOA', N'', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1016_zhangyifan', N'zhangyifan', N'1016', N'邮件管理', N'2002', N'0103', N'', 3, N'CCOA', N'', N'', N'', N'', 0, 0, 0)
GO
print 'Processed 100 total records'
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1016_zhoupeng', N'zhoupeng', N'1016', N'邮件管理', N'2002', N'0103', N'', 3, N'CCOA', N'', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1016_zhoushengyu', N'zhoushengyu', N'1016', N'邮件管理', N'2002', N'0103', N'', 3, N'CCOA', N'', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1016_zhoutianjiao', N'zhoutianjiao', N'1016', N'邮件管理', N'2002', N'0103', N'', 3, N'CCOA', N'', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1017_admin', N'admin', N'1017', N'写邮件', N'1016', N'010301', N'/App/Message/SendMsg.aspx', 4, N'CCOA', N'add.png', N'', N'png', N'//DataUser/BP.GPM.Menu/1017.png', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1017_fuhui', N'fuhui', N'1017', N'写邮件', N'1016', N'010301', N'/App/Message/SendMsg.aspx', 4, N'CCOA', N'add.png', N'', N'png', N'//DataUser/BP.GPM.Menu/1017.png', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1017_guobaogeng', N'guobaogeng', N'1017', N'写邮件', N'1016', N'010301', N'/App/Message/SendMsg.aspx', 4, N'CCOA', N'add.png', N'', N'png', N'//DataUser/BP.GPM.Menu/1017.png', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1017_guoxiangbin', N'guoxiangbin', N'1017', N'写邮件', N'1016', N'010301', N'/App/Message/SendMsg.aspx', 4, N'CCOA', N'add.png', N'', N'png', N'//DataUser/BP.GPM.Menu/1017.png', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1017_liping', N'liping', N'1017', N'写邮件', N'1016', N'010301', N'/App/Message/SendMsg.aspx', 4, N'CCOA', N'add.png', N'', N'png', N'//DataUser/BP.GPM.Menu/1017.png', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1017_liyan', N'liyan', N'1017', N'写邮件', N'1016', N'010301', N'/App/Message/SendMsg.aspx', 4, N'CCOA', N'add.png', N'', N'png', N'//DataUser/BP.GPM.Menu/1017.png', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1017_qifenglin', N'qifenglin', N'1017', N'写邮件', N'1016', N'010301', N'/App/Message/SendMsg.aspx', 4, N'CCOA', N'add.png', N'', N'png', N'//DataUser/BP.GPM.Menu/1017.png', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1017_yangyilei', N'yangyilei', N'1017', N'写邮件', N'1016', N'010301', N'/App/Message/SendMsg.aspx', 4, N'CCOA', N'add.png', N'', N'png', N'//DataUser/BP.GPM.Menu/1017.png', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1017_zhanghaicheng', N'zhanghaicheng', N'1017', N'写邮件', N'1016', N'010301', N'/App/Message/SendMsg.aspx', 4, N'CCOA', N'add.png', N'', N'png', N'//DataUser/BP.GPM.Menu/1017.png', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1017_zhangyifan', N'zhangyifan', N'1017', N'写邮件', N'1016', N'010301', N'/App/Message/SendMsg.aspx', 4, N'CCOA', N'add.png', N'', N'png', N'//DataUser/BP.GPM.Menu/1017.png', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1017_zhoupeng', N'zhoupeng', N'1017', N'写邮件', N'1016', N'010301', N'/App/Message/SendMsg.aspx', 4, N'CCOA', N'add.png', N'', N'png', N'//DataUser/BP.GPM.Menu/1017.png', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1017_zhoushengyu', N'zhoushengyu', N'1017', N'写邮件', N'1016', N'010301', N'/App/Message/SendMsg.aspx', 4, N'CCOA', N'add.png', N'', N'png', N'//DataUser/BP.GPM.Menu/1017.png', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1017_zhoutianjiao', N'zhoutianjiao', N'1017', N'写邮件', N'1016', N'010301', N'/App/Message/SendMsg.aspx', 4, N'CCOA', N'add.png', N'', N'png', N'//DataUser/BP.GPM.Menu/1017.png', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1018_admin', N'admin', N'1018', N'收件箱', N'1016', N'10302', N'/app/message/InBox.aspx', 4, N'CCOA', N'calendar1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1018.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1018_fuhui', N'fuhui', N'1018', N'收件箱', N'1016', N'10302', N'/app/message/InBox.aspx', 4, N'CCOA', N'calendar1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1018.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1018_guobaogeng', N'guobaogeng', N'1018', N'收件箱', N'1016', N'10302', N'/app/message/InBox.aspx', 4, N'CCOA', N'calendar1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1018.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1018_guoxiangbin', N'guoxiangbin', N'1018', N'收件箱', N'1016', N'10302', N'/app/message/InBox.aspx', 4, N'CCOA', N'calendar1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1018.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1018_liping', N'liping', N'1018', N'收件箱', N'1016', N'10302', N'/app/message/InBox.aspx', 4, N'CCOA', N'calendar1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1018.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1018_liyan', N'liyan', N'1018', N'收件箱', N'1016', N'10302', N'/app/message/InBox.aspx', 4, N'CCOA', N'calendar1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1018.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1018_qifenglin', N'qifenglin', N'1018', N'收件箱', N'1016', N'10302', N'/app/message/InBox.aspx', 4, N'CCOA', N'calendar1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1018.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1018_yangyilei', N'yangyilei', N'1018', N'收件箱', N'1016', N'10302', N'/app/message/InBox.aspx', 4, N'CCOA', N'calendar1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1018.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1018_zhanghaicheng', N'zhanghaicheng', N'1018', N'收件箱', N'1016', N'10302', N'/app/message/InBox.aspx', 4, N'CCOA', N'calendar1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1018.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1018_zhangyifan', N'zhangyifan', N'1018', N'收件箱', N'1016', N'10302', N'/app/message/InBox.aspx', 4, N'CCOA', N'calendar1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1018.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1018_zhoupeng', N'zhoupeng', N'1018', N'收件箱', N'1016', N'10302', N'/app/message/InBox.aspx', 4, N'CCOA', N'calendar1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1018.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1018_zhoushengyu', N'zhoushengyu', N'1018', N'收件箱', N'1016', N'10302', N'/app/message/InBox.aspx', 4, N'CCOA', N'calendar1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1018.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1018_zhoutianjiao', N'zhoutianjiao', N'1018', N'收件箱', N'1016', N'10302', N'/app/message/InBox.aspx', 4, N'CCOA', N'calendar1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1018.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1019_admin', N'admin', N'1019', N'发件箱', N'1016', N'10303', N'/app/Message/SendBox.aspx', 4, N'CCOA', N'aremove.png', N'', N'png', N'//DataUser/BP.GPM.Menu/1019.png', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1019_fuhui', N'fuhui', N'1019', N'发件箱', N'1016', N'10303', N'/app/Message/SendBox.aspx', 4, N'CCOA', N'aremove.png', N'', N'png', N'//DataUser/BP.GPM.Menu/1019.png', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1019_guobaogeng', N'guobaogeng', N'1019', N'发件箱', N'1016', N'10303', N'/app/Message/SendBox.aspx', 4, N'CCOA', N'aremove.png', N'', N'png', N'//DataUser/BP.GPM.Menu/1019.png', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1019_guoxiangbin', N'guoxiangbin', N'1019', N'发件箱', N'1016', N'10303', N'/app/Message/SendBox.aspx', 4, N'CCOA', N'aremove.png', N'', N'png', N'//DataUser/BP.GPM.Menu/1019.png', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1019_liping', N'liping', N'1019', N'发件箱', N'1016', N'10303', N'/app/Message/SendBox.aspx', 4, N'CCOA', N'aremove.png', N'', N'png', N'//DataUser/BP.GPM.Menu/1019.png', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1019_liyan', N'liyan', N'1019', N'发件箱', N'1016', N'10303', N'/app/Message/SendBox.aspx', 4, N'CCOA', N'aremove.png', N'', N'png', N'//DataUser/BP.GPM.Menu/1019.png', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1019_qifenglin', N'qifenglin', N'1019', N'发件箱', N'1016', N'10303', N'/app/Message/SendBox.aspx', 4, N'CCOA', N'aremove.png', N'', N'png', N'//DataUser/BP.GPM.Menu/1019.png', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1019_yangyilei', N'yangyilei', N'1019', N'发件箱', N'1016', N'10303', N'/app/Message/SendBox.aspx', 4, N'CCOA', N'aremove.png', N'', N'png', N'//DataUser/BP.GPM.Menu/1019.png', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1019_zhanghaicheng', N'zhanghaicheng', N'1019', N'发件箱', N'1016', N'10303', N'/app/Message/SendBox.aspx', 4, N'CCOA', N'aremove.png', N'', N'png', N'//DataUser/BP.GPM.Menu/1019.png', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1019_zhangyifan', N'zhangyifan', N'1019', N'发件箱', N'1016', N'10303', N'/app/Message/SendBox.aspx', 4, N'CCOA', N'aremove.png', N'', N'png', N'//DataUser/BP.GPM.Menu/1019.png', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1019_zhoupeng', N'zhoupeng', N'1019', N'发件箱', N'1016', N'10303', N'/app/Message/SendBox.aspx', 4, N'CCOA', N'aremove.png', N'', N'png', N'//DataUser/BP.GPM.Menu/1019.png', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1019_zhoushengyu', N'zhoushengyu', N'1019', N'发件箱', N'1016', N'10303', N'/app/Message/SendBox.aspx', 4, N'CCOA', N'aremove.png', N'', N'png', N'//DataUser/BP.GPM.Menu/1019.png', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1019_zhoutianjiao', N'zhoutianjiao', N'1019', N'发件箱', N'1016', N'10303', N'/app/Message/SendBox.aspx', 4, N'CCOA', N'aremove.png', N'', N'png', N'//DataUser/BP.GPM.Menu/1019.png', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1020_admin', N'admin', N'1020', N'新闻管理', N'2002', N'0104', N'', 3, N'CCOA', N'', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1020_fuhui', N'fuhui', N'1020', N'新闻管理', N'2002', N'0104', N'', 3, N'CCOA', N'', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1020_guobaogeng', N'guobaogeng', N'1020', N'新闻管理', N'2002', N'0104', N'', 3, N'CCOA', N'', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1020_guoxiangbin', N'guoxiangbin', N'1020', N'新闻管理', N'2002', N'0104', N'', 3, N'CCOA', N'', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1020_liping', N'liping', N'1020', N'新闻管理', N'2002', N'0104', N'', 3, N'CCOA', N'', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1020_liyan', N'liyan', N'1020', N'新闻管理', N'2002', N'0104', N'', 3, N'CCOA', N'', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1020_qifenglin', N'qifenglin', N'1020', N'新闻管理', N'2002', N'0104', N'', 3, N'CCOA', N'', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1020_yangyilei', N'yangyilei', N'1020', N'新闻管理', N'2002', N'0104', N'', 3, N'CCOA', N'', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1020_zhanghaicheng', N'zhanghaicheng', N'1020', N'新闻管理', N'2002', N'0104', N'', 3, N'CCOA', N'', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1020_zhangyifan', N'zhangyifan', N'1020', N'新闻管理', N'2002', N'0104', N'', 3, N'CCOA', N'', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1020_zhoupeng', N'zhoupeng', N'1020', N'新闻管理', N'2002', N'0104', N'', 3, N'CCOA', N'', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1020_zhoushengyu', N'zhoushengyu', N'1020', N'新闻管理', N'2002', N'0104', N'', 3, N'CCOA', N'', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1020_zhoutianjiao', N'zhoutianjiao', N'1020', N'新闻管理', N'2002', N'0104', N'', 3, N'CCOA', N'', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1021_admin', N'admin', N'1021', N'添加新闻', N'1020', N'010401', N'/app/News/NewsAdd.aspx', 4, N'CCOA', N'pen.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1021.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1021_fuhui', N'fuhui', N'1021', N'添加新闻', N'1020', N'010401', N'/app/News/NewsAdd.aspx', 4, N'CCOA', N'pen.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1021.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1021_guobaogeng', N'guobaogeng', N'1021', N'添加新闻', N'1020', N'010401', N'/app/News/NewsAdd.aspx', 4, N'CCOA', N'pen.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1021.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1021_guoxiangbin', N'guoxiangbin', N'1021', N'添加新闻', N'1020', N'010401', N'/app/News/NewsAdd.aspx', 4, N'CCOA', N'pen.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1021.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1021_liping', N'liping', N'1021', N'添加新闻', N'1020', N'010401', N'/app/News/NewsAdd.aspx', 4, N'CCOA', N'pen.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1021.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1021_liyan', N'liyan', N'1021', N'添加新闻', N'1020', N'010401', N'/app/News/NewsAdd.aspx', 4, N'CCOA', N'pen.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1021.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1021_qifenglin', N'qifenglin', N'1021', N'添加新闻', N'1020', N'010401', N'/app/News/NewsAdd.aspx', 4, N'CCOA', N'pen.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1021.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1021_yangyilei', N'yangyilei', N'1021', N'添加新闻', N'1020', N'010401', N'/app/News/NewsAdd.aspx', 4, N'CCOA', N'pen.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1021.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1021_zhanghaicheng', N'zhanghaicheng', N'1021', N'添加新闻', N'1020', N'010401', N'/app/News/NewsAdd.aspx', 4, N'CCOA', N'pen.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1021.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1021_zhangyifan', N'zhangyifan', N'1021', N'添加新闻', N'1020', N'010401', N'/app/News/NewsAdd.aspx', 4, N'CCOA', N'pen.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1021.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1021_zhoupeng', N'zhoupeng', N'1021', N'添加新闻', N'1020', N'010401', N'/app/News/NewsAdd.aspx', 4, N'CCOA', N'pen.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1021.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1021_zhoushengyu', N'zhoushengyu', N'1021', N'添加新闻', N'1020', N'010401', N'/app/News/NewsAdd.aspx', 4, N'CCOA', N'pen.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1021.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1021_zhoutianjiao', N'zhoutianjiao', N'1021', N'添加新闻', N'1020', N'010401', N'/app/News/NewsAdd.aspx', 4, N'CCOA', N'pen.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1021.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1022_admin', N'admin', N'1022', N'新闻管理', N'1020', N'10402', N'/app/News/NewsList.aspx', 4, N'CCOA', N'ico_news.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1022.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1022_fuhui', N'fuhui', N'1022', N'新闻管理', N'1020', N'10402', N'/app/News/NewsList.aspx', 4, N'CCOA', N'ico_news.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1022.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1022_guobaogeng', N'guobaogeng', N'1022', N'新闻管理', N'1020', N'10402', N'/app/News/NewsList.aspx', 4, N'CCOA', N'ico_news.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1022.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1022_guoxiangbin', N'guoxiangbin', N'1022', N'新闻管理', N'1020', N'10402', N'/app/News/NewsList.aspx', 4, N'CCOA', N'ico_news.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1022.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1022_liping', N'liping', N'1022', N'新闻管理', N'1020', N'10402', N'/app/News/NewsList.aspx', 4, N'CCOA', N'ico_news.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1022.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1022_liyan', N'liyan', N'1022', N'新闻管理', N'1020', N'10402', N'/app/News/NewsList.aspx', 4, N'CCOA', N'ico_news.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1022.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1022_qifenglin', N'qifenglin', N'1022', N'新闻管理', N'1020', N'10402', N'/app/News/NewsList.aspx', 4, N'CCOA', N'ico_news.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1022.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1022_yangyilei', N'yangyilei', N'1022', N'新闻管理', N'1020', N'10402', N'/app/News/NewsList.aspx', 4, N'CCOA', N'ico_news.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1022.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1022_zhanghaicheng', N'zhanghaicheng', N'1022', N'新闻管理', N'1020', N'10402', N'/app/News/NewsList.aspx', 4, N'CCOA', N'ico_news.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1022.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1022_zhangyifan', N'zhangyifan', N'1022', N'新闻管理', N'1020', N'10402', N'/app/News/NewsList.aspx', 4, N'CCOA', N'ico_news.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1022.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1022_zhoupeng', N'zhoupeng', N'1022', N'新闻管理', N'1020', N'10402', N'/app/News/NewsList.aspx', 4, N'CCOA', N'ico_news.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1022.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1022_zhoushengyu', N'zhoushengyu', N'1022', N'新闻管理', N'1020', N'10402', N'/app/News/NewsList.aspx', 4, N'CCOA', N'ico_news.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1022.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1022_zhoutianjiao', N'zhoutianjiao', N'1022', N'新闻管理', N'1020', N'10402', N'/app/News/NewsList.aspx', 4, N'CCOA', N'ico_news.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1022.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1024_admin', N'admin', N'1024', N'新闻栏目', N'1020', N'10404', N'/Comm/Search.aspx?EnsName=BP.OA.Article.ArticleCatagorys', 4, N'CCOA', N'sharedir.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1024.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1024_fuhui', N'fuhui', N'1024', N'新闻栏目', N'1020', N'10404', N'/Comm/Search.aspx?EnsName=BP.OA.Article.ArticleCatagorys', 4, N'CCOA', N'sharedir.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1024.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1024_guobaogeng', N'guobaogeng', N'1024', N'新闻栏目', N'1020', N'10404', N'/Comm/Search.aspx?EnsName=BP.OA.Article.ArticleCatagorys', 4, N'CCOA', N'sharedir.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1024.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1024_guoxiangbin', N'guoxiangbin', N'1024', N'新闻栏目', N'1020', N'10404', N'/Comm/Search.aspx?EnsName=BP.OA.Article.ArticleCatagorys', 4, N'CCOA', N'sharedir.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1024.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1024_liping', N'liping', N'1024', N'新闻栏目', N'1020', N'10404', N'/Comm/Search.aspx?EnsName=BP.OA.Article.ArticleCatagorys', 4, N'CCOA', N'sharedir.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1024.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1024_liyan', N'liyan', N'1024', N'新闻栏目', N'1020', N'10404', N'/Comm/Search.aspx?EnsName=BP.OA.Article.ArticleCatagorys', 4, N'CCOA', N'sharedir.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1024.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1024_qifenglin', N'qifenglin', N'1024', N'新闻栏目', N'1020', N'10404', N'/Comm/Search.aspx?EnsName=BP.OA.Article.ArticleCatagorys', 4, N'CCOA', N'sharedir.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1024.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1024_yangyilei', N'yangyilei', N'1024', N'新闻栏目', N'1020', N'10404', N'/Comm/Search.aspx?EnsName=BP.OA.Article.ArticleCatagorys', 4, N'CCOA', N'sharedir.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1024.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1024_zhanghaicheng', N'zhanghaicheng', N'1024', N'新闻栏目', N'1020', N'10404', N'/Comm/Search.aspx?EnsName=BP.OA.Article.ArticleCatagorys', 4, N'CCOA', N'sharedir.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1024.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1024_zhangyifan', N'zhangyifan', N'1024', N'新闻栏目', N'1020', N'10404', N'/Comm/Search.aspx?EnsName=BP.OA.Article.ArticleCatagorys', 4, N'CCOA', N'sharedir.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1024.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1024_zhoupeng', N'zhoupeng', N'1024', N'新闻栏目', N'1020', N'10404', N'/Comm/Search.aspx?EnsName=BP.OA.Article.ArticleCatagorys', 4, N'CCOA', N'sharedir.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1024.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1024_zhoushengyu', N'zhoushengyu', N'1024', N'新闻栏目', N'1020', N'10404', N'/Comm/Search.aspx?EnsName=BP.OA.Article.ArticleCatagorys', 4, N'CCOA', N'sharedir.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1024.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1024_zhoutianjiao', N'zhoutianjiao', N'1024', N'新闻栏目', N'1020', N'10404', N'/Comm/Search.aspx?EnsName=BP.OA.Article.ArticleCatagorys', 4, N'CCOA', N'sharedir.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1024.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1025_admin', N'admin', N'1025', N'流程管理', N'2002', N'0105', N'', 3, N'CCOA', N'', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1025_fuhui', N'fuhui', N'1025', N'流程管理', N'2002', N'0105', N'', 3, N'CCOA', N'', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1025_guobaogeng', N'guobaogeng', N'1025', N'流程管理', N'2002', N'0105', N'', 3, N'CCOA', N'', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1025_guoxiangbin', N'guoxiangbin', N'1025', N'流程管理', N'2002', N'0105', N'', 3, N'CCOA', N'', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1025_liping', N'liping', N'1025', N'流程管理', N'2002', N'0105', N'', 3, N'CCOA', N'', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1025_liyan', N'liyan', N'1025', N'流程管理', N'2002', N'0105', N'', 3, N'CCOA', N'', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1025_qifenglin', N'qifenglin', N'1025', N'流程管理', N'2002', N'0105', N'', 3, N'CCOA', N'', N'', N'', N'', 0, 0, 0)
GO
print 'Processed 200 total records'
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1025_yangyilei', N'yangyilei', N'1025', N'流程管理', N'2002', N'0105', N'', 3, N'CCOA', N'', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1025_zhanghaicheng', N'zhanghaicheng', N'1025', N'流程管理', N'2002', N'0105', N'', 3, N'CCOA', N'', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1025_zhangyifan', N'zhangyifan', N'1025', N'流程管理', N'2002', N'0105', N'', 3, N'CCOA', N'', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1025_zhoupeng', N'zhoupeng', N'1025', N'流程管理', N'2002', N'0105', N'', 3, N'CCOA', N'', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1025_zhoushengyu', N'zhoushengyu', N'1025', N'流程管理', N'2002', N'0105', N'', 3, N'CCOA', N'', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1025_zhoutianjiao', N'zhoutianjiao', N'1025', N'流程管理', N'2002', N'0105', N'', 3, N'CCOA', N'', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1026_admin', N'admin', N'1026', N'发起', N'1025', N'010501', N'/WF/Start.aspx', 3, N'CCOA', N'data1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1026.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1026_fuhui', N'fuhui', N'1026', N'发起', N'1025', N'010501', N'/WF/Start.aspx', 3, N'CCOA', N'data1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1026.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1026_guobaogeng', N'guobaogeng', N'1026', N'发起', N'1025', N'010501', N'/WF/Start.aspx', 3, N'CCOA', N'data1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1026.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1026_guoxiangbin', N'guoxiangbin', N'1026', N'发起', N'1025', N'010501', N'/WF/Start.aspx', 3, N'CCOA', N'data1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1026.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1026_liping', N'liping', N'1026', N'发起', N'1025', N'010501', N'/WF/Start.aspx', 3, N'CCOA', N'data1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1026.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1026_liyan', N'liyan', N'1026', N'发起', N'1025', N'010501', N'/WF/Start.aspx', 3, N'CCOA', N'data1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1026.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1026_qifenglin', N'qifenglin', N'1026', N'发起', N'1025', N'010501', N'/WF/Start.aspx', 3, N'CCOA', N'data1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1026.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1026_yangyilei', N'yangyilei', N'1026', N'发起', N'1025', N'010501', N'/WF/Start.aspx', 3, N'CCOA', N'data1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1026.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1026_zhanghaicheng', N'zhanghaicheng', N'1026', N'发起', N'1025', N'010501', N'/WF/Start.aspx', 3, N'CCOA', N'data1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1026.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1026_zhangyifan', N'zhangyifan', N'1026', N'发起', N'1025', N'010501', N'/WF/Start.aspx', 3, N'CCOA', N'data1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1026.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1026_zhoupeng', N'zhoupeng', N'1026', N'发起', N'1025', N'010501', N'/WF/Start.aspx', 3, N'CCOA', N'data1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1026.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1026_zhoushengyu', N'zhoushengyu', N'1026', N'发起', N'1025', N'010501', N'/WF/Start.aspx', 3, N'CCOA', N'data1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1026.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1026_zhoutianjiao', N'zhoutianjiao', N'1026', N'发起', N'1025', N'010501', N'/WF/Start.aspx', 3, N'CCOA', N'data1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1026.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1027_admin', N'admin', N'1027', N'待办', N'1025', N'10502', N'/WF/EmpWorks.aspx', 3, N'CCOA', N'search1.png', N'', N'png', N'//DataUser/BP.GPM.Menu/1027.png', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1027_fuhui', N'fuhui', N'1027', N'待办', N'1025', N'10502', N'/WF/EmpWorks.aspx', 3, N'CCOA', N'search1.png', N'', N'png', N'//DataUser/BP.GPM.Menu/1027.png', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1027_guobaogeng', N'guobaogeng', N'1027', N'待办', N'1025', N'10502', N'/WF/EmpWorks.aspx', 3, N'CCOA', N'search1.png', N'', N'png', N'//DataUser/BP.GPM.Menu/1027.png', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1027_guoxiangbin', N'guoxiangbin', N'1027', N'待办', N'1025', N'10502', N'/WF/EmpWorks.aspx', 3, N'CCOA', N'search1.png', N'', N'png', N'//DataUser/BP.GPM.Menu/1027.png', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1027_liping', N'liping', N'1027', N'待办', N'1025', N'10502', N'/WF/EmpWorks.aspx', 3, N'CCOA', N'search1.png', N'', N'png', N'//DataUser/BP.GPM.Menu/1027.png', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1027_liyan', N'liyan', N'1027', N'待办', N'1025', N'10502', N'/WF/EmpWorks.aspx', 3, N'CCOA', N'search1.png', N'', N'png', N'//DataUser/BP.GPM.Menu/1027.png', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1027_qifenglin', N'qifenglin', N'1027', N'待办', N'1025', N'10502', N'/WF/EmpWorks.aspx', 3, N'CCOA', N'search1.png', N'', N'png', N'//DataUser/BP.GPM.Menu/1027.png', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1027_yangyilei', N'yangyilei', N'1027', N'待办', N'1025', N'10502', N'/WF/EmpWorks.aspx', 3, N'CCOA', N'search1.png', N'', N'png', N'//DataUser/BP.GPM.Menu/1027.png', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1027_zhanghaicheng', N'zhanghaicheng', N'1027', N'待办', N'1025', N'10502', N'/WF/EmpWorks.aspx', 3, N'CCOA', N'search1.png', N'', N'png', N'//DataUser/BP.GPM.Menu/1027.png', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1027_zhangyifan', N'zhangyifan', N'1027', N'待办', N'1025', N'10502', N'/WF/EmpWorks.aspx', 3, N'CCOA', N'search1.png', N'', N'png', N'//DataUser/BP.GPM.Menu/1027.png', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1027_zhoupeng', N'zhoupeng', N'1027', N'待办', N'1025', N'10502', N'/WF/EmpWorks.aspx', 3, N'CCOA', N'search1.png', N'', N'png', N'//DataUser/BP.GPM.Menu/1027.png', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1027_zhoushengyu', N'zhoushengyu', N'1027', N'待办', N'1025', N'10502', N'/WF/EmpWorks.aspx', 3, N'CCOA', N'search1.png', N'', N'png', N'//DataUser/BP.GPM.Menu/1027.png', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1027_zhoutianjiao', N'zhoutianjiao', N'1027', N'待办', N'1025', N'10502', N'/WF/EmpWorks.aspx', 3, N'CCOA', N'search1.png', N'', N'png', N'//DataUser/BP.GPM.Menu/1027.png', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1028_admin', N'admin', N'1028', N'抄送', N'1025', N'10503', N'/WF/CC.aspx', 3, N'CCOA', N'data1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1028.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1028_fuhui', N'fuhui', N'1028', N'抄送', N'1025', N'10503', N'/WF/CC.aspx', 3, N'CCOA', N'data1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1028.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1028_guobaogeng', N'guobaogeng', N'1028', N'抄送', N'1025', N'10503', N'/WF/CC.aspx', 3, N'CCOA', N'data1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1028.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1028_guoxiangbin', N'guoxiangbin', N'1028', N'抄送', N'1025', N'10503', N'/WF/CC.aspx', 3, N'CCOA', N'data1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1028.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1028_liping', N'liping', N'1028', N'抄送', N'1025', N'10503', N'/WF/CC.aspx', 3, N'CCOA', N'data1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1028.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1028_liyan', N'liyan', N'1028', N'抄送', N'1025', N'10503', N'/WF/CC.aspx', 3, N'CCOA', N'data1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1028.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1028_qifenglin', N'qifenglin', N'1028', N'抄送', N'1025', N'10503', N'/WF/CC.aspx', 3, N'CCOA', N'data1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1028.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1028_yangyilei', N'yangyilei', N'1028', N'抄送', N'1025', N'10503', N'/WF/CC.aspx', 3, N'CCOA', N'data1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1028.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1028_zhanghaicheng', N'zhanghaicheng', N'1028', N'抄送', N'1025', N'10503', N'/WF/CC.aspx', 3, N'CCOA', N'data1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1028.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1028_zhangyifan', N'zhangyifan', N'1028', N'抄送', N'1025', N'10503', N'/WF/CC.aspx', 3, N'CCOA', N'data1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1028.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1028_zhoupeng', N'zhoupeng', N'1028', N'抄送', N'1025', N'10503', N'/WF/CC.aspx', 3, N'CCOA', N'data1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1028.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1028_zhoushengyu', N'zhoushengyu', N'1028', N'抄送', N'1025', N'10503', N'/WF/CC.aspx', 3, N'CCOA', N'data1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1028.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1028_zhoutianjiao', N'zhoutianjiao', N'1028', N'抄送', N'1025', N'10503', N'/WF/CC.aspx', 3, N'CCOA', N'data1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1028.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1029_admin', N'admin', N'1029', N'挂起', N'1025', N'10504', N'/WF/HungupList.aspx', 3, N'CCOA', N'data5.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1029.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1029_fuhui', N'fuhui', N'1029', N'挂起', N'1025', N'10504', N'/WF/HungupList.aspx', 3, N'CCOA', N'data5.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1029.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1029_guobaogeng', N'guobaogeng', N'1029', N'挂起', N'1025', N'10504', N'/WF/HungupList.aspx', 3, N'CCOA', N'data5.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1029.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1029_guoxiangbin', N'guoxiangbin', N'1029', N'挂起', N'1025', N'10504', N'/WF/HungupList.aspx', 3, N'CCOA', N'data5.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1029.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1029_liping', N'liping', N'1029', N'挂起', N'1025', N'10504', N'/WF/HungupList.aspx', 3, N'CCOA', N'data5.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1029.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1029_liyan', N'liyan', N'1029', N'挂起', N'1025', N'10504', N'/WF/HungupList.aspx', 3, N'CCOA', N'data5.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1029.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1029_qifenglin', N'qifenglin', N'1029', N'挂起', N'1025', N'10504', N'/WF/HungupList.aspx', 3, N'CCOA', N'data5.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1029.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1029_yangyilei', N'yangyilei', N'1029', N'挂起', N'1025', N'10504', N'/WF/HungupList.aspx', 3, N'CCOA', N'data5.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1029.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1029_zhanghaicheng', N'zhanghaicheng', N'1029', N'挂起', N'1025', N'10504', N'/WF/HungupList.aspx', 3, N'CCOA', N'data5.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1029.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1029_zhangyifan', N'zhangyifan', N'1029', N'挂起', N'1025', N'10504', N'/WF/HungupList.aspx', 3, N'CCOA', N'data5.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1029.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1029_zhoupeng', N'zhoupeng', N'1029', N'挂起', N'1025', N'10504', N'/WF/HungupList.aspx', 3, N'CCOA', N'data5.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1029.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1029_zhoushengyu', N'zhoushengyu', N'1029', N'挂起', N'1025', N'10504', N'/WF/HungupList.aspx', 3, N'CCOA', N'data5.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1029.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1029_zhoutianjiao', N'zhoutianjiao', N'1029', N'挂起', N'1025', N'10504', N'/WF/HungupList.aspx', 3, N'CCOA', N'data5.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1029.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1030_admin', N'admin', N'1030', N'在途', N'1025', N'10505', N'/WF/Runing.aspx', 3, N'CCOA', N'data4.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1030.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1030_fuhui', N'fuhui', N'1030', N'在途', N'1025', N'10505', N'/WF/Runing.aspx', 3, N'CCOA', N'data4.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1030.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1030_guobaogeng', N'guobaogeng', N'1030', N'在途', N'1025', N'10505', N'/WF/Runing.aspx', 3, N'CCOA', N'data4.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1030.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1030_guoxiangbin', N'guoxiangbin', N'1030', N'在途', N'1025', N'10505', N'/WF/Runing.aspx', 3, N'CCOA', N'data4.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1030.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1030_liping', N'liping', N'1030', N'在途', N'1025', N'10505', N'/WF/Runing.aspx', 3, N'CCOA', N'data4.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1030.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1030_liyan', N'liyan', N'1030', N'在途', N'1025', N'10505', N'/WF/Runing.aspx', 3, N'CCOA', N'data4.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1030.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1030_qifenglin', N'qifenglin', N'1030', N'在途', N'1025', N'10505', N'/WF/Runing.aspx', 3, N'CCOA', N'data4.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1030.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1030_yangyilei', N'yangyilei', N'1030', N'在途', N'1025', N'10505', N'/WF/Runing.aspx', 3, N'CCOA', N'data4.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1030.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1030_zhanghaicheng', N'zhanghaicheng', N'1030', N'在途', N'1025', N'10505', N'/WF/Runing.aspx', 3, N'CCOA', N'data4.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1030.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1030_zhangyifan', N'zhangyifan', N'1030', N'在途', N'1025', N'10505', N'/WF/Runing.aspx', 3, N'CCOA', N'data4.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1030.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1030_zhoupeng', N'zhoupeng', N'1030', N'在途', N'1025', N'10505', N'/WF/Runing.aspx', 3, N'CCOA', N'data4.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1030.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1030_zhoushengyu', N'zhoushengyu', N'1030', N'在途', N'1025', N'10505', N'/WF/Runing.aspx', 3, N'CCOA', N'data4.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1030.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1030_zhoutianjiao', N'zhoutianjiao', N'1030', N'在途', N'1025', N'10505', N'/WF/Runing.aspx', 3, N'CCOA', N'data4.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1030.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1031_admin', N'admin', N'1031', N'查询', N'1025', N'10506', N'/WF/FlowSearch.aspx', 3, N'CCOA', N'ico_search.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1031.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1031_fuhui', N'fuhui', N'1031', N'查询', N'1025', N'10506', N'/WF/FlowSearch.aspx', 3, N'CCOA', N'ico_search.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1031.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1031_guobaogeng', N'guobaogeng', N'1031', N'查询', N'1025', N'10506', N'/WF/FlowSearch.aspx', 3, N'CCOA', N'ico_search.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1031.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1031_guoxiangbin', N'guoxiangbin', N'1031', N'查询', N'1025', N'10506', N'/WF/FlowSearch.aspx', 3, N'CCOA', N'ico_search.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1031.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1031_liping', N'liping', N'1031', N'查询', N'1025', N'10506', N'/WF/FlowSearch.aspx', 3, N'CCOA', N'ico_search.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1031.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1031_liyan', N'liyan', N'1031', N'查询', N'1025', N'10506', N'/WF/FlowSearch.aspx', 3, N'CCOA', N'ico_search.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1031.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1031_qifenglin', N'qifenglin', N'1031', N'查询', N'1025', N'10506', N'/WF/FlowSearch.aspx', 3, N'CCOA', N'ico_search.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1031.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1031_yangyilei', N'yangyilei', N'1031', N'查询', N'1025', N'10506', N'/WF/FlowSearch.aspx', 3, N'CCOA', N'ico_search.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1031.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1031_zhanghaicheng', N'zhanghaicheng', N'1031', N'查询', N'1025', N'10506', N'/WF/FlowSearch.aspx', 3, N'CCOA', N'ico_search.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1031.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1031_zhangyifan', N'zhangyifan', N'1031', N'查询', N'1025', N'10506', N'/WF/FlowSearch.aspx', 3, N'CCOA', N'ico_search.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1031.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1031_zhoupeng', N'zhoupeng', N'1031', N'查询', N'1025', N'10506', N'/WF/FlowSearch.aspx', 3, N'CCOA', N'ico_search.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1031.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1031_zhoushengyu', N'zhoushengyu', N'1031', N'查询', N'1025', N'10506', N'/WF/FlowSearch.aspx', 3, N'CCOA', N'ico_search.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1031.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1031_zhoutianjiao', N'zhoutianjiao', N'1031', N'查询', N'1025', N'10506', N'/WF/FlowSearch.aspx', 3, N'CCOA', N'ico_search.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1031.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1032_admin', N'admin', N'1032', N'关键字查询', N'1025', N'10507', N'/WF/KeySearch.aspx', 3, N'CCOA', N'cache.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1032.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1032_fuhui', N'fuhui', N'1032', N'关键字查询', N'1025', N'10507', N'/WF/KeySearch.aspx', 3, N'CCOA', N'cache.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1032.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1032_guobaogeng', N'guobaogeng', N'1032', N'关键字查询', N'1025', N'10507', N'/WF/KeySearch.aspx', 3, N'CCOA', N'cache.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1032.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1032_guoxiangbin', N'guoxiangbin', N'1032', N'关键字查询', N'1025', N'10507', N'/WF/KeySearch.aspx', 3, N'CCOA', N'cache.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1032.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1032_liping', N'liping', N'1032', N'关键字查询', N'1025', N'10507', N'/WF/KeySearch.aspx', 3, N'CCOA', N'cache.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1032.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1032_liyan', N'liyan', N'1032', N'关键字查询', N'1025', N'10507', N'/WF/KeySearch.aspx', 3, N'CCOA', N'cache.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1032.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1032_qifenglin', N'qifenglin', N'1032', N'关键字查询', N'1025', N'10507', N'/WF/KeySearch.aspx', 3, N'CCOA', N'cache.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1032.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1032_yangyilei', N'yangyilei', N'1032', N'关键字查询', N'1025', N'10507', N'/WF/KeySearch.aspx', 3, N'CCOA', N'cache.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1032.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1032_zhanghaicheng', N'zhanghaicheng', N'1032', N'关键字查询', N'1025', N'10507', N'/WF/KeySearch.aspx', 3, N'CCOA', N'cache.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1032.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1032_zhangyifan', N'zhangyifan', N'1032', N'关键字查询', N'1025', N'10507', N'/WF/KeySearch.aspx', 3, N'CCOA', N'cache.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1032.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1032_zhoupeng', N'zhoupeng', N'1032', N'关键字查询', N'1025', N'10507', N'/WF/KeySearch.aspx', 3, N'CCOA', N'cache.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1032.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1032_zhoushengyu', N'zhoushengyu', N'1032', N'关键字查询', N'1025', N'10507', N'/WF/KeySearch.aspx', 3, N'CCOA', N'cache.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1032.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1032_zhoutianjiao', N'zhoutianjiao', N'1032', N'关键字查询', N'1025', N'10507', N'/WF/KeySearch.aspx', 3, N'CCOA', N'cache.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1032.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1033_admin', N'admin', N'1033', N'取消审批', N'1025', N'10508', N'/WF/GetTask.aspx', 3, N'CCOA', N'color.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1033.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1033_fuhui', N'fuhui', N'1033', N'取消审批', N'1025', N'10508', N'/WF/GetTask.aspx', 3, N'CCOA', N'color.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1033.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1033_guobaogeng', N'guobaogeng', N'1033', N'取消审批', N'1025', N'10508', N'/WF/GetTask.aspx', 3, N'CCOA', N'color.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1033.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1033_guoxiangbin', N'guoxiangbin', N'1033', N'取消审批', N'1025', N'10508', N'/WF/GetTask.aspx', 3, N'CCOA', N'color.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1033.gif', 16, 16, 0)
GO
print 'Processed 300 total records'
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1033_liping', N'liping', N'1033', N'取消审批', N'1025', N'10508', N'/WF/GetTask.aspx', 3, N'CCOA', N'color.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1033.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1033_liyan', N'liyan', N'1033', N'取消审批', N'1025', N'10508', N'/WF/GetTask.aspx', 3, N'CCOA', N'color.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1033.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1033_qifenglin', N'qifenglin', N'1033', N'取消审批', N'1025', N'10508', N'/WF/GetTask.aspx', 3, N'CCOA', N'color.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1033.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1033_yangyilei', N'yangyilei', N'1033', N'取消审批', N'1025', N'10508', N'/WF/GetTask.aspx', 3, N'CCOA', N'color.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1033.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1033_zhanghaicheng', N'zhanghaicheng', N'1033', N'取消审批', N'1025', N'10508', N'/WF/GetTask.aspx', 3, N'CCOA', N'color.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1033.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1033_zhangyifan', N'zhangyifan', N'1033', N'取消审批', N'1025', N'10508', N'/WF/GetTask.aspx', 3, N'CCOA', N'color.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1033.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1033_zhoupeng', N'zhoupeng', N'1033', N'取消审批', N'1025', N'10508', N'/WF/GetTask.aspx', 3, N'CCOA', N'color.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1033.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1033_zhoushengyu', N'zhoushengyu', N'1033', N'取消审批', N'1025', N'10508', N'/WF/GetTask.aspx', 3, N'CCOA', N'color.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1033.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1033_zhoutianjiao', N'zhoutianjiao', N'1033', N'取消审批', N'1025', N'10508', N'/WF/GetTask.aspx', 3, N'CCOA', N'color.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1033.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1034_admin', N'admin', N'1034', N'成员', N'1025', N'10509', N'/WF/Emps.aspx', 3, N'CCOA', N'data3.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1034.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1034_fuhui', N'fuhui', N'1034', N'成员', N'1025', N'10509', N'/WF/Emps.aspx', 3, N'CCOA', N'data3.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1034.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1034_guobaogeng', N'guobaogeng', N'1034', N'成员', N'1025', N'10509', N'/WF/Emps.aspx', 3, N'CCOA', N'data3.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1034.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1034_guoxiangbin', N'guoxiangbin', N'1034', N'成员', N'1025', N'10509', N'/WF/Emps.aspx', 3, N'CCOA', N'data3.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1034.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1034_liping', N'liping', N'1034', N'成员', N'1025', N'10509', N'/WF/Emps.aspx', 3, N'CCOA', N'data3.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1034.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1034_liyan', N'liyan', N'1034', N'成员', N'1025', N'10509', N'/WF/Emps.aspx', 3, N'CCOA', N'data3.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1034.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1034_qifenglin', N'qifenglin', N'1034', N'成员', N'1025', N'10509', N'/WF/Emps.aspx', 3, N'CCOA', N'data3.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1034.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1034_yangyilei', N'yangyilei', N'1034', N'成员', N'1025', N'10509', N'/WF/Emps.aspx', 3, N'CCOA', N'data3.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1034.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1034_zhanghaicheng', N'zhanghaicheng', N'1034', N'成员', N'1025', N'10509', N'/WF/Emps.aspx', 3, N'CCOA', N'data3.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1034.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1034_zhangyifan', N'zhangyifan', N'1034', N'成员', N'1025', N'10509', N'/WF/Emps.aspx', 3, N'CCOA', N'data3.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1034.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1034_zhoupeng', N'zhoupeng', N'1034', N'成员', N'1025', N'10509', N'/WF/Emps.aspx', 3, N'CCOA', N'data3.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1034.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1034_zhoushengyu', N'zhoushengyu', N'1034', N'成员', N'1025', N'10509', N'/WF/Emps.aspx', 3, N'CCOA', N'data3.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1034.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1034_zhoutianjiao', N'zhoutianjiao', N'1034', N'成员', N'1025', N'10509', N'/WF/Emps.aspx', 3, N'CCOA', N'data3.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1034.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1036_admin', N'admin', N'1036', N'设置', N'1025', N'10511', N'/WF/Tools.aspx', 3, N'CCOA', N'ico_html.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1036.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1036_fuhui', N'fuhui', N'1036', N'设置', N'1025', N'10511', N'/WF/Tools.aspx', 3, N'CCOA', N'ico_html.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1036.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1036_guobaogeng', N'guobaogeng', N'1036', N'设置', N'1025', N'10511', N'/WF/Tools.aspx', 3, N'CCOA', N'ico_html.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1036.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1036_guoxiangbin', N'guoxiangbin', N'1036', N'设置', N'1025', N'10511', N'/WF/Tools.aspx', 3, N'CCOA', N'ico_html.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1036.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1036_liping', N'liping', N'1036', N'设置', N'1025', N'10511', N'/WF/Tools.aspx', 3, N'CCOA', N'ico_html.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1036.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1036_liyan', N'liyan', N'1036', N'设置', N'1025', N'10511', N'/WF/Tools.aspx', 3, N'CCOA', N'ico_html.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1036.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1036_qifenglin', N'qifenglin', N'1036', N'设置', N'1025', N'10511', N'/WF/Tools.aspx', 3, N'CCOA', N'ico_html.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1036.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1036_yangyilei', N'yangyilei', N'1036', N'设置', N'1025', N'10511', N'/WF/Tools.aspx', 3, N'CCOA', N'ico_html.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1036.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1036_zhanghaicheng', N'zhanghaicheng', N'1036', N'设置', N'1025', N'10511', N'/WF/Tools.aspx', 3, N'CCOA', N'ico_html.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1036.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1036_zhangyifan', N'zhangyifan', N'1036', N'设置', N'1025', N'10511', N'/WF/Tools.aspx', 3, N'CCOA', N'ico_html.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1036.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1036_zhoupeng', N'zhoupeng', N'1036', N'设置', N'1025', N'10511', N'/WF/Tools.aspx', 3, N'CCOA', N'ico_html.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1036.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1036_zhoushengyu', N'zhoushengyu', N'1036', N'设置', N'1025', N'10511', N'/WF/Tools.aspx', 3, N'CCOA', N'ico_html.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1036.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1036_zhoutianjiao', N'zhoutianjiao', N'1036', N'设置', N'1025', N'10511', N'/WF/Tools.aspx', 3, N'CCOA', N'ico_html.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1036.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1037_admin', N'admin', N'1037', N'我的日程', N'1010', N'010102', N'/app/Calendar/MyCalendar.aspx', 4, N'CCOA', N'calendar.png', N'', N'png', N'//DataUser/BP.GPM.Menu/1037.png', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1037_fuhui', N'fuhui', N'1037', N'我的日程', N'1010', N'010102', N'/app/Calendar/MyCalendar.aspx', 4, N'CCOA', N'calendar.png', N'', N'png', N'//DataUser/BP.GPM.Menu/1037.png', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1037_guobaogeng', N'guobaogeng', N'1037', N'我的日程', N'1010', N'010102', N'/app/Calendar/MyCalendar.aspx', 4, N'CCOA', N'calendar.png', N'', N'png', N'//DataUser/BP.GPM.Menu/1037.png', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1037_guoxiangbin', N'guoxiangbin', N'1037', N'我的日程', N'1010', N'010102', N'/app/Calendar/MyCalendar.aspx', 4, N'CCOA', N'calendar.png', N'', N'png', N'//DataUser/BP.GPM.Menu/1037.png', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1037_liping', N'liping', N'1037', N'我的日程', N'1010', N'010102', N'/app/Calendar/MyCalendar.aspx', 4, N'CCOA', N'calendar.png', N'', N'png', N'//DataUser/BP.GPM.Menu/1037.png', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1037_liyan', N'liyan', N'1037', N'我的日程', N'1010', N'010102', N'/app/Calendar/MyCalendar.aspx', 4, N'CCOA', N'calendar.png', N'', N'png', N'//DataUser/BP.GPM.Menu/1037.png', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1037_qifenglin', N'qifenglin', N'1037', N'我的日程', N'1010', N'010102', N'/app/Calendar/MyCalendar.aspx', 4, N'CCOA', N'calendar.png', N'', N'png', N'//DataUser/BP.GPM.Menu/1037.png', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1037_yangyilei', N'yangyilei', N'1037', N'我的日程', N'1010', N'010102', N'/app/Calendar/MyCalendar.aspx', 4, N'CCOA', N'calendar.png', N'', N'png', N'//DataUser/BP.GPM.Menu/1037.png', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1037_zhanghaicheng', N'zhanghaicheng', N'1037', N'我的日程', N'1010', N'010102', N'/app/Calendar/MyCalendar.aspx', 4, N'CCOA', N'calendar.png', N'', N'png', N'//DataUser/BP.GPM.Menu/1037.png', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1037_zhangyifan', N'zhangyifan', N'1037', N'我的日程', N'1010', N'010102', N'/app/Calendar/MyCalendar.aspx', 4, N'CCOA', N'calendar.png', N'', N'png', N'//DataUser/BP.GPM.Menu/1037.png', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1037_zhoupeng', N'zhoupeng', N'1037', N'我的日程', N'1010', N'010102', N'/app/Calendar/MyCalendar.aspx', 4, N'CCOA', N'calendar.png', N'', N'png', N'//DataUser/BP.GPM.Menu/1037.png', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1037_zhoushengyu', N'zhoushengyu', N'1037', N'我的日程', N'1010', N'010102', N'/app/Calendar/MyCalendar.aspx', 4, N'CCOA', N'calendar.png', N'', N'png', N'//DataUser/BP.GPM.Menu/1037.png', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1037_zhoutianjiao', N'zhoutianjiao', N'1037', N'我的日程', N'1010', N'010102', N'/app/Calendar/MyCalendar.aspx', 4, N'CCOA', N'calendar.png', N'', N'png', N'//DataUser/BP.GPM.Menu/1037.png', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1038_admin', N'admin', N'1038', N'创建流程', N'1025', N'10512', N'/WF/Admin/XAP/Designer.aspx', 3, N'CCOA', N'com.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1038.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1038_fuhui', N'fuhui', N'1038', N'创建流程', N'1025', N'10512', N'/WF/Admin/XAP/Designer.aspx', 3, N'CCOA', N'com.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1038.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1038_guobaogeng', N'guobaogeng', N'1038', N'创建流程', N'1025', N'10512', N'/WF/Admin/XAP/Designer.aspx', 3, N'CCOA', N'com.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1038.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1038_guoxiangbin', N'guoxiangbin', N'1038', N'创建流程', N'1025', N'10512', N'/WF/Admin/XAP/Designer.aspx', 3, N'CCOA', N'com.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1038.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1038_liping', N'liping', N'1038', N'创建流程', N'1025', N'10512', N'/WF/Admin/XAP/Designer.aspx', 3, N'CCOA', N'com.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1038.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1038_liyan', N'liyan', N'1038', N'创建流程', N'1025', N'10512', N'/WF/Admin/XAP/Designer.aspx', 3, N'CCOA', N'com.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1038.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1038_qifenglin', N'qifenglin', N'1038', N'创建流程', N'1025', N'10512', N'/WF/Admin/XAP/Designer.aspx', 3, N'CCOA', N'com.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1038.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1038_yangyilei', N'yangyilei', N'1038', N'创建流程', N'1025', N'10512', N'/WF/Admin/XAP/Designer.aspx', 3, N'CCOA', N'com.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1038.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1038_zhanghaicheng', N'zhanghaicheng', N'1038', N'创建流程', N'1025', N'10512', N'/WF/Admin/XAP/Designer.aspx', 3, N'CCOA', N'com.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1038.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1038_zhangyifan', N'zhangyifan', N'1038', N'创建流程', N'1025', N'10512', N'/WF/Admin/XAP/Designer.aspx', 3, N'CCOA', N'com.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1038.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1038_zhoupeng', N'zhoupeng', N'1038', N'创建流程', N'1025', N'10512', N'/WF/Admin/XAP/Designer.aspx', 3, N'CCOA', N'com.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1038.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1038_zhoushengyu', N'zhoushengyu', N'1038', N'创建流程', N'1025', N'10512', N'/WF/Admin/XAP/Designer.aspx', 3, N'CCOA', N'com.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1038.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1038_zhoutianjiao', N'zhoutianjiao', N'1038', N'创建流程', N'1025', N'10512', N'/WF/Admin/XAP/Designer.aspx', 3, N'CCOA', N'com.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1038.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1039_admin', N'admin', N'1039', N'草稿箱', N'1016', N'10304', N'/app/Message/DraftBox.aspx', 4, N'CCOA', N'Rpt.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1039.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1039_fuhui', N'fuhui', N'1039', N'草稿箱', N'1016', N'10304', N'/app/Message/DraftBox.aspx', 4, N'CCOA', N'Rpt.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1039.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1039_guobaogeng', N'guobaogeng', N'1039', N'草稿箱', N'1016', N'10304', N'/app/Message/DraftBox.aspx', 4, N'CCOA', N'Rpt.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1039.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1039_guoxiangbin', N'guoxiangbin', N'1039', N'草稿箱', N'1016', N'10304', N'/app/Message/DraftBox.aspx', 4, N'CCOA', N'Rpt.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1039.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1039_liping', N'liping', N'1039', N'草稿箱', N'1016', N'10304', N'/app/Message/DraftBox.aspx', 4, N'CCOA', N'Rpt.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1039.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1039_liyan', N'liyan', N'1039', N'草稿箱', N'1016', N'10304', N'/app/Message/DraftBox.aspx', 4, N'CCOA', N'Rpt.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1039.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1039_qifenglin', N'qifenglin', N'1039', N'草稿箱', N'1016', N'10304', N'/app/Message/DraftBox.aspx', 4, N'CCOA', N'Rpt.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1039.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1039_yangyilei', N'yangyilei', N'1039', N'草稿箱', N'1016', N'10304', N'/app/Message/DraftBox.aspx', 4, N'CCOA', N'Rpt.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1039.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1039_zhanghaicheng', N'zhanghaicheng', N'1039', N'草稿箱', N'1016', N'10304', N'/app/Message/DraftBox.aspx', 4, N'CCOA', N'Rpt.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1039.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1039_zhangyifan', N'zhangyifan', N'1039', N'草稿箱', N'1016', N'10304', N'/app/Message/DraftBox.aspx', 4, N'CCOA', N'Rpt.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1039.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1039_zhoupeng', N'zhoupeng', N'1039', N'草稿箱', N'1016', N'10304', N'/app/Message/DraftBox.aspx', 4, N'CCOA', N'Rpt.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1039.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1039_zhoushengyu', N'zhoushengyu', N'1039', N'草稿箱', N'1016', N'10304', N'/app/Message/DraftBox.aspx', 4, N'CCOA', N'Rpt.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1039.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1039_zhoutianjiao', N'zhoutianjiao', N'1039', N'草稿箱', N'1016', N'10304', N'/app/Message/DraftBox.aspx', 4, N'CCOA', N'Rpt.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1039.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1040_admin', N'admin', N'1040', N'垃圾箱', N'1016', N'10305', N'/App/Message/RecycleBox.aspx', 4, N'CCOA', N'admin.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1040.gif', 17, 19, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1040_fuhui', N'fuhui', N'1040', N'垃圾箱', N'1016', N'10305', N'/App/Message/RecycleBox.aspx', 4, N'CCOA', N'admin.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1040.gif', 17, 19, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1040_guobaogeng', N'guobaogeng', N'1040', N'垃圾箱', N'1016', N'10305', N'/App/Message/RecycleBox.aspx', 4, N'CCOA', N'admin.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1040.gif', 17, 19, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1040_guoxiangbin', N'guoxiangbin', N'1040', N'垃圾箱', N'1016', N'10305', N'/App/Message/RecycleBox.aspx', 4, N'CCOA', N'admin.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1040.gif', 17, 19, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1040_liping', N'liping', N'1040', N'垃圾箱', N'1016', N'10305', N'/App/Message/RecycleBox.aspx', 4, N'CCOA', N'admin.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1040.gif', 17, 19, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1040_liyan', N'liyan', N'1040', N'垃圾箱', N'1016', N'10305', N'/App/Message/RecycleBox.aspx', 4, N'CCOA', N'admin.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1040.gif', 17, 19, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1040_qifenglin', N'qifenglin', N'1040', N'垃圾箱', N'1016', N'10305', N'/App/Message/RecycleBox.aspx', 4, N'CCOA', N'admin.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1040.gif', 17, 19, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1040_yangyilei', N'yangyilei', N'1040', N'垃圾箱', N'1016', N'10305', N'/App/Message/RecycleBox.aspx', 4, N'CCOA', N'admin.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1040.gif', 17, 19, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1040_zhanghaicheng', N'zhanghaicheng', N'1040', N'垃圾箱', N'1016', N'10305', N'/App/Message/RecycleBox.aspx', 4, N'CCOA', N'admin.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1040.gif', 17, 19, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1040_zhangyifan', N'zhangyifan', N'1040', N'垃圾箱', N'1016', N'10305', N'/App/Message/RecycleBox.aspx', 4, N'CCOA', N'admin.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1040.gif', 17, 19, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1040_zhoupeng', N'zhoupeng', N'1040', N'垃圾箱', N'1016', N'10305', N'/App/Message/RecycleBox.aspx', 4, N'CCOA', N'admin.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1040.gif', 17, 19, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1040_zhoushengyu', N'zhoushengyu', N'1040', N'垃圾箱', N'1016', N'10305', N'/App/Message/RecycleBox.aspx', 4, N'CCOA', N'admin.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1040.gif', 17, 19, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'1040_zhoutianjiao', N'zhoutianjiao', N'1040', N'垃圾箱', N'1016', N'10305', N'/App/Message/RecycleBox.aspx', 4, N'CCOA', N'admin.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/1040.gif', 17, 19, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2000_admin', N'admin', N'2000', N'业务系统', N'1000', N'0101', N'', 1, N'AppSort', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2000_fuhui', N'fuhui', N'2000', N'业务系统', N'1000', N'0101', N'', 1, N'AppSort', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2000_guobaogeng', N'guobaogeng', N'2000', N'业务系统', N'1000', N'0101', N'', 1, N'AppSort', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2000_guoxiangbin', N'guoxiangbin', N'2000', N'业务系统', N'1000', N'0101', N'', 1, N'AppSort', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2000_liping', N'liping', N'2000', N'业务系统', N'1000', N'0101', N'', 1, N'AppSort', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2000_liyan', N'liyan', N'2000', N'业务系统', N'1000', N'0101', N'', 1, N'AppSort', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2000_qifenglin', N'qifenglin', N'2000', N'业务系统', N'1000', N'0101', N'', 1, N'AppSort', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2000_yangyilei', N'yangyilei', N'2000', N'业务系统', N'1000', N'0101', N'', 1, N'AppSort', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2000_zhanghaicheng', N'zhanghaicheng', N'2000', N'业务系统', N'1000', N'0101', N'', 1, N'AppSort', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2000_zhangyifan', N'zhangyifan', N'2000', N'业务系统', N'1000', N'0101', N'', 1, N'AppSort', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2000_zhoupeng', N'zhoupeng', N'2000', N'业务系统', N'1000', N'0101', N'', 1, N'AppSort', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2000_zhoushengyu', N'zhoushengyu', N'2000', N'业务系统', N'1000', N'0101', N'', 1, N'AppSort', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2000_zhoutianjiao', N'zhoutianjiao', N'2000', N'业务系统', N'1000', N'0101', N'', 1, N'AppSort', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2001_admin', N'admin', N'2001', N'办公系统', N'1000', N'0102', N'', 1, N'AppSort', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
GO
print 'Processed 400 total records'
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2001_fuhui', N'fuhui', N'2001', N'办公系统', N'1000', N'0102', N'', 1, N'AppSort', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2001_guobaogeng', N'guobaogeng', N'2001', N'办公系统', N'1000', N'0102', N'', 1, N'AppSort', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2001_guoxiangbin', N'guoxiangbin', N'2001', N'办公系统', N'1000', N'0102', N'', 1, N'AppSort', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2001_liping', N'liping', N'2001', N'办公系统', N'1000', N'0102', N'', 1, N'AppSort', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2001_liyan', N'liyan', N'2001', N'办公系统', N'1000', N'0102', N'', 1, N'AppSort', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2001_qifenglin', N'qifenglin', N'2001', N'办公系统', N'1000', N'0102', N'', 1, N'AppSort', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2001_yangyilei', N'yangyilei', N'2001', N'办公系统', N'1000', N'0102', N'', 1, N'AppSort', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2001_zhanghaicheng', N'zhanghaicheng', N'2001', N'办公系统', N'1000', N'0102', N'', 1, N'AppSort', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2001_zhangyifan', N'zhangyifan', N'2001', N'办公系统', N'1000', N'0102', N'', 1, N'AppSort', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2001_zhoupeng', N'zhoupeng', N'2001', N'办公系统', N'1000', N'0102', N'', 1, N'AppSort', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2001_zhoushengyu', N'zhoushengyu', N'2001', N'办公系统', N'1000', N'0102', N'', 1, N'AppSort', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2001_zhoutianjiao', N'zhoutianjiao', N'2001', N'办公系统', N'1000', N'0102', N'', 1, N'AppSort', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2002_admin', N'admin', N'2002', N'驰骋OA', N'2000', N'0103', N'', 2, N'CCOA', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2002_fuhui', N'fuhui', N'2002', N'驰骋OA', N'2000', N'0103', N'', 2, N'CCOA', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2002_guobaogeng', N'guobaogeng', N'2002', N'驰骋OA', N'2000', N'0103', N'', 2, N'CCOA', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2002_guoxiangbin', N'guoxiangbin', N'2002', N'驰骋OA', N'2000', N'0103', N'', 2, N'CCOA', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2002_liping', N'liping', N'2002', N'驰骋OA', N'2000', N'0103', N'', 2, N'CCOA', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2002_liyan', N'liyan', N'2002', N'驰骋OA', N'2000', N'0103', N'', 2, N'CCOA', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2002_qifenglin', N'qifenglin', N'2002', N'驰骋OA', N'2000', N'0103', N'', 2, N'CCOA', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2002_yangyilei', N'yangyilei', N'2002', N'驰骋OA', N'2000', N'0103', N'', 2, N'CCOA', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2002_zhanghaicheng', N'zhanghaicheng', N'2002', N'驰骋OA', N'2000', N'0103', N'', 2, N'CCOA', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2002_zhangyifan', N'zhangyifan', N'2002', N'驰骋OA', N'2000', N'0103', N'', 2, N'CCOA', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2002_zhoupeng', N'zhoupeng', N'2002', N'驰骋OA', N'2000', N'0103', N'', 2, N'CCOA', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2002_zhoushengyu', N'zhoushengyu', N'2002', N'驰骋OA', N'2000', N'0103', N'', 2, N'CCOA', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2002_zhoutianjiao', N'zhoutianjiao', N'2002', N'驰骋OA', N'2000', N'0103', N'', 2, N'CCOA', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2003_admin', N'admin', N'2003', N'Tester', N'2001', N'0104', N'', 2, N'Tester', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2003_fuhui', N'fuhui', N'2003', N'Tester', N'2001', N'0104', N'', 2, N'Tester', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2003_guobaogeng', N'guobaogeng', N'2003', N'Tester', N'2001', N'0104', N'', 2, N'Tester', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2003_guoxiangbin', N'guoxiangbin', N'2003', N'Tester', N'2001', N'0104', N'', 2, N'Tester', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2003_liping', N'liping', N'2003', N'Tester', N'2001', N'0104', N'', 2, N'Tester', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2003_liyan', N'liyan', N'2003', N'Tester', N'2001', N'0104', N'', 2, N'Tester', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2003_qifenglin', N'qifenglin', N'2003', N'Tester', N'2001', N'0104', N'', 2, N'Tester', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2003_yangyilei', N'yangyilei', N'2003', N'Tester', N'2001', N'0104', N'', 2, N'Tester', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2003_zhanghaicheng', N'zhanghaicheng', N'2003', N'Tester', N'2001', N'0104', N'', 2, N'Tester', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2003_zhangyifan', N'zhangyifan', N'2003', N'Tester', N'2001', N'0104', N'', 2, N'Tester', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2003_zhoupeng', N'zhoupeng', N'2003', N'Tester', N'2001', N'0104', N'', 2, N'Tester', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2003_zhoushengyu', N'zhoushengyu', N'2003', N'Tester', N'2001', N'0104', N'', 2, N'Tester', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2003_zhoutianjiao', N'zhoutianjiao', N'2003', N'Tester', N'2001', N'0104', N'', 2, N'Tester', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2004_admin', N'admin', N'2004', N'工作流程', N'2000', N'0105', N'', 2, N'CCFlow', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2004_fuhui', N'fuhui', N'2004', N'工作流程', N'2000', N'0105', N'', 2, N'CCFlow', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2004_guobaogeng', N'guobaogeng', N'2004', N'工作流程', N'2000', N'0105', N'', 2, N'CCFlow', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2004_guoxiangbin', N'guoxiangbin', N'2004', N'工作流程', N'2000', N'0105', N'', 2, N'CCFlow', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2004_liping', N'liping', N'2004', N'工作流程', N'2000', N'0105', N'', 2, N'CCFlow', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2004_liyan', N'liyan', N'2004', N'工作流程', N'2000', N'0105', N'', 2, N'CCFlow', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2004_qifenglin', N'qifenglin', N'2004', N'工作流程', N'2000', N'0105', N'', 2, N'CCFlow', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2004_yangyilei', N'yangyilei', N'2004', N'工作流程', N'2000', N'0105', N'', 2, N'CCFlow', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2004_zhanghaicheng', N'zhanghaicheng', N'2004', N'工作流程', N'2000', N'0105', N'', 2, N'CCFlow', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2004_zhangyifan', N'zhangyifan', N'2004', N'工作流程', N'2000', N'0105', N'', 2, N'CCFlow', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2004_zhoupeng', N'zhoupeng', N'2004', N'工作流程', N'2000', N'0105', N'', 2, N'CCFlow', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2004_zhoushengyu', N'zhoushengyu', N'2004', N'工作流程', N'2000', N'0105', N'', 2, N'CCFlow', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2004_zhoutianjiao', N'zhoutianjiao', N'2004', N'工作流程', N'2000', N'0105', N'', 2, N'CCFlow', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2005_admin', N'admin', N'2005', N'即时通讯', N'2000', N'0106', N'', 2, N'CCIM', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2005_fuhui', N'fuhui', N'2005', N'即时通讯', N'2000', N'0106', N'', 2, N'CCIM', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2005_guobaogeng', N'guobaogeng', N'2005', N'即时通讯', N'2000', N'0106', N'', 2, N'CCIM', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2005_guoxiangbin', N'guoxiangbin', N'2005', N'即时通讯', N'2000', N'0106', N'', 2, N'CCIM', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2005_liping', N'liping', N'2005', N'即时通讯', N'2000', N'0106', N'', 2, N'CCIM', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2005_liyan', N'liyan', N'2005', N'即时通讯', N'2000', N'0106', N'', 2, N'CCIM', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2005_qifenglin', N'qifenglin', N'2005', N'即时通讯', N'2000', N'0106', N'', 2, N'CCIM', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2005_yangyilei', N'yangyilei', N'2005', N'即时通讯', N'2000', N'0106', N'', 2, N'CCIM', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2005_zhanghaicheng', N'zhanghaicheng', N'2005', N'即时通讯', N'2000', N'0106', N'', 2, N'CCIM', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2005_zhangyifan', N'zhangyifan', N'2005', N'即时通讯', N'2000', N'0106', N'', 2, N'CCIM', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2005_zhoupeng', N'zhoupeng', N'2005', N'即时通讯', N'2000', N'0106', N'', 2, N'CCIM', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2005_zhoushengyu', N'zhoushengyu', N'2005', N'即时通讯', N'2000', N'0106', N'', 2, N'CCIM', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2005_zhoutianjiao', N'zhoutianjiao', N'2005', N'即时通讯', N'2000', N'0106', N'', 2, N'CCIM', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2006_admin', N'admin', N'2006', N'公共信息网', N'2000', N'0107', N'', 2, N'GGXXW', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2006_fuhui', N'fuhui', N'2006', N'公共信息网', N'2000', N'0107', N'', 2, N'GGXXW', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2006_guobaogeng', N'guobaogeng', N'2006', N'公共信息网', N'2000', N'0107', N'', 2, N'GGXXW', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2006_guoxiangbin', N'guoxiangbin', N'2006', N'公共信息网', N'2000', N'0107', N'', 2, N'GGXXW', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2006_liping', N'liping', N'2006', N'公共信息网', N'2000', N'0107', N'', 2, N'GGXXW', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2006_liyan', N'liyan', N'2006', N'公共信息网', N'2000', N'0107', N'', 2, N'GGXXW', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2006_qifenglin', N'qifenglin', N'2006', N'公共信息网', N'2000', N'0107', N'', 2, N'GGXXW', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2006_yangyilei', N'yangyilei', N'2006', N'公共信息网', N'2000', N'0107', N'', 2, N'GGXXW', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2006_zhanghaicheng', N'zhanghaicheng', N'2006', N'公共信息网', N'2000', N'0107', N'', 2, N'GGXXW', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2006_zhangyifan', N'zhangyifan', N'2006', N'公共信息网', N'2000', N'0107', N'', 2, N'GGXXW', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2006_zhoupeng', N'zhoupeng', N'2006', N'公共信息网', N'2000', N'0107', N'', 2, N'GGXXW', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2006_zhoushengyu', N'zhoushengyu', N'2006', N'公共信息网', N'2000', N'0107', N'', 2, N'GGXXW', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2006_zhoutianjiao', N'zhoutianjiao', N'2006', N'公共信息网', N'2000', N'0107', N'', 2, N'GGXXW', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2007_admin', N'admin', N'2007', N'权限管理', N'2000', N'0108', N'', 2, N'GPM', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2007_fuhui', N'fuhui', N'2007', N'权限管理', N'2000', N'0108', N'', 2, N'GPM', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2007_guobaogeng', N'guobaogeng', N'2007', N'权限管理', N'2000', N'0108', N'', 2, N'GPM', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2007_guoxiangbin', N'guoxiangbin', N'2007', N'权限管理', N'2000', N'0108', N'', 2, N'GPM', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2007_liping', N'liping', N'2007', N'权限管理', N'2000', N'0108', N'', 2, N'GPM', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2007_liyan', N'liyan', N'2007', N'权限管理', N'2000', N'0108', N'', 2, N'GPM', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2007_qifenglin', N'qifenglin', N'2007', N'权限管理', N'2000', N'0108', N'', 2, N'GPM', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2007_yangyilei', N'yangyilei', N'2007', N'权限管理', N'2000', N'0108', N'', 2, N'GPM', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2007_zhanghaicheng', N'zhanghaicheng', N'2007', N'权限管理', N'2000', N'0108', N'', 2, N'GPM', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2007_zhangyifan', N'zhangyifan', N'2007', N'权限管理', N'2000', N'0108', N'', 2, N'GPM', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2007_zhoupeng', N'zhoupeng', N'2007', N'权限管理', N'2000', N'0108', N'', 2, N'GPM', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2007_zhoushengyu', N'zhoushengyu', N'2007', N'权限管理', N'2000', N'0108', N'', 2, N'GPM', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2007_zhoutianjiao', N'zhoutianjiao', N'2007', N'权限管理', N'2000', N'0108', N'', 2, N'GPM', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2008_admin', N'admin', N'2008', N'单点登陆', N'2000', N'0109', N'', 2, N'SSO', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2008_fuhui', N'fuhui', N'2008', N'单点登陆', N'2000', N'0109', N'', 2, N'SSO', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2008_guobaogeng', N'guobaogeng', N'2008', N'单点登陆', N'2000', N'0109', N'', 2, N'SSO', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2008_guoxiangbin', N'guoxiangbin', N'2008', N'单点登陆', N'2000', N'0109', N'', 2, N'SSO', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2008_liping', N'liping', N'2008', N'单点登陆', N'2000', N'0109', N'', 2, N'SSO', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2008_liyan', N'liyan', N'2008', N'单点登陆', N'2000', N'0109', N'', 2, N'SSO', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2008_qifenglin', N'qifenglin', N'2008', N'单点登陆', N'2000', N'0109', N'', 2, N'SSO', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2008_yangyilei', N'yangyilei', N'2008', N'单点登陆', N'2000', N'0109', N'', 2, N'SSO', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2008_zhanghaicheng', N'zhanghaicheng', N'2008', N'单点登陆', N'2000', N'0109', N'', 2, N'SSO', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2008_zhangyifan', N'zhangyifan', N'2008', N'单点登陆', N'2000', N'0109', N'', 2, N'SSO', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2008_zhoupeng', N'zhoupeng', N'2008', N'单点登陆', N'2000', N'0109', N'', 2, N'SSO', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
GO
print 'Processed 500 total records'
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2008_zhoushengyu', N'zhoushengyu', N'2008', N'单点登陆', N'2000', N'0109', N'', 2, N'SSO', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2008_zhoutianjiao', N'zhoutianjiao', N'2008', N'单点登陆', N'2000', N'0109', N'', 2, N'SSO', N'', N'', N'', N'/WF/Img/FileType/IE.gif', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2010_admin', N'admin', N'2010', N'发送短消息', N'1010', N'10103', N'/Main/Sys/SendShortMsg.aspx', 3, N'CCOA', N'new1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/2010.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2010_fuhui', N'fuhui', N'2010', N'发送短消息', N'1010', N'10103', N'/Main/Sys/SendShortMsg.aspx', 3, N'CCOA', N'new1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/2010.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2010_guobaogeng', N'guobaogeng', N'2010', N'发送短消息', N'1010', N'10103', N'/Main/Sys/SendShortMsg.aspx', 3, N'CCOA', N'new1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/2010.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2010_guoxiangbin', N'guoxiangbin', N'2010', N'发送短消息', N'1010', N'10103', N'/Main/Sys/SendShortMsg.aspx', 3, N'CCOA', N'new1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/2010.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2010_liping', N'liping', N'2010', N'发送短消息', N'1010', N'10103', N'/Main/Sys/SendShortMsg.aspx', 3, N'CCOA', N'new1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/2010.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2010_liyan', N'liyan', N'2010', N'发送短消息', N'1010', N'10103', N'/Main/Sys/SendShortMsg.aspx', 3, N'CCOA', N'new1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/2010.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2010_qifenglin', N'qifenglin', N'2010', N'发送短消息', N'1010', N'10103', N'/Main/Sys/SendShortMsg.aspx', 3, N'CCOA', N'new1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/2010.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2010_yangyilei', N'yangyilei', N'2010', N'发送短消息', N'1010', N'10103', N'/Main/Sys/SendShortMsg.aspx', 3, N'CCOA', N'new1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/2010.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2010_zhanghaicheng', N'zhanghaicheng', N'2010', N'发送短消息', N'1010', N'10103', N'/Main/Sys/SendShortMsg.aspx', 3, N'CCOA', N'new1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/2010.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2010_zhangyifan', N'zhangyifan', N'2010', N'发送短消息', N'1010', N'10103', N'/Main/Sys/SendShortMsg.aspx', 3, N'CCOA', N'new1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/2010.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2010_zhoupeng', N'zhoupeng', N'2010', N'发送短消息', N'1010', N'10103', N'/Main/Sys/SendShortMsg.aspx', 3, N'CCOA', N'new1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/2010.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2010_zhoushengyu', N'zhoushengyu', N'2010', N'发送短消息', N'1010', N'10103', N'/Main/Sys/SendShortMsg.aspx', 3, N'CCOA', N'new1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/2010.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2010_zhoutianjiao', N'zhoutianjiao', N'2010', N'发送短消息', N'1010', N'10103', N'/Main/Sys/SendShortMsg.aspx', 3, N'CCOA', N'new1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/2010.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2011_admin', N'admin', N'2011', N'公告类型', N'1012', N'10204', N'/Comm/Search.aspx?EnsName=BP.OA.NoticeCategorys', 4, N'CCOA', N'data3.png', N'', N'png', N'//DataUser/BP.GPM.Menu/1015.png', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2011_fuhui', N'fuhui', N'2011', N'公告类型', N'1012', N'10204', N'/Comm/Search.aspx?EnsName=BP.OA.NoticeCategorys', 4, N'CCOA', N'data3.png', N'', N'png', N'//DataUser/BP.GPM.Menu/1015.png', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2011_guobaogeng', N'guobaogeng', N'2011', N'公告类型', N'1012', N'10204', N'/Comm/Search.aspx?EnsName=BP.OA.NoticeCategorys', 4, N'CCOA', N'data3.png', N'', N'png', N'//DataUser/BP.GPM.Menu/1015.png', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2011_guoxiangbin', N'guoxiangbin', N'2011', N'公告类型', N'1012', N'10204', N'/Comm/Search.aspx?EnsName=BP.OA.NoticeCategorys', 4, N'CCOA', N'data3.png', N'', N'png', N'//DataUser/BP.GPM.Menu/1015.png', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2011_liping', N'liping', N'2011', N'公告类型', N'1012', N'10204', N'/Comm/Search.aspx?EnsName=BP.OA.NoticeCategorys', 4, N'CCOA', N'data3.png', N'', N'png', N'//DataUser/BP.GPM.Menu/1015.png', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2011_liyan', N'liyan', N'2011', N'公告类型', N'1012', N'10204', N'/Comm/Search.aspx?EnsName=BP.OA.NoticeCategorys', 4, N'CCOA', N'data3.png', N'', N'png', N'//DataUser/BP.GPM.Menu/1015.png', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2011_qifenglin', N'qifenglin', N'2011', N'公告类型', N'1012', N'10204', N'/Comm/Search.aspx?EnsName=BP.OA.NoticeCategorys', 4, N'CCOA', N'data3.png', N'', N'png', N'//DataUser/BP.GPM.Menu/1015.png', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2011_yangyilei', N'yangyilei', N'2011', N'公告类型', N'1012', N'10204', N'/Comm/Search.aspx?EnsName=BP.OA.NoticeCategorys', 4, N'CCOA', N'data3.png', N'', N'png', N'//DataUser/BP.GPM.Menu/1015.png', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2011_zhanghaicheng', N'zhanghaicheng', N'2011', N'公告类型', N'1012', N'10204', N'/Comm/Search.aspx?EnsName=BP.OA.NoticeCategorys', 4, N'CCOA', N'data3.png', N'', N'png', N'//DataUser/BP.GPM.Menu/1015.png', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2011_zhangyifan', N'zhangyifan', N'2011', N'公告类型', N'1012', N'10204', N'/Comm/Search.aspx?EnsName=BP.OA.NoticeCategorys', 4, N'CCOA', N'data3.png', N'', N'png', N'//DataUser/BP.GPM.Menu/1015.png', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2011_zhoupeng', N'zhoupeng', N'2011', N'公告类型', N'1012', N'10204', N'/Comm/Search.aspx?EnsName=BP.OA.NoticeCategorys', 4, N'CCOA', N'data3.png', N'', N'png', N'//DataUser/BP.GPM.Menu/1015.png', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2011_zhoushengyu', N'zhoushengyu', N'2011', N'公告类型', N'1012', N'10204', N'/Comm/Search.aspx?EnsName=BP.OA.NoticeCategorys', 4, N'CCOA', N'data3.png', N'', N'png', N'//DataUser/BP.GPM.Menu/1015.png', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2011_zhoutianjiao', N'zhoutianjiao', N'2011', N'公告类型', N'1012', N'10204', N'/Comm/Search.aspx?EnsName=BP.OA.NoticeCategorys', 4, N'CCOA', N'data3.png', N'', N'png', N'//DataUser/BP.GPM.Menu/1015.png', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2101_admin', N'admin', N'2101', N'办公用品管理', N'2002', N'0105', N'', 3, N'CCOA', N'', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2101_fuhui', N'fuhui', N'2101', N'办公用品管理', N'2002', N'0105', N'', 3, N'CCOA', N'', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2101_guobaogeng', N'guobaogeng', N'2101', N'办公用品管理', N'2002', N'0105', N'', 3, N'CCOA', N'', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2101_guoxiangbin', N'guoxiangbin', N'2101', N'办公用品管理', N'2002', N'0105', N'', 3, N'CCOA', N'', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2101_liping', N'liping', N'2101', N'办公用品管理', N'2002', N'0105', N'', 3, N'CCOA', N'', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2101_liyan', N'liyan', N'2101', N'办公用品管理', N'2002', N'0105', N'', 3, N'CCOA', N'', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2101_qifenglin', N'qifenglin', N'2101', N'办公用品管理', N'2002', N'0105', N'', 3, N'CCOA', N'', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2101_yangyilei', N'yangyilei', N'2101', N'办公用品管理', N'2002', N'0105', N'', 3, N'CCOA', N'', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2101_zhanghaicheng', N'zhanghaicheng', N'2101', N'办公用品管理', N'2002', N'0105', N'', 3, N'CCOA', N'', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2101_zhangyifan', N'zhangyifan', N'2101', N'办公用品管理', N'2002', N'0105', N'', 3, N'CCOA', N'', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2101_zhoupeng', N'zhoupeng', N'2101', N'办公用品管理', N'2002', N'0105', N'', 3, N'CCOA', N'', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2101_zhoushengyu', N'zhoushengyu', N'2101', N'办公用品管理', N'2002', N'0105', N'', 3, N'CCOA', N'', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2101_zhoutianjiao', N'zhoutianjiao', N'2101', N'办公用品管理', N'2002', N'0105', N'', 3, N'CCOA', N'', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2111_admin', N'admin', N'2111', N'类别维护', N'2101', N'10103', N'/Comm/Ens.aspx?EnsName=BP.DS.DSSorts', 3, N'CCOA', N'new1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/2010.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2111_fuhui', N'fuhui', N'2111', N'类别维护', N'2101', N'10103', N'/Comm/Ens.aspx?EnsName=BP.DS.DSSorts', 3, N'CCOA', N'new1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/2010.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2111_guobaogeng', N'guobaogeng', N'2111', N'类别维护', N'2101', N'10103', N'/Comm/Ens.aspx?EnsName=BP.DS.DSSorts', 3, N'CCOA', N'new1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/2010.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2111_guoxiangbin', N'guoxiangbin', N'2111', N'类别维护', N'2101', N'10103', N'/Comm/Ens.aspx?EnsName=BP.DS.DSSorts', 3, N'CCOA', N'new1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/2010.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2111_liping', N'liping', N'2111', N'类别维护', N'2101', N'10103', N'/Comm/Ens.aspx?EnsName=BP.DS.DSSorts', 3, N'CCOA', N'new1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/2010.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2111_liyan', N'liyan', N'2111', N'类别维护', N'2101', N'10103', N'/Comm/Ens.aspx?EnsName=BP.DS.DSSorts', 3, N'CCOA', N'new1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/2010.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2111_qifenglin', N'qifenglin', N'2111', N'类别维护', N'2101', N'10103', N'/Comm/Ens.aspx?EnsName=BP.DS.DSSorts', 3, N'CCOA', N'new1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/2010.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2111_yangyilei', N'yangyilei', N'2111', N'类别维护', N'2101', N'10103', N'/Comm/Ens.aspx?EnsName=BP.DS.DSSorts', 3, N'CCOA', N'new1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/2010.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2111_zhanghaicheng', N'zhanghaicheng', N'2111', N'类别维护', N'2101', N'10103', N'/Comm/Ens.aspx?EnsName=BP.DS.DSSorts', 3, N'CCOA', N'new1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/2010.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2111_zhangyifan', N'zhangyifan', N'2111', N'类别维护', N'2101', N'10103', N'/Comm/Ens.aspx?EnsName=BP.DS.DSSorts', 3, N'CCOA', N'new1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/2010.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2111_zhoupeng', N'zhoupeng', N'2111', N'类别维护', N'2101', N'10103', N'/Comm/Ens.aspx?EnsName=BP.DS.DSSorts', 3, N'CCOA', N'new1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/2010.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2111_zhoushengyu', N'zhoushengyu', N'2111', N'类别维护', N'2101', N'10103', N'/Comm/Ens.aspx?EnsName=BP.DS.DSSorts', 3, N'CCOA', N'new1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/2010.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2111_zhoutianjiao', N'zhoutianjiao', N'2111', N'类别维护', N'2101', N'10103', N'/Comm/Ens.aspx?EnsName=BP.DS.DSSorts', 3, N'CCOA', N'new1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/2010.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2112_admin', N'admin', N'2112', N'库存台帐', N'2101', N'10103', N'/Comm/Search.aspx?EnsName=BP.DS.DSMains', 3, N'CCOA', N'new1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/2010.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2112_fuhui', N'fuhui', N'2112', N'库存台帐', N'2101', N'10103', N'/Comm/Search.aspx?EnsName=BP.DS.DSMains', 3, N'CCOA', N'new1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/2010.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2112_guobaogeng', N'guobaogeng', N'2112', N'库存台帐', N'2101', N'10103', N'/Comm/Search.aspx?EnsName=BP.DS.DSMains', 3, N'CCOA', N'new1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/2010.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2112_guoxiangbin', N'guoxiangbin', N'2112', N'库存台帐', N'2101', N'10103', N'/Comm/Search.aspx?EnsName=BP.DS.DSMains', 3, N'CCOA', N'new1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/2010.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2112_liping', N'liping', N'2112', N'库存台帐', N'2101', N'10103', N'/Comm/Search.aspx?EnsName=BP.DS.DSMains', 3, N'CCOA', N'new1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/2010.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2112_liyan', N'liyan', N'2112', N'库存台帐', N'2101', N'10103', N'/Comm/Search.aspx?EnsName=BP.DS.DSMains', 3, N'CCOA', N'new1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/2010.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2112_qifenglin', N'qifenglin', N'2112', N'库存台帐', N'2101', N'10103', N'/Comm/Search.aspx?EnsName=BP.DS.DSMains', 3, N'CCOA', N'new1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/2010.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2112_yangyilei', N'yangyilei', N'2112', N'库存台帐', N'2101', N'10103', N'/Comm/Search.aspx?EnsName=BP.DS.DSMains', 3, N'CCOA', N'new1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/2010.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2112_zhanghaicheng', N'zhanghaicheng', N'2112', N'库存台帐', N'2101', N'10103', N'/Comm/Search.aspx?EnsName=BP.DS.DSMains', 3, N'CCOA', N'new1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/2010.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2112_zhangyifan', N'zhangyifan', N'2112', N'库存台帐', N'2101', N'10103', N'/Comm/Search.aspx?EnsName=BP.DS.DSMains', 3, N'CCOA', N'new1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/2010.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2112_zhoupeng', N'zhoupeng', N'2112', N'库存台帐', N'2101', N'10103', N'/Comm/Search.aspx?EnsName=BP.DS.DSMains', 3, N'CCOA', N'new1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/2010.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2112_zhoushengyu', N'zhoushengyu', N'2112', N'库存台帐', N'2101', N'10103', N'/Comm/Search.aspx?EnsName=BP.DS.DSMains', 3, N'CCOA', N'new1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/2010.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2112_zhoutianjiao', N'zhoutianjiao', N'2112', N'库存台帐', N'2101', N'10103', N'/Comm/Search.aspx?EnsName=BP.DS.DSMains', 3, N'CCOA', N'new1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/2010.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2113_admin', N'admin', N'2113', N'采购台帐', N'2101', N'10103', N'/Comm/Search.aspx?EnsName=BP.DS.DSBuys', 3, N'CCOA', N'new1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/2010.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2113_fuhui', N'fuhui', N'2113', N'采购台帐', N'2101', N'10103', N'/Comm/Search.aspx?EnsName=BP.DS.DSBuys', 3, N'CCOA', N'new1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/2010.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2113_guobaogeng', N'guobaogeng', N'2113', N'采购台帐', N'2101', N'10103', N'/Comm/Search.aspx?EnsName=BP.DS.DSBuys', 3, N'CCOA', N'new1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/2010.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2113_guoxiangbin', N'guoxiangbin', N'2113', N'采购台帐', N'2101', N'10103', N'/Comm/Search.aspx?EnsName=BP.DS.DSBuys', 3, N'CCOA', N'new1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/2010.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2113_liping', N'liping', N'2113', N'采购台帐', N'2101', N'10103', N'/Comm/Search.aspx?EnsName=BP.DS.DSBuys', 3, N'CCOA', N'new1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/2010.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2113_liyan', N'liyan', N'2113', N'采购台帐', N'2101', N'10103', N'/Comm/Search.aspx?EnsName=BP.DS.DSBuys', 3, N'CCOA', N'new1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/2010.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2113_qifenglin', N'qifenglin', N'2113', N'采购台帐', N'2101', N'10103', N'/Comm/Search.aspx?EnsName=BP.DS.DSBuys', 3, N'CCOA', N'new1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/2010.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2113_yangyilei', N'yangyilei', N'2113', N'采购台帐', N'2101', N'10103', N'/Comm/Search.aspx?EnsName=BP.DS.DSBuys', 3, N'CCOA', N'new1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/2010.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2113_zhanghaicheng', N'zhanghaicheng', N'2113', N'采购台帐', N'2101', N'10103', N'/Comm/Search.aspx?EnsName=BP.DS.DSBuys', 3, N'CCOA', N'new1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/2010.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2113_zhangyifan', N'zhangyifan', N'2113', N'采购台帐', N'2101', N'10103', N'/Comm/Search.aspx?EnsName=BP.DS.DSBuys', 3, N'CCOA', N'new1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/2010.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2113_zhoupeng', N'zhoupeng', N'2113', N'采购台帐', N'2101', N'10103', N'/Comm/Search.aspx?EnsName=BP.DS.DSBuys', 3, N'CCOA', N'new1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/2010.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2113_zhoushengyu', N'zhoushengyu', N'2113', N'采购台帐', N'2101', N'10103', N'/Comm/Search.aspx?EnsName=BP.DS.DSBuys', 3, N'CCOA', N'new1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/2010.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2113_zhoutianjiao', N'zhoutianjiao', N'2113', N'采购台帐', N'2101', N'10103', N'/Comm/Search.aspx?EnsName=BP.DS.DSBuys', 3, N'CCOA', N'new1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/2010.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2114_admin', N'admin', N'2114', N'领用台帐', N'2101', N'10103', N'/Comm/Search.aspx?EnsName=BP.DS.DSTakes', 3, N'CCOA', N'new1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/2010.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2114_fuhui', N'fuhui', N'2114', N'领用台帐', N'2101', N'10103', N'/Comm/Search.aspx?EnsName=BP.DS.DSTakes', 3, N'CCOA', N'new1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/2010.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2114_guobaogeng', N'guobaogeng', N'2114', N'领用台帐', N'2101', N'10103', N'/Comm/Search.aspx?EnsName=BP.DS.DSTakes', 3, N'CCOA', N'new1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/2010.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2114_guoxiangbin', N'guoxiangbin', N'2114', N'领用台帐', N'2101', N'10103', N'/Comm/Search.aspx?EnsName=BP.DS.DSTakes', 3, N'CCOA', N'new1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/2010.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2114_liping', N'liping', N'2114', N'领用台帐', N'2101', N'10103', N'/Comm/Search.aspx?EnsName=BP.DS.DSTakes', 3, N'CCOA', N'new1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/2010.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2114_liyan', N'liyan', N'2114', N'领用台帐', N'2101', N'10103', N'/Comm/Search.aspx?EnsName=BP.DS.DSTakes', 3, N'CCOA', N'new1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/2010.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2114_qifenglin', N'qifenglin', N'2114', N'领用台帐', N'2101', N'10103', N'/Comm/Search.aspx?EnsName=BP.DS.DSTakes', 3, N'CCOA', N'new1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/2010.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2114_yangyilei', N'yangyilei', N'2114', N'领用台帐', N'2101', N'10103', N'/Comm/Search.aspx?EnsName=BP.DS.DSTakes', 3, N'CCOA', N'new1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/2010.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2114_zhanghaicheng', N'zhanghaicheng', N'2114', N'领用台帐', N'2101', N'10103', N'/Comm/Search.aspx?EnsName=BP.DS.DSTakes', 3, N'CCOA', N'new1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/2010.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2114_zhangyifan', N'zhangyifan', N'2114', N'领用台帐', N'2101', N'10103', N'/Comm/Search.aspx?EnsName=BP.DS.DSTakes', 3, N'CCOA', N'new1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/2010.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2114_zhoupeng', N'zhoupeng', N'2114', N'领用台帐', N'2101', N'10103', N'/Comm/Search.aspx?EnsName=BP.DS.DSTakes', 3, N'CCOA', N'new1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/2010.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2114_zhoushengyu', N'zhoushengyu', N'2114', N'领用台帐', N'2101', N'10103', N'/Comm/Search.aspx?EnsName=BP.DS.DSTakes', 3, N'CCOA', N'new1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/2010.gif', 16, 16, 0)
INSERT [dbo].[GPM_EmpMenu] ([MyPK], [FK_Emp], [FK_Menu], [Name], [ParentNo], [TreeNo], [Url], [MenuType], [FK_App], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'2114_zhoutianjiao', N'zhoutianjiao', N'2114', N'领用台帐', N'2101', N'10103', N'/Comm/Search.aspx?EnsName=BP.DS.DSTakes', 3, N'CCOA', N'new1.gif', N'', N'gif', N'//DataUser/BP.GPM.Menu/2010.gif', 16, 16, 0)
/****** Object:  Table [dbo].[GPM_EmpInfoPush]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GPM_EmpInfoPush](
	[MyPK] [nvarchar](100) NOT NULL,
	[FK_Emp] [nvarchar](30) NULL,
	[FK_InfoPush] [nvarchar](30) NULL,
	[Name] [nvarchar](3900) NULL,
	[Url] [nvarchar](3900) NULL,
	[OpenWay] [int] NULL,
	[Idx] [int] NULL,
	[MyFileName] [nvarchar](100) NULL,
	[MyFilePath] [nvarchar](100) NULL,
	[MyFileExt] [nvarchar](10) NULL,
	[WebPath] [nvarchar](200) NULL,
	[MyFileH] [int] NULL,
	[MyFileW] [int] NULL,
	[MyFileSize] [float] NULL,
 CONSTRAINT [GPM_EmpInfoPushpk] PRIMARY KEY CLUSTERED 
(
	[MyPK] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[GPM_EmpApp]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GPM_EmpApp](
	[MyPK] [nvarchar](100) NOT NULL,
	[FK_Emp] [nvarchar](50) NULL,
	[FK_App] [nvarchar](50) NULL,
	[Name] [nvarchar](3900) NULL,
	[Url] [nvarchar](3900) NULL,
	[MyFileName] [nvarchar](100) NULL,
	[MyFilePath] [nvarchar](100) NULL,
	[MyFileExt] [nvarchar](10) NULL,
	[WebPath] [nvarchar](200) NULL,
	[MyFileH] [int] NULL,
	[MyFileW] [int] NULL,
	[MyFileSize] [float] NULL,
 CONSTRAINT [GPM_EmpApppk] PRIMARY KEY CLUSTERED 
(
	[MyPK] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[GPM_EmpApp] ([MyPK], [FK_Emp], [FK_App], [Name], [Url], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'CCIM_admin', N'admin', N'CCIM', N'即时通讯', N'http://localhost/ccflow/WF/Login.aspx', N'admin.gif', N'Path', N'GIF', N'Img/im.png', 0, 0, 0)
INSERT [dbo].[GPM_EmpApp] ([MyPK], [FK_Emp], [FK_App], [Name], [Url], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'CCIM_fuhui', N'fuhui', N'CCIM', N'即时通讯', N'http://localhost/ccflow/WF/Login.aspx', N'admin.gif', N'Path', N'GIF', N'Img/im.png', 0, 0, 0)
INSERT [dbo].[GPM_EmpApp] ([MyPK], [FK_Emp], [FK_App], [Name], [Url], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'CCIM_guobaogeng', N'guobaogeng', N'CCIM', N'即时通讯', N'http://localhost/ccflow/WF/Login.aspx', N'admin.gif', N'Path', N'GIF', N'Img/im.png', 0, 0, 0)
INSERT [dbo].[GPM_EmpApp] ([MyPK], [FK_Emp], [FK_App], [Name], [Url], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'CCIM_guoxiangbin', N'guoxiangbin', N'CCIM', N'即时通讯', N'http://localhost/ccflow/WF/Login.aspx', N'admin.gif', N'Path', N'GIF', N'Img/im.png', 0, 0, 0)
INSERT [dbo].[GPM_EmpApp] ([MyPK], [FK_Emp], [FK_App], [Name], [Url], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'CCIM_liping', N'liping', N'CCIM', N'即时通讯', N'http://localhost/ccflow/WF/Login.aspx', N'admin.gif', N'Path', N'GIF', N'Img/im.png', 0, 0, 0)
INSERT [dbo].[GPM_EmpApp] ([MyPK], [FK_Emp], [FK_App], [Name], [Url], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'CCIM_liyan', N'liyan', N'CCIM', N'即时通讯', N'http://localhost/ccflow/WF/Login.aspx', N'admin.gif', N'Path', N'GIF', N'Img/im.png', 0, 0, 0)
INSERT [dbo].[GPM_EmpApp] ([MyPK], [FK_Emp], [FK_App], [Name], [Url], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'CCIM_qifenglin', N'qifenglin', N'CCIM', N'即时通讯', N'http://localhost/ccflow/WF/Login.aspx', N'admin.gif', N'Path', N'GIF', N'Img/im.png', 0, 0, 0)
INSERT [dbo].[GPM_EmpApp] ([MyPK], [FK_Emp], [FK_App], [Name], [Url], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'CCIM_yangyilei', N'yangyilei', N'CCIM', N'即时通讯', N'http://localhost/ccflow/WF/Login.aspx', N'admin.gif', N'Path', N'GIF', N'Img/im.png', 0, 0, 0)
INSERT [dbo].[GPM_EmpApp] ([MyPK], [FK_Emp], [FK_App], [Name], [Url], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'CCIM_zhanghaicheng', N'zhanghaicheng', N'CCIM', N'即时通讯', N'http://localhost/ccflow/WF/Login.aspx', N'admin.gif', N'Path', N'GIF', N'Img/im.png', 0, 0, 0)
INSERT [dbo].[GPM_EmpApp] ([MyPK], [FK_Emp], [FK_App], [Name], [Url], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'CCIM_zhangyifan', N'zhangyifan', N'CCIM', N'即时通讯', N'http://localhost/ccflow/WF/Login.aspx', N'admin.gif', N'Path', N'GIF', N'Img/im.png', 0, 0, 0)
INSERT [dbo].[GPM_EmpApp] ([MyPK], [FK_Emp], [FK_App], [Name], [Url], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'CCIM_zhoupeng', N'zhoupeng', N'CCIM', N'即时通讯', N'http://localhost/ccflow/WF/Login.aspx', N'admin.gif', N'Path', N'GIF', N'Img/im.png', 0, 0, 0)
INSERT [dbo].[GPM_EmpApp] ([MyPK], [FK_Emp], [FK_App], [Name], [Url], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'CCIM_zhoushengyu', N'zhoushengyu', N'CCIM', N'即时通讯', N'http://localhost/ccflow/WF/Login.aspx', N'admin.gif', N'Path', N'GIF', N'Img/im.png', 0, 0, 0)
INSERT [dbo].[GPM_EmpApp] ([MyPK], [FK_Emp], [FK_App], [Name], [Url], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'CCIM_zhoutianjiao', N'zhoutianjiao', N'CCIM', N'即时通讯', N'http://localhost/ccflow/WF/Login.aspx', N'admin.gif', N'Path', N'GIF', N'Img/im.png', 0, 0, 0)
INSERT [dbo].[GPM_EmpApp] ([MyPK], [FK_Emp], [FK_App], [Name], [Url], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'CCOA_admin', N'admin', N'CCOA', N'驰骋OA', N'http://www.chichengoa.org/app/login/login.ashx', N'ccoa.png', N'Path', N'GIF', N'/DataUser/BP.GPM.STem/CCOA.png', 0, 0, 0)
INSERT [dbo].[GPM_EmpApp] ([MyPK], [FK_Emp], [FK_App], [Name], [Url], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'CCOA_fuhui', N'fuhui', N'CCOA', N'驰骋OA', N'http://www.chichengoa.org/app/login/login.ashx', N'ccoa.png', N'Path', N'GIF', N'/DataUser/BP.GPM.STem/CCOA.png', 0, 0, 0)
INSERT [dbo].[GPM_EmpApp] ([MyPK], [FK_Emp], [FK_App], [Name], [Url], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'CCOA_guobaogeng', N'guobaogeng', N'CCOA', N'驰骋OA', N'http://www.chichengoa.org/app/login/login.ashx', N'ccoa.png', N'Path', N'GIF', N'/DataUser/BP.GPM.STem/CCOA.png', 0, 0, 0)
INSERT [dbo].[GPM_EmpApp] ([MyPK], [FK_Emp], [FK_App], [Name], [Url], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'CCOA_guoxiangbin', N'guoxiangbin', N'CCOA', N'驰骋OA', N'http://www.chichengoa.org/app/login/login.ashx', N'ccoa.png', N'Path', N'GIF', N'/DataUser/BP.GPM.STem/CCOA.png', 0, 0, 0)
INSERT [dbo].[GPM_EmpApp] ([MyPK], [FK_Emp], [FK_App], [Name], [Url], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'CCOA_liping', N'liping', N'CCOA', N'驰骋OA', N'http://www.chichengoa.org/app/login/login.ashx', N'ccoa.png', N'Path', N'GIF', N'/DataUser/BP.GPM.STem/CCOA.png', 0, 0, 0)
INSERT [dbo].[GPM_EmpApp] ([MyPK], [FK_Emp], [FK_App], [Name], [Url], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'CCOA_liyan', N'liyan', N'CCOA', N'驰骋OA', N'http://www.chichengoa.org/app/login/login.ashx', N'ccoa.png', N'Path', N'GIF', N'/DataUser/BP.GPM.STem/CCOA.png', 0, 0, 0)
INSERT [dbo].[GPM_EmpApp] ([MyPK], [FK_Emp], [FK_App], [Name], [Url], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'CCOA_qifenglin', N'qifenglin', N'CCOA', N'驰骋OA', N'http://www.chichengoa.org/app/login/login.ashx', N'ccoa.png', N'Path', N'GIF', N'/DataUser/BP.GPM.STem/CCOA.png', 0, 0, 0)
INSERT [dbo].[GPM_EmpApp] ([MyPK], [FK_Emp], [FK_App], [Name], [Url], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'CCOA_yangyilei', N'yangyilei', N'CCOA', N'驰骋OA', N'http://www.chichengoa.org/app/login/login.ashx', N'ccoa.png', N'Path', N'GIF', N'/DataUser/BP.GPM.STem/CCOA.png', 0, 0, 0)
INSERT [dbo].[GPM_EmpApp] ([MyPK], [FK_Emp], [FK_App], [Name], [Url], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'CCOA_zhanghaicheng', N'zhanghaicheng', N'CCOA', N'驰骋OA', N'http://www.chichengoa.org/app/login/login.ashx', N'ccoa.png', N'Path', N'GIF', N'/DataUser/BP.GPM.STem/CCOA.png', 0, 0, 0)
INSERT [dbo].[GPM_EmpApp] ([MyPK], [FK_Emp], [FK_App], [Name], [Url], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'CCOA_zhangyifan', N'zhangyifan', N'CCOA', N'驰骋OA', N'http://www.chichengoa.org/app/login/login.ashx', N'ccoa.png', N'Path', N'GIF', N'/DataUser/BP.GPM.STem/CCOA.png', 0, 0, 0)
INSERT [dbo].[GPM_EmpApp] ([MyPK], [FK_Emp], [FK_App], [Name], [Url], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'CCOA_zhoupeng', N'zhoupeng', N'CCOA', N'驰骋OA', N'http://www.chichengoa.org/app/login/login.ashx', N'ccoa.png', N'Path', N'GIF', N'/DataUser/BP.GPM.STem/CCOA.png', 0, 0, 0)
INSERT [dbo].[GPM_EmpApp] ([MyPK], [FK_Emp], [FK_App], [Name], [Url], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'CCOA_zhoushengyu', N'zhoushengyu', N'CCOA', N'驰骋OA', N'http://www.chichengoa.org/app/login/login.ashx', N'ccoa.png', N'Path', N'GIF', N'/DataUser/BP.GPM.STem/CCOA.png', 0, 0, 0)
INSERT [dbo].[GPM_EmpApp] ([MyPK], [FK_Emp], [FK_App], [Name], [Url], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'CCOA_zhoutianjiao', N'zhoutianjiao', N'CCOA', N'驰骋OA', N'http://www.chichengoa.org/app/login/login.ashx', N'ccoa.png', N'Path', N'GIF', N'/DataUser/BP.GPM.STem/CCOA.png', 0, 0, 0)
INSERT [dbo].[GPM_EmpApp] ([MyPK], [FK_Emp], [FK_App], [Name], [Url], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'GGXXW_admin', N'admin', N'GGXXW', N'公共信息网', N'http://localhost:8083/', N'db.gif', N'Path', N'GIF', N'Img/common.png', 0, 0, 0)
INSERT [dbo].[GPM_EmpApp] ([MyPK], [FK_Emp], [FK_App], [Name], [Url], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'GGXXW_fuhui', N'fuhui', N'GGXXW', N'公共信息网', N'http://localhost:8083/', N'db.gif', N'Path', N'GIF', N'Img/common.png', 0, 0, 0)
INSERT [dbo].[GPM_EmpApp] ([MyPK], [FK_Emp], [FK_App], [Name], [Url], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'GGXXW_guobaogeng', N'guobaogeng', N'GGXXW', N'公共信息网', N'http://localhost:8083/', N'db.gif', N'Path', N'GIF', N'Img/common.png', 0, 0, 0)
INSERT [dbo].[GPM_EmpApp] ([MyPK], [FK_Emp], [FK_App], [Name], [Url], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'GGXXW_guoxiangbin', N'guoxiangbin', N'GGXXW', N'公共信息网', N'http://localhost:8083/', N'db.gif', N'Path', N'GIF', N'Img/common.png', 0, 0, 0)
INSERT [dbo].[GPM_EmpApp] ([MyPK], [FK_Emp], [FK_App], [Name], [Url], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'GGXXW_liping', N'liping', N'GGXXW', N'公共信息网', N'http://localhost:8083/', N'db.gif', N'Path', N'GIF', N'Img/common.png', 0, 0, 0)
INSERT [dbo].[GPM_EmpApp] ([MyPK], [FK_Emp], [FK_App], [Name], [Url], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'GGXXW_liyan', N'liyan', N'GGXXW', N'公共信息网', N'http://localhost:8083/', N'db.gif', N'Path', N'GIF', N'Img/common.png', 0, 0, 0)
INSERT [dbo].[GPM_EmpApp] ([MyPK], [FK_Emp], [FK_App], [Name], [Url], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'GGXXW_qifenglin', N'qifenglin', N'GGXXW', N'公共信息网', N'http://localhost:8083/', N'db.gif', N'Path', N'GIF', N'Img/common.png', 0, 0, 0)
INSERT [dbo].[GPM_EmpApp] ([MyPK], [FK_Emp], [FK_App], [Name], [Url], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'GGXXW_yangyilei', N'yangyilei', N'GGXXW', N'公共信息网', N'http://localhost:8083/', N'db.gif', N'Path', N'GIF', N'Img/common.png', 0, 0, 0)
INSERT [dbo].[GPM_EmpApp] ([MyPK], [FK_Emp], [FK_App], [Name], [Url], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'GGXXW_zhanghaicheng', N'zhanghaicheng', N'GGXXW', N'公共信息网', N'http://localhost:8083/', N'db.gif', N'Path', N'GIF', N'Img/common.png', 0, 0, 0)
INSERT [dbo].[GPM_EmpApp] ([MyPK], [FK_Emp], [FK_App], [Name], [Url], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'GGXXW_zhangyifan', N'zhangyifan', N'GGXXW', N'公共信息网', N'http://localhost:8083/', N'db.gif', N'Path', N'GIF', N'Img/common.png', 0, 0, 0)
INSERT [dbo].[GPM_EmpApp] ([MyPK], [FK_Emp], [FK_App], [Name], [Url], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'GGXXW_zhoupeng', N'zhoupeng', N'GGXXW', N'公共信息网', N'http://localhost:8083/', N'db.gif', N'Path', N'GIF', N'Img/common.png', 0, 0, 0)
INSERT [dbo].[GPM_EmpApp] ([MyPK], [FK_Emp], [FK_App], [Name], [Url], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'GGXXW_zhoushengyu', N'zhoushengyu', N'GGXXW', N'公共信息网', N'http://localhost:8083/', N'db.gif', N'Path', N'GIF', N'Img/common.png', 0, 0, 0)
INSERT [dbo].[GPM_EmpApp] ([MyPK], [FK_Emp], [FK_App], [Name], [Url], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'GGXXW_zhoutianjiao', N'zhoutianjiao', N'GGXXW', N'公共信息网', N'http://localhost:8083/', N'db.gif', N'Path', N'GIF', N'Img/common.png', 0, 0, 0)
INSERT [dbo].[GPM_EmpApp] ([MyPK], [FK_Emp], [FK_App], [Name], [Url], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'GPM_admin', N'admin', N'GPM', N'权限管理', N'/GPM/Default.aspx', N'MyWork.gif', N'Path', N'GIF', N'Img/auth.png', 0, 0, 0)
INSERT [dbo].[GPM_EmpApp] ([MyPK], [FK_Emp], [FK_App], [Name], [Url], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'GPM_fuhui', N'fuhui', N'GPM', N'权限管理', N'/GPM/Default.aspx', N'MyWork.gif', N'Path', N'GIF', N'Img/auth.png', 0, 0, 0)
INSERT [dbo].[GPM_EmpApp] ([MyPK], [FK_Emp], [FK_App], [Name], [Url], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'GPM_guobaogeng', N'guobaogeng', N'GPM', N'权限管理', N'/GPM/Default.aspx', N'MyWork.gif', N'Path', N'GIF', N'Img/auth.png', 0, 0, 0)
INSERT [dbo].[GPM_EmpApp] ([MyPK], [FK_Emp], [FK_App], [Name], [Url], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'GPM_guoxiangbin', N'guoxiangbin', N'GPM', N'权限管理', N'/GPM/Default.aspx', N'MyWork.gif', N'Path', N'GIF', N'Img/auth.png', 0, 0, 0)
INSERT [dbo].[GPM_EmpApp] ([MyPK], [FK_Emp], [FK_App], [Name], [Url], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'GPM_liping', N'liping', N'GPM', N'权限管理', N'/GPM/Default.aspx', N'MyWork.gif', N'Path', N'GIF', N'Img/auth.png', 0, 0, 0)
INSERT [dbo].[GPM_EmpApp] ([MyPK], [FK_Emp], [FK_App], [Name], [Url], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'GPM_liyan', N'liyan', N'GPM', N'权限管理', N'/GPM/Default.aspx', N'MyWork.gif', N'Path', N'GIF', N'Img/auth.png', 0, 0, 0)
INSERT [dbo].[GPM_EmpApp] ([MyPK], [FK_Emp], [FK_App], [Name], [Url], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'GPM_qifenglin', N'qifenglin', N'GPM', N'权限管理', N'/GPM/Default.aspx', N'MyWork.gif', N'Path', N'GIF', N'Img/auth.png', 0, 0, 0)
INSERT [dbo].[GPM_EmpApp] ([MyPK], [FK_Emp], [FK_App], [Name], [Url], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'GPM_yangyilei', N'yangyilei', N'GPM', N'权限管理', N'/GPM/Default.aspx', N'MyWork.gif', N'Path', N'GIF', N'Img/auth.png', 0, 0, 0)
INSERT [dbo].[GPM_EmpApp] ([MyPK], [FK_Emp], [FK_App], [Name], [Url], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'GPM_zhanghaicheng', N'zhanghaicheng', N'GPM', N'权限管理', N'/GPM/Default.aspx', N'MyWork.gif', N'Path', N'GIF', N'Img/auth.png', 0, 0, 0)
INSERT [dbo].[GPM_EmpApp] ([MyPK], [FK_Emp], [FK_App], [Name], [Url], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'GPM_zhangyifan', N'zhangyifan', N'GPM', N'权限管理', N'/GPM/Default.aspx', N'MyWork.gif', N'Path', N'GIF', N'Img/auth.png', 0, 0, 0)
INSERT [dbo].[GPM_EmpApp] ([MyPK], [FK_Emp], [FK_App], [Name], [Url], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'GPM_zhoupeng', N'zhoupeng', N'GPM', N'权限管理', N'/GPM/Default.aspx', N'MyWork.gif', N'Path', N'GIF', N'Img/auth.png', 0, 0, 0)
INSERT [dbo].[GPM_EmpApp] ([MyPK], [FK_Emp], [FK_App], [Name], [Url], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'GPM_zhoushengyu', N'zhoushengyu', N'GPM', N'权限管理', N'/GPM/Default.aspx', N'MyWork.gif', N'Path', N'GIF', N'Img/auth.png', 0, 0, 0)
INSERT [dbo].[GPM_EmpApp] ([MyPK], [FK_Emp], [FK_App], [Name], [Url], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'GPM_zhoutianjiao', N'zhoutianjiao', N'GPM', N'权限管理', N'/GPM/Default.aspx', N'MyWork.gif', N'Path', N'GIF', N'Img/auth.png', 0, 0, 0)
INSERT [dbo].[GPM_EmpApp] ([MyPK], [FK_Emp], [FK_App], [Name], [Url], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'SSO_admin', N'admin', N'SSO', N'单点登陆', N'/SSO/Default.aspx', N'RptDir.gif', N'Path', N'GIF', N'Img/sso.png', 0, 0, 0)
INSERT [dbo].[GPM_EmpApp] ([MyPK], [FK_Emp], [FK_App], [Name], [Url], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'SSO_fuhui', N'fuhui', N'SSO', N'单点登陆', N'/SSO/Default.aspx', N'RptDir.gif', N'Path', N'GIF', N'Img/sso.png', 0, 0, 0)
INSERT [dbo].[GPM_EmpApp] ([MyPK], [FK_Emp], [FK_App], [Name], [Url], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'SSO_guobaogeng', N'guobaogeng', N'SSO', N'单点登陆', N'/SSO/Default.aspx', N'RptDir.gif', N'Path', N'GIF', N'Img/sso.png', 0, 0, 0)
INSERT [dbo].[GPM_EmpApp] ([MyPK], [FK_Emp], [FK_App], [Name], [Url], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'SSO_guoxiangbin', N'guoxiangbin', N'SSO', N'单点登陆', N'/SSO/Default.aspx', N'RptDir.gif', N'Path', N'GIF', N'Img/sso.png', 0, 0, 0)
INSERT [dbo].[GPM_EmpApp] ([MyPK], [FK_Emp], [FK_App], [Name], [Url], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'SSO_liping', N'liping', N'SSO', N'单点登陆', N'/SSO/Default.aspx', N'RptDir.gif', N'Path', N'GIF', N'Img/sso.png', 0, 0, 0)
INSERT [dbo].[GPM_EmpApp] ([MyPK], [FK_Emp], [FK_App], [Name], [Url], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'SSO_liyan', N'liyan', N'SSO', N'单点登陆', N'/SSO/Default.aspx', N'RptDir.gif', N'Path', N'GIF', N'Img/sso.png', 0, 0, 0)
INSERT [dbo].[GPM_EmpApp] ([MyPK], [FK_Emp], [FK_App], [Name], [Url], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'SSO_qifenglin', N'qifenglin', N'SSO', N'单点登陆', N'/SSO/Default.aspx', N'RptDir.gif', N'Path', N'GIF', N'Img/sso.png', 0, 0, 0)
INSERT [dbo].[GPM_EmpApp] ([MyPK], [FK_Emp], [FK_App], [Name], [Url], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'SSO_yangyilei', N'yangyilei', N'SSO', N'单点登陆', N'/SSO/Default.aspx', N'RptDir.gif', N'Path', N'GIF', N'Img/sso.png', 0, 0, 0)
INSERT [dbo].[GPM_EmpApp] ([MyPK], [FK_Emp], [FK_App], [Name], [Url], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'SSO_zhanghaicheng', N'zhanghaicheng', N'SSO', N'单点登陆', N'/SSO/Default.aspx', N'RptDir.gif', N'Path', N'GIF', N'Img/sso.png', 0, 0, 0)
INSERT [dbo].[GPM_EmpApp] ([MyPK], [FK_Emp], [FK_App], [Name], [Url], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'SSO_zhangyifan', N'zhangyifan', N'SSO', N'单点登陆', N'/SSO/Default.aspx', N'RptDir.gif', N'Path', N'GIF', N'Img/sso.png', 0, 0, 0)
INSERT [dbo].[GPM_EmpApp] ([MyPK], [FK_Emp], [FK_App], [Name], [Url], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'SSO_zhoupeng', N'zhoupeng', N'SSO', N'单点登陆', N'/SSO/Default.aspx', N'RptDir.gif', N'Path', N'GIF', N'Img/sso.png', 0, 0, 0)
INSERT [dbo].[GPM_EmpApp] ([MyPK], [FK_Emp], [FK_App], [Name], [Url], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'SSO_zhoushengyu', N'zhoushengyu', N'SSO', N'单点登陆', N'/SSO/Default.aspx', N'RptDir.gif', N'Path', N'GIF', N'Img/sso.png', 0, 0, 0)
INSERT [dbo].[GPM_EmpApp] ([MyPK], [FK_Emp], [FK_App], [Name], [Url], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'SSO_zhoutianjiao', N'zhoutianjiao', N'SSO', N'单点登陆', N'/SSO/Default.aspx', N'RptDir.gif', N'Path', N'GIF', N'Img/sso.png', 0, 0, 0)
/****** Object:  Table [dbo].[GPM_ByStation]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GPM_ByStation](
	[RefObj] [nvarchar](15) NOT NULL,
	[FK_Station] [nvarchar](100) NOT NULL,
 CONSTRAINT [GPM_ByStationpk] PRIMARY KEY CLUSTERED 
(
	[RefObj] ASC,
	[FK_Station] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[GPM_ByEmp]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GPM_ByEmp](
	[RefObj] [nvarchar](15) NOT NULL,
	[FK_Emp] [nvarchar](100) NOT NULL,
 CONSTRAINT [GPM_ByEmppk] PRIMARY KEY CLUSTERED 
(
	[RefObj] ASC,
	[FK_Emp] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[GPM_ByDept]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GPM_ByDept](
	[RefObj] [nvarchar](15) NOT NULL,
	[FK_Dept] [nvarchar](100) NOT NULL,
 CONSTRAINT [GPM_ByDeptpk] PRIMARY KEY CLUSTERED 
(
	[RefObj] ASC,
	[FK_Dept] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[GPM_BarEmp]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GPM_BarEmp](
	[MyPK] [nvarchar](100) NOT NULL,
	[FK_Bar] [nvarchar](90) NULL,
	[FK_Emp] [nvarchar](90) NULL,
	[IsShow] [int] NULL,
	[Idx] [int] NULL,
 CONSTRAINT [GPM_BarEmppk] PRIMARY KEY CLUSTERED 
(
	[MyPK] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[GPM_Bar]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GPM_Bar](
	[No] [nvarchar](2) NOT NULL,
	[Name] [nvarchar](3900) NULL,
	[Title] [nvarchar](3900) NULL,
	[BarType] [int] NULL,
	[Tag1] [nvarchar](3900) NULL,
	[Tag2] [nvarchar](3900) NULL,
	[Tag3] [nvarchar](3900) NULL,
	[IsDel] [int] NULL,
	[OpenWay] [int] NULL,
	[Idx] [int] NULL,
	[MoreLab] [nvarchar](900) NULL,
	[MoreUrl] [nvarchar](3900) NULL,
	[Doc] [nvarchar](3900) NULL,
	[DocGenerRDT] [nvarchar](50) NULL,
	[Width] [int] NULL,
	[Height] [int] NULL,
 CONSTRAINT [GPM_Barpk] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[GPM_Bar] ([No], [Name], [Title], [BarType], [Tag1], [Tag2], [Tag3], [IsDel], [OpenWay], [Idx], [MoreLab], [MoreUrl], [Doc], [DocGenerRDT], [Width], [Height]) VALUES (N'01', N'信息块测试', N'信息块测试1', NULL, N'SELECT No,Name FROM PORT_DEPT', N'http://ccflow.org', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[GPM_Bar] ([No], [Name], [Title], [BarType], [Tag1], [Tag2], [Tag3], [IsDel], [OpenWay], [Idx], [MoreLab], [MoreUrl], [Doc], [DocGenerRDT], [Width], [Height]) VALUES (N'02', N'信息块测试', N'信息块测试2', NULL, N'SELECT No,Name FROM PORT_DEPT', N'http://ccflow.org', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[GPM_Bar] ([No], [Name], [Title], [BarType], [Tag1], [Tag2], [Tag3], [IsDel], [OpenWay], [Idx], [MoreLab], [MoreUrl], [Doc], [DocGenerRDT], [Width], [Height]) VALUES (N'03', N'信息块测试', N'信息块测试3', NULL, N'SELECT No,Name FROM PORT_DEPT', N'http://ccflow.org', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[GPM_Bar] ([No], [Name], [Title], [BarType], [Tag1], [Tag2], [Tag3], [IsDel], [OpenWay], [Idx], [MoreLab], [MoreUrl], [Doc], [DocGenerRDT], [Width], [Height]) VALUES (N'04', N'信息块测试', N'信息块测试4', NULL, N'SELECT No,Name FROM PORT_DEPT', N'http://ccflow.org', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[GPM_Bar] ([No], [Name], [Title], [BarType], [Tag1], [Tag2], [Tag3], [IsDel], [OpenWay], [Idx], [MoreLab], [MoreUrl], [Doc], [DocGenerRDT], [Width], [Height]) VALUES (N'05', N'信息块测试', N'信息块测试5', NULL, N'SELECT No,Name FROM PORT_DEPT', N'http://ccflow.org', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[GPM_Bar] ([No], [Name], [Title], [BarType], [Tag1], [Tag2], [Tag3], [IsDel], [OpenWay], [Idx], [MoreLab], [MoreUrl], [Doc], [DocGenerRDT], [Width], [Height]) VALUES (N'06', N'信息块测试', N'信息块测试6', NULL, N'SELECT No,Name FROM PORT_DEPT', N'http://ccflow.org', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[GPM_Bar] ([No], [Name], [Title], [BarType], [Tag1], [Tag2], [Tag3], [IsDel], [OpenWay], [Idx], [MoreLab], [MoreUrl], [Doc], [DocGenerRDT], [Width], [Height]) VALUES (N'07', N'信息块测试', N'信息块测试7', NULL, N'SELECT No,Name FROM PORT_DEPT', N'http://ccflow.org', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[GPM_Bar] ([No], [Name], [Title], [BarType], [Tag1], [Tag2], [Tag3], [IsDel], [OpenWay], [Idx], [MoreLab], [MoreUrl], [Doc], [DocGenerRDT], [Width], [Height]) VALUES (N'08', N'信息块测试', N'信息块测试8', NULL, N'SELECT No,Name FROM PORT_DEPT', N'http://ccflow.org', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[GPM_Bar] ([No], [Name], [Title], [BarType], [Tag1], [Tag2], [Tag3], [IsDel], [OpenWay], [Idx], [MoreLab], [MoreUrl], [Doc], [DocGenerRDT], [Width], [Height]) VALUES (N'09', N'信息块测试', N'信息块测试9', NULL, N'SELECT No,Name FROM PORT_DEPT', N'http://ccflow.org', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[GPM_Bar] ([No], [Name], [Title], [BarType], [Tag1], [Tag2], [Tag3], [IsDel], [OpenWay], [Idx], [MoreLab], [MoreUrl], [Doc], [DocGenerRDT], [Width], [Height]) VALUES (N'10', N'信息块测试', N'信息块测试10', NULL, N'SELECT No,Name FROM PORT_DEPT', N'http://ccflow.org', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[GPM_Bar] ([No], [Name], [Title], [BarType], [Tag1], [Tag2], [Tag3], [IsDel], [OpenWay], [Idx], [MoreLab], [MoreUrl], [Doc], [DocGenerRDT], [Width], [Height]) VALUES (N'11', N'信息块测试', N'信息块测试11', NULL, N'SELECT No,Name FROM PORT_DEPT', N'http://ccflow.org', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
/****** Object:  Table [dbo].[GPM_AppSort]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GPM_AppSort](
	[No] [nvarchar](2) NOT NULL,
	[Name] [nvarchar](300) NULL,
	[Idx] [int] NULL,
	[RefMenuNo] [nvarchar](300) NULL,
 CONSTRAINT [GPM_AppSortpk] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[GPM_AppSort] ([No], [Name], [Idx], [RefMenuNo]) VALUES (N'01', N'业务系统', NULL, N'2000')
INSERT [dbo].[GPM_AppSort] ([No], [Name], [Idx], [RefMenuNo]) VALUES (N'02', N'办公系统', NULL, N'2001')
/****** Object:  Table [dbo].[GPM_App]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GPM_App](
	[No] [nvarchar](30) NOT NULL,
	[Name] [nvarchar](3900) NULL,
	[AppModel] [int] NULL,
	[FK_AppSort] [nvarchar](100) NULL,
	[Url] [nvarchar](3900) NULL,
	[SubUrl] [nvarchar](3900) NULL,
	[UidControl] [nvarchar](100) NULL,
	[PwdControl] [nvarchar](100) NULL,
	[ActionType] [int] NULL,
	[SSOType] [int] NULL,
	[OpenWay] [int] NULL,
	[Idx] [int] NULL,
	[IsEnable] [int] NULL,
	[RefMenuNo] [nvarchar](3900) NULL,
	[AppRemark] [nvarchar](500) NULL,
	[MyFileName] [nvarchar](100) NULL,
	[MyFilePath] [nvarchar](100) NULL,
	[MyFileExt] [nvarchar](10) NULL,
	[WebPath] [nvarchar](200) NULL,
	[MyFileH] [int] NULL,
	[MyFileW] [int] NULL,
	[MyFileSize] [float] NULL,
 CONSTRAINT [GPM_Apppk] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[GPM_App] ([No], [Name], [AppModel], [FK_AppSort], [Url], [SubUrl], [UidControl], [PwdControl], [ActionType], [SSOType], [OpenWay], [Idx], [IsEnable], [RefMenuNo], [AppRemark], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'CCFlowBPM', N'BPM系统', 0, N'01', N'', N'', N'', N'', 0, 0, 0, 0, 1, N'100', N'', N'', N'', N'', N'', 0, 0, 0)
INSERT [dbo].[GPM_App] ([No], [Name], [AppModel], [FK_AppSort], [Url], [SubUrl], [UidControl], [PwdControl], [ActionType], [SSOType], [OpenWay], [Idx], [IsEnable], [RefMenuNo], [AppRemark], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'CCIM', N'即时通讯', 1, N'01', N'http://localhost/ccflow/WF/Login.aspx', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'2005', NULL, N'admin.gif', N'Path', N'GIF', N'Img/im.png', NULL, NULL, NULL)
INSERT [dbo].[GPM_App] ([No], [Name], [AppModel], [FK_AppSort], [Url], [SubUrl], [UidControl], [PwdControl], [ActionType], [SSOType], [OpenWay], [Idx], [IsEnable], [RefMenuNo], [AppRemark], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'CCOA', N'驰骋OA', 0, N'01', N'http://www.chichengoa.org/app/login/login.ashx', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'2002', NULL, N'ccoa.png', N'Path', N'GIF', N'/DataUser/BP.GPM.STem/CCOA.png', NULL, NULL, NULL)
INSERT [dbo].[GPM_App] ([No], [Name], [AppModel], [FK_AppSort], [Url], [SubUrl], [UidControl], [PwdControl], [ActionType], [SSOType], [OpenWay], [Idx], [IsEnable], [RefMenuNo], [AppRemark], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'GGXXW', N'公共信息网', 1, N'02', N'http://localhost:8083/', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'2006', NULL, N'db.gif', N'Path', N'GIF', N'Img/common.png', NULL, NULL, NULL)
INSERT [dbo].[GPM_App] ([No], [Name], [AppModel], [FK_AppSort], [Url], [SubUrl], [UidControl], [PwdControl], [ActionType], [SSOType], [OpenWay], [Idx], [IsEnable], [RefMenuNo], [AppRemark], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'GPM', N'权限管理', 1, N'02', N'/GPM/Default.aspx', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'2007', NULL, N'MyWork.gif', N'Path', N'GIF', N'Img/auth.png', NULL, NULL, NULL)
INSERT [dbo].[GPM_App] ([No], [Name], [AppModel], [FK_AppSort], [Url], [SubUrl], [UidControl], [PwdControl], [ActionType], [SSOType], [OpenWay], [Idx], [IsEnable], [RefMenuNo], [AppRemark], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (N'SSO', N'单点登陆', 1, N'02', N'/SSO/Default.aspx', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'2008', NULL, N'RptDir.gif', N'Path', N'GIF', N'Img/sso.png', NULL, NULL, NULL)
/****** Object:  Table [dbo].[BM_ZhiDu]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BM_ZhiDu](
	[OID] [int] NOT NULL,
	[FK_ND] [nvarchar](100) NULL,
	[FK_Sort] [nvarchar](100) NULL,
	[BH] [nvarchar](50) NULL,
	[Name] [nvarchar](50) NULL,
	[DTRel] [nvarchar](50) NULL,
	[ZFDW] [nvarchar](50) NULL,
	[BZ] [nvarchar](4000) NULL,
	[MyFileName] [nvarchar](100) NULL,
	[MyFilePath] [nvarchar](100) NULL,
	[MyFileExt] [nvarchar](10) NULL,
	[WebPath] [nvarchar](200) NULL,
	[MyFileH] [int] NULL,
	[MyFileW] [int] NULL,
	[MyFileSize] [float] NULL,
 CONSTRAINT [BM_ZhiDupk] PRIMARY KEY CLUSTERED 
(
	[OID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[BM_ZhiDu] ([OID], [FK_ND], [FK_Sort], [BH], [Name], [DTRel], [ZFDW], [BZ], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (193, N'001', N'01', N'阿萨德发射的', N'的所发生的', N'2015-07-21', N'阿萨德发射的', N'阿达啊苏打水所得税撒旦撒', N'产品需求思路整理V1.ppt', N'D:\ccflow\value-added\CCPortal\CCOA\CCOA\DataUser\BP.BM.ZhiDu\', N'ppt', N'//DataUser/BP.BM.ZhiDu/193.ppt', 0, 0, 1)
INSERT [dbo].[BM_ZhiDu] ([OID], [FK_ND], [FK_Sort], [BH], [Name], [DTRel], [ZFDW], [BZ], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (195, N'001', N'01', N'阿斯达斯的', N'阿大使', N'2015-07-21', N'', N'啊大大声实打实的实打实的', N'数据结构图.vsd', N'D:\ccflow\value-added\CCPortal\CCOA\CCOA\DataUser\BP.BM.ZhiDu\', N'vsd', N'//DataUser/BP.BM.ZhiDu/195.vsd', 0, 0, 0)
INSERT [dbo].[BM_ZhiDu] ([OID], [FK_ND], [FK_Sort], [BH], [Name], [DTRel], [ZFDW], [BZ], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (197, N'001', N'01', N' 撒旦发射的', N'的 啊士大夫', N'2015-07-08', N'啊苏打水分', N'阿德法撒旦', N'产品需求思路整理V1.ppt', N'D:\ccflow\value-added\CCPortal\CCOA\CCOA\DataUser\BP.BM.ZhiDu\', N'ppt', N'//DataUser/BP.BM.ZhiDu/197.ppt', 0, 0, 1)
/****** Object:  Table [dbo].[BM_XCBDDtl]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BM_XCBDDtl](
	[OID] [int] NOT NULL,
	[FK_Emp] [nvarchar](500) NULL,
	[Title] [nvarchar](500) NULL,
	[Target] [nvarchar](50) NULL,
	[Url] [nvarchar](500) NULL,
	[FK_Dept] [nvarchar](100) NULL,
	[XXSort] [int] NULL,
	[MJ] [int] NULL,
	[DTApp] [nvarchar](50) NULL,
	[DTRel] [nvarchar](50) NULL,
	[ZZDW] [nvarchar](500) NULL,
	[BillNo] [nvarchar](500) NULL,
	[BZ] [nvarchar](4000) NULL,
	[Sort] [int] NULL,
	[YuanYin] [nvarchar](500) NULL,
 CONSTRAINT [BM_XCBDDtlpk] PRIMARY KEY CLUSTERED 
(
	[OID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[BM_XCBDDtl] ([OID], [FK_Emp], [Title], [Target], [Url], [FK_Dept], [XXSort], [MJ], [DTApp], [DTRel], [ZZDW], [BillNo], [BZ], [Sort], [YuanYin]) VALUES (127, N'003', N'asdfasdsadfasdf', N'asdfa', N'sdfasdasdfasfasf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[BM_XCBDDtl] ([OID], [FK_Emp], [Title], [Target], [Url], [FK_Dept], [XXSort], [MJ], [DTApp], [DTRel], [ZZDW], [BillNo], [BZ], [Sort], [YuanYin]) VALUES (129, N'002', N'wqer', N'qwerqwer', N'rqwerqwe', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[BM_XCBDDtl] ([OID], [FK_Emp], [Title], [Target], [Url], [FK_Dept], [XXSort], [MJ], [DTApp], [DTRel], [ZZDW], [BillNo], [BZ], [Sort], [YuanYin]) VALUES (131, N'002', N'werqwe', N'wqer', N'qwerwqer', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[BM_XCBDDtl] ([OID], [FK_Emp], [Title], [Target], [Url], [FK_Dept], [XXSort], [MJ], [DTApp], [DTRel], [ZZDW], [BillNo], [BZ], [Sort], [YuanYin]) VALUES (141, N'002', N'asdfasdfas', NULL, NULL, N'1003', 2, 1, N'2015-06-29', N'2015-07-07', N'asdfasdadasfassdfasdfasdfas', N'fasdfsadfasdfa', N'dfsdfasdfasddsfasdf
asdfasdfsdfsd
asdfasdfasdfasd
asdfasdfasdfsdfsd', NULL, NULL)
INSERT [dbo].[BM_XCBDDtl] ([OID], [FK_Emp], [Title], [Target], [Url], [FK_Dept], [XXSort], [MJ], [DTApp], [DTRel], [ZZDW], [BillNo], [BZ], [Sort], [YuanYin]) VALUES (143, N'003', N'sdfasasdfasdfa', NULL, NULL, N'1004', 0, 0, N'2015-07-07', N'2015-07-07', N'sdfasdfa', N'sdfasdf', N'asdfasfasdfasdfasdf', NULL, NULL)
INSERT [dbo].[BM_XCBDDtl] ([OID], [FK_Emp], [Title], [Target], [Url], [FK_Dept], [XXSort], [MJ], [DTApp], [DTRel], [ZZDW], [BillNo], [BZ], [Sort], [YuanYin]) VALUES (145, N'003', N'asdfasdasd', NULL, NULL, N'1003', 1, 1, N'2015-07-14', N'2015-07-13', N'asdfasdsfasdfasd', N'dfasdsdfas', N'fasdfasdfasasdfasdsadfasd
asasdfsdfsd
dfasdfasdfsdfsd', NULL, NULL)
INSERT [dbo].[BM_XCBDDtl] ([OID], [FK_Emp], [Title], [Target], [Url], [FK_Dept], [XXSort], [MJ], [DTApp], [DTRel], [ZZDW], [BillNo], [BZ], [Sort], [YuanYin]) VALUES (147, N'003', N'asdfasdasdfasd', NULL, NULL, N'1061', 2, 1, N'2015-07-15', N'2015-06-29', N'asdfas', N'dfasd', N'fasdfasdfsdfsd', NULL, NULL)
INSERT [dbo].[BM_XCBDDtl] ([OID], [FK_Emp], [Title], [Target], [Url], [FK_Dept], [XXSort], [MJ], [DTApp], [DTRel], [ZZDW], [BillNo], [BZ], [Sort], [YuanYin]) VALUES (149, N'002', N'sadfasfsdfsd', NULL, NULL, N'100', 1, 1, N'2015-07-21', N'2015-07-22', N'adsfasd', N'fasdfa', N'sdfasfdasd', NULL, NULL)
/****** Object:  Table [dbo].[BM_WLJF]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BM_WLJF](
	[OID] [int] NOT NULL,
	[BH] [nvarchar](50) NULL,
	[Name] [nvarchar](50) NULL,
	[FK_Dept] [nvarchar](100) NULL,
	[FK_Emp] [nvarchar](100) NULL,
	[SBLB] [int] NULL,
	[SBZT] [int] NULL,
	[WLLB] [int] NULL,
	[WLJFMJ] [int] NULL,
	[PPXH] [nvarchar](50) NULL,
	[SKey] [nvarchar](50) NULL,
	[GQ] [int] NULL,
	[XTBB] [nvarchar](50) NULL,
	[AZRQ] [nvarchar](50) NULL,
	[XLH] [nvarchar](50) NULL,
	[IP] [nvarchar](50) NULL,
	[MAC] [nvarchar](50) NULL,
	[DK] [nvarchar](50) NULL,
	[DD] [nvarchar](50) NULL,
	[YT] [nvarchar](500) NULL,
	[BZ] [nvarchar](4000) NULL,
	[CZXT] [int] NULL,
 CONSTRAINT [BM_WLJFpk] PRIMARY KEY CLUSTERED 
(
	[OID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[BM_WLJF] ([OID], [BH], [Name], [FK_Dept], [FK_Emp], [SBLB], [SBZT], [WLLB], [WLJFMJ], [PPXH], [SKey], [GQ], [XTBB], [AZRQ], [XLH], [IP], [MAC], [DK], [DD], [YT], [BZ], [CZXT]) VALUES (177, N'asdfsa', N'sadfasdfasd', N'100', N'001', 0, 3, 0, 0, N'sdfasdf', N'fasdfsdfas', 0, N'fsdfassdfsdfsdfsdf', N'2015-07-06', N'sdaf', N'dfasdfasdfas', N'sadfas', N'fsadf', N'sdfasdssdsds', N'fasdfas', N'sadfsdfasdsadfasd
asd
fasdfasdfasdfsad', 0)
INSERT [dbo].[BM_WLJF] ([OID], [BH], [Name], [FK_Dept], [FK_Emp], [SBLB], [SBZT], [WLLB], [WLJFMJ], [PPXH], [SKey], [GQ], [XTBB], [AZRQ], [XLH], [IP], [MAC], [DK], [DD], [YT], [BZ], [CZXT]) VALUES (181, N'asdfasd', N'asdfasdfasdfa', N'100', N'001', 3, 1, 0, 0, N'', N'', 0, N'asdfasdf', N'2015-07-07', N'fsdfa', N'asdfasd', N'asfads', N'fasfasf', N'sfasdf', N'asdf', N'sdfasdfadsfasasfasdfasf', 0)
/****** Object:  Table [dbo].[BM_USB]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BM_USB](
	[OID] [int] NOT NULL,
	[BH] [nvarchar](50) NULL,
	[Name] [nvarchar](50) NULL,
	[PPXH] [nvarchar](50) NULL,
	[SN] [nvarchar](50) NULL,
	[FK_Dept] [nvarchar](100) NULL,
	[FK_Emp] [nvarchar](100) NULL,
	[SBZT] [int] NULL,
	[USBMJ] [int] NULL,
	[XLH] [nvarchar](50) NULL,
	[YT] [nvarchar](500) NULL,
	[BZ] [nvarchar](4000) NULL,
	[USBType] [int] NULL,
 CONSTRAINT [BM_USBpk] PRIMARY KEY CLUSTERED 
(
	[OID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[BM_USB] ([OID], [BH], [Name], [PPXH], [SN], [FK_Dept], [FK_Emp], [SBZT], [USBMJ], [XLH], [YT], [BZ], [USBType]) VALUES (183, N'asdfasd', N'USb移动硬盘.', N'asdfasdfasdfasdf', N'sadfadsf', N'1002', N'001', 3, 1, N'asdfasdfasdfsdfsd', N'asdfa', N'sdfasdfasdfsadfasdfa
asdfasdfas', NULL)
/****** Object:  Table [dbo].[BM_SFWDtl]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BM_SFWDtl](
	[OID] [int] NOT NULL,
	[FK_Dept] [nvarchar](100) NULL,
	[FK_Emp] [nvarchar](100) NULL,
	[Sort] [int] NULL,
	[MJ] [int] NULL,
	[JSDW] [nvarchar](500) NULL,
	[Title] [nvarchar](500) NULL,
	[DTApp] [nvarchar](50) NULL,
	[DTRel] [nvarchar](50) NULL,
	[YS] [nvarchar](500) NULL,
	[FWBH] [nvarchar](100) NULL,
	[BillNo] [nvarchar](100) NULL,
	[BZ] [nvarchar](4000) NULL,
 CONSTRAINT [BM_SFWDtlpk] PRIMARY KEY CLUSTERED 
(
	[OID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BM_SBDtl]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BM_SBDtl](
	[OID] [int] NOT NULL,
	[FK_Dept] [nvarchar](100) NULL,
	[FK_Emp] [nvarchar](100) NULL,
	[ShiYongRen] [nvarchar](500) NULL,
	[Title] [nvarchar](500) NULL,
	[SBSort] [int] NULL,
	[MJ] [int] NULL,
	[YYLB] [int] NULL,
	[SBXH] [nvarchar](500) NULL,
	[SBBH] [nvarchar](500) NULL,
	[SBCCBH] [nvarchar](500) NULL,
	[GuangQu] [int] NULL,
	[IP] [nvarchar](500) NULL,
	[Mac] [nvarchar](500) NULL,
	[CFDD] [nvarchar](500) NULL,
	[SBSta] [int] NULL,
	[YongTu] [nvarchar](500) NULL,
	[DTApp] [nvarchar](50) NULL,
	[DTRel] [nvarchar](50) NULL,
	[BZ] [nvarchar](4000) NULL,
	[SBMJ] [int] NULL,
	[CZXT] [int] NULL,
	[MyNum] [int] NULL,
 CONSTRAINT [BM_SBDtlpk] PRIMARY KEY CLUSTERED 
(
	[OID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[BM_SBDtl] ([OID], [FK_Dept], [FK_Emp], [ShiYongRen], [Title], [SBSort], [MJ], [YYLB], [SBXH], [SBBH], [SBCCBH], [GuangQu], [IP], [Mac], [CFDD], [SBSta], [YongTu], [DTApp], [DTRel], [BZ], [SBMJ], [CZXT], [MyNum]) VALUES (155, N'100', N'001', N'asdfasd', N'asdfasdf', 0, NULL, 0, N'asdfasd', N'asdfasd', N'asdfasd', 1, N'asdf', N'adfasd', N'asdfasdfasdf', 2, N'dfasdf', N'2015-07-28', N'', N'asdfasdfasdfasdasdfasdasdfasdfasdffasd
sdsdsd
asdfasdfasdfasdfasdfasdfasdfasds', 2, 1, 1)
INSERT [dbo].[BM_SBDtl] ([OID], [FK_Dept], [FK_Emp], [ShiYongRen], [Title], [SBSort], [MJ], [YYLB], [SBXH], [SBBH], [SBCCBH], [GuangQu], [IP], [Mac], [CFDD], [SBSta], [YongTu], [DTApp], [DTRel], [BZ], [SBMJ], [CZXT], [MyNum]) VALUES (157, N'100', N'002', N'asdfasdfasdsadfasdasdfds', N'dsdsds', 0, NULL, 0, N'asdf', N'asdfasd', N'asdfasdf', 0, N'asdfasdasd', N'dfasfasfas', N'fasdfadsf', 2, N'asdfas', N'2015-07-29', N'', N'asdfasdsdfas sds 
sdfsdfsddfasdfasdfasdf
sdfasdfsdfsdasdfsdaasdfasdfsdfs', 2, 0, 1)
INSERT [dbo].[BM_SBDtl] ([OID], [FK_Dept], [FK_Emp], [ShiYongRen], [Title], [SBSort], [MJ], [YYLB], [SBXH], [SBBH], [SBCCBH], [GuangQu], [IP], [Mac], [CFDD], [SBSta], [YongTu], [DTApp], [DTRel], [BZ], [SBMJ], [CZXT], [MyNum]) VALUES (165, N'100', N'003', N'asdfas', N'dfasfsadsfasdfasdfas', 1, NULL, 1, N'sdafasdf', N'asdf', N'asdfasdf', 1, N'sdfasd', N'dsfadssdsdsd', N'sdfads', 1, N'fasdfadfa', N'2015-08-06', N'2015-07-29', N'asdfsadfsdfasdfasdf

dfasdfasdf', 1, 3, 1)
INSERT [dbo].[BM_SBDtl] ([OID], [FK_Dept], [FK_Emp], [ShiYongRen], [Title], [SBSort], [MJ], [YYLB], [SBXH], [SBBH], [SBCCBH], [GuangQu], [IP], [Mac], [CFDD], [SBSta], [YongTu], [DTApp], [DTRel], [BZ], [SBMJ], [CZXT], [MyNum]) VALUES (171, N'100', N'002', N'asdfas', N'asdfa', 2, NULL, 1, N'asdfas', N'asdfs', N'sadf', 2, N'asdfa', N'asdfa', N'sdfsd', 2, N'sdfasdf', N'2015-07-30', N'2015-07-31', N'fasdfaasdfasdfasd', 1, 1, NULL)
/****** Object:  Table [dbo].[BM_SB]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BM_SB](
	[No] [nvarchar](5) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[FK_Dept] [nvarchar](100) NULL,
	[FK_Emp] [nvarchar](50) NULL,
	[MJ] [int] NULL,
	[PPXH] [nvarchar](50) NULL,
	[SYQK] [nvarchar](50) NULL,
	[IsSM] [int] NULL,
 CONSTRAINT [BM_SBpk] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[BM_SB] ([No], [Name], [FK_Dept], [FK_Emp], [MJ], [PPXH], [SYQK], [IsSM]) VALUES (N'00001', N'23QWQW', N'1001', N'003', 0, N'23', N'2323', 0)
INSERT [dbo].[BM_SB] ([No], [Name], [FK_Dept], [FK_Emp], [MJ], [PPXH], [SYQK], [IsSM]) VALUES (N'00002', N'sdsf', N'1003', N'003', 1, N'sdfds', N'asAD', 1)
INSERT [dbo].[BM_SB] ([No], [Name], [FK_Dept], [FK_Emp], [MJ], [PPXH], [SYQK], [IsSM]) VALUES (N'00003', N'ASDasdA', N'100', N'003', 0, N'asd', N'ASDas', 0)
INSERT [dbo].[BM_SB] ([No], [Name], [FK_Dept], [FK_Emp], [MJ], [PPXH], [SYQK], [IsSM]) VALUES (N'00004', N'ASDAD', N'1002', N'003', 0, N'', N'', 0)
INSERT [dbo].[BM_SB] ([No], [Name], [FK_Dept], [FK_Emp], [MJ], [PPXH], [SYQK], [IsSM]) VALUES (N'00005', N'DAFASDFSAD', N'1004', N'003', 0, N'', N'', 0)
INSERT [dbo].[BM_SB] ([No], [Name], [FK_Dept], [FK_Emp], [MJ], [PPXH], [SYQK], [IsSM]) VALUES (N'00006', N'', N'100', N'003', 0, N'', N'', 0)
INSERT [dbo].[BM_SB] ([No], [Name], [FK_Dept], [FK_Emp], [MJ], [PPXH], [SYQK], [IsSM]) VALUES (N'00007', N'asd', N'1002', N'002', 1, N'asdfasd', N'fasdfas232323', 0)
INSERT [dbo].[BM_SB] ([No], [Name], [FK_Dept], [FK_Emp], [MJ], [PPXH], [SYQK], [IsSM]) VALUES (N'00008', N'asdfa', N'100', N'002', 0, N'sadf', N'23232', 0)
INSERT [dbo].[BM_SB] ([No], [Name], [FK_Dept], [FK_Emp], [MJ], [PPXH], [SYQK], [IsSM]) VALUES (N'00009', N'dsfasd', N'1003', N'002', 0, N'dsfasdf', N'asd32323fasdf', 0)
INSERT [dbo].[BM_SB] ([No], [Name], [FK_Dept], [FK_Emp], [MJ], [PPXH], [SYQK], [IsSM]) VALUES (N'00010', N'', N'100', N'002', 0, N'', N'', 0)
INSERT [dbo].[BM_SB] ([No], [Name], [FK_Dept], [FK_Emp], [MJ], [PPXH], [SYQK], [IsSM]) VALUES (N'00011', N'', N'100', N'002', 0, N'', N'', 0)
/****** Object:  Table [dbo].[BM_JSJ]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BM_JSJ](
	[OID] [int] NOT NULL,
	[BH] [nvarchar](50) NULL,
	[Name] [nvarchar](50) NULL,
	[FK_Dept] [nvarchar](100) NULL,
	[FK_Emp] [nvarchar](100) NULL,
	[SBLB] [int] NULL,
	[SBZT] [int] NULL,
	[WLLB] [int] NULL,
	[JSJMJ] [int] NULL,
	[SKey] [nvarchar](50) NULL,
	[GQ] [int] NULL,
	[XTBB] [nvarchar](50) NULL,
	[AZRQ] [nvarchar](50) NULL,
	[XLH] [nvarchar](50) NULL,
	[IP] [nvarchar](50) NULL,
	[MAC] [nvarchar](50) NULL,
	[DK] [nvarchar](50) NULL,
	[DD] [nvarchar](50) NULL,
	[YT] [nvarchar](500) NULL,
	[HHDY] [int] NULL,
	[SPGRY] [int] NULL,
	[BZ] [nvarchar](4000) NULL,
	[PPXH] [nvarchar](50) NULL,
	[SN] [nvarchar](50) NULL,
	[CZXT] [int] NULL,
 CONSTRAINT [BM_JSJpk] PRIMARY KEY CLUSTERED 
(
	[OID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[BM_JSJ] ([OID], [BH], [Name], [FK_Dept], [FK_Emp], [SBLB], [SBZT], [WLLB], [JSJMJ], [SKey], [GQ], [XTBB], [AZRQ], [XLH], [IP], [MAC], [DK], [DD], [YT], [HHDY], [SPGRY], [BZ], [PPXH], [SN], [CZXT]) VALUES (173, N'asdfasdf', N'asdfasdfasd', N'100', N'001', 0, 1, 0, 0, N'asdf', 0, N'asdfasdfasd', N'2015-07-14', N'fasdf', N'asdf', N'asdfad', N'asfas', N'fasdfasf', N'fasdfa', 0, 0, N'sfdasfasdfasasdfasdfadsfas', N'asdfa', N'sdfasfda', NULL)
INSERT [dbo].[BM_JSJ] ([OID], [BH], [Name], [FK_Dept], [FK_Emp], [SBLB], [SBZT], [WLLB], [JSJMJ], [SKey], [GQ], [XTBB], [AZRQ], [XLH], [IP], [MAC], [DK], [DD], [YT], [HHDY], [SPGRY], [BZ], [PPXH], [SN], [CZXT]) VALUES (175, N'asdfasd', N'fasdfasf', N'100', N'001', 0, 4, 0, 2, N'asdfa', 1, N'sdfads', N'2015-06-30', N'fasdfasdf', N'asdfa', N'asdfasdf', N'sdfasdf', N'asdfa', N'adsfasf', 0, 0, N'dsfasfasdfasfasd', N'asdfa', N'sdfasdfasdf', 0)
INSERT [dbo].[BM_JSJ] ([OID], [BH], [Name], [FK_Dept], [FK_Emp], [SBLB], [SBZT], [WLLB], [JSJMJ], [SKey], [GQ], [XTBB], [AZRQ], [XLH], [IP], [MAC], [DK], [DD], [YT], [HHDY], [SPGRY], [BZ], [PPXH], [SN], [CZXT]) VALUES (179, N'阿萨德发射的', N'大苏打', N'100', N'001', 0, 1, 0, 0, N'', 0, N'safsaf', N'2015-06-29', N'adfsafa', N'sdfasd', N'fsadfs', N'fasdfsadfas', N'asfasd', N'dfsaf', 0, 0, N'safasdfasdf', N'啊大撒旦', N'啊大撒旦', 2)
/****** Object:  Table [dbo].[BM_HuiYiDtl]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BM_HuiYiDtl](
	[OID] [int] NOT NULL,
	[FK_Dept] [nvarchar](100) NULL,
	[FK_Emp] [nvarchar](100) NULL,
	[HYSort] [int] NULL,
	[MJ] [int] NULL,
	[Apper] [nvarchar](500) NULL,
	[DTApp] [nvarchar](50) NULL,
	[Title] [nvarchar](500) NULL,
	[DTRel] [nvarchar](50) NULL,
	[CYR] [nvarchar](500) NULL,
	[BillNo] [nvarchar](100) NULL,
	[BZ] [nvarchar](4000) NULL,
	[MyFileName] [nvarchar](100) NULL,
	[MyFilePath] [nvarchar](100) NULL,
	[MyFileExt] [nvarchar](10) NULL,
	[WebPath] [nvarchar](200) NULL,
	[MyFileH] [int] NULL,
	[MyFileW] [int] NULL,
	[MyFileSize] [float] NULL,
 CONSTRAINT [BM_HuiYiDtlpk] PRIMARY KEY CLUSTERED 
(
	[OID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[BM_HuiYiDtl] ([OID], [FK_Dept], [FK_Emp], [HYSort], [MJ], [Apper], [DTApp], [Title], [DTRel], [CYR], [BillNo], [BZ], [MyFileName], [MyFilePath], [MyFileExt], [WebPath], [MyFileH], [MyFileW], [MyFileSize]) VALUES (151, N'100', N'001', 0, 0, N'asdfasdf', N'2015-06-30', N'asdfsdf', N'2015-07-07', N'asdfsdf', N'sdfas', N'dfasdfasdfasdfasdfsds', N'总体需求.docx', N'D:\ccflow\value-added\CCPortal\CCOA\CCOA\DataUser\BP.BM.HuiYiDtl\', N'docx', N'//DataUser/BP.BM.HuiYiDtl/151.docx', 0, 0, 0)
/****** Object:  Table [dbo].[BM_Emp]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BM_Emp](
	[No] [nvarchar](3) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[XueLi] [int] NULL,
	[XB] [int] NULL,
	[EmpSta] [int] NULL,
	[Age] [int] NULL,
	[GL] [int] NULL,
	[IDCard] [nvarchar](50) NULL,
	[Tel] [nvarchar](50) NULL,
	[Addr] [nvarchar](50) NULL,
	[EmpMJ] [int] NULL,
	[BZ] [nvarchar](4000) NULL,
	[GA_QFD] [nvarchar](50) NULL,
	[GA_ID] [nvarchar](50) NULL,
	[GA_AppDT] [nvarchar](50) NULL,
	[GA_ReltoDT] [nvarchar](50) NULL,
	[GA_Sta] [int] NULL,
	[GA_Note] [nvarchar](4000) NULL,
	[GA_QFD1] [nvarchar](50) NULL,
	[GA_ID1] [nvarchar](50) NULL,
	[GA_AppDT1] [nvarchar](50) NULL,
	[GA_ReltoDT1] [nvarchar](50) NULL,
	[GA_Sta1] [int] NULL,
	[GA_Note1] [nvarchar](50) NULL,
	[GA_QFD2] [nvarchar](50) NULL,
	[GA_ID2] [nvarchar](50) NULL,
	[GA_AppDT2] [nvarchar](50) NULL,
	[GA_ReltoDT2] [nvarchar](50) NULL,
	[GA_Sta2] [int] NULL,
	[GA_Note2] [nvarchar](50) NULL,
	[TA_QFD] [nvarchar](50) NULL,
	[TA_ID] [nvarchar](50) NULL,
	[TA_AppDT] [nvarchar](50) NULL,
	[TA_ReltoDT] [nvarchar](50) NULL,
	[TA_Sta] [int] NULL,
	[TA_Note] [nvarchar](4000) NULL,
	[HZ_QFD] [nvarchar](50) NULL,
	[HZ_ID] [nvarchar](50) NULL,
	[HZ_AppDT] [nvarchar](50) NULL,
	[HZ_ReltoDT] [nvarchar](50) NULL,
	[HZ_Sta] [int] NULL,
	[HZ_Note] [nvarchar](4000) NULL,
	[FK_Dept] [nvarchar](100) NULL,
	[FK_Duty] [nvarchar](100) NULL,
	[RYLX] [int] NULL,
	[ZZMM] [int] NULL,
 CONSTRAINT [BM_Emppk] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[BM_Emp] ([No], [Name], [XueLi], [XB], [EmpSta], [Age], [GL], [IDCard], [Tel], [Addr], [EmpMJ], [BZ], [GA_QFD], [GA_ID], [GA_AppDT], [GA_ReltoDT], [GA_Sta], [GA_Note], [GA_QFD1], [GA_ID1], [GA_AppDT1], [GA_ReltoDT1], [GA_Sta1], [GA_Note1], [GA_QFD2], [GA_ID2], [GA_AppDT2], [GA_ReltoDT2], [GA_Sta2], [GA_Note2], [TA_QFD], [TA_ID], [TA_AppDT], [TA_ReltoDT], [TA_Sta], [TA_Note], [HZ_QFD], [HZ_ID], [HZ_AppDT], [HZ_ReltoDT], [HZ_Sta], [HZ_Note], [FK_Dept], [FK_Duty], [RYLX], [ZZMM]) VALUES (N'001', N'周朋', 1, 1, 0, 20, 20, N'asdfdasf', N'asdfas', N'dfasdfasdfasdf', 0, N'dasdf', N'', N'', N'', N'', 0, N'', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'', N'', N'', N'', 0, N'', N'', N'', N'', N'', 0, N'', N'100', N'01', 0, 0)
INSERT [dbo].[BM_Emp] ([No], [Name], [XueLi], [XB], [EmpSta], [Age], [GL], [IDCard], [Tel], [Addr], [EmpMJ], [BZ], [GA_QFD], [GA_ID], [GA_AppDT], [GA_ReltoDT], [GA_Sta], [GA_Note], [GA_QFD1], [GA_ID1], [GA_AppDT1], [GA_ReltoDT1], [GA_Sta1], [GA_Note1], [GA_QFD2], [GA_ID2], [GA_AppDT2], [GA_ReltoDT2], [GA_Sta2], [GA_Note2], [TA_QFD], [TA_ID], [TA_AppDT], [TA_ReltoDT], [TA_Sta], [TA_Note], [HZ_QFD], [HZ_ID], [HZ_AppDT], [HZ_ReltoDT], [HZ_Sta], [HZ_Note], [FK_Dept], [FK_Duty], [RYLX], [ZZMM]) VALUES (N'002', N'周朋', 2, 1, 1, 3230, 232, N'asdfasd', N'asdsadfasdf', N'sdfsdfsdfsdsdfsd', 1, N'sdfasdfsdsddsdsxxsdsd ewewew
kkkkkkkcxxcxsdfassdsds dsfgdsgdeerererer
sdafsdfdsfdssdfasdfsdfsdf', N'', N'', N'', N'', 1, N'', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'', N'', N'', N'', 0, N'', N'', N'', N'', N'', 0, N'', N'100', N'01', 0, 0)
INSERT [dbo].[BM_Emp] ([No], [Name], [XueLi], [XB], [EmpSta], [Age], [GL], [IDCard], [Tel], [Addr], [EmpMJ], [BZ], [GA_QFD], [GA_ID], [GA_AppDT], [GA_ReltoDT], [GA_Sta], [GA_Note], [GA_QFD1], [GA_ID1], [GA_AppDT1], [GA_ReltoDT1], [GA_Sta1], [GA_Note1], [GA_QFD2], [GA_ID2], [GA_AppDT2], [GA_ReltoDT2], [GA_Sta2], [GA_Note2], [TA_QFD], [TA_ID], [TA_AppDT], [TA_ReltoDT], [TA_Sta], [TA_Note], [HZ_QFD], [HZ_ID], [HZ_AppDT], [HZ_ReltoDT], [HZ_Sta], [HZ_Note], [FK_Dept], [FK_Duty], [RYLX], [ZZMM]) VALUES (N'003', N'asdfasdf', 0, 1, 0, 23, 23, N'asdfasdfasdfsadf', N'', N'asdfasdfasdasdfasdfasdfasdfsdfsdf', 0, N'', N'asdfasdfasdf', N'', N'2015-06-29', N'2015-07-07', 0, N'', N'qwerqwerwer', N'', N'', N'', 0, N'', N'', N'', N'', N'', 0, N'', N'', N'', N'', N'', 0, N'', N'', N'', N'', N'', 0, N'', N'100', N'01', 0, 0)
INSERT [dbo].[BM_Emp] ([No], [Name], [XueLi], [XB], [EmpSta], [Age], [GL], [IDCard], [Tel], [Addr], [EmpMJ], [BZ], [GA_QFD], [GA_ID], [GA_AppDT], [GA_ReltoDT], [GA_Sta], [GA_Note], [GA_QFD1], [GA_ID1], [GA_AppDT1], [GA_ReltoDT1], [GA_Sta1], [GA_Note1], [GA_QFD2], [GA_ID2], [GA_AppDT2], [GA_ReltoDT2], [GA_Sta2], [GA_Note2], [TA_QFD], [TA_ID], [TA_AppDT], [TA_ReltoDT], [TA_Sta], [TA_Note], [HZ_QFD], [HZ_ID], [HZ_AppDT], [HZ_ReltoDT], [HZ_Sta], [HZ_Note], [FK_Dept], [FK_Duty], [RYLX], [ZZMM]) VALUES (N'004', N'SDFas', 2, 1, 0, 20, 20, N'dfasdfasdfasdfasd', N'', N'asdfasdfasdasdf', 1, N'', N'', N'', N'', N'', 0, N'', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'', N'', N'', N'', 0, N'', N'', N'', N'', N'', 0, N'', N'100', N'04', 0, 0)
INSERT [dbo].[BM_Emp] ([No], [Name], [XueLi], [XB], [EmpSta], [Age], [GL], [IDCard], [Tel], [Addr], [EmpMJ], [BZ], [GA_QFD], [GA_ID], [GA_AppDT], [GA_ReltoDT], [GA_Sta], [GA_Note], [GA_QFD1], [GA_ID1], [GA_AppDT1], [GA_ReltoDT1], [GA_Sta1], [GA_Note1], [GA_QFD2], [GA_ID2], [GA_AppDT2], [GA_ReltoDT2], [GA_Sta2], [GA_Note2], [TA_QFD], [TA_ID], [TA_AppDT], [TA_ReltoDT], [TA_Sta], [TA_Note], [HZ_QFD], [HZ_ID], [HZ_AppDT], [HZ_ReltoDT], [HZ_Sta], [HZ_Note], [FK_Dept], [FK_Duty], [RYLX], [ZZMM]) VALUES (N'005', N'efew', 0, 1, 0, 20, 20, N'ASD', N'', N'', 1, N'wrqwerasdADSadsA', N'', N'', N'', N'', 0, N'', N'', N'', N'', N'', 0, N'', N'', N'', N'', N'', 0, N'', N'', N'', N'', N'', 0, N'', N'', N'', N'', N'', 0, N'', N'100', N'01', 0, 0)
/****** Object:  Table [dbo].[BM_EduDtl]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BM_EduDtl](
	[OID] [int] NOT NULL,
	[Title] [nvarchar](500) NULL,
	[Target] [nvarchar](50) NULL,
	[Url] [nvarchar](500) NULL,
	[FK_Emp] [nvarchar](500) NULL,
	[DTFrom] [nvarchar](50) NULL,
	[DTTo] [nvarchar](50) NULL,
	[FK_Dept] [nvarchar](100) NULL,
	[XXLX] [int] NULL,
	[DTApp] [nvarchar](50) NULL,
	[XueShi] [int] NULL,
	[ZSBH] [nvarchar](100) NULL,
	[FZJG] [nvarchar](100) NULL,
	[DTRel] [nvarchar](50) NULL,
	[BillNo] [nvarchar](100) NULL,
	[SJLY] [int] NULL,
	[BZ] [nvarchar](4000) NULL,
 CONSTRAINT [BM_EduDtlpk] PRIMARY KEY CLUSTERED 
(
	[OID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[BM_EduDtl] ([OID], [Title], [Target], [Url], [FK_Emp], [DTFrom], [DTTo], [FK_Dept], [XXLX], [DTApp], [XueShi], [ZSBH], [FZJG], [DTRel], [BillNo], [SJLY], [BZ]) VALUES (113, N'dfawewe', N'werqwerqwe', N'wqerqwerqwe', N'002', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[BM_EduDtl] ([OID], [Title], [Target], [Url], [FK_Emp], [DTFrom], [DTTo], [FK_Dept], [XXLX], [DTApp], [XueShi], [ZSBH], [FZJG], [DTRel], [BillNo], [SJLY], [BZ]) VALUES (115, N'qwerqw', N'erqwe', N'rqwerqwerqwe', N'002', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[BM_EduDtl] ([OID], [Title], [Target], [Url], [FK_Emp], [DTFrom], [DTTo], [FK_Dept], [XXLX], [DTApp], [XueShi], [ZSBH], [FZJG], [DTRel], [BillNo], [SJLY], [BZ]) VALUES (117, N'wqerqwe', N'rqwe', N'rqwerqwerwerqwer', N'002', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[BM_EduDtl] ([OID], [Title], [Target], [Url], [FK_Emp], [DTFrom], [DTTo], [FK_Dept], [XXLX], [DTApp], [XueShi], [ZSBH], [FZJG], [DTRel], [BillNo], [SJLY], [BZ]) VALUES (119, N'wqerqwe', N'rqwer', N'qwerqwerqwwerq', N'002', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[BM_EduDtl] ([OID], [Title], [Target], [Url], [FK_Emp], [DTFrom], [DTTo], [FK_Dept], [XXLX], [DTApp], [XueShi], [ZSBH], [FZJG], [DTRel], [BillNo], [SJLY], [BZ]) VALUES (121, N'wqerqwer', N'qwer', N'qwrqwerqwe', N'002', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[BM_EduDtl] ([OID], [Title], [Target], [Url], [FK_Emp], [DTFrom], [DTTo], [FK_Dept], [XXLX], [DTApp], [XueShi], [ZSBH], [FZJG], [DTRel], [BillNo], [SJLY], [BZ]) VALUES (123, N'qwer', N'qwerqwe', N'rqwerqwe', N'002', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[BM_EduDtl] ([OID], [Title], [Target], [Url], [FK_Emp], [DTFrom], [DTTo], [FK_Dept], [XXLX], [DTApp], [XueShi], [ZSBH], [FZJG], [DTRel], [BillNo], [SJLY], [BZ]) VALUES (125, N'das', N'dsfasd', N'asdfasd', N'003', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[BM_EduDtl] ([OID], [Title], [Target], [Url], [FK_Emp], [DTFrom], [DTTo], [FK_Dept], [XXLX], [DTApp], [XueShi], [ZSBH], [FZJG], [DTRel], [BillNo], [SJLY], [BZ]) VALUES (159, N'sdfsdfasdfsdfasdfasd', NULL, NULL, N'001', N'', N'', N'100', 0, N'2015-07-06', 0, N'sdfas', N'asdfas', N'2015-07-14', N'asdf', 0, N'asdfasdfasdfsdfasd
asdfasdfasdfasdasdfasdfasd
adsfasdasdfasdfsdsdsdasdfasdfsdf')
INSERT [dbo].[BM_EduDtl] ([OID], [Title], [Target], [Url], [FK_Emp], [DTFrom], [DTTo], [FK_Dept], [XXLX], [DTApp], [XueShi], [ZSBH], [FZJG], [DTRel], [BillNo], [SJLY], [BZ]) VALUES (161, N'zcsdfasdfasd', NULL, NULL, N'001', N'', N'', N'100', 0, N'2015-07-08', 0, N'asdfasd', N'sdfa', N'2015-07-14', N'sdfasdf', 2, N'fasdfasdf')
INSERT [dbo].[BM_EduDtl] ([OID], [Title], [Target], [Url], [FK_Emp], [DTFrom], [DTTo], [FK_Dept], [XXLX], [DTApp], [XueShi], [ZSBH], [FZJG], [DTRel], [BillNo], [SJLY], [BZ]) VALUES (163, N'SDasd', NULL, NULL, N'003', N'', N'', N'100', 0, N'2015-07-08', 230, N'', N'asdfddsafasdfa', N'2015-07-21', N'sdfas', 2, N'dfasdfasdasdfasdfsad')
/****** Object:  Table [dbo].[BM_CRJDtl]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BM_CRJDtl](
	[OID] [int] NOT NULL,
	[Title] [nvarchar](500) NULL,
	[DTFrom] [nvarchar](50) NULL,
	[DTTo] [nvarchar](50) NULL,
	[FK_Emp] [nvarchar](500) NULL,
	[FK_Dept] [nvarchar](100) NULL,
	[MDD] [nvarchar](500) NULL,
	[DTApp] [nvarchar](50) NULL,
	[DTRel] [nvarchar](50) NULL,
	[DTBack] [nvarchar](50) NULL,
	[ZZBH] [nvarchar](500) NULL,
	[BillNo] [nvarchar](100) NULL,
	[BZ] [nvarchar](4000) NULL,
	[CardType] [int] NULL,
 CONSTRAINT [BM_CRJDtlpk] PRIMARY KEY CLUSTERED 
(
	[OID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[BM_CRJDtl] ([OID], [Title], [DTFrom], [DTTo], [FK_Emp], [FK_Dept], [MDD], [DTApp], [DTRel], [DTBack], [ZZBH], [BillNo], [BZ], [CardType]) VALUES (133, N'adsfawewews', N'2015-07-29', N'2015-07-14', N'002', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[BM_CRJDtl] ([OID], [Title], [DTFrom], [DTTo], [FK_Emp], [FK_Dept], [MDD], [DTApp], [DTRel], [DTBack], [ZZBH], [BillNo], [BZ], [CardType]) VALUES (135, N'sdfs', N'2015-07-14', N'2015-06-29', N'003', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[BM_CRJDtl] ([OID], [Title], [DTFrom], [DTTo], [FK_Emp], [FK_Dept], [MDD], [DTApp], [DTRel], [DTBack], [ZZBH], [BillNo], [BZ], [CardType]) VALUES (137, N'ewrw', N'2015-07-15', N'2015-06-28', N'003', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[BM_CRJDtl] ([OID], [Title], [DTFrom], [DTTo], [FK_Emp], [FK_Dept], [MDD], [DTApp], [DTRel], [DTBack], [ZZBH], [BillNo], [BZ], [CardType]) VALUES (139, N'qweqw', N'2015-07-06', N'2015-07-06', N'001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[BM_CRJDtl] ([OID], [Title], [DTFrom], [DTTo], [FK_Emp], [FK_Dept], [MDD], [DTApp], [DTRel], [DTBack], [ZZBH], [BillNo], [BZ], [CardType]) VALUES (169, N'fadsfsdf', NULL, NULL, N'001', N'100', N'adsfds', N'2015-07-14', N'2015-07-14', N'2015-07-07', N'sadfasdfads', N'sadfasd', N'fasdfasdfasdfas', NULL)
/****** Object:  Table [dbo].[BM_CardDtl]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BM_CardDtl](
	[OID] [int] NOT NULL,
	[FK_Dept] [nvarchar](100) NULL,
	[FK_Emp] [nvarchar](100) NULL,
	[CardType] [int] NULL,
	[QFD] [nvarchar](50) NULL,
	[Title] [nvarchar](50) NULL,
	[AppDT] [nvarchar](50) NULL,
	[ReltoDT] [nvarchar](50) NULL,
	[Sta] [int] NULL,
	[BZ] [nvarchar](4000) NULL,
 CONSTRAINT [BM_CardDtlpk] PRIMARY KEY CLUSTERED 
(
	[OID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[BM_CardDtl] ([OID], [FK_Dept], [FK_Emp], [CardType], [QFD], [Title], [AppDT], [ReltoDT], [Sta], [BZ]) VALUES (167, N'100', N'001', 0, N'asdfasdasdfasdf', N'asdfsdasdfasdf', N'2015-06-29', N'2015-07-15', 2, N'asdfsdfasdf
sadfasdfsadfasd
asdfasdfsdfsad')
/****** Object:  Table [dbo].[BM_AQCP]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BM_AQCP](
	[No] [nvarchar](5) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[FK_Dept] [nvarchar](100) NULL,
	[FK_Emp] [nvarchar](50) NULL,
	[MJ] [int] NULL,
	[SoftName] [nvarchar](50) NULL,
	[CShang] [nvarchar](50) NULL,
	[IsSM] [int] NULL,
 CONSTRAINT [BM_AQCPpk] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[BM_AQCP] ([No], [Name], [FK_Dept], [FK_Emp], [MJ], [SoftName], [CShang], [IsSM]) VALUES (N'00001', N'weqwerqwew', N'1002', N'002', 0, N'rqwerqwer', N'wqerq', 0)
INSERT [dbo].[BM_AQCP] ([No], [Name], [FK_Dept], [FK_Emp], [MJ], [SoftName], [CShang], [IsSM]) VALUES (N'00002', N'qwerqw', N'1003', N'002', 0, N'werqwe', N'werqwe', 0)
INSERT [dbo].[BM_AQCP] ([No], [Name], [FK_Dept], [FK_Emp], [MJ], [SoftName], [CShang], [IsSM]) VALUES (N'00003', N'erqwerqw', N'1001', N'002', 0, N'qwerq', N'qwerqwe', 0)
INSERT [dbo].[BM_AQCP] ([No], [Name], [FK_Dept], [FK_Emp], [MJ], [SoftName], [CShang], [IsSM]) VALUES (N'00004', N'werwer', N'100', N'002', 0, N'wqerqwe', N'rqwerqwerqwe', 0)
INSERT [dbo].[BM_AQCP] ([No], [Name], [FK_Dept], [FK_Emp], [MJ], [SoftName], [CShang], [IsSM]) VALUES (N'00005', N'wqer', N'1004', N'002', 0, N'werqwerqwe', N'wewerwerwe', 0)
INSERT [dbo].[BM_AQCP] ([No], [Name], [FK_Dept], [FK_Emp], [MJ], [SoftName], [CShang], [IsSM]) VALUES (N'00006', N'qwerqw', N'100', N'002', 0, N'wer', N'werwe', 0)
INSERT [dbo].[BM_AQCP] ([No], [Name], [FK_Dept], [FK_Emp], [MJ], [SoftName], [CShang], [IsSM]) VALUES (N'00007', N'erqwer', N'100', N'002', 0, N'rwer', N'werwe', 0)
INSERT [dbo].[BM_AQCP] ([No], [Name], [FK_Dept], [FK_Emp], [MJ], [SoftName], [CShang], [IsSM]) VALUES (N'00008', N'qwerqwer', N'100', N'002', 0, N'werwe', N'werwe', 0)
INSERT [dbo].[BM_AQCP] ([No], [Name], [FK_Dept], [FK_Emp], [MJ], [SoftName], [CShang], [IsSM]) VALUES (N'00009', N'erw', N'100', N'002', 0, N'werwe', N'werwe', 0)
INSERT [dbo].[BM_AQCP] ([No], [Name], [FK_Dept], [FK_Emp], [MJ], [SoftName], [CShang], [IsSM]) VALUES (N'00010', N'qerqwer', N'100', N'002', 0, N'wer', N'werwer', 0)
INSERT [dbo].[BM_AQCP] ([No], [Name], [FK_Dept], [FK_Emp], [MJ], [SoftName], [CShang], [IsSM]) VALUES (N'00011', N'wqerqwe', N'100', N'002', 0, N'erwerwe', N'werwe', 0)
INSERT [dbo].[BM_AQCP] ([No], [Name], [FK_Dept], [FK_Emp], [MJ], [SoftName], [CShang], [IsSM]) VALUES (N'00012', N'werwe', N'100', N'002', 0, N'rwerw', N'werwe', 0)
INSERT [dbo].[BM_AQCP] ([No], [Name], [FK_Dept], [FK_Emp], [MJ], [SoftName], [CShang], [IsSM]) VALUES (N'00013', N'', N'100', N'002', 0, N'', N'', 0)
/****** Object:  Table [dbo].[BM_AnQuanCP]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BM_AnQuanCP](
	[OID] [int] NOT NULL,
	[CPLB] [int] NULL,
	[BH] [nvarchar](50) NULL,
	[Name] [nvarchar](50) NULL,
	[PPXH] [nvarchar](50) NULL,
	[SN] [nvarchar](50) NULL,
	[FK_Dept] [nvarchar](100) NULL,
	[FK_Emp] [nvarchar](100) NULL,
	[SBZT] [int] NULL,
	[WLLB] [int] NULL,
	[AnQuanCPMJ] [int] NULL,
	[DD] [nvarchar](50) NULL,
	[YT] [nvarchar](500) NULL,
	[BZ] [nvarchar](4000) NULL,
 CONSTRAINT [BM_AnQuanCPpk] PRIMARY KEY CLUSTERED 
(
	[OID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[BM_AnQuanCP] ([OID], [CPLB], [BH], [Name], [PPXH], [SN], [FK_Dept], [FK_Emp], [SBZT], [WLLB], [AnQuanCPMJ], [DD], [YT], [BZ]) VALUES (185, 2, N'阿萨德发射的', N'撒旦发射', N'阿萨德发射的', N'阿萨德发射的', N'100', N'003', 3, 1, 2, N'撒旦发射的', N'阿萨德发射的', N'阿萨德法德萨呵隧道发生的阿萨德发射的
啊大幅度阿萨德发射的啊飒飒的')
INSERT [dbo].[BM_AnQuanCP] ([OID], [CPLB], [BH], [Name], [PPXH], [SN], [FK_Dept], [FK_Emp], [SBZT], [WLLB], [AnQuanCPMJ], [DD], [YT], [BZ]) VALUES (187, 0, N'啊倒萨岁的v', N'阿萨德发射的', N'啊苏打水', N'', N'100', N'003', 1, 1, 1, N'阿斯达斯的', N'啊苏打水', N'啊大撒旦撒撒旦发射的啊大师傅的啊苏打水')
/****** Object:  StoredProcedure [dbo].[SYST_pGetScopeRows]    Script Date: 07/06/2015 09:28:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SYST_pGetScopeRows] 

  @Sql nvarchar(2000),

  @Top int,

  @Order nvarchar(500),

  @nFrom int,

  @nTo int

AS

BEGIN

  declare @NewSql nvarchar(3000),@NewSqlStat nvarchar(3000),@TopStr nvarchar(100);

  set @NewSql=@Sql;

  set @TopStr='';

  set @NewSql=ltrim(rtrim(@NewSql));

  if(@Top>0)set @TopStr=' Top '+Cast(@Top as varchar)+' ';

  if (lower(left(@NewSql,6))='select' and charindex('order by',lower(@NewSql))=0)

  Begin

     set @NewSql='select '+@TopStr+' row_number() over ('+@Order+') as RowNum,A.* from ('+@NewSql+') as A';

     set @NewSql='select * from ('+@NewSql+') as T where T.RowNum between '+Cast(@nFrom as varchar)+' and '+Cast(@nTo as varchar)+' '+@Order+';';

     exec (@NewSql);
  End

  else
     select 'Not_Select_Sql_statement!'

END
GO
/****** Object:  Table [dbo].[WF_Emp]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WF_Emp](
	[No] [nvarchar](50) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[UseSta] [int] NULL,
	[Tel] [nvarchar](50) NULL,
	[FK_Dept] [nvarchar](100) NULL,
	[Email] [nvarchar](50) NULL,
	[TM] [nvarchar](50) NULL,
	[AlertWay] [int] NULL,
	[Author] [nvarchar](50) NULL,
	[AuthorDate] [nvarchar](50) NULL,
	[AuthorWay] [int] NULL,
	[AuthorToDate] [nvarchar](50) NULL,
	[AuthorFlows] [nvarchar](1000) NULL,
	[Stas] [nvarchar](3000) NULL,
	[FtpUrl] [nvarchar](50) NULL,
	[Msg] [nvarchar](4000) NULL,
	[Style] [nvarchar](4000) NULL,
	[Idx] [int] NULL,
 CONSTRAINT [WF_Emppk] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[WF_Direction]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WF_Direction](
	[MyPK] [nvarchar](100) NOT NULL,
	[FK_Flow] [nvarchar](3) NULL,
	[Node] [int] NULL,
	[ToNode] [int] NULL,
	[DirType] [int] NULL,
	[IsCanBack] [int] NULL,
	[Dots] [nvarchar](300) NULL,
 CONSTRAINT [WF_Directionpk] PRIMARY KEY CLUSTERED 
(
	[MyPK] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[WF_Direction] ([MyPK], [FK_Flow], [Node], [ToNode], [DirType], [IsCanBack], [Dots]) VALUES (N'001_101_102_0', N'001', 101, 102, 0, 0, N'')
INSERT [dbo].[WF_Direction] ([MyPK], [FK_Flow], [Node], [ToNode], [DirType], [IsCanBack], [Dots]) VALUES (N'002_201_202_0', N'002', 201, 202, 0, 0, N'')
INSERT [dbo].[WF_Direction] ([MyPK], [FK_Flow], [Node], [ToNode], [DirType], [IsCanBack], [Dots]) VALUES (N'003_301_302_0', N'003', 301, 302, 0, 0, N'')
/****** Object:  Table [dbo].[WF_DeptFlowSearch]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WF_DeptFlowSearch](
	[MyPK] [nvarchar](100) NOT NULL,
	[FK_Emp] [nvarchar](50) NULL,
	[FK_Flow] [nvarchar](50) NULL,
	[FK_Dept] [nvarchar](100) NULL,
 CONSTRAINT [WF_DeptFlowSearchpk] PRIMARY KEY CLUSTERED 
(
	[MyPK] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[WF_DataApply]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WF_DataApply](
	[MyPK] [nvarchar](100) NOT NULL,
	[WorkID] [int] NULL,
	[NodeId] [int] NULL,
	[RunState] [int] NULL,
	[ApplyDays] [int] NULL,
	[ApplyData] [nvarchar](50) NULL,
	[Applyer] [nvarchar](100) NULL,
	[ApplyNote1] [nvarchar](4000) NULL,
	[ApplyNote2] [nvarchar](4000) NULL,
	[Checker] [nvarchar](100) NULL,
	[CheckerData] [nvarchar](50) NULL,
	[CheckerDays] [int] NULL,
	[CheckerNote1] [nvarchar](4000) NULL,
	[CheckerNote2] [nvarchar](4000) NULL,
 CONSTRAINT [WF_DataApplypk] PRIMARY KEY CLUSTERED 
(
	[MyPK] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[WF_Cond]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WF_Cond](
	[MyPK] [nvarchar](100) NOT NULL,
	[CondType] [int] NULL,
	[DataFrom] [int] NULL,
	[FK_Flow] [nvarchar](60) NULL,
	[NodeID] [int] NULL,
	[FK_Node] [int] NULL,
	[FK_Attr] [nvarchar](80) NULL,
	[AttrKey] [nvarchar](60) NULL,
	[AttrName] [nvarchar](500) NULL,
	[FK_Operator] [nvarchar](60) NULL,
	[OperatorValue] [nvarchar](4000) NULL,
	[OperatorValueT] [nvarchar](4000) NULL,
	[ToNodeID] [int] NULL,
	[ConnJudgeWay] [int] NULL,
	[MyPOID] [int] NULL,
	[PRI] [int] NULL,
	[CondOrAnd] [int] NULL,
 CONSTRAINT [WF_Condpk] PRIMARY KEY CLUSTERED 
(
	[MyPK] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[WF_CHEval]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WF_CHEval](
	[MyPK] [nvarchar](100) NOT NULL,
	[Title] [nvarchar](500) NULL,
	[FK_Flow] [nvarchar](7) NULL,
	[FlowName] [nvarchar](100) NULL,
	[WorkID] [int] NULL,
	[FK_Node] [int] NULL,
	[NodeName] [nvarchar](100) NULL,
	[Rec] [nvarchar](50) NULL,
	[RecName] [nvarchar](50) NULL,
	[RDT] [nvarchar](50) NULL,
	[EvalEmpNo] [nvarchar](50) NULL,
	[EvalEmpName] [nvarchar](50) NULL,
	[EvalCent] [nvarchar](20) NULL,
	[EvalNote] [nvarchar](20) NULL,
	[FK_Dept] [nvarchar](50) NULL,
	[DeptName] [nvarchar](100) NULL,
	[FK_NY] [nvarchar](7) NULL,
 CONSTRAINT [WF_CHEvalpk] PRIMARY KEY CLUSTERED 
(
	[MyPK] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[WF_CCStation]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WF_CCStation](
	[FK_Node] [int] NOT NULL,
	[FK_Station] [nvarchar](100) NOT NULL,
 CONSTRAINT [WF_CCStationpk] PRIMARY KEY CLUSTERED 
(
	[FK_Node] ASC,
	[FK_Station] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[WF_CCList]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WF_CCList](
	[MyPK] [nvarchar](100) NOT NULL,
	[Title] [nvarchar](500) NULL,
	[FK_Flow] [nvarchar](3) NULL,
	[FlowName] [nvarchar](200) NULL,
	[NDFrom] [int] NULL,
	[FK_Node] [int] NULL,
	[NodeName] [nvarchar](500) NULL,
	[WorkID] [int] NULL,
	[FID] [int] NULL,
	[Doc] [nvarchar](4000) NULL,
	[Rec] [nvarchar](50) NULL,
	[RecDept] [nvarchar](50) NULL,
	[RDT] [nvarchar](50) NULL,
	[Sta] [int] NULL,
	[CCTo] [nvarchar](50) NULL,
	[CCToDept] [nvarchar](50) NULL,
	[CCToName] [nvarchar](50) NULL,
	[CheckNote] [nvarchar](600) NULL,
	[CDT] [nvarchar](50) NULL,
	[PFlowNo] [nvarchar](100) NULL,
	[PWorkID] [int] NULL,
 CONSTRAINT [WF_CCListpk] PRIMARY KEY CLUSTERED 
(
	[MyPK] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[WF_CCEmp]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WF_CCEmp](
	[FK_Node] [int] NOT NULL,
	[FK_Emp] [nvarchar](100) NOT NULL,
 CONSTRAINT [WF_CCEmppk] PRIMARY KEY CLUSTERED 
(
	[FK_Node] ASC,
	[FK_Emp] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[WF_CCDept]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WF_CCDept](
	[FK_Node] [int] NOT NULL,
	[FK_Dept] [nvarchar](100) NOT NULL,
 CONSTRAINT [WF_CCDeptpk] PRIMARY KEY CLUSTERED 
(
	[FK_Node] ASC,
	[FK_Dept] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[WF_BillType]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WF_BillType](
	[No] [nvarchar](2) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[FK_Flow] [nvarchar](50) NULL,
	[IDX] [int] NULL,
 CONSTRAINT [WF_BillTypepk] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[WF_BillTemplate]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WF_BillTemplate](
	[No] [nvarchar](300) NOT NULL,
	[Name] [nvarchar](200) NULL,
	[Url] [nvarchar](200) NULL,
	[NodeID] [int] NULL,
	[BillFileType] [int] NULL,
	[FK_BillType] [nvarchar](4) NULL,
	[IDX] [nvarchar](200) NULL,
	[ExpField] [nvarchar](800) NULL,
	[ReplaceVal] [nvarchar](3000) NULL,
 CONSTRAINT [WF_BillTemplatepk] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[WF_Bill]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WF_Bill](
	[MyPK] [nvarchar](100) NOT NULL,
	[WorkID] [int] NULL,
	[FID] [int] NULL,
	[FK_Flow] [nvarchar](4) NULL,
	[FK_BillType] [nvarchar](300) NULL,
	[Title] [nvarchar](900) NULL,
	[FK_Starter] [nvarchar](50) NULL,
	[StartDT] [nvarchar](50) NULL,
	[Url] [nvarchar](2000) NULL,
	[FullPath] [nvarchar](2000) NULL,
	[FK_Emp] [nvarchar](100) NULL,
	[RDT] [nvarchar](50) NULL,
	[FK_Dept] [nvarchar](100) NULL,
	[FK_NY] [nvarchar](100) NULL,
	[Emps] [nvarchar](4000) NULL,
	[FK_Node] [nvarchar](30) NULL,
	[FK_Bill] [nvarchar](500) NULL,
	[MyNum] [int] NULL,
 CONSTRAINT [WF_Billpk] PRIMARY KEY CLUSTERED 
(
	[MyPK] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[WF_AccepterRole]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WF_AccepterRole](
	[OID] [int] NOT NULL,
	[Name] [nvarchar](200) NULL,
	[FK_Node] [nvarchar](100) NULL,
	[FK_Mode] [int] NULL,
	[Tag0] [nvarchar](999) NULL,
	[Tag1] [nvarchar](999) NULL,
	[Tag2] [nvarchar](999) NULL,
	[Tag3] [nvarchar](999) NULL,
	[Tag4] [nvarchar](999) NULL,
	[Tag5] [nvarchar](999) NULL,
 CONSTRAINT [WF_AccepterRolepk] PRIMARY KEY CLUSTERED 
(
	[OID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ZZ_DRSheet]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ZZ_DRSheet](
	[OID] [int] NOT NULL,
	[Doc] [nvarchar](4000) NULL,
	[DRSta] [int] NULL,
	[DRDate] [nvarchar](50) NULL,
	[DRType] [nvarchar](100) NULL,
	[Rec] [nvarchar](100) NULL,
	[RDT] [nvarchar](50) NULL,
	[FK_Dept] [nvarchar](100) NULL,
 CONSTRAINT [ZZ_DRSheetpk] PRIMARY KEY CLUSTERED 
(
	[OID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[WorkRegister]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WorkRegister](
	[OID] [int] NOT NULL,
	[Sequence] [nvarchar](50) NULL,
	[RegisterType] [nvarchar](50) NULL,
	[CanRegisterTime] [nvarchar](50) NULL,
	[AheadRegisterTime] [nvarchar](50) NULL,
	[BehindRegisterTime] [nvarchar](50) NULL,
 CONSTRAINT [WorkRegisterpk] PRIMARY KEY CLUSTERED 
(
	[OID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[WF_WorkFlowDeleteLog]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WF_WorkFlowDeleteLog](
	[OID] [int] NOT NULL,
	[FID] [int] NULL,
	[FK_Dept] [nvarchar](100) NULL,
	[Title] [nvarchar](100) NULL,
	[FlowStarter] [nvarchar](100) NULL,
	[FlowStartRDT] [nvarchar](50) NULL,
	[FK_NY] [nvarchar](100) NULL,
	[FK_Flow] [nvarchar](100) NULL,
	[FlowEnderRDT] [nvarchar](50) NULL,
	[FlowEndNode] [int] NULL,
	[FlowDaySpan] [int] NULL,
	[MyNum] [int] NULL,
	[FlowEmps] [nvarchar](100) NULL,
	[Oper] [nvarchar](20) NULL,
	[OperDept] [nvarchar](20) NULL,
	[OperDeptName] [nvarchar](200) NULL,
	[DeleteNote] [nvarchar](4000) NULL,
	[DeleteDT] [nvarchar](50) NULL,
 CONSTRAINT [WF_WorkFlowDeleteLogpk] PRIMARY KEY CLUSTERED 
(
	[OID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[WF_TurnTo]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WF_TurnTo](
	[MyPK] [nvarchar](100) NOT NULL,
	[TurnToType] [int] NULL,
	[FK_Flow] [nvarchar](60) NULL,
	[FK_Node] [int] NULL,
	[FK_Attr] [nvarchar](80) NULL,
	[AttrKey] [nvarchar](80) NULL,
	[AttrT] [nvarchar](80) NULL,
	[FK_Operator] [nvarchar](60) NULL,
	[OperatorValue] [nvarchar](60) NULL,
	[OperatorValueT] [nvarchar](60) NULL,
	[TurnToURL] [nvarchar](700) NULL,
 CONSTRAINT [WF_TurnTopk] PRIMARY KEY CLUSTERED 
(
	[MyPK] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[WF_TransferCustom]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WF_TransferCustom](
	[MyPK] [nvarchar](100) NOT NULL,
	[WorkID] [int] NULL,
	[FK_Node] [int] NULL,
	[Worker] [nvarchar](200) NULL,
	[SubFlowNo] [nvarchar](3) NULL,
	[RDT] [nvarchar](50) NULL,
	[Idx] [int] NULL,
 CONSTRAINT [WF_TransferCustompk] PRIMARY KEY CLUSTERED 
(
	[MyPK] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[WF_Task]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WF_Task](
	[MyPK] [nvarchar](100) NOT NULL,
	[FK_Flow] [nvarchar](200) NULL,
	[Starter] [nvarchar](200) NULL,
	[Paras] [nvarchar](4000) NULL,
	[TaskSta] [int] NULL,
	[Msg] [nvarchar](4000) NULL,
	[StartDT] [nvarchar](20) NULL,
	[RDT] [nvarchar](20) NULL,
 CONSTRAINT [WF_Taskpk] PRIMARY KEY CLUSTERED 
(
	[MyPK] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[WF_ShiftWork]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WF_ShiftWork](
	[MyPK] [nvarchar](100) NOT NULL,
	[WorkID] [int] NULL,
	[FK_Node] [int] NULL,
	[FK_Emp] [nvarchar](40) NULL,
	[FK_EmpName] [nvarchar](40) NULL,
	[ToEmp] [nvarchar](40) NULL,
	[ToEmpName] [nvarchar](40) NULL,
	[RDT] [nvarchar](50) NULL,
	[Note] [nvarchar](2000) NULL,
	[IsRead] [int] NULL,
 CONSTRAINT [WF_ShiftWorkpk] PRIMARY KEY CLUSTERED 
(
	[MyPK] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[WF_SelectInfo]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WF_SelectInfo](
	[MyPK] [nvarchar](100) NOT NULL,
	[AcceptNodeID] [int] NULL,
	[WorkID] [int] NULL,
	[InfoLeft] [nvarchar](200) NULL,
	[InfoCenter] [nvarchar](200) NULL,
	[InfoRight] [nvarchar](200) NULL,
	[AccType] [int] NULL,
 CONSTRAINT [WF_SelectInfopk] PRIMARY KEY CLUSTERED 
(
	[MyPK] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[WF_SelectAccper]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WF_SelectAccper](
	[MyPK] [nvarchar](100) NOT NULL,
	[FK_Node] [int] NULL,
	[WorkID] [int] NULL,
	[FK_Emp] [nvarchar](20) NULL,
	[EmpName] [nvarchar](20) NULL,
	[AccType] [int] NULL,
	[Rec] [nvarchar](20) NULL,
	[Info] [nvarchar](200) NULL,
	[IsRemember] [int] NULL,
	[Idx] [int] NULL,
	[Tag] [nvarchar](200) NULL,
 CONSTRAINT [WF_SelectAccperpk] PRIMARY KEY CLUSTERED 
(
	[MyPK] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[WF_ReturnWork]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WF_ReturnWork](
	[MyPK] [nvarchar](100) NOT NULL,
	[WorkID] [int] NULL,
	[ReturnNode] [int] NULL,
	[ReturnNodeName] [nvarchar](200) NULL,
	[Returner] [nvarchar](20) NULL,
	[ReturnerName] [nvarchar](200) NULL,
	[ReturnToNode] [int] NULL,
	[ReturnToEmp] [nvarchar](4000) NULL,
	[Note] [nvarchar](4000) NULL,
	[RDT] [nvarchar](50) NULL,
	[IsBackTracking] [int] NULL,
 CONSTRAINT [WF_ReturnWorkpk] PRIMARY KEY CLUSTERED 
(
	[MyPK] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[WF_RememberMe]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WF_RememberMe](
	[MyPK] [nvarchar](100) NOT NULL,
	[FK_Node] [int] NULL,
	[FK_Emp] [nvarchar](30) NULL,
	[Objs] [nvarchar](4000) NULL,
	[ObjsExt] [nvarchar](4000) NULL,
	[Emps] [nvarchar](4000) NULL,
	[EmpsExt] [nvarchar](4000) NULL,
 CONSTRAINT [WF_RememberMepk] PRIMARY KEY CLUSTERED 
(
	[MyPK] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[WF_PushMsg]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WF_PushMsg](
	[MyPK] [nvarchar](100) NOT NULL,
	[FK_Node] [int] NULL,
	[FK_Event] [nvarchar](15) NULL,
	[PushWay] [int] NULL,
	[PushDoc] [nvarchar](3500) NULL,
	[Tag] [nvarchar](500) NULL,
	[MsgMailEnable] [int] NULL,
	[MailTitle] [nvarchar](200) NULL,
	[MailDoc] [nvarchar](4000) NULL,
	[SMSEnable] [int] NULL,
	[SMSDoc] [nvarchar](4000) NULL,
	[MobilePushEnable] [int] NULL,
 CONSTRAINT [WF_PushMsgpk] PRIMARY KEY CLUSTERED 
(
	[MyPK] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[WF_NodeToolbar]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WF_NodeToolbar](
	[OID] [int] NOT NULL,
	[Title] [nvarchar](100) NULL,
	[Target] [nvarchar](50) NULL,
	[Url] [nvarchar](500) NULL,
	[ShowWhere] [int] NULL,
	[Idx] [int] NULL,
	[FK_Node] [int] NULL,
	[MyFileName] [nvarchar](100) NULL,
	[MyFilePath] [nvarchar](100) NULL,
	[MyFileExt] [nvarchar](10) NULL,
	[WebPath] [nvarchar](200) NULL,
	[MyFileH] [int] NULL,
	[MyFileW] [int] NULL,
	[MyFileSize] [float] NULL,
 CONSTRAINT [WF_NodeToolbarpk] PRIMARY KEY CLUSTERED 
(
	[OID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[WF_NodeStation]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WF_NodeStation](
	[FK_Node] [int] NOT NULL,
	[FK_Station] [nvarchar](100) NOT NULL,
 CONSTRAINT [WF_NodeStationpk] PRIMARY KEY CLUSTERED 
(
	[FK_Node] ASC,
	[FK_Station] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[WF_NodeReturn]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WF_NodeReturn](
	[FK_Node] [int] NOT NULL,
	[ReturnTo] [int] NOT NULL,
	[Dots] [nvarchar](300) NULL,
 CONSTRAINT [WF_NodeReturnpk] PRIMARY KEY CLUSTERED 
(
	[FK_Node] ASC,
	[ReturnTo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[WF_NodeFlow]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WF_NodeFlow](
	[FK_Node] [int] NOT NULL,
	[FK_Flow] [nvarchar](100) NOT NULL,
 CONSTRAINT [WF_NodeFlowpk] PRIMARY KEY CLUSTERED 
(
	[FK_Node] ASC,
	[FK_Flow] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[WF_NodeEmp]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WF_NodeEmp](
	[FK_Node] [int] NOT NULL,
	[FK_Emp] [nvarchar](100) NOT NULL,
 CONSTRAINT [WF_NodeEmppk] PRIMARY KEY CLUSTERED 
(
	[FK_Node] ASC,
	[FK_Emp] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[WF_NodeDept]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WF_NodeDept](
	[FK_Node] [int] NOT NULL,
	[FK_Dept] [nvarchar](100) NOT NULL,
 CONSTRAINT [WF_NodeDeptpk] PRIMARY KEY CLUSTERED 
(
	[FK_Node] ASC,
	[FK_Dept] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[WF_NodeCancel]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WF_NodeCancel](
	[FK_Node] [int] NOT NULL,
	[CancelTo] [int] NOT NULL,
 CONSTRAINT [WF_NodeCancelpk] PRIMARY KEY CLUSTERED 
(
	[FK_Node] ASC,
	[CancelTo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[WF_Node]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WF_Node](
	[NodeID] [int] NOT NULL,
	[SendLab] [nvarchar](50) NULL,
	[SendJS] [nvarchar](50) NULL,
	[JumpWayLab] [nvarchar](50) NULL,
	[JumpWay] [int] NULL,
	[SaveLab] [nvarchar](50) NULL,
	[SaveEnable] [int] NULL,
	[ThreadLab] [nvarchar](50) NULL,
	[ThreadEnable] [int] NULL,
	[SubFlowLab] [nvarchar](50) NULL,
	[SubFlowCtrlRole] [int] NULL,
	[ReturnLab] [nvarchar](50) NULL,
	[ReturnRole] [int] NULL,
	[ReturnField] [nvarchar](50) NULL,
	[CCLab] [nvarchar](50) NULL,
	[CCRole] [int] NULL,
	[ShiftLab] [nvarchar](50) NULL,
	[ShiftEnable] [int] NULL,
	[DelLab] [nvarchar](50) NULL,
	[DelEnable] [int] NULL,
	[EndFlowLab] [nvarchar](50) NULL,
	[EndFlowEnable] [int] NULL,
	[HungLab] [nvarchar](50) NULL,
	[HungEnable] [int] NULL,
	[PrintDocLab] [nvarchar](50) NULL,
	[PrintDocEnable] [int] NULL,
	[TrackLab] [nvarchar](50) NULL,
	[TrackEnable] [int] NULL,
	[SelectAccepterLab] [nvarchar](50) NULL,
	[SelectAccepterEnable] [int] NULL,
	[SearchLab] [nvarchar](50) NULL,
	[SearchEnable] [int] NULL,
	[WorkCheckLab] [nvarchar](50) NULL,
	[WorkCheckEnable] [int] NULL,
	[BatchLab] [nvarchar](50) NULL,
	[BatchEnable] [int] NULL,
	[AskforLab] [nvarchar](50) NULL,
	[AskforEnable] [int] NULL,
	[TCLab] [nvarchar](50) NULL,
	[TCEnable] [int] NULL,
	[WebOffice] [nvarchar](50) NULL,
	[WebOfficeEnable] [int] NULL,
	[OfficeOpenLab] [nvarchar](50) NULL,
	[OfficeOpenEnable] [int] NULL,
	[OfficeOpenTemplateLab] [nvarchar](50) NULL,
	[OfficeOpenTemplateEnable] [int] NULL,
	[OfficeSaveLab] [nvarchar](50) NULL,
	[OfficeSaveEnable] [int] NULL,
	[OfficeAcceptLab] [nvarchar](50) NULL,
	[OfficeAcceptEnable] [int] NULL,
	[OfficeRefuseLab] [nvarchar](50) NULL,
	[OfficeRefuseEnable] [int] NULL,
	[OfficeTHTemplate] [nvarchar](200) NULL,
	[OfficeOverEnable] [int] NULL,
	[OfficeMarksEnable] [int] NULL,
	[OfficePrintLab] [nvarchar](50) NULL,
	[OfficePrintEnable] [int] NULL,
	[OfficeSealLab] [nvarchar](50) NULL,
	[OfficeSealEnable] [int] NULL,
	[OfficeInsertFlowLab] [nvarchar](50) NULL,
	[OfficeInsertFlowEnable] [int] NULL,
	[OfficeNodeInfo] [int] NULL,
	[OfficeReSavePDF] [int] NULL,
	[OfficeDownLab] [nvarchar](50) NULL,
	[OfficeDownEnable] [int] NULL,
	[OfficeIsMarks] [int] NULL,
	[OfficeTemplate] [nvarchar](100) NULL,
	[OfficeIsParent] [int] NULL,
	[OfficeTHEnable] [int] NULL,
	[Step] [int] NULL,
	[FK_Flow] [nvarchar](10) NULL,
	[Name] [nvarchar](100) NULL,
	[DeliveryWay] [int] NULL,
	[DeliveryParas] [nvarchar](500) NULL,
	[WhoExeIt] [int] NULL,
	[TurnToDeal] [int] NULL,
	[TurnToDealDoc] [nvarchar](1000) NULL,
	[ReadReceipts] [int] NULL,
	[CondModel] [int] NULL,
	[CancelRole] [int] NULL,
	[BatchRole] [int] NULL,
	[BatchListCount] [int] NULL,
	[BatchParas] [nvarchar](300) NULL,
	[IsTask] [int] NULL,
	[IsRM] [int] NULL,
	[DTFrom] [nvarchar](50) NULL,
	[DTTo] [nvarchar](50) NULL,
	[FormType] [int] NULL,
	[NodeFrmID] [nvarchar](50) NULL,
	[FormUrl] [nvarchar](2000) NULL,
	[FocusField] [nvarchar](50) NULL,
	[SaveModel] [int] NULL,
	[RunModel] [int] NULL,
	[SubThreadType] [int] NULL,
	[PassRate] [float] NULL,
	[SubFlowStartWay] [int] NULL,
	[SubFlowStartParas] [nvarchar](100) NULL,
	[TodolistModel] [int] NULL,
	[BlockModel] [int] NULL,
	[BlockExp] [nvarchar](700) NULL,
	[BlockAlert] [nvarchar](700) NULL,
	[IsAllowRepeatEmps] [int] NULL,
	[IsGuestNode] [int] NULL,
	[AutoJumpRole0] [int] NULL,
	[AutoJumpRole1] [int] NULL,
	[AutoJumpRole2] [int] NULL,
	[WhenNoWorker] [int] NULL,
	[ThreadKillRole] [int] NULL,
	[JumpToNodes] [nvarchar](200) NULL,
	[IsBackTracking] [int] NULL,
	[CCWriteTo] [int] NULL,
	[WarningDays] [float] NULL,
	[DeductDays] [float] NULL,
	[DeductCent] [float] NULL,
	[MaxDeductCent] [float] NULL,
	[SwinkCent] [float] NULL,
	[OutTimeDeal] [int] NULL,
	[DoOutTime] [nvarchar](300) NULL,
	[DoOutTimeCond] [nvarchar](100) NULL,
	[CHWay] [int] NULL,
	[Workload] [float] NULL,
	[IsEval] [int] NULL,
	[FWCSta] [int] NULL,
	[FWCShowModel] [int] NULL,
	[FWCType] [int] NULL,
	[FWCNodeName] [nvarchar](100) NULL,
	[FWCTrackEnable] [int] NULL,
	[FWCListEnable] [int] NULL,
	[FWCIsShowAllStep] [int] NULL,
	[FWCOpLabel] [nvarchar](50) NULL,
	[FWCDefInfo] [nvarchar](50) NULL,
	[SigantureEnabel] [int] NULL,
	[FWCIsFullInfo] [int] NULL,
	[FWC_H] [float] NULL,
	[FWC_W] [float] NULL,
	[OfficeOverLab] [nvarchar](50) NULL,
	[MPhone_WorkModel] [int] NULL,
	[MPhone_SrcModel] [int] NULL,
	[MPad_WorkModel] [int] NULL,
	[MPad_SrcModel] [int] NULL,
	[SelectorDBShowWay] [int] NULL,
	[SelectorModel] [int] NULL,
	[SelectorP1] [nvarchar](4000) NULL,
	[SelectorP2] [nvarchar](4000) NULL,
	[CheckNodes] [nvarchar](800) NULL,
	[CCCtrlWay] [int] NULL,
	[CCSQL] [nvarchar](500) NULL,
	[CCTitle] [nvarchar](500) NULL,
	[CCDoc] [nvarchar](4000) NULL,
	[ICON] [nvarchar](50) NULL,
	[NodeWorkType] [int] NULL,
	[FlowName] [nvarchar](100) NULL,
	[FK_FlowSort] [nvarchar](4) NULL,
	[FK_FlowSortT] [nvarchar](100) NULL,
	[FrmAttr] [nvarchar](300) NULL,
	[Doc] [nvarchar](100) NULL,
	[IsCanRpt] [int] NULL,
	[IsCanOver] [int] NULL,
	[IsSecret] [int] NULL,
	[IsCanDelFlow] [int] NULL,
	[IsHandOver] [int] NULL,
	[NodePosType] [int] NULL,
	[IsCCFlow] [int] NULL,
	[HisStas] [nvarchar](4000) NULL,
	[HisDeptStrs] [nvarchar](4000) NULL,
	[HisToNDs] [nvarchar](100) NULL,
	[HisBillIDs] [nvarchar](200) NULL,
	[HisSubFlows] [nvarchar](50) NULL,
	[PTable] [nvarchar](100) NULL,
	[ShowSheets] [nvarchar](100) NULL,
	[GroupStaNDs] [nvarchar](200) NULL,
	[X] [int] NULL,
	[Y] [int] NULL,
	[AtPara] [nvarchar](500) NULL,
	[DocLeftWord] [nvarchar](200) NULL,
	[DocRightWord] [nvarchar](200) NULL,
	[FWC_X] [float] NULL,
	[FWC_Y] [float] NULL,
	[Tip] [nvarchar](100) NULL,
	[IsExpSender] [int] NULL,
	[FWCAth] [int] NULL,
 CONSTRAINT [WF_Nodepk] PRIMARY KEY CLUSTERED 
(
	[NodeID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[WF_Node] ([NodeID], [SendLab], [SendJS], [JumpWayLab], [JumpWay], [SaveLab], [SaveEnable], [ThreadLab], [ThreadEnable], [SubFlowLab], [SubFlowCtrlRole], [ReturnLab], [ReturnRole], [ReturnField], [CCLab], [CCRole], [ShiftLab], [ShiftEnable], [DelLab], [DelEnable], [EndFlowLab], [EndFlowEnable], [HungLab], [HungEnable], [PrintDocLab], [PrintDocEnable], [TrackLab], [TrackEnable], [SelectAccepterLab], [SelectAccepterEnable], [SearchLab], [SearchEnable], [WorkCheckLab], [WorkCheckEnable], [BatchLab], [BatchEnable], [AskforLab], [AskforEnable], [TCLab], [TCEnable], [WebOffice], [WebOfficeEnable], [OfficeOpenLab], [OfficeOpenEnable], [OfficeOpenTemplateLab], [OfficeOpenTemplateEnable], [OfficeSaveLab], [OfficeSaveEnable], [OfficeAcceptLab], [OfficeAcceptEnable], [OfficeRefuseLab], [OfficeRefuseEnable], [OfficeTHTemplate], [OfficeOverEnable], [OfficeMarksEnable], [OfficePrintLab], [OfficePrintEnable], [OfficeSealLab], [OfficeSealEnable], [OfficeInsertFlowLab], [OfficeInsertFlowEnable], [OfficeNodeInfo], [OfficeReSavePDF], [OfficeDownLab], [OfficeDownEnable], [OfficeIsMarks], [OfficeTemplate], [OfficeIsParent], [OfficeTHEnable], [Step], [FK_Flow], [Name], [DeliveryWay], [DeliveryParas], [WhoExeIt], [TurnToDeal], [TurnToDealDoc], [ReadReceipts], [CondModel], [CancelRole], [BatchRole], [BatchListCount], [BatchParas], [IsTask], [IsRM], [DTFrom], [DTTo], [FormType], [NodeFrmID], [FormUrl], [FocusField], [SaveModel], [RunModel], [SubThreadType], [PassRate], [SubFlowStartWay], [SubFlowStartParas], [TodolistModel], [BlockModel], [BlockExp], [BlockAlert], [IsAllowRepeatEmps], [IsGuestNode], [AutoJumpRole0], [AutoJumpRole1], [AutoJumpRole2], [WhenNoWorker], [ThreadKillRole], [JumpToNodes], [IsBackTracking], [CCWriteTo], [WarningDays], [DeductDays], [DeductCent], [MaxDeductCent], [SwinkCent], [OutTimeDeal], [DoOutTime], [DoOutTimeCond], [CHWay], [Workload], [IsEval], [FWCSta], [FWCShowModel], [FWCType], [FWCNodeName], [FWCTrackEnable], [FWCListEnable], [FWCIsShowAllStep], [FWCOpLabel], [FWCDefInfo], [SigantureEnabel], [FWCIsFullInfo], [FWC_H], [FWC_W], [OfficeOverLab], [MPhone_WorkModel], [MPhone_SrcModel], [MPad_WorkModel], [MPad_SrcModel], [SelectorDBShowWay], [SelectorModel], [SelectorP1], [SelectorP2], [CheckNodes], [CCCtrlWay], [CCSQL], [CCTitle], [CCDoc], [ICON], [NodeWorkType], [FlowName], [FK_FlowSort], [FK_FlowSortT], [FrmAttr], [Doc], [IsCanRpt], [IsCanOver], [IsSecret], [IsCanDelFlow], [IsHandOver], [NodePosType], [IsCCFlow], [HisStas], [HisDeptStrs], [HisToNDs], [HisBillIDs], [HisSubFlows], [PTable], [ShowSheets], [GroupStaNDs], [X], [Y], [AtPara], [DocLeftWord], [DocRightWord], [FWC_X], [FWC_Y], [Tip], [IsExpSender], [FWCAth]) VALUES (101, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, N'001', N'开始节点', 0, N'', 0, 0, N'', 0, 0, 0, 0, 12, N'', 1, 1, N'2015-05-11 09:45', N'2015-05-11 09:45', 1, N'', N'http://', N'', 0, 0, 0, 100, 0, N'', 0, 0, N'', N'', 0, 0, 0, 0, 0, 0, 0, N'', 0, 0, 0, 5, 2, 10, 0.1, 0, N'', N'', 0, 0, 0, 0, 1, 0, N'', 1, 1, 0, N'审核', N'同意', 0, 1, 300, 400, N'套红', 0, 0, 0, 0, 0, 0, N'', N'', N'', 0, N'', N'', N'', N'前台', 1, N'涉密人员入职', N'', N'', N'', N'', 1, 0, 0, 0, 0, 0, 0, N'', N'', N'', N'', N'', N'', N'', N'', 136, 103, N'', N'', N'', 5, 5, N'', 1, 0)
INSERT [dbo].[WF_Node] ([NodeID], [SendLab], [SendJS], [JumpWayLab], [JumpWay], [SaveLab], [SaveEnable], [ThreadLab], [ThreadEnable], [SubFlowLab], [SubFlowCtrlRole], [ReturnLab], [ReturnRole], [ReturnField], [CCLab], [CCRole], [ShiftLab], [ShiftEnable], [DelLab], [DelEnable], [EndFlowLab], [EndFlowEnable], [HungLab], [HungEnable], [PrintDocLab], [PrintDocEnable], [TrackLab], [TrackEnable], [SelectAccepterLab], [SelectAccepterEnable], [SearchLab], [SearchEnable], [WorkCheckLab], [WorkCheckEnable], [BatchLab], [BatchEnable], [AskforLab], [AskforEnable], [TCLab], [TCEnable], [WebOffice], [WebOfficeEnable], [OfficeOpenLab], [OfficeOpenEnable], [OfficeOpenTemplateLab], [OfficeOpenTemplateEnable], [OfficeSaveLab], [OfficeSaveEnable], [OfficeAcceptLab], [OfficeAcceptEnable], [OfficeRefuseLab], [OfficeRefuseEnable], [OfficeTHTemplate], [OfficeOverEnable], [OfficeMarksEnable], [OfficePrintLab], [OfficePrintEnable], [OfficeSealLab], [OfficeSealEnable], [OfficeInsertFlowLab], [OfficeInsertFlowEnable], [OfficeNodeInfo], [OfficeReSavePDF], [OfficeDownLab], [OfficeDownEnable], [OfficeIsMarks], [OfficeTemplate], [OfficeIsParent], [OfficeTHEnable], [Step], [FK_Flow], [Name], [DeliveryWay], [DeliveryParas], [WhoExeIt], [TurnToDeal], [TurnToDealDoc], [ReadReceipts], [CondModel], [CancelRole], [BatchRole], [BatchListCount], [BatchParas], [IsTask], [IsRM], [DTFrom], [DTTo], [FormType], [NodeFrmID], [FormUrl], [FocusField], [SaveModel], [RunModel], [SubThreadType], [PassRate], [SubFlowStartWay], [SubFlowStartParas], [TodolistModel], [BlockModel], [BlockExp], [BlockAlert], [IsAllowRepeatEmps], [IsGuestNode], [AutoJumpRole0], [AutoJumpRole1], [AutoJumpRole2], [WhenNoWorker], [ThreadKillRole], [JumpToNodes], [IsBackTracking], [CCWriteTo], [WarningDays], [DeductDays], [DeductCent], [MaxDeductCent], [SwinkCent], [OutTimeDeal], [DoOutTime], [DoOutTimeCond], [CHWay], [Workload], [IsEval], [FWCSta], [FWCShowModel], [FWCType], [FWCNodeName], [FWCTrackEnable], [FWCListEnable], [FWCIsShowAllStep], [FWCOpLabel], [FWCDefInfo], [SigantureEnabel], [FWCIsFullInfo], [FWC_H], [FWC_W], [OfficeOverLab], [MPhone_WorkModel], [MPhone_SrcModel], [MPad_WorkModel], [MPad_SrcModel], [SelectorDBShowWay], [SelectorModel], [SelectorP1], [SelectorP2], [CheckNodes], [CCCtrlWay], [CCSQL], [CCTitle], [CCDoc], [ICON], [NodeWorkType], [FlowName], [FK_FlowSort], [FK_FlowSortT], [FrmAttr], [Doc], [IsCanRpt], [IsCanOver], [IsSecret], [IsCanDelFlow], [IsHandOver], [NodePosType], [IsCCFlow], [HisStas], [HisDeptStrs], [HisToNDs], [HisBillIDs], [HisSubFlows], [PTable], [ShowSheets], [GroupStaNDs], [X], [Y], [AtPara], [DocLeftWord], [DocRightWord], [FWC_X], [FWC_Y], [Tip], [IsExpSender], [FWCAth]) VALUES (102, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, N'001', N'节点2', 0, N'', 0, 0, N'', 0, 0, 0, 0, 12, N'', 1, 1, N'2015-05-11 09:45', N'2015-05-11 09:45', 1, N'', N'http://', N'', 0, 0, 0, 100, 0, N'', 0, 0, N'', N'', 0, 0, 0, 0, 0, 0, 0, N'', 0, 0, 0, 5, 2, 10, 0.1, 0, N'', N'', 0, 0, 0, 0, 1, 0, N'', 1, 1, 0, N'审核', N'同意', 0, 1, 300, 400, N'套红', 0, 0, 0, 0, 0, 0, N'', N'', N'', 0, N'', N'', N'', N'审核', 0, N'涉密人员入职', N'', N'', N'', N'', 1, 0, 0, 0, 0, 1, 0, N'', N'', N'', N'', N'', N'', N'', N'', 222, 288, N'', N'', N'', 5, 5, N'', 1, 0)
INSERT [dbo].[WF_Node] ([NodeID], [SendLab], [SendJS], [JumpWayLab], [JumpWay], [SaveLab], [SaveEnable], [ThreadLab], [ThreadEnable], [SubFlowLab], [SubFlowCtrlRole], [ReturnLab], [ReturnRole], [ReturnField], [CCLab], [CCRole], [ShiftLab], [ShiftEnable], [DelLab], [DelEnable], [EndFlowLab], [EndFlowEnable], [HungLab], [HungEnable], [PrintDocLab], [PrintDocEnable], [TrackLab], [TrackEnable], [SelectAccepterLab], [SelectAccepterEnable], [SearchLab], [SearchEnable], [WorkCheckLab], [WorkCheckEnable], [BatchLab], [BatchEnable], [AskforLab], [AskforEnable], [TCLab], [TCEnable], [WebOffice], [WebOfficeEnable], [OfficeOpenLab], [OfficeOpenEnable], [OfficeOpenTemplateLab], [OfficeOpenTemplateEnable], [OfficeSaveLab], [OfficeSaveEnable], [OfficeAcceptLab], [OfficeAcceptEnable], [OfficeRefuseLab], [OfficeRefuseEnable], [OfficeTHTemplate], [OfficeOverEnable], [OfficeMarksEnable], [OfficePrintLab], [OfficePrintEnable], [OfficeSealLab], [OfficeSealEnable], [OfficeInsertFlowLab], [OfficeInsertFlowEnable], [OfficeNodeInfo], [OfficeReSavePDF], [OfficeDownLab], [OfficeDownEnable], [OfficeIsMarks], [OfficeTemplate], [OfficeIsParent], [OfficeTHEnable], [Step], [FK_Flow], [Name], [DeliveryWay], [DeliveryParas], [WhoExeIt], [TurnToDeal], [TurnToDealDoc], [ReadReceipts], [CondModel], [CancelRole], [BatchRole], [BatchListCount], [BatchParas], [IsTask], [IsRM], [DTFrom], [DTTo], [FormType], [NodeFrmID], [FormUrl], [FocusField], [SaveModel], [RunModel], [SubThreadType], [PassRate], [SubFlowStartWay], [SubFlowStartParas], [TodolistModel], [BlockModel], [BlockExp], [BlockAlert], [IsAllowRepeatEmps], [IsGuestNode], [AutoJumpRole0], [AutoJumpRole1], [AutoJumpRole2], [WhenNoWorker], [ThreadKillRole], [JumpToNodes], [IsBackTracking], [CCWriteTo], [WarningDays], [DeductDays], [DeductCent], [MaxDeductCent], [SwinkCent], [OutTimeDeal], [DoOutTime], [DoOutTimeCond], [CHWay], [Workload], [IsEval], [FWCSta], [FWCShowModel], [FWCType], [FWCNodeName], [FWCTrackEnable], [FWCListEnable], [FWCIsShowAllStep], [FWCOpLabel], [FWCDefInfo], [SigantureEnabel], [FWCIsFullInfo], [FWC_H], [FWC_W], [OfficeOverLab], [MPhone_WorkModel], [MPhone_SrcModel], [MPad_WorkModel], [MPad_SrcModel], [SelectorDBShowWay], [SelectorModel], [SelectorP1], [SelectorP2], [CheckNodes], [CCCtrlWay], [CCSQL], [CCTitle], [CCDoc], [ICON], [NodeWorkType], [FlowName], [FK_FlowSort], [FK_FlowSortT], [FrmAttr], [Doc], [IsCanRpt], [IsCanOver], [IsSecret], [IsCanDelFlow], [IsHandOver], [NodePosType], [IsCCFlow], [HisStas], [HisDeptStrs], [HisToNDs], [HisBillIDs], [HisSubFlows], [PTable], [ShowSheets], [GroupStaNDs], [X], [Y], [AtPara], [DocLeftWord], [DocRightWord], [FWC_X], [FWC_Y], [Tip], [IsExpSender], [FWCAth]) VALUES (201, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, N'002', N'开始节点', 0, N'', 0, 0, N'', 0, 0, 0, 0, 12, N'', 1, 1, N'2015-05-11 09:45', N'2015-05-11 09:45', 1, N'', N'http://', N'', 0, 0, 0, 100, 0, N'', 0, 0, N'', N'', 0, 0, 0, 0, 0, 0, 0, N'', 0, 0, 0, 5, 2, 10, 0.1, 0, N'', N'', 0, 0, 0, 0, 1, 0, N'', 1, 1, 0, N'审核', N'同意', 0, 1, 300, 400, N'套红', 0, 0, 0, 0, 0, 0, N'', N'', N'', 0, N'', N'', N'', N'前台', 1, N'密级变更', N'', N'', N'', N'', 1, 0, 0, 0, 0, 0, 0, N'', N'', N'', N'', N'', N'', N'', N'', 133, 117, N'', N'', N'', 5, 5, N'', 1, 0)
INSERT [dbo].[WF_Node] ([NodeID], [SendLab], [SendJS], [JumpWayLab], [JumpWay], [SaveLab], [SaveEnable], [ThreadLab], [ThreadEnable], [SubFlowLab], [SubFlowCtrlRole], [ReturnLab], [ReturnRole], [ReturnField], [CCLab], [CCRole], [ShiftLab], [ShiftEnable], [DelLab], [DelEnable], [EndFlowLab], [EndFlowEnable], [HungLab], [HungEnable], [PrintDocLab], [PrintDocEnable], [TrackLab], [TrackEnable], [SelectAccepterLab], [SelectAccepterEnable], [SearchLab], [SearchEnable], [WorkCheckLab], [WorkCheckEnable], [BatchLab], [BatchEnable], [AskforLab], [AskforEnable], [TCLab], [TCEnable], [WebOffice], [WebOfficeEnable], [OfficeOpenLab], [OfficeOpenEnable], [OfficeOpenTemplateLab], [OfficeOpenTemplateEnable], [OfficeSaveLab], [OfficeSaveEnable], [OfficeAcceptLab], [OfficeAcceptEnable], [OfficeRefuseLab], [OfficeRefuseEnable], [OfficeTHTemplate], [OfficeOverEnable], [OfficeMarksEnable], [OfficePrintLab], [OfficePrintEnable], [OfficeSealLab], [OfficeSealEnable], [OfficeInsertFlowLab], [OfficeInsertFlowEnable], [OfficeNodeInfo], [OfficeReSavePDF], [OfficeDownLab], [OfficeDownEnable], [OfficeIsMarks], [OfficeTemplate], [OfficeIsParent], [OfficeTHEnable], [Step], [FK_Flow], [Name], [DeliveryWay], [DeliveryParas], [WhoExeIt], [TurnToDeal], [TurnToDealDoc], [ReadReceipts], [CondModel], [CancelRole], [BatchRole], [BatchListCount], [BatchParas], [IsTask], [IsRM], [DTFrom], [DTTo], [FormType], [NodeFrmID], [FormUrl], [FocusField], [SaveModel], [RunModel], [SubThreadType], [PassRate], [SubFlowStartWay], [SubFlowStartParas], [TodolistModel], [BlockModel], [BlockExp], [BlockAlert], [IsAllowRepeatEmps], [IsGuestNode], [AutoJumpRole0], [AutoJumpRole1], [AutoJumpRole2], [WhenNoWorker], [ThreadKillRole], [JumpToNodes], [IsBackTracking], [CCWriteTo], [WarningDays], [DeductDays], [DeductCent], [MaxDeductCent], [SwinkCent], [OutTimeDeal], [DoOutTime], [DoOutTimeCond], [CHWay], [Workload], [IsEval], [FWCSta], [FWCShowModel], [FWCType], [FWCNodeName], [FWCTrackEnable], [FWCListEnable], [FWCIsShowAllStep], [FWCOpLabel], [FWCDefInfo], [SigantureEnabel], [FWCIsFullInfo], [FWC_H], [FWC_W], [OfficeOverLab], [MPhone_WorkModel], [MPhone_SrcModel], [MPad_WorkModel], [MPad_SrcModel], [SelectorDBShowWay], [SelectorModel], [SelectorP1], [SelectorP2], [CheckNodes], [CCCtrlWay], [CCSQL], [CCTitle], [CCDoc], [ICON], [NodeWorkType], [FlowName], [FK_FlowSort], [FK_FlowSortT], [FrmAttr], [Doc], [IsCanRpt], [IsCanOver], [IsSecret], [IsCanDelFlow], [IsHandOver], [NodePosType], [IsCCFlow], [HisStas], [HisDeptStrs], [HisToNDs], [HisBillIDs], [HisSubFlows], [PTable], [ShowSheets], [GroupStaNDs], [X], [Y], [AtPara], [DocLeftWord], [DocRightWord], [FWC_X], [FWC_Y], [Tip], [IsExpSender], [FWCAth]) VALUES (202, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, N'002', N'节点2', 0, N'', 0, 0, N'', 0, 0, 0, 0, 12, N'', 1, 1, N'2015-05-11 09:45', N'2015-05-11 09:45', 1, N'', N'http://', N'', 0, 0, 0, 100, 0, N'', 0, 0, N'', N'', 0, 0, 0, 0, 0, 0, 0, N'', 0, 0, 0, 5, 2, 10, 0.1, 0, N'', N'', 0, 0, 0, 0, 1, 0, N'', 1, 1, 0, N'审核', N'同意', 0, 1, 300, 400, N'套红', 0, 0, 0, 0, 0, 0, N'', N'', N'', 0, N'', N'', N'', N'审核', 0, N'密级变更', N'', N'', N'', N'', 1, 0, 0, 0, 0, 1, 0, N'', N'', N'', N'', N'', N'', N'', N'', 200, 250, N'', N'', N'', 5, 5, N'', 1, 0)
INSERT [dbo].[WF_Node] ([NodeID], [SendLab], [SendJS], [JumpWayLab], [JumpWay], [SaveLab], [SaveEnable], [ThreadLab], [ThreadEnable], [SubFlowLab], [SubFlowCtrlRole], [ReturnLab], [ReturnRole], [ReturnField], [CCLab], [CCRole], [ShiftLab], [ShiftEnable], [DelLab], [DelEnable], [EndFlowLab], [EndFlowEnable], [HungLab], [HungEnable], [PrintDocLab], [PrintDocEnable], [TrackLab], [TrackEnable], [SelectAccepterLab], [SelectAccepterEnable], [SearchLab], [SearchEnable], [WorkCheckLab], [WorkCheckEnable], [BatchLab], [BatchEnable], [AskforLab], [AskforEnable], [TCLab], [TCEnable], [WebOffice], [WebOfficeEnable], [OfficeOpenLab], [OfficeOpenEnable], [OfficeOpenTemplateLab], [OfficeOpenTemplateEnable], [OfficeSaveLab], [OfficeSaveEnable], [OfficeAcceptLab], [OfficeAcceptEnable], [OfficeRefuseLab], [OfficeRefuseEnable], [OfficeTHTemplate], [OfficeOverEnable], [OfficeMarksEnable], [OfficePrintLab], [OfficePrintEnable], [OfficeSealLab], [OfficeSealEnable], [OfficeInsertFlowLab], [OfficeInsertFlowEnable], [OfficeNodeInfo], [OfficeReSavePDF], [OfficeDownLab], [OfficeDownEnable], [OfficeIsMarks], [OfficeTemplate], [OfficeIsParent], [OfficeTHEnable], [Step], [FK_Flow], [Name], [DeliveryWay], [DeliveryParas], [WhoExeIt], [TurnToDeal], [TurnToDealDoc], [ReadReceipts], [CondModel], [CancelRole], [BatchRole], [BatchListCount], [BatchParas], [IsTask], [IsRM], [DTFrom], [DTTo], [FormType], [NodeFrmID], [FormUrl], [FocusField], [SaveModel], [RunModel], [SubThreadType], [PassRate], [SubFlowStartWay], [SubFlowStartParas], [TodolistModel], [BlockModel], [BlockExp], [BlockAlert], [IsAllowRepeatEmps], [IsGuestNode], [AutoJumpRole0], [AutoJumpRole1], [AutoJumpRole2], [WhenNoWorker], [ThreadKillRole], [JumpToNodes], [IsBackTracking], [CCWriteTo], [WarningDays], [DeductDays], [DeductCent], [MaxDeductCent], [SwinkCent], [OutTimeDeal], [DoOutTime], [DoOutTimeCond], [CHWay], [Workload], [IsEval], [FWCSta], [FWCShowModel], [FWCType], [FWCNodeName], [FWCTrackEnable], [FWCListEnable], [FWCIsShowAllStep], [FWCOpLabel], [FWCDefInfo], [SigantureEnabel], [FWCIsFullInfo], [FWC_H], [FWC_W], [OfficeOverLab], [MPhone_WorkModel], [MPhone_SrcModel], [MPad_WorkModel], [MPad_SrcModel], [SelectorDBShowWay], [SelectorModel], [SelectorP1], [SelectorP2], [CheckNodes], [CCCtrlWay], [CCSQL], [CCTitle], [CCDoc], [ICON], [NodeWorkType], [FlowName], [FK_FlowSort], [FK_FlowSortT], [FrmAttr], [Doc], [IsCanRpt], [IsCanOver], [IsSecret], [IsCanDelFlow], [IsHandOver], [NodePosType], [IsCCFlow], [HisStas], [HisDeptStrs], [HisToNDs], [HisBillIDs], [HisSubFlows], [PTable], [ShowSheets], [GroupStaNDs], [X], [Y], [AtPara], [DocLeftWord], [DocRightWord], [FWC_X], [FWC_Y], [Tip], [IsExpSender], [FWCAth]) VALUES (301, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, N'003', N'开始节点', 0, N'', 0, 0, N'', 0, 0, 0, 0, 12, N'', 1, 1, N'2015-05-11 09:45', N'2015-05-11 09:45', 1, N'', N'http://', N'', 0, 0, 0, 100, 0, N'', 0, 0, N'', N'', 0, 0, 0, 0, 0, 0, 0, N'', 0, 0, 0, 5, 2, 10, 0.1, 0, N'', N'', 0, 0, 0, 0, 1, 0, N'', 1, 1, 0, N'审核', N'同意', 0, 1, 300, 400, N'套红', 0, 0, 0, 0, 0, 0, N'', N'', N'', 0, N'', N'', N'', N'前台', 1, N'涉密人员注销', N'', N'', N'', N'', 1, 0, 0, 0, 0, 0, 0, N'', N'', N'', N'', N'', N'', N'', N'', 133, 104, N'', N'', N'', 5, 5, N'', 1, 0)
INSERT [dbo].[WF_Node] ([NodeID], [SendLab], [SendJS], [JumpWayLab], [JumpWay], [SaveLab], [SaveEnable], [ThreadLab], [ThreadEnable], [SubFlowLab], [SubFlowCtrlRole], [ReturnLab], [ReturnRole], [ReturnField], [CCLab], [CCRole], [ShiftLab], [ShiftEnable], [DelLab], [DelEnable], [EndFlowLab], [EndFlowEnable], [HungLab], [HungEnable], [PrintDocLab], [PrintDocEnable], [TrackLab], [TrackEnable], [SelectAccepterLab], [SelectAccepterEnable], [SearchLab], [SearchEnable], [WorkCheckLab], [WorkCheckEnable], [BatchLab], [BatchEnable], [AskforLab], [AskforEnable], [TCLab], [TCEnable], [WebOffice], [WebOfficeEnable], [OfficeOpenLab], [OfficeOpenEnable], [OfficeOpenTemplateLab], [OfficeOpenTemplateEnable], [OfficeSaveLab], [OfficeSaveEnable], [OfficeAcceptLab], [OfficeAcceptEnable], [OfficeRefuseLab], [OfficeRefuseEnable], [OfficeTHTemplate], [OfficeOverEnable], [OfficeMarksEnable], [OfficePrintLab], [OfficePrintEnable], [OfficeSealLab], [OfficeSealEnable], [OfficeInsertFlowLab], [OfficeInsertFlowEnable], [OfficeNodeInfo], [OfficeReSavePDF], [OfficeDownLab], [OfficeDownEnable], [OfficeIsMarks], [OfficeTemplate], [OfficeIsParent], [OfficeTHEnable], [Step], [FK_Flow], [Name], [DeliveryWay], [DeliveryParas], [WhoExeIt], [TurnToDeal], [TurnToDealDoc], [ReadReceipts], [CondModel], [CancelRole], [BatchRole], [BatchListCount], [BatchParas], [IsTask], [IsRM], [DTFrom], [DTTo], [FormType], [NodeFrmID], [FormUrl], [FocusField], [SaveModel], [RunModel], [SubThreadType], [PassRate], [SubFlowStartWay], [SubFlowStartParas], [TodolistModel], [BlockModel], [BlockExp], [BlockAlert], [IsAllowRepeatEmps], [IsGuestNode], [AutoJumpRole0], [AutoJumpRole1], [AutoJumpRole2], [WhenNoWorker], [ThreadKillRole], [JumpToNodes], [IsBackTracking], [CCWriteTo], [WarningDays], [DeductDays], [DeductCent], [MaxDeductCent], [SwinkCent], [OutTimeDeal], [DoOutTime], [DoOutTimeCond], [CHWay], [Workload], [IsEval], [FWCSta], [FWCShowModel], [FWCType], [FWCNodeName], [FWCTrackEnable], [FWCListEnable], [FWCIsShowAllStep], [FWCOpLabel], [FWCDefInfo], [SigantureEnabel], [FWCIsFullInfo], [FWC_H], [FWC_W], [OfficeOverLab], [MPhone_WorkModel], [MPhone_SrcModel], [MPad_WorkModel], [MPad_SrcModel], [SelectorDBShowWay], [SelectorModel], [SelectorP1], [SelectorP2], [CheckNodes], [CCCtrlWay], [CCSQL], [CCTitle], [CCDoc], [ICON], [NodeWorkType], [FlowName], [FK_FlowSort], [FK_FlowSortT], [FrmAttr], [Doc], [IsCanRpt], [IsCanOver], [IsSecret], [IsCanDelFlow], [IsHandOver], [NodePosType], [IsCCFlow], [HisStas], [HisDeptStrs], [HisToNDs], [HisBillIDs], [HisSubFlows], [PTable], [ShowSheets], [GroupStaNDs], [X], [Y], [AtPara], [DocLeftWord], [DocRightWord], [FWC_X], [FWC_Y], [Tip], [IsExpSender], [FWCAth]) VALUES (302, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, N'003', N'节点2', 0, N'', 0, 0, N'', 0, 0, 0, 0, 12, N'', 1, 1, N'2015-05-11 09:45', N'2015-05-11 09:45', 1, N'', N'http://', N'', 0, 0, 0, 100, 0, N'', 0, 0, N'', N'', 0, 0, 0, 0, 0, 0, 0, N'', 0, 0, 0, 5, 2, 10, 0.1, 0, N'', N'', 0, 0, 0, 0, 1, 0, N'', 1, 1, 0, N'审核', N'同意', 0, 1, 300, 400, N'套红', 0, 0, 0, 0, 0, 0, N'', N'', N'', 0, N'', N'', N'', N'审核', 0, N'涉密人员注销', N'', N'', N'', N'', 1, 0, 0, 0, 0, 1, 0, N'', N'', N'', N'', N'', N'', N'', N'', 210, 253, N'', N'', N'', 5, 5, N'', 1, 0)
/****** Object:  Table [dbo].[WF_Listen]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WF_Listen](
	[OID] [int] NOT NULL,
	[FK_Node] [int] NULL,
	[Nodes] [nvarchar](400) NULL,
	[NodesDesc] [nvarchar](400) NULL,
	[Title] [nvarchar](400) NULL,
	[Doc] [nvarchar](4000) NULL,
 CONSTRAINT [WF_Listenpk] PRIMARY KEY CLUSTERED 
(
	[OID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[WF_LabNote]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WF_LabNote](
	[MyPK] [nvarchar](100) NOT NULL,
	[Name] [nvarchar](3000) NULL,
	[FK_Flow] [nvarchar](100) NULL,
	[X] [int] NULL,
	[Y] [int] NULL,
 CONSTRAINT [WF_LabNotepk] PRIMARY KEY CLUSTERED 
(
	[MyPK] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[WF_HungUp]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WF_HungUp](
	[MyPK] [nvarchar](100) NOT NULL,
	[FK_Node] [int] NULL,
	[WorkID] [int] NULL,
	[HungUpWay] [int] NULL,
	[Note] [nvarchar](4000) NULL,
	[Rec] [nvarchar](50) NULL,
	[DTOfHungUp] [nvarchar](50) NULL,
	[DTOfUnHungUp] [nvarchar](50) NULL,
	[DTOfUnHungUpPlan] [nvarchar](50) NULL,
 CONSTRAINT [WF_HungUppk] PRIMARY KEY CLUSTERED 
(
	[MyPK] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[WF_GenerWorkFlow]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WF_GenerWorkFlow](
	[WorkID] [int] NOT NULL,
	[FID] [int] NULL,
	[FK_FlowSort] [nvarchar](100) NULL,
	[FK_Flow] [nvarchar](100) NULL,
	[FK_Dept] [nvarchar](100) NULL,
	[StarterName] [nvarchar](200) NULL,
	[BillNo] [nvarchar](100) NULL,
	[Title] [nvarchar](100) NULL,
	[WFSta] [int] NULL,
	[NodeName] [nvarchar](100) NULL,
	[RDT] [nvarchar](50) NULL,
	[FlowNote] [nvarchar](4000) NULL,
	[MyNum] [int] NULL,
	[FlowName] [nvarchar](100) NULL,
	[WFState] [int] NULL,
	[Starter] [nvarchar](200) NULL,
	[Sender] [nvarchar](200) NULL,
	[FK_Node] [int] NULL,
	[DeptName] [nvarchar](100) NULL,
	[PRI] [int] NULL,
	[SDTOfNode] [nvarchar](50) NULL,
	[SDTOfFlow] [nvarchar](50) NULL,
	[PFlowNo] [nvarchar](3) NULL,
	[PWorkID] [int] NULL,
	[PNodeID] [int] NULL,
	[PFID] [int] NULL,
	[PEmp] [nvarchar](32) NULL,
	[CFlowNo] [nvarchar](3) NULL,
	[CWorkID] [int] NULL,
	[GuestNo] [nvarchar](100) NULL,
	[GuestName] [nvarchar](100) NULL,
	[TodoEmps] [nvarchar](4000) NULL,
	[TodoEmpsNum] [int] NULL,
	[TaskSta] [int] NULL,
	[AtPara] [nvarchar](2000) NULL,
	[Emps] [nvarchar](4000) NULL,
	[GUID] [nvarchar](36) NULL,
	[FK_NY] [nvarchar](7) NULL,
 CONSTRAINT [WF_GenerWorkFlowpk] PRIMARY KEY CLUSTERED 
(
	[WorkID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[WF_GenerWorkerlist]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WF_GenerWorkerlist](
	[WorkID] [int] NOT NULL,
	[FK_Emp] [nvarchar](20) NOT NULL,
	[FK_Node] [int] NOT NULL,
	[FID] [int] NULL,
	[FK_EmpText] [nvarchar](30) NULL,
	[FK_NodeText] [nvarchar](100) NULL,
	[FK_Flow] [nvarchar](3) NULL,
	[FK_Dept] [nvarchar](100) NULL,
	[SDT] [nvarchar](50) NULL,
	[DTOfWarning] [nvarchar](50) NULL,
	[WarningDays] [float] NULL,
	[RDT] [nvarchar](50) NULL,
	[CDT] [nvarchar](50) NULL,
	[IsEnable] [int] NULL,
	[IsRead] [int] NULL,
	[IsPass] [int] NULL,
	[WhoExeIt] [int] NULL,
	[Sender] [nvarchar](200) NULL,
	[PRI] [int] NULL,
	[PressTimes] [int] NULL,
	[DTOfHungUp] [nvarchar](50) NULL,
	[DTOfUnHungUp] [nvarchar](50) NULL,
	[HungUpTimes] [int] NULL,
	[GuestNo] [nvarchar](30) NULL,
	[GuestName] [nvarchar](100) NULL,
	[AtPara] [nvarchar](4000) NULL,
 CONSTRAINT [WF_GenerWorkerlistpk] PRIMARY KEY CLUSTERED 
(
	[WorkID] ASC,
	[FK_Emp] ASC,
	[FK_Node] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[WF_GenerFH]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WF_GenerFH](
	[FID] [int] NOT NULL,
	[Title] [nvarchar](4000) NULL,
	[GroupKey] [nvarchar](3000) NULL,
	[FK_Flow] [nvarchar](500) NULL,
	[ToEmpsMsg] [nvarchar](4000) NULL,
	[FK_Node] [int] NULL,
	[WFState] [int] NULL,
	[RDT] [nvarchar](50) NULL,
 CONSTRAINT [WF_GenerFHpk] PRIMARY KEY CLUSTERED 
(
	[FID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[WF_FrmNode]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WF_FrmNode](
	[MyPK] [nvarchar](100) NOT NULL,
	[FK_Frm] [nvarchar](32) NULL,
	[FK_Node] [int] NULL,
	[FK_Flow] [nvarchar](20) NULL,
	[OfficeOpenLab] [nvarchar](50) NULL,
	[OfficeOpenEnable] [int] NULL,
	[OfficeOpenTemplateLab] [nvarchar](50) NULL,
	[OfficeOpenTemplateEnable] [int] NULL,
	[OfficeSaveLab] [nvarchar](50) NULL,
	[OfficeSaveEnable] [int] NULL,
	[OfficeAcceptLab] [nvarchar](50) NULL,
	[OfficeAcceptEnable] [int] NULL,
	[OfficeRefuseLab] [nvarchar](50) NULL,
	[OfficeRefuseEnable] [int] NULL,
	[OfficeOverLab] [nvarchar](50) NULL,
	[OfficeOverEnable] [int] NULL,
	[OfficeMarksEnable] [int] NULL,
	[OfficePrintLab] [nvarchar](50) NULL,
	[OfficePrintEnable] [int] NULL,
	[OfficeSealLab] [nvarchar](50) NULL,
	[OfficeSealEnable] [int] NULL,
	[OfficeInsertFlowLab] [nvarchar](50) NULL,
	[OfficeInsertFlowEnable] [int] NULL,
	[OfficeNodeInfo] [int] NULL,
	[OfficeReSavePDF] [int] NULL,
	[OfficeDownLab] [nvarchar](50) NULL,
	[OfficeDownEnable] [int] NULL,
	[OfficeIsMarks] [int] NULL,
	[OfficeTemplate] [nvarchar](100) NULL,
	[OfficeIsParent] [int] NULL,
	[OfficeTHEnable] [int] NULL,
	[OfficeTHTemplate] [nvarchar](200) NULL,
	[FrmType] [nvarchar](20) NULL,
	[IsEdit] [int] NULL,
	[IsPrint] [int] NULL,
	[IsEnableLoadData] [int] NULL,
	[Idx] [int] NULL,
	[FrmSln] [int] NULL,
	[WhoIsPK] [int] NULL,
 CONSTRAINT [WF_FrmNodepk] PRIMARY KEY CLUSTERED 
(
	[MyPK] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[WF_FlowSort]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WF_FlowSort](
	[No] [nvarchar](10) NOT NULL,
	[Name] [nvarchar](100) NULL,
	[ParentNo] [nvarchar](100) NULL,
	[TreeNo] [nvarchar](100) NULL,
	[Idx] [int] NULL,
	[IsDir] [int] NULL,
 CONSTRAINT [WF_FlowSortpk] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[WF_FlowSort] ([No], [Name], [ParentNo], [TreeNo], [Idx], [IsDir]) VALUES (N'02', N'其他类', N'99', N'', 0, 0)
INSERT [dbo].[WF_FlowSort] ([No], [Name], [ParentNo], [TreeNo], [Idx], [IsDir]) VALUES (N'99', N'流程树', N'0', N'', 0, 0)
/****** Object:  Table [dbo].[WF_FlowNode]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WF_FlowNode](
	[FK_Flow] [nvarchar](20) NOT NULL,
	[FK_Node] [nvarchar](20) NOT NULL,
 CONSTRAINT [WF_FlowNodepk] PRIMARY KEY CLUSTERED 
(
	[FK_Flow] ASC,
	[FK_Node] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[WF_FlowFormTree]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WF_FlowFormTree](
	[No] [nvarchar](10) NOT NULL,
	[Name] [nvarchar](100) NULL,
	[ParentNo] [nvarchar](100) NULL,
	[TreeNo] [nvarchar](100) NULL,
	[Idx] [int] NULL,
	[IsDir] [int] NULL,
	[FK_Flow] [nvarchar](20) NULL,
 CONSTRAINT [WF_FlowFormTreepk] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[WF_FlowEmp]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WF_FlowEmp](
	[FK_Flow] [nvarchar](100) NOT NULL,
	[FK_Emp] [nvarchar](100) NOT NULL,
 CONSTRAINT [WF_FlowEmppk] PRIMARY KEY CLUSTERED 
(
	[FK_Flow] ASC,
	[FK_Emp] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[WF_Flow]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WF_Flow](
	[No] [nvarchar](10) NOT NULL,
	[Name] [nvarchar](500) NULL,
	[FK_FlowSort] [nvarchar](100) NULL,
	[FlowRunWay] [int] NULL,
	[RunObj] [nvarchar](max) NULL,
	[Note] [nvarchar](100) NULL,
	[RunSQL] [nvarchar](2000) NULL,
	[NumOfBill] [int] NULL,
	[NumOfDtl] [int] NULL,
	[FlowAppType] [int] NULL,
	[ChartType] [int] NULL,
	[IsCanStart] [int] NULL,
	[AvgDay] [float] NULL,
	[IsFullSA] [int] NULL,
	[IsMD5] [int] NULL,
	[Idx] [int] NULL,
	[TimelineRole] [int] NULL,
	[Paras] [nvarchar](400) NULL,
	[PTable] [nvarchar](30) NULL,
	[DataStoreModel] [int] NULL,
	[TitleRole] [nvarchar](150) NULL,
	[FlowMark] [nvarchar](150) NULL,
	[FlowEventEntity] [nvarchar](150) NULL,
	[HistoryFields] [nvarchar](500) NULL,
	[IsGuestFlow] [int] NULL,
	[BillNoFormat] [nvarchar](50) NULL,
	[FlowNoteExp] [nvarchar](500) NULL,
	[DRCtrlType] [int] NULL,
	[StartLimitRole] [int] NULL,
	[StartLimitPara] [nvarchar](500) NULL,
	[StartLimitAlert] [nvarchar](max) NULL,
	[StartLimitWhen] [int] NULL,
	[StartGuideWay] [int] NULL,
	[StartGuidePara1] [nvarchar](max) NULL,
	[StartGuidePara2] [nvarchar](max) NULL,
	[StartGuidePara3] [nvarchar](max) NULL,
	[IsResetData] [int] NULL,
	[IsLoadPriData] [int] NULL,
	[CFlowWay] [int] NULL,
	[CFlowPara] [nvarchar](max) NULL,
	[IsBatchStart] [int] NULL,
	[BatchStartFields] [nvarchar](500) NULL,
	[IsAutoSendSubFlowOver] [int] NULL,
	[Ver] [nvarchar](20) NULL,
	[DesignerNo] [nvarchar](32) NULL,
	[DesignerName] [nvarchar](100) NULL,
	[Draft] [int] NULL,
	[DTSWay] [int] NULL,
	[DTSBTable] [nvarchar](200) NULL,
	[DTSBTablePK] [nvarchar](32) NULL,
	[DTSTime] [int] NULL,
	[DTSSpecNodes] [nvarchar](200) NULL,
	[DTSField] [int] NULL,
	[DTSFields] [nvarchar](2000) NULL,
 CONSTRAINT [WF_Flowpk] PRIMARY KEY CLUSTERED 
(
	[No] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[WF_Flow] ([No], [Name], [FK_FlowSort], [FlowRunWay], [RunObj], [Note], [RunSQL], [NumOfBill], [NumOfDtl], [FlowAppType], [ChartType], [IsCanStart], [AvgDay], [IsFullSA], [IsMD5], [Idx], [TimelineRole], [Paras], [PTable], [DataStoreModel], [TitleRole], [FlowMark], [FlowEventEntity], [HistoryFields], [IsGuestFlow], [BillNoFormat], [FlowNoteExp], [DRCtrlType], [StartLimitRole], [StartLimitPara], [StartLimitAlert], [StartLimitWhen], [StartGuideWay], [StartGuidePara1], [StartGuidePara2], [StartGuidePara3], [IsResetData], [IsLoadPriData], [CFlowWay], [CFlowPara], [IsBatchStart], [BatchStartFields], [IsAutoSendSubFlowOver], [Ver], [DesignerNo], [DesignerName], [Draft], [DTSWay], [DTSBTable], [DTSBTablePK], [DTSTime], [DTSSpecNodes], [DTSField], [DTSFields]) VALUES (N'001', N'涉密人员入职', N'02', 0, N'', N'', N'', 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, N'@StartNodeX=200@StartNodeY=50@EndNodeX=200@EndNodeY=350', N'', 0, N'', N'EmpNew', N'', N'', 0, N'', N'', 0, 0, N'', N'', 0, 0, N'', N'', N'', 0, 0, 0, N'', 0, N'', 0, N'2015-07-04 15:16:48', N'', N'', 0, 0, N'', N'', 0, N'', 0, N'')
INSERT [dbo].[WF_Flow] ([No], [Name], [FK_FlowSort], [FlowRunWay], [RunObj], [Note], [RunSQL], [NumOfBill], [NumOfDtl], [FlowAppType], [ChartType], [IsCanStart], [AvgDay], [IsFullSA], [IsMD5], [Idx], [TimelineRole], [Paras], [PTable], [DataStoreModel], [TitleRole], [FlowMark], [FlowEventEntity], [HistoryFields], [IsGuestFlow], [BillNoFormat], [FlowNoteExp], [DRCtrlType], [StartLimitRole], [StartLimitPara], [StartLimitAlert], [StartLimitWhen], [StartGuideWay], [StartGuidePara1], [StartGuidePara2], [StartGuidePara3], [IsResetData], [IsLoadPriData], [CFlowWay], [CFlowPara], [IsBatchStart], [BatchStartFields], [IsAutoSendSubFlowOver], [Ver], [DesignerNo], [DesignerName], [Draft], [DTSWay], [DTSBTable], [DTSBTablePK], [DTSTime], [DTSSpecNodes], [DTSField], [DTSFields]) VALUES (N'002', N'密级变更', N'02', 0, N'', N'', N'', 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, N'@StartNodeX=200@StartNodeY=50@EndNodeX=200@EndNodeY=350', N'', 0, N'', N'EmpMJ', N'', N'', 0, N'', N'', 0, 0, N'', N'', 0, 0, N'', N'', N'', 0, 0, 0, N'', 0, N'', 0, N'2015-07-04 15:18:16', N'', N'', 0, 0, N'', N'', 0, N'', 0, N'')
INSERT [dbo].[WF_Flow] ([No], [Name], [FK_FlowSort], [FlowRunWay], [RunObj], [Note], [RunSQL], [NumOfBill], [NumOfDtl], [FlowAppType], [ChartType], [IsCanStart], [AvgDay], [IsFullSA], [IsMD5], [Idx], [TimelineRole], [Paras], [PTable], [DataStoreModel], [TitleRole], [FlowMark], [FlowEventEntity], [HistoryFields], [IsGuestFlow], [BillNoFormat], [FlowNoteExp], [DRCtrlType], [StartLimitRole], [StartLimitPara], [StartLimitAlert], [StartLimitWhen], [StartGuideWay], [StartGuidePara1], [StartGuidePara2], [StartGuidePara3], [IsResetData], [IsLoadPriData], [CFlowWay], [CFlowPara], [IsBatchStart], [BatchStartFields], [IsAutoSendSubFlowOver], [Ver], [DesignerNo], [DesignerName], [Draft], [DTSWay], [DTSBTable], [DTSBTablePK], [DTSTime], [DTSSpecNodes], [DTSField], [DTSFields]) VALUES (N'003', N'涉密人员注销', N'02', 0, N'', N'', N'', 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, N'@StartNodeX=200@StartNodeY=50@EndNodeX=200@EndNodeY=350', N'', 0, N'', N'EmpZX', N'', N'', 0, N'', N'', 0, 0, N'', N'', 0, 0, N'', N'', N'', 0, 0, 0, N'', 0, N'', 0, N'2015-07-04 15:18:12', N'', N'', 0, 0, N'', N'', 0, N'', 0, N'')
/****** Object:  Table [dbo].[WF_FindWorkerRole]    Script Date: 07/06/2015 09:28:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WF_FindWorkerRole](
	[OID] [int] NOT NULL,
	[Name] [nvarchar](200) NULL,
	[FK_Node] [int] NULL,
	[SortVal0] [nvarchar](200) NULL,
	[SortText0] [nvarchar](200) NULL,
	[SortVal1] [nvarchar](200) NULL,
	[SortText1] [nvarchar](200) NULL,
	[SortVal2] [nvarchar](200) NULL,
	[SortText2] [nvarchar](200) NULL,
	[SortVal3] [nvarchar](200) NULL,
	[SortText3] [nvarchar](200) NULL,
	[TagVal0] [nvarchar](1000) NULL,
	[TagVal1] [nvarchar](1000) NULL,
	[TagVal2] [nvarchar](1000) NULL,
	[TagVal3] [nvarchar](1000) NULL,
	[TagText0] [nvarchar](1000) NULL,
	[TagText1] [nvarchar](1000) NULL,
	[TagText2] [nvarchar](1000) NULL,
	[TagText3] [nvarchar](1000) NULL,
	[IsEnable] [int] NULL,
	[Idx] [int] NULL,
 CONSTRAINT [WF_FindWorkerRolepk] PRIMARY KEY CLUSTERED 
(
	[OID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[WF_EmpWorks]    Script Date: 07/06/2015 09:28:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW  [dbo].[WF_EmpWorks]
(
PRI,
WorkID,
IsRead,
Starter,
StarterName,
WFState, 
FK_Dept,
DeptName,
FK_Flow,
FlowName,
PWorkID,
PFlowNo,
FK_Node,
NodeName,
WorkerDept,
Title,
RDT,
ADT,
SDT,
FK_Emp,
FID,
FK_FlowSort,
SDTOfNode,
PressTimes,
GuestNo,
GuestName,
BillNo,
FlowNote,
TodoEmps,
TodoEmpsNum,
TaskSta,
ListType,
Sender,
AtPara,
MyNum
)
AS

SELECT A.PRI,A.WorkID,B.IsRead, A.Starter,
A.StarterName,
A.WFState,
A.FK_Dept,A.DeptName, A.FK_Flow, A.FlowName,A.PWorkID,
A.PFlowNo,
B.FK_Node, B.FK_NodeText AS NodeName, B.FK_Dept WorkerDept, A.Title, A.RDT, B.RDT AS ADT, 
B.SDT, B.FK_Emp,B.FID ,A.FK_FlowSort,A.SDTOfNode,B.PressTimes,
A.GuestNo,
A.GuestName,
A.BillNo,
A.FlowNote,
A.TodoEmps,
A.TodoEmpsNum,
A.TaskSta,
0 as ListType,
A.Sender,
B.AtPara+''+A.AtPara,
1 as MyNum
FROM  WF_GenerWorkFlow A, WF_GenerWorkerlist B
WHERE     (B.IsEnable = 1) AND (B.IsPass = 0)
 AND A.WorkID = B.WorkID AND A.FK_Node = B.FK_Node 
 UNION
SELECT A.PRI,A.WorkID,B.Sta AS IsRead, A.Starter,
A.StarterName,
2 AS WFState,
A.FK_Dept,A.DeptName, A.FK_Flow, A.FlowName,A.PWorkID,
A.PFlowNo,
B.FK_Node, B.NodeName,  B.CCToDept as WorkerDept,A.Title, A.RDT, B.RDT AS ADT, 
B.RDT AS SDT, B.CCTo as FK_Emp,B.FID ,A.FK_FlowSort,A.SDTOfNode, 0 as PressTimes,
A.GuestNo,
A.GuestName,
A.BillNo,
A.FlowNote,
A.TodoEmps,
A.TodoEmpsNum,
0 AS TaskSta,
1 as ListType,
B.Rec as Sender,
'@IsCC=1'+A.AtPara as AtPara,
1 as MyNum
  FROM WF_GenerWorkFlow A, WF_CCList B WHERE A.WorkID=B.WorkID AND  B.Sta <=1
GO
/****** Object:  View [dbo].[V_GPM_EmpStationMenu]    Script Date: 07/06/2015 09:28:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[V_GPM_EmpStationMenu]
AS
SELECT b.FK_Station,a.FK_Emp,b.FK_Menu,b.IsChecked FROM Port_EmpStation a,GPM_StationMenu b
WHERE a.FK_Station = b.FK_Station
GO
/****** Object:  View [dbo].[V_GPM_EmpGroup]    Script Date: 07/06/2015 09:28:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[V_GPM_EmpGroup]
AS
SELECT FK_Group,FK_Emp FROM GPM_GroupEmp
UNION
SELECT a.FK_Group,B.FK_Emp FROM GPM_GroupStation a, Port_DeptEmpStation b 
WHERE a.FK_Station=b.FK_Station
GO
/****** Object:  View [dbo].[V_FlowStarterBPM]    Script Date: 07/06/2015 09:28:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[V_FlowStarterBPM] (FK_Flow,FlowName,FK_Emp)
AS
SELECT A.FK_Flow, a.FlowName, C.FK_Emp FROM WF_Node a, WF_NodeStation b, Port_DeptEmpStation c 
 WHERE a.NodePosType=0 AND ( a.WhoExeIt=0 OR a.WhoExeIt=2 ) 
AND  a.NodeID=b.FK_Node AND B.FK_Station=C.FK_Station   AND A.DeliveryWay=0
UNION 
SELECT A.FK_Flow, a.FlowName, C.FK_Emp FROM WF_Node a, WF_NodeDept b, Port_DeptEmp c 
 WHERE a.NodePosType=0 AND ( a.WhoExeIt=0 OR a.WhoExeIt=2 ) 
AND  a.NodeID=b.FK_Node AND B.FK_Dept=C.FK_Dept   AND A.DeliveryWay=1 
UNION 
SELECT A.FK_Flow, a.FlowName, B.FK_Emp FROM WF_Node A, WF_NodeEmp B 
 WHERE A.NodePosType=0 AND ( A.WhoExeIt=0 OR A.WhoExeIt=2 ) 
AND A.NodeID=B.FK_Node  AND A.DeliveryWay=3
GO
/****** Object:  View [dbo].[V_FlowStarter]    Script Date: 07/06/2015 09:28:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[V_FlowStarter] (FK_Flow,FlowName,FK_Emp)
AS
SELECT A.FK_Flow, a.FlowName, C.FK_Emp FROM WF_Node a, WF_NodeStation b, Port_EmpStation c 
 WHERE a.NodePosType=0 AND ( a.WhoExeIt=0 OR a.WhoExeIt=2 ) 
AND  a.NodeID=b.FK_Node AND B.FK_Station=C.FK_Station   AND A.DeliveryWay=0
UNION 
SELECT A.FK_Flow, a.FlowName, C.FK_Emp FROM WF_Node a, WF_NodeDept b, Port_EmpDept c 
 WHERE a.NodePosType=0 AND ( a.WhoExeIt=0 OR a.WhoExeIt=2 ) 
AND  a.NodeID=b.FK_Node AND B.FK_Dept=C.FK_Dept   AND A.DeliveryWay=1 
UNION 
SELECT A.FK_Flow, a.FlowName, B.FK_Emp FROM WF_Node A, WF_NodeEmp B 
 WHERE A.NodePosType=0 AND ( A.WhoExeIt=0 OR A.WhoExeIt=2 ) 
AND A.NodeID=B.FK_Node  AND A.DeliveryWay=3
GO
/****** Object:  StoredProcedure [dbo].[SYST_pGetSingleRow]    Script Date: 07/06/2015 09:28:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SYST_pGetSingleRow] 

  @Sql nvarchar(2000),

  @Top int,

  @Order nvarchar(500),

  @nPos int

AS

BEGIN

  exec dbo.SYST_pGetScopeRows @Sql,@Top,@Order,@nPos,@nPos;

END
GO
/****** Object:  StoredProcedure [dbo].[SYST_pGetPageRows]    Script Date: 07/06/2015 09:28:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SYST_pGetPageRows] 


  @Sql nvarchar(2000),

  @Top int,

  @Order nvarchar(500),

  @nPageSize int,

  @nPageID int

AS

BEGIN

  declare @nFrom int,@nTo int;

  set @nFrom=(@nPageID-1)*@nPageSize+1;

  set @nTo=@nPageID*@nPageSize;

  exec dbo.SYST_pGetScopeRows @Sql,@Top,@Order,@nFrom,@nTo;

END
GO
/****** Object:  View [dbo].[V_GPM_EmpGroupMenu]    Script Date: 07/06/2015 09:28:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[V_GPM_EmpGroupMenu]
AS
SELECT a.FK_Group,a.FK_Emp,b.FK_Menu,b.IsChecked FROM V_GPM_EmpGroup a, GPM_GroupMenu b 
WHERE a.FK_Group=b.FK_Group
GO
/****** Object:  View [dbo].[V_GPM_EmpMenu_GPM]    Script Date: 07/06/2015 09:28:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[V_GPM_EmpMenu_GPM]
AS
select distinct c.* from (
SELECT a.FK_Emp+'_'+a.FK_Menu as MyPK, a.FK_Emp,a.IsChecked, b.No as FK_Menu, b.* FROM 
   GPM_UserMenu a,GPM_Menu b WHERE a.FK_Menu=b.No AND B.IsEnable=1
UNION
SELECT a.FK_Emp+'_'+a.FK_Menu as MyPK, a.FK_Emp,a.IsChecked, b.No as FK_Menu, b.* FROM 
  V_GPM_EmpGroupMenu a,GPM_Menu b  WHERE a.FK_Menu=b.No AND B.IsEnable=1
UNION
SELECT a.FK_Emp+'_'+a.FK_Menu as MyPK, a.FK_Emp,a.IsChecked, b.No as FK_Menu, b.* 
FROM V_GPM_EmpStationMenu a,GPM_Menu b WHERE a.FK_Menu=b.No AND B.IsEnable=1
) c
GO
/****** Object:  View [dbo].[V_GPM_EmpMenu]    Script Date: 07/06/2015 09:28:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[V_GPM_EmpMenu]
AS
select distinct c.* from (
SELECT a.FK_Emp+'_'+a.FK_Menu as MyPK, a.FK_Emp, b.No as FK_Menu, b.* FROM 
   GPM_UserMenu a,GPM_Menu b WHERE a.FK_Menu=b.No AND B.IsEnable=1
UNION
SELECT a.FK_Emp+'_'+a.FK_Menu as MyPK, a.FK_Emp, b.No as FK_Menu, b.* FROM 
  V_GPM_EmpGroupMenu a,GPM_Menu b  WHERE a.FK_Menu=b.No AND B.IsEnable=1
) c
GO
/****** Object:  Default [DF__BM_CRJDtl__FK_De__09946309]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[BM_CRJDtl] ADD  DEFAULT ('') FOR [FK_Dept]
GO
/****** Object:  Default [DF__BM_CRJDtl__MDD__0A888742]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[BM_CRJDtl] ADD  DEFAULT ('') FOR [MDD]
GO
/****** Object:  Default [DF__BM_CRJDtl__DTApp__0B7CAB7B]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[BM_CRJDtl] ADD  DEFAULT ('') FOR [DTApp]
GO
/****** Object:  Default [DF__BM_CRJDtl__DTRel__0C70CFB4]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[BM_CRJDtl] ADD  DEFAULT ('') FOR [DTRel]
GO
/****** Object:  Default [DF__BM_CRJDtl__DTBac__0D64F3ED]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[BM_CRJDtl] ADD  DEFAULT ('') FOR [DTBack]
GO
/****** Object:  Default [DF__BM_CRJDtl__ZZBH__0E591826]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[BM_CRJDtl] ADD  DEFAULT ('') FOR [ZZBH]
GO
/****** Object:  Default [DF__BM_CRJDtl__BillN__0F4D3C5F]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[BM_CRJDtl] ADD  DEFAULT ('') FOR [BillNo]
GO
/****** Object:  Default [DF__BM_CRJDtl__BZ__10416098]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[BM_CRJDtl] ADD  DEFAULT ('') FOR [BZ]
GO
/****** Object:  Default [DF__BM_CRJDtl__CardT__113584D1]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[BM_CRJDtl] ADD  DEFAULT ('0') FOR [CardType]
GO
/****** Object:  Default [DF__BM_EduDtl__DTFro__65570293]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[BM_EduDtl] ADD  DEFAULT ('') FOR [DTFrom]
GO
/****** Object:  Default [DF__BM_EduDtl__DTTo__664B26CC]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[BM_EduDtl] ADD  DEFAULT ('') FOR [DTTo]
GO
/****** Object:  Default [DF__BM_EduDtl__FK_De__7E22B05D]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[BM_EduDtl] ADD  DEFAULT ('') FOR [FK_Dept]
GO
/****** Object:  Default [DF__BM_EduDtl__XXLX__7F16D496]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[BM_EduDtl] ADD  DEFAULT ('0') FOR [XXLX]
GO
/****** Object:  Default [DF__BM_EduDtl__DTApp__000AF8CF]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[BM_EduDtl] ADD  DEFAULT ('') FOR [DTApp]
GO
/****** Object:  Default [DF__BM_EduDtl__XueSh__00FF1D08]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[BM_EduDtl] ADD  DEFAULT ('0') FOR [XueShi]
GO
/****** Object:  Default [DF__BM_EduDtl__ZSBH__01F34141]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[BM_EduDtl] ADD  DEFAULT ('') FOR [ZSBH]
GO
/****** Object:  Default [DF__BM_EduDtl__FZJG__02E7657A]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[BM_EduDtl] ADD  DEFAULT ('') FOR [FZJG]
GO
/****** Object:  Default [DF__BM_EduDtl__DTRel__03DB89B3]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[BM_EduDtl] ADD  DEFAULT ('') FOR [DTRel]
GO
/****** Object:  Default [DF__BM_EduDtl__BillN__04CFADEC]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[BM_EduDtl] ADD  DEFAULT ('') FOR [BillNo]
GO
/****** Object:  Default [DF__BM_EduDtl__SJLY__05C3D225]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[BM_EduDtl] ADD  DEFAULT ('0') FOR [SJLY]
GO
/****** Object:  Default [DF__BM_EduDtl__BZ__06B7F65E]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[BM_EduDtl] ADD  DEFAULT ('') FOR [BZ]
GO
/****** Object:  Default [DF__BM_Emp__EmpMJ__284DF453]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[BM_Emp] ADD  DEFAULT ('0') FOR [EmpMJ]
GO
/****** Object:  Default [DF__BM_Emp__BZ__2942188C]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[BM_Emp] ADD  DEFAULT ('') FOR [BZ]
GO
/****** Object:  Default [DF__BM_Emp__GA_QFD__4119A21D]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[BM_Emp] ADD  DEFAULT ('') FOR [GA_QFD]
GO
/****** Object:  Default [DF__BM_Emp__GA_ID__420DC656]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[BM_Emp] ADD  DEFAULT ('') FOR [GA_ID]
GO
/****** Object:  Default [DF__BM_Emp__GA_AppDT__4301EA8F]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[BM_Emp] ADD  DEFAULT ('') FOR [GA_AppDT]
GO
/****** Object:  Default [DF__BM_Emp__GA_Relto__43F60EC8]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[BM_Emp] ADD  DEFAULT ('') FOR [GA_ReltoDT]
GO
/****** Object:  Default [DF__BM_Emp__GA_Sta__44EA3301]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[BM_Emp] ADD  DEFAULT ('0') FOR [GA_Sta]
GO
/****** Object:  Default [DF__BM_Emp__GA_QFD1__46D27B73]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[BM_Emp] ADD  DEFAULT ('') FOR [GA_QFD1]
GO
/****** Object:  Default [DF__BM_Emp__GA_ID1__47C69FAC]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[BM_Emp] ADD  DEFAULT ('') FOR [GA_ID1]
GO
/****** Object:  Default [DF__BM_Emp__GA_AppDT__48BAC3E5]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[BM_Emp] ADD  DEFAULT ('') FOR [GA_AppDT1]
GO
/****** Object:  Default [DF__BM_Emp__GA_Relto__49AEE81E]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[BM_Emp] ADD  DEFAULT ('') FOR [GA_ReltoDT1]
GO
/****** Object:  Default [DF__BM_Emp__GA_Sta1__4AA30C57]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[BM_Emp] ADD  DEFAULT ('0') FOR [GA_Sta1]
GO
/****** Object:  Default [DF__BM_Emp__GA_Note1__4B973090]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[BM_Emp] ADD  DEFAULT ('') FOR [GA_Note1]
GO
/****** Object:  Default [DF__BM_Emp__GA_QFD2__4C8B54C9]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[BM_Emp] ADD  DEFAULT ('') FOR [GA_QFD2]
GO
/****** Object:  Default [DF__BM_Emp__GA_ID2__4D7F7902]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[BM_Emp] ADD  DEFAULT ('') FOR [GA_ID2]
GO
/****** Object:  Default [DF__BM_Emp__GA_AppDT__4E739D3B]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[BM_Emp] ADD  DEFAULT ('') FOR [GA_AppDT2]
GO
/****** Object:  Default [DF__BM_Emp__GA_Relto__4F67C174]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[BM_Emp] ADD  DEFAULT ('') FOR [GA_ReltoDT2]
GO
/****** Object:  Default [DF__BM_Emp__GA_Sta2__505BE5AD]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[BM_Emp] ADD  DEFAULT ('0') FOR [GA_Sta2]
GO
/****** Object:  Default [DF__BM_Emp__GA_Note2__515009E6]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[BM_Emp] ADD  DEFAULT ('') FOR [GA_Note2]
GO
/****** Object:  Default [DF__BM_Emp__TA_QFD__52442E1F]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[BM_Emp] ADD  DEFAULT ('') FOR [TA_QFD]
GO
/****** Object:  Default [DF__BM_Emp__TA_ID__53385258]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[BM_Emp] ADD  DEFAULT ('') FOR [TA_ID]
GO
/****** Object:  Default [DF__BM_Emp__TA_AppDT__542C7691]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[BM_Emp] ADD  DEFAULT ('') FOR [TA_AppDT]
GO
/****** Object:  Default [DF__BM_Emp__TA_Relto__55209ACA]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[BM_Emp] ADD  DEFAULT ('') FOR [TA_ReltoDT]
GO
/****** Object:  Default [DF__BM_Emp__TA_Sta__5614BF03]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[BM_Emp] ADD  DEFAULT ('0') FOR [TA_Sta]
GO
/****** Object:  Default [DF__BM_Emp__TA_Note__5708E33C]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[BM_Emp] ADD  DEFAULT ('') FOR [TA_Note]
GO
/****** Object:  Default [DF__BM_Emp__HZ_QFD__57FD0775]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[BM_Emp] ADD  DEFAULT ('') FOR [HZ_QFD]
GO
/****** Object:  Default [DF__BM_Emp__HZ_ID__58F12BAE]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[BM_Emp] ADD  DEFAULT ('') FOR [HZ_ID]
GO
/****** Object:  Default [DF__BM_Emp__HZ_AppDT__59E54FE7]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[BM_Emp] ADD  DEFAULT ('') FOR [HZ_AppDT]
GO
/****** Object:  Default [DF__BM_Emp__HZ_Relto__5AD97420]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[BM_Emp] ADD  DEFAULT ('') FOR [HZ_ReltoDT]
GO
/****** Object:  Default [DF__BM_Emp__HZ_Sta__5BCD9859]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[BM_Emp] ADD  DEFAULT ('0') FOR [HZ_Sta]
GO
/****** Object:  Default [DF__BM_Emp__HZ_Note__5CC1BC92]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[BM_Emp] ADD  DEFAULT ('') FOR [HZ_Note]
GO
/****** Object:  Default [DF__BM_Emp__FK_Dept__69279377]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[BM_Emp] ADD  DEFAULT ('') FOR [FK_Dept]
GO
/****** Object:  Default [DF__BM_Emp__FK_Duty__6A1BB7B0]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[BM_Emp] ADD  DEFAULT ('') FOR [FK_Duty]
GO
/****** Object:  Default [DF__BM_Emp__RYLX__6B0FDBE9]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[BM_Emp] ADD  DEFAULT ('0') FOR [RYLX]
GO
/****** Object:  Default [DF__BM_Emp__ZZMM__6C040022]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[BM_Emp] ADD  DEFAULT ('0') FOR [ZZMM]
GO
/****** Object:  Default [DF__BM_JSJ__PPXH__1BB31344]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[BM_JSJ] ADD  DEFAULT ('') FOR [PPXH]
GO
/****** Object:  Default [DF__BM_JSJ__SN__1CA7377D]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[BM_JSJ] ADD  DEFAULT ('') FOR [SN]
GO
/****** Object:  Default [DF__BM_JSJ__CZXT__1E8F7FEF]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[BM_JSJ] ADD  DEFAULT ('0') FOR [CZXT]
GO
/****** Object:  Default [DF__BM_SBDtl__SBMJ__7C3A67EB]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[BM_SBDtl] ADD  DEFAULT ('0') FOR [SBMJ]
GO
/****** Object:  Default [DF__BM_SBDtl__CZXT__7D2E8C24]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[BM_SBDtl] ADD  DEFAULT ('0') FOR [CZXT]
GO
/****** Object:  Default [DF__BM_SBDtl__MyNum__1229A90A]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[BM_SBDtl] ADD  DEFAULT ('1') FOR [MyNum]
GO
/****** Object:  Default [DF__BM_USB__USBType__216BEC9A]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[BM_USB] ADD  DEFAULT ('0') FOR [USBType]
GO
/****** Object:  Default [DF__BM_WLJF__CZXT__1D9B5BB6]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[BM_WLJF] ADD  DEFAULT ('0') FOR [CZXT]
GO
/****** Object:  Default [DF__BM_XCBDDt__FK_De__6CF8245B]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[BM_XCBDDtl] ADD  DEFAULT ('') FOR [FK_Dept]
GO
/****** Object:  Default [DF__BM_XCBDDt__XXSor__6DEC4894]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[BM_XCBDDtl] ADD  DEFAULT ('0') FOR [XXSort]
GO
/****** Object:  Default [DF__BM_XCBDDtl__MJ__6EE06CCD]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[BM_XCBDDtl] ADD  DEFAULT ('0') FOR [MJ]
GO
/****** Object:  Default [DF__BM_XCBDDt__DTApp__6FD49106]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[BM_XCBDDtl] ADD  DEFAULT ('') FOR [DTApp]
GO
/****** Object:  Default [DF__BM_XCBDDt__DTRel__70C8B53F]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[BM_XCBDDtl] ADD  DEFAULT ('') FOR [DTRel]
GO
/****** Object:  Default [DF__BM_XCBDDtl__ZZDW__71BCD978]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[BM_XCBDDtl] ADD  DEFAULT ('') FOR [ZZDW]
GO
/****** Object:  Default [DF__BM_XCBDDt__BillN__72B0FDB1]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[BM_XCBDDtl] ADD  DEFAULT ('') FOR [BillNo]
GO
/****** Object:  Default [DF__BM_XCBDDtl__BZ__73A521EA]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[BM_XCBDDtl] ADD  DEFAULT ('') FOR [BZ]
GO
/****** Object:  Default [DF__BM_XCBDDtl__Sort__74994623]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[BM_XCBDDtl] ADD  DEFAULT ('0') FOR [Sort]
GO
/****** Object:  Default [DF__BM_XCBDDt__YuanY__7775B2CE]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[BM_XCBDDtl] ADD  DEFAULT ('') FOR [YuanYin]
GO
/****** Object:  Default [DF__Port_Stat__StaGr__5090EFD7]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[Port_Station] ADD  DEFAULT ('0') FOR [StaGrade]
GO
/****** Object:  Default [DF__Prj_EmpPr__Stati__2EDAF651]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[Prj_EmpPrj] ADD  DEFAULT ('') FOR [StationStrs]
GO
/****** Object:  Default [DF__Sys_MapDa__Offic__08EA5793]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[Sys_MapData] ADD  DEFAULT ('打开本地') FOR [OfficeOpenLab]
GO
/****** Object:  Default [DF__Sys_MapDa__Offic__09DE7BCC]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[Sys_MapData] ADD  DEFAULT ('0') FOR [OfficeOpenEnable]
GO
/****** Object:  Default [DF__Sys_MapDa__Offic__0AD2A005]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[Sys_MapData] ADD  DEFAULT ('打开模板') FOR [OfficeOpenTemplateLab]
GO
/****** Object:  Default [DF__Sys_MapDa__Offic__0BC6C43E]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[Sys_MapData] ADD  DEFAULT ('0') FOR [OfficeOpenTemplateEnable]
GO
/****** Object:  Default [DF__Sys_MapDa__Offic__0CBAE877]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[Sys_MapData] ADD  DEFAULT ('保存') FOR [OfficeSaveLab]
GO
/****** Object:  Default [DF__Sys_MapDa__Offic__0DAF0CB0]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[Sys_MapData] ADD  DEFAULT ('1') FOR [OfficeSaveEnable]
GO
/****** Object:  Default [DF__Sys_MapDa__Offic__0EA330E9]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[Sys_MapData] ADD  DEFAULT ('接受修订') FOR [OfficeAcceptLab]
GO
/****** Object:  Default [DF__Sys_MapDa__Offic__0F975522]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[Sys_MapData] ADD  DEFAULT ('0') FOR [OfficeAcceptEnable]
GO
/****** Object:  Default [DF__Sys_MapDa__Offic__108B795B]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[Sys_MapData] ADD  DEFAULT ('拒绝修订') FOR [OfficeRefuseLab]
GO
/****** Object:  Default [DF__Sys_MapDa__Offic__117F9D94]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[Sys_MapData] ADD  DEFAULT ('0') FOR [OfficeRefuseEnable]
GO
/****** Object:  Default [DF__Sys_MapDa__Offic__1273C1CD]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[Sys_MapData] ADD  DEFAULT ('套红按钮') FOR [OfficeOverLab]
GO
/****** Object:  Default [DF__Sys_MapDa__Offic__1367E606]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[Sys_MapData] ADD  DEFAULT ('0') FOR [OfficeOverEnable]
GO
/****** Object:  Default [DF__Sys_MapDa__Offic__145C0A3F]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[Sys_MapData] ADD  DEFAULT ('1') FOR [OfficeMarksEnable]
GO
/****** Object:  Default [DF__Sys_MapDa__Offic__15502E78]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[Sys_MapData] ADD  DEFAULT ('打印按钮') FOR [OfficePrintLab]
GO
/****** Object:  Default [DF__Sys_MapDa__Offic__164452B1]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[Sys_MapData] ADD  DEFAULT ('0') FOR [OfficePrintEnable]
GO
/****** Object:  Default [DF__Sys_MapDa__Offic__173876EA]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[Sys_MapData] ADD  DEFAULT ('签章按钮') FOR [OfficeSealLab]
GO
/****** Object:  Default [DF__Sys_MapDa__Offic__182C9B23]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[Sys_MapData] ADD  DEFAULT ('0') FOR [OfficeSealEnable]
GO
/****** Object:  Default [DF__Sys_MapDa__Offic__1920BF5C]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[Sys_MapData] ADD  DEFAULT ('插入流程') FOR [OfficeInsertFlowLab]
GO
/****** Object:  Default [DF__Sys_MapDa__Offic__1A14E395]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[Sys_MapData] ADD  DEFAULT ('0') FOR [OfficeInsertFlowEnable]
GO
/****** Object:  Default [DF__Sys_MapDa__Offic__1B0907CE]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[Sys_MapData] ADD  DEFAULT ('0') FOR [OfficeNodeInfo]
GO
/****** Object:  Default [DF__Sys_MapDa__Offic__1BFD2C07]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[Sys_MapData] ADD  DEFAULT ('0') FOR [OfficeReSavePDF]
GO
/****** Object:  Default [DF__Sys_MapDa__Offic__1CF15040]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[Sys_MapData] ADD  DEFAULT ('下载') FOR [OfficeDownLab]
GO
/****** Object:  Default [DF__Sys_MapDa__Offic__1DE57479]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[Sys_MapData] ADD  DEFAULT ('0') FOR [OfficeDownEnable]
GO
/****** Object:  Default [DF__Sys_MapDa__Offic__1ED998B2]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[Sys_MapData] ADD  DEFAULT ('1') FOR [OfficeIsMarks]
GO
/****** Object:  Default [DF__Sys_MapDa__Offic__1FCDBCEB]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[Sys_MapData] ADD  DEFAULT ('') FOR [OfficeTemplate]
GO
/****** Object:  Default [DF__Sys_MapDa__Offic__20C1E124]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[Sys_MapData] ADD  DEFAULT ('1') FOR [OfficeIsParent]
GO
/****** Object:  Default [DF__Sys_MapDa__Offic__21B6055D]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[Sys_MapData] ADD  DEFAULT ('0') FOR [OfficeTHEnable]
GO
/****** Object:  Default [DF__Sys_MapDa__Offic__22AA2996]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[Sys_MapData] ADD  DEFAULT ('') FOR [OfficeTHTemplate]
GO
/****** Object:  Default [DF__Sys_MapDa__FormR__2E70E1FD]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[Sys_MapData] ADD  DEFAULT ('1') FOR [FormRunType]
GO
/****** Object:  Default [DF__Sys_MapDa__FK_Fl__297722B6]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[Sys_MapData] ADD  DEFAULT ('') FOR [FK_Flow]
GO
/****** Object:  Default [DF__Sys_MapDa__Paren__2A6B46EF]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[Sys_MapData] ADD  DEFAULT ('') FOR [ParentMapData]
GO
/****** Object:  Default [DF__Sys_MapDa__Right__2B5F6B28]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[Sys_MapData] ADD  DEFAULT ('0') FOR [RightViewWay]
GO
/****** Object:  Default [DF__Sys_MapDa__Right__2C538F61]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[Sys_MapData] ADD  DEFAULT ('') FOR [RightViewTag]
GO
/****** Object:  Default [DF__Sys_MapDa__Right__2D47B39A]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[Sys_MapData] ADD  DEFAULT ('0') FOR [RightDeptWay]
GO
/****** Object:  Default [DF__Sys_MapDa__Right__2E3BD7D3]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[Sys_MapData] ADD  DEFAULT ('') FOR [RightDeptTag]
GO
/****** Object:  Default [DF__WF_Flow__Designe__10AB74EC]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Flow] ADD  DEFAULT ('') FOR [DesignerNo]
GO
/****** Object:  Default [DF__WF_Flow__Designe__119F9925]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Flow] ADD  DEFAULT ('') FOR [DesignerName]
GO
/****** Object:  Default [DF__WF_Flow__Draft__1BE81D6E]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Flow] ADD  DEFAULT ('0') FOR [Draft]
GO
/****** Object:  Default [DF__WF_Flow__DTSWay__1CDC41A7]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Flow] ADD  DEFAULT ('0') FOR [DTSWay]
GO
/****** Object:  Default [DF__WF_Flow__DTSBTab__1DD065E0]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Flow] ADD  DEFAULT ('') FOR [DTSBTable]
GO
/****** Object:  Default [DF__WF_Flow__DTSBTab__1EC48A19]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Flow] ADD  DEFAULT ('') FOR [DTSBTablePK]
GO
/****** Object:  Default [DF__WF_Flow__DTSTime__1FB8AE52]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Flow] ADD  DEFAULT ('0') FOR [DTSTime]
GO
/****** Object:  Default [DF__WF_Flow__DTSSpec__20ACD28B]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Flow] ADD  DEFAULT ('') FOR [DTSSpecNodes]
GO
/****** Object:  Default [DF__WF_Flow__DTSFiel__21A0F6C4]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Flow] ADD  DEFAULT ('0') FOR [DTSField]
GO
/****** Object:  Default [DF__WF_Flow__DTSFiel__22951AFD]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Flow] ADD  DEFAULT ('') FOR [DTSFields]
GO
/****** Object:  Default [DF__WF_FrmNod__FrmTy__6D2D2E85]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_FrmNode] ADD  DEFAULT ('0') FOR [FrmType]
GO
/****** Object:  Default [DF__WF_FrmNod__IsEdi__6E2152BE]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_FrmNode] ADD  DEFAULT ('1') FOR [IsEdit]
GO
/****** Object:  Default [DF__WF_FrmNod__IsPri__6F1576F7]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_FrmNode] ADD  DEFAULT ('0') FOR [IsPrint]
GO
/****** Object:  Default [DF__WF_FrmNod__IsEna__70099B30]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_FrmNode] ADD  DEFAULT ('0') FOR [IsEnableLoadData]
GO
/****** Object:  Default [DF__WF_FrmNode__Idx__70FDBF69]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_FrmNode] ADD  DEFAULT ('0') FOR [Idx]
GO
/****** Object:  Default [DF__WF_FrmNod__FrmSl__71F1E3A2]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_FrmNode] ADD  DEFAULT ('0') FOR [FrmSln]
GO
/****** Object:  Default [DF__WF_FrmNod__WhoIs__72E607DB]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_FrmNode] ADD  DEFAULT ('0') FOR [WhoIsPK]
GO
/****** Object:  Default [DF__WF_GenerW__FlowN__3118447E]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_GenerWorkFlow] ADD  DEFAULT ('') FOR [FlowName]
GO
/****** Object:  Default [DF__WF_GenerW__WFSta__320C68B7]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_GenerWorkFlow] ADD  DEFAULT ('0') FOR [WFState]
GO
/****** Object:  Default [DF__WF_GenerW__Start__33008CF0]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_GenerWorkFlow] ADD  DEFAULT ('') FOR [Starter]
GO
/****** Object:  Default [DF__WF_GenerW__Sende__33F4B129]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_GenerWorkFlow] ADD  DEFAULT ('') FOR [Sender]
GO
/****** Object:  Default [DF__WF_GenerW__FK_No__34E8D562]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_GenerWorkFlow] ADD  DEFAULT ('0') FOR [FK_Node]
GO
/****** Object:  Default [DF__WF_GenerW__DeptN__35DCF99B]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_GenerWorkFlow] ADD  DEFAULT ('') FOR [DeptName]
GO
/****** Object:  Default [DF__WF_GenerWor__PRI__36D11DD4]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_GenerWorkFlow] ADD  DEFAULT ('1') FOR [PRI]
GO
/****** Object:  Default [DF__WF_GenerW__SDTOf__37C5420D]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_GenerWorkFlow] ADD  DEFAULT ('2015-05-11 09:45') FOR [SDTOfNode]
GO
/****** Object:  Default [DF__WF_GenerW__SDTOf__38B96646]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_GenerWorkFlow] ADD  DEFAULT ('2015-05-11 09:45') FOR [SDTOfFlow]
GO
/****** Object:  Default [DF__WF_GenerW__PFlow__39AD8A7F]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_GenerWorkFlow] ADD  DEFAULT ('') FOR [PFlowNo]
GO
/****** Object:  Default [DF__WF_GenerW__PWork__3AA1AEB8]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_GenerWorkFlow] ADD  DEFAULT ('0') FOR [PWorkID]
GO
/****** Object:  Default [DF__WF_GenerW__PNode__3B95D2F1]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_GenerWorkFlow] ADD  DEFAULT ('0') FOR [PNodeID]
GO
/****** Object:  Default [DF__WF_GenerWo__PFID__3C89F72A]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_GenerWorkFlow] ADD  DEFAULT ('0') FOR [PFID]
GO
/****** Object:  Default [DF__WF_GenerWo__PEmp__3D7E1B63]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_GenerWorkFlow] ADD  DEFAULT ('') FOR [PEmp]
GO
/****** Object:  Default [DF__WF_GenerW__CFlow__3E723F9C]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_GenerWorkFlow] ADD  DEFAULT ('') FOR [CFlowNo]
GO
/****** Object:  Default [DF__WF_GenerW__CWork__3F6663D5]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_GenerWorkFlow] ADD  DEFAULT ('0') FOR [CWorkID]
GO
/****** Object:  Default [DF__WF_GenerW__Guest__405A880E]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_GenerWorkFlow] ADD  DEFAULT ('') FOR [GuestNo]
GO
/****** Object:  Default [DF__WF_GenerW__Guest__414EAC47]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_GenerWorkFlow] ADD  DEFAULT ('') FOR [GuestName]
GO
/****** Object:  Default [DF__WF_GenerW__TodoE__4242D080]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_GenerWorkFlow] ADD  DEFAULT ('') FOR [TodoEmps]
GO
/****** Object:  Default [DF__WF_GenerW__TodoE__4336F4B9]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_GenerWorkFlow] ADD  DEFAULT ('0') FOR [TodoEmpsNum]
GO
/****** Object:  Default [DF__WF_GenerW__TaskS__442B18F2]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_GenerWorkFlow] ADD  DEFAULT ('0') FOR [TaskSta]
GO
/****** Object:  Default [DF__WF_GenerW__AtPar__451F3D2B]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_GenerWorkFlow] ADD  DEFAULT ('') FOR [AtPara]
GO
/****** Object:  Default [DF__WF_GenerWo__Emps__46136164]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_GenerWorkFlow] ADD  DEFAULT ('') FOR [Emps]
GO
/****** Object:  Default [DF__WF_GenerWo__GUID__4707859D]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_GenerWorkFlow] ADD  DEFAULT ('') FOR [GUID]
GO
/****** Object:  Default [DF__WF_GenerW__FK_NY__47FBA9D6]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_GenerWorkFlow] ADD  DEFAULT ('') FOR [FK_NY]
GO
/****** Object:  Default [DF__WF_Node__Step__3335971A]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('0') FOR [Step]
GO
/****** Object:  Default [DF__WF_Node__FK_Flow__3429BB53]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('') FOR [FK_Flow]
GO
/****** Object:  Default [DF__WF_Node__Name__351DDF8C]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('') FOR [Name]
GO
/****** Object:  Default [DF__WF_Node__Deliver__361203C5]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('0') FOR [DeliveryWay]
GO
/****** Object:  Default [DF__WF_Node__Deliver__370627FE]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('') FOR [DeliveryParas]
GO
/****** Object:  Default [DF__WF_Node__WhoExeI__37FA4C37]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('0') FOR [WhoExeIt]
GO
/****** Object:  Default [DF__WF_Node__TurnToD__38EE7070]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('0') FOR [TurnToDeal]
GO
/****** Object:  Default [DF__WF_Node__TurnToD__39E294A9]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('') FOR [TurnToDealDoc]
GO
/****** Object:  Default [DF__WF_Node__ReadRec__3AD6B8E2]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('0') FOR [ReadReceipts]
GO
/****** Object:  Default [DF__WF_Node__CondMod__3BCADD1B]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('0') FOR [CondModel]
GO
/****** Object:  Default [DF__WF_Node__CancelR__3CBF0154]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('0') FOR [CancelRole]
GO
/****** Object:  Default [DF__WF_Node__BatchRo__3DB3258D]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('0') FOR [BatchRole]
GO
/****** Object:  Default [DF__WF_Node__BatchLi__3EA749C6]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('12') FOR [BatchListCount]
GO
/****** Object:  Default [DF__WF_Node__BatchPa__3F9B6DFF]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('') FOR [BatchParas]
GO
/****** Object:  Default [DF__WF_Node__IsTask__408F9238]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('1') FOR [IsTask]
GO
/****** Object:  Default [DF__WF_Node__IsRM__4183B671]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('1') FOR [IsRM]
GO
/****** Object:  Default [DF__WF_Node__DTFrom__4277DAAA]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('2015-05-11 09:45') FOR [DTFrom]
GO
/****** Object:  Default [DF__WF_Node__DTTo__436BFEE3]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('2015-05-11 09:45') FOR [DTTo]
GO
/****** Object:  Default [DF__WF_Node__FormTyp__4460231C]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('1') FOR [FormType]
GO
/****** Object:  Default [DF__WF_Node__NodeFrm__45544755]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('') FOR [NodeFrmID]
GO
/****** Object:  Default [DF__WF_Node__FormUrl__46486B8E]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('') FOR [FormUrl]
GO
/****** Object:  Default [DF__WF_Node__FocusFi__473C8FC7]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('') FOR [FocusField]
GO
/****** Object:  Default [DF__WF_Node__SaveMod__4830B400]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('0') FOR [SaveModel]
GO
/****** Object:  Default [DF__WF_Node__RunMode__4924D839]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('0') FOR [RunModel]
GO
/****** Object:  Default [DF__WF_Node__SubThre__4A18FC72]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('0') FOR [SubThreadType]
GO
/****** Object:  Default [DF__WF_Node__PassRat__4B0D20AB]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('100') FOR [PassRate]
GO
/****** Object:  Default [DF__WF_Node__SubFlow__4C0144E4]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('0') FOR [SubFlowStartWay]
GO
/****** Object:  Default [DF__WF_Node__SubFlow__4CF5691D]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('') FOR [SubFlowStartParas]
GO
/****** Object:  Default [DF__WF_Node__Todolis__4DE98D56]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('0') FOR [TodolistModel]
GO
/****** Object:  Default [DF__WF_Node__BlockMo__4EDDB18F]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('0') FOR [BlockModel]
GO
/****** Object:  Default [DF__WF_Node__BlockEx__4FD1D5C8]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('') FOR [BlockExp]
GO
/****** Object:  Default [DF__WF_Node__BlockAl__50C5FA01]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('') FOR [BlockAlert]
GO
/****** Object:  Default [DF__WF_Node__IsAllow__51BA1E3A]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('0') FOR [IsAllowRepeatEmps]
GO
/****** Object:  Default [DF__WF_Node__IsGuest__52AE4273]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('0') FOR [IsGuestNode]
GO
/****** Object:  Default [DF__WF_Node__AutoJum__53A266AC]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('0') FOR [AutoJumpRole0]
GO
/****** Object:  Default [DF__WF_Node__AutoJum__54968AE5]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('0') FOR [AutoJumpRole1]
GO
/****** Object:  Default [DF__WF_Node__AutoJum__558AAF1E]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('0') FOR [AutoJumpRole2]
GO
/****** Object:  Default [DF__WF_Node__WhenNoW__567ED357]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('0') FOR [WhenNoWorker]
GO
/****** Object:  Default [DF__WF_Node__ThreadK__5772F790]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('0') FOR [ThreadKillRole]
GO
/****** Object:  Default [DF__WF_Node__JumpToN__58671BC9]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('') FOR [JumpToNodes]
GO
/****** Object:  Default [DF__WF_Node__IsBackT__595B4002]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('0') FOR [IsBackTracking]
GO
/****** Object:  Default [DF__WF_Node__CCWrite__5A4F643B]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('0') FOR [CCWriteTo]
GO
/****** Object:  Default [DF__WF_Node__Warning__5B438874]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('0') FOR [WarningDays]
GO
/****** Object:  Default [DF__WF_Node__DeductD__5C37ACAD]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('1') FOR [DeductDays]
GO
/****** Object:  Default [DF__WF_Node__DeductC__5D2BD0E6]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('2') FOR [DeductCent]
GO
/****** Object:  Default [DF__WF_Node__MaxDedu__5E1FF51F]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('0') FOR [MaxDeductCent]
GO
/****** Object:  Default [DF__WF_Node__SwinkCe__5F141958]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('0.1') FOR [SwinkCent]
GO
/****** Object:  Default [DF__WF_Node__OutTime__60083D91]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('0') FOR [OutTimeDeal]
GO
/****** Object:  Default [DF__WF_Node__DoOutTi__60FC61CA]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('') FOR [DoOutTime]
GO
/****** Object:  Default [DF__WF_Node__DoOutTi__61F08603]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('') FOR [DoOutTimeCond]
GO
/****** Object:  Default [DF__WF_Node__CHWay__62E4AA3C]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('0') FOR [CHWay]
GO
/****** Object:  Default [DF__WF_Node__Workloa__63D8CE75]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('0') FOR [Workload]
GO
/****** Object:  Default [DF__WF_Node__IsEval__64CCF2AE]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('0') FOR [IsEval]
GO
/****** Object:  Default [DF__WF_Node__FWCSta__65C116E7]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('0') FOR [FWCSta]
GO
/****** Object:  Default [DF__WF_Node__FWCShow__66B53B20]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('1') FOR [FWCShowModel]
GO
/****** Object:  Default [DF__WF_Node__FWCType__67A95F59]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('0') FOR [FWCType]
GO
/****** Object:  Default [DF__WF_Node__FWCNode__689D8392]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('') FOR [FWCNodeName]
GO
/****** Object:  Default [DF__WF_Node__FWCTrac__6991A7CB]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('1') FOR [FWCTrackEnable]
GO
/****** Object:  Default [DF__WF_Node__FWCList__6A85CC04]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('1') FOR [FWCListEnable]
GO
/****** Object:  Default [DF__WF_Node__FWCIsSh__6B79F03D]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('0') FOR [FWCIsShowAllStep]
GO
/****** Object:  Default [DF__WF_Node__FWCOpLa__6C6E1476]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('审核') FOR [FWCOpLabel]
GO
/****** Object:  Default [DF__WF_Node__FWCDefI__6D6238AF]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('同意') FOR [FWCDefInfo]
GO
/****** Object:  Default [DF__WF_Node__Sigantu__6E565CE8]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('0') FOR [SigantureEnabel]
GO
/****** Object:  Default [DF__WF_Node__FWCIsFu__6F4A8121]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('1') FOR [FWCIsFullInfo]
GO
/****** Object:  Default [DF__WF_Node__FWC_H__703EA55A]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('300') FOR [FWC_H]
GO
/****** Object:  Default [DF__WF_Node__FWC_W__7132C993]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('400') FOR [FWC_W]
GO
/****** Object:  Default [DF__WF_Node__OfficeO__7226EDCC]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('套红') FOR [OfficeOverLab]
GO
/****** Object:  Default [DF__WF_Node__MPhone___731B1205]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('0') FOR [MPhone_WorkModel]
GO
/****** Object:  Default [DF__WF_Node__MPhone___740F363E]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('0') FOR [MPhone_SrcModel]
GO
/****** Object:  Default [DF__WF_Node__MPad_Wo__75035A77]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('0') FOR [MPad_WorkModel]
GO
/****** Object:  Default [DF__WF_Node__MPad_Sr__75F77EB0]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('0') FOR [MPad_SrcModel]
GO
/****** Object:  Default [DF__WF_Node__Selecto__7CA47C3F]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('0') FOR [SelectorDBShowWay]
GO
/****** Object:  Default [DF__WF_Node__Selecto__7D98A078]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('0') FOR [SelectorModel]
GO
/****** Object:  Default [DF__WF_Node__Selecto__7E8CC4B1]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('') FOR [SelectorP1]
GO
/****** Object:  Default [DF__WF_Node__Selecto__7F80E8EA]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('') FOR [SelectorP2]
GO
/****** Object:  Default [DF__WF_Node__CheckNo__04459E07]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('') FOR [CheckNodes]
GO
/****** Object:  Default [DF__WF_Node__CCCtrlW__1A34DF26]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('0') FOR [CCCtrlWay]
GO
/****** Object:  Default [DF__WF_Node__CCSQL__1B29035F]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('') FOR [CCSQL]
GO
/****** Object:  Default [DF__WF_Node__CCTitle__1C1D2798]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('') FOR [CCTitle]
GO
/****** Object:  Default [DF__WF_Node__CCDoc__1D114BD1]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('') FOR [CCDoc]
GO
/****** Object:  Default [DF__WF_Node__ICON__536D5C82]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('') FOR [ICON]
GO
/****** Object:  Default [DF__WF_Node__NodeWor__546180BB]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('0') FOR [NodeWorkType]
GO
/****** Object:  Default [DF__WF_Node__FlowNam__5555A4F4]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('') FOR [FlowName]
GO
/****** Object:  Default [DF__WF_Node__FK_Flow__5649C92D]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('') FOR [FK_FlowSort]
GO
/****** Object:  Default [DF__WF_Node__FK_Flow__573DED66]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('') FOR [FK_FlowSortT]
GO
/****** Object:  Default [DF__WF_Node__FrmAttr__5832119F]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('') FOR [FrmAttr]
GO
/****** Object:  Default [DF__WF_Node__Doc__592635D8]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('') FOR [Doc]
GO
/****** Object:  Default [DF__WF_Node__IsCanRp__5A1A5A11]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('1') FOR [IsCanRpt]
GO
/****** Object:  Default [DF__WF_Node__IsCanOv__5B0E7E4A]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('0') FOR [IsCanOver]
GO
/****** Object:  Default [DF__WF_Node__IsSecre__5C02A283]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('0') FOR [IsSecret]
GO
/****** Object:  Default [DF__WF_Node__IsCanDe__5CF6C6BC]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('0') FOR [IsCanDelFlow]
GO
/****** Object:  Default [DF__WF_Node__IsHandO__5DEAEAF5]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('0') FOR [IsHandOver]
GO
/****** Object:  Default [DF__WF_Node__NodePos__5EDF0F2E]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('0') FOR [NodePosType]
GO
/****** Object:  Default [DF__WF_Node__IsCCFlo__5FD33367]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('0') FOR [IsCCFlow]
GO
/****** Object:  Default [DF__WF_Node__HisStas__60C757A0]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('') FOR [HisStas]
GO
/****** Object:  Default [DF__WF_Node__HisDept__61BB7BD9]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('') FOR [HisDeptStrs]
GO
/****** Object:  Default [DF__WF_Node__HisToND__62AFA012]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('') FOR [HisToNDs]
GO
/****** Object:  Default [DF__WF_Node__HisBill__63A3C44B]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('') FOR [HisBillIDs]
GO
/****** Object:  Default [DF__WF_Node__HisSubF__6497E884]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('') FOR [HisSubFlows]
GO
/****** Object:  Default [DF__WF_Node__PTable__658C0CBD]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('') FOR [PTable]
GO
/****** Object:  Default [DF__WF_Node__ShowShe__668030F6]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('') FOR [ShowSheets]
GO
/****** Object:  Default [DF__WF_Node__GroupSt__6774552F]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('') FOR [GroupStaNDs]
GO
/****** Object:  Default [DF__WF_Node__X__68687968]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('0') FOR [X]
GO
/****** Object:  Default [DF__WF_Node__Y__695C9DA1]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('0') FOR [Y]
GO
/****** Object:  Default [DF__WF_Node__AtPara__6A50C1DA]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('') FOR [AtPara]
GO
/****** Object:  Default [DF__WF_Node__DocLeft__6B44E613]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('') FOR [DocLeftWord]
GO
/****** Object:  Default [DF__WF_Node__DocRigh__6C390A4C]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('') FOR [DocRightWord]
GO
/****** Object:  Default [DF__WF_Node__FWC_X__77AABCF8]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('5') FOR [FWC_X]
GO
/****** Object:  Default [DF__WF_Node__FWC_Y__789EE131]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('5') FOR [FWC_Y]
GO
/****** Object:  Default [DF__WF_Node__Tip__190BB0C3]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('') FOR [Tip]
GO
/****** Object:  Default [DF__WF_Node__IsExpSe__19FFD4FC]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('1') FOR [IsExpSender]
GO
/****** Object:  Default [DF__WF_Node__FWCAth__1AF3F935]    Script Date: 07/06/2015 09:28:43 ******/
ALTER TABLE [dbo].[WF_Node] ADD  DEFAULT ('0') FOR [FWCAth]
GO
