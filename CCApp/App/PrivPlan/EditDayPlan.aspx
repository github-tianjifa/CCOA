﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main/master/Site1.Master" ClientIDMode="Static" AutoEventWireup="true" CodeBehind="EditDayPlan.aspx.cs" Inherits="CCOA.App.PrivPlan.EditDayPlan" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cph_head" runat="server">
    <style type="text/css">
        body
        {
            font-size: 13px;
        }
        div#nav a
        {
            border: solid 1px #777;
            padding: 1px 1px 1px 1px;
            margin: 3px 3px 3px 3px;
            background-color: #eee;
            font-weight: lighter;
            color: #555;
        }
    </style>
    <script type="text/javascript">
        var theLastIndex = <%=this.TheLastIndex %>;  //至少几个任务，从1开始。
        var theTopIndex = <%=this.TheTopIndex %>;  //至少几个任务，从1开始。
        /*
        $(function ($) {
            for (var i = 2; i <= theLastIndex; i++) {
                addTask();
            }
        });
        */
        $(function ($) {
           refreshButtons();
        });
        function addTask() {
           //取得最后一个div_task
            var index = $("#txt_itemCount").val();
            var div = $("#div_task_" + index);
            var html = div.html();            
            //创建一个新的div_task
            var index_New = String(Number(index) + 1);
            var div_New = $("<div id='div_task_"+index_New+"'><div>");
            html = html.replace(/[_][0-9]+/g, "_" + index_New);
            div_New.html(html);
            //添加
            div.after(div_New);
            var chNum=["一","二","三","四","五","六","七","八","九","十"];
            $("#span_no_" + index_New).text('第'+chNum[Number(index_New)-1]+'项');
            //状态
            $("#txt_itemCount").val(index_New);
            refreshButtons();
        }
        function removeTask() {
            //取得最后一个div_task
            var index = $("#txt_itemCount").val();
            var div = $("#div_task_" + index);
            //删除
            div.remove();
            //状态
            var index_New = String(Number(index) - 1);
            $("#txt_itemCount").val(index_New);
            refreshButtons();
        }
        function refreshButtons()
        {
            $(".addTask").hide();
            $(".removeTask").hide();
            var index = $("#txt_itemCount").val();
            if (index < theTopIndex) {
                $("#btn_add_" + index).show();
            }
            if (index > theLastIndex) {
                $("#btn_remove_" + index).show();
            }
        }
        function preSubmit() {
            var planDoc = $("#txtTodayDesc").val();
            if (planDoc.length < 10) { alert("计划简介不能少于10个字！"); return false; }
            var taskCount = $("#txt_itemCount").val();
            if (taskCount == 0) { alert("至少有一个计划项，量化你的任务！"); return false; }
            var hasMain = false;
            var sumHours = 0;
            for(var i=1;i<=taskCount;i++)
            {
                var type = $("#radio_type1_" + String(i)).attr("checked");
                var itemHours = Number($("#select_hours_" + String(i)).val());
                var itemDoc = $("#txt_item_" + String(i)).val();
                if (type=="checked") hasMain = true;
                sumHours += itemHours;
                if (itemDoc.length < 10) {alert("所有计划项内容都不能少于10个字！");  return false; }
                if (type == "checked" && itemHours == 0) { alert("主要任务项必须有一定耗时！"); return false; }
            }
            if (!hasMain) { alert("必须至少有一个主要任务项！"); return false; }
            if (sumHours < 6) { alert("每日任务耗时不能少于6小时！"); return false; }
            //document.getElementById("form1").submit();
            return true;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_title" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_body" runat="server">
    <div id="nav" style="padding: 5px 5px 5px 5px; ">
        <asp:LinkButton runat="server" ID="lbReturn" Text="返回" OnClick="lbReturn_Click"></asp:LinkButton>
        <asp:Label runat="server" ID="lblDate" Font-Size="18px" Font-Bold="true" ForeColor="#B8860B"></asp:Label>
    </div>
    <div id="div_mainPlan">
        <fieldset style="padding: 10px 10px 10px 10px">
            <legend>今日计划简介</legend>
            <asp:TextBox runat="server" id="txtTodayDesc" Width="98%" Height="50px" TextMode="MultiLine"></asp:TextBox>
            <asp:HiddenField runat="server" id="txt_itemCount" />
        </fieldset>
        <asp:Repeater runat="server" ID="rptTasks">
            <ItemTemplate>
                <div id="div_task_<%# Eval("Sort") %>">
                    <div style="padding: 5px 5px 1px 5px; margin: 5px 5px 1px 5px">
                        <span id="span_no_<%# Eval("Sort") %>"><%# GetTitleNo(Eval("Sort")) %></span>&nbsp;&nbsp;&nbsp;&nbsp;
                        <input type="radio" id="radio_type0_<%# Eval("Sort") %>" name="radio_type_<%# Eval("Sort") %>" value="0"  <%# Convert.ToInt32(Eval("Type"))==0?"checked='checked'":"" %> />可选任务
                        <input type="radio" id="radio_type1_<%# Eval("Sort") %>" name="radio_type_<%# Eval("Sort") %>" value="1"  <%# Convert.ToInt32(Eval("Type"))==1?"checked='checked'":"" %> />主要任务
                        &nbsp;&nbsp;&nbsp;&nbsp; 耗时:&nbsp;&nbsp;<select id="select_hours_<%# Eval("Sort") %>" name="select_hours_<%# Eval("Sort") %>"
                            style="width: 100px;">
                            <option value="0"  <%# Convert.ToInt32(Eval("Hours"))==0?"selected='selected'":"" %> >不用时间</option>
                            <option value="1"  <%# Convert.ToInt32(Eval("Hours"))==1?"selected='selected'":"" %>>1小时</option>
                            <option value="2"  <%# Convert.ToInt32(Eval("Hours"))==2?"selected='selected'":"" %>>2小时</option>
                            <option value="3"  <%# Convert.ToInt32(Eval("Hours"))==3?"selected='selected'":"" %>>3小时</option>
                            <option value="4"  <%# Convert.ToInt32(Eval("Hours"))==4?"selected='selected'":"" %>>4小时</option>
                            <option value="5"  <%# Convert.ToInt32(Eval("Hours"))==5?"selected='selected'":"" %>>5小时</option>
                            <option value="6"  <%# Convert.ToInt32(Eval("Hours"))==6?"selected='selected'":"" %>>6小时</option>
                            <option value="7"  <%# Convert.ToInt32(Eval("Hours"))==7?"selected='selected'":"" %>>7小时</option>
                            <option value="8"  <%# Convert.ToInt32(Eval("Hours"))==8?"selected='selected'":"" %>>8小时</option>
                        </select>&nbsp;&nbsp;&nbsp;&nbsp;<input class="addTask" id="btn_add_<%# Eval("Sort") %>" type="button"
                            value=" + " onclick="addTask()" />
                        <input class="removeTask" id="btn_remove_<%# Eval("Sort") %>" type="button" value=" - " onclick="removeTask()"
                            style="display: none;" /></div>
                    <div style="padding: 1px 5px 5px 5px; margin: 1px 5px 5px 5px">
                        <textarea name="txt_item_<%# Eval("Sort") %>" id="txt_item_<%# Eval("Sort") %>" style="width: 98%" rows="2"><%# Eval("Task") %></textarea>
                    </div>
                </div>
            </ItemTemplate>
        </asp:Repeater>
    </div>
    <div style="text-align: center;">
        <!--input type="button" value="提交" name="btnSubmit" onclick="preSubmit()" /-->
        <asp:Button runat="server" Text="提交" OnClick="btnSubmit_Onclick" OnClientClick="return preSubmit()" />
     </div>
</asp:Content>
