﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CCOA.App.PrivPlan
{
    public partial class Start : System.Web.UI.Page
    {
        #region //1.公共接口(Public Interface)
        //权限控制
        private string FuncNo = null;
        private string CurUserNo
        {
            get { return BP.Web.WebUser.No; }
        }
        private int StartMyPlan()
        {
            return BP.OA.PrivPlan.PrivPlan.StartPlan(BP.Web.WebUser.No);
        }
        #endregion

        #region //2.页面装载(Page Load)
        protected void Page_Init(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!BP.OA.GPM.IsCanUseFun(this.FuncNo)) { BP.OA.GPM.RedirectNoAccess(); return; }
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this.LoadData();
            }
        }
        private void LoadData()
        {
        }
        #endregion

        #region //3.页面事件(Page Event)
        protected void StartPrivPlan(object sender, EventArgs e)
        {
            int iR = this.StartMyPlan();
            if (iR > 0)
            {
                Response.Redirect("~/App/PrivPlan/MyPlan.aspx",true);
            }
            else
            {
                BP.OA.Debug.Alert(this,"开启失败！请联系管理员！");
            }
        }
        #endregion
    }
}