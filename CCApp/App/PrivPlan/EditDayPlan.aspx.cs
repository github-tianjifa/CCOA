﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using BP.OA.PrivPlan;
namespace CCOA.App.PrivPlan
{
    public partial class EditDayPlan : System.Web.UI.Page
    {
        #region //1.公共接口(Public Interface)
        private string[] ChineseNo = new String[] { "一", "二", "三", "四", "五", "六", "七", "八", "九", "十" };
        //权限控制
        private string FuncNo = null;
        private DateTime rDate
        {
            get 
            {
                string sDate = Convert.ToString(Request.QueryString["Date"]);
                if (String.IsNullOrEmpty(sDate))
                    return DateTime.Now;
                else
                    return Convert.ToDateTime(sDate);
            }
        }
        public int TheLastIndex = 1;
        public int TheTopIndex = 10;
        private string CurUserNo
        {
            get { return BP.Web.WebUser.No; }
        }
        #endregion

        #region //2.页面装载(Page Load)
        protected void Page_Init(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!BP.OA.GPM.IsCanUseFun(this.FuncNo)) { BP.OA.GPM.RedirectNoAccess(); return; }
            }
            else
            {
                if (this.Request.Form.Count > 0)
                {
                    //Response.Write(this.Request.Form.Count.ToString());
                    //this.btnSubmit_Onclick(sender, e);
                }
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this.lblDate.Text = String.Format("&nbsp;&nbsp;{0:dddd}&nbsp;&nbsp;{0:yyyy年MM月dd日}", this.rDate);   
                this.LoadData();
            }
        }
        private void LoadData()
        {
            BP.OA.PrivPlan.PrivPlan pp = new BP.OA.PrivPlan.PrivPlan();
            bool blnR = pp.RetrieveByAttrAnd(PrivPlanAttr.FK_UserNo, BP.Web.WebUser.No, PrivPlanAttr.PlanDate, String.Format("{0:yyyy-MM-dd}", this.rDate));
            if (blnR)
            {
                this.txtTodayDesc.Text = pp.MyDoc;
                string sql = String.Format("select Sort,Task,Hours,Score,Reason,Succeed,Improve,PrivPlanItemType as Type from OA_PrivPlanItem where FK_PrivPlan='{0}' order by Sort", pp.OID);
                System.Data.DataTable dt = BP.DA.DBAccess.RunSQLReturnTable(sql);
                this.rptTasks.DataSource = dt;
                this.rptTasks.DataBind();
                this.txt_itemCount.Value = dt.Rows.Count.ToString();
            }
            else
            {
                this.txtTodayDesc.Text = "请填写计划简介";
                DataTable dt = new DataTable();
                dt.Columns.Add("Sort");
                dt.Columns.Add("Type");
                dt.Columns.Add("Hours");
                dt.Columns.Add("Task");
                for (int i = 1; i <= this.TheLastIndex; i++)
                {
                    dt.Rows.Add(i, 1, 2, "请填写任务说明！");
                }
                this.rptTasks.DataSource = dt;
                this.rptTasks.DataBind();
                this.txt_itemCount.Value = dt.Rows.Count.ToString();
            }
        }
        public string GetTitleNo(object evalNo)
        {
            int i = Convert.ToInt32(evalNo);
            return String.Format("第{0}项", this.ChineseNo[i - 1]);
        }
        #endregion

        #region //3.页面事件(Page Event)
        protected void btnSubmit_Onclick(object sender, EventArgs e)
        {
            //0.用户权限限制
            if (!BP.OA.GPM.IsCanUseFun(this.FuncNo)) { BP.OA.GPM.RedirectNoAccess(); return; }
            //1.获取用户变量
            string planDoc = this.txtTodayDesc.Text;
            int count = Convert.ToInt32(this.txt_itemCount.Value);
            //2.数据验证      
            if (String.IsNullOrEmpty(planDoc))
            {
                //BP.OA.Debug.Alert(this, "计划简介不能为空，且不能少于10个字！");
                BP.OA.Debug.Alert(this, "计划简介不能为空！");
                return;
            }
            if (count == 0)
            {
                BP.OA.Debug.Alert(this, "计划项不能为空！");
                return;
            }
            bool hasMainTask = false;
            int hoursSum = 0;
            for (int i = 1; i <= count; i++)
            {
                int itemType = Convert.ToInt32(this.Request.Form[String.Format("radio_type_{0}", i)]);
                int itemHours = Convert.ToInt32(this.Request.Form[String.Format("select_hours_{0}", i)]);
                String itemDoc = Convert.ToString(this.Request.Form[String.Format("txt_item_{0}", i)]);                                
                hoursSum += itemHours;
                if (itemType == 1) hasMainTask = true;
                if (hasMainTask && itemHours == 0)
                {
                    BP.OA.Debug.Alert(this, "计划主要任务项必须设置所用时间！");
                    return;
                }
                //if (String.IsNullOrEmpty(itemDoc) || itemDoc.Trim().Length < 10)
                if (String.IsNullOrEmpty(itemDoc))
                {
                    BP.OA.Debug.Alert(this, "所有计划任务的要求不能为空！");
                    return;
                }
            }
            if (!hasMainTask)
            {
                BP.OA.Debug.Alert(this, "每日计划任务项不能没有主要任务项！");
                return;
            }
            if (hoursSum < 6)
            {
                BP.OA.Debug.Alert(this, "计划任务所用时间总和不得小于6个小时！");
                return;
            }
            //3.数据处理
            BP.OA.PrivPlan.PrivPlan pp = new BP.OA.PrivPlan.PrivPlan();
            bool blnR = pp.RetrieveByAttrAnd(PrivPlanAttr.FK_UserNo, BP.Web.WebUser.No, PrivPlanAttr.PlanDate, String.Format("{0:yyyy-MM-dd}", this.rDate));

            bool blnOper = false;
            if (!blnR)
            {
                pp.AddTime = DateTime.Now;
                pp.PlanDate = String.Format("{0:yyyy-MM-dd}", this.rDate);
                pp.MyDoc = planDoc;
                pp.FK_UserNo = BP.Web.WebUser.No;
                pp.Checked = false;
                pp.Insert();
                blnOper = pp.OID != 0;
            }
            else
            {
                pp.MyDoc = planDoc;
                int iR = pp.Update();
                blnOper = iR > 0;
            }

            if (!blnOper)
            {
                BP.OA.Debug.Alert(this, "计划提交失败！");
                return; 
            }

            for (int i = 1; i <= count; i++)
            {
                int itemType = Convert.ToInt32(this.Request.Form[String.Format("radio_type_{0}", i)]);
                int itemHours = Convert.ToInt32(this.Request.Form[String.Format("select_hours_{0}", i)]);
                String itemDoc = Convert.ToString(this.Request.Form[String.Format("txt_item_{0}", i)]);
                BP.OA.PrivPlan.PrivPlanItem ppi = new PrivPlanItem();
                bool blnR_ppi = ppi.RetrieveByAttrAnd(PrivPlanItemAttr.FK_PrivPlan, pp.OID, PrivPlanItemAttr.Sort, i);
                if (!blnR_ppi)
                {
                    ppi.FK_PrivPlan = pp.OID;
                    ppi.AddTime = DateTime.Now;
                    ppi.PrivPlanItemType = itemType;
                    ppi.Hours = itemHours;
                    ppi.Score = 0;
                    ppi.Sort = i;
                    ppi.Succeed = false;
                    ppi.Task = itemDoc;
                    ppi.Insert();
                }
                else
                {
                    ppi.Hours = itemHours;
                    ppi.Task = itemDoc;
                    ppi.PrivPlanItemType = itemType;
                    ppi.Update();
                }
            }
            //4.统计/日志
            //5.提交信息
            //BP.OA.Debug.Alert(this, "计划提交成功！");
            //this.LoadData();
            this.Response.Redirect(String.Format("MyPlan.aspx?Date={0:yyyy-MM-dd}",this.rDate));
        }
        protected void lbReturn_Click(object sender, EventArgs e)
        {
            this.Response.Redirect(String.Format("MyPlan.aspx?Date={0:yyyy-MM-dd}", this.rDate));
        }        
        #endregion
    }
}