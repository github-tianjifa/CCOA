﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BP.OA.PrivPlan;
namespace CCOA.App.PrivPlan
{
    public partial class NewTodayPlan : System.Web.UI.Page
    {
        #region //1.公共接口(Public Interface)
        //权限控制
        private string FuncNo = null;
        private DateTime rDate
        {
            get 
            {
                string sDate = Convert.ToString(Request.QueryString["Date"]);
                if (String.IsNullOrEmpty(sDate))
                    return DateTime.Now;
                else
                    return Convert.ToDateTime(sDate);
            }
        }
        private string CurUserNo
        {
            get { return BP.Web.WebUser.No; }
        }
        #endregion

        #region //2.页面装载(Page Load)
        protected void Page_Init(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!BP.OA.GPM.IsCanUseFun(this.FuncNo)) { BP.OA.GPM.RedirectNoAccess(); return; }
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this.LoadData();
            }
        }
        private void LoadData()
        {
            this.lblDate.Text = String.Format("{0:yyyy年MM月dd日}",this.rDate);
        }
        #endregion

        #region //3.页面事件(Page Event)
        protected void btnSubmit_Onclick(object sender, EventArgs e)
        {
            //0.用户权限限制
            if (!BP.OA.GPM.IsCanUseFun(this.FuncNo)) { BP.OA.GPM.RedirectNoAccess(); return; }
            //1.获取用户变量
            string planDoc = this.txtTodayDesc.Text;
            int count = Convert.ToInt32(this.Request.Form["txt_itemCount"]);
            //2.数据验证      
            if (String.IsNullOrEmpty(planDoc)||planDoc.Trim().Length<10)
            {
                BP.OA.Debug.Alert(this, "计划简介不能为空，且不能少于10个字！");
                return;
            }
            if (count == 0)
            {
                BP.OA.Debug.Alert(this, "计划项不能为空！");
                return;
            }
            bool hasMainTask = false;
            int hoursSum = 0;
            for (int i = 1; i <= count; i++)
            {
                int itemType = Convert.ToInt32(this.Request.Form[String.Format("radio_type_{0}", i)]);
                int itemHours = Convert.ToInt32(this.Request.Form[String.Format("select_hours_{0}", i)]);
                String itemDoc = Convert.ToString(this.Request.Form[String.Format("txt_item_{0}", i)]);                                
                hoursSum += itemHours;
                if (itemType == 1) hasMainTask = true;
                if (hasMainTask && itemHours == 0)
                {
                    BP.OA.Debug.Alert(this, "计划主要任务项必须设置所用时间！");
                    return;
                }
                if (String.IsNullOrEmpty(itemDoc) || itemDoc.Trim().Length < 10)
                {
                    BP.OA.Debug.Alert(this, "所有计划任务的要求不能为空，最少10个字，且最好要量化！");
                    return;
                }
            }
            if (!hasMainTask)
            {
                BP.OA.Debug.Alert(this, "每日计划任务项不能没有主要任务项！");
                return;
            }
            if (hoursSum < 6)
            {
                BP.OA.Debug.Alert(this, "计划任务所用时间总和不得小于6个小时！");
                return;
            }
            //3.数据处理
            BP.OA.PrivPlan.PrivPlan pp = new BP.OA.PrivPlan.PrivPlan();
            pp.AddTime = DateTime.Now;
            pp.PlanDate = String.Format("{0:yyyy-MM-dd}", DateTime.Now);
            pp.MyDoc = planDoc;
            pp.FK_UserNo = BP.Web.WebUser.No;
            pp.Checked = false;
            pp.Insert();
            if (pp.OID == 0)
            {
                BP.OA.Debug.Alert(this, "计划添加失败！");
                return; 
            }
            for (int i = 1; i <= count; i++)
            {
                int itemType = Convert.ToInt32(this.Request.Form[String.Format("radio_type_{0}", i)]);
                int itemHours = Convert.ToInt32(this.Request.Form[String.Format("select_hours_{0}", i)]);
                String itemDoc = Convert.ToString(this.Request.Form[String.Format("txt_item_{0}", i)]);
                BP.OA.PrivPlan.PrivPlanItem ppi = new PrivPlanItem();
                ppi.FK_PrivPlan = pp.OID;
                ppi.AddTime = DateTime.Now;
                ppi.Hours = itemHours;
                ppi.Score = 0;
                ppi.Sort = i;
                ppi.Succeed = false;
                ppi.Task = itemDoc;
                ppi.Insert();
            }
            //4.统计/日志
            //5.提交信息
            BP.OA.Debug.Alert(this, "计划添加成功！");
        }
        #endregion
    }
}