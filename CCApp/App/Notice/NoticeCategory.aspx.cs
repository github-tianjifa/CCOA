﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
namespace CCOA.App.Notice
{
    public partial class NoticeCategory : System.Web.UI.Page
    {
        #region //PI
        private int _Cn_Mode = 1; //0-ConnectionString,1-AppSettings
        private string _Cn_Name = "AppCenterDSN";
        private string _Table = "OA_NoticeCategory";
        private string _ID_Fld = "No";
        private string _Name_Fld = "Name";
        #endregion

        #region //Load Part
        protected void Page_Load(object sender, EventArgs e)
        {
            this.LoadCtrl();
        }
        private void LoadCtrl()
        {
            if (_Cn_Mode == 0)
                this.SqlDataSource1.ConnectionString = ConfigurationManager.ConnectionStrings[_Cn_Name].ToString();
            else
                this.SqlDataSource1.ConnectionString = ConfigurationManager.AppSettings[_Cn_Name].ToString();
            this.SqlDataSource1.SelectCommand = String.Format("SELECT [{0}] as No, [{1}] as Name FROM [{2}] Order By {0}", _ID_Fld, _Name_Fld, _Table);
            this.SqlDataSource1.UpdateCommand = String.Format("UPDATE [{2}] SET [{1}] = @Name WHERE [{0}] = @No", _ID_Fld, _Name_Fld,_Table);
            this.SqlDataSource1.InsertCommand = String.Format("INSERT INTO [{2}] ([{0}],[{1}]) VALUES (@No,@Name)", _ID_Fld, _Name_Fld,_Table);
            this.SqlDataSource1.DeleteCommand = String.Format("DELETE FROM [{1}] WHERE [{0}] = @No", _ID_Fld, _Table);
        }
        #endregion

        #region //Events
        #endregion
    }
}