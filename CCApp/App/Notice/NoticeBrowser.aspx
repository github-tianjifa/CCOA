﻿<%@ Page Language="C#" MasterPageFile="../../Main/master/Site1.Master" AutoEventWireup="true"
    CodeBehind="NoticeBrowser.aspx.cs" Inherits="CCOA.App.NOTICE.NoticeBrowser" %>

<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="cph_head">
    <style type="text/css">
        body
        {
            font-size: 13px;
        }
    </style>
    <script type="text/javascript">
        function $(xixi) {
            return document.getElementById(xixi);
        }
        //转换字号
        function doZoom(size) {
            if (size == 12) {
                $("contentText").style.fontSize = size + "px";
                $("fs12").style.display = "";
                $("fs14").style.display = "none";
                $("fs16").style.display = "none";
            }
            if (size == 14) {
                $("contentText").style.fontSize = size + "px";
                $("fs12").style.display = "none";
                $("fs14").style.display = "";
                $("fs16").style.display = "none";
            }
            if (size == 16) {
                $("contentText").style.fontSize = size + "px";
                $("fs12").style.display = "none";
                $("fs14").style.display = "none";
                $("fs16").style.display = "";
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="cph_title">
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="cph_body">
    <div style="position: absolute;">
        <img src="img/Notice.png" />
    </div>
    <div style="">
        <p style="font-size: x-large; text-align: center; font-weight: bolder;">
            <asp:Literal runat="server" ID="liTitle"></asp:Literal></p>
        <p style="text-align: center;">
            发布者：<asp:Literal runat="server" ID="liUser"></asp:Literal>&nbsp;&nbsp;&nbsp;&nbsp;
            发布时间：<asp:Literal runat="server" ID="liRDT"></asp:Literal></p>
            <div style="margin:0 auto;width:900px;text-align:center; ">
              <div class="fontSize" id="fs12" style="display: none;margin:0 auto;">字体:
            <a href="javascript:doZoom(16)">大</a> <a href="javascript:doZoom(14)">中</a> <span>小</span></div>
       <div class="fontSize" id="fs14">字体:
            <a href="javascript:doZoom(16)">大</a> <span>中</span> <a href="javascript:doZoom(12)">
                小</a></div>
        <div class="fontSize" id="fs16" style="display: none">字体:
            <span>大</span> <a href="javascript:doZoom(14)">中</a> <a href="javascript:doZoom(12)">
                小</a></div>

            </div>
        <div id="contentText" style="margin: 10x 10px 10px 10px; padding: 10px 10px 10px 10px;
            border: dashed 1px #dddddd; height: 430px; background-color: write;">
            <asp:Literal runat="server" ID="liDoc"></asp:Literal>
        </div>
        <asp:Panel runat="server" ID="pnAttachFile" Visible="false">
            <img src="img/attach1.png" style="height: 18px;">附件下载(右击另存下载)：<asp:Literal runat="server"
                ID="liAttachFiles"></asp:Literal>
        </asp:Panel>
    </div>
</asp:Content>
