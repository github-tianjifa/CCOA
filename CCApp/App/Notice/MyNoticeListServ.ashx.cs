﻿using BP.En;
using BP.Sys;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using BP.OA;
using BP.DA;
namespace CCOA.App.Notice
{
    /// <summary>
    /// MyNote1 的摘要说明
    /// </summary>
    public class MyNote1 : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            string action = context.Request["action"];
            switch (action)
            {
                case "query":
                    Query();
                    break;
                case "del":
                    Del();
                    break;
                //case "add":
                //    Add();
                //    break;
                //case "edit":
                //    Edit();
                //    break;
                case "overtop":
                    OverTop();
                    break;
                case "canceltop":
                    CancelTop();
                    break;
            }
        }
        /// <summary>
        /// 取消置顶
        /// </summary>
        private void CancelTop()
        {
            string selectedid = HttpContext.Current.Request["data"];
            string sSql = String.Format("Delete FROM OA_Notice where OID={0}", selectedid);
            int iR = BP.DA.DBAccess.RunSQL(sSql);   
        }
        /// <summary>
        /// 置顶
        /// </summary> 
        private void OverTop()
        {
            string selectedid = HttpContext.Current.Request["data"];
            BP.OA.Notice nt = new BP.OA.Notice(Int32.Parse(selectedid));
            DateTime now = DateTime.Now;
            now = Convert.ToDateTime(String.Format("{0:yyyy-MM-dd HH:mm:ss}", now));
            nt.SetTop = now; 
            nt.RDT = now;
            nt.Update();
        }
        /// <summary>
        /// 查询的方法
        /// </summary>
        private void Query()
        {
            //一页显示几行数据
            string rows = HttpContext.Current.Request["rows"];
            //当前页
            string page = HttpContext.Current.Request["page"];
            //获取数据源
            BP.OA.Notices nes = new BP.OA.Notices();
            nes.RetrieveAll();
            nes.ToDataTableDescField();
            int total = nes.Count;
            string str = string.Empty;
            QueryObject obj = new QueryObject(nes);
            string rbselected = HttpContext.Current.Request["rbselected"];
            obj.AddWhere("1=1");
            if (rbselected != "undefined")
            {
                if (rbselected ==""||rbselected=="0")
                {

                }
                else
                {
                    obj.AddWhere(String.Format(" and FK_NoticeCategory='{0}'", rbselected));
                }
            }
            //if (!String.IsNullOrEmpty(keyWords))
            //{
            //    obj.AddWhere(String.Format(" and Title LIKE '%{0}%' OR KeyWords like '%{0}%'", keyWords));
            //}
            obj.addOrderByDesc(NoticeAttr.SetTop, NoticeAttr.RDT);
            obj.DoQuery("OID", Int32.Parse(rows), Int32.Parse(page));
            DataSet ds = nes.ToDataSet();
            DataTable dt = ds.Tables[0];
            //将数据转换成json格式
            str = CreateJsonParameters(dt, true, total);
            HttpContext.Current.Response.Write(str);
        }
        /// <summary>
        /// 批量删除
        /// </summary>
        /// <param name="tabName"></param>
        /// <param name="ID"></param>
        /// <returns></returns>
        public static int DelData(string tabName, string ID)
        {
            if (ID != string.Empty && ID != "0")
            {
                string sql = string.Format("delete from {0}  WHERE (OID IN ({1}))", tabName, ID);
                int delNum = DBAccess.RunSQL(sql);
                return delNum;
            }
            return 0;
        }
        //删除的方法
        private void Del()
        {
            //获取到选中行的id
            BP.OA.Notices nes = new BP.OA.Notices();
            nes.RetrieveAll();
            string id = HttpContext.Current.Request["id"];
            int count = 0;
            count = DelData("oa_notice", id);
            if (count > 0)
            {
                HttpContext.Current.Response.Write("共删除了" + count + "条数据");
            }
            else
            {
                HttpContext.Current.Response.Write("error");
            }
        }
        public static string CreateJsonParameters(DataTable dt, bool displayCount, int totalcount)
        {
            StringBuilder JsonString = new StringBuilder();
            //Exception Handling        
            if (dt != null)
            {
                JsonString.Append("{ ");
                if (displayCount)
                {
                    JsonString.Append("\"total\":");
                    JsonString.Append(totalcount);
                    JsonString.Append(",");
                }
                JsonString.Append("\"rows\":[ ");
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    JsonString.Append("{ ");
                    for (int j = 0; j < dt.Columns.Count; j++)
                    {
                        if (j < dt.Columns.Count - 1)
                        {
                            //if (dt.Rows[i][j] == DBNull.Value) continue;
                            if (dt.Columns[j].DataType == typeof(bool))
                            {
                                JsonString.Append("\"JSON_" + dt.Columns[j].ColumnName.ToLower() + "\":" +
                                                  dt.Rows[i][j].ToString().ToLower() + ",");
                            }
                            else if (dt.Columns[j].DataType == typeof(string))
                            {
                                JsonString.Append("\"JSON_" + dt.Columns[j].ColumnName.ToLower() + "\":" + "\"" +
                                                  dt.Rows[i][j].ToString().Replace("\"", "\\\"") + "\",");
                            }
                            else
                            {
                                JsonString.Append("\"JSON_" + dt.Columns[j].ColumnName.ToLower() + "\":" + "\"" + dt.Rows[i][j] + "\",");
                            }
                        }
                        else if (j == dt.Columns.Count - 1)
                        {
                            //if (dt.Rows[i][j] == DBNull.Value) continue;
                            if (dt.Columns[j].DataType == typeof(bool))
                            {
                                JsonString.Append("\"JSON_" + dt.Columns[j].ColumnName.ToLower() + "\":" +
                                                  dt.Rows[i][j].ToString().ToLower());
                            }
                            else if (dt.Columns[j].DataType == typeof(string))
                            {
                                JsonString.Append("\"JSON_" + dt.Columns[j].ColumnName.ToLower() + "\":" + "\"" +
                                                  dt.Rows[i][j].ToString().Replace("\"", "\\\"") + "\"");
                            }
                            else
                            {
                                JsonString.Append("\"JSON_" + dt.Columns[j].ColumnName.ToLower() + "\":" + "\"" + dt.Rows[i][j] + "\"");
                            }
                        }
                    }
                    /*end Of String*/
                    if (i == dt.Rows.Count - 1)
                    {
                        JsonString.Append("} ");
                    }
                    else
                    {
                        JsonString.Append("}, ");
                    }
                }
                JsonString.Append("]");
                JsonString.Append("}");
                return JsonString.ToString().Replace("\n", "");
            }
            else
            {
                return null;
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}