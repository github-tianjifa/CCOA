﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App/Mobile.Master" AutoEventWireup="true" CodeBehind="NoticeBrowserMobile.aspx.cs" Inherits="CCOA.App.Notice.NoticeBrowserMobile" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../../Js/Js_EasyUI/themes/gray/easyui.css" rel="stylesheet" type="text/css" />
    <link href="../../Js/Js_EasyUI/themes/icon.css" rel="stylesheet" type="text/css" />
    <script src="../../Js/jquery-1.7.2.min.js" type="text/javascript"></script>
    <script src="../../Js/Js_EasyUI/jquery.easyui.min.js" type="text/javascript"></script>
    <script src="../../Js/Js_EasyUI/locale/easyui-lang-zh_CN.js" type="text/javascript"></script>
    <link href="../../CCMobile/css/themes/classic/theme-classic.css" rel="stylesheet" type="text/css" />
    <script src="../../CCMobile/js/jquery.js" type="text/javascript"></script>
    <script src="../../CCMobile/js/jquery.mobile-1.4.5.min.js" type="text/javascript"></script>
    <script src="../../CCMobile/js/comment/action.js" type="text/javascript"></script>
<style type="text/css">
        body
        {
            font-size: 13px;
        }
    </style>
    <script type="text/javascript">
        function $(xixi) {
            return document.getElementById(xixi);
        }
        //转换字号
        function doZoom(size) {
            if (size == 12) {
                $("contentText").style.fontSize = size + "px";
                $("fs12").style.display = "";
                $("fs14").style.display = "none";
                $("fs16").style.display = "none";
            }
            if (size == 14) {
                $("contentText").style.fontSize = size + "px";
                $("fs12").style.display = "none";
                $("fs14").style.display = "";
                $("fs16").style.display = "none";
            }
            if (size == 16) {
                $("contentText").style.fontSize = size + "px";
                $("fs12").style.display = "none";
                $("fs14").style.display = "none";
                $("fs16").style.display = "";
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div data-role="page" data-theme="c">
 <div data-role="header" data-theme="b">
            <a href="NoticeListMobile.aspx" data-icon="black" data-role="button ">返回</a>
        </div>

    <div data-role="main" >
        <p style="font-size: x-large; text-align: center; font-weight: bolder;">
            <asp:Literal runat="server" ID="liTitle"></asp:Literal></p>
        <p style="text-align: center;">
            发布者：<asp:Literal runat="server" ID="liUser"></asp:Literal>&nbsp;&nbsp;&nbsp;&nbsp;
            发布时间：<asp:Literal runat="server" ID="liRDT"></asp:Literal></p>
            <%--<div style="margin:0 auto;text-align:center; ">
              <div class="fontSize" id="fs12" style="display: none;margin:0 auto;">字体:
            <a href="javascript:doZoom(16)">大</a> <a href="javascript:doZoom(14)">中</a> <span>小</span></div>
       <div class="fontSize" id="fs14">字体:
            <a href="javascript:doZoom(16)">大</a> <span>中</span> <a href="javascript:doZoom(12)">
                小</a></div>
        <div class="fontSize" id="fs16" style="display: none">字体:
            <span>大</span> <a href="javascript:doZoom(14)">中</a> <a href="javascript:doZoom(12)">
                小</a></div>

            </div>--%>
        <div id="contentText" style="margin: 10x 10px 10px 10px; padding: 10px 10px 10px 10px;
            border: dashed 1px #dddddd; height: 430px; background-color: write;">
            <asp:Literal runat="server" ID="liDoc"></asp:Literal>
        </div>
        <asp:Panel runat="server" ID="pnAttachFile" Visible="false">
            <img src="img/attach1.png" style="height: 18px;">附件下载(右击另存下载)：<asp:Literal runat="server"
                ID="liAttachFiles"></asp:Literal>
        </asp:Panel>
    </div>
    </div>
</asp:Content>
