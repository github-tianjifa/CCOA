﻿using BP.En;
using BP.OA;
using BP.Sys;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace CCOA.App.Notice
{
    /// <summary>
    /// MyNoticeList 的摘要说明
    /// </summary>
    public class MyNoticeList : IHttpHandler
    {
        /// <summary>
        /// 封装有关个别 HTTP 请求的所有 HTTP 特定的信息
        /// </summary>
        HttpContext _Context = null;

        public string getUTF8ToString(string param)
        {
            return HttpUtility.UrlDecode(_Context.Request[param], System.Text.Encoding.UTF8);
        }

        public void ProcessRequest(HttpContext context)
        {
            _Context = context;
            //if (BP.Web.WebUser.No == null)
            //    return;
            string method = string.Empty;
            //返回值
            string s_responsetext = string.Empty;
            if (!string.IsNullOrEmpty(_Context.Request["method"]))
                method = _Context.Request["method"].ToString();
            switch (method)
            {
                case "getnewslist"://获取列表
                    s_responsetext = GetNewsList();
                    break;
            }
            if (string.IsNullOrEmpty(s_responsetext))
                s_responsetext = "";
            //组装ajax字符串格式,返回调用客户端
            _Context.Response.Charset = "UTF-8";
            _Context.Response.ContentEncoding = System.Text.Encoding.UTF8;
            _Context.Response.ContentType = "text/html";
            _Context.Response.Expires = 0;
            _Context.Response.Write(s_responsetext);
            _Context.Response.End();
        }
        private string GetNewsList()
        {
            string con = String.Empty;
            string sSql = String.Format("select G.Name as UserName,F.Name as Dept,A.OID,A.Title,B.Name as NoticeCategory,RDT,C.Lab as Importance,A.Importance as ImportLevel,NoticeSta,d.Lab as NoticeStaTitle,SetTop"
                                 + " ,case when E.ReadTime='' then '0' else '1' end as ReadState"
                                 + " ,case when E.ReadTime='' then '0' else '1' end as ReadStateFlag"
                                 + ",case when NoticeSta=0 and GetDate() between StartTime and StopTime then cast(DateDiff(d,GetDate(),StopTime) as varchar)+'天后关闭'"
                                 + "        when NoticeSta=0 and GetDate() < StartTime then cast(DateDiff(d,GetDate(),StartTime) as varchar)+'天后打开'"
                                 + "        when NoticeSta=0 and GetDate() > StopTime then '已过期'+cast(DateDiff(d,StopTime,GetDate()) as varchar)+'天'"
                                 + "        when NoticeSta=1 then '手工打开'"
                                 + "        when NoticeSta=2 then '手工关闭'  End as PublishState"
                                 + " from OA_Notice A"
                                 + " inner join OA_NoticeCategory B on B.No=A.FK_NoticeCategory"
                                 + " inner join OA_NoticeReader E on E.FK_NoticeNo=A.OID"
                                 + " inner join Sys_Enum C on C.IntKey=A.Importance and C.EnumKey='Importance'"
                                 + " inner join Sys_Enum D on D.IntKey=A.NoticeSta and D.EnumKey='NoticeSta'"
                                 + " left join Port_Emp G on G.No=A.FK_UserNo"
                                 + " left join Port_Dept F on F.No=A.FK_Dept"
                                 //+ " where E.FK_UserNo='{0}'", BP.Web.WebUser.No);
                                 + "   where E.FK_UserNo='{0}' and A.NoticeSta=1"
                                 + "      or E.FK_UserNo='{0}' and A.NoticeSta=0"
                                 + "         and GetDate() between A.StartTime and A.StopTime"
                                 , BP.Web.WebUser.No);
            if (BP.Sys.SystemConfig.AppCenterDBType == BP.DA.DBType.MySQL)
            {
                sSql = String.Format("select  G.Name as UserName,F.Name as Dept,A.OID,A.Title,B.Name as NoticeCategory,RDT,C.Lab as Importance,A.Importance as ImportLevel,NoticeSta,d.Lab as NoticeStaTitle,SetTop"
                                                + " ,case when E.ReadTime='' then '0' else '1' end as ReadState"
                                                + " ,case when E.ReadTime='' then '0' else '1' end as ReadStateFlag"
                                                + ",case when NoticeSta=0 and GetDate() between StartTime and StopTime then CONCAT(DateDiff(GetDate(),StopTime),'天后关闭')"
                                                + "        when NoticeSta=0 and GetDate() < StartTime then CONCAT(DateDiff(GetDate(),StartTime),'天后打开')"
                                                + "        when NoticeSta=0 and GetDate() > StopTime then CONCAT('已过期',DateDiff(StopTime,GetDate()),'天')"
                                                + "        when NoticeSta=1 then '手工打开'"
                                                + "        when NoticeSta=2 then '手工关闭'  End as PublishState"
                                                + " from OA_Notice A"
                                                + " inner join OA_NoticeCategory B on B.No=A.FK_NoticeCategory"
                                                + " inner join OA_NoticeReader E on E.FK_NoticeNo=A.OID"
                                                + " inner join Sys_Enum C on C.IntKey=A.Importance and C.EnumKey='Importance'"
                                                + " inner join Sys_Enum D on D.IntKey=A.NoticeSta and D.EnumKey='NoticeSta'"
                                                + " left join Port_Emp G on G.No=A.FK_UserNo"
                                                + " left join Port_Dept F on F.No=A.FK_Dept"
                                                + "   where E.FK_UserNo='{0}' and A.NoticeSta=1"
                                                + "      or E.FK_UserNo='{0}' and A.NoticeSta=0"
                                                + "         and GetDate() between A.StartTime and A.StopTime",BP.Web.WebUser.No
                                              );
            }
                int count = BP.OA.Main.GetPagedRowsCount(sSql);
                if (count == 0)
                {
                    return null;
                }
                
                //当前页
                string pageNumber = getUTF8ToString("pageNumber");
                int iPageNumber = string.IsNullOrEmpty(pageNumber) ? 1 : Convert.ToInt32(pageNumber);
                //每页多少行
                string pageSize = getUTF8ToString("pageSize");
                int iPageSize = string.IsNullOrEmpty(pageSize) ? 9999 : Convert.ToInt32(pageSize);

                BP.OA.Notices notices = new BP.OA.Notices();
                QueryObject obj = new QueryObject(notices);
                //AspNetPager1.RecordCount = count;
                //AspNetPager1.PageSize = 20;
                //AspNetPager1.CurrentPageIndex = 1;
            ;
            return BP.DA.DataTableConvertJson.DataTable2Json(BP.OA.Main.GetPagedRows(sSql, -1, "order by SetTop desc,OID", iPageSize, iPageNumber), count);
        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}