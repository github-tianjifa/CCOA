﻿<%@ Page Language="C#" MasterPageFile="../../Main/master/Site1.Master" AutoEventWireup="true" CodeBehind="NoticeRead.aspx.cs" Inherits="CCOA.App.NOTICE.NoticeRead" %>
<asp:Content ID="Content3" runat="server" contentplaceholderid="cph_head">
</asp:Content>
<asp:Content ID="Content1" runat="server" contentplaceholderid="cph_body">
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="cph_title">
    <div>
        <asp:GridView ID="GridView1" CssClass="grid" runat="server" AutoGenerateColumns="false" Width="98%">
            <Columns>
                <asp:TemplateField HeaderText="用户" ItemStyle-Width="150px">
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%#this.GetUserNames(Eval("FK_UserNo")) %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="已读状态">
                    <ItemTemplate>
                        <%# GetReadState(Eval("AddTime"),Eval("ReadTime")) %>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </div>
</asp:Content>
