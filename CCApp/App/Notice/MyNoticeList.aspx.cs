﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
namespace CCOA.App.NOTICE
{
    public partial class MyNoticeList : System.Web.UI.Page
    {
        #region //1.公共接口(Public Interface)
        //权限控制
        private string FuncNo = null;
        private string CurUserNo
        {
            get { return BP.Web.WebUser.No; }
        }
        private int GetNotReadCount(string userNo)
        {
            string sSql = String.Format("select count(*) from OA_NoticeReader A "
                + "   inner join OA_Notice B on A.FK_NoticeNo=B.OID"
                + "   where A.FK_UserNo='{0}' and B.NoticeSta=1 and ReadTime=''"
                + "      or A.FK_UserNo='{0}' and B.NoticeSta=0 and ReadTime='' "
                + "         and GetDate() between B.StartTime and B.StopTime", userNo);
            int notRead = Convert.ToInt32(BP.DA.DBAccess.RunSQLReturnVal(sSql));
            return notRead;
        }
        private DataTable GetMyPagedNoticeList(bool start)
        {
            string con = String.Empty;
            string sSql = String.Format("select A.FK_UserNo as UserName,FK_Dept as Dept,A.OID,A.Title,B.Name as NoticeCategory,RDT,C.Lab as Importance,A.Importance as ImportLevel,NoticeSta,d.Lab as NoticeStaTitle,SetTop"
                                 + " ,case when E.ReadTime='' then '0' else '1' end as ReadState"
                                 + " ,case when E.ReadTime='' then '0' else '1' end as ReadStateFlag"
                                 + ",case when NoticeSta=0 and GetDate() between StartTime and StopTime then cast(DateDiff(d,GetDate(),StopTime) as varchar)+'天后关闭'"
                                 + "        when NoticeSta=0 and GetDate() < StartTime then cast(DateDiff(d,GetDate(),StartTime) as varchar)+'天后打开'"
                                 + "        when NoticeSta=0 and GetDate() > StopTime then '已过期'+cast(DateDiff(d,StopTime,GetDate()) as varchar)+'天'"
                                 + "        when NoticeSta=1 then '手工打开'"
                                 + "        when NoticeSta=2 then '手工关闭'  End as PublishState"
                                 + " from OA_Notice A"
                                 + " inner join OA_NoticeCategory B on B.No=A.FK_NoticeCategory"
                                 + " inner join OA_NoticeReader E on E.FK_NoticeNo=A.OID"
                                 + " inner join Sys_Enum C on C.IntKey=A.Importance and C.EnumKey='Importance'"
                                 + " inner join Sys_Enum D on D.IntKey=A.NoticeSta and D.EnumKey='NoticeSta'"
                                 //+ " where E.FK_UserNo='{0}'", BP.Web.WebUser.No);
                                 + "   where E.FK_UserNo='{0}' and A.NoticeSta=1"
                                 + "      or E.FK_UserNo='{0}' and A.NoticeSta=0"
                                 + "         and GetDate() between A.StartTime and A.StopTime"
                                 , BP.Web.WebUser.No);
            if (BP.Sys.SystemConfig.AppCenterDBType == BP.DA.DBType.MySQL)
            {
                sSql = String.Format("select A.FK_UserNo as UserName,FK_Dept as Dept,A.OID,A.Title,B.Name as NoticeCategory,RDT,C.Lab as Importance,A.Importance as ImportLevel,NoticeSta,d.Lab as NoticeStaTitle,SetTop"
                                                + " ,case when E.ReadTime='' then '0' else '1' end as ReadState"
                                                + " ,case when E.ReadTime='' then '0' else '1' end as ReadStateFlag"
                                                + ",case when NoticeSta=0 and GetDate() between StartTime and StopTime then CONCAT(DateDiff(GetDate(),StopTime),'天后关闭')"
                                                + "        when NoticeSta=0 and GetDate() < StartTime then CONCAT(DateDiff(GetDate(),StartTime),'天后打开')"
                                                + "        when NoticeSta=0 and GetDate() > StopTime then CONCAT('已过期',DateDiff(StopTime,GetDate()),'天')"
                                                + "        when NoticeSta=1 then '手工打开'"
                                                + "        when NoticeSta=2 then '手工关闭'  End as PublishState"
                                                + " from OA_Notice A"
                                                + " inner join OA_NoticeCategory B on B.No=A.FK_NoticeCategory"
                                                + " inner join OA_NoticeReader E on E.FK_NoticeNo=A.OID"
                                                + " inner join Sys_Enum C on C.IntKey=A.Importance and C.EnumKey='Importance'"
                                                + " inner join Sys_Enum D on D.IntKey=A.NoticeSta and D.EnumKey='NoticeSta'"
                                                + "   where E.FK_UserNo='{0}' and A.NoticeSta=1"
                                                + "      or E.FK_UserNo='{0}' and A.NoticeSta=0"
                                                + "         and GetDate() between A.StartTime and A.StopTime"
                                                , BP.Web.WebUser.No);
            }

            if (start)
            {
                int count = BP.OA.Main.GetPagedRowsCount(sSql);
                if (count == 0 )
                {
                    return null;
                }
                AspNetPager1.RecordCount = count;
                AspNetPager1.PageSize = 20;
                AspNetPager1.CurrentPageIndex = 1;
            }
            this.AspNetPager1.CustomInfoHTML = string.Format("当前第{0}/{1}页 共{2}条记录 每页{3}条", new object[] { this.AspNetPager1.CurrentPageIndex, this.AspNetPager1.PageCount, this.AspNetPager1.RecordCount, this.AspNetPager1.PageSize });
            return BP.OA.Main.GetPagedRows(sSql, -1, "order by CAST(SetTop as datetime) desc,OID desc", AspNetPager1.PageSize, AspNetPager1.CurrentPageIndex);
        }
        //我的公告-最顶上公告阅读提示方案
        private string NotReadTopMsg = "<span style='color:red'>目前未读信息有{0}条！</span>";
        private string ReadTopMsg = "<span style='color:#333'>目前没有未读信息！</span>";
        //我的公告-阅读状态栏格式方案
        private string NotReadStateLabel = "<span style='color:red'>未读</span>";
        private string ReadStateLabel = "<span style='color:Green'>已读</span>";
        //我的公告-标题状态格式方案
        private string NotReadStateTitleStyle = "color:#333333;";
        private string ReadStateTitleStyle = "color:gray;text-decoration:line-through;";
        #endregion
        #region //2.页面装载(Page Load)
        protected void Page_Init(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!BP.OA.GPM.IsCanUseFun(this.FuncNo)) { BP.OA.GPM.RedirectNoAccess(); return; }
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this.BindMyReadState();
                this.BindData(true);
                //清除即时消息
                BP.OA.ShortMsg.ReceivedInfo(null, "公告");
            }
        }
        //绑定数据        
        public void BindData(bool start)
        {
            DataTable dt = this.GetMyPagedNoticeList(start);
            if (dt == null) return;
            this.GridView1.DataSource = dt;
            this.GridView1.DataBind();
        }
        /// <summary>
        /// 取得发布者为用户还是部门
        /// </summary>
        /// <param name="evalUser"></param>
        /// <param name="evalDept"></param>
        /// <returns></returns>
        public string GetPublish(object evalUser, object evalDept)
        {
            if (evalDept == Convert.DBNull || evalDept == null || String.IsNullOrEmpty(Convert.ToString(evalDept)))
                return BP.OA.GPM.GetUserName_By_No(Convert.ToString(evalUser));
            else
                return BP.OA.GPM.GetDeptName_By_No(Convert.ToString(evalDept));
        }
        public string GetTitleFormatByReadState(object evalState)
        {
            if (evalState == null || evalState == Convert.DBNull) return String.Empty;
            int state = Convert.ToInt32(evalState);
            if (state == 0)
                return this.NotReadStateTitleStyle;
            else if (state == 1)
                return this.ReadStateTitleStyle;
            else
                return String.Empty;
        }
        public string GetImportLevel(object evalImportLevel)
        {
            if (evalImportLevel == Convert.DBNull) return null;
            String il = Convert.ToString(evalImportLevel);
            return String.Format("/Images/Notice/Import{0}.png", il);
        }
        public string GetReadState(object evalState)
        {
            if (evalState == null || evalState == Convert.DBNull) return String.Empty;
            int state = Convert.ToInt32(evalState);
            if (state == 0)
                return this.NotReadStateLabel;
            else if (state == 1)
                return this.ReadStateLabel;
            else
                return String.Empty;
        }
        public string GetRelativeTime(object evalTime)
        {
            return BP.OA.Main.GetTimeEslapseStr(Convert.ToDateTime(evalTime), null, null);
        }
        public void BindMyReadState()
        {
            int notRead = this.GetNotReadCount(BP.Web.WebUser.No);
            if (notRead > 0)
                this.liMyReadState.Text = String.Format(this.NotReadTopMsg, notRead);
            else
                this.liMyReadState.Text = String.Format(this.ReadTopMsg);
        }
        #endregion
        #region //3.页面事件(Page Event)
        protected void GridView1_DataBound(object sender, EventArgs e)
        {
            if (GridView1.Rows.Count > 0)
                GridView1.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
        protected void AspNetPager1_PageChanged(object sender, EventArgs e)
        {
            this.BindData(false);
        }
        protected void rbl_Category_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.BindData(false);
        }
        #endregion
    }
}