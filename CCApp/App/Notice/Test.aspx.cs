﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BP.Web.Controls;

namespace CCOA.App.Notice
{
    public partial class Test : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Pub1.AddTable();
            this.Pub1.AddTR();
            this.Pub1.AddTDTitle("ID");
            this.Pub1.AddTDTitle("属性");
            this.Pub1.AddTREnd();
            
            this.Pub1.AddTR();
            this.Pub1.AddTD("标题");
            TB tb = new TB();
            tb.ID = "TB_Title";
            this.Pub1.AddTD(tb);
            this.Pub1.AddTREnd();

            this.Pub1.AddTR();
            this.Pub1.AddTD("doc");
              tb = new TB();
            tb.ID = "TB_Doc";
            tb.TextMode = TextBoxMode.MultiLine;
            this.Pub1.AddTD(tb);
            this.Pub1.AddTREnd();
            this.Pub1.AddTableEnd();

            Button btn = new Button();
            btn.Click += new EventHandler(btn_Click);
            btn.Text = "Save";
        }

        void btn_Click(object sender, EventArgs e)
        {
            BP.OA.Notice en = new BP.OA.Notice();
            en = this.Pub1.Copy(en) as BP.OA.Notice;

            if (en.Title.Length==0)
            {
                BP.Sys.PubClass.Alert("sds");
                return;
            }

            en.Insert();

        }
    }
}