﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;
namespace CCOA.Serv
{
    /// <summary>
    /// SendMsg 的摘要说明
    /// </summary>
    public class SendMsg : IHttpHandler,IRequiresSessionState
    {
        private string rUserNo
        {
            get { return Convert.ToString(HttpContext.Current.Request["userNo"]); }
        }
        private string rText
        {
            get { return Convert.ToString(HttpContext.Current.Request["text"]); }
        }
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            //string f = String.Format("{0}|{1}", rUserNo, rText);
            int i = BP.OA.ShortMsg.Send_ShortInfo(rUserNo, rText, String.Empty,false);
            if (i > 0)
                context.Response.Write("SUCCESS");
            else
                context.Response.Write("FAILD");
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}