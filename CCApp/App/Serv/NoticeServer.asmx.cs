﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using BP.OA;
using BP.Sys;
using BP.Tools;
namespace CCOA.App.Serv
{
    /// <summary>
    /// NoticeServer 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // 若要允许使用 ASP.NET AJAX 从脚本中调用此 Web 服务，请取消注释以下行。 
    // [System.Web.Script.Services.ScriptService]
    public class NoticeServer : System.Web.Services.WebService
    {
        #region 1.公告管理
        /// <summary>
        /// 获取公告类型
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public string GetNoticesCategoryAll()
        {

            NoticeCategorys atcts = new NoticeCategorys();
            atcts.RetrieveAll();
            return Entitis2Json.ConvertEntitis2GridJsonOnlyData(atcts);
        }
        /// <summary>
        /// 根据公告类型获取公告列表
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public string GetNoticesByCategory(string no)
        {
            Notices nts = new Notices();
            nts.RetrieveByAttr(NoticeAttr.FK_NoticeCategory, no);
            return Entitis2Json.ConvertEntitis2GridJsonOnlyData(nts);
        }
        /// <summary>
        /// 根据公告oid获取详细公告
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public string GetNoticeDetailByID(string id)
        {
            //返回数据表格得包含以下字段：Title,KeyWords,NoticeCatagory,AddTime,FK_UserNo,Doc,NoticeSource

            Notices nts = new Notices();
            nts.RetrieveByAttr("OID", id);
            return Entitis2Json.ConvertEntitis2GridJsonOnlyData(nts);
        }
        /// <summary>
        /// 根据公告oid删除
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public string DelNoticeByID(string id)
        {
            BP.OA.Notice nt = new BP.OA.Notice();

            nt.OID = Int32.Parse(id);
            return nt.Delete() > 0 ? "true" : "false";
        }
        //<summary>
        //根据公告多个oid批量删除
        //用“，”隔开
        //</summary>
        //<returns></returns>
        [WebMethod]
        public string DelMutiNoticeByID(string idList)
        {
            string sSql = String.Format("Delete from OA_Notice Where OID in ({0})", idList);
            return BP.DA.DBAccess.RunSQL(sSql) > 0 ? "true" : "false";
        }
        /// <summary>
        /// 根据公告oid编辑
        /// </summary>
        /// <param name="oid"></param>
        /// <param name="Title"></param>
        /// <param name="AttachFile"></param>
        /// <param name="FK_NoticeCategory"></param>
        /// <param name="FK_Dept"></param>
        /// <param name="FK_UserNo"></param>
        /// <param name="SendToPart"></param>
        /// <param name="SendToUsers"></param>
        /// <param name="SendToDepts"></param>
        /// <param name="SendToStations"></param>
        /// <param name="SetTop"></param>
        /// <param name="Importance"></param>
        /// <param name="GetAdvices"></param>
        /// <param name="AdviceDesc"></param>
        /// <param name="AdviceItems"></param>
        /// <param name="NoticeSta"></param>
        /// <param name="StartTime"></param>
        /// <param name="StopTime"></param>
        /// <param name="SetMes"></param>
        /// <returns></returns>
        [WebMethod]
        public string EditNotice(string oid ,string Title,string Doc, string AttachFile,DateTime RDT,
        string FK_NoticeCategory, string FK_Dept, string FK_UserNo, bool SendToPart,
        string SendToUsers, string SendToDepts, string SendToStations, int Importance,
        bool GetAdvices, string AdviceDesc, string AdviceItems, int NoticeSta, DateTime StartTime, DateTime StopTime, int SetMes)
        {
            Notices nts = new Notices();
            nts.RetrieveAll();
            BP.OA.Notice nt = (BP.OA.Notice)nts.Filter(NoticeAttr.OID, oid);
            if (nt != null)
            {
                //nt.Title = Title;
                //nt.AttachFile = AttachFile;
                //nt.FK_NoticeCategory = FK_NoticeCategory;
                //nt.RDT = DateTime.Now;
                //nt.FK_Dept = FK_Dept;
                //nt.FK_UserNo = FK_UserNo;
                //bool r;

                //if (bool.TryParse(SendToPart, out r) == false)
                //{
                //    return "false";
                //}
                //nt.SendToPart = r;
                //nt.SendToUsers = SendToUsers;
                //nt.SendToDepts = SendToDepts;
                //nt.SendToStations = SendToStations;
                //DateTime dt;
                //if (DateTime.TryParse(SetTop, out dt))
                //{
                //    nt.SetTop = dt;
                //}
                //else
                //{
                //    return "false";
                //}
                //int im;
                //if (Int32.TryParse(Importance, out im))
                //{
                //    nt.Importance = im;
                //}
                //else
                //{
                //    return "false";
                //}
                //bool getAdvices;

                //if (bool.TryParse(SendToPart, out getAdvices) == false)
                //{
                //    return "false";
                //}
                //nt.GetAdvices = getAdvices;
                //nt.AdviceDesc = AdviceDesc;
                //nt.AdviceItems = AdviceItems;
                //int noticeSta;
                //if (!Int32.TryParse(NoticeSta, out noticeSta))
                //{
                //    return "false";
                //}
                //else
                //{
                //    nt.NoticeSta = noticeSta;
                //}
                nt.Title = Title;
                nt.AttachFile = AttachFile;
                nt.FK_NoticeCategory = FK_NoticeCategory;
                nt.Doc = Doc;
                nt.RDT =RDT;
                nt.FK_Dept = FK_Dept;
                nt.FK_UserNo = FK_UserNo;
                nt.SendToPart = SendToPart;
                nt.SendToUsers = SendToUsers;
                nt.SendToDepts = SendToDepts;
                nt.SendToStations = SendToStations;
                nt.Importance = Importance;
                nt.GetAdvices = GetAdvices;
                nt.AdviceDesc = AdviceDesc;
                nt.AdviceItems = AdviceItems;
                nt.NoticeSta = NoticeSta;
                nt.StartTime = StartTime;
                nt.StopTime = StopTime;
                nt.SetMes = SetMes;
                return nt.Update() > 0 ? "true" : "false";
            }
            return "false";
        }
       /// <summary>
        /// 添加公告
       /// </summary>
       /// <param name="Title"></param>
       /// <param name="AttachFile"></param>
       /// <param name="FK_NoticeCategory"></param>
       /// <param name="FK_Dept"></param>
       /// <param name="FK_UserNo"></param>
       /// <param name="SendToPart"></param>
       /// <param name="SendToUsers"></param>
       /// <param name="SendToDepts"></param>
       /// <param name="SendToStations"></param>
       /// <param name="Importance"></param>
       /// <param name="GetAdvices"></param>
       /// <param name="AdviceDesc"></param>
       /// <param name="AdviceItems"></param>
       /// <param name="NoticeSta"></param>
       /// <param name="StartTime"></param>
       /// <param name="StopTime"></param>
       /// <param name="SetMes"></param>
       /// <returns></returns>
        [WebMethod]
        public string AddNotice( string Title, string AttachFile,string Doc,
        string FK_NoticeCategory, string FK_Dept, string FK_UserNo, bool SendToPart,
        string SendToUsers, string SendToDepts, string SendToStations,int Importance,
        bool GetAdvices, string AdviceDesc, string AdviceItems, int NoticeSta, DateTime StartTime, DateTime StopTime, int SetMes)
        {
            BP.OA.Notice nt = new BP.OA.Notice();
            nt.Title = Title;
            nt.AttachFile = AttachFile;
            nt.Doc = Doc;
            nt.FK_NoticeCategory = FK_NoticeCategory;
            nt.RDT = DateTime.Now;
            nt.FK_Dept = FK_Dept;
            nt.FK_UserNo = FK_UserNo;
            nt.SendToPart = SendToPart;
            nt.SendToUsers = SendToUsers;
            nt.SendToDepts = SendToDepts;
            nt.SendToStations = SendToStations;
            nt.Importance = Importance;
            nt.GetAdvices = GetAdvices;
            nt.AdviceDesc = AdviceDesc;
            nt.AdviceItems = AdviceItems;
            nt.NoticeSta = NoticeSta;
            nt.StartTime = StartTime;
            nt.StopTime = StopTime;
            nt.SetMes = SetMes;
            return nt.Insert() > 0 ? "true" : "false";
        }
        /// <summary>
        /// 置顶
        /// </summary>
        /// <param name="oid">oid编号</param>
        /// <returns></returns>
        [WebMethod]
        public string SetTop(string oid)
        {
            Notices art = new Notices();
            art.RetrieveAll();
            BP.OA.Notice nt = (BP.OA.Notice)art.Filter("OID", oid);

            if (nt != null)
            {
                nt.SetTop = DateTime.Now;
                return "true";
            }
            return "false";
        }
        /// <summary>
        /// 取消置顶
        /// </summary>
        /// <param name="oid">编号</param>
        /// <returns></returns>
        [WebMethod]
        public string CancelTop(string oid)
        {
            Notices art = new Notices();
            art.RetrieveAll();
            BP.OA.Notice nt = (BP.OA.Notice)art.Filter("OID", oid);
            if (nt != null)
            {
                nt.SetTop = DateTime.MinValue;
                return "true";
            }
            return "false";

        }
        #endregion
        #region 2.我的公告
        [WebMethod]
        public string GetMyNotices(string no)
        {
            Notices nts = new Notices();
            nts.RetrieveByAttr("FK_UserNo", no);
            return Entitis2Json.ConvertEntitis2GridJsonOnlyData(nts);
        }
        #endregion
    }
}
