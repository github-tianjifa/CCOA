﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BP.OA;
using BP.En;
using BP.Web;

namespace CCOA.App.OA
{
    public partial class RiCheng : System.Web.UI.Page
    {
        public string FK_Leader
        {
            get
            {
                return this.Request.QueryString["FK_Leader"];
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.FK_Leader == null)
            {
                this.BindAll();
                return;
            }
            BP.OA.Leader myen = new BP.OA.Leader(this.FK_Leader);
            BP.OA.LeaderDtls dtls = new BP.OA.LeaderDtls();
            string strs = "";
            BP.OA.Leaders ens = new Leaders();
            ens.RetrieveAll();
            foreach (BP.OA.Leader en in ens)
            {
                if (en.No == this.FK_Leader)
                    strs += "&nbsp;&nbsp;&nbsp;&nbsp;<b>" + en.Name + "</b>";
                else
                    strs += "&nbsp;&nbsp;&nbsp;&nbsp;<a href='RiCheng.aspx?FK_Leader=" + en.No + "'>" + en.Name + "</a>";
            }
            this.Title = myen.Name + " 的活动信息";
            this.Pub1.AddTable("width=100%");
            this.Pub1.AddCaption("<a href='LeaderList.aspx'><img src='all.png' class=Icon border=0/>领导风采</a> | <a href='RiCheng.aspx'><img src='rili.png' class=Icon border=0/>全部日程</a> - " + strs);
            //#region 输出内容.
            //this.Pub1.AddTR();
            //this.Pub1.AddTDBegin();
            //this.Pub1.Add("<td><img src='" + myen.WebPath + "' width='200px'>");
            //this.Pub1.Add("<h2>姓名:" + myen.Name + "</h2>");
            //this.Pub1.Add("<br>职务:" + myen.Duty);
            //this.Pub1.Add("<br>简历:" + myen.JianLi);
            //this.Pub1.Add("</td>");
            //this.Pub1.AddTREnd();
            //#endregion 输出内容.
            dtls.Retrieve("FK_Leader", this.FK_Leader);
            foreach (BP.OA.LeaderDtl dtl in dtls)
            {
                this.Pub1.AddTR();
                this.Pub1.AddTDBegin();
                this.Pub1.Add("<img src='rili.png' width='28px' border=0/><b>" + dtl.DTFrom + " - " + dtl.DTTo + "</b>");
                this.Pub1.AddBR();
                this.Pub1.Add("<div >");
                this.Pub1.Add(dtl.Title);
                this.Pub1.Add("</div>");
                this.Pub1.AddTDEnd();
                this.Pub1.AddTREnd();
            }
            this.Pub1.AddTableEnd();
        }

        public void BindAll()
        {
            BP.OA.LeaderDtls dtls = new BP.OA.LeaderDtls();
            QueryObject qo = new QueryObject(dtls);
            qo.addOrderBy(LeaderDtlAttr.OID);
            qo.DoQuery();

            string strs = "";
            BP.OA.Leaders ens = new Leaders();
            ens.RetrieveAll();
            foreach (BP.OA.Leader en in ens)
            {
                strs += "&nbsp;&nbsp;&nbsp;&nbsp;<a href='RiCheng.aspx?FK_Leader=" + en.No + "'>" + en.Name + "</a>";
            }
            this.Title = "全部信息";
            this.Pub1.AddTable("width=100%");
            this.Pub1.AddCaption("<a href='LeaderList.aspx'><img src='all.png' class=Icon border=0/>领导风采</a> | <b><img src='rili.png' class=Icon border=0/>全部日程</B> - " + strs);
            foreach (BP.OA.LeaderDtl dtl in dtls)
            {
                this.Pub1.AddTR();
                BP.OA.Leader leader = new Leader(dtl.FK_Leader);
                this.Pub1.AddTDBegin();
                this.Pub1.Add("<img src='" + leader.WebPath + "' width='100px'  style='line-height:28px;' /> <div  style='line-height:28px;'>姓名:" + leader.Name + " <br>职务：" + leader.Duty + "</div>");
                this.Pub1.Add("<img src='rili.png' width='28px' border=0 style='line-height:28px;'/><b>" + dtl.DTFrom + " - " + dtl.DTTo + "</b>");
                this.Pub1.Add("<div style='line-height:28px;'>");
                this.Pub1.Add(dtl.Title);
                this.Pub1.Add("</div>");
                this.Pub1.AddTDEnd();
                this.Pub1.AddTREnd();
            }
            this.Pub1.AddTableEnd();
        }
    }
}