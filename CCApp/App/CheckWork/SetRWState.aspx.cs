﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;

namespace CCOA.App.CheckWork
{
    public partial class SetRWState : System.Web.UI.Page
    {
        public string trCount = "0";

        protected void Page_Load(object sender, EventArgs e)
        {
            GetData();
        }


        /// <summary>
        /// 获取数据
        /// </summary>
        public void GetData()
        {
            DataTable dt = BP.DA.DBAccess.RunSQLReturnTable("select * from CW_RWState");
            if (dt != null && dt.Rows.Count > 0)
            {
                trCount = dt.Rows.Count.ToString();
                int rowcount = 0;
                StringBuilder sbContent = new StringBuilder("");
                sbContent.Append("<table id=\"TB_content\">");
                sbContent.Append("<tr id=\"tr_0\">");
                sbContent.Append("<td>编号</td><td>状态</td><td>操作 </td>");
                sbContent.Append("</tr>");

                foreach (DataRow dr in dt.Rows)
                {
                    rowcount++;
                    sbContent.Append("<tr id='tr_'" + rowcount + ">");
                    sbContent.AppendFormat("<td>{0}</td><td>{1}</td>", dr["No"].ToString(), dr["Name"].ToString());
                    sbContent.Append("<td><a href=\"#\" onclick=\"Update('" + dr["No"].ToString() + "','" + dr["Name"].ToString() + "');\">修改</a></td>");
                    sbContent.Append("</tr>");
                }

                sbContent.Append("</table>");
                this.div_content.InnerHtml = sbContent.ToString();
            }
        }



        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Save_Click(object sender, EventArgs e)
        {
            BP.OA.CheckWork.RWState rwstate = new BP.OA.CheckWork.RWState();
            rwstate.No = this.txtNo.Value;
            rwstate.Name = this.txtName.Value;
            if (rwstate.IsExits)
            {
                rwstate.Update();
            }
            else
            {
                rwstate.Insert();
            }
        }
    }
}