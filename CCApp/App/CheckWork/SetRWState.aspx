﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SetRWState.aspx.cs" Inherits="CCOA.App.CheckWork.SetRWState" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>签到状态</title>
    <script src="../../Js/jquery-1.7.2.min.js" type="text/javascript"></script>
    <link href="jquery/lib/ligerUI/skins/Aqua/css/ligerui-all.css" rel="stylesheet" type="text/css" />
    <link href="jquery/tablestyle.css" rel="stylesheet" type="text/css" />
    <link href="jquery/lib/ligerUI/skins/ligerui-icons.css" rel="stylesheet" type="text/css" />
    <script src="jquery/lib/jquery/jquery-1.5.2.min.js" type="text/javascript"></script>
    <script src="jquery/lib/ligerUI/js/core/base.js" type="text/javascript"></script>
    <script src="jquery/lib/ligerUI/js/ligerui.all.js" type="text/javascript"></script>
    <script src="jquery/lib/ligerUI/js/plugins/ligerGrid.js" type="text/javascript"></script>
    <script src="jquery/lib/ligerUI/js/plugins/ligerDialog.js" type="text/javascript"></script>
    <script src="js/CKAppData.js" type="text/javascript"></script>
    <script src="js/SetRWState.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        var trcount = $("#txttrcount").val();
        var btnvalue = "保存";

        //新增一行
        function AddTr() {
            trcount++;

            var table = document.getElementById("TB_content");

            var tr = document.createElement("tr");
            tr.id = "tr_" + trcount;

            var td = document.createElement("td");
            td.id = "td_No" + trcount;
            td.innerHTML = "<input  type=\"text\" id='" + SetControlID('txtNo_') + "'/>";
            tr.appendChild(td);

            var td2 = document.createElement("td");
            td2.id = "td_Name" + trcount;
            td2.innerHTML = "<input  type=\"text\"  id='" + SetControlID('txtName_') + "' />";
            tr.appendChild(td2);

            var td3 = document.createElement("td");
            td3.id = "td_OP" + trcount;
            td3.innerHTML = "<input type=\"button\" value=\"保存\" id='" + SetControlID('btnOP_') + "'  onclick=\"Save();\"/>";
            tr.appendChild(td3);

            table.appendChild(tr);
        }

        function SetControlID(obj) {
            return obj + trcount;
        }
        //保存
        function Save() {
            $("#txttrcount").val(trcount);
            var no = $("#txtNo_" + trcount).val();
            var name = $("#txtName_" + trcount).val();

            $("#txtNo").val($("#txtNo_" + trcount).val());
            $("#txtName").val($("#txtName_" + trcount).val());
            var btn = document.getElementById("<%=Btn_Save.ClientID %>");
            if (btn) {
                btn.click();
                alert('保存成功');
                window.location.reload();
            }
        }

        //修改
        function Update(obj, name) {
            var div = document.getElementById("divUpdate");
            div.style.display = "block";
            $("#lbNo").html(obj);
            $("#txtNo").val(obj);
            $("#txtUpName").val(name);

        }
        // 保存修改
        function UpOK() {
            $("#txtName").val($("#txtUpName").val());
            var btn = document.getElementById("<%=Btn_Save.ClientID %>");
            if (btn) {
                btn.click();
                alert('保存成功');
                window.location.reload();
            }
        }
        //取消
        function UpCancel() {
            var div = document.getElementById("divUpdate");
            div.style.display = "none";
            $("#lbNo").html("");
            $("#txtNo").val("");
            $("#txtUpName").val("");
            $("#txtName").val("");
        }
        //================一下是ligerui事件===========
     

    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div id="divHidden" style="display: none">
        <input type="hidden" id="txttrcount" runat="server" value="<%=trCount %>" />
        <input type="hidden" id="txtNo" runat="server" />
        <input type="hidden" id="txtName" runat="server" />
        <asp:Button ID="Btn_Save" CssClass="HBtn" runat="server" Text="保存" OnClick="Save_Click" />
        <input type="button" onclick="AddTr()" value="新增" />
        <div id="div_content" runat="server">
            <table id="TB_content">
                <tr id="tr_0">
                    <td>
                        编号
                    </td>
                    <td>
                        状态
                    </td>
                    <td>
                        操作
                    </td>
                </tr>
            </table>
        </div>
        <div id="divUpdate" style="display: none">
            编号:<label id="lbNo" runat="server"></label>
            状态:<input type="text" id="txtUpName" runat="server" />
            <input type="button" id="btnUpOK" value="保存" onclick="UpOK()" />
            <input type="button" id="btnUpCancel" value="取消" onclick="UpCancel()" />
        </div>
    </div>
    <div id="pageloading">
    </div>
    <div id="toptoolbar">
    </div>
    <div id="maingrid" style="margin: 0; padding: 0;">
    </div>
    <div id="stateInfo" style="display: none;">
    <input type="hidden" id="txtoptype" />
        <table style="margin: 10px;">
            <tr style="margin: 10px; height: 30px;">
                <td>
                    编号：
                </td>
                <td>
                    <input id="TB_No" type="text" />
                </td>
            </tr>
            <tr style="margin: 10px; height: 30px;">
                <td>
                    名称：
                </td>
                <td>
                    <input id="TB_Name" type="text" />
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
