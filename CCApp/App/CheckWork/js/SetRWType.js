﻿
//初始化
$(function () {
    var toolBarManager = $("#toptoolbar").ligerToolBar({ items: [
                { text: '新增', click: itemclick, icon: 'add' },
                { line: true },
                 { text: '修改', click: itemclick, icon: 'modify' },
                { line: true },
                { text: '删除', click: itemclick, icon: 'delete' },
                { line: true }
            ]
    });
    LoadGrid();
});
var title = "";
//工具栏事件
function itemclick(item) {
    title = item.text + "类型";
    if (item.text == "新增") {
        $("#txtoptype").val('add');
        OpType("add");
    }
    else if (item.text == "修改") {
        $("#txtoptype").val('modify');
        OpType("modify");
    }
    else if (item.text == "删除") {
        $("#txtoptype").val('delete');
        OpType("delete");
    }
}
//类型操作
function OpType(obj) {
    var op = $("#txtoptype").val();
    $("#TB_No").attr("disabled", "");
    SetEmpty();
    if (obj == "delete") {
        var grid = $("#maingrid").ligerGetGridManager();
        var row = grid.getSelectedRow();
        if (row) {
            $.ligerDialog.confirm('您确定要删除所选项吗？', function (yes) {
                if (yes) {
                    Application.data.opRWType(row.No, encodeURI(row.Name),row.RegularTime, op, function (js) {
                        if (js == "1") {
                            SetEmpty();
                            LoadGrid();
                            return;
                        }
                        else {
                            $.ligerDialog.warn('请重新操作！');
                        }
                    }, this);
                }
            })
        } else {
            $.ligerDialog.warn('请选择删除数据！');
            return;
        }
    }
    else {
        if (obj == "modify") {
            var grid = $("#maingrid").ligerGetGridManager();
            var row = grid.getSelectedRow();
            if (row) {
                $("#TB_No").val(row.No);
                $("#TB_No").attr("disabled", "disabled");
                $("#TB_Name").val(row.Name);
                $("#TB_RegularTime").val(row.RegularTime);
            } else {
                $.ligerDialog.warn('请选择修改数据！');
                return;
            }
        }
        $.ligerDialog.open({
            target: $("#divInfo"),
            title: title,
            width: 380,
            height: 220,
            isResize: true,
            modal: true,
            buttons: [
        { text: '保存', onclick: function (i, d) {

            var no = $("#TB_No").val();
            var name = $("#TB_Name").val();
            if (name == "") {
                $.ligerDialog.warn('类型名称不能为空！');
                $("#TB_Name").focus();
                return;
            }
            name = encodeURI(name);
            var regularTime = $("#TB_RegularTime").val();
            Application.data.opRWType(no, name,regularTime, op, function (js) {
                if (js == "1") {
                    SetEmpty();
                    d.hide();
                    LoadGrid();
                }
                else {
                    $.ligerDialog.warn('请重新操作！');
                }
            }, this);
        }
        }, { text: '关闭', onclick: function (i, d) {
            SetEmpty();
            d.hide();
        }
        }]
        });

    }

}
//加载列表
function LoadGrid() {
    $("#pageloading").show();
    Application.data.getRWType(callBack, this);
}
//回调函数
function callBack(jsonData, scope) {
    if (jsonData) {
        var pushData = eval('(' + jsonData + ')');
        var grid = $("#maingrid").ligerGrid({
            columns: [
                   { display: '类型', name: 'Name', width: 380, align: 'left' },
                   { display: '规定时间', name: 'RegularTime' }
                   ],
            pageSize: 20,
            data: pushData,
            rownumbers: true,
            height: "99%",
            width: "99%",
            columnWidth: 100,
            onReload: LoadGrid
        });
    }
    else {
        $.ligerDialog.warn('加载数据出错，请关闭后重试！');
    }
    $("#pageloading").hide();
}

//清空 控件 
function SetEmpty() {
    $("#TB_No").val('');
    $("#TB_Name").val('');
    $("#TB_RegularTime").val('');
    $("#txtoptype").val('');
}
