﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SetRWType.aspx.cs" Inherits="CCOA.App.CheckWork.SetRWType" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>设置考勤时间</title>
    <style type="text/css">
        .HBtn
        {
            /* display:none; */
            visibility: visible;
            background-color: White;
            border: 0;
        }
    </style>
    <script src="../../Js/jquery-1.7.2.min.js" type="text/javascript"></script>
    <link href="jquery/lib/ligerUI/skins/Aqua/css/ligerui-all.css" rel="stylesheet" type="text/css" />
    <link href="jquery/tablestyle.css" rel="stylesheet" type="text/css" />
    <link href="jquery/lib/ligerUI/skins/ligerui-icons.css" rel="stylesheet" type="text/css" />
    <script src="jquery/lib/jquery/jquery-1.5.2.min.js" type="text/javascript"></script>
    <script src="jquery/lib/ligerUI/js/core/base.js" type="text/javascript"></script>
    <script src="jquery/lib/ligerUI/js/ligerui.all.js" type="text/javascript"></script>
    <script src="jquery/lib/ligerUI/js/plugins/ligerGrid.js" type="text/javascript"></script>
    <script src="jquery/lib/ligerUI/js/plugins/ligerDialog.js" type="text/javascript"></script>
    <script src="js/CKAppData.js" type="text/javascript"></script>
    <script src="js/SetRWType.js" type="text/javascript"></script>
    <script type="text/javascript" language="javascript">
        //页面加载
        $(function () {
            //  DDLData();
        })
        //填充时间下拉框
        function DDLData() {
            $("#ddl_Time").html("");
            for (var i = 0; i < 24; i++) {
                $("#ddl_Time").append("<option value='" + i + "'>" + i + ":00</option>");
                $("#ddl_Time").append("<option value='" + i + "2'>" + i + ":30</option>");
            }
        }
        //下拉框内容修改
        function DDL_SelectVal() {
            var time = "";
            var ddltime = document.getElementById('ddl_Time');
            for (var i = 0; i < ddltime.length; i++) {//下拉框的长度就是他的选项数 
                if (ddltime[i].selected == true) {
                    time = ddltime[i].text; //获取文本
                }
            }
            $("#txttime").val(time);
        }
        //修改
        function Update(obj) {
            $("#txttype").val(obj);
            var ui = document.getElementById("divSetTime");
            ui.style.display = "block";
            var lbtext = "";
            switch (obj) {
                case "S1":
                    lbtext = "上班(上午)";
                    break;
                case "S2":
                    lbtext = "上班(下午)";
                    break;
                case "X1":
                    lbtext = "下班(上午)";
                    break;
                case "X2":
                    lbtext = "下班(下午)";
                    break;
            }
            $("#lbType").html(lbtext);
        }
        //保存
        function Save() {
            var btn = document.getElementById("<%=Btn_Save.ClientID %>");
            if (btn) {
                btn.click();
                alert('修改成功');
                window.location.reload();
            }
        }
        //取消
        function Cancel() {
            $("#txttype").val("");
            $("#lbType").html("");
            $("#divSetTime").style.display = "none";
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div id="divHidden" style="display: none">
        <input type="hidden" id="txttype" runat="server" />
        <input type="hidden" id="txttime" runat="server" />
        <asp:Button ID="Btn_Save" CssClass="HBtn" runat="server" Text="保存" OnClick="Save_Click" />
        <asp:Button ID="Btn_Add" runat="server" Text="添加" OnClick="Btn_Add_Click" />
        <div>
            <table>
                <tr>
                    <td>
                        登记类型
                    </td>
                    <td>
                        规定时间
                    </td>
                    <td>
                        操作
                    </td>
                </tr>
                <tr>
                    <td>
                        上班
                    </td>
                    <td>
                        <asp:TextBox ID="txtS1" runat="server"></asp:TextBox>
                    </td>
                    <td>
                        <a href="#" onclick="Update('S1')">修改</a>
                    </td>
                </tr>
                <tr>
                    <td>
                        下班
                    </td>
                    <td>
                        <asp:TextBox ID="txtX1" runat="server"></asp:TextBox>
                    </td>
                    <td>
                        <a href="#" onclick="Update('X1')">修改</a>
                    </td>
                </tr>
                <tr>
                    <td>
                        上班
                    </td>
                    <td>
                        <asp:TextBox ID="txtS2" runat="server"></asp:TextBox>
                    </td>
                    <td>
                        <a href="#" onclick="Update('S2')">修改</a>
                    </td>
                </tr>
                <tr>
                    <td>
                        下班
                    </td>
                    <td>
                        <asp:TextBox ID="txtX2" runat="server"></asp:TextBox>
                    </td>
                    <td>
                        <a href="#" onclick="Update('X2')">修改</a>
                    </td>
                </tr>
            </table>
        </div>
        <%--时间设置弹出框--%>
        <div id="divSetTime" style="display: none">
            类型：
            <label id="lbType">
            </label>
            <br />
            时间：<select id="ddl_Time" onchange="DDL_SelectVal();" runat="server">
            </select><br />
            <input type="button" value="确定" id="btnOK" onclick="Save();" />
            <input type="button" value="取消" id="btnCancel" onclick="Cancel();" />
        </div>
    </div>
    <%-- 用列表形式--%>
    <div id="pageloading">
    </div>
    <div id="toptoolbar">
    </div>
    <div id="maingrid" style="margin: 0; padding: 0;">
    </div>
    <input type="hidden"  id="txtoptype"/>
    <div id="divInfo">
    
        <table>
            <tr>
                <td>
                    编号:<input type="text" id="TB_No" />
                </td>
            </tr>
            <tr>
                <td>
                    类型:<input type="text" id="TB_Name" />
                </td>
            </tr>
            <tr>
                <td>
                    规定时间:<input type="text" id="TB_RegularTime" />(*可以为空)
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
