﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main/master/Site1.Master" AutoEventWireup="true"
    CodeBehind="GanttPlans.aspx.cs" Inherits="CCOA.App.PublicPlan.GanttPlans" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cph_head" runat="server">
    <script src="../../js/js_EasyUI/jquery.easyui.min.js" type="text/javascript"></script>
    <link href="../../js/js_EasyUI/themes/default/easyui.css" rel="stylesheet" type="text/css" />
    <script src="../../js/js_EasyUI/locale/easyui-lang-zh_CN.js" type="text/javascript"></script>
    <link href="../../js/js_EasyUI/themes/icon.css" rel="stylesheet" type="text/css" />
    <link href="../../Js/FusionCharts/Style.css" rel="stylesheet" type="text/css" />
    <script src="../../Js/FusionCharts/FusionCharts.js" type="text/javascript"></script>
    <script src="js/AppData.js" type="text/javascript"></script>
    <script src="js/GanttPlans.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_title" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_body" runat="server">
<div id="tb" class="datagrid-toolbar" style="height: 30px;">
    <div style="margin:5px;">
        <div style="float: left; margin-left: 5px;">
            筛选：<input type="text" id="TB_KeyWord" />
        </div>
        <div style="float:left;">
            <input type="checkbox" id="CB_Catalog" onclick="CheckBoxChange()" style="vertical-align:middle;" />
            <label for="CB_Catalog" style="vertical-align:middle;">显示子级</label>
        </div>
        <div class="datagrid-btn-separator"></div>
        <a id="btnBackPrePlan" style="float: left;" href="#" onclick="BackPrePlan()" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-reset'">返回上一层</a> 
        <a id="btnSetTop" style="float: left;" href="#" onclick="SetToTopView()" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-new'">重置</a> 
        <div class="datagrid-btn-separator"></div>
        <a id="btnTranUp" style="float: left;" href="#" onclick="PlanDetails()" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-save-close'">查看详细</a> 
        <a id="btnTranDown" style="float: left;" href="#" onclick="ShowPlanTick()" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-mulu'">反馈情况</a> 
    </div>
</div>
<div id="chartdiv" align="center">
</div>
<div id="infoMesage">
</div>
<input type="hidden" id="HD_CurPlanNo" value="99" />
</asp:Content>
