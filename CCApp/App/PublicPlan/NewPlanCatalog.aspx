﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main/master/Site1.Master" AutoEventWireup="true"
    CodeBehind="NewPlanCatalog.aspx.cs" ValidateRequest="false" ClientIDMode="Static"
    Inherits="CCOA.App.PublicPlan.NewPlanCatalog" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cph_head" runat="server">
    <style type="text/css">
        body
        {
            font-size: 13px;
        }
        .doc-table
        {
            border-collapse: collapse;
            border-spacing: 0;
            margin-left: 9px;
            display: table;
            border-color: gray;
            width: 99%;
        }
        .doc-table th
        {
            background: #EEE;
        }
        .doc-table th, .doc-table td
        {
            border: 1px solid #BDDBEF;
            padding: 3px 3px;
        }
        td.title
        {
            width: 100px;
            text-align: right;
        }
        td.title3
        {
            width: 100px;
            text-align: right;
        }
        td.text
        {
            text-align: left;
            width: 200px;
            padding-left: 10px;
        }
    </style>
    <script src="../../js/js_EasyUI/jquery.easyui.min.js" type="text/javascript"></script>
    <link href="../../js/js_EasyUI/themes/default/easyui.css" rel="stylesheet" type="text/css" />
    <script src="../../js/js_EasyUI/locale/easyui-lang-zh_CN.js" type="text/javascript"></script>
    <link href="../../js/js_EasyUI/themes/icon.css" rel="stylesheet" type="text/css" />
    <script src="../../js/js_EasyUI/plugins/jquery.datebox.js" type="text/javascript"></script>
    <script src="../../js/zDialog/zDialog.js" type="text/javascript"></script>
    <link rel="stylesheet" href="../../ctrl/kindeditor/themes/default/default.css" />
    <link rel="stylesheet" href="../../ctrl/kindeditor/plugins/code/prettify.css" />
    <script type="text/javascript" charset="utf-8" src="../../ctrl/kindeditor/kindeditor.js"></script>
    <script type="text/javascript" charset="utf-8" src="../../ctrl/kindeditor/lang/zh_CN.js"></script>
    <script type="text/javascript" charset="utf-8" src="../../ctrl/kindeditor/plugins/code/prettify.js"></script>
    <script type="text/javascript">
        var kindEditToolBar = ["source", "|", "undo", "redo", "|", "preview", "print", "cut", "copy", "paste", "plainpaste",
        "wordpaste", "|", "justifyleft", "justifycenter", "justifyright", "justifyfull", "indent", "outdent", "clearhtml", "quickformat",
        "selectall", "|", "fullscreen", "/", "formatblock", "fontname", "fontsize", "|", "forecolor", "hilitecolor",
        "bold", "italic", "underline", "strikethrough", "lineheight", "removeformat", "|", "image", "multiimage",
        "insertfile", "table", "hr", "emoticons"];

        KindEditor.ready(function (K) {
            var editor1 = K.create('#ui_SimpleContents', {
                cssPath: '../../ctrl/kindeditor/plugins/code/prettify.css',
                uploadJson: '../../ctrl/kindeditor/asp.net/upload_json.ashx',
                fileManagerJson: '../../ctrl/kindeditor/asp.net/file_manager_json.ashx',
                allowFileManager: true,
                items: kindEditToolBar,
                minWidth: 550
            });
            editor1.sync();
            prettyPrint();
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_title" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_body" runat="server">
    <div style="margin: 5px 5px 5px 5px; width: 700px;">
        <table class="doc-table">
            <caption style="text-align: left; font-size: 14px; font-weight: bold;">
                <img src="../../Images/Btn/Insert.gif" style="width: 18px; height: 18px;" />计划基本信息</caption>
            <tr>
                <td class="title">
                    <span style="color: Red;">*</span>&nbsp;计划类型:
                </td>
                <td class="text">
                    &nbsp;<asp:RadioButtonList ID="ui_PCModel" runat="server" RepeatLayout="Flow" RepeatDirection="Horizontal">
                    </asp:RadioButtonList>
                </td>
                <td class="title3">
                    <span style="color: Red;">*</span>&nbsp;完成进度:
                </td>
                <td class="text">
                    <asp:Label ID="ui_FinishPercent" runat="server" Width="30px"></asp:Label>%
                </td>
            </tr>
            <tr>
                <td class="title">
                    <span style="color: Red;">*</span>&nbsp;计划名称:
                </td>
                <td class="text" colspan="3">
                    &nbsp;<asp:TextBox ID="ui_Name" runat="server" Width="90%"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="title">
                    <span style="color: Red;">*</span>&nbsp;负责部门:
                </td>
                <td class="text">
                    <div id="div2" style="display: block; margin-top: 1px; padding-top: 1px;">
                        &nbsp;<input type="text" id="ui_FK_DeptText" style="width: 70%;" runat="server" readonly="readonly" />
                        <asp:HiddenField ID="ui_FK_Dept_hd" runat="server"></asp:HiddenField>
                        <input type="button" value="选择" onclick="zDialog_open2('../../ctrl/SelectDepts/SelectDept_zTree.aspx', '选择部门', 650, 430,'#ui_FK_Dept_hd','ui_FK_Dept_hd','ui_FK_DeptText');" />
                    </div>
                </td>
                <td class="title3">
                    负责人:
                </td>
                <td class="text">
                    <div id="div_CopyUsers" style="display: block; margin-top: 1px; padding-top: 1px;">
                        &nbsp;<input type="text" id="ui_PersonCharge" style="width: 70%;" runat="server"
                            readonly="readonly" />
                        <asp:HiddenField ID="ui_PersonCharge_hd" runat="server"></asp:HiddenField>
                        <input type="button" value="选择" onclick="zDialog_open2('../../ctrl/SelectUsers/SelectUser_Jq.aspx?SelectModel=Single', '选择人员', 650, 430,'#ui_PersonCharge_hd','ui_PersonCharge_hd','ui_PersonCharge');" />
                    </div>
                </td>
            </tr>
            <tr>
                <td class="title">
                    参与人员:
                </td>
                <td class="text" colspan="3">
                    <div id="div1" style="display: block; margin-top: 1px; padding-top: 1px; width:550px;">
                        &nbsp;<input type="text" id="ui_PlanPartake" style="width: 89%;" runat="server" readonly="readonly" />
                        <asp:HiddenField ID="ui_PlanPartake_hd" runat="server"></asp:HiddenField>
                        <input type="button" value="选择" onclick="zDialog_open2('../../ctrl/SelectUsers/SelectUser_Jq.aspx', '选择人员', 650, 430,'#ui_PlanPartake_hd','ui_PlanPartake_hd','ui_PlanPartake');" />
                    </div>
                </td>
            </tr>
            <tr>
                <td class="title">
                    <span style="color: Red;">*</span>&nbsp;开始时间:
                </td>
                <td class="text">
                    &nbsp;<asp:TextBox ID="ui_StartDate" runat="server" CssClass="easyui-datebox"></asp:TextBox>
                </td>
                <td class="title3">
                    <span style="color: Red;">*</span>&nbsp;结束时间:
                </td>
                <td class="text">
                    &nbsp;<asp:TextBox ID="ui_EndDate" runat="server" CssClass="easyui-datebox"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="title">
                    内容:
                </td>
                <td class="text" colspan="3" style="width: 400px;">
                    &nbsp;<asp:TextBox ID="ui_SimpleContents" runat="server" TextMode="MultiLine" Style="height: 300px;"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td colspan="4" style="text-align: center;">
                    <asp:Button runat="server" Text="保存" ID="btnSubmit" OnClick="btnSubmit_Click" />
                </td>
            </tr>
        </table>
        <asp:HiddenField ID="ui_PK_NO" runat="server"></asp:HiddenField>
    </div>
</asp:Content>
