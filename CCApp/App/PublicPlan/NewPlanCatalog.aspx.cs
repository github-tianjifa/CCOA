﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using BP.DA;
using BP.En;
using BP.Web;
using BP.Sys;
using BP.OA.PublicPlan;

namespace CCOA.App.PublicPlan
{
    public partial class NewPlanCatalog : WebPage
    {
        /// <summary>
        /// 计划编号
        /// </summary>
        private string PlanNo
        {
            get
            {
                return this.Request.QueryString["PK"];
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadNoticeCategorys();
                this.ui_PK_NO.Value = this.PlanNo;
                if (!string.IsNullOrEmpty(this.PlanNo))
                {
                    BindPageControl();
                }
            }
        }

        /// <summary>
        /// 计划类型
        /// </summary>
        private void LoadNoticeCategorys()
        {
            SysEnums sysenumus = new SysEnums();
            sysenumus.RetrieveByAttr(SysEnumAttr.EnumKey, PlanCatalogAttr.PCModel);
            BP.OA.UI.Dict.BindListCtrl(sysenumus, this.ui_PCModel, "Lab", "IntKey", null, null, "0");
        }

        /// <summary>
        /// 绑定页面值
        /// </summary>
        private void BindPageControl()
        {
            btnSubmit.Visible = false;
            PlanCatalog catalog = new PlanCatalog();
            catalog.RetrieveByAttr(PlanCatalogAttr.No, this.PlanNo);
            if (catalog == null || catalog.Name == "")
            {
                Alert("访问的记录不存在。");
                return;
            }
            ui_PCModel.SelectedValue = catalog.PCModel == PCModel.PlanModel ? "0" : "1";
            ui_Name.Text = catalog.Name;
            ui_FinishPercent.Text = catalog.FinishPercent.ToString();
            ui_FK_DeptText.Value = catalog.FK_DeptText;
            ui_FK_Dept_hd.Value = catalog.FK_Dept;
            ui_PersonCharge.Value = catalog.PersonChargeText;
            ui_PersonCharge_hd.Value = catalog.PersonCharge;
            ui_PlanPartake.Value = catalog.PlanPartakeText;
            ui_PlanPartake_hd.Value = catalog.PlanPartake;
            ui_StartDate.Text = catalog.StartDate;
            ui_EndDate.Text = catalog.EndDate;
            ui_SimpleContents.Text = catalog.SimpleContents;
            //创建人和负责人可以修改
            if (catalog.FK_Emp == "" || catalog.FK_Emp == WebUser.No || catalog.PersonCharge == WebUser.No)
            {
                btnSubmit.Visible = true;
            }
        }
        /// <summary>
        /// 执行保存
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            //保存前检查
            if (String.IsNullOrEmpty(ui_Name.Text))
            {
                this.Alert("计划名称不能为空!");
                return;
            }
            if (String.IsNullOrEmpty(ui_FinishPercent.Text))
            {
                this.Alert("完成百分比不能为空!");
                return;
            }
            if (String.IsNullOrEmpty(ui_StartDate.Text))
            {
                this.Alert("开始时间不能为空!");
                return;
            }
            if (String.IsNullOrEmpty(ui_EndDate.Text))
            {
                this.Alert("结束时间不能为空!");
                return;
            }
            DateTime StartTime = Convert.ToDateTime(String.Format("{0:yyyy-MM-dd HH:mm:ss}", ui_StartDate.Text));
            DateTime EndTime = Convert.ToDateTime(String.Format("{0:yyyy-MM-dd HH:mm:ss}", ui_EndDate.Text));
            if (StartTime > EndTime)
            {
                this.Alert("开始时间不能晚于结束时间!");
                return;
            }
            //执行查询
            PlanCatalog catalog = new PlanCatalog();
            catalog.RetrieveByAttr(PlanCatalogAttr.No, this.PlanNo);

            catalog.FK_Emp = string.IsNullOrEmpty(catalog.FK_Emp) ? WebUser.No : catalog.FK_Emp;
            catalog.PCModel = ui_PCModel.SelectedValue == "0" ? PCModel.PlanModel : PCModel.StageModel;
            catalog.Name = ui_Name.Text;
            catalog.FinishPercent = decimal.Parse(ui_FinishPercent.Text);
            catalog.FK_Dept = ui_FK_Dept_hd.Value;
            catalog.PersonCharge = ui_PersonCharge_hd.Value;
            catalog.PlanPartakeText = ui_PlanPartake.Value;
            catalog.PlanPartake = ui_PlanPartake_hd.Value;
            catalog.StartDate = ui_StartDate.Text;
            catalog.EndDate = ui_EndDate.Text;
            catalog.SimpleContents = ui_SimpleContents.Text;
            catalog.ICON = ui_PCModel.SelectedValue == "0" ? "icon-plan-0" : "icon-plan-1";
            if (catalog.Update() > 0)
            {
                this.RegisterStartupScript("close", "<script language='JavaScript'>window.close();window.parent.Win_Close_Dialog();</script>");
            }
            else
                this.Alert("保存失败！");
        }
    }
}