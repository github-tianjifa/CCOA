﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ErrorMsg.aspx.cs" Inherits="CCOA.App.PublicPlan.ErrorMsg" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../../js/js_EasyUI/themes/default/easyui.css" rel="stylesheet" type="text/css" />
    <script src="../../js/jquery-1.7.2.min.js" type="text/javascript"></script>
    <script src="../../js/js_EasyUI/jquery.easyui.min.js" type="text/javascript"></script>
    <script src="../../js/js_EasyUI/locale/easyui-lang-zh_CN.js" type="text/javascript"></script>
    <link href="../../js/js_EasyUI/themes/icon.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript">
        $(function () {
            $('#dialogWindow').dialog({
                title: '系统提示',
                width: 360,
                height: 200,
                closed: false,
                closable: false,
                cache: false,
                resizable: false,
                modal: true
            });
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div id="dialogWindow">
        <div style="margin-top:15px; margin-left:10px;">
            <img alt="" src="js/workflow.png" align="middle" /> 没有权限查看
        </div>
    </div>
    </form>
</body>
</html>
