﻿var childPlanCount = 0;
$(function () {
    var parentNo = getArgsFromHref("ParentNo");
    if (parentNo) {
        $("#HD_CurPlanNo").val(parentNo);
        GetChildPlanCount(parentNo);
        return;
    }
    GetChildPlanCount("99");
});

//获取图表数据
function GetChartData(parentNo) {
    $("#HD_CurPlanNo").val(parentNo);
    var expendChild = document.getElementById("CB_Catalog").checked;
    var param = {
        method: "getchartdata",
        ParentNo: parentNo,
        expendChild: expendChild
    };

    queryData(param, function (js, scorp) {
        if (js.status == "500" || js == "") {
            $("#infoMesage").html("<b style='color:red;'><br/>获取图表数据出错，请检查计划安排是否合理。<b>");
            $('#infoMesage').dialog({
                title: '系统消息',
                width: 400,
                height: 200,
                closed: false,
                cache: false,
                modal: true
            });
            return;
        }

        if (js) {
            LoadCharts(js);
        }
    }, this);
}
//加载图表
function LoadCharts(db) {
    var chartHeight = childPlanCount * 50 + 60;
    if (chartHeight < 200) chartHeight = 150;
    var chart = new FusionCharts("../../Js/FusionCharts/Flash/FCF_Gantt.swf", "ChartId", "1100", chartHeight);
    chart.setDataXML(db);
    chart.render("chartdiv");
}
//获取子计划个数
function GetChildPlanCount(parentNo) {
    var expendChild = document.getElementById("CB_Catalog").checked;
    var param = {
        method: "getchildplancount",
        ParentNo: parentNo,
        expendChild: expendChild
    };

    queryData(param, function (js, scorp) {
        if (js.status == "500" || js == "") {
            $("#infoMesage").html("<b style='color:red;'><br/>获取图表数据出错，请检查计划安排是否合理。<b>");
            $('#infoMesage').dialog({
                title: '系统消息',
                width: 400,
                height: 200,
                closed: false,
                cache: false,
                modal: true
            });
            return;
        }
        childPlanCount = parseInt(js);
        if (childPlanCount == 0) {
            alert("所选计划下不存在子计划.");
            return;
        }
        //然后加载图表
        GetChartData(parentNo);
    }, this);
}

//显示详细
function PlanDetails() {
    var planNo = $("#HD_CurPlanNo").val();
    WindowOpen("显示详细", "/App/PublicPlan/PlanMain.aspx?ParentNo=" + planNo);
}

//显示反馈信息
function ShowPlanTick() {
    var planNo = $("#HD_CurPlanNo").val();
    if (planNo == "99") {
        alert("反馈信息不存在");
        return;
    }
    var url = "/App/PublicPlan/ViewPlanCatalog.aspx?PK=" + planNo;
    WindowOpen("反馈情况", url);
}
//重置
function SetToTopView() {
    GetChildPlanCount("99");
}
//显示全部切换
function CheckBoxChange() {
    var parentNo = $("#HD_CurPlanNo").val();
    GetChildPlanCount(parentNo);
}
//返回上一层
function BackPrePlan() {
    var curPlanNo = $("#HD_CurPlanNo").val();
    if (curPlanNo) {
        //获取上一层数据
        var params = {
            method: "backpreplan",
            nodeNo: curPlanNo
        };
        queryData(params, function (js, scope) {
            if (js) {
                var pushData = eval('(' + js + ')');
                $("#HD_CurPlanNo").val(pushData.ParentNo);
                if (pushData.ParentNo == "0") {
                    alert('已经返回到最顶层！');
                    return;
                }
                GetChildPlanCount(pushData.ParentNo);
            }
        }, this);
    }
    else {
        $.messager.alert('提示:', '不存在上一层！', 'info');
    }
}

function WindowOpen(title, url) {
    AddTab(title, url);
}
//公共方法
function queryData(param, callback, scope, method, showErrMsg) {
    if (!method) method = 'GET';
    $.ajax({
        type: method, //使用GET或POST方法访问后台
        dataType: "text", //返回json格式的数据
        contentType: "application/json; charset=utf-8",
        url: "GanttPlans.aspx", //要访问的后台地址
        data: param, //要发送的数据
        async: false,
        cache: false,
        complete: function () { }, //AJAX请求完成时隐藏loading提示
        error: function (XMLHttpRequest, errorThrown) {
            callback(XMLHttpRequest);
        },
        success: function (msg) {//msg为返回的数据，在这里做数据绑定
            callback(msg, scope);
        }
    });
}