﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App/Mobile.Master" AutoEventWireup="true"
    CodeBehind="ThreadMobile.aspx.cs" Inherits="CCOA.App.BBS.ThreadMobile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../../CCMobile/css/themes/default/jquery.mobile-1.4.5.min.css" rel="stylesheet"
        type="text/css" />
    <link href="../../CCMobile/css/themes/classic/theme-classic.css" rel="stylesheet"
        type="text/css" />
    <script src="../../CCMobile/js/jquery.js" type="text/javascript"></script>
    <script src="../../CCMobile/js/jquery.mobile-1.4.5.min.js" type="text/javascript"></script>
    <script src="../../CCMobile/js/comment/action.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <% 
        string FK_Motif = this.Request.QueryString["FK_Motif"];
        string FK_Class = this.Request.QueryString["FK_Class"];
        BP.OA.BBS.BBSMotif motif = new BP.OA.BBS.BBSMotif();
        motif.RetrieveByAttr(BP.OA.BBS.BBSMotifAttr.No, FK_Motif);
        //修改浏览次数
        motif.Hits = motif.Hits + 1;
        motif.Update();
        
        BP.OA.BBS.BBSReplays BBSR = new BP.OA.BBS.BBSReplays();
        BP.En.QueryObject qo = new BP.En.QueryObject(BBSR);
        qo.AddWhere(BP.OA.BBS.BBSReplayAttr.FK_Motif, FK_Motif);
        qo.addOrderByDesc(BP.OA.BBS.BBSReplayAttr.Ranking);
        qo.DoQuery();
    %>
    <div data-role="page" data-theme="c">
        <div data-role="header" data-theme="b">
            <a href="ListMobileForum.aspx" data-icon="black" data-role="button ">返回</a>
            <h2>
                <%=motif.Name%></h2>
        </div>
        <div class="ui-content" data-role="content" style="padding: 0px;">
            <table style="width: 100%;">
                <tr>
                    <td>
                        <img src="images/default.gif" height="40px" />
                    </td>
                    <td>
                        <%=motif.FK_Emp%>
                        [楼主]<br />
                        <font color="green" size="-3">
                            <%=motif.RDT%>
                        </font>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <%=motif.Contents%>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <hr />
                    </td>
                </tr>
                <%
                    int cs = 1;
                    foreach (BP.OA.BBS.BBSReplay item in BBSR)
                    {
                        cs++;
                %>
                <tr>
                <td><img src="images/default.gif" height="40px" /> </td>
                <td> <%=item.FK_EmpText%> [<%=cs%> 楼] <br /> <font color="green" size="-3">
                            <%=item.AddTime%>
                        </font>  </td>
                </tr>
                <tr>
                <td colspan="2">
                        <%=item.Contents%>
                    </td>
                </tr>
                 <tr>
                    <td colspan="2">
                        <hr />
                    </td>
                </tr>
                <%
                    } %>
            </table>
        </div>
    </div>
</asp:Content>
