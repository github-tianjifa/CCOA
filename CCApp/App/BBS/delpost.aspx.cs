﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using BP.En;
using BP.DA;
using BP.Web;
using BP.Sys;
using BP.OA;
using BP.OA.BBS;

namespace CCOA.App.BBS
{
    public partial class delpost : WebPage
    {
        /// <summary>
        /// 版块编号
        /// </summary>
        public string FK_Class
        {
            get
            {
                return StringFormat.GetQuerystring("FK_Class");
            }
        }
        /// <summary>
        /// 主题编号
        /// </summary>
        public string FK_Motif
        {
            get
            {
                return StringFormat.GetQuerystring("FK_Motif");
            }
        }
        /// <summary>
        /// 回帖编号
        /// </summary>
        public string FK_Replay
        {
            get
            {
                return StringFormat.GetQuerystring("FK_Replay");
            }
        }
        /// <summary>
        /// 删除类别
        /// </summary>
        public string posttype
        {
            get
            {
                return StringFormat.GetQuerystring("posttype");
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            string returnVal = "";
            if (posttype == "motif")
            {
                BBSMotif motif = new BBSMotif();
                motif.RetrieveByAttr(BBSMotifAttr.No, FK_Motif);
                //时间段
                TimeSpan span1 = new TimeSpan(motif.RDT.Ticks);
                TimeSpan spanNow = new TimeSpan(DateTime.Now.Ticks);
                TimeSpan ts = spanNow.Subtract(span1).Duration();
                if (motif.Replays > 0)
                {
                    returnVal = "已有回帖，不能删除。";
                }
                else if (ts.TotalMinutes > 20)
                {
                    returnVal = "发帖超过20分钟，不允许删除。";
                }
                else
                {
                    returnVal = "删除失败";
                    if (motif.Delete() > 0)
                        returnVal = "true";
                }
            }
            else if (posttype == "replay")
            {
                BBSReplay replay = new BBSReplay();
                replay.RetrieveByAttr(BBSReplayAttr.No, FK_Replay);
                TimeSpan span1 = new TimeSpan(replay.LastEditTime.Ticks);
                TimeSpan spanNow = new TimeSpan(DateTime.Now.Ticks);
                TimeSpan ts = spanNow.Subtract(span1).Duration();
                if (ts.TotalMinutes > 20)
                {
                    returnVal = "回帖超过20分钟，不允许删除。";
                }
                else
                {
                    returnVal = "删除失败";
                    if (replay.Delete() > 0)
                        returnVal = "true";
                }
            }
            Response.Write(returnVal);
            Response.End();
        }
    }
}