﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Config.aspx.cs" Inherits="CCOA.App.BBS.SysAdmin.Config" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>无标题页</title>
    <link href="images/AdminDefault.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div class="container" id="cpcontainer">
    <div class="itemtitle">
      <h3>系统设置管理中心</h3>
    </div>
    
    <table class="tb tb2 " align="center">
      <tbody>
        <tr class="hover">
          <td width="20%"><span style="color:Red;">*</span>域名</td>
          <td width="80%"><asp:TextBox ID="txt_webSite" runat="server" CssClass="txt" Width="300"  /></td>
        </tr>
        <tr class="hover">
          <td><span style="color:Red;">*</span>网站名称</td>
          <td><asp:TextBox ID="txt_webName" runat="server" CssClass="txt" Width="300" /></td>
        </tr>
        <tr class="hover">
          <td><span style="color:Red;">*</span>关键字</td>
          <td><asp:TextBox ID="txt_keywords" runat="server" CssClass="txt" Width="300" /></td>
        </tr>
        <tr class="hover">
          <td>网站描述</td>
          <td><asp:TextBox ID="txt_description" runat="server" CssClass="txt" Width="300" TextMode="MultiLine" Height="150" /></td>
        </tr>
        <tr class="hover">
          <td>过滤词</td>
          <td><asp:TextBox ID="txt_filter" runat="server" CssClass="txt" Width="300" TextMode="MultiLine"  Height="150" /></td>
        </tr>
        <tr class="hover">
          <td><span style="color:Red;">*</span>发贴加经验</td>
          <td><asp:TextBox ID="txt_F_exp" runat="server" CssClass="txt" Width="300" /></td>
        </tr>
        <tr class="hover">
          <td><span style="color:Red;">*</span>发贴加积分</td>
          <td><asp:TextBox ID="txt_F_integral" runat="server" CssClass="txt" Width="300" /></td>
        </tr>
        <tr class="hover">
          <td><span style="color:Red;">*</span>发贴加金币</td>
          <td><asp:TextBox ID="txt_F_gold" runat="server" CssClass="txt"  Width="300"/></td>
        </tr>
        <tr class="hover">
          <td><span style="color:Red;">*</span>回帖加经验</td>
          <td><asp:TextBox ID="txt_R_exp" runat="server" CssClass="txt"  Width="300"/></td>
        </tr>
        <tr class="hover">
          <td><span style="color:Red;">*</span>回帖加积分</td>
          <td><asp:TextBox ID="txt_R_integral" runat="server" CssClass="txt" Width="300" /></td>
        </tr>
        <tr class="hover">
          <td><span style="color:Red;">*</span>回帖加金币</td>
          <td><asp:TextBox ID="txt_R_gold" runat="server" CssClass="txt" Width="300" /></td>
        </tr>
        <%--<tr class="hover">
          <td>上传图片的水印</td>
          <td>
              <asp:RadioButtonList ID="WaterSet" runat="server">
                <asp:ListItem Text="不添加水印" Value="no" />
                <asp:ListItem Text="添加文字水印" Value="txt" />
                <asp:ListItem Text="添加图片水印" Value="img" />
              </asp:RadioButtonList>
          </td>
        </tr>--%>
        <%--<tr class="hover">
          <td>水印文字（或图片地址）</td>
          <td><asp:TextBox ID="txt_Water" runat="server" Text="不添加水印" /></td>
        </tr>--%>
        <tr class="hover">
          <td colspan="2"><asp:Button ID="btn_Save" Text="保存设置" runat="server" CssClass="btn" 
                  onclick="btn_Save_Click" /></td>
        </tr>
      </tbody>
    </table>
    
    </div>
    </form>
</body>
</html>
