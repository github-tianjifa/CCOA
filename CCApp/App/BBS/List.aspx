﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App/BBS/BBSPage.Master" AutoEventWireup="true"
    CodeBehind="List.aspx.cs" Inherits="CCOA.App.BBS.List" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <ul class="sorttag clearfix" style="margin-top: 8px;">
        <li class="ntop"></li>
        <li class="ncurr"><a>全部</a></li>
        <li class="nbott"></li>
    </ul>
    <!-- page -->
    <div class="page" runat="server" id="PageTopShow">
        <span>总贴数：<font class="forange">0</font>篇</span> <span class="pgs"><a class='pcurr'>
            1</a><a href="">下一页</a></span> <span>第<input type="text" size="3" class='ipt_pg' />页&nbsp;<a
                style="cursor: pointer" class='btn_go' onclick="">GO</a></span>
    </div>
    <!-- page end -->
    <!-- 主题分类 -->
    <div class="threadtype clearfix">
        <table border="0" cellspacing="0" cellpadding="0">
            <tr>
                <th>
                    &nbsp; &nbsp; &nbsp; &nbsp; 标题
                </th>
                <td class="author">
                    作者
                </td>
                <td class="num">
                    查看 / 回复
                </td>
                <td class="lastpost">
                    最后发表
                </td>
            </tr>
        </table>
    </div>
    <!-- 主题分类end -->
    <div class="threadlist">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <asp:DataList ID="datalistF" RepeatLayout="Flow" runat="server" Width="100%" ShowFooter="False"
                ShowHeader="False" RepeatDirection="Horizontal">
                <ItemTemplate>
                    <tr>
                        <td class="ico">
                            <%#StateImg((int)Eval("Hits"), (int)Eval("Replays"))%>
                        </td>
                        <th>
                            <span class="type">
                                <%#Eval("MotifState")%></span><strong><a class="fbold" href='thread.aspx?FK_Class=<%#Eval("FK_Class") %>&FK_Motif=<%#Eval("No") %>'
                                ><%#Eval("Name") %></a></strong>
                        </th>
                        <td class="author">
                            <a href="userCenter.aspx?uid=<%#Eval("FK_Emp") %>">
                                <%#Eval("FK_EmpText")%></a><i><%#DateTime.Parse(Eval("RDT").ToString()).ToString("yyyy-MM-dd HH:mm")%></i>
                        </td>
                        <td class="num">
                            <em>
                                <%#Eval("Hits")%></em> /
                            <%#Eval("Replays")%>
                        </td>
                        <td class="lastpost">
                            <%#DateTime.Parse(Eval("LastReTime").ToString()).ToString("yyyy-MM-dd HH:mm")%>
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:DataList>
        </table>
    </div>
    <div class="page" id="PageDownShow" runat="server">
    </div>
</asp:Content>
