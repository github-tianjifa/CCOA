﻿<%@ Page Language="C#" MasterPageFile="~/App/Message/Site1.Master" AutoEventWireup="true"
    CodeBehind="InBox.aspx.cs" Inherits="CCOA.App.Message.InBox" %>

<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../../js/zDialog/zDialog.js" type="text/javascript"></script>
    <link href="../../CSS/GridviewPager.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        function checkAll() {
            $(".checkdelete").prop("checked", $("#checkall").prop("checked"));
        }
        function checkHasSel() {
            var selCount = 0;
            $(".checkdelete").each(function () {
                if ($(this).prop("checked") == true) selCount++;
            });
            if (selCount == 0) {
                alert("你没有选择记录");
                return false;
            }
            else {
                return confirm("你真要删除你所选择的信息吗？");
            }
        }
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <div>
        <div>
            <div style="width: 200px; float: left; padding-top: 10px;">
                <asp:Label runat="server" ID="lblMsg"></asp:Label>
            </div>
            <div style="width: 500px; float: right;">
                <asp:Button ID="btnEmptyBox" runat="server" Text="清空收件箱" OnClientClick="return confirm('你真的要清空收件箱吗？')"
                    OnClick="btnEmptyBox_Click" />
                <asp:Button ID="btnDeleteMsg" runat="server" Text="删除所选信息" OnClientClick="return checkHasSel()"
                    OnClick="btnDeleteMsg_Click" />
                输入关键字:<asp:TextBox ID="txtKeywords" runat="server"></asp:TextBox>
                <asp:Button ID="btnSearchByKey" runat="server" Text="查询" OnClick="btnSearchByKey_Click" /></div>
            <div style="clear: both;">
            </div>
        </div>
        <asp:GridView ID="gridData" runat="server" CssClass="grid" AutoGenerateColumns="False">
            <Columns>
                <asp:TemplateField HeaderText="全选" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="40px">
                    <HeaderTemplate>
                        <input type="checkbox" title='全选' id="checkall" onclick='checkAll()' />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <input type="checkbox" name="chkSel" value='<%# Eval("No") %>' class="checkdelete" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="标记" ItemStyle-Width="17px" ItemStyle-HorizontalAlign="Center">
                    <HeaderTemplate>
                        <img style="width: 15px; height: 12px;" src="img/Mail_Read.png" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <img style="height: 12px;" src="<%# Convert.ToInt32(Eval("Received"))==1?"Img/Mail_Read.png":"Img/Mail_UnRead.png" %>"
                            title="<%# Convert.ToInt32(Eval("Received"))==1?"已读":"未读" %>" alt="是否已读状态" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="标记" ItemStyle-Width="10px" ItemStyle-HorizontalAlign="Center">
                    <HeaderTemplate>
                        <img style="height: 12px;" src="img/Attach.png" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <a href="#" title="有附件" style="display: <%# String.IsNullOrEmpty(Convert.ToString(Eval("AttachFile")))?"none":""%>">
                            <img alt="有附件" style="height: 12px;" src="<%# String.IsNullOrEmpty(Convert.ToString(Eval("AttachFile")))?"":"Img/Attach.png" %>" />
                        </a>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="主题">
                    <ItemTemplate>
                        <a style="font-weight: bold; color: #444;" href="<%# GetUrl(Eval("No")) %>">
                            <%#GetContentStr(Eval("Title")) %></a>
                        <%# Convert.ToInt32(Eval("Received"))==0?"<img alt='新邮件' src='Img/email_new.gif'>":"" %></ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="发件人" ItemStyle-Width="80px" ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                        <%#this.GetUserNames(Eval("Sender"))%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="发送时间" ItemStyle-Width="90px" ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                        <span title="<%# Eval("RPT") %>">
                            <%#BP.OA.Main.GetTimeEslapseStr(Convert.ToDateTime(Eval("RPT")),null,null) %></span>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="回复" ItemStyle-Width="90px" ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                        <a href="<%#Eval("No","SendMsg.aspx?Re={0}") %>">回复</a>&nbsp;&nbsp; <a href="<%#Eval("No","SendMsg.aspx?Trans={0}") %>">
                            转发</a></ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
        <table cellpadding="0" cellspacing="0" align="center" width="99%" class="border">
            <tr>
                <td align="left" colspan="2">
                    <webdiyer:AspNetPager ID="AspNetPager1" CssClass="paginator" CurrentPageButtonClass="cpb"
                        runat="server" AlwaysShow="false" FirstPageText="首页" LastPageText="尾页" NextPageText="下一页"
                        PrevPageText="上一页" ShowCustomInfoSection="Left" ShowInputBox="Never" OnPageChanged="AspNetPager1_PageChanged"
                        CustomInfoTextAlign="Left" LayoutType="Table">
                    </webdiyer:AspNetPager>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
