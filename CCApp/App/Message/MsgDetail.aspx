﻿<%@ Page Title="" Language="C#" MasterPageFile="../../Main/master/Site1.Master" AutoEventWireup="true"
    CodeBehind="MsgDetail.aspx.cs" Inherits="CCOA.App.Message.MsgDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cph_head" runat="server">
    <script src="../../Js_EasyUI/jquery-1.7.2.min.js" type="text/javascript"></script>
    <style type="text/css">
        body
        {
            font-size: 13px;
        }
        .doc-table
        {
            border-collapse: collapse;
            border-spacing: 0;
            margin-left: 9px;
            display: table;
            border-color: gray;
        }
        .doc-table th
        {
            background: #EEE;
        }
        .doc-table th, .doc-table td
        {
            border: 1px solid #BDDBEF;
            padding: 3px 3px;
        }
        td.title
        {
            width: 100px;
            text-align: right;
        }
        td.text
        {
            width: 445px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_title" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_body" runat="server">
    <div>
        <div style="margin: 30px 100px; width: 600px;">
            <table class="doc-table">
                <caption style="text-align: left; font-size: 14px; font-weight: bold;">
                    详细信息</caption>
                <tr>
                    <td class="title">
                        收件人:
                    </td>
                    <td>
                        <div id="divUsers" runat="server" style="display: block; height: 50px; width: 80%; margin-top: 5px; padding-top: 5px;
                            border-top:1px;">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="title">
                        时间:
                    </td>
                    <td class="text">
                        <asp:Literal runat="server" ID="lblSTime"></asp:Literal>
                    </td>
                </tr>
                <tr>
                    <td class="title">
                        内容:
                    </td>
                    <td class="text">
                    <div id="divContent" runat="server" style=" width:80%; height:250px; border:1px;">
                    
                    </div>
                    </td>
                </tr>
                <tr>
                    <td class="title">
                        附件:
                    </td>
                    <td>
                    <asp:LinkButton ID="afile" runat="server" OnClick="HrefClik">下载</asp:LinkButton>
                    </td>
                </tr>
                <tr>
                    <td colspan='2' style="text-align: center">
                        <asp:Button ID="btnClose" runat="server" Text="返回" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
