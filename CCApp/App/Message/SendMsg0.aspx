﻿<%@ Page Title="" Language="C#" MasterPageFile="../../Main/master/Site1.Master" AutoEventWireup="true"
    CodeBehind="SendMsg0.aspx.cs" Inherits="CCOA.App.Message.SendMsg0" ClientIDMode="Static" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cph_head" runat="server">
    <script src="../../Js/zDialog/zDialog.js" type="text/javascript"></script>
    <style type="text/css">
        body
        {
            font-size: 13px;
        }
        .doc-table
        {
            border-collapse: collapse;
            border-spacing: 0;
            margin-left: 9px;
            display: table;
            border-color: gray;
        }
        .doc-table th
        {
            background: #EEE;
        }
        .doc-table th, .doc-table td
        {
            border: 1px solid #BDDBEF;
            padding: 3px 3px;
        }
        td.title
        {
            width: 100px;
            text-align: right;
        }
        td.text
        {
            width: 445px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_title" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_body" runat="server">
    <div>
        <div style="margin: 30px 100px; width: 600px;">
            <table class="doc-table">
                <caption style="text-align: left; font-size: 14px; font-weight: bold;">
                    发送信息</caption>
                <tr>
                    <td class="title">
                        收件人:
                    </td>
                    <td>
                      <div id="div_Users" style="display:block; margin-top: 5px; padding-top: 5px;">
                        <asp:TextBox ID="txt_Users" runat="server" TextMode="MultiLine" Width="85%" Rows="5"></asp:TextBox>
                        <asp:HiddenField ID="txt_UserNames" runat="server"></asp:HiddenField>
                        <input type="button" value="add.." onclick="zDialog_open2('/ctrl/SelectUsers/SelectUser.aspx', '选择人员', 500, 400,'#txt_UserNames','txt_UserNames','txt_Users');" />
                    </div>
                     </td>
                </tr>
                <tr>
                    <td class="title">
                        时间:
                    </td>
                    <td class="text">
                        <asp:Label runat="server" ID="lblSTime"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="title">
                        内容:
                    </td>
                    <td class="text">
                        <asp:TextBox  ID="txtContent" TextMode="MultiLine" runat="server" Style="width: 97%; height: 200px;"></asp:TextBox>
                    </td>
                </tr>
                <tr style="">
                    <td class="title">
                        附件:
                    </td>
                    <td>
                         <asp:FileUpload ID="FileUpload1" runat="server" Width="80%" />
                    </td>
                </tr>
                <tr>
                    <td colspan='2' style="text-align: center">
                        <asp:Button ID="btnSaveMsg" runat="server" Text="保存为草稿" OnClick="btnSaveMsg_Click" />&nbsp;&nbsp;
                        <asp:Button ID="btnSendMsg" runat="server" Text="发送" OnClick="btnSendMsg_Click" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
