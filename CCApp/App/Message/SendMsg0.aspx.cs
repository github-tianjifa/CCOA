﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BP.OA.Message;
namespace CCOA.App.Message
{
    public partial class SendMsg0 : System.Web.UI.Page
    {
        #region //1.公共接口(Public Interface)
        //权限控制
        private string FuncNo = null;
        private void Alert(string msg)
        {
            BP.OA.Debug.Alert(this, msg);
        }
        private string CurUserNo
        {
            get { return BP.Web.WebUser.No; }
        }
        /// <summary>
        /// 获取路径函数
        /// </summary>
        /// <returns></returns>
        private string GetAttachPathName()
        {
            string dir = String.Format("/UploadFiles/Message/{0:yyyyMMdd}", DateTime.Now);
            return dir;
        }
        /// <summary>
        /// 获取新文件名函数
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        private string GetAttachFileName(string fileName)
        {
            string f = System.IO.Path.GetFileNameWithoutExtension(fileName);
            string ext = System.IO.Path.GetExtension(fileName);
            string newFileName = String.Format("{0}_{1:yyyyMMddHHmmss}{2}", f, DateTime.Now, ext);
            return newFileName;
        }
        #endregion

        #region //2.页面装载(Page Load)
        protected void Page_Init(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!BP.OA.GPM.IsCanUseFun(this.FuncNo)) { BP.OA.GPM.RedirectNoAccess(); return; }
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this.lblSTime.Text = String.Format("&nbsp;&nbsp;{0}", DateTime.Now);
                this.LoadData();
            }
        }
        private void LoadData()
        {

        }
        private void ClearCtrl()
        {
            this.txt_UserNames.Value = String.Empty;
            this.txt_Users.Text = String.Empty;
            this.txtContent.Text = String.Empty;
        }

        #endregion
        
        #region //3.页面事件(Page Event)
        /// <summary>
        /// 发送 信息
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSendMsg_Click(object sender, EventArgs e)
        {
            // 1.获取UI值   
            string users = this.txt_UserNames.Value;
            string dtSTime = this.lblSTime.Text.ToString();
            string content = this.txtContent.Text;
            string names = this.txt_Users.Text;
            string AttachFile = null;
            //附件
            
            // 2.验证   
            if (string.IsNullOrEmpty(users))
            {
                this.Alert("收件人不能为空"); return;
            }
            if (string.IsNullOrEmpty(content))
            {
                this.Alert("信息内容不能为空"); return;
            }
            // 3.数据处理
            if (this.FileUpload1.HasFile)
            {
                string dir = this.GetAttachPathName();
                if (!System.IO.Directory.Exists(Server.MapPath(dir)))
                    System.IO.Directory.CreateDirectory(Server.MapPath(dir));

                string newFileName = this.GetAttachFileName(this.FileUpload1.FileName);

                string path = String.Format("{0}/{1}", dir, newFileName);
                //string filePath = this.FileUpload1.PostedFile.FileName;
                this.FileUpload1.SaveAs(Server.MapPath(path));
                AttachFile = path;
                try
                {

                }
                catch
                {
                    ;
                }
            }
            BP.OA.Message.Message msg = new BP.OA.Message.Message();
            msg.AddTime = DateTime.Now;
            msg.Doc = content;
            msg.FK_UserNo = BP.Web.WebUser.No;
            msg.MessageState = 1;
            msg.SendToUsers = users;
            msg.AttachFile = AttachFile;
            msg.Insert();
            msg.Doc = content;
            msg.Update();
            if (msg.OID == 0)
            {
                this.Alert("消息发送失败！"); return;
            }
            if (true)
            {  //发件箱
                BP.OA.Message.SendBox sb = new BP.OA.Message.SendBox();
                sb.FK_MsgNo = msg.OID;
                sb.FK_SendUserNo = BP.Web.WebUser.No;
                sb.Receiver = names;
                sb.SendTime = DateTime.Now;                
                sb.Insert();
            }
            String[] arr_users = users.Split(new String[]{","},StringSplitOptions.RemoveEmptyEntries);
            foreach (String user in arr_users)
            {   //收件箱 n个人
                BP.OA.Message.InBox ib = new BP.OA.Message.InBox();
                ib.AddTime = DateTime.Now;
                ib.FK_MsgNo = msg.OID;
                ib.FK_ReceiveUserNo = user;
                ib.Sender = BP.Web.WebUser.No;
                ib.Received = false;
                ib.Insert();
            }
            // 4.数据统计、日志
            // 5.提交或返回
            this.Alert(String.Format("消息发送成功，发送给人员（{0}）！",names));
            this.ClearCtrl();
        }
        protected void btnSaveMsg_Click(object sender, EventArgs e)
        {
            // 1.获取UI值   
            string users = this.txt_UserNames.Value;
            string dtSTime = this.lblSTime.Text.ToString();
            string content = this.txtContent.Text;
            string names = this.txt_Users.Text;
            string AttachFile = null;
            //附件

            // 2.验证   
            if (string.IsNullOrEmpty(users))
            {
                this.Alert("收件人不能为空"); return;
            }
            if (string.IsNullOrEmpty(content))
            {
                this.Alert("信息内容不能为空"); return;
            }
            // 3.数据处理
            if (this.FileUpload1.HasFile)
            {
                string dir = this.GetAttachPathName();
                if (!System.IO.Directory.Exists(Server.MapPath(dir)))
                    System.IO.Directory.CreateDirectory(Server.MapPath(dir));

                string newFileName = this.GetAttachFileName(this.FileUpload1.FileName);

                string path = String.Format("{0}/{1}", dir, newFileName);
                //string filePath = this.FileUpload1.PostedFile.FileName;
                this.FileUpload1.SaveAs(Server.MapPath(path));
                    AttachFile = path;
                try
                {
                   
                }
                catch
                {
                    ;
                }
            }
            BP.OA.Message.Message msg = new BP.OA.Message.Message();
            msg.AddTime = DateTime.Now;
            msg.Doc = content;
            msg.FK_UserNo = BP.Web.WebUser.No;
            msg.MessageState = 0;
            msg.SendToUsers = users;
            msg.AttachFile = AttachFile;
            msg.Insert();
            msg.Doc = content;
            msg.Update();
            if (msg.OID == 0)
            {
                this.Alert("消息保存到草稿箱失败！"); return;
            }
            if (true)
            {//草稿箱
                BP.OA.Message.DraftBox dft = new DraftBox();
                dft.AddTime = DateTime.Now;
                dft.FK_MsgNo = msg.OID;
                dft.FK_UserNo = BP.Web.WebUser.No;
                dft.Insert(); 
            }
            // 4.数据统计、日志
            // 5.提交或返回            
            this.Alert(String.Format("消息成功保存到草稿箱！", names));
            this.ClearCtrl();
        }

        #endregion

    }
}