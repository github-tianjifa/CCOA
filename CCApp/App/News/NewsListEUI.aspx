﻿<%@ Page Title="" Language="C#" MasterPageFile="../AppMaster/AppSite.Master" AutoEventWireup="true"
    CodeBehind="NewsListEUI.aspx.cs" Inherits="CCOA.App.News.NewsListEUI" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../../Js/zDialog/zDialog.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        //加载grid后回调函数
        function LoadDataGridCallBack(js, scorp) {
            $("#pageloading").hide();
            if (js == "") js = "[]";

            var pushData = eval('(' + js + ')');
            $('#newsGrid').datagrid({
                columns: [[
                    { checkbox: true },
                    { field: 'OID', title: '编号', width: 60 },
                    { field: 'FK_ArticleCatagoryText', title: '新闻目录', width: 150 },
                    { field: 'Title', title: '标题', width: 360, formatter: function (value, rec) {
                        var title = "<a href='NewsEdit.aspx?page=nlEUI&ID=" + rec.OID + "'>" + rec.Title + "</a>";

                        var d1 = new Date();
                        var d2 = new Date(rec.AddTime.replace(/-/g, "/"));
                        var h = parseInt((d1 - d2) / 3600000);
                        //小于2小时
                        if (h < 2) {
                            title += "&nbsp;&nbsp;<img src='img/news_New.gif' alt='最新'/>";
                        }
                        //置顶
                        if (rec.SetTop) {
                            title += "&nbsp;&nbsp;<img src='img/top.gif' alt='置顶'/>";
                        }
                        return title;
                    }
                    },
                    { field: 'KeyWords', title: '关键字', width: 120 },
                    { field: 'AddTime', title: '发布时间', width: 120 },
                    { field: 'BrowseCount', title: '浏览', align: 'center', width: 80, formatter: function (value, rec) {
                        return rec.BrowseCount + "次";
                    }
                    }
                ]],
                data: pushData,
                width: 'auto',
                height: 'auto',
                striped: true,
                rownumbers: true,
                selectOnCheck: false,
                checkOnSelect: true,
                singleSelect: true,
                pagination: true,
                remoteSort: false,
                fitColumns: true,
                pageNumber: scorp.pageNumber,
                pageSize: scorp.pageSize,
                pageList: [20, 30, 40, 50],
                onDblClickCell: function (index, field, value) {
                    var row = $('#newsGrid').datagrid('getSelected');
                    if (row) {
                        if (window.parent.addTab && 1==2) {//先关闭
                            window.parent.addTab(row.Title, "../App/News/NewsEdit.aspx?type=nlEUI&ID=" + row.OID, "");
                        } else {
                            window.location.href = "NewsEdit.aspx?ID=" + row.OID;
                        }
                    }
                },
                loadMsg: '数据加载中......'
            });
            //分页
            var pg = $("#newsGrid").datagrid("getPager");
            if (pg) {
                $(pg).pagination({
                    onRefresh: function (pageNumber, pageSize) {
                        LoadGridData(pageNumber, pageSize);
                    },
                    onSelectPage: function (pageNumber, pageSize) {
                        LoadGridData(pageNumber, pageSize);
                    }
                });
            }
        }

        //加载grid
        function LoadGridData(pageNumber, pageSize) {
            this.pageNumber = pageNumber;
            this.pageSize = pageSize;
            var catagory = $("#NewCatagory").val();
            var keyWords = $("#TB_KeyWords").val();

            var params = {
                method: "getnewslist",
                keyWords: encodeURI(keyWords),
                pageNumber: pageNumber,
                pageSize: pageSize,
                newsCatagory: catagory
            };
            queryData(params, LoadDataGridCallBack, this);
        }
        //绑定下拉框
        function InitNewsCatagory() {
            var params = {
                method: "getnewcatagory"
            };

            queryData(params, function (js, scope) {
                var pushData = eval('(' + js + ')');

                var objSelect = document.getElementById("NewCatagory");
                var option = new Option("所有类型", "0");
                objSelect.options.add(option);

                for (var i = 0; i < pushData.length; i++) {
                    var varItem = new Option(pushData[i].Name, pushData[i].No);
                    objSelect.options.add(varItem);
                }
            }, this);
        }
        //添加新闻
        function NewsAdd() {
            if (window.parent.addTab) {
                window.parent.addTab("添加新闻", "../App/News/NewsAdd.aspx", "");
            } else {
                window.location.href = "NewsAdd.aspx";
            }
        }
        //新闻编辑
        function NewsEdit() {
            var row = $('#newsGrid').datagrid('getSelected');
            if (row) {
                window.location.href = "NewsEdit.aspx?ID=" + row.OID;
            }
            else {
                $.messager.alert('提示', '请选择记录后再试！', 'info');
            }
        }
        //删除邮件
        function DelNews() {
            var rows = $('#newsGrid').datagrid('getChecked');
            var ids = [];
            if (rows && rows.length > 0) {
                for (var i = 0; i < rows.length; i++) {
                    ids.push(rows[i].OID);
                    test = rows[i].OID;
                }
            }
            //判断是否选中项
            if (ids.length == 0) {
                $.messager.alert('提示', '请选择记录后再试！', 'info');
                return;
            }
            $.messager.confirm('确认', '您确认想要删除记录吗？', function (r) {
                if (r) {
                    var params = {
                        method: 'newsdelete',
                        newIds: ids.join(',')
                    };
                    queryData(params, function (js, scope) {
                        var grid = $('#newsGrid');
                        var options = grid.datagrid('getPager').data("pagination").options;
                        var curPage = options.pageNumber;
                        LoadGridData(curPage, 20);
                    }, this);
                }
            }); 
        }
        //置顶\取消置顶
        function SetNewsTop(model) {
            var rows = $('#newsGrid').datagrid('getChecked');
            var ids = [];
            if (rows && rows.length > 0) {
                for (var i = 0; i < rows.length; i++) {
                    ids.push(rows[i].OID);
                    test = rows[i].OID;
                }
            }
            //判断是否选中项
            if (ids.length == 0) {
                $.messager.alert('提示', '请选择记录后再试！', 'info');
                return;
            }

            var params = {
                method: 'setnewstop',
                model:model,
                newIds: ids.join(',')
            };
            queryData(params, function (js, scope) {
                var pg = $('#newsGrid').datagrid('getPager');
                var curPage = $(pg).pagination.pageNumber;
                LoadGridData(curPage, 20);
            }, this);
        }

        //初始化
        $(function () {
            $("#pageloading").show();
            InitNewsCatagory();
            LoadGridData(1, 20);
        });

        //公共方法
        function queryData(param, callback, scope, method, showErrMsg) {
            if (!method) method = 'GET';
            $.ajax({
                type: method, //使用GET或POST方法访问后台
                dataType: "text", //返回json格式的数据
                contentType: "application/json; charset=utf-8",
                url: "NewsDataService.ashx", //要访问的后台地址
                data: param, //要发送的数据
                async: false,
                cache: false,
                complete: function () { }, //AJAX请求完成时隐藏loading提示
                error: function (XMLHttpRequest, errorThrown) {
                    $("body").html("<b>访问页面出错，请联系管理员。<b>");
                    //callback(XMLHttpRequest);
                },
                success: function (msg) {//msg为返回的数据，在这里做数据绑定
                    var data = msg;
                    callback(data, scope);
                }
            });
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="pageloading">
    </div>
    <div data-options="region:'center'" border="false" style="margin: 0; padding: 0;
        overflow: hidden;">
        <div id="tb" style="padding: 6px; height: 28px;">
            <div style="float: left;">
                新闻目录：<select id="NewCatagory" name="NewCatagory"></select>
                关键字：<input id="TB_KeyWords" type="text" style="width: 150px;" />
            </div>
            <a id="DoQueryByKey" style="float: left;" href="#" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-search'"
                onclick="LoadGridData(1, 20)">查询</a>
            <div class="datagrid-btn-separator">
            </div>
            <a id="addNews" href="#" style="float: left;" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-addfile'"
                onclick="NewsAdd()" >添加新闻</a> 
            <a id="editNews" href="#" style="float: left;" class="easyui-linkbutton"
                    data-options="plain:true,iconCls:'icon-edit'" onclick="NewsEdit()">编辑</a>
            <a id="deleteNews" href="#" style="float: left;" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-delete'"
                onclick="DelNews()">删除</a> 
            <div class="datagrid-btn-separator">
            </div>
            <a id="setTop" href="#" style="float: left;" class="easyui-linkbutton"
                    data-options="plain:true,iconCls:'icon-up'" onclick="SetNewsTop('setnewstop')">置顶</a>
            <a id="cancelTop" href="#" style="float: left;" class="easyui-linkbutton"
                    data-options="plain:true,iconCls:'icon-down'" onclick="SetNewsTop('cancelnewstop')">取消置顶</a>
        </div>
        <table id="newsGrid" fit="true" fitcolumns="true" toolbar="#tb" class="easyui-datagrid">
        </table>
    </div>
</asp:Content>
