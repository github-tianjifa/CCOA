﻿<%@ Page Title="" Language="C#" MasterPageFile="../../Main/master/Site1.Master" ValidateRequest="false" ClientIDMode="Static" AutoEventWireup="true" CodeBehind="NewsAdd.aspx.cs" Inherits="CCOA.App.News.NewsAdd" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cph_head" runat="server">
    <style type="text/css">
        body
        {
            font-size: 13px;
        }
    </style>
    <link rel="stylesheet" href="../../ctrl/kindeditor/themes/default/default.css" />
    <link rel="stylesheet" href="../../ctrl/kindeditor/plugins/code/prettify.css" />
    <script type="text/javascript" charset="utf-8" src="../../ctrl/kindeditor/kindeditor.js"></script>
    <script type="text/javascript" charset="utf-8" src="../../ctrl/kindeditor/lang/zh_CN.js"></script>
    <script type="text/javascript" charset="utf-8" src="../../ctrl/kindeditor/plugins/code/prettify.js"></script>
    <script type="text/javascript">
            KindEditor.ready(function (K) {
            var editor1 = K.create('#ui_Doc', {
                cssPath: '../../ctrl/kindeditor/plugins/code/prettify.css',
                uploadJson: '../../ctrl/kindeditor/asp.net/upload_json.ashx',
                fileManagerJson: '../../ctrl/kindeditor/asp.net/file_manager_json.ashx',
                allowFileManager: true
            });
            editor1.sync();
            prettyPrint();
        });
    </script>
    <script src="../../Js/jquery.MultiFile.js" charset="gb2312" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_title" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_body" runat="server">
    <div style=" padding: 1px 10px 10px 30px; font-size:16px;  font-weight:bold;">添加新闻</div>
    <table style="width:100%;">
        <tr>
            <td style="text-align:right;width:100px;">
                <span style="color:Red;">*</span>&nbsp;新闻标题：
            </td>
            <td>
                <asp:TextBox ID="ui_Title" runat="server" Width="97%" MaxLength="100"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="text-align:right;">
                <span style="color:Red;">*</span>&nbsp;新闻栏目：
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ui_ArticleCatagory" Width="200px"></asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td style="text-align: right;">
                新闻来源：
            </td>
            <td>
                <asp:TextBox ID="ui_ArticleSource" runat="server" Width="400" MaxLength="100"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="text-align: right;">
                新闻关键字：
            </td>
            <td>
                <asp:TextBox ID="ui_KeyWords" runat="server" Width="400" MaxLength="100"></asp:TextBox>（关键字用空格分开）
            </td>
        </tr>
        <tr>
            <td style="text-align: right;">
                <span style="color:Red;">*</span>&nbsp;新闻内容：
            </td>
            <td>
                <asp:TextBox ID="ui_Doc" runat="server" Width="97%" Rows="30" TextMode="MultiLine"></asp:TextBox><br />
            </td>
        </tr>
        <tr style="">
            <td class="title">
                &nbsp;附件:
            </td>
            <td class="text">
                <div id="div_fu_1" style="width:200px;"><asp:FileUpload ID="FileUpload1" runat="server" Width="80%" CssClass="multi" /></div>
            </td>
        </tr>
        <tr style="">
            <td class="title">
                &nbsp;是否提醒:
            </td>
            <td class="text">
                <asp:CheckBox runat="server" ID="cbMsg" />
            </td>
        </tr>
       </table>
    <div style="text-align: center;">
        <asp:Button runat="server" ID="btnSubmit" Text="添加新闻" OnClick="btnSubmit_Click" /></div>
</asp:Content>
