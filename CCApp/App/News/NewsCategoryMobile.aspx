﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App/Mobile.Master" AutoEventWireup="true"
    CodeBehind="NewsCategoryMobile.aspx.cs" Inherits="CCOA.App.News.NewsCategoryMobile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../../CCMobile/css/themes/default/jquery.mobile-1.4.5.min.css" rel="stylesheet"
        type="text/css" />
    <link href="../../CCMobile/css/themes/classic/theme-classic.css" rel="stylesheet"
        type="text/css" />
    <script src="../../CCMobile/js/jquery.js" type="text/javascript"></script>
    <script src="../../CCMobile/js/jquery.mobile-1.4.5.min.js" type="text/javascript"></script>
    <script src="../../CCMobile/js/comment/action.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%
        string No = this.Request.QueryString["No"];
        BP.OA.ArticleCatagory cats = new BP.OA.ArticleCatagory(No);

        BP.OA.Articles ens = new BP.OA.Articles();
        ens.RetrieveAll();
    %>
    <div data-role="page" data-theme="e">
        <div data-role="header" data-position="fixed" data-theme="b">
            <a href="/CCMobile/Home.aspx" data-icon="black" data-role="button ">返回</a>
            <h2>
                <%=cats.Name%></h2>
        </div>
        <div class="ui-content" data-role="main" style="padding: 0px;">
            <table style="width: 100%;">
                <%
                    int cs = 0;//显示条数控制
                    foreach (BP.OA.Article item in ens)
                    {

                        if (item.FK_ArticleCatagory != No)
                            continue;
                        cs++;
                        if (cs > 21)
                            break;
                        
                %>
                <tr>
                    <td>
                        <img src="img/xinwen.png" style="width: 60px;" />
                    </td>
                    <td>
                        <a href="/App/News/NewArticleMobile.aspx?ID=<%=item.OID %>">
                            <%=item.Title %></a>
                        <br />
                        <font color="green" size="-3">
                            <%= BP.DA.DataType.ParseSysDate2DateTimeFriendly( item.AddTime) %>
                            |
                            <%= item.FK_UserNo %>
                        </font>
                    </td>
                </tr>
                <tr>
                <td colspan="2"><hr /></td>
                </tr>
                <%}
                %>
            </table>
        </div>
    </div>
</asp:Content>
