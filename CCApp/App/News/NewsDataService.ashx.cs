﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;

using BP;
using BP.En;
using BP.DA;
using BP.Sys;
using BP.Web;
using BP.OA;
using BP.Port;
using BP.Tools;

namespace CCOA.App.News
{
    /// <summary>
    /// NewsDataService 的摘要说明
    /// 新闻管理数据操作
    /// </summary>
    public class NewsDataService : IHttpHandler
    {
        /// <summary>
        /// 封装有关个别 HTTP 请求的所有 HTTP 特定的信息
        /// </summary>
        HttpContext _Context = null;

        public string getUTF8ToString(string param)
        {
            return HttpUtility.UrlDecode(_Context.Request[param], System.Text.Encoding.UTF8);
        }

        public void ProcessRequest(HttpContext context)
        {
            _Context = context;

            if (BP.Web.WebUser.No == null)
                return;

            string method = string.Empty;
            //返回值
            string s_responsetext = string.Empty;
            if (!string.IsNullOrEmpty(_Context.Request["method"]))
                method = _Context.Request["method"].ToString();

            switch (method)
            {
                case "getnewcatagory"://获取新闻类别
                    s_responsetext = GetNewCatagory();
                    break;
                case "getnewslist"://获取新闻列表
                    s_responsetext = GetNewsList();
                    break;
                case "setnewstop"://新闻置顶/取消置顶
                    s_responsetext = SetNewsTop();
                    break;
                case "newsdelete"://删除新闻
                    s_responsetext = DelNews();
                    break;
            }
            if (string.IsNullOrEmpty(s_responsetext))
                s_responsetext = "";
            //组装ajax字符串格式,返回调用客户端
            _Context.Response.Charset = "UTF-8";
            _Context.Response.ContentEncoding = System.Text.Encoding.UTF8;
            _Context.Response.ContentType = "text/html";
            _Context.Response.Expires = 0;
            _Context.Response.Write(s_responsetext);
            _Context.Response.End();
        }

        /// <summary>
        /// 获取新闻类别
        /// </summary>
        /// <returns></returns>
        private string GetNewCatagory()
        {
            ArticleCatagorys catagorys = new ArticleCatagorys();
            catagorys.RetrieveAll();
            return Entitis2Json.ConvertEntities2ListJson(catagorys);
        }

        /// <summary>
        /// 获取新闻列表
        /// </summary>
        /// <returns></returns>
        public string GetNewsList()
        {
            int totalCount = 0;
            string keyWords = getUTF8ToString("keyWords");
            string cataValue = getUTF8ToString("newsCatagory");

            //当前页
            string pageNumber = getUTF8ToString("pageNumber");
            int iPageNumber = string.IsNullOrEmpty(pageNumber) ? 1 : Convert.ToInt32(pageNumber);
            //每页多少行
            string pageSize = getUTF8ToString("pageSize");
            int iPageSize = string.IsNullOrEmpty(pageSize) ? 9999 : Convert.ToInt32(pageSize);

            Articles articles = new Articles();
            QueryObject obj = new QueryObject(articles);
            obj.AddWhere("1=1");
            if (cataValue != "0")
            {
                obj.AddWhere(String.Format(" and FK_ArticleCatagory='{0}'", cataValue));
            }
            if (!String.IsNullOrEmpty(keyWords))
            {
                obj.AddWhere(String.Format(" and Title LIKE '%{0}%' OR KeyWords like '%{0}%'", keyWords));
            }
            totalCount = obj.GetCount();
            obj.addOrderBy(ArticleAttr.SetTop, ArticleAttr.AddTime);
            obj.DoQuery(ArticleAttr.OID, iPageSize, iPageNumber);
            return Entitis2Json.ConvertEntitis2GridJsonOnlyData(articles, totalCount);
        }
        /// <summary>
        /// 新闻置顶
        /// </summary>
        /// <returns></returns>
        private string SetNewsTop()
        {
            try
            {
                //模式，置顶、取消置顶
                string model = getUTF8ToString("model");
                string newIds = getUTF8ToString("newIds");
                string[] idsArrary = newIds.Split(',');
                //置顶
                if (model.Equals("setnewstop"))
                {
                    foreach (string str in idsArrary)
                    {
                        int articleID = Convert.ToInt32(str);
                        Article article = new Article(articleID);
                        DateTime now = DateTime.Now;
                        now = Convert.ToDateTime(String.Format("{0:yyyy-MM-dd HH:mm:ss}", now));
                        article.SetTop = now;
                        article.Update();
                    }
                }
                else if (model.Equals("cancelnewstop"))//取消置顶
                {
                    foreach (string str in idsArrary)
                    {
                        string sSql = String.Format("Update OA_Article set SetTop=''  where OID={0}", str);
                        BP.DA.DBAccess.RunSQL(sSql);
                    }
                }
                return "true";
            }
            catch (Exception ex)
            {

            }
            return "false";
        }

        /// <summary>
        /// 删除新闻
        /// </summary>
        /// <returns></returns>
        private string DelNews()
        {
            try
            {
                string newIds = getUTF8ToString("newIds");
                string[] idsArrary = newIds.Split(',');
                foreach (string str in idsArrary)
                {
                    Article article = new Article();
                    article.Delete(ArticleAttr.OID,str);
                }
                return "true";
            }
            catch (Exception ex)
            {
 
            }
            return "false";
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}