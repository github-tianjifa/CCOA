﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ResSelect.aspx.cs" Inherits="CCOA.App.Meeting.ResSelect" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../../CSS/table.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <asp:Repeater ID="Repeater1" runat="server">
        <HeaderTemplate>
            <table class="table">
                <tr>
                    <th>
                        选择
                    </th>
                    <th>
                        资源编号
                    </th>
                    <th>
                        资源名称
                    </th>
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
                <td>
                    <input type="checkbox" name="cbNo" value='<%#DataBinder.Eval(Container.DataItem ,"No")%>'
                        title='<%#DataBinder.Eval(Container.DataItem ,"Name")%>' <%#ChekcState(DataBinder.Eval(Container.DataItem ,"No"))%> />
                </td>
                <td>
                    <%#DataBinder.Eval(Container.DataItem ,"No")%>
                </td>
                <td>
                    <%#DataBinder.Eval(Container.DataItem ,"Name")%>
                </td>
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
    </asp:Repeater>
    </form>
</body>
</html>
