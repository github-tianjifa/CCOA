﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BP.OA.Meeting;

namespace CCOA.App.Meeting
{
    public partial class AddRoom : System.Web.UI.Page
    {
        public BP.OA.Meeting.Room room = new Room() { Capacity = 0 };
        protected void Page_Load(object sender, EventArgs e)
        {
            string no = Request.QueryString["No"];
            if (!string.IsNullOrEmpty(no))
            {
                room.No = no;
                room.Retrieve();
                Button1.Text = "修改";
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            int i = 0;
            if (Request.QueryString["no"] != null)
            {
                i = BP.OA.UI.PageCommon.Update(this, room);
            }
            else
            {
                i = BP.OA.UI.PageCommon.Insert(this, new Room());
            }
            string strJs = string.Empty;
            if (i > 0)
            {
                strJs = "alert('" + Button1.Text + "成功！" + "');";
            }
            else
            {
                strJs = "alert('" + Button1.Text + "失败！" + "');";
            }
            Refresh(strJs);
        }
       
        public void Refresh(string strJs)
        {
            ClientScript.RegisterStartupScript(this.GetType(), "refresh", strJs + "self.DoRefresh()", true);
        }
      
       

        public string GetEmpName(object obj)
        {
            return MyHelper.GetEmpName(obj);
        }
        public string GetResName(object obj)
        {
            return MyHelper.GetResName(obj);
        }
        public string GetSelectedState(object obj, string state)
        {
            if (obj != null && obj.ToString() == state)
            {
                return "selected='selected'";
            }
            return string.Empty;
        }
    }
}