﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BP.OA.Meeting;

namespace CCOA.App.Meeting
{
    public partial class AddRes : System.Web.UI.Page
    {
        public string No { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            string no = Request.QueryString["no"];
            string name = Request.QueryString["name"];
            if (no != null && name != null)
            {
                No = no;
                ResName.Value = name;
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ResName.Value))
            {
                RoomResource res = new RoomResource();
                res.Name = ResName.Value;
                res.Insert();
                span1.InnerText = "添加成功！";
            }
        }
    }
}