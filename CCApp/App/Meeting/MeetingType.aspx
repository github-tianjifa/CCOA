﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MeetingType.aspx.cs" Inherits="CCOA.App.Meeting.MeetingType"
    EnableViewState="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../../Js/Js_EasyUI/themes/default/easyui.css" rel="stylesheet" type="text/css" />
    <link href="../../Js/Js_EasyUI/themes/default/tree.css" rel="stylesheet" type="text/css" />
    <link href="../../Js/Js_EasyUI/themes/default/datagrid.css" rel="stylesheet" type="text/css" />
    <link href="../../Js/Js_EasyUI/themes/icon.css" rel="stylesheet" type="text/css" />
    <script src="../../Js/jquery-1.7.2.min.js" type="text/javascript"></script>
    <script src="../../Js/zDialog/zDialog.js" type="text/javascript"></script>
    <script src="../../Js/zDialog/zDrag.js" type="text/javascript"></script>
    <script src="../../Js/Validform_v5.3.2_min.js" type="text/javascript"></script>
    <script src="../../Js/Js_EasyUI/jquery.easyui.min.js" type="text/javascript"></script>
    <script src="../../Js/Js_EasyUI/locale/easyui-lang-zh_CN.js" type="text/javascript"
        charset="UTF-8"></script>
    <script type="text/javascript">
        function openDialog(sender, e) {
            var diag = new Dialog();
            diag.Width = 500;
            diag.Height = 120;
            diag.Title = "内容页为外部连接的窗口";
            diag.URL = e;
            diag.show();
            ZDIALOG_DIALOG = diag;
        }
        function zDialog_close() {
            if (ZDIALOG_DIALOG != null)
                ZDIALOG_DIALOG.close();
        }
        function doSubmit(no) {
            if (confirm("确定要删除吗？")) {
                $("input[name=hidNo]").val(no);
                $("#form1").submit();
            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <h3 style="background-image: url( ../../Images/title.png); margin: 0px;">
        <a href="#" onclick="openDialog(this,'AddMeetingType.aspx')">新建会议类型</a>
    </h3>
    <asp:Repeater ID="Repeater1" runat="server">
        <HeaderTemplate>
            <table class="easyui-datagrid">
                <thead>
                    <tr>
                        <th data-options="field:'no'">
                            会议类型编号
                        </th>
                        <th data-options="field:'name'">
                            会议类型名称
                        </th>
                        <th data-options="field:'note'">
                            说明
                        </th>
                        <th data-options="field:'operate'">
                            操作
                        </th>
                    </tr>
                </thead>
                <tbody>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
                <td>
                    <%#DataBinder.Eval (Container.DataItem ,"NO") %>
                </td>
                <td>
                    <%#DataBinder.Eval (Container.DataItem ,"Name") %>
                </td>
                <td>
                    <%#DataBinder.Eval (Container.DataItem ,"Note") %>
                </td>
                <td>
                    <a href="#" onclick="openDialog(this,'AddMeetingType.aspx?no=<%#Eval ("NO") %>')">编辑</a>
                    <a href="#" onclick="doSubmit('<%#Eval ("NO") %>')">删除</a>
                </td>
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </tbody> </table>
        </FooterTemplate>
    </asp:Repeater>
    <input type="hidden" value="" name="hidNo" />
    </form>
</body>
</html>
