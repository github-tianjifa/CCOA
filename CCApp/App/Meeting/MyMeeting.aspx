﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MyMeeting.aspx.cs" Inherits="CCOA.App.Meeting.MyMeeting"
    EnableViewState="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <script src="../../Js/jquery-1.7.2.min.js" type="text/javascript"></script>
    <link href="../../Js/Js_EasyUI/themes/default/easyui.css" rel="stylesheet" type="text/css" />
    <link href="../../Js/Js_EasyUI/themes/default/datagrid.css" rel="stylesheet" type="text/css" />
    <link href="../../Js/Js_EasyUI/themes/icon.css" rel="stylesheet" type="text/css" />
    <script src="../../Js/Js_EasyUI/jquery.easyui.min.js" type="text/javascript"></script>
    <style type="text/css">
        select
        {
            width: 120px;
            margin-right: 10px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <h4 style="background-image: url( ../../Images/title.png); margin: 0px;">
            会议状态：<select name="state1" id="ddlState1" runat="server">
                <option value="未发起" selected="selected">未发起</option>
                <option value="未开始">未开始</option>
                <option value="正在进行">正在进行</option>
                <option value="未发起">未发起</option>
                <option value="已取消">已取消</option>
                <option value="已结束">已结束</option>
            </select>
            我的会议：<select name="state2" id="ddlState2" runat="server">
                <option value="0" selected="selected">我预定的</option>
                <option value="1">我发起的</option>
                <option value="2">我参与的</option>
            </select>
            <input type="submit" value="查询" />
        </h4>
        <table class="easyui-datagrid">
            <thead>
                <tr>
                    <th data-options="field:'title'">
                        议题
                    </th>
                    <th data-options="field:'compere'">
                        主持人
                    </th>
                    <th data-options="field:'emp'">
                        主办部门
                    </th>
                    <th data-options="field:'time'">
                        时间
                    </th>
                    <th data-options="field:'place'">
                        地点
                    </th>
                    <th data-options="field:'state'">
                        状态
                    </th>
                    <th data-options="field:'summary'">
                        纪要
                    </th>
                </tr>
            </thead>
            <tbody>
                <asp:Repeater ID="Repeater1" runat="server">
                    <ItemTemplate>
                        <tr>
                            <td>
                                <%#DataBinder.Eval(Container.DataItem,"Title")%>
                            </td>
                            <td>
                                <%#DataBinder.Eval(Container.DataItem, "CompereEmpNo")%>
                            </td>
                            <td>
                                <%#DataBinder.Eval(Container.DataItem, "Dept")%>
                            </td>
                            <td>
                                <%#DataBinder.Eval(Container.DataItem, "DateFrom")%>
                                --
                                <%#DataBinder.Eval(Container.DataItem, "DateTo")%>
                            </td>
                            <td>
                                <%#DataBinder.Eval(Container.DataItem, "Room")%>
                            </td>
                            <td>
                                <%#DataBinder.Eval(Container.DataItem, "State")%>
                            </td>
                            <td>
                                <a href='SummaryShow.aspx?oid=<%#Eval("OID") %>'>纪要</a>
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </tbody>
        </table>
    </div>
    </form>
</body>
</html>
