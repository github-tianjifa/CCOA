﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CCOA.App.Meeting
{
    public partial class MeetingType : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //提交过来的，编号不是空的，代表要执行删除方法。
            if (Page.IsPostBack && Request.Form["hidNo"] != null)
            {
                BP.OA.Meeting.MeetingType type = new BP.OA.Meeting.MeetingType();
                type.No = Request.Form["hidNo"];
                type.Delete();
            }
            BP.OA.Meeting.MeetingTypes ens = new BP.OA.Meeting.MeetingTypes();
            ens.RetrieveAll();
            Repeater1.DataSource = ens;
            Repeater1.DataBind();
        }
    }
}