﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CCOA.App.Meeting
{
    public partial class SaveSponsorMeeting : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack)
            {
                int i = BP.OA.UI.PageCommon.Insert(this, new BP.OA.Meeting.SponsorMeeting());
                if (i > 0)
                {
                    Common.Alert("发起成功！", this);
                }
                else
                {
                    Common.Alert("发起失败！", this);
                }
            }
        }
    }
}