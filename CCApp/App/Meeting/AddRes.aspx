﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddRes.aspx.cs" Inherits="CCOA.App.Meeting.AddRes" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../../CSS/table.css" rel="stylesheet" type="text/css" />
    <script src="../../Js/jquery-1.7.2.min.js" type="text/javascript"></script>
    <script src="../../Js/Validform_v5.3.2_min.js" type="text/javascript"></script>
    <style type="text/css">
        .Validform_checktip
        {
            color: Red;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $(".registerform").Validform({
                tiptype: 2,
                showAllError: true
            });
        });
    </script>
</head>
<body style="text-align: center">
    <form id="form1" runat="server" class="registerform">
    <p style="margin-top: 20px">
        资源名称：
        <input type="text" datatype="s2-50" errormsg="【资源名称至少6个字符,最多50个字符！】" nullmsg="【资源名称不能为空！】"
            name="ResName" id="ResName" runat="server" />
        <span style="color: Red">*</span>&nbsp;
        <asp:Button ID="Button1" runat="server" Text="添加" OnClick="Button1_Click" />
    </p>
    <p>
        <span class="Validform_checktip"><span style="color: Red" runat="server" id="span1">
        </span></span>
    </p>
    </form>
</body>
</html>
