﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using BP.En;
using BP.DA;
using BP.OA.Notepaper;

namespace CCOA.App
{
    public partial class NotepaperList : BP.Web.PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DataGridBind(true);
            }
        }

        //绑定列表
        private void DataGridBind(bool start)
        {
            //初始化实例
            Notepapers notepapers = new Notepapers();
            //创建查询对象，并将空实例集合传入查询对象
            QueryObject objInfo = new QueryObject(notepapers);
            //添加查询条件
            objInfo.AddWhere(NotepaperAttr.FK_Emp, BP.Web.WebUser.No);

            if (start)
            {
                //获取符合查询条件总记录数
                this.AspNetPager1.RecordCount = objInfo.GetCount();
            }
            //执行查询符合条件的当前页数据
            objInfo.DoQuery(NotepaperAttr.No, AspNetPager1.PageSize, AspNetPager1.CurrentPageIndex);
            //分页控件显示内容
            this.AspNetPager1.CustomInfoHTML = string.Format("当前第{0}/{1}页 共{2}条记录 每页{3}条", new object[] { this.AspNetPager1.CurrentPageIndex, this.AspNetPager1.PageCount, this.AspNetPager1.RecordCount, this.AspNetPager1.PageSize });
            //绑定数据
            gv_Notepaper.DataSource = notepapers;
            gv_Notepaper.DataBind();
        }

        //翻页事件
        protected void AspNetPager1_PageChanged(object sender, EventArgs e)
        {
            DataGridBind(false);
        }

        //删除处理过程
        protected void Del_Notepaper(object sender, EventArgs e)
        {
            LinkButton lb = (LinkButton)sender;
            string No = lb.CommandName;
            //实例化实体
            Notepaper notepaper = new Notepaper();
            //赋值
            notepaper.No = No;
            //执行删除，成功后刷新Grid
            if (notepaper.Delete() > 0)
                DataGridBind(true);
        }
    }
}