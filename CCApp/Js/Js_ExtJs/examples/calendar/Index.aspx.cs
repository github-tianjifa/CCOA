﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Script.Serialization;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
namespace CCOA.Js_ExtJs.examples.calendar
{
    public partial class Index : System.Web.UI.Page
    {
        public string userCalendars = null;
        protected void Page_Init(object sender, EventArgs e)
        {
            this.GenerateEvents();
        }
        private void GenerateEvents()
        {
            string sql = String.Format("select OID as EventId,CalendarCatagory as CalendarId,Title,StartDate,EndDate,Location,Notes,Url,IsAllDay,Reminder,IsNew from OA_CalendarTask where FK_UserNo='{0}'", BP.Web.WebUser.No);
            DataTable dt = BP.DA.DBAccess.RunSQLReturnTable(sql);
            this.userCalendars = new BuildUserCalendars(dt).GetUserCalendar();
        }
    }
    //***********以下为Json支持类**************************
    public class JsonConvert
    {
        public static T GetJsonObject<T>(string jsonString, bool objectIsSerialized)
        {
            if (objectIsSerialized)
            {
                using (var ms = new MemoryStream(System.Text.Encoding.UTF8.GetBytes(jsonString)))
                { return (T)new DataContractJsonSerializer(typeof(T)).ReadObject(ms); }
            }
            else
            {
                JavaScriptSerializer jss = new JavaScriptSerializer();
                try
                {
                    return jss.Deserialize<T>(jsonString);
                }
                catch
                {
                    return default(T);
                }
            }
        }
        public static string GetJsonString(object jsonObject, bool objectIsSerialized)
        {
            if (objectIsSerialized)
            {
                using (var ms = new MemoryStream())
                {
                    new DataContractJsonSerializer(jsonObject.GetType()).WriteObject(ms, jsonObject);
                    return System.Text.Encoding.UTF8.GetString(ms.ToArray());
                }
            }
            else
            {
                return Newtonsoft.Json.JsonConvert.SerializeObject(jsonObject, Formatting.Indented);
                //JavaScriptSerializer jss = new JavaScriptSerializer();
                //try
                //{
                //    return jss.Serialize(jsonObject);
                //}
                //catch
                //{
                //    return String.Empty;
                //}
            }
        }
    }
    public class CalendarEvent
    {
        public int id { get; set; }
        public int cid { get; set; }
        public String title { get; set; }
        public DateTime start { get; set; }
        public DateTime end { get; set; }
        public String loc { get; set; }
        public String notes { get; set; }
        public String url { get; set; }
        public bool ad { get; set; }
        public String rem { get; set; }
        public bool n { get; set; }
    }
    /// <summary>
    /// 生成用户Calendar类
    /// </summary>
    public class BuildUserCalendars
    {
        private DataTable DTBase = null;
        /// <summary>
        /// 生成菜单类
        /// </summary>
        public BuildUserCalendars(DataTable dtCalendar)
        {
            this.DTBase = dtCalendar;
        }
        public string GetUserCalendar()
        {
            List<CalendarEvent> UserCalendars = this.CreateCalendar();
            return JsonConvert.GetJsonString(UserCalendars, false);
        }
        private List<CalendarEvent> CreateCalendar()
        {
            if (this.DTBase == null) return null;
            List<CalendarEvent> l_ce = new List<CalendarEvent>();
            foreach (DataRow dr in this.DTBase.Rows)
            {
                CalendarEvent umNew = new CalendarEvent()
                {
                    id = Convert.ToInt32(dr["EventId"]),
                    cid = Convert.ToInt32(dr["CalendarId"])
                    ,title = Convert.ToString(dr["Title"])
                    ,start = Convert.ToDateTime(dr["StartDate"])
                    ,end = Convert.ToDateTime(dr["EndDate"])
                    ,loc = Convert.ToString(dr["Location"])
                    ,notes = Convert.ToString(dr["Notes"])
                    ,url = Convert.ToString(dr["Url"])
                    ,ad = Convert.ToInt32(dr["IsAllDay"])==1
                    ,rem = Convert.ToString(dr["Reminder"])
                    ,n = Convert.ToInt32(dr["IsNew"])==1
                };
                l_ce.Add(umNew);
            }
            return l_ce;
        }
    }
}