/**
 * @class Ext.calendar.form.field.ReminderCombo
 * @extends Ext.form.ComboBox
 * <p>A custom combo used for choosing a reminder setting for an event.</p>
 * <p>This is pretty much a standard combo that is simply pre-configured for the options needed by the
 * calendar components. The default configs are as follows:<pre><code>
    width: 200,
    fieldLabel: 'Reminder',
    queryMode: 'local',
    triggerAction: 'all',
    forceSelection: true,
    displayField: 'desc',
    valueField: 'value'
</code></pre>
 * @constructor
 * @param {Object} config The config object
 */
Ext.define('Ext.calendar.form.field.ReminderCombo', {
    extend: 'Ext.form.field.ComboBox',
    alias: 'widget.reminderfield',

    fieldLabel: '提醒',
    queryMode: 'local',
    triggerAction: 'all',
    forceSelection: true,
    displayField: 'desc',
    valueField: 'value',

    // private
    initComponent: function() {
        this.store = this.store || new Ext.data.ArrayStore({
            fields: ['value', 'desc'],
            idIndex: 0,
            data: [
            ['', '未设置'],
            ['0', '时间开始后'],
            ['5', '开始5分钟后'],
            ['15', '开始15分钟后'],
            ['30', '开始30分钟后'],
            ['60', '开始1小时后'],
            ['90', '开始1个半小时后'],
            ['120', '开始2小时后'],
            ['180', '开始3小时后'],
            ['360', '开始6小时后'],
            ['720', '开始12小时后'],
            ['1440', '开始1天后'],
            ['2880', '开始2天后'],
            ['4320', '开始3天后'],
            ['5760', '开始4天后'],
            ['7200', '开始5天后'],
            ['10080', '开始1周后'],
            ['20160', '开始2周后']
            ]
        });

        this.callParent();
    },

    // inherited docs
    initValue: function() {
        if (this.value !== undefined) {
            this.setValue(this.value);
        }
        else {
            this.setValue('');
        }
        this.originalValue = this.getValue();
    }
});
