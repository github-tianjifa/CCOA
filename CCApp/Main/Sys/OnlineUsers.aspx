﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main/master/Site1.Master" ClientIDMode="Static"
    AutoEventWireup="true" CodeBehind="OnlineUsers.aspx.cs" Inherits="CCOA.Main.Sys.OnlineUsers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cph_head" runat="server">
    <link rel="stylesheet" type="text/css" href="/js/js_EasyUI/themes/default/easyui.css">
    <link rel="stylesheet" type="text/css" href="/js/js_EasyUI/themes/icon.css">
    <script src="/js/js_EasyUI/jquery.easyui.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {
            $('#div_SendBox').dialog({ autoOpen: false });
        });
        function OpenDlg(userNo) {
            $('#hfUserNo').val(userNo);
            $('#div_SendBox').dialog('open');
        }
        function LoadData() {
            window.location.reload();
        }
        setInterval('LoadData()', 30000); //定时刷新  30秒



        function SendMsgAndClose() {
            var userNo = $('#hfUserNo').val();
            var text = $('#tbText').val();
            if (userNo == null || userNo == "") {
                alert("用户竟然为空,出错,请管理员!"); return;
            }
            if (text == null || text == "") {
                alert("发送文本不能为空!"); return;
            }
            $.ajax({
                type: "GET",
                url: "/App/Serv/SendMsg.ashx?rand=" + Math.random(),
                dataType: 'html',
                type: 'post',
                data: { "userNo": userNo, "text": text },
                success: function (data) {
                    if (data == "SUCCESS") {
                        $('#tbText').val('');
                        $('#hfUserNo').val('');
                        $('#div_SendBox').dialog('close');
                    }
                    else {
                        alert("不明错误,请查看!");
                    }
                }
            });
        }
        //限定输入字数,多输入部分被截断.
        function checkLength(which) {
            var maxChars = 50;
            if (which.value.length > maxChars) {
                $.messager.alert("提示", "您的输入字数限定为" + maxChars + "字!", "info");
                // 超过限制的字数了就将 文本框中的内容按规定的字数 截取
                which.value = which.value.substring(0, maxChars);
                return false;
            }
            else {
                var curr = maxChars - which.value.length; //50 减去 当前输入的
                document.getElementById("sy").innerHTML = curr.toString();
                return true;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_title" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_body" runat="server">
    <h3>
        在线员工</h3>
    <div style="margin-left: 20px;">
        <asp:GridView ID="GridView1" runat="server" GridLines="Both" CellPadding="3" CellSpacing="0"
            AutoGenerateColumns="false" Width="300">
            <Columns>
                <asp:TemplateField HeaderText="在线用户">
                    <ItemTemplate>
                        <%# this.GetUserNames(Eval("UserNo")) %>
                    </ItemTemplate>
                    <ItemStyle Width="150px" HorizontalAlign="Center" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="登录时间">
                    <ItemTemplate>
                        <span title="<%# String.Format("{0:yyyy年MM月dd日 HH:mm:ss}",Convert.ToDateTime(Eval("LoginTime"))) %>">
                            <%# BP.OA.Main.GetTimeEslapseStr(Convert.ToDateTime(Eval("LoginTime")), null, null)%>
                        </span>
                    </ItemTemplate>
                    <ItemStyle Width="150px" HorizontalAlign="Center" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="发送消息">
                    <ItemTemplate>
                        <input type="button" value="发送" onclick='<%# Eval("UserNo","return OpenDlg(\"{0}\")") %>' />
                    </ItemTemplate>
                    <ItemStyle Width="150px" HorizontalAlign="Center" />
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
        <div id="div_SendBox" class="easyui-dialog" closed="true" title="发送消息" data-options="iconCls:'icon-save'"
            style="width: 400px; height: 188px; padding: 10px; overflow: hidden;">
            <asp:TextBox runat="server" ID="tbText" TextMode="MultiLine" Width="95%" Height="100px"
                onkeyup="checkLength(this)" onkeydown="checkLength(this)"></asp:TextBox>
            限制字数为：50 剩余字数：<span id="sy" style="color: Red;">50</span>
            <input type="button" value="发送" style="margin-left: 130px;" onclick='SendMsgAndClose()' />
            <asp:HiddenField runat="server" ID="hfUserNo" />
        </div>
    </div>
</asp:Content>
