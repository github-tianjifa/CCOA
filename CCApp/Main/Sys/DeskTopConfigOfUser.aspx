﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DeskTopConfigOfUser.aspx.cs"
    Inherits="CCOA.Main.Sys.DeskTopConfigOfUser" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="../../Js/jquery-1.7.2.min.js" type="text/javascript"></script>
    <script src="../../Js/jquery.dragsort-0.5.1.min.js" type="text/javascript"></script>
    <style type="text/css">
        body
        {
            font-family: Arial;
            font-size: 12pt;
            padding: 20px;
            width: 820px;
            margin: 20px auto;
        }
        h1
        {
            font-size: 16pt;
        }
        h2
        {
            font-size: 13pt;
        }
        
        
    </style>
    
</head>
<body>
 
    <!-- save sort order here which can be retrieved on server on postback -->
    
</body>
</html>
