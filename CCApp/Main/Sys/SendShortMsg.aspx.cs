﻿using System;

using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;

namespace CCOA.Main.Sys
{
    public partial class SendShortMsg :BP.Web.WebPage
    {
        #region //1.公共接口(Public Interface)
        //权限控制
        private string FuncNo = null;
        #endregion

        #region //2.页面装载(Page Load)
        protected void Page_Init(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (!BP.OA.GPM.IsCanUseFun(this.FuncNo)) { BP.OA.GPM.RedirectNoAccess(); return; }
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this.LoadData();
            }
        }
        private void LoadData()
        {
        }
        #endregion

        #region //3.页面事件(Page Event)
        protected void btnSend_OnClick(object sender, EventArgs e)
        {
            //0.用户权限
            if (!BP.OA.GPM.IsCanUseFun(this.FuncNo)) { BP.OA.GPM.RedirectNoAccess(); return; }

            //1.获取用户变量
            String curUserNo = BP.Web.WebUser.No;
            string users = this.txt_SendUserNames.Value;
            string msg = this.txt_Text.Text;
            bool isWap = this.cb_IsWap.Checked;

            //2.数据验证
            StringBuilder sbErr = new StringBuilder();
            if (String.IsNullOrEmpty(users))
            {
                sbErr.AppendFormat("{0}\r\n", "必须选择人员！");
            }
            if (String.IsNullOrEmpty(users))
            {
                sbErr.AppendFormat("{0}\r\n", "必须填写内容！");
            }
            if (sbErr.Length > 0)
            {
                this.Alert(sbErr.ToString());
                return;
            }
            //3.数据处理
            bool blnOpe = false;
            int i = BP.OA.ShortMsg.Send_ShortInfo(users, msg, String.Empty,false);
            blnOpe = i > 0;

            //4.统计/日志
            //5.提示处理信息
            if (blnOpe)
            {
                Response.Write("");
                //this.ToCommMsgPage("信息发送成功!!!<hr><a href='/Main/Sys/SendShortMsg.aspx' >新建短消息</a>");

                string js = "<script language=javascript>alert('{0}');window.location.replace('{1}')</script>";
                Response.Write(string.Format(js, "信息发送成功!!!", "/Main/Sys/SendShortMsg.aspx"));
            }
            else
            {
                this.Alert("发送失败！");
            }
        }
        #endregion
    }
}