﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CCOA.Main.Sys
{
    public partial class OnLineUser : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
                this.LoadData();
        }
        private void LoadData()
        {
            if (BP.OA.Main.OnLineUsers != null)
            {
                this.GridView1.DataSource = BP.OA.Main.OnLineUsers;
                this.GridView1.DataBind();
            }
        }
        public string GetUserNames(object userNos)
        {
            String users = String.Format("{0}", userNos);
            return BP.OA.GPM.GetUserNames(users);
        }
        protected void Send(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            string userName = btn.CommandName;
            Response.Write(userName);
        }
    }
}