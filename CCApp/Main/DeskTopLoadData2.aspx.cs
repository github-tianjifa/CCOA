﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CCOA
{
    public partial class DeskTopLoadData2 : System.Web.UI.Page
    {
        public string pageStringShow = String.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            String KeyNo = Request.QueryString["No"];
            CCOA.Main.DeskTopLoadData dataClass = new CCOA.Main.DeskTopLoadData();
            //加载数据
            CCOA.Main.DeskTopLoadData.TempModel model = dataClass.LoadDataByParam(KeyNo);
            //返回页面
            pageStringShow = dataClass.getPageOutPrintString(model);
                        
        }
    }
}