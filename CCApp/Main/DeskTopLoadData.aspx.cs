﻿using BP.OA.Desktop;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CCOA.Main
{
    /// <summary>
    /// 0.图片，轮换图 1.外部链接 2.列表 3.表格式 4.图标
    /// </summary>
    public partial class DeskTopLoadData : System.Web.UI.Page
    {
        public string pageStringShow = String.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            String KeyNo = Request.QueryString["No"];
            //加载数据
            TempModel model = LoadDataByParam(KeyNo);
            //返回页面
            pageStringShow = getPageOutPrintString(model);

            string b = string.Empty;
        }
        
        /// <summary>
        /// 图片，轮换图  
        /// </summary>
        /// <param name="dataTable"></param>
        /// <returns></returns>      
        public String getImgModel(TempModel model)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<div class='yx-rotaion' style='width:100%;'><ul class='rotaion_list' style='height:auto;'>");
	        for (int i = 0; i < model.dataTable.Rows.Count; i++)
            {
                sb.Append("<li><a href='#'><img src='" + model.dataTable.Rows[i]["AttachFile"] + "' alt='" + model.dataTable.Rows[i]["Title"] + "' style='width:100%;height:184px;display:block; " + model.OutHref+ "'></a></li>");            
            }
            sb.Append("</ul></div><script type=\"text/javascript\">$(function () {$(\".yx-rotaion\").yx_rotaion({ auto: true});$(\".groupWrapper\").bind(\"sortstop\", function (event, ui) { var img_height = \"\"; ui.item.find(\".rotaion_list li\").each(function (i) {if ($(this).css(\"display\") == \"list-item\") { img_height = $(this).children().children().height(); ui.item.find(\".yx-rotaion\").css(\"height\", img_height)}});");
            //sb.Append("alert(img_height);");
            //sb.Append("var param = { method: 'updateDeskImgSizeMethod', baseConfig: ui.item.attr(\"id\") + \"#\" + \"min-height:\" + img_height + \"px;\" };");
            //sb.Append("$.post(\"desktop/updateDeskTop.aspx\", param, function (js) {});");
            sb.Append("});});</script>");
            return sb.ToString();
        }

        /// <summary>
        /// 表格式
        /// </summary>
        /// <param name="dataTable"></param>
        /// <returns></returns>
        protected String getDataTableModel(TempModel model)
        {
            string[] strColName = model.ColumnName.Split(',');
            string[] strColNameTitle = model.ColumnDesc.Split(',');
            StringBuilder sb = new StringBuilder();
            //标题行
            sb.Append("<table border='1' width='100%'><tr>");
            foreach (string str in strColNameTitle)
            {
                sb.Append("<th>" + str + "</th>");
            }
            sb.Append("</tr>");

            for (int i = 0; i < model.dataTable.Rows.Count; i++)
            {
                sb.Append("<tr>");
                foreach (string str in strColName) {
                    sb.Append("<td>" + model.dataTable.Rows[i][str] + "</td>");
                }
                sb.Append("</tr>");
            }
            sb.Append("</table>");
            return sb.ToString();
        }

        /// <summary>
        /// 列表
        /// </summary>
        /// <param name="dataTable"></param>
        /// <returns></returns>
        protected String getListModel(TempModel model)
        {            
            StringBuilder sb = new StringBuilder();            
            sb.Append("<table border='1' width='100%'>");  
            for (int i = 0; i < model.dataTable.Rows.Count; i++)
            {
                sb.Append("<tr>");
                foreach (DataColumn str in model.dataTable.Columns)
                {
                    sb.Append("<td>" + model.dataTable.Rows[i][str.ColumnName] + "</td>");
                }
                sb.Append("</tr>");
            }
            sb.Append("</table>");
            return sb.ToString();
        }

        /// <summary>
        /// 外部链接
        /// </summary>
        /// <param name="dataTable"></param>
        /// <returns></returns>
        protected String getOutHref(TempModel model)
        {
            String str = "<iframe frameborder='0' width='100%' scrolling='no' src='" + model.OutHref + "'></iframe>";
            return str;
        }
        
        /// <summary>
        /// 图表
        /// </summary>
        /// <param name="dataTable"></param>
        /// <returns></returns>
        protected String getChart(TempModel model)
        {
            return "";
        }
       
        /// <summary>
        /// 拼接页面返回字符串
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public String getPageOutPrintString(TempModel model) {
            String resultStr = String.Empty;
            if (model.type == "1")
            {
                resultStr = getOutHref(model);
            }
            else 
            {
                if (model.dataTable.Rows.Count > 0)
                {
                    if (model.type == "0")
                    {
                        resultStr = getImgModel(model);
                    }                   
                    else if (model.type == "2")
                    {
                        resultStr = getListModel(model);
                    }
                    else if (model.type == "3")
                    {
                        resultStr = getDataTableModel(model);
                    }
                    else if (model.type == "4")
                    {

                    }
                    else
                    {

                    }            
             }            
            }                        
            return resultStr;
        }

        /// <summary>
        /// 根据NO查询DataTable
        /// </summary>
        /// <param name="KeyNo"></param>
        public TempModel LoadDataByParam(string KeyNo)
        {
            DesktopPanel model = new DesktopPanel(KeyNo);
            StringBuilder sqlStr = new StringBuilder();
            String operSql = String.Empty;
            TempModel returnModel = new TempModel();
            DataTable dt = new DataTable();
            sqlStr.Append("SELECT ");
            if (model != null)
            {
                //sqlStr.Append(model.FK_MyPK==""?"":" "+model.FK_MyPK +"," + model.TitleColumn==""?"": model.TitleColumn+",");

                if (model.PanelType.Equals(BP.OA.Desktop.PanelType.ImgModel))
                {
                    //图片，轮换图                
                    //sqlStr.Append(model.EDT == "" ? "" : model.EDT + "," + model.ImgUrl == "" ? "" : model.ImgUrl);
                    sqlStr.Append(model.ColumnName == "" ? "" : model.ColumnName);
                }
                else if (model.PanelType.Equals(BP.OA.Desktop.PanelType.OutHref))
                {
                    //外部链接                    
                }
                else if (model.PanelType.Equals(BP.OA.Desktop.PanelType.ListModel))
                {
                    //列表
                    //sqlStr.Append(model.ColumnName == "" ? "" : model.ColumnName + "," + model.ExtUrl==""?"":model.ExtUrl);
                    sqlStr.Append(model.ColumnName);
                }
                else if (model.PanelType.Equals(BP.OA.Desktop.PanelType.DataTableModel))
                {
                    //表格式
                    sqlStr.Append(model.ColumnName == "" ? "" : model.ColumnName);
                }
                else if (model.PanelType.Equals(BP.OA.Desktop.PanelType.Chart))
                {
                    //图表                
                }
                else
                {       
         
                }                
                sqlStr.Append(" FROM "+model.PTable);
                sqlStr.Append(model.OrderColumn == "" ? "" : " ORDER BY " + model.OrderColumn);
                sqlStr.Append(Convert.ToString(model.AllowRows) == "" ? " LIMIT 0" : " LIMIT 0," + model.AllowRows);

                if (sqlStr.ToString().Split(',').Length>2)
                {
                    dt = BP.DA.DBAccess.RunSQLReturnTable(sqlStr.ToString());
                }

                returnModel.type = ((int)model.PanelType).ToString();
                returnModel.dataTable = dt;
                returnModel.ColumnDesc = model.ColumnDesc;
                returnModel.ColumnName = model.ColumnName;
                returnModel.OutHref = model.OutHref;
            }
            return returnModel;
        }


        /// <summary>
        /// 返回桌面模块配置信息
        /// </summary>
        /// <returns></returns>
        private Dictionary<string, DesktopPanel> getDeskOADeskTopPanel()
        {            
            BP.OA.Desktop.DesktopPanels panels = new BP.OA.Desktop.DesktopPanels();
            BP.En.QueryObject queryObject = new BP.En.QueryObject(panels);
            queryObject.AddWhere(BP.OA.Desktop.DesktopPanelAttr.IsEnabled,"1");            
            queryObject.DoQuery();

            Dictionary<string, DesktopPanel> dict = new Dictionary<string, DesktopPanel>();

            foreach(BP.OA.Desktop.DesktopPanel item in panels)
            {
                if (!dict.ContainsKey(item.No))
                {
                    dict.Add(item.No, item);
                }
            }
            return dict;
        }
              

        /// <summary>
        /// 工具类
        /// </summary>
        public class TempModel{
            public string type;
            public DataTable dataTable;
            public string ColumnDesc;
            public string ColumnName;
            public string OutHref;
        }
     }
}