﻿ [ {
    "menuid": "1027",
    "menuname": "我的工作",
    "icon": "insert.gif",
    "url": "",
    "menus": [
      {
        "menuid": "1050",
        "menuname": "我的计划",
        "icon": "edit.gif",
        "url": "/Manage/Plan/Plan_MyPlan.aspx",
        "menus": null,
        "child": null
      },
      {
        "menuid": "1103",
        "menuname": "我的申请",
        "icon": "icon2_004.png",
        "url": "/Manage/Work/Work_Apply.aspx",
        "menus": null,
        "child": null
      },
      {
        "menuid": "1049",
        "menuname": "我的流程",
        "icon": "ico_log.gif",
        "url": "/Manage/Work/Work_MyWork.aspx?flag=0 ",
        "menus": null,
        "child": null
      },
      {
        "menuid": "1104",
        "menuname": "我的收支",
        "icon": "mde.gif",
        "url": "",
        "menus": null,
        "child": null
      },
      {
        "menuid": "1105",
        "menuname": "我的装备",
        "icon": "download.gif",
        "url": "",
        "menus": null,
        "child": null
      },
      {
        "menuid": "1106",
        "menuname": "我的客户",
        "icon": "users.png",
        "url": "/Manage/CRM/Crm_My_CustomerList.aspx",
        "menus": null,
        "child": null
      },
      {
        "menuid": "1108",
        "menuname": "我的合同",
        "icon": "other.gif",
        "url": "/Manage/CTR/ContractList.aspx",
        "menus": null,
        "child": null
      },
      {
        "menuid": "1107",
        "menuname": "我的项目",
        "icon": "data4.gif",
        "url": "/Manage/Proj/Proj_ProjectManage.aspx",
        "menus": null,
        "child": null
      },
      {
        "menuid": "1109",
        "menuname": "我的职责",
        "icon": "ico_task.gif",
        "url": "",
        "menus": null,
        "child": null
      },
      {
        "menuid": "1110",
        "menuname": "我的考勤",
        "icon": "edit.gif",
        "url": "/Manage/HR/HR_Signin.aspx",
        "menus": null,
        "child": null
      },
      {
        "menuid": "1111",
        "menuname": "我的档案",
        "icon": "new2.gif",
        "url": "/Manage/Private/Priv_UserInfo.aspx",
        "menus": null,
        "child": null
      },
      {
        "menuid": "1113",
        "menuname": "我的面试",
        "icon": "ico_user.gif",
        "url": "/Manage/HR/HR_NewIntojobs.aspx",
        "menus": null,
        "child": null
      },
      {
        "menuid": "1112",
        "menuname": "修改密码",
        "icon": "mdb.gif",
        "url": "/Manage/Private/Priv_ModiPwd.aspx",
        "menus": null,
        "child": null
      }
    ],
    "child": null
  },
  {
    "menuid": "1114",
    "menuname": "计划管理",
    "icon": "ico_jingpin_insert.gif",
    "url": "/Manage/Plan/Plan_DeptManager.aspx",
    "menus": [
      {
        "menuid": "1117",
        "menuname": "我的计划",
        "icon": "new2.gif",
        "url": "/App/PrivPlan/MyPlan.aspx",
        "menus": null,
        "child": null
      },
      {
        "menuid": "1115",
        "menuname": "部门计划",
        "icon": "edit.gif",
        "url": "/Manage/Plan/Plan_DeptManager.aspx",
        "menus": null,
        "child": null
      },
      {
        "menuid": "1116",
        "menuname": "公司计划",
        "icon": "imageedit.gif",
        "url": "/Manage/Plan/Plan_CmpManager.aspx",
        "menus": null,
        "child": null
      }
    ],
    "child": null
  },
  {
    "menuid": "1121",
    "menuname": "站内消息",
    "icon": "txt.gif",
    "url": "/Manage/WorkOrder/",
    "menus": [
      {
        "menuid": "1124",
        "menuname": "发送消息",
        "icon": "all.gif",
        "url": "/App/Message/SendMsg.aspx",
        "menus": null,
        "child": null
      },
      {
        "menuid": "1123",
        "menuname": "收件箱",
        "icon": "app.gif",
        "url": "/App/Message/InBox.aspx",
        "menus": null,
        "child": null
      },
      {
        "menuid": "1122",
        "menuname": "发件箱",
        "icon": "addnew.gif",
        "url": "/App/Message/SendBox.aspx",
        "menus": null,
        "child": null
      }
    ],
    "child": null
  },
  {
    "menuid": "1101",
    "menuname": "行政管理",
    "icon": "ico_jingpin_insert.gif",
    "url": "/Manage/XZ/",
    "menus": [
      {
        "menuid": "1102",
        "menuname": "公告管理",
        "icon": "ico_bbs.gif",
        "url": "/App/Notice/NoticeList.aspx",
        "menus": null,
        "child": null
      },
      {
        "menuid": "1121",
        "menuname": "添加公告",
        "icon": "ico_bbs.gif",
        "url": "/App/Notice/NewNotice.aspx",
        "menus": null,
        "child": null
      },
      {
        "menuid": "1223",
        "menuname": "我的公告",
        "icon": "ico_bbs.gif",
        "url": "/App/Notice/MyNoticeList.aspx",
        "menus": null,
        "child": null
      }
    ],
    "child": null
  },
  {
    "menuid": "1006",
    "menuname": "系统管理",
    "icon": "ico_jingpin_insert.gif",
    "url": "",
    "menus": [
      {
        "menuid": "1048",
        "menuname": "公司信息",
        "icon": "home1.gif",
        "url": "/Manage/Sys/Dept_MyCompany.aspx",
        "menus": null,
        "child": null
      },
      {
        "menuid": "1034",
        "menuname": "部门设置",
        "icon": "ico_system.gif",
        "url": "/Manage/Sys/Dept_DepartmentList.aspx?CompanyID=11",
        "menus": null,
        "child": null
      },
      {
        "menuid": "1043",
        "menuname": "职务设置",
        "icon": "ico_usergroup.gif",
        "url": "/Manage/Sys/Duty_List.aspx?CompanyID=11",
        "menus": null,
        "child": null
      },
      {
        "menuid": "1078",
        "menuname": "级别管理",
        "icon": "aremove.png",
        "url": "/Manage/HR/HR_Grade.aspx",
        "menus": null,
        "child": null
      },
      {
        "menuid": "1060",
        "menuname": "菜单管理",
        "icon": "all.gif",
        "url": "/Manage/Sys/Menu_List.aspx",
        "menus": null,
        "child": null
      },
      {
        "menuid": "1038",
        "menuname": "功能管理",
        "icon": "ico_settings.gif",
        "url": "/Manage/Sys/Func_ListFunctions.aspx",
        "menus": null,
        "child": null
      },
      {
        "menuid": "1008",
        "menuname": "用户管理",
        "icon": "ico_user.gif",
        "url": "/Manage/sys/User_UserList.aspx?CompanyID=11",
        "menus": null,
        "child": null
      },
      {
        "menuid": "1120",
        "menuname": "系统设置",
        "icon": "db.gif",
        "url": "/Manage/Sys/Set_Cache.aspx",
        "menus": null,
        "child": null
      },
      {
        "menuid": "1087",
        "menuname": "日志查询",
        "icon": "guestbook.gif",
        "url": "/Manage/sys/Logs_Account.aspx",
        "menus": null,
        "child": null
      }
    ],
    "child": null
  }]