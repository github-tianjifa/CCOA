﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DeskTop2.aspx.cs" Inherits="CCOA.Main.DeskTop2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="../../Js/jquery-1.7.2.min.js" type="text/javascript"></script>
    <script src="../../Js/jquery.dragsort-0.5.1.min.js" type="text/javascript"></script>
    <style type="text/css">
        .panel
        {
            background-color: rgb(239, 245, 255);
            font-size: 10pt;
            font-family: Tahoma;
            cursor: default;
            border: 1px solid #79A7E2;
        }
        .panelTitle
        {
            background-color: rgb(239, 245, 255);
            color: black;
            height: 20px;
        }
        .panelTitleT
        {
            font-weight: bold;
            font-size: 12;
            padding-left: 3px;
        }
        .panelFun
        {
            cursor: pointer;
            width: 16px;
        }
        .imgTitle
        {
            width: 16px;
            height: 16px;
        }
        .panelContent
        {
            background-color: white;
            line-height: 14px;
            padding-top: 3px;
            padding-left: 5px;
            padding-right: 5px;
            padding-bottom: 5px;
            overflow: hidden;
        }
        .dragul
        {
            margin: 0px;
            padding: 0px;
            width: 100%;
            margin-left: 20px;
            list-style-type: none;
        }
        .dragul li
        {
            float: left;
            padding: 5px;
            width: 410px; 
            height: 400px;
        }
        .dragdiv
        {
            width: 400px; 
            height: 390px;
            border: solid 1px #79A7E2;
        }
        .placeHolder div
        {
            width: 410px; 
            height: 400px;
            background-color: white !important;
            border: dashed 1px gray !important;
        }
    </style>
    <script type="text/javascript">
        $("ul:first").dragsort();
        function GetVal() {
            var val = $("input[name=list1SortOrder]").val();
            alert(val);
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <ul class="dragul" id="list1">
        <li>
            <div id="new" class="dragdiv">
                <div id="2" class="panelTitle">
                    <span id="idspanxMsg" class="panelTitleT">
                        <img src="Img/icon/ico_home.gif" class="imgTitle" vspace='4' align='middle' alt="" />新闻
                    </span><span class="panelFun" onclick="reFresh(this)">
                        <img src='Img/icon/ico_19.gif' class="imgTitle" align='middle' alt="刷新" />
                    </span><span class="panelFun" onclick="reMore(this)">
                        <img src='Img/icon/ico_89.gif' class="imgTitle" align='middle' alt="更多" />
                    </span>
                </div>
                <div id="idconxMsg11" class="panelContent" style="height: 360px;">
                    <iframe scrolling="auto" frameborder="0" style="width: 100%; height: 100%; overflow: hidden;"
                        src="Desktop.aspx"></iframe>
                </div>
            </div>
        </li>
        <li>
            <div id="Div1" class="dragdiv">
                <div id="Div2" class="panelTitle">
                    <span id="Span1" class="panelTitleT">
                        <img src="Img/icon/ico_home.gif" class="imgTitle" vspace='4' align='middle' alt="" />公告通知
                    </span><span class="panelFun" onclick="reFresh(this)">
                        <img src='Img/icon/ico_19.gif' class="imgTitle" align='middle' alt="刷新" />
                    </span><span class="panelFun" onclick="reMore(this)">
                        <img src='Img/icon/ico_89.gif' class="imgTitle" align='middle' alt="更多" />
                    </span>
                </div>
                <div id="Div3" class="panelContent">
                    内容
                </div>
            </div>
        </li>
    </ul>
    <input name="list1SortOrder" type="hidden" />
    <script type="text/javascript">
        $("#list1, #list2").dragsort({ dragSelector: "div", dragBetween: true, dragEnd: saveOrder, placeHolderTemplate: "<li class='placeHolder'><div></div></li>" });

        function saveOrder() {
            var data = $("#list1 li").map(function () { return $(this).children().attr("id"); }).get();
            $("input[name=list1SortOrder]").val(data.join("|"));
            var val = $("input[name=list1SortOrder]").val();
            alert(val);
        };
    </script>
    </form>
</body>
</html>
