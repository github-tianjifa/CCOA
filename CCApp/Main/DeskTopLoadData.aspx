﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DeskTopLoadData.aspx.cs" Inherits="CCOA.Main.DeskTopLoadData" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>    
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>    
    <style>
        table th {
            padding: 5px 5px 5px 5px;
            border: solid 1px #dddddd;
            background-color: #EEEEEE;
        }
        table td {
            padding: 5px 5px 5px 5px;
            border: solid 1px #dddddd;
        }
    </style>    
</head>
<body>    
    <div>
       <%=pageStringShow %>         
    </div>    
</body>
</html>
