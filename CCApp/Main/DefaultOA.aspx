﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DefaultOA.aspx.cs" Inherits="CCOA.Main.DefaultOA" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title> <%=BP.OA.Soft.SysName %>（<%=UserTitle%>） </title>
    <link href="css/style_dOA.css" rel="stylesheet" type="text/css" />
    <link href="../js/Js_EasyUI/themes/default/easyuiOA.css" rel="stylesheet" type="text/css" />
    <link href="../Js/Js_EasyUI/themes/icon.css" rel="stylesheet" type="text/css" />
    <script src="../js/jquery-1.7.2.min.js" type="text/javascript"></script>
    <script src="../js/Js_EasyUI/jquery.easyui.min.js" type="text/javascript"></script>
    <script src="js/outlook2OA.js" type="text/javascript"></script>
    <script src="js/zDialog/zDialog.js" type="text/javascript"></script>
    <script src="js/QueryString.js" type="text/javascript"></script>
    <style type="text/css">
        .topToolImg
        {
            width:26px;
            height:26px;
            border:0;            
        }
    </style>
    <script type="text/javascript">
        var ShowMessageNum = 0;
        $("document").ready(function () {
            //初始化菜单
            InitLeftMenu();
            //初始化Tab
            $('#tabs').tabs('add', {
                title: '我的工作台',
                //iconCls: 'icon icon-home',
                content: createFrame('<%=DesktopUrl %>')
            });
            var qUrl=GetQueryString("url");
            var qTitle="我的信息";//unescape(GetQueryString("title"));
            if(qUrl!=null&&qTitle!=null)
            {
                $('#tabs').tabs('add', {
                    title: qTitle,
                    //iconCls: 'icon icon-home',
                    content: createFrame(qUrl)
                });
            }
            tabClose();
            tabCloseEven();
            //初始化在线状态
            //setInterval("$('#timebox').html(new Date().toLocaleString())", 500);
            showOnline();
            setInterval(showOnline, 10000);
            //var t = $("#notice_time").val();
            //初始化提醒状态
            showMessage();
            setInterval(showMessage, 10000);
            //添加刷新页面
        });
        var _menus = {
            "menus":<% =MenuList %>
        };
        function hwsx() {
            var diag = new Dialog();
            diag.Width = 520;
            diag.Height = 400;
            diag.Title = "手写板 - 汉王手写输入";
            diag.URL = "/App_Js/sx.htm";
            diag.show();
        }

        function showOnline() {
            $.ajax({
                type: "GET",
                url: "/App/Serv/online.ashx",
                dataType: 'html',
                data: "s=" + Math.random(),
                success: function (data) {
                    if (data == "LOGIN_OUT")//不在线
                    {
                        location.href = "/Main/Login/Login.aspx";
                    }
                    else if(data=="CN_ERROR")//不在线
                    {
                        /*
                        $("#imgUserState").attr("src","img/user_logo.png");
                        $("#span_offline_state").css("display","inline");
                        $("#span_online_state").css("display","none");
                        */
                    }
                    else if (data.length>20) {/*
                        $("#imgUserState").attr("src","img/user_logo.png");
                        $("#span_offline_state").css("display","none");
                        $("#span_online_state").css("display","inline");*/
                        $('#online').html(data);
                    }
                }
            });
        }
        
        function showMessage() {
            var p = $("#stay_time").val();
            $.ajax({
                type: "GET",
                url: "/App/Serv/Message.ashx",
                dataType: 'html',
                data: 's=' + Math.random(),
                success: function (data) {
                    var msg=data;
                    if(msg=="")
                    {
                       ;
                    }
                    else
                    {   //收到消息
                         var arr_data=data.split('|');
                         $.messager.show({
                            title: arr_data[0],
                            msg: arr_data[1],
                            timeout: p,
                            width: 245,
                            height: 130,
                            showType: 'slide'
                         });
                         if (ShowMessageNum % 6 == 0) {
                             //document.getElementById("music").src = "img/msg.wav";
                             play_click('img/msg.wav');
                         }
                         ShowMessageNum++;                    
                     }
                }
              });
         }
        function play_click(url){
            var div = document.getElementById('divSound1');
            div.innerHTML = '<embed src="'+url+'" loop="0" autostart="true" hidden="true"></embed>';
            var emb = document.getElementsByTagName('EMBED')[0];
            if (emb) {
             /* 这里可以写成一个判断 wav 文件是否已加载完毕，以下采用setTimeout模拟一下 */
            div = document.getElementById('divSound2');
            div.innerHTML = 'loading: '+emb.src;
            setTimeout(function(){div.innerHTML='';},1000);
         }
}
    function MouseChangeImg(img,imgUrl,model){
        img.src = imgUrl;
    }
    </script>
</head>
<body class="easyui-layout" style="overflow-y: hidden" fit="true" scroll="no" runat="server">
    <bgsound src="#" id="music" loop="1" autostart="true" />
    <input name="uid" type="hidden" id="uid" value="31" />
    <input name="stay_time" type="hidden" id="stay_time" value="5000" />
    <input name="notice_time" type="hidden" id="notice_time" value="10000" />
    <div id="dxbbs_div">
    </div>
    <div id="divSound1">
    </div>
    <div id="divSound2">
    </div>
    <input type="hidden" id="sxtmp" />
    <noscript>
        <div style="position: absolute; z-index: 100000; height: 2046px; top: 0px; left: 0px;
            width: 100%; background: white; text-align: center;">
            <img src="img/noscript.gif" alt='抱歉，请开启脚本支持！' />
        </div>
    </noscript>
    <div id="loading-mask" style="position: absolute; top: 0px; left: 0px; width: 100%;
        height: 100%; background: #D2E0F2; z-index: 20000">
        <div id="pageloading" style="position: absolute; top: 50%; left: 50%; margin: -120px 0px 0px -120px;
            text-align: center; border: 2px solid #8DB2E3; width: 200px; height: 40px; font-size: 14px;
            padding: 10px; font-weight: bold; background: #fff; color: #15428B;">
            <img alt="" src="img/loading.gif" align="middle" />
            网络加载中,请稍候...
        </div>
    </div>
    <div region="north" split="true" style="vertical-align: middle; overflow: hidden;
        height: 48px; background: url(img/layout-browser-hd-bg.png) no-repeat left; margin:0px; padding:0px;
        line-height: 45px; background-color: #b5e0f0; font-family: Verdana, 微软雅黑,黑体">
        <span id="interface_bt1" style="margin-top:1px;"><a href='?logout=1'>
            <img alt="" class="topToolImg" src="img/KJbutton_out1.png" onmousemove="MouseChangeImg(this,'img/KJbutton_out2.png')" onmouseout="MouseChangeImg(this,'img/KJbutton_out1.png')"  title="安全注销，退出登陆" align="middle" /></a></span>
           <%if (this.IsAdmin){ %>
        <span id="interface_bt2" style="margin-top:1px;"><a href="<%= GPM_GPMPage %>" target="_blank">
            <img alt="" class="topToolImg" src="img/KJbutton_-org1.png" onmousemove="MouseChangeImg(this,'img/KJbutton_-org2.png')" onmouseout="MouseChangeImg(this,'img/KJbutton_-org1.png')" title="组织结构" align="middle" /></a></span>
           <%} %>
        <span id="interface_bt8" style="margin-top:1px;"><a onclick="addTab('系统设置','Sys/DefaultSetting.aspx','icon-calendar')"
            href='#'>
            <img alt="" class="topToolImg" src="img/KJbutton_systemset1.png" onmousemove="MouseChangeImg(this,'img/KJbutton_systemset2.png')" onmouseout="MouseChangeImg(this,'img/KJbutton_systemset1.png')" title="系统设置" align="middle" /></a></span>
        <!--span id="interface_bt9"><a onclick="addTab('即时通讯','','icon-phone')" href='#'>
            <img alt="" src="img/icon/phone.gif" border="0" title="即时通讯" align="middle" /></a></span-->
           <%if (this.IsAdmin){ %>
        <span id="interface_bt3" style="margin-top:1px;"><a href='<%= BPM_BPMPage %>' target="_blank">
            <img alt="" class="topToolImg" src="img/KJbutton_-processset1.png" onmousemove="MouseChangeImg(this,'img/KJbutton_-processset2.png')" onmouseout="MouseChangeImg(this,'img/KJbutton_-processset1.png')"  title="流程设置" align="middle" /></a></span>
            <%} %>
        <span id="interface_bt4" style="margin-top:1px;"><a onclick="addTab('我的消息','/Main/Sys/ShortMsg.aspx','')"
            href='#'>
            <img alt="" class="topToolImg" src="img/KJbutton_news1.png" onmousemove="MouseChangeImg(this,'img/KJbutton_news2.png')" onmouseout="MouseChangeImg(this,'img/KJbutton_news1.png')"  title="我的消息" align="middle" /></a></span>
        <!--span id="interface_bt7"><a onclick="addTab('所有资讯','','icon-paste')" href='#'>
            <img alt="" src="img/news.gif" border="0" title="我的资讯" align="middle" /></a></span-->
        <span id="interface_bt5" style="margin-top:1px;"><a onclick="addTab('我的工作台','DeskTop3.aspx','')"
            href='#'>
          <img alt="" class="topToolImg" src="img/KJbutton_work1.png" onmousemove="MouseChangeImg(this,'img/KJbutton_work2.png')" onmouseout="MouseChangeImg(this,'img/KJbutton_work1.png')"  title="我的工作台" align="middle" /></a></span>
        <span style="padding-left: 10px; padding-top:0px; font-size: 14px; font-weight: bold;">
            <img id="imgUserState" alt="" src="../DataUser/ICON/Comlogo.png" style="height:36px;" align="middle" />
            &nbsp;
            </span>
    </div>
    <div region="south" split="true" style="height: 30px; background: #71BDD0; ">
        <div class="footer">
		<div id="timebox" style='float:right;text-align:right;width:385px;padding-right:20px;'>&nbsp;</div>
		<div id="online" style='float:left;text-align:left;width:160px;padding-left:20px;'><a onclick=addTab('在线用户','Common/User_OnLine.aspx','') href='#'>在线用户：<strong>0</strong> 人</a></div>
		</div>
    </div>
    <div region="west" split="true"  title="<%=UserTitle%> (在线)" style="width:200px;" id="west">
      
      <div id="nav">
		   <!--  导航内容 -->
      </div>
    </div>
    <div id="mainPanle" split="true" border="false" region="center" style="background: #eee; overflow-y:hidden">
        <div id="tabs" class="easyui-tabs"  fit="true"  border="false">
        <!--<div title='隐藏层(勿删)'></div>-->
		</div>
    </div>
	<div id="mm" class="easyui-menu" style="width:150px;">
		<div id="mm-tabupdate">刷新选项卡</div>
		<div class="menu-sep"></div>
		<div id="mm-tabclose">关闭</div>
		<div id="mm-tabcloseall">全部关闭</div>
		<div id="mm-tabcloseother">除此之外全部关闭</div>
		<div class="menu-sep"></div>
		<div id="mm-tabcloseright">当前页右侧全部关闭</div>
		<div id="mm-tabcloseleft">当前页左侧全部关闭</div>
		<div class="menu-sep"></div>
		<div id="mm-exit">退出</div>
	</div>
</body>
</html>
