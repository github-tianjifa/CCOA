﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BP.Web;

namespace CCOA.Login
{
    public partial class Login : System.Web.UI.Page
    {
        #region //1.公共接口(Public Interface)
        private void Alert(string msg)
        {
            BP.OA.Debug.Alert(this, msg);
        }
        /// <summary>
        /// 对外接口，用于改变Session名称，支持WebConfig配置
        /// </summary>
        private string CheckCode_SessionName
        {
            get
            {
                string code = Convert.ToString(ConfigurationManager.AppSettings["CheckCode_SessionName"]);
                if (String.IsNullOrEmpty(code))
                    return "CheckCode";
                else
                    return code;
            }
        }
        /// <summary>
        /// 超级密码，如果没有配置或为空，则不允许使用。
        /// </summary>
        private string Super_Pwd
        {
            get
            {
                return Convert.ToString(ConfigurationManager.AppSettings["Super_Pwd"]);
            }
        }
        /// <summary>
        /// 登录失败后，会产生一个错误标记，有这个标记后必须注册验证码！
        /// </summary>
        public string Login_Error
        {
            get { return Convert.ToString(HttpContext.Current.Session["Login_Error"]); }
        }
        private string MainBoard_Url = "~/Main/Default.aspx";
        private bool ExistsUser(string userNo)
        {
            return BP.OA.Auth.ExistsUser(userNo);
        }
        private bool IsOnline()
        {
            return BP.OA.Auth.IsOnline();
        }
        private void LoginOut()
        {
            BP.OA.Auth.LoginOut();
        }
        private void LoginIn(string userName, bool createPersisterStore)
        {
            BP.OA.Auth.LoginIn(userName, createPersisterStore);
        }
        #endregion

        #region //2.页面装载(Page Load)
        protected void Page_Init(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (BP.DA.DBAccess.IsExitsObject("Port_Emp") == false)
                {
                    this.Response.Redirect("/App/Admin/DBInstall.aspx", true);
                    return;
                }
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Title = String.Format("{0}登录", BP.OA.Soft.SysName);
            //2013.06.08 H
            if (Request.QueryString["DoType"] == "Logout")
            {
                BP.Web.WebUser.Exit();
            }

            string userNo = this.Request.QueryString["UserNo"];
            if (userNo != null && userNo.Length > 1)
            {
                string sid = this.Request.QueryString["SID"];
                if (WebUser.CheckSID(userNo, sid) == true)
                {
                    //消除以往用户住处
                    if (this.IsOnline())
                    {
                        this.LoginOut();
                    }

                    //登录
                    this.LoginIn(userNo, true);
                    BP.Web.WebUser.Token = this.Session.SessionID;
                    BP.Web.WebUser.SetSID(BP.Web.WebUser.Token);

                    //登录到OA主面板
                    Response.Redirect(this.MainBoard_Url, true);
                }
                else
                {
                    this.Response.Write("授权验证失败。");
                }
            }
        }
        #endregion

        #region //3.页面事件(Page Event)
        protected void LoginUser(object sender, EventArgs e)
        {
           
            //获取用户变量
            string userName = this.UserName.Value;
            string userPwd = this.PassWord.Value;
            string code = this.GetCode.Value;
            //验证用户变量
            if (!String.IsNullOrEmpty(this.Login_Error)
                && HttpContext.Current.Session[this.CheckCode_SessionName] != null
                && Convert.ToString(HttpContext.Current.Session[this.CheckCode_SessionName]) != code)
            {
                this.Alert("验证码不对");
                return;
            }
            //验证用户
            bool bSuccess = false;
            BP.Port.Emp emp = new BP.Port.Emp();
            emp.No = userName;
            if (emp.RetrieveFromDBSources() == 0)
                bSuccess = false;
            else if (emp.CheckPass(userPwd))
            {
                bSuccess = true;
            }

            if (bSuccess)
            {
                //消除以往用户住处
                if (this.IsOnline())
                {
                    this.LoginOut();
                }

                //登录
                this.LoginIn(userName, true);
                BP.Web.WebUser.Token = this.Session.SessionID;
                BP.Web.WebUser.SetSID(BP.Web.WebUser.Token);
                //添加日志
                //

                //添加在线用户
                //

                //登录到OA主面板
                Response.Redirect(this.MainBoard_Url, true);
            }
            else
            {
                HttpContext.Current.Session["Login_Error"] = 1;
                this.Alert("用户不存在或密码错误!");
            }
        }
        #endregion
    }
}