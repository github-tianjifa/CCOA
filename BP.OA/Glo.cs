﻿using System;
using System.Collections;
using System.Data;
using BP.Sys;
using BP.DA;
using BP.En;
using BP.Port;

namespace BP.OA
{
    public class Glo
    {
        #region 会议室预定
        /// <summary>
        /// 会议室预定
        /// </summary>
        /// <param name="fk_room">会议室编号</param>
        /// <param name="orderNo">订单编号</param>
        /// <param name="dtFrom">时间从</param>
        /// <param name="dtTo">到</param>
        public static bool Meeting_Order(string fk_room, string orderNo, string meetingNae, string dtFrom, string dtTo, string note)
        {
            //检查是否可以预定.
            //Meeting_IsCanOrder(fk_room, dtFrom, dtTo);

            ////转化日期.
            //DateTime dFrom = DataType.ParseSysDateTime2DateTime(dtFrom);
            //DateTime dTo = DataType.ParseSysDateTime2DateTime(dtTo);

            ////执行预定 , 这里需要根据时间写入多条数据。
            //BP.Meeting.RoomOrding en = new Meeting.RoomOrding();
            //en.Name = meetingNae;
            //en.OrderNo = orderNo;
            //en.DTFrom = dtFrom;
            //en.DTTo = dtTo;
            //en.Doc = note;
            //en.Insert();

            return true;
        }
        /// <summary>
        /// 是否可以预定？
        /// </summary>
        /// <param name="fk_room">会议室编号</param>
        /// <param name="dtFrom">时间从</param>
        /// <param name="dtTo">到</param>
        /// <returns>可以预定就返回true, 否则抛出异常.</returns>
        public static bool Meeting_IsCanOrder(string fk_room, string dtFrom, string dtTo)
        {
            return true;
        }
        /// <summary>
        /// 取消预定
        /// </summary>
        /// <param name="orderNo">订单号</param>
        public static void Meeting_CancelOrder(string orderNo)
        {
            //BP.Meeting.RoomOrding en = new Meeting.RoomOrding();
            //en.Delete(BP.Meeting.RoomOrdingAttr.OrderNo, orderNo);
        }
        #endregion 会议室预定。

        /// <summary>
        /// 安装包
        /// </summary>
        public static void DoInstallDataBase()
        {

            ArrayList al = null;
            string info = "BP.En.Entity";
            al = ClassFactory.GetObjects(info);

            #region 1, 修复表
            foreach (Object obj in al)
            {
                Entity en = null;
                en = obj as Entity;
                if (en == null)
                    continue;

                string table = null;
                try
                {
                    table = en.EnMap.PhysicsTable;
                    if (table == null)
                        continue;
                }
                catch
                {
                    continue;
                }

                switch (table)
                {
                    case "WF_EmpWorks":
                    case "WF_GenerEmpWorkDtls":
                    case "WF_GenerEmpWorks":
                    case "WF_NodeExt":
                    case "V_FlowData":
                        continue;
                    case "Sys_Enum":
                        en.CheckPhysicsTable();
                        break;
                    default:
                        en.CheckPhysicsTable();
                        break;
                }
                en.PKVal = "123";
                try
                {
                    en.RetrieveFromDBSources();
                }
                catch (Exception ex)
                {
                    Log.DebugWriteWarning(ex.Message);
                    en.CheckPhysicsTable();
                }
            }
            //创建视图.
            if (DBAccess.IsExitsObject("Port_EmpDept") == false)
            {
                string sql = "create view Port_EmpDept as select No as FK_Emp, FK_Dept from Port_Emp";
                BP.DA.DBAccess.RunSQL(sql);
            }
            #endregion 修复

            #region 1, 执行基本的 sql
            string sqlscript = SystemConfig.PathOfWebApp + "\\App\\Admin\\SQLScript\\OA1.sql";
            BP.DA.DBAccess.RunSQLScript(sqlscript);
            #endregion 执行基本的

            #region 2, 创建存储过程.
            sqlscript = SystemConfig.PathOfWebApp + "\\App\\Admin\\SQLScript\\OA2.sql";
            BP.DA.DBAccess.RunSQLScriptGo(sqlscript);
            #endregion 创建存储过程

            #region 3, 创建存储过程.
            if (SystemConfig.AppCenterDBType == BP.DA.DBType.Oracle)
            {
                sqlscript = SystemConfig.PathOfWebApp + "\\App\\Admin\\SQLScript\\MySQLOA2.sql";
                BP.DA.DBAccess.RunSQLScriptGo(sqlscript);
            }
            #endregion 创建存储过程

            #region 创建文档管理视图
            BP.DA.DBAccess.RunSQLScriptGo(SystemConfig.PathOfWebApp + "\\App\\Admin\\SQLScript\\V_KM_DocTree.sql");
            #endregion

            #region (徐淑豪写的代码移动到这里.)初始化数据.
            //公告
            BP.OA.Notice notice = new BP.OA.Notice();
            notice.Title = "公司公告";
            notice.FK_NoticeCategory = "04";
            notice.RDT = Convert.ToDateTime(String.Format("{0:yyyy-MM-dd HH:mm:ss}", DateTime.Now));
            notice.FK_UserNo = "zhoupeng";
            notice.Importance = 0;
            notice.SendToPart = true;
            notice.GetAdvices = false;
            notice.SetTop = BP.OA.Main.CurDateTime;
            notice.Doc = " 济南驰骋信息技术有限公司，简称驰骋软件，成立于2003年，以研发工作流引擎ccflow闻名全国。" +
                "驰骋公司作风务实，不做虚假宣传，重合同，守信誉，以技术见长，以为中国提供开源、可靠、稳定、简洁流程引擎为己任。" +
                "公司坚持走开源路线，践行向社会永久开源的诺言。坚持把为中国提供最好工作流程引擎为公司发展主线，坚持体现产品的应用价值为核心目标。" +
                "十多年来，经历成百上千项目的洗礼，驰骋工作流程引擎简洁、可靠、稳定。先后为国内外大中型企业提供优秀的流程技术解决方案，" +
                "先后为国内众多的软件企业、事业、研究院、高校提供了一款优秀的工作流引擎软件，填补了国内在工作流引擎领域开源的空白。";
            notice.NoticeSta = 1;
            notice.Insert();
            //授权查看权限
            DataTable dt_Empss = BP.OA.GPM.GetAllEmps();
            foreach (DataRow dr in dt_Empss.Rows)
            {
                NoticeReader nr = new NoticeReader();
                nr.MyPK = Guid.NewGuid().ToString();
                nr.FK_UserNo = dr["No"].ToString();
                nr.FK_NoticeNo = notice.OID.ToString();
                nr.AddTime = String.Format("{0:yyyy-MM-dd HH:mm:ss}", DateTime.Now);
                nr.Insert();
            }
            //公告第二条
            BP.OA.Notice nnotice = new BP.OA.Notice();
            nnotice.Title = "ccflow能为您解决什么问题？";
            nnotice.FK_NoticeCategory = "04";
            nnotice.RDT = Convert.ToDateTime(String.Format("{0:yyyy-MM-dd HH:mm:ss}", DateTime.Now));
            nnotice.FK_UserNo = "zhoupeng";
            nnotice.Importance = 0;
            nnotice.SendToPart = true;
            nnotice.GetAdvices = false;
            nnotice.SetTop = BP.OA.Main.CurDateTime;
            nnotice.Doc = " 1.ccflow成长与一线用户需求，弱化了专业难懂的概念，面向业务人员推出的符合中国国情了工作流引擎。即使不懂程序开发，只要了解单位业务，就可以设计工作流引擎。所有的设计都是可视化的，所见即所得 <br/>" +
                "2.工作流引擎是一门横向的科学，应用非常广泛，只要有管理活动的地方，就有管理流程，有管理流程，就有ccflow生存的条件。工作流引擎，它与任何管理软件交叉都可以产生新的系统，比如：与税务结合，税收业务流程管理系统，与财务结合形成财务流程管理系统。<br/>" +
                "3.ccflow是用来解决政府机关、企事业单位管理经营活动中的业务作业过程中规范化、合理化的软件管理系统。<br/>" +
                "4.它以多样的终端（计算机、手机、短信、PDA），把单位的内外部的各个部门、人员有机的联系起来,它有效的解决了各个环节中的时间、人力、财物等资源的损耗。<br/>" +
                "5.ccflow与手机+手机短信+短信猫+电子邮件无缝连接，让您的工作第一时间沟通，第一时间处理。<br/>" +
                "6.ccflow提供了强大的数据分析功能：流程运行的各种报表、图形、挖掘、赚取，可以对实（时）效性、成本分析（人力、时间、财物），进行全方位的分析、监控。<br/>";
            nnotice.NoticeSta = 1;
            nnotice.Insert();
            //授权查看权限
            DataTable dt_Empsss = BP.OA.GPM.GetAllEmps();
            foreach (DataRow dr in dt_Empsss.Rows)
            {
                NoticeReader nr = new NoticeReader();
                nr.MyPK = Guid.NewGuid().ToString();
                nr.FK_UserNo = dr["No"].ToString();
                nr.FK_NoticeNo = nnotice.OID.ToString();
                nr.AddTime = String.Format("{0:yyyy-MM-dd HH:mm:ss}", DateTime.Now);
                nr.Insert();
            }
            //公告第三条
            BP.OA.Notice NnoticeE = new BP.OA.Notice();
            NnoticeE.Title = "ccflow产品概述";
            NnoticeE.FK_NoticeCategory = "04";
            NnoticeE.RDT = Convert.ToDateTime(String.Format("{0:yyyy-MM-dd HH:mm:ss}", DateTime.Now));
            NnoticeE.FK_UserNo = "zhoupeng";
            NnoticeE.Importance = 0;
            NnoticeE.SendToPart = true;
            NnoticeE.GetAdvices = false;
            NnoticeE.SetTop = BP.OA.Main.CurDateTime;
            NnoticeE.Doc = "产品名称: 驰骋asp.net工作流引擎. 英文名称: chicheng workflow engine<br/>" +
                           "简 称: ccflow 版 本: 6.0 开发历史: 2003-2015年. 性 质: 开源软件 <br/>" +
                           "许可协议: GPL. URL: http://www.gnu.org/licenses/gpl.html <br/>" +
                           "官方网站: http://ccflow.org <br/>" +
                           "运行环境: bs 结构. windows xp, windows server. IIS6.0以上. .net4.0. Office2007 (如果需要单据打印.)<br/>" +
                           "开发语言: VS2010. .net4.0 c#.net.<br/>" +
                           "客 户 端: FireFox 3.0以上. E 6.0 以上,或者使用IE内核的浏览器.<br/>" +
                           "组成部分: 流程图形设计器(有:cs版本与bs版本)/流程前台运行程序(BS结构)/流程服务(CS)/web的方式的表单设计器/BRP 工具 基本功能: 图形化流程设计/智能表单web定义定义免程序开发/级联下拉框/流程轨迹/单据自定义打印/邮件短信工作到达通 知/自动任务分配/支持sdk模式开发/简洁集成/消息侦听/丰富事件接口/报表定义/工作量分析/绩效考核/手机访问/支持 sqlserve,oracle,mysql,Informix 数据库<br/>";
            NnoticeE.NoticeSta = 1;
            NnoticeE.Insert();
            //授权查看权限
            DataTable dt_EmpssS = BP.OA.GPM.GetAllEmps();
            foreach (DataRow dr in dt_EmpssS.Rows)
            {
                NoticeReader nr = new NoticeReader();
                nr.MyPK = Guid.NewGuid().ToString();
                nr.FK_UserNo = dr["No"].ToString();
                nr.FK_NoticeNo = NnoticeE.OID.ToString();
                nr.AddTime = String.Format("{0:yyyy-MM-dd HH:mm:ss}", DateTime.Now);
                nr.Insert();
            }
            //公告
            BP.OA.Notice nnotiticeeees = new BP.OA.Notice();
            nnotiticeeees.Title = "签约中船信息";
            nnotiticeeees.FK_NoticeCategory = "04";
            nnotiticeeees.RDT = Convert.ToDateTime(String.Format("{0:yyyy-MM-dd HH:mm:ss}", DateTime.Now));
            nnotiticeeees.FK_UserNo = "zhoupeng";
            nnotiticeeees.Importance = 0;
            nnotiticeeees.SendToPart = true;
            nnotiticeeees.GetAdvices = false;
            nnotiticeeees.SetTop = BP.OA.Main.CurDateTime;
            nnotiticeeees.Doc = " 签约日期：2015年4月  行业:GRP<br/>" +
                "开发单位/个人：中船信息<br/>" +
                "代表流程：保密单位流程.<br/>" +
                "客户介绍：";
            nnotiticeeees.NoticeSta = 1;
            nnotiticeeees.Insert();
            //授权查看权限
            DataTable dt_Empssrssss = BP.OA.GPM.GetAllEmps();
            foreach (DataRow dr in dt_Empssrssss.Rows)
            {
                NoticeReader nr = new NoticeReader();
                nr.MyPK = Guid.NewGuid().ToString();
                nr.FK_UserNo = dr["No"].ToString();
                nr.FK_NoticeNo = nnotiticeeees.OID.ToString();
                nr.AddTime = String.Format("{0:yyyy-MM-dd HH:mm:ss}", DateTime.Now);
                nr.Insert();
            }
            //公告
            BP.OA.Notice nnotiticeeee = new BP.OA.Notice();
            nnotiticeeee.Title = "签约中国船舶工业系统工程研究院";
            nnotiticeeee.FK_NoticeCategory = "04";
            nnotiticeeee.RDT = Convert.ToDateTime(String.Format("{0:yyyy-MM-dd HH:mm:ss}", DateTime.Now));
            nnotiticeeee.FK_UserNo = "zhoupeng";
            nnotiticeeee.Importance = 0;
            nnotiticeeee.SendToPart = true;
            nnotiticeeee.GetAdvices = false;
            nnotiticeeee.SetTop = BP.OA.Main.CurDateTime;
            nnotiticeeee.Doc = " 签约日期：2015年6月  行业:GRP<br/>" +
                "开发单位/个人：中国船舶工业系统工程研究院<br/>" +
                "代表流程：保密单位流程.<br/>" +
                "客户介绍：";
            nnotiticeeee.NoticeSta = 1;
            nnotiticeeee.Insert();
            //授权查看权限
            DataTable dt_Empssrsss = BP.OA.GPM.GetAllEmps();
            foreach (DataRow dr in dt_Empssrsss.Rows)
            {
                NoticeReader nr = new NoticeReader();
                nr.MyPK = Guid.NewGuid().ToString();
                nr.FK_UserNo = dr["No"].ToString();
                nr.FK_NoticeNo = nnotiticeeee.OID.ToString();
                nr.AddTime = String.Format("{0:yyyy-MM-dd HH:mm:ss}", DateTime.Now);
                nr.Insert();
            }
            //公告
            BP.OA.Notice nnotiticeee = new BP.OA.Notice();
            nnotiticeee.Title = "签约中国船舶工业系统工程研究院（北船）";
            nnotiticeee.FK_NoticeCategory = "04";
            nnotiticeee.RDT = Convert.ToDateTime(String.Format("{0:yyyy-MM-dd HH:mm:ss}", DateTime.Now));
            nnotiticeee.FK_UserNo = "zhoupeng";
            nnotiticeee.Importance = 0;
            nnotiticeee.SendToPart = true;
            nnotiticeee.GetAdvices = false;
            nnotiticeee.SetTop = BP.OA.Main.CurDateTime;
            nnotiticeee.Doc = " 签约日期：2015年6月  行业:GRP<br/>" +
                "开发单位/个人：中国船舶工业系统工程研究院<br/>" +
                "代表流程：保密单位流程.<br/>" +
                "客户介绍：";
            nnotiticeee.NoticeSta = 1;
            nnotiticeee.Insert();
            //授权查看权限
            DataTable dt_Empssrss = BP.OA.GPM.GetAllEmps();
            foreach (DataRow dr in dt_Empssrss.Rows)
            {
                NoticeReader nr = new NoticeReader();
                nr.MyPK = Guid.NewGuid().ToString();
                nr.FK_UserNo = dr["No"].ToString();
                nr.FK_NoticeNo = nnotiticeee.OID.ToString();
                nr.AddTime = String.Format("{0:yyyy-MM-dd HH:mm:ss}", DateTime.Now);
                nr.Insert();
            }
            //公告
            BP.OA.Notice nnotiticee = new BP.OA.Notice();
            nnotiticee.Title = "签约北京昌平地税局";
            nnotiticee.FK_NoticeCategory = "04";
            nnotiticee.RDT = Convert.ToDateTime(String.Format("{0:yyyy-MM-dd HH:mm:ss}", DateTime.Now));
            nnotiticee.FK_UserNo = "zhoupeng";
            nnotiticee.Importance = 0;
            nnotiticee.SendToPart = true;
            nnotiticee.GetAdvices = false;
            nnotiticee.SetTop = BP.OA.Main.CurDateTime;
            nnotiticee.Doc = " 签约日期：2015年3月  行业:GRP<br/>" +
                "开发单位/个人：北京昌平地税<br/>" +
                "代表流程：60多条税收业务流程.<br/>" +
                "客户介绍：";
            nnotiticee.NoticeSta = 1;
            nnotiticee.Insert();
            //授权查看权限
            DataTable dt_Empssrs = BP.OA.GPM.GetAllEmps();
            foreach (DataRow dr in dt_Empssrs.Rows)
            {
                NoticeReader nr = new NoticeReader();
                nr.MyPK = Guid.NewGuid().ToString();
                nr.FK_UserNo = dr["No"].ToString();
                nr.FK_NoticeNo = nnotiticee.OID.ToString();
                nr.AddTime = String.Format("{0:yyyy-MM-dd HH:mm:ss}", DateTime.Now);
                nr.Insert();
            }
            //公告
            BP.OA.Notice nnotiicee = new BP.OA.Notice();
            nnotiicee.Title = "签约中国船舶工业系统工程研究院（南船）";
            nnotiicee.FK_NoticeCategory = "04";
            nnotiicee.RDT = Convert.ToDateTime(String.Format("{0:yyyy-MM-dd HH:mm:ss}", DateTime.Now));
            nnotiicee.FK_UserNo = "zhoupeng";
            nnotiicee.Importance = 0;
            nnotiicee.SendToPart = true;
            nnotiicee.GetAdvices = false;
            nnotiicee.SetTop = BP.OA.Main.CurDateTime;
            nnotiicee.Doc = " 签约日期：2015年6月  行业:GRP<br/>" +
                "开发单位/个人：中国船舶工业系统工程研究院<br/>" +
                "代表流程：保密单位流程.<br/>" +
                "客户介绍：";
            nnotiicee.NoticeSta = 1;
            nnotiicee.Insert();
            //授权查看权限
            DataTable dt_Empssr = BP.OA.GPM.GetAllEmps();
            foreach (DataRow dr in dt_Empssr.Rows)
            {
                NoticeReader nr = new NoticeReader();
                nr.MyPK = Guid.NewGuid().ToString();
                nr.FK_UserNo = dr["No"].ToString();
                nr.FK_NoticeNo = nnotiicee.OID.ToString();
                nr.AddTime = String.Format("{0:yyyy-MM-dd HH:mm:ss}", DateTime.Now);
                nr.Insert();
            }
            //公告
            BP.OA.Notice nnoticee = new BP.OA.Notice();
            nnoticee.Title = "山东测绘院2期工程";
            nnoticee.FK_NoticeCategory = "04";
            nnoticee.RDT = Convert.ToDateTime(String.Format("{0:yyyy-MM-dd HH:mm:ss}", DateTime.Now));
            nnoticee.FK_UserNo = "zhoupeng";
            nnoticee.Importance = 0;
            nnoticee.SendToPart = true;
            nnoticee.GetAdvices = false;
            nnoticee.SetTop = BP.OA.Main.CurDateTime;
            nnoticee.Doc = " 签约日期：2015年6月  行业:GRP<br/>" +
                "开发单位/个人：山东测绘院<br/>" +
                "代表流程：国土资源厅,耕地保护业务流程.<br/>" +
                "客户介绍：山东省国土测绘院是山东省国土资源厅直属事业单位。";
            nnoticee.NoticeSta = 1;
            nnoticee.Insert();
            //授权查看权限
            DataTable dt_Empsr = BP.OA.GPM.GetAllEmps();
            foreach (DataRow dr in dt_Empsr.Rows)
            {
                NoticeReader nr = new NoticeReader();
                nr.MyPK = Guid.NewGuid().ToString();
                nr.FK_UserNo = dr["No"].ToString();
                nr.FK_NoticeNo = nnoticee.OID.ToString();
                nr.AddTime = String.Format("{0:yyyy-MM-dd HH:mm:ss}", DateTime.Now);
                nr.Insert();
            }
            //公告
            BP.OA.Notice noticee = new BP.OA.Notice();
            noticee.Title = "公司签约广东华立股份";
            noticee.FK_NoticeCategory = "04";
            noticee.RDT = Convert.ToDateTime(String.Format("{0:yyyy-MM-dd HH:mm:ss}", DateTime.Now));
            noticee.FK_UserNo = "zhoupeng";
            noticee.Importance = 0;
            noticee.SendToPart = true;
            noticee.GetAdvices = false;
            noticee.SetTop = BP.OA.Main.CurDateTime;
            noticee.Doc = " 签约日期：2015年9月 行业：制造业<br/>" +
                "开发单位/个人：广东华立股份<br/>" +
                "代表流程：制造业流程，日常办公流程，集团子公司流程<br/>" +
                "客户介绍：公司成立于1995年，一直专注于装饰复合材料发的研发、生产和销售。公司产品主要包括封边片材、异型线材和装饰面材料三大类别，用于对板式家具、室内装演潢的封边和表面装饰，是行业内产品种类最为齐全的企业之一。";
            noticee.NoticeSta = 1;
            noticee.Insert();
            //授权查看权限
            DataTable dt_Emps = BP.OA.GPM.GetAllEmps();
            foreach (DataRow dr in dt_Emps.Rows)
            {
                NoticeReader nr = new NoticeReader();
                nr.MyPK = Guid.NewGuid().ToString();
                nr.FK_UserNo = dr["No"].ToString();
                nr.FK_NoticeNo = noticee.OID.ToString();
                nr.AddTime = String.Format("{0:yyyy-MM-dd HH:mm:ss}", DateTime.Now);
                nr.Insert();
            }

            //塞入新闻信息
            BP.OA.Article art = new BP.OA.Article();
            art.AddTime = BP.DA.DataType.CurrentDataTime;
            art.FK_UserNo = "zhoupeng";
            art.FK_ArticleCatagory = "06";
            art.Title = "构建改革发展内生动力机制";
            art.KeyWords = "ccbpm";
            art.ArticleSource = "ccflow";
            art.Doc = "中央经济工作会议提出“10个更加注重”“5大政策支柱”“5大任务”等，提出以供给侧结构性改革引领经济发展新常态，展开2016年的“歼灭战”和未来的“持久战”，体现了党和国家在经济治理上的系统创新。";
            //arNew.AttachFile = AttachFile;
            art.Insert();

            //塞入新闻信息
            art = new BP.OA.Article();
            art.AddTime = BP.DA.DataType.CurrentDataTime;
            art.FK_UserNo = "zhoupeng";
            art.FK_ArticleCatagory = "06";
            art.Title = "人民日报民生三问：去库存 房价能降多少";
            art.KeyWords = "ccbpm";
            art.ArticleSource = "ccflow";
            art.Doc = "一问" +
                       "农民工买房对去库存有何效果" +
                       "理论上10%的农民工市民化就能消化库存，关键要看政府的支持力度<br/>" +
                       "二问" +
                     "降低商品房价格，政府能做啥" +
                       "降房价有可能实现，政府应采取激励政策，并压缩税费、交易成本 " +
                       "三问" +
                      "让房地产商割肉，他们情愿吗" +
                      "企业主动降价比较少见，兼并重组有利于降价空间的出现 ";

            //arNew.AttachFile = AttachFile;
            art.Insert();

            //塞入新闻信息
            art = new BP.OA.Article();
            art.AddTime = BP.DA.DataType.CurrentDataTime;
            art.FK_UserNo = "zhoupeng";
            art.FK_ArticleCatagory = "06";
            art.Title = "习近平会见梁振英:确保一国两制不走样";
            art.KeyWords = "ccbpm";
            art.ArticleSource = "ccflow";
            art.Doc = "据新华社电 昨天下午，国家主席习近平在中南海瀛台分别会见了来京述职的香港特别行政区行政长官梁振英和澳门特别行政区行政长官崔世安。听取他们对香港、澳门当前形势和特别行政区政府工作情况的汇报，并祝全体香港、澳门市民新年快乐。" +
  "在会见梁振英时，习近平强调，近年来，香港“一国两制”实践出现了一些新情况。我想强调的是，中央贯彻“一国两制”方针坚持两点。一是坚定不移，不会变、不动摇。二是全面准确，确保“一国两制”在香港的实践不走样、不变形，始终沿着正确方向前进。" +
  "习近平指出，当前，谋发展、保稳定、促和谐是香港广大市民的共同愿望，也是特区的主要任务。希望特区政府团结社会各界，稳健施政，维护香港社会政治的稳定；抓住国家发展进入“十三五”时期的机遇，发展经济，改善民生；积极谋划长远发展，为“一国两制”的成功实践和香港的长期繁荣稳定打下坚实基础。" +
  "在会见崔世安时，习近平强调，中央高度重视澳门的繁荣稳定。去年我到澳门出席回归祖国15周年庆典活动时宣布，中央决定启动明确澳门特别行政区习惯水域管理范围相关工作。经过一年努力，这项工作已顺利完成，不仅明确了澳门习惯水域管理范围，还明确了澳门陆地界线。这必将对促进澳门经济社会发展产生积极影响。同时，国家制定“十三五”规划、实施“一带一路”战略也将发挥澳门作用。" +
  "习近平指出，当前，澳门经济受多种因素影响连续下滑，但澳门经济具有较强的抗压能力，社会保持稳定，发展也有许多新的机遇。希望特区政府团结社会各界增强信心，抓住国家发展提供的重大机遇，推动经济适度多元化、可持续发展，不断改善民生；增强忧患意识，保持澳门社会政治稳定；与时俱进，进一步提高施政能力和水平；开拓创新，积极进取，推动“一国两制”事业和澳门各项事业稳步向前发展。" +
  "张德江、李源潮、栗战书、杨洁篪等参加了会见。<br/>";
            //arNew.AttachFile = AttachFile;
            art.Insert();

            //塞入新闻信息
            art = new BP.OA.Article();
            art.AddTime = BP.DA.DataType.CurrentDataTime;
            art.FK_UserNo = "zhoupeng";
            art.FK_ArticleCatagory = "06";
            art.Title = "新版本的驰骋工作流引擎ccbpm将有那些变化？";
            art.KeyWords = "ccbpm";
            art.ArticleSource = "ccflow";
            art.Doc = "各位ccbpm的爱好者：<br/>" +
                       "经过ccbpm队员们几个月的辛苦的努力，ccbpm将有将脱胎换骨的变化，这些变化主要体现在后台上，总结如下几点，敬请各位同学的期待。<br/>" +
                       "1. 我们将要全部放弃sl的表单设计器与流程设计器，全面采用Html5的来实现。<br/> " +
                       "1.1 我们会向下兼容，以保持用于原有的设计还可以兼容。<br/>" +
                       "1.2 两个版本设计器，将会能在支持h5的浏览器上运行，但是前台的最终用户的操作界面仍然兼容IE6+ 以上与一些通用的浏览器<br/>" +
                       "2. 操作设计界面更加顺畅，功能排列与布局会更友好，增加了功能面板，对流程的每个节点设计一目了然。<br/>" +
                       "3. 把节点属性与流程属性独立了出来，做成了功能专题，提高了使用的友好性，降低了使用门槛。<br/>" +
                       "4. 增加很多新功能，比如：业务数据导出、流程模版，表单模版云。<br/>" +
                       "5,组织结构维护与集成更加方便。<br/>" +
                       "6. 增加了社区功能，让ccbpm的开发者，能够互相交流，共享开发成果，可以把自己设计的作品放入表单云，流程云上。<br/>" +
                       "7. 集成CCGPM的权限管理系统。<br/>" +
                       "<br/>" +
                       "<br/>" +
                       "最后，我们澄清一下几个互联网对ccbpm片面的评价与评估两个问题。<br/>" +
                       "第1：我们是一款100%开源的工作流程引擎、表单引擎系统。<br/>" +
                       "第2：我们的文档是健全完善的，合计有15万字，这些文档都是公开、开放的。<br/>" +
                       "第3：我们开源多年，一直奉行人人为我、我为人人的理念，从没有闭源的计划，并且我们在官网做了核心的代码永远开源的承诺。<br/>" +
                       "<br/>" +
                       "<br/>" +
                       "<br/>" +
                       "感谢您支持ccbpm，开源软件的发展。<br/>" +
                       "<br/>" +
                       "<br/>" +
                       "济南驰骋信息技术有限公司 – ccbpm开发团队";
            //arNew.AttachFile = AttachFile;
            art.Insert();

            //塞入新闻2信息
            BP.OA.Article AarNew = new BP.OA.Article();
            AarNew.AddTime = BP.DA.DataType.CurrentDataTime;
            AarNew.FK_UserNo = "zhoupeng";
            AarNew.FK_ArticleCatagory = "06";
            AarNew.Title = "关于济南驰骋信息技术有限公司的.net与java开源工作流引擎";
            AarNew.KeyWords = "ccbpm";
            AarNew.ArticleSource = "ccflow";
            AarNew.Doc = "各位ccbpm的爱好者：<br/>" +
                        "驰骋工作流引擎是济南驰骋信息技术有限公司向社会提供的一款100%开源软件，我们向社会承诺，核心代码100%的开源，多年以来我们践行自己的诺言，努力提高产品质量，奉献社会，成为了国内知名的老牌工作流引擎。<br/>" +
                        "驰骋工作流引擎具有.net与java两个版本，这两个版本代码解构，数据库解构，设计思想，功能组成， 操作手册，完全相同. 导入导出的流程模版，表单模版两个版本完全通用。<br/> " +
                        "驰骋工作流引擎简称cc， 为了区别两个版本的产品.net版本称为ccflow，网站 http://ccflow.org, java版本称为jFlow.网站 http://jflow.cn 。<br/>" +
                        "驰骋工作流引擎包含表单引擎与流程引擎两大部分，并且两块完美结合，协同高效工作. 流程与表单界面可视化的设计， 节点，流程属性达到300多项，可配置程度高，适应于中国国情的多种场景的需要。<br/>" +
                        "驰骋工作流引擎发展与2003年，历经十多年的发展，ccflow在.net的BPM领域，在国内拥有最广泛的研究群体与应用客户群，是大型集团企业IT部门、软件公司、研究院、高校研究与应用的产品。<br/>" +
                        "济南驰骋信息技术公司是国内为数不多的开源软件并且持续盈利的公司之一，.net的BPM领域中，在国内唯一盈利的开源软件。<br/>" +
                        "驰骋工作流引擎不仅仅能够满足中小企业的需要，也能满足通信级用户的应用，先后在西门子、海南航空、陕汽重卡、山东省国土资源厅、华电国际、山东省国税局、江苏测绘院、厦门证券、天津港等国内外大型企业政府单位服役。<br/>" +
                        "ccflow拥有完整的解决方案:ccform表单引擎、ccgpm权限管理系统、ccsso单点登录系统、ccoa驰骋OA、CCIM即时通讯(能够满足20万人同时在线).以上的解决方案除ccim以外都是开源的，是整体的。<br/>" +
                        "ccflow & jflow 方便方便与您的开发框架集成，与第三方组织机构集成. 既有配置类型的开发适用于业务人员，IT维护人员， 也有面向向程序员的高级引擎API开发。<br/>" +

                        "济南驰骋信息技术有限公司 – ccbpm开发团队";
            //arNew.AttachFile = AttachFile;
            AarNew.Insert();


            //邮件信息
            BP.OA.Message.Message msg = new BP.OA.Message.Message();
            msg.AddTime = DateTime.Now;
            msg.Doc = "挺好用的 ";
            msg.FK_UserNo = "zhoupeng";
            msg.MessageState = 1;
            msg.SendToUsers = "admin";
            msg.Title = "我的毕业设计也是这个";
            msg.Insert();
            //发件箱
            BP.OA.Message.SendBox sb = new BP.OA.Message.SendBox();
            sb.FK_MsgNo = msg.OID;
            sb.FK_SendUserNo = "zhoupeng";
            sb.Receiver = "admin";
            sb.SendTime = DateTime.Now;
            sb.Insert();
            //收件箱
            BP.OA.Message.InBox ib = new BP.OA.Message.InBox();
            ib.AddTime = DateTime.Now;
            ib.FK_MsgNo = msg.OID;
            ib.FK_ReceiveUserNo = "admin";
            ib.Sender = BP.Web.WebUser.No;
            ib.Received = false;
            ib.IsCopy = false;  //是否抄送标志
            ib.Insert();

            //邮件信息2
            BP.OA.Message.Message mmsg = new BP.OA.Message.Message();
            mmsg.AddTime = DateTime.Now;
            mmsg.Doc = "过年放假七天 祝同事们玩的开心";
            mmsg.FK_UserNo = "admin";
            mmsg.MessageState = 1;
            mmsg.SendToUsers = "zhoupeng";
            mmsg.Title = "放假通知";
            mmsg.Insert();
            //发件箱
            BP.OA.Message.SendBox ssb = new BP.OA.Message.SendBox();
            ssb.FK_MsgNo = msg.OID;
            ssb.FK_SendUserNo = "admin";
            ssb.Receiver = "zhoupeng";
            ssb.SendTime = DateTime.Now;
            ssb.Insert();
            //收件箱
            BP.OA.Message.InBox iib = new BP.OA.Message.InBox();
            iib.AddTime = DateTime.Now;
            iib.FK_MsgNo = msg.OID;
            iib.FK_ReceiveUserNo = "zhoupeng";
            iib.Sender = "admin";
            iib.Received = false;
            iib.IsCopy = false;  //是否抄送标志
            iib.Insert();

            //邮件信息3
            BP.OA.Message.Message nmmsg = new BP.OA.Message.Message();
            nmmsg.AddTime = DateTime.Now;
            nmmsg.Doc = "欢迎新同事周天娇入职";
            nmmsg.FK_UserNo = "admin";
            nmmsg.MessageState = 1;
            nmmsg.SendToUsers = "zhoupeng";
            nmmsg.Title = "新同事入职";
            nmmsg.Insert();
            //发件箱
            BP.OA.Message.SendBox sssb = new BP.OA.Message.SendBox();
            sssb.FK_MsgNo = msg.OID;
            sssb.FK_SendUserNo = "admin";
            sssb.Receiver = "zhoupeng";
            sssb.SendTime = DateTime.Now;
            sssb.Insert();
            //收件箱
            BP.OA.Message.InBox iibs = new BP.OA.Message.InBox();
            iibs.AddTime = DateTime.Now;
            iibs.FK_MsgNo = msg.OID;
            iibs.FK_ReceiveUserNo = "zhoupeng";
            iibs.Sender = "admin";
            iibs.Received = false;
            iibs.IsCopy = false;  //是否抄送标志
            iibs.Insert();

            //个人通讯录
            ALEmp alemp = new ALEmp();
            alemp.Name = "周天娇";
            alemp.Email = "www.zhoutianjiao@163.com";
            alemp.Tel = "13823483927";
            alemp.faxNo = "";
            alemp.Address = "济南";
            alemp.Birthday = DateTime.Now.ToString();
            alemp.MyHomePage = "www.zhoutianjiao.com";
            alemp.Organization = "党员";
            alemp.Dept = "财务部";
            alemp.PosiTion = " 经理";
            alemp.Role = "经理";
            alemp.Remarks = "爱好旅游";
            alemp.Insert();
            //个人通讯录2
            ALEmp aalemp = new ALEmp();
            aalemp.Name = "张海成";
            aalemp.Email = "www.wangwu@163.com";
            aalemp.Tel = "13704855068";
            aalemp.faxNo = "";
            aalemp.Address = "济南";
            aalemp.Birthday = DateTime.Now.ToString();
            aalemp.MyHomePage = "www.zhczy.com";
            aalemp.Organization = "党员";
            aalemp.Dept = "市场部";
            aalemp.PosiTion = " 经理";
            aalemp.Role = "经理";
            aalemp.Remarks = "喜好打篮球 和乒乓球";
            aalemp.Insert();
            //个人通讯录3
            ALEmp aslemp = new ALEmp();
            aslemp.Name = "周升雨";
            aslemp.Email = "www.zhoushengyu@163.com";
            aslemp.Tel = "15933048762";
            aslemp.faxNo = "";
            aslemp.Address = "济南";
            aslemp.Birthday = DateTime.Now.ToString();
            aslemp.MyHomePage = "www.zshengyu.com";
            aslemp.Organization = "无";
            aslemp.Dept = "南方分公司";
            aslemp.PosiTion = " 组长";
            aslemp.Role = "组长";
            aslemp.Remarks = "爱好看电视剧";
            aslemp.Insert();
            //个人通讯录4
            ALEmp asldemp = new ALEmp();
            asldemp.Name = "郭祥斌";
            asldemp.Email = "guoxiangbin@163.com";
            asldemp.Tel = "15438947592";
            asldemp.faxNo = "232434332";
            asldemp.Address = "济南";
            asldemp.Birthday = DateTime.Now.ToString();
            asldemp.MyHomePage = "www.guoxiangbin.com";
            asldemp.Organization = "共青团员";
            asldemp.Dept = "人力资源部";
            asldemp.PosiTion = " 经理";
            asldemp.Role = "经理";
            asldemp.Remarks = "爱好打篮球";
            asldemp.Insert();
            //发起投票
            BP.OA.Vote.OA_Vote vote = new Vote.OA_Vote();
            vote.Title = "oa主界面样式";
            vote.Item = "高端大气上档次\r\n低调奢华有内涵";
            vote.FK_Emp = "admin";
            vote.VoteType = "0";
            vote.IsAnonymous = "0";
            vote.IsEnable = "1";
            vote.DateFrom = DateTime.Now.ToString();
            vote.DateTo = DateTime.Now.AddDays(4).ToString();
            vote.Insert();

            // 会议管理：会议室，会议室资源数据初始化：放到OA初始数据脚本中 SQL  10.6

            //预定会议
            BP.OA.Meeting.RoomOrding ro = new Meeting.RoomOrding();
            ro.Title = "2015年公司年会";
            ro.Fk_Emp = "周朋";
            ro.Fk_Dept = "100";
            ro.Fk_Room = "001";
            ro.DateFrom = DateTime.Now.ToString();
            ro.DateTo = DateTime.Now.AddDays(4).ToString();
            ro.Note = "请员工准时到场，迟到者罚款50";
            ro.Insert();

            BP.Port.Emps emps = new Emps();
            emps.RetrieveAll();

            //bool isZCLX = false;
            foreach (BP.Port.Emp emp in emps)
            {
                if (emp.No == "admin")
                    continue;
                //增加驾驶员信息
                BP.OA.Car.Driver driver = new Car.Driver();
                driver.Name = emp.Name;
                driver.ZJCX = "A1";
                driver.LZRQ = DataType.CurrentDataTime;
                driver.JZDQR = DateTime.Now.AddDays(7).ToString(DataType.SysDataTimeFormat);
                driver.CCCS = 40;
                driver.WZCS = 1;
                driver.FKZJE = 200;
                driver.Insert();
            }

            int i = 0;
            foreach (BP.Port.Emp emp in emps)
            {
                if (emp.No == "admin")
                    continue;
                i++;

                //指标管理
                BP.OA.Car.ZhiBiao zb = new Car.ZhiBiao();
                zb.Name = "京A3D3" + i;
                zb.YCPH = "京A3D3" + i;
                zb.DJR = emp.Name;
                zb.DJRQ = DateTime.Now.ToString(DataType.SysDataFormat);
                zb.ZBYXRQ = DateTime.Now.ToString(DataType.SysDataFormat);
                zb.ZBDQRQ = DateTime.Now.ToString(DataType.SysDataFormat);
                zb.HDFS = 1;
                zb.ZBZT = 0;
                zb.ZBBH = "001-" + i;
                zb.SSDW = emp.FK_DeptText;
                zb.JBR = emp.FK_Dept;
                zb.BZ = "测试数据初始化.";
                zb.Insert();
            }


            //车辆信息
            BP.OA.Car.CarInfo cif = new Car.CarInfo();
            cif.FK_CPH = "001";
            cif.CX = "轿车";
            cif.CJH = "1G1BL52P7TR115520";
            cif.FDJH = "246764k";
            cif.GCRQ = DateTime.Now.ToString();
            cif.ZYBM = "财务部";
            cif.ZYR = "祁凤林";
            cif.ZRR = "祁凤林";
            cif.Insert();

            //车辆信息
            BP.OA.Car.CarInfo ccif = new Car.CarInfo();
            ccif.FK_CPH = "002";
            ccif.CX = "轿车";
            ccif.CJH = "LSVNN2180D2013488";
            ccif.FDJH = "BJ2020S";
            ccif.GCRQ = DateTime.Now.ToString();
            ccif.ZYBM = "销售部";
            ccif.ZYR = "周天娇";
            ccif.ZRR = "周天娇";
            ccif.Insert();


            //论坛
            BP.OA.BBS.BBSClass bbs = new BBS.BBSClass();
            bbs.Name = "公司杂谈";
            bbs.FK_Emp = "周朋";
            bbs.ReMark = "谈天说地";
            bbs.Insert();
            //论坛
            BP.OA.BBS.BBSClass bbbs = new BBS.BBSClass();
            bbbs.Name = "足球天地";
            bbbs.FK_Emp = "周朋";
            bbbs.ReMark = "看国足 走天下";
            bbbs.Insert();

            i = 0;
            foreach (BP.Port.Emp emp in emps)
            {
                if (emp.No == "admin")
                    continue;
                i++;
                //员工档案
                BP.OA.HR.HRRecord hrr = new HR.HRRecord();
                hrr.Name = emp.Name;
                hrr.InCompanyDT = DataType.CurrentData; 
                hrr.Gender = 0;
                hrr.BirthDT = "1989-03-23";
                hrr.MinZu = "汉族";
                hrr.JiGuan = "山东省济南市天桥区";
                hrr.HuKouAddr = "山东省枣庄市";
                hrr.IDCard = "392929198903233948";
                hrr.JiaRuDT = DataType.CurrentData; 
                hrr.ZhuangTai = "0";
                hrr.FK_Dept = "1062";
                hrr.FK_Duty = "04";
                hrr.CongShiZhuanYe = "软件编程";
                hrr.WenHuaChengDu = "本科";
                hrr.ZhengZhiMianMao = "预备党员";
                hrr.BiYeSchool = "山东大学";
                hrr.SuoXueZhuanYe = "软件编程";
                hrr.BiYeDT = "2012-09-22";
                hrr.WorkDT = "2012-10-1";
                hrr.YuanCompany = "无";
                hrr.Note = "勤奋好学 积极工作 心细严谨";
                hrr.Insert();
            }
            #endregion
        }
    }
}
