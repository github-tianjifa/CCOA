﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BP.En;
using System.Data;

namespace BP.OA.Vote
{
    public class OA_VoteDetailAttr : EntityOIDAttr
    {
        public const string FK_VoteOID = "FK_VoteOID";
        public const string VoteItem = "VoteItem";
        public const string VComment = "VComment";
        public const string IsAnony = "IsAnony";
        public const string FK_Emp = "FK_Emp";
        public const string VoteDate = "VoteDate";
    }
    public class OA_VoteDetail : EntityOID
    {
        public int FK_VoteOID
        {
            get { return this.GetValIntByKey(OA_VoteDetailAttr.FK_VoteOID); }
            set { this.SetValByKey(OA_VoteDetailAttr.FK_VoteOID, value); }
        }
        public string VoteItem
        {
            get { return this.GetValStrByKey(OA_VoteDetailAttr.VoteItem); }
            set { this.SetValByKey(OA_VoteDetailAttr.VoteItem, value); }
        }
        public string VComment
        {
            get { return this.GetValStrByKey(OA_VoteDetailAttr.VComment); }
            set { this.SetValByKey(OA_VoteDetailAttr.VComment, value); }
        }
        public string IsAnony
        {
            get { return this.GetValStrByKey(OA_VoteDetailAttr.IsAnony); }
            set { this.SetValByKey(OA_VoteDetailAttr.IsAnony, value); }
        }
        public string FK_Emp
        {
            get { return this.GetValStrByKey(OA_VoteDetailAttr.FK_Emp); }
            set { this.SetValByKey(OA_VoteDetailAttr.FK_Emp, value); }
        }
        public DateTime VoteDate
        {
            get { return this.GetValDateTime(OA_VoteDetailAttr.VoteDate); }
            set { this.SetValByKey(OA_VoteDetailAttr.VoteDate, value); }
        }
        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                    return this._enMap;
                Map map = new Map();
                map.PhysicsTable = "OA_VoteDetail";
                map.EnDesc = "投票明细";
                map.CodeStruct = "9";
                ////map.DepositaryOfEntity = Depositary.Application; //实体map的存放位置.
                ////map.DepositaryOfMap = Depositary.Application;    // Map 的存放位置.
                map.AddTBIntPKOID();
                map.AddTBInt(OA_VoteDetailAttr.FK_VoteOID, 0, "投票ID", true, false);
                map.AddTBString(OA_VoteDetailAttr.VoteItem, string.Empty, "投票项", true, false, 0, 100, 200);
                map.AddTBString(OA_VoteDetailAttr.VComment, string.Empty, "评论", true, false, 0, 500, 200);
                map.AddDDLSysEnum(OA_VoteDetailAttr.IsAnony, 0, "是否匿名", true, true, OA_VoteDetailAttr.IsAnony, "@0=实名@1=匿名");
                map.AddTBString(OA_VoteDetailAttr.FK_Emp, string.Empty, "投票人", true, false, 0, 10, 100);
                map.AddTBDate(OA_VoteDetailAttr.VoteDate, "投票日期", true, false);
                this._enMap = map;
                return this._enMap;
            }
        }

        public static DataTable GetVoteDetailByOID(string oid)
        {
            OA_VoteDetail detail = new OA_VoteDetail();
            detail.CheckPhysicsTable();
            string strSql = "select a.*,case IsAnony when 1 then '匿名用户' else (select Name from Port_Emp where No= FK_Emp) end as Name from OA_VoteDetail a ";
            strSql += "where FK_VoteOID=" + oid;
            DataTable dt = BP.DA.DBAccess.RunSQLReturnTable(strSql);
            return dt;
        }
        public static bool IsHasVoted(string userNo, string voteId)
        {
            string strSql = "select COUNT(*) from OA_VoteDetail where FK_Emp='{0}' and FK_VoteOID={1}";
            int result = BP.DA.DBAccess.RunSQLReturnValInt(string.Format(strSql, userNo, voteId));
            return result >= 1 ? true : false;
        }

        
    }

    public class OA_VoteDetails : EntitiesOID
    {
        /// <summary>
        /// 得到一个新实体
        /// </summary>
        public override Entity GetNewEntity
        {
            get
            {
                return new OA_VoteDetail();
            }
        }
        /// <summary>
        /// 通知类别集合
        /// </summary>
        public OA_VoteDetails()
        {
        }
    }
}
