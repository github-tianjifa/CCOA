﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BP.En;
using System.Data;
using BP.Port;

namespace BP.OA.Vote
{
    public class OA_VoteAttr : EntityOIDAttr
    {
        public const String Title = "Title";
        public const String Item = "Item";
        public const String FK_Emp = "FK_Emp";
        public const String VoteEmpNo = "VoteEmpNo";
        public const String VoteType = "VoteType";
        public const String IsAnonymous = "IsAnonymous";
        public const String IsEnable = "IsEnable";
        public const String DateFrom = "DateFrom";
        public const String DateTo = "DateTo";
        public const String CreateDate = "CreateDate";
        public const string VoteEmpName = "VoteEmpName";
    }
    /// <summary>
    /// 通知类别
    /// </summary>
    public partial class OA_Vote : EntityOID
    {
        #region 属性
        public string Title
        {
            get { return this.GetValStrByKey(OA_VoteAttr.Title); }
            set { this.SetValByKey(OA_VoteAttr.Title, value); }
        }
        public string Item
        {
            get { return this.GetValStrByKey(OA_VoteAttr.Item); }
            set { this.SetValByKey(OA_VoteAttr.Item, value); }
        }
        public String FK_Emp
        {
            get { return this.GetValStrByKey(OA_VoteAttr.FK_Emp); }
            set { this.SetValByKey(OA_VoteAttr.FK_Emp, value); }
        }
        public string FK_EmpText
        {
            get
            {
                return this.GetValRefTextByKey(OA_VoteAttr.FK_Emp);
            }
        }
        public String VoteEmpNo
        {
            get { return this.GetValStrByKey(OA_VoteAttr.VoteEmpNo); }
            set { this.SetValByKey(OA_VoteAttr.VoteEmpNo, value); }
        }
        public String DateFrom
        {
            get { return this.GetValStrByKey(OA_VoteAttr.DateFrom); }
            set { this.SetValByKey(OA_VoteAttr.DateFrom, value); }
        }
        public String DateTo
        {
            get { return this.GetValStrByKey(OA_VoteAttr.DateTo); }
            set { this.SetValByKey(OA_VoteAttr.DateTo, value); }
        }
        public String CreateDate
        {
            get { return this.GetValStrByKey(OA_VoteAttr.CreateDate); }
            set { this.SetValByKey(OA_VoteAttr.CreateDate, value); }
        }
        public String VoteType
        {
            get { return this.GetValStrByKey(OA_VoteAttr.VoteType); }
            set { this.SetValByKey(OA_VoteAttr.VoteType, value); }
        }

        public String IsAnonymous
        {
            get { return this.GetValStrByKey(OA_VoteAttr.IsAnonymous); }
            set { this.SetValByKey(OA_VoteAttr.IsAnonymous, value); }
        }
        public string IsEnable
        {
            get { return this.GetValStrByKey(OA_VoteAttr.IsEnable); }
            set { this.SetValByKey(OA_VoteAttr.IsEnable, value); }
        }
        public string VoteEmpName
        {
            get { return this.GetValStrByKey(OA_VoteAttr.VoteEmpName); }
            set { this.SetValByKey(OA_VoteAttr.VoteEmpName, value); }
        }
        #endregion

        #region 构造函数
        /// <summary>
        /// 通知类别
        /// </summary>
        public OA_Vote() { }
        /// <summary>
        /// 通知类别
        /// </summary>
        /// <param name="no">编号</param>
        public OA_Vote(int no) : base(no) { }
        #endregion

        #region 重写方法
        public override UAC HisUAC
        {
            get
            {
                UAC uac = new UAC();
                uac.OpenForSysAdmin();
                return uac;
            }
        }
        /// <summary>
        /// Map
        /// </summary>
        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                    return this._enMap;

                Map map = new Map();
                map.PhysicsTable = "OA_Vote";
                map.EnDesc = "投票管理";
                map.CodeStruct = "2";
                ////map.DepositaryOfEntity = Depositary.Application; //实体map的存放位置.
                ////map.DepositaryOfMap = Depositary.Application;    // Map 的存放位置.
                map.AddTBIntPKOID();
                map.AddTBString(OA_VoteAttr.Title, string.Empty, "标题", true, false, 2, 50, 200);
                map.AddTBString(OA_VoteAttr.Item, string.Empty, "投票项", true, false, 2, 500, 500);
                map.AddDDLEntitiesPK(OA_VoteAttr.FK_Emp, string.Empty, "创建人", new Emps(), true);
                map.AddTBString(OA_VoteAttr.VoteEmpNo, string.Empty, "参与人编号", true, false, 0, 4000, 500);
                map.AddDDLSysEnum(OA_VoteAttr.VoteType, 0, "单投多投", true, true, OA_VoteAttr.VoteType, "@0=单投@1=多投");
                map.AddDDLSysEnum(OA_VoteAttr.IsAnonymous, 0, "是否允许匿名", true, true, OA_VoteAttr.IsAnonymous, "@0=不允许@1=允许");
                map.AddDDLSysEnum(OA_VoteAttr.IsEnable, 1, "是否启用", true, true, OA_VoteAttr.IsEnable, "@0=禁用@1=启用");
                map.AddTBDateTime(OA_VoteAttr.DateFrom, "开始时间", true, false);
                map.AddTBDateTime(OA_VoteAttr.DateTo, "结束时间", true, false);
                map.AddTBDateTime(OA_VoteAttr.CreateDate, "创建时间", true, false);
                map.AddTBString(OA_VoteAttr.VoteEmpName, string.Empty, "参与人名称", true, false, 0, 4000, 500);
                this._enMap = map;
                return this._enMap;
            }
        }
        #endregion

        #region 自主开发
        public static DataTable GetVotes()
        {
            OA_Vote vote = new OA_Vote();
            vote.CheckPhysicsTable();

            string strSql = "select a.*,b.Name from OA_Vote a   left join Port_Emp  b on a.FK_Emp=b.No";
            DataTable dt = BP.DA.DBAccess.RunSQLReturnTable(strSql);
            return dt;
        }
        public static DataTable GetVoteByOID(string oid)
        {
            string strSql = "select * from oa_vote  where OID=" + oid;
            DataTable dt = BP.DA.DBAccess.RunSQLReturnTable(strSql);
            return dt;
        }
        public static int ChangeState(string state, string oid)
        {
            //state==1 启用
            int result = 0;
            if (!string.IsNullOrEmpty(state))
            {
                string newState = state == "1" ? "0" : "1";
                string strSql = "update OA_Vote set IsEnable =" + newState + " where OID=" + oid;
                result = BP.DA.DBAccess.RunSQL(strSql);
            }
            return result;
        }
        public static int DeleteVote(string oid)
        {
            int result = 0;
            if (!string.IsNullOrEmpty(oid))
            {
                string strSql1 = "delete from OA_Vote where oid=" + oid;
                string strSql2 = "delete from OA_VoteDetail where FK_VoteOID=" + oid;
                int r1 = BP.DA.DBAccess.RunSQL(strSql1);
                int r2 = BP.DA.DBAccess.RunSQL(strSql2);
                result = r1 + r2;
            }
            return result;
        }
        #endregion
    }
    /// <summary>
    ///得到集合
    /// </summary>
    public class OA_Votes : EntitiesOID
    {
        /// <summary>
        /// 得到一个新实体
        /// </summary>
        public override Entity GetNewEntity
        {
            get
            {
                return new OA_Vote();
            }
        }
        /// <summary>
        /// 通知类别集合
        /// </summary>
        public OA_Votes()
        {
        }
    }
}
