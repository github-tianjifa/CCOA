﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Data;
using BP.DA;
using BP.DTS;
using BP.En;
using BP.Web;
using BP.Sys;
using BP.WF;
using BP.Port;
namespace BP.OA.YJGL
{
    public class SealUseAttr : EntityNoNameAttr
    {

        ///
        ///是否带走
        ///
        public const string IsTack = "IsTack";
        ///
        ///使用部门
        ///
        public const string FK_Dept = "FK_Dept";
        ///
        ///使用人
        ///
        public const string FK_Emp = "FK_Emp";
        ///
        ///开始使用时间
        ///
        public const string BeginDate = "BeginDate";
        ///
        ///使用结束时间
        ///
        public const string EndDate = "EndDate";
        ///
        ///使用时间
        ///
        public const string UseTime = "UseTime";
        ///
        ///事由
        ///
        public const string Note = "Note";

        public const string WorkId = "WorkId";
        /// <summary>
        /// 外键关联印鉴管理
        /// </summary>
        public const string FK_Seal = "FK_Seal";
        /// <summary>
        /// WorkiD
        /// </summary>
        public const string WorkID = "WorkID";

    }
    public class SealUse : EntityOIDName
    {

        #region  属性
        public string FK_Seal
        {
            get
            {
                return this.GetValStringByKey(SealUseAttr.FK_Seal);
            }
            set
            {
                this.SetValByKey(SealUseAttr.FK_Seal, value);
            }
        }
        public string FK_SealText
        {
            get
            {
                return this.GetValRefTextByKey(SealUseAttr.FK_Seal);
            }
        }
        public string FK_Dept
        {
            get
            {
                return this.GetValStringByKey(SealUseAttr.FK_Dept);
            }
            set
            {
                this.SetValByKey(SealUseAttr.FK_Dept, value);
            }
        }

        public Int64 WorkId
        {
            get
            {
                return this.GetValInt64ByKey(SealUseAttr.WorkId);
            }
            set
            {
                this.SetValByKey(SealUseAttr.WorkId, value);
            }
        }
        public string BeginDate
        {
            get
            {
                return this.GetValStringByKey(SealUseAttr.BeginDate);

            }
            set
            {
                this.SetValByKey(SealUseAttr.BeginDate, value);
            }
        }
        public string EndDate
        {
            get
            {
                return this.GetValStringByKey(SealUseAttr.EndDate);

            }
            set
            {
                this.SetValByKey(SealUseAttr.EndDate, value);
            }
        }
        public string FK_Emp
        {
            get
            {
                return this.GetValStringByKey(SealUseAttr.FK_Emp);
            }
            set
            {
                this.SetValByKey(SealUseAttr.FK_Emp, value);
            }
        }
        public string Note
        {
            get
            {
                return this.GetValStringByKey(SealUseAttr.Note);
            }
            set
            {
                this.SetValByKey(SealUseAttr.Note, value);
            }
        }
        public string IsTack
        {
            get
            {
                return this.GetValStringByKey(SealUseAttr.IsTack);
            }
            set
            {
                this.SetValByKey(SealUseAttr.IsTack, value);
            }
        }
        #endregion

        #region 权限控制
        public override UAC HisUAC
        {
            get
            {
                UAC uac = new UAC();
                if (API.Seal_Use_FlowIsEnable)
                {
                    uac.Readonly();
                }
                else
                {
                    /*如果流程启用起来了. */
                    if (BP.Web.WebUser.No == "admin")
                    {
                        uac.IsDelete = true;
                        uac.IsUpdate = true;
                        uac.IsInsert = true;
                    }
                    else
                    {
                        uac.Readonly();
                    }
                }

                return uac;
            }
        }
    
        #endregion 权限控制

        #region 构造方法
        /// <summary>
        /// 印章
        /// </summary>
        public SealUse()
        {
        }
        /// <summary>
        /// 印章
        /// </summary>
        /// <param name="oid"></param>
        public SealUse(int oid) : base(oid) { }
        #endregion 构造方法

        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                    return this._enMap;
                Map map = new Map("OA_SealUse");
                map.EnDesc = "印鉴使用";
                map.CodeStruct = "3";
                map.DepositaryOfEntity = Depositary.Application;
                map.DepositaryOfMap = Depositary.Application;

                map.AddTBIntPKOID();
                //map.AddTBStringPK(SealManagerAttr.No, null, "编号", true, true, 3, 3, 3);
                map.AddTBString(SealUseAttr.Name, null, "", false, true, 0, 100, 30);
                map.AddDDLSysEnum(SealUseAttr.IsTack, 0, "是否带走", true, true, SealUseAttr.IsTack,
                    "@0=否@1=是");
             
                map.AddDDLEntities(SealUseAttr.FK_Seal, null, "印鉴", new Seals(), true);// 外键关联到印鉴管理
                map.AddDDLEntities(SealUseAttr.FK_Dept, null, "使用部门", new Depts(), true);// 外键关联到印鉴管理
                map.AddDDLEntities(SealUseAttr.FK_Emp, null, "使用人", new Emps(), true);// 外键关联到印鉴管理

                map.AddTBDate(SealUseAttr.BeginDate, "使用开始时间", true, false);
                map.AddTBDate(SealUseAttr.EndDate, "使用结束时间", true, false);
               // map.AddTBDecimal(SealUseAttr.UseTime, 0, "使用时间(天)", true, false);

                map.AddTBStringDoc(SealUseAttr.Note, null, "事由", true, false, true);

                map.AddTBInt(SealUseAttr.WorkID, 0, "流程ID", true, true);

                //查询条件
                map.AddSearchAttr(SealUseAttr.FK_Seal);
                map.AddSearchAttr(SealUseAttr.FK_Dept);
                map.AddSearchAttr(SealUseAttr.IsTack);

                RefMethod rm = new RefMethod();
                rm.Title = "流程信息";
                rm.ClassMethodName = this.ToString() + ".DoOpenTrack";
                map.AddRefMethod(rm);
                this._enMap = map;
                return this._enMap;
            }
        }
        /// <summary>
        /// 流程信息
        /// </summary>
        /// <returns></returns>
        public string DoOpenTrack()
        {
            if (this.WorkId == 0)
                return "该使用没有走流程";
            PubClass.WinOpen("/WF/WFRpt.aspx?WorkID=" + this.WorkId + "&FK_Flow=" + API.Seal_Use_FlowMark, 800, 600);
            return null;
        }


        #region 重写方法
        protected override bool beforeInsert()
        {
            return base.beforeInsert();
        }

        protected override bool beforeUpdate()
        {
            return base.beforeUpdate();
        }
        protected override bool beforeUpdateInsertAction()
        {
            this.Name = this.FK_Dept;
            return base.beforeUpdateInsertAction();
        }
        #endregion 重写方法
    }
    public class SealUses : EntitiesOID
    {
        /// <summary>
        /// SealUse
        /// </summary>
        public SealUses() { }
        /// <summary>
        /// 得到它的 Entity 
        /// </summary>
        public override Entity GetNewEntity
        {
            get
            {
                return new SealUse();
            }
        }
    }
}
