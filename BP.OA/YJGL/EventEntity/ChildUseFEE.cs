﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Data;
using BP.DA;
using BP.DTS;
using BP.En;
using BP.Web;
using BP.Sys;
using BP.WF;
using BP.Port;

namespace BP.OA.YJGL.EventEntity
{
    /// <summary>
    /// 车辆年检 流程事件实体
    /// </summary>
    public class ChildUseFEE : BP.WF.FlowEventBase
    {
        #region 构造.
        /// <summary>
        /// 车辆年检 流程事件实体
        /// </summary>
        public ChildUseFEE()
        {
        }
        #endregion 构造.

        #region 重写属性.
        public override string FlowMark
        {
            get { return API.ChildSeal_Use_FlowMark; }
        }
        #endregion 重写属性.

        #region 重写节点运动事件.
        /// <summary>
        /// 删除后
        /// </summary>
        /// <returns></returns>
        public override string AfterFlowDel()
        {
            return null;
        }
        /// <summary>
        /// 删除前
        /// </summary>
        /// <returns></returns>
        public override string BeforeFlowDel()
        {
            return null;
        }
        /// <summary>
        /// 结束后
        /// </summary>
        /// <returns></returns>
        public override string FlowOverAfter()
        {
            //API.Seal_Use(this.GetValInt("UseTime"),
            //    this.GetValStr("UserID"), this.GetValStr("UseReason"),  this.WorkID);
            
            //return "写入成功....";
            return "";
        }
        /// <summary>
        /// 结束前
        /// </summary>
        /// <returns></returns>
        public override string FlowOverBefore()
        {
            return null;
        }
        #endregion 重写事件，完成业务逻辑.
    }
}

