﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BP.OA.YJGL
{
    public class API
    {
        #region 年检 api.
        /// <summary>
        /// 年检流程是否启用?
        /// </summary>
        public static bool Seal_Use_FlowIsEnable
        {
            get
            {
                return BP.Sys.SystemConfig.GetValByKeyBoolen("Seal_Use_FlowIsEnable", false);
            }
        }
        /// <summary>
        /// 年检 流程标记
        /// </summary>
        public static string Seal_Use_FlowMark
        {
            get
            {
                return "SealUse";
            }
        }
        public static string ChildSeal_Use_FlowMark
        {
            get
            {
                return "ChildSealUse";
            }
        }
       
       
        /// <summary>
        /// 印鉴使用API
        /// </summary>
        /// <param name="seal">印鉴名称</param>
        /// <param name="deptNo">使用部门</param>
        /// <param name="userNo">使用人</param>
        /// <param name="bDT">开始使用时间</param>
        /// <param name="endDT">使用结束时间</param>
        /// <param name="note">备注</param>
        /// <param name="workid">WorkID</param>
        /// <returns></returns>
        public static string Seal_Use(string seal, string deptNo, string userNo, string bDT, string endDT, string note, Int64 workid)
        {
            SealUse en = new SealUse();
            try
            {
                en.FK_Seal = seal;
                en.FK_Dept = deptNo;
                en.FK_Emp = userNo;
                en.BeginDate = bDT;
                en.EndDate = endDT;
                en.Note = note;
                en.WorkId = workid;
                en.Insert();

                Seal se = new Seal();
                se.No = seal;
                se.Retrieve();// 从数据库获取对应No的印章信息
                se.OperatorName = userNo;
                se.RegistrationDate = endDT;
                se.Update();// 更新信息
                
                return "执行印鉴使用成功...";
            }
            catch(Exception ex)
            {
                en.Delete();
                return "@印鉴使用失败:"+ex.Message;
            }
        }
        #endregion 采购api.
    }
}
