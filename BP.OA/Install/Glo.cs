﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using BP.Sys;
using BP.DA;
using BP.En;
using BP;
using BP.Port;

namespace BP.OA
{
    public class Glo
    {
        /// <summary>
        /// 安装包
        /// </summary>
        public static void DoInstallDataBase(string lang, string yunXingHuanjing)
        {
            ArrayList al = null;
            string info = "BP.En.Entity";
            al = BP.DA.ClassFactory.GetObjects(info);

            #region 1, 修复表
            foreach (Object obj in al)
            {
                Entity en = null;
                en = obj as Entity;
                if (en == null)
                    continue;
                string table = null;
                try
                {
                    table = en.EnMap.PhysicsTable;
                    if (table == null)
                        continue;
                }
                catch
                {
                    continue;
                }

                switch (table)
                {
                    case "WF_EmpWorks":
                    case "WF_GenerEmpWorkDtls":
                    case "WF_GenerEmpWorks":
                    case "WF_NodeExt":
                        continue;
                    case "Sys_Enum":
                        en.CheckPhysicsTable();
                        break;
                    default:
                        en.CheckPhysicsTable();
                        break;
                }

                en.PKVal = "123";
                try
                {
                    en.RetrieveFromDBSources();
                }
                catch (Exception ex)
                {
                    Log.DebugWriteWarning(ex.Message);
                    en.CheckPhysicsTable();
                }
            }
            #endregion 修复

            #region 2, 注册枚举类型 sql
            // 2, 注册枚举类型。
            BP.Sys.Xml.EnumInfoXmls xmls = new BP.Sys.Xml.EnumInfoXmls();
            xmls.RetrieveAll();
            foreach (BP.Sys.Xml.EnumInfoXml xml in xmls)
            {
                BP.Sys.SysEnums ses = new BP.Sys.SysEnums();
                ses.RegIt(xml.Key, xml.Vals);
            }
            #endregion 注册枚举类型

            #region 3, 执行基本的 sql
            string sqlscript = SystemConfig.PathOfData + "Install\\SQLScript\\Port_" + yunXingHuanjing + "_" + lang + ".sql";
            BP.DA.DBAccess.RunSQLScript(sqlscript);
            #endregion 修复

            #region 5, 初始化数据。
            sqlscript = SystemConfig.PathOfData + "Install\\SQLScript\\InitPublicData.sql";
            BP.DA.DBAccess.RunSQLScriptGo(sqlscript);
            #endregion 初始化数据


            #region 6, create wf view
            sqlscript = SystemConfig.PathOfWebApp + "\\WF\\Data\\Install\\SQLScript\\CreateViewSQL.sql";
            BP.DA.DBAccess.RunSQLScript(sqlscript);
            #endregion 

            
            //#region 7, create ccim view
            //sqlscript = SystemConfig.PathOfWebApp + "\\WF\\Data\\Install\\SQLScript\\CCIM.sql";
            //BP.DA.DBAccess.RunSQLScript(sqlscript);
            //#endregion  
           
        }
    }
}
