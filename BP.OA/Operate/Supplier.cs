﻿using BP.En;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BP.OA.Operate
{
    public class SupplierAttr : EntityNoNameAttr
    {
    }
    public class Supplier : EntityNoName
    {
        public Supplier() { }
        public Supplier(string _No) : base(_No) { }
        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                    return this._enMap;

                Map map = new Map("OA_Supplier");

                map.EnDesc = "供应厂商";
                map.CodeStruct = "3";

                map.DepositaryOfEntity = BP.DA.Depositary.Application;
                map.DepositaryOfMap = BP.DA.Depositary.Application;
                map.AddTBStringPK(SupplierAttr.No,null,"编号",true,true,3,3,3);
                map.AddTBString(SupplierAttr.Name,null,"供应商名称",true,false,0,100,30);
                
                this._enMap = map;
                return this._enMap;    
            }
        }
    }
    public class Suppliers : SimpleNoNames
    {
        public Suppliers() { }
        public override Entity GetNewEntity
        {
            get { return new Supplier(); }
        }
    }
}
