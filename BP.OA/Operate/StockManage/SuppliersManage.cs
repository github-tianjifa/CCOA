﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BP.DA;
using BP.En;
using BP.Port;
using System.Data;

namespace BP.OA.Operate.StockManage
{
   
     public class SuppliersManageAttr : EntityNoNameAttr
    {
        
        /// <summary>
        /// 供应商编号
        /// </summary>
         public const string SuppliersNO = "SuppliersNO";
        /// <summary>
        /// 供应商名称
        /// </summary>
         public const string SuppliersName = "SuppliersName";
        /// <summary>
        /// 公司电话
        /// </summary>
         public const string CompanyPhoneNum = "CompanyPhoneNum";
        /// <summary>
        /// 联系人
        /// </summary>
         public const string Contacts = "Contacts";
        /// <summary>
        /// 联系人电话
        /// </summary>
         public const string ContactsPhoneNum = "ContactsPhoneNum";
        /// <summary>
        /// 传真
        /// </summary>
         public const string Fax = "Fax";
        /// <summary>
        /// E_mail
        /// </summary>
         public const string E_mail = "E_mail";
        ///
        ///邮编
        ///
         public const string ZipCode = "ZipCode";
    }

    public class SuppliersManage : EntityNoName
    {
        #region  属性
        public string SuppliersName
        {
            get {
                return this.GetValStringByKey(SuppliersManageAttr.SuppliersName);
            }
            set{
                this.SetValByKey(SuppliersManageAttr.SuppliersName, value);
            }
        }

        public string Contacts
        {
            get
            {
                return this.GetValStrByKey(SuppliersManageAttr.Contacts);
            }
            set
            {
                this.SetValByKey(SuppliersManageAttr.Contacts, value);
            }
        }
        public string SuppliersNO
        {
            get
            {
                return this.GetValStrByKey(SuppliersManageAttr.SuppliersNO);
            }
            set
            {
                this.SetValByKey(SuppliersManageAttr.SuppliersNO, value);
            }
        }
        #endregion

        #region 权限控制
        #endregion 权限控制

        #region 构造方法
        /// <summary>
        /// 供应商
        /// </summary>
        public SuppliersManage()
        { }
        /// <summary>
        /// 供应商
        /// </summary>
        /// <param name="_No"></param>
        public SuppliersManage(string _No) : base(_No) { }
        #endregion 构造方法

        public override Map EnMap
        {
            get {
                if (this._enMap != null)
                    return this._enMap;
                Map map = new Map("OA_StockSuppliersManage");
                map.EnDesc = "供应商管理";
                map.IsAutoGenerNo = true;
                map.CodeStruct = "3";
                map.DepositaryOfEntity = Depositary.Application;
                map.DepositaryOfMap = Depositary.Application;

                map.AddTBStringPK(SuppliersManageAttr.No, null, "编号", true, true, 3, 3, 3);
                //map.AddTBString(SuppliersManageAttr.Name,null,"",false,true,0,100,30);
                map.AddTBString(SuppliersManageAttr.SuppliersNO,null,"供应商编号",true,false,0,100,30,false);
                map.AddTBString(SuppliersManageAttr.Name, null, "供应商名称", true, false, 0,100, 30, false);
                map.AddTBString(SuppliersManageAttr.CompanyPhoneNum, null, "公司电话", true, false, 0,100, 30, false);
                map.AddTBString(SuppliersManageAttr.Contacts, null, "联系人", true, false, 0,100, 30, false);
                map.AddTBString(SuppliersManageAttr.ContactsPhoneNum, null, "联系人电话", true, false, 0,100, 30, false);
                map.AddTBString(SuppliersManageAttr.Fax, null, "传真", true, false, 0,100, 30, false);
                map.AddTBString(SuppliersManageAttr.E_mail, null, "E_mail", true, false, 0,100, 30, false);
                map.AddTBString(SuppliersManageAttr.ZipCode, null, "邮编", true, false, 0,100, 30, false);

                RefMethod rm = new RefMethod();
                RefMethod rs = new RefMethod();
                rm.Title = "添加评价信息";
                rs.Title = "显示此供应商评价信息";
                rm.ClassMethodName = this.ToString() + ".DoWrite";
                rs.ClassMethodName = this.ToString() + ".DoShow";
                map.AddRefMethod(rm);
                map.AddRefMethod(rs);
                this._enMap = map;
                return this._enMap;
                }
        }
        public string DoWrite()
        {
            BP.PubClass.WinOpen("/Comm/UIEn.aspx?EnsName=BP.OA.Operate.StockManage.SuppliersEvaluations&FK_Suppliers=" + this.No, 500, 600);
            return null;
        }
        public string DoShow()
        {
            BP.PubClass.WinOpen("/Comm/Search.aspx?EnsName=BP.OA.Operate.StockManage.SuppliersEvaluations&FK_Suppliers=" + this.No, 500, 600);
            
            return null;
        }

        #region 重写方法
        protected override bool beforeInsert()
        {
            return base.beforeInsert();
        }

        protected override bool beforeUpdate()
        {
            return base.beforeUpdate();
        }
        protected override bool beforeUpdateInsertAction()
        {
            return base.beforeUpdateInsertAction();
        }
        #endregion 重写方法
    }
    public class SuppliersManages : SimpleNoNames
    {
        /// <summary>
        /// 卡s
        /// </summary>
        public SuppliersManages() { }
        /// <summary>
        /// 得到它的 Entity 
        /// </summary>
        public override Entity GetNewEntity
        {
            get
            {
                return new SuppliersManage();
            }
        }
    }
}
