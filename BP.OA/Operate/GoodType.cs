﻿using BP.En;
using BP.DA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BP.OA.Operate
{
    public class GoodTypeAttr : EntityNoNameAttr
    {
    }
    public class GoodType : EntityNoName
    {
        #region 构造方法
        public GoodType() { }
        public GoodType(string _No) : base(_No) { }
        #endregion

        public override Map EnMap
        {
            get 
            {
                if (this._enMap != null)
                    return this._enMap;

                Map map = new Map("OA_GoodType");
                map.EnDesc = "商品类型";
                map.CodeStruct = "3";

                map.DepositaryOfEntity = Depositary.Application;
                map.DepositaryOfMap = Depositary.Application;

                map.AddTBStringPK(GoodTypeAttr.No, null, "编号", true, true, 3, 3, 3);
                map.AddTBString(GoodTypeAttr.Name, null, "商品类型", true, false, 0, 100, 30);

                this._enMap = map;
                return this._enMap;
            }
        }
    }
    public class GoodTypes : SimpleNoNames
    {
        public GoodTypes() { }

        public override Entity GetNewEntity
        {
            get 
            {
                return new GoodType();
            }
        }

    }
}
