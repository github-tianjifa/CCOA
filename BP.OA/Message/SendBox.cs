﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BP.DA;
using BP.En;
using BP.Web;

namespace BP.OA.Message
{
    /// <summary>
    /// 通知类别属性
    /// </summary>
    public class SendBoxAttr : EntityOIDAttr
    {
        public const String FK_SendUserNo = "FK_SendUserNo";
        public const String Receiver = "Receiver";
        public const String FK_MsgNo = "FK_MsgNo";
        public const String SendTime = "SendTime";
    }
    /// <summary>
    /// 通知类别
    /// </summary>
    public partial class SendBox : EntityOID
    {
        #region 属性
        public string FK_SendUserNo
        {
            get { return this.GetValStrByKey(SendBoxAttr.FK_SendUserNo); }
            set { this.SetValByKey(SendBoxAttr.FK_SendUserNo, value); }
        }
        public string Receiver
        {
            get { return this.GetValStrByKey(SendBoxAttr.Receiver); }
            set { this.SetValByKey(SendBoxAttr.Receiver, value); }
        }
        public int FK_MsgNo
        {
            get { return this.GetValIntByKey(SendBoxAttr.FK_MsgNo); }
            set { this.SetValByKey(SendBoxAttr.FK_MsgNo, value); }
        }
        public DateTime SendTime
        {
            get { return this.GetValDateTime(SendBoxAttr.SendTime); }
            set { this.SetValByKey(SendBoxAttr.SendTime, value); }
        }
        #endregion
        #region 构造函数
        /// <summary>
        /// 通知类别
        /// </summary>
        public SendBox() { }
        /// <summary>
        /// 通知类别
        /// </summary>
        /// <param name="no">编号</param>
        public SendBox(int no) : base(no) { }
        #endregion

        #region 重写方法
        public override UAC HisUAC
        {
            get
            {
                UAC uac = new UAC();
                uac.OpenForSysAdmin();
                return uac;
            }
        }
        /// <summary>
        /// Map
        /// </summary>
        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                    return this._enMap;

                Map map = new Map();
                map.PhysicsTable = "OA_MessageSendBox";
                map.EnDesc = "新闻文章";                   // 实体的描述.
                map.CodeStruct = "2";
                //map.DepositaryOfEntity = Depositary.Application; //实体map的存放位置.
                //map.DepositaryOfMap = Depositary.Application;    // Map 的存放位置.
                map.AddTBIntPKOID();
                map.AddTBString(SendBoxAttr.Receiver, null, "接收者", true, false, 2, 1000, 20);
                map.AddTBString(SendBoxAttr.FK_SendUserNo, null, "发送者", true, false, 2, 50, 20);
                map.AddTBInt(SendBoxAttr.FK_MsgNo, 0, "消息编号", true, false);
                map.AddTBDateTime(SendBoxAttr.SendTime, null, "添加时间", true, false);
                //map.AddTBString(SendBoxAttr.ParentNo, null, "父知识树No", true, false, 0, 100, 30);
                //map.AddTBString(SendBoxAttr.FK_Unit, null, "所在单位", true, false, 0, 50, 30);

                this._enMap = map;
                return this._enMap;
            }
        }
        #endregion
    }
    /// <summary>
    ///得到集合
    /// </summary>
    public class SendBoxs : EntitiesOID
    {
        /// <summary>
        /// 得到一个新实体
        /// </summary>
        public override Entity GetNewEntity
        {
            get
            {
                return new SendBox();
            }
        }
        /// <summary>
        /// 通知类别集合
        /// </summary>
        public SendBoxs()
        {
        }
    }
}
