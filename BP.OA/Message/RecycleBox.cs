﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BP.DA;
using BP.En;
using BP.Web;

namespace BP.OA.Message
{
    /// <summary>
    /// 通知类别属性
    /// </summary>
    public class RecycleBoxAttr : EntityOIDAttr
    {
        public const String FK_MsgNo = "FK_MsgNo";
        public const String FK_UserNo = "FK_UserNo";
        public const String FromType = "FromType";
        public const String Received = "Received";
        public const String ReceivedTime = "ReceivedTime";
        public const String AddTime = "AddTime";
    }
    /// <summary>
    /// 通知类别
    /// </summary>
    public partial class RecycleBox : EntityOID
    {
        #region 属性
        public int FK_MsgNo
        {
            get { return this.GetValIntByKey(RecycleBoxAttr.FK_MsgNo); }
            set { this.SetValByKey(RecycleBoxAttr.FK_MsgNo, value); }
        }
        public String FK_UserNo
        {
            get { return this.GetValStrByKey(RecycleBoxAttr.FK_UserNo); }
            set { this.SetValByKey(RecycleBoxAttr.FK_UserNo, value); }
        }
        public bool Received
        {
            get { return this.GetValBooleanByKey(RecycleBoxAttr.Received); }
            set { this.SetValByKey(RecycleBoxAttr.Received, value); }
        }
        /// <summary>
        /// 0-草稿箱，1-发件箱，2-收件箱
        /// </summary>
        public int FromType
        {
            get { return this.GetValIntByKey(RecycleBoxAttr.FromType); }
            set { this.SetValByKey(RecycleBoxAttr.FromType, value); }
        }        
        public string ReceivedTime
        {
            get { return this.GetValStrByKey(RecycleBoxAttr.ReceivedTime); }
            set { this.GetValStrByKey(RecycleBoxAttr.ReceivedTime, value); }
        }
        public DateTime AddTime
        {
            get { return this.GetValDateTime(RecycleBoxAttr.AddTime); }
            set { this.SetValByKey(RecycleBoxAttr.AddTime, value); }
        }
        #endregion
        #region 构造函数
        /// <summary>
        /// 通知类别
        /// </summary>
        public RecycleBox() { }
        /// <summary>
        /// 通知类别
        /// </summary>
        /// <param name="no">编号</param>
        public RecycleBox(int no) : base(no) { }
        #endregion

        #region 重写方法
        public override UAC HisUAC
        {
            get
            {
                UAC uac = new UAC();
                uac.OpenForSysAdmin();
                return uac;
            }
        }
        /// <summary>
        /// Map
        /// </summary>
        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                    return this._enMap;

                Map map = new Map();
                map.PhysicsTable = "OA_MessageRecycleBox";
                map.EnDesc = "新闻文章";                   // 实体的描述.
                map.CodeStruct = "2";
                //map.DepositaryOfEntity = Depositary.Application; //实体map的存放位置.
                //map.DepositaryOfMap = Depositary.Application;    // Map 的存放位置.
                map.AddTBIntPKOID();
                map.AddTBInt(RecycleBoxAttr.FK_MsgNo, 0, "消息编号", true, false);
                map.AddTBString(RecycleBoxAttr.FK_UserNo, null, "用户编号", true, false, 2, 50, 50);
                map.AddTBDateTime(RecycleBoxAttr.AddTime, null, "添加时间", true, false);
                map.AddTBDateTime(RecycleBoxAttr.ReceivedTime, null, "接收时间", true, false);
                map.AddBoolean(RecycleBoxAttr.Received, false, "是否接收", true, false);
                map.AddTBInt(RecycleBoxAttr.FromType, 0, "删除来源类型", true, false);
                //map.AddTBString(RecycleBoxAttr.ParentNo, null, "父知识树No", true, false, 0, 100, 30);
                //map.AddTBString(RecycleBoxAttr.FK_Unit, null, "所在单位", true, false, 0, 50, 30);

                this._enMap = map;
                return this._enMap;
            }
        }
        #endregion
    }
    /// <summary>
    ///得到集合
    /// </summary>
    public class RecycleBoxs : EntitiesOID
    {
        /// <summary>
        /// 得到一个新实体
        /// </summary>
        public override Entity GetNewEntity
        {
            get
            {
                return new RecycleBox();
            }
        }
        /// <summary>
        /// 通知类别集合
        /// </summary>
        public RecycleBoxs()
        {
        }
    }
}
