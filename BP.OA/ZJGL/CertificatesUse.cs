﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BP.DA;
using BP.En;
using BP.Port;
using System.Data;
using BP.Sys;


namespace BP.OA.ZJGL
{
    public class CertificatesUseAttr : EntityNoNameAttr
    {

        /// <summary>
        /// 从父类继承的Name属性，用于外键显示
        /// </summary>
        public const string Name = "Name";
        ///
        ///是否带走
        ///
        //public const string IsTakeOut = "IsTakeOut";
        ///
        ///使用部门
        ///
        public const string FK_Dept = "FK_Dept";
        ///
        ///使用人
        ///
        public const string FK_Emp = "FK_Emp";
        ///
        ///开始使用时间
        ///
        public const string BeginDate = "BeginDate";
        ///
        ///使用结束时间
        ///
        public const string EndDate = "EndDate";
        ///
        ///使用时间
        ///
        public const string UseTime = "UseTime";
        ///
        ///事由
        ///
        public const string UseReason = "UseReason";
        /// <summary>
        /// 外键关联印鉴管理
        /// </summary>
        /// 
        public const string FK_Certificates = "FK_Certificates";
        public const string Note = "Note";
        public const string CType = "CType";
        public const string WorkID = "WorkID";

    }
    public class CertificatesUse : EntityOIDName
    {
        #region  属性
        public string FK_Certificates
        {
            get
            {
                return this.GetValStringByKey(CertificatesUseAttr.FK_Certificates);
            }
            set
            {
                this.SetValByKey(CertificatesUseAttr.FK_Certificates, value);
            }
        }
        public string FK_CertificatesText
        {
            get
            {
                return this.GetValRefTextByKey(CertificatesUseAttr.FK_Certificates);
            }
        }
        public string FK_Dept
        {
            get
            {
                return this.GetValStringByKey(CertificatesUseAttr.FK_Dept);
            }
            set
            {
                this.SetValByKey(CertificatesUseAttr.FK_Dept, value);
            }
        }
     
        public string BeginDate
        {
            get
            {
                return this.GetValStringByKey(CertificatesUseAttr.BeginDate);

            }
            set
            {
                this.SetValByKey(CertificatesUseAttr.BeginDate, value);
            }
        }
        public string EndDate
        {
            get
            {
                return this.GetValStringByKey(CertificatesUseAttr.EndDate);

            }
            set
            {
                this.SetValByKey(CertificatesUseAttr.EndDate, value);
            }
        }
        public decimal UseTime
        {
            get
            {
                return this.GetValDecimalByKey(CertificatesUseAttr.UseTime);
            }
            set
            {
                this.SetValByKey(CertificatesUseAttr.UseTime, value);
            }
        }
        public string UserID
        {
            get
            {
                return this.GetValStringByKey(CertificatesUseAttr.FK_Emp);
            }
            set
            {
                this.SetValByKey(CertificatesUseAttr.FK_Emp, value);
            }
        }
        public string CType
        {
            get
            {
                return this.GetValStringByKey(CertificatesUseAttr.CType);
            }
            set
            {
                this.SetValByKey(CertificatesUseAttr.CType, value);
            }
        }
        public string UseReason
        {
            get
            {
                return this.GetValStringByKey(CertificatesUseAttr.UseReason);
            }
            set
            {
                this.SetValByKey(CertificatesUseAttr.UseReason, value);
            }
        }
       
        public string Note
        {
            get
            {
                return this.GetValStringByKey(CertificatesUseAttr.Note);

            }
            set
            {
                this.SetValByKey(CertificatesUseAttr.Note, value);
            }
        }
        public Int64 WorkID
        {
            get
            {
                return this.GetValInt64ByKey(CertificatesUseAttr.WorkID);
            }
            set
            {
                this.SetValByKey(CertificatesUseAttr.WorkID, value);
            }
        }
        #endregion
        #region 权限控制
        public override UAC HisUAC
        {
            get
            {
                UAC uac = new UAC();
                if (API.Certificates_Use_FlowIsEnable)
                {
                    uac.Readonly();
                }
                else
                {
                    /*如果流程启用起来了. */
                    if (BP.Web.WebUser.No == "admin")
                    {
                        uac.IsDelete = true;
                        uac.IsUpdate = true;
                        uac.IsInsert = true;
                    }
                    else
                    {
                        uac.Readonly();
                    }
                }

                return uac;
            }
        }
    
        #endregion 权限控制

        #region 构造方法
        /// <summary>
        /// 印章
        /// </summary>
        public CertificatesUse()
        { }
        /// <summary>
        /// 证件
        /// </summary>
        /// <param name="oid"></param>
        public CertificatesUse(int oid) : base(oid) { }
        #endregion 构造方法

        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                    return this._enMap;
                Map map = new Map("OA_CertificatesUse");
                map.EnDesc = "证件使用";
                map.CodeStruct = "3";
                map.DepositaryOfEntity = Depositary.Application;
                map.DepositaryOfMap = Depositary.Application;
                map.AddTBIntPKOID();
                //map.AddTBStringPK(CertificateAttr.No, null, "编号", true, true, 3, 3, 3);
                map.AddTBString(CertificatesUseAttr.Name, null, "", false, true, 0, 100, 30);
                //map.AddDDLSysEnum(CertificatesUseAttr.IsTakeOut, 0, "是否带走", true, true, CertificatesUseAttr.IsTakeOut,
                //    "@0=否@1=是");


                map.AddDDLEntities(CertificatesUseAttr.FK_Certificates, null, "证件名称", new Certificates(), true);// 外键关联到印鉴管理
                map.AddDDLEntities(CertificatesUseAttr.FK_Dept, null, "使用部门", new Depts(), true);// 外键关联到印鉴管理
                map.AddDDLEntities(CertificatesUseAttr.FK_Emp, null, "使用人", new Emps(), true);// 外键关联到印鉴管理
                //map.AddDDLSysEnum(CertificatesUseAttr.CType, 0, "印信类型", true, true, CertificatesUseAttr.CType, "@0=营业执照@2=银行开户许可证@3=财务章@3=税务登记证@4=组织机构代码证@5=银行贷款证 @6=统计登记证@7=@8=财政登记证@9=其他");
                map.AddTBString(CertificatesUseAttr.CType, null, "印信类型", true, false, 0, 100, 30);
                map.AddTBDate(CertificatesUseAttr.BeginDate, "使用开始时间", true, false);
                map.AddTBDate(CertificatesUseAttr.EndDate, "使用结束时间", true, false);
                map.AddTBDecimal(CertificatesUseAttr.UseTime, 0, "使用时间(天)", true, false);
                map.AddTBStringDoc(CertificatesUseAttr.UseReason, null, "事由", true, false, true);
                map.AddTBInt(CertificatesUseAttr.WorkID, 0, "流程ID", true, true);
                //查询条件
                map.AddSearchAttr(CertificatesUseAttr.FK_Certificates);
                map.AddSearchAttr(CertificatesUseAttr.FK_Dept);
                map.AddSearchAttr(CertificatesUseAttr.FK_Emp);

                RefMethod rm = new RefMethod();
                rm.Title = "流程信息";
                rm.ClassMethodName = this.ToString() + ".DoOpenTrack";
                map.AddRefMethod(rm);
                this._enMap = map;
                return this._enMap;
            }
        }

        public string DoOpenTrack()
        {
            if (this.WorkID == 0)
                return "该使用没有走流程";
            PubClass.WinOpen("/WF/WFRpt.aspx?WorkID=" + this.WorkID + "&FK_Flow=" + API.Certificates_Use_FlowIsEnable, 800, 600);
            return null;
        }
        #region 重写方法
        protected override bool beforeInsert()
        {
            return base.beforeInsert();
        }

        protected override bool beforeUpdate()
        {
            return base.beforeUpdate();
        }
        protected override bool beforeUpdateInsertAction()
        {
            this.Name = this.FK_Dept;
            return base.beforeUpdateInsertAction();
        }
        #endregion 重写方法
    }
    public class CertificatesUses : EntitiesOID
    {
        /// <summary>
        /// CertificatesUse
        /// </summary>
        public CertificatesUses() { }
        /// <summary>
        /// 得到它的 Entity 
        /// </summary>
        public override Entity GetNewEntity
        {
            get
            {
                return new CertificatesUse();
            }
        }
    }
}
