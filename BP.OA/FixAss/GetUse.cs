﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BP.DA;
using BP.DTS;
using BP.En;
using BP.Web;
using BP.Sys;
using BP.WF;
using BP.Port;
using System.Data;

namespace BP.FixAss
{
    /// <summary>
    /// 审核状态(枚举)
    /// </summary>
    public enum GetUseZT
    {
        /// <summary>
        /// 审核中
        /// </summary>
        Free,
        /// <summary>
        /// 通过
        /// </summary>
        Using,
        /// <summary>
        /// 未通过
        /// </summary>
        Ording
    }
    /// <summary>
    /// 固定资产领用属性
    /// </summary>
    public class GetUseAttr : EntityOIDAttr
    {
        // <summary>
        /// 资产编号
        /// </summary>
        public const string FixNum = "FixNum";
        /// <summary>
        /// 资产名称
        /// </summary>
        public const string FK_FixMan = "FK_FixMan";
        /// <summary>
        /// 所属大类
        /// </summary>
        public const string FK_FixAssBigCatagory = "FK_FixAssBigCatagory";
        /// <summary>
        /// 所属小类
        /// </summary>
        public const string FK_FixAssBigCatSon = "FK_FixAssBigCatSon";
        /// <summary>
        /// 规格
        /// </summary>
        public const string ZipSpec = "ZipSpec";
        /// <summary>
        /// 领用人
        /// </summary>
        public const string WhoGetUse = "WhoGetUse";
        /// <summary>
        /// 领用部门
        /// </summary>
        public const string GetUseDept = "GetUseDept";
        /// <summary>
        /// 领用日期
        /// </summary>
        public const string GetUseDate = "GetUseDate";
        /// <summary>
        /// 审批状态
        /// </summary>
        public const string GetUseZT = "GetUseZT";
        /// <summary>
        /// 业务编号
        /// </summary>
        public const string WorkID = "WorkID";
    }
    /// <summary>
    /// 固定资产领用台帐
    /// </summary>
    public class GetUse : EntityOIDName
    {
        #region 属性
        /// <summary>
        /// 资产编号
        /// </summary>
        public string FixNum
        {
            get
            {
                return this.GetValStringByKey(GetUseAttr.FixNum);
            }
            set
            {
                this.SetValByKey(GetUseAttr.FixNum, value);
            }
        }
        /// <summary>
        /// 物品名
        /// </summary>
        public string FK_FixMan
        {
            get
            {
                return this.GetValStringByKey(GetUseAttr.FK_FixMan);
            }
            set
            {
                this.SetValByKey(GetUseAttr.FK_FixMan, value);
            }
        }
        /// <summary>
        /// 所属大类
        /// </summary>
        public string FK_FixAssBigCatagory
        {
            get
            {
                return this.GetValStringByKey(GetUseAttr.FK_FixAssBigCatagory);
            }
            set
            {
                this.SetValByKey(GetUseAttr.FK_FixAssBigCatagory, value);
            }
        }
        /// <summary>
        /// 所属小类
        /// </summary>
        public string FK_FixAssBigCatSon
        {
            get
            {
                return this.GetValStringByKey(GetUseAttr.FK_FixAssBigCatSon);
            }
            set
            {
                this.SetValByKey(GetUseAttr.FK_FixAssBigCatSon, value);
            }
        }
        /// <summary>
        /// 规格型号
        /// </summary>
        public string ZipSpec
        {
            get
            {
                return this.GetValStringByKey(GetUseAttr.ZipSpec);
            }
            set
            {
                this.SetValByKey(GetUseAttr.ZipSpec, value);
            }
        }
        /// <summary>
        /// 申请人
        /// </summary>
        public string WhoGetUse
        {
            get
            {
                return this.GetValStringByKey(GetUseAttr.WhoGetUse);
            }
            set
            {
                this.SetValByKey(GetUseAttr.WhoGetUse, value);
            }
        }
        /// <summary>
        /// 隶属部门
        /// </summary>
        public string GetUseDept
        {
            get
            {
                return this.GetValStringByKey(GetUseAttr.GetUseDept);
            }
            set
            {
                this.SetValByKey(GetUseAttr.GetUseDept, value);
            }
        }
        /// <summary>
        /// 日期
        /// </summary>
        public string GetUseDate
        {
            get
            {
                return this.GetValStringByKey(GetUseAttr.GetUseDate);
            }
            set
            {
                this.SetValByKey(GetUseAttr.GetUseDate, value);
            }
        }
        /// <summary>
        /// 审核状态
        /// </summary>
        public string GetUseZT
        {
            get
            {
                return this.GetValStringByKey(GetUseAttr.GetUseZT);
            }
            set
            {
                this.SetValByKey(GetUseAttr.GetUseZT, value);
            }
        }
        /// <summary>
        /// 业务编号
        /// </summary>
        public string WorkID
        {
            get
            {
                return this.GetValStringByKey(GetUseAttr.WorkID);
            }
            set
            {
                this.SetValByKey(GetUseAttr.WorkID, value);
            }
        }

        #endregion

        #region 构造函数
        public GetUse() { }
        public GetUse(int oid) : base(oid) { }
        #endregion

        #region 重写方法
        public override UAC HisUAC
        {
            get
            {
                UAC uac = new UAC();
                uac.IsDelete = false;
                uac.IsInsert = false;
                uac.IsUpdate = false;
                uac.IsView = true;
                return uac;
            }
        }
        /// <summary>
        /// Map
        /// </summary>
        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                    return this._enMap;

                Map map = new Map();
                map.PhysicsTable = "OA_FixGetUse";
                map.EnDesc = "固定资产领用台帐";
                map.CodeStruct = "5";
                map.IsAutoGenerNo = true;

                map.AddTBIntPKOID();
                
                map.AddTBString(GetUseAttr.FixNum, null, "资产编号", true, true, 0, 100, 50);
                map.AddDDLEntities(GetUseAttr.FK_FixMan, null, "物品名", new FixMans(), true);
                map.AddDDLEntities(GetUseAttr.FK_FixAssBigCatagory, null, "所属大类", new FixAssBigCatagorys(),true);
                map.AddDDLEntities(GetUseAttr.FK_FixAssBigCatSon, null, "所属小类", new FixAssBigCatSons(), true);
                map.AddTBString(GetUseAttr.ZipSpec, null, "规格型号", true, true, 1, 100, 50);
                map.AddTBString(GetUseAttr.WhoGetUse, "", "申请人", true, false, 1, 100, 50);
                map.AddTBString(GetUseAttr.GetUseDept, "", "部门", true, false, 1, 100, 50);
                map.AddTBDate(GetUseAttr.GetUseDate, "申请日期", true, false);
                map.AddDDLSysEnum(GetUseAttr.GetUseZT, 0, "状态", true, true, GetUseAttr.GetUseZT,
                  "@0=审核中@1=通过@2=未通过");
                map.AddTBInt(GetUseAttr.WorkID, 0, "业务编号", false, true);

                map.AddSearchAttr(GetUseAttr.FK_FixMan);

                this._enMap = map;
                return this._enMap;
            }
        }
        #endregion

        protected override bool beforeUpdateInsertAction()
        {
            if (this.GetUseDate == "")
                this.GetUseDate = DataType.CurrentData;

            FixMan ds = new FixMan(this.FK_FixMan);
            //复制主表的属性.
            this.FixNum = ds.No;
            this.ZipSpec = ds.ZipSpec;
            this.FK_FixAssBigCatagory = ds.FK_FixAssBigCatagory;
            this.FK_FixAssBigCatSon = ds.FK_FixAssBigCatSon;
            return base.beforeUpdateInsertAction();
        }
        protected override bool beforeInsert()
        {
            return base.beforeInsert();
        }
        protected override void afterInsert()
        {
            base.afterInsert();
        }
    }
    /// <summary>
    ///固定资产领用台帐s
    /// </summary>
    public class GetUses : EntitiesOIDName
    {
        public GetUses() { }
        public override Entity GetNewEntity
        {
            get
            {
                return new GetUse();
            }
        }
    }
}
