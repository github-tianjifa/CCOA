using System;
using System.Collections;
using System.Data;
using BP.DA;
using BP.DTS;
using BP.En;
using BP.Web;
using BP.Sys;
using BP.WF;
using BP.Port;

namespace BP.FixAss
{
    /// <summary>
    /// 领用流程 - 开始节点.
    /// </summary>
    public class FixManGetUseND1401NEE : BP.WF.FlowEventBase
    {
        #region 构造.
        /// <summary>
        /// 领用流程事件
        /// </summary>
        public FixManGetUseND1401NEE()
        {
        }
        #endregion 属性.

        #region 重写属性.
        /// <summary>
        /// 流程标记
        /// </summary>
        public override string FlowMark
        {
            get { return BP.FixAss.API.FixMan_GetUse_FlowMark; }
        }
        
        #endregion 重写属性.

        #region 重写节点表单事件.
        /// <summary>
        /// 表单载入前
        /// </summary>
        public override string FrmLoadAfter()
        {
            return null;
        }
        /// <summary>
        /// 表单载入后
        /// </summary>
        public override string FrmLoadBefore()
        {
            return null;
        }
        /// <summary>
        /// 表单保存后
        /// </summary>
        public override string SaveAfter()
        {
            return null;
        }
        /// <summary>
        /// 表单保存前
        /// </summary>
        public override string SaveBefore()
        {
            return null;
        }
        #endregion 重写节点表单事件

        #region 重写节点运动事件.
        /// <summary>
        /// 发送前:用于检查业务逻辑是否可以执行发送，不能执行发送就抛出异常.
        /// </summary>
        public override string SendWhen()
        {
            if (this.HisNode.NodeID == 1401)
            {
                //限制多次申请同一物品
                int GetNameInd = 0;
                string[] GetName = new string[10];

                string sql = "select * from ND1401Dtl1 where  RefPK=" + this.WorkID;

                DataTable dt = DBAccess.RunSQLReturnTable(sql);
                foreach (DataRow dr in dt.Rows)
                {
                    //数组赋值
                    GetName[GetNameInd] = dr["OA_FixMan"].ToString();
                    GetNameInd += 1;
                }
                //判断
                for (int i = 0; i < GetNameInd; i++)
                {
                    for (int j = i + 1; j < GetName.Length; j++)
                    {
                        if (GetName[i] == GetName[j])
                        {
                            throw new Exception("@不允许填写重复项!");
                        }
                    }
                }
                return "合计已经在发送前事件完成.";
            }

            if (this.HisNode.NodeID == 1402)
            {
                //排除同一物品被多人同时申请成功的bug
                string isExitSql = "select * from oa_fixgetuse";
                string checkSql = "select * from ND1401Dtl1 where  RefPK=" + this.WorkID;
                DataTable dtIsExitSql = DBAccess.RunSQLReturnTable(isExitSql);
                DataTable dtCheckSql = DBAccess.RunSQLReturnTable(checkSql);

                for (int i = 0; i < dtCheckSql.Rows.Count; i++)//少
                {
                    for (int j = 0; j < dtIsExitSql.Rows.Count; j++)//多
                    {
                        if (dtCheckSql.Rows[i]["OA_FixMan"].ToString() == dtIsExitSql.Rows[j]["FK_FixMan"].ToString() && dtIsExitSql.Rows[j]["GetUseZT"].ToString() == "1")
                        {
                            string delSql = string.Format("delete from  oa_fixgetuse where FK_FixMan='{0}' and GetUseZT ={1}", dtIsExitSql.Rows[j]["FK_FixMan"].ToString(), 0);
                            DBAccess.RunSQL(delSql);
                            throw new Exception("@此物品已被领取!");
                        }
                    }
                }
                return "合计已经在发送前事件完成.";
            }

            return null;
        }
        /// <summary>
        /// 发送成功后
        /// </summary>
        public override string SendSuccess()
        {
            if (this.HisNode.NodeID == 1402)
            {
                string sql = "select * from ND1401Dtl1 where  RefPK=" + this.WorkID;
                DataTable dt = DBAccess.RunSQLReturnTable(sql);

                foreach (DataRow dr in dt.Rows)
                {
                    BP.FixAss.API.FixManGetUse_OneInsert(this.WorkID,
                        dr["OA_FixMan"].ToString(),
                        this.GetValStr("ShenQingRen"),
                        this.GetValStr("LiShuBuMen"),
                        this.GetValStr("RiQi"),
                        "0");
                }
                return "SendSuccess调用成功....";
            }


            if (this.HisNode.NodeID == 1402)
            {
                string sql = "select * from ND1401Dtl1 where  RefPK=" + this.WorkID;
                DataTable dt = DBAccess.RunSQLReturnTable(sql);

                foreach (DataRow dr in dt.Rows)
                {
                    string fk_fixMan = dr["OA_FixMan"].ToString();
                    string GetUseZT = "0";
                    string GetOidsSql = "select OID FROM  OA_FixGetUse WHERE WorkID='" + this.WorkID + "' AND  FK_FixMan=" + fk_fixMan;
                    DataTable GetOidDt = DBAccess.RunSQLReturnTable(GetOidsSql);
                    int oid = int.Parse(GetOidDt.Rows[0][0].ToString());

                    if (this.GetValStr("SHZT") == "0")
                    {
                        GetUseZT = "1";
                    }
                    else
                    {
                        GetUseZT = "2";
                    }
                    BP.FixAss.API.FixManGetUse_TwoUpdate(oid,
                       GetUseZT);
                }

                return "SendSuccess调用成功....";
            }

            return null;
        }
        /// <summary>
        /// 发送失败后
        /// </summary>
        public override string SendError()
        {
            return null;
        }
        /// <summary>
        /// 退回前
        /// </summary>
        public override string ReturnBefore()
        {
            return null;
        }
        /// <summary>
        /// 退回后
        /// </summary>
        public override string ReturnAfter()
        {
            return null;
        }
        #endregion 重写事件，完成业务逻辑.
    }
}
