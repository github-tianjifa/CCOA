using System;
using System.Collections;
using System.Data;
using BP.DA;
using BP.DTS;
using BP.En;
using BP.Web;
using BP.Sys;
using BP.WF;
using BP.Port;

namespace BP.FixAss
{
    /// <summary>
    /// 固定资产归还流程事件实体
    /// </summary>
    public class FixManGiveBackFEE : BP.WF.FlowEventBase
    {
        #region 构造.
        /// <summary>
        /// 固定资产归还流程事件实体
        /// </summary>
        public FixManGiveBackFEE()
        {
        }
        #endregion 构造.

        #region 重写属性.
        public override string FlowMark
        {
            get { return "FixManGiveBack"; }
        }
        #endregion 重写属性.

        #region 重写节点运动事件.
        /// <summary>
        /// 删除后
        /// </summary>
        /// <returns></returns>
        public override string AfterFlowDel()
        {
            return null;
        }
        /// <summary>
        /// 删除前
        /// </summary>
        /// <returns></returns>
        public override string BeforeFlowDel()
        {
            return null;
        }
        /// <summary>
        /// 结束后
        /// </summary>
        /// <returns></returns>
        public override string FlowOverAfter()
        {
            //string sql = "select * from  ND1801dtl1 where  RefPK=" + this.WorkID;

            //DataTable dt = DBAccess.RunSQLReturnTable(sql);

            //foreach (DataRow dr in dt.Rows)
            //{
            //    BP.FixAss.API.FixMan_GiveBack(
            //        dr["OA_FixMan"].ToString(),
            //        dr["OA_FixAssBigCatagory"].ToString(),
            //        dr["OA_FixAssBigCatSon"].ToString(),
            //        dr["GuiGe"].ToString(),
            //        this.GetValStr("TianXieRen"),
            //        this.GetValStr("LiShuBuMen"),
            //      this.GetValStr("ShiJian"),
            //      this.GetValStr("SHZT"),
            //      dr["BeiZhu"].ToString());
            //}
            return "写入成功....";
        }
        /// <summary>
        /// 结束前
        /// </summary>
        /// <returns></returns>
        public override string FlowOverBefore()
        {
            return null;
        }
        #endregion 重写事件，完成业务逻辑.
    }
}
