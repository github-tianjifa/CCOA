﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BP.En;
using BP.DA;
using BP.Web;

namespace BP.OA
{
   
    /// <summary>
    /// 日志薄 属性
    /// </summary>
    public class DRCommentAttr : EntityOIDAttr
    {
        /// <summary>
        /// 日志编号
        /// </summary>
        public const string FK_COM = "FK_COM";
        /// <summary>
        /// 评论
        /// </summary>
        public const string Comm = "Comm";
        /// <summary>
        /// 评论人员
        /// </summary>
        public const string Critic = "Critic";
        /// <summary>
        /// 评论时间
        /// </summary>
        public const string CommDate = "CommDate";
        
    }
    /// <summary>
    /// 日志薄
    /// </summary>
    public partial class DRComment : EntityOID
    {
        #region 属性
        /// <summary>
        /// 日志编号
        /// </summary>
        public string FK_COM
        {
            get
            {
                return this.GetValStringByKey(DRCommentAttr.FK_COM);
            }
            set
            {
                this.SetValByKey(DRCommentAttr.FK_COM, value);
            }
        }
       
        /// <summary>
        /// 内容
        /// </summary>
        public string Comm
        {
            get
            {
                return this.GetValStringByKey(DRCommentAttr.Comm);
            }
            set
            {
                this.SetValByKey(DRCommentAttr.Comm, value);
            }
        }
        /// <summary>
        /// 评论人
        /// </summary>
        public string Critic
        {
            get
            {
                return this.GetValStringByKey(DRCommentAttr.Critic);
            }
            set
            {
                this.SetValByKey(DRCommentAttr.Critic, value);
            }
        }
       /// <summary>
       /// 评论时间
       /// </summary>
        public string CommDate
        {
            get
            {
                return this.GetValStringByKey(DRCommentAttr.CommDate);
            }
            set
            {
                this.SetValByKey(DRCommentAttr.CommDate, value);
            }
        }
       
        #endregion

        #region 构造函数
        /// <summary>
        /// 日志薄
        /// </summary>
        public DRComment() { }
        /// <summary>
        /// 日志薄
        /// </summary>
        /// <param name="no">编号</param>
        public DRComment(int oid) : base(oid) { }
        #endregion

        #region 重写方法
        public override UAC HisUAC
        {
            get
            {
                UAC uac = new UAC();
                uac.OpenForSysAdmin();
                return uac;
            }
        }
        /// <summary>
        /// Map
        /// </summary>
        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                    return this._enMap;

                Map map = new Map();
                map.PhysicsTable = "OA_DRComment"; //物理表名
                map.EnDesc = "日志评论";   // 实体的描述.

                //基本信息
                map.AddTBIntPKOID();
                
                map.AddTBString(DRCommentAttr.FK_COM, null, "日志编号", true, false, 0, 300, 30, true);
                map.AddTBStringDoc(DRCommentAttr.Comm, null, "评论内容", true, false, true);
                map.AddTBString(DRCommentAttr.Critic, null, "评论人", true, false, 0, 100, 30);
                map.AddTBDateTime(DRCommentAttr.CommDate, null, "评论时间", true, false);
                //查询
                this._enMap = map;
                return this._enMap;
            }
        }
        #endregion

        protected override bool beforeInsert()
        {
            this.Critic= WebUser.Name;
            this.CommDate = DataType.CurrentDataTime;
            
            return base.beforeInsert();
        }

     

    }
    /// <summary>
    ///得到集合
    /// </summary>
    public class DRComments : EntitiesOID
    {
        /// <summary>
        /// 得到一个新实体
        /// </summary>
        public override Entity GetNewEntity
        {
            get
            {
                return new DRComment();
            }
        }
        /// <summary>
        /// 日志薄集合
        /// </summary>
        public DRComments()
        {
        }
    }
}
