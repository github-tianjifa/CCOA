﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BP.En;
using System.Data;

namespace BP.OA.Attendance
{
    public class WorkRegisterDetailsAttr : EntityOIDAttr
    {

    }
    public class WorkRegisterDetail : EntityOID
    {

        public override Map EnMap
        {
            get { throw new NotImplementedException(); }
        }
        /// <summary>
        /// 查询某人当天的登记情况
        /// </summary>
        /// <returns></returns>
        public static DataTable GetWorkRegisterDetails(string Fk_Emp)
        {
            string strSql = "select * from OA_WorkRegisterDetails where Fk_emp='" + Fk_Emp + "' and  DATEDIFF(day,GETDATE(),RegDateTime)=0";
            return BP.DA.DBAccess.RunSQLReturnTable(strSql);
        }
    }
    public class WorkRegisterDetails : EntitiesOID
    {
        public override Entity GetNewEntity
        {
            get
            {
                return new WorkRegisterDetail();
            }
        }
    }
}
