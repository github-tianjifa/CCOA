﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BP.DA;
using BP.En;
using BP.Web;

namespace BP.OA
{
    /// <summary>
    /// 通知类别属性
    /// </summary>
    public class ArticleImageAttr : EntityOIDAttr
    {
        public const String AddTime = "AddTime";
        public const String Path = "Path";
    }
    /// <summary>
    /// 通知类别
    /// </summary>
    public partial class ArticleImage : EntityOID
    {
        #region 属性
        public DateTime AddTime
        {
            get { return this.GetValDateTime(ArticleImageAttr.AddTime); }
            set { this.SetValByKey(ArticleImageAttr.AddTime, value); }
        }
        public String Path
        {
            get { return this.GetValStrByKey(ArticleImageAttr.Path); }
            set { this.SetValByKey(ArticleImageAttr.Path, value); }
        }
        #endregion
        #region 构造函数
        /// <summary>
        /// 通知类别
        /// </summary>
        public ArticleImage() { }
        /// <summary>
        /// 通知类别
        /// </summary>
        /// <param name="no">编号</param>
        public ArticleImage(int no) : base(no) { }
        #endregion

        #region 重写方法
        public override UAC HisUAC
        {
            get
            {
                UAC uac = new UAC();
                uac.OpenForSysAdmin();
                return uac;
            }
        }
        /// <summary>
        /// Map
        /// </summary>
        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                    return this._enMap;

                Map map = new Map();
                map.PhysicsTable = "OA_ArticleImage";
                map.EnDesc = "新闻图片";                   // 实体的描述.
                map.CodeStruct = "2";
                //map.DepositaryOfEntity = Depositary.Application; //实体map的存放位置.
                //map.DepositaryOfMap = Depositary.Application;    // Map 的存放位置.
                map.AddTBIntPKOID();
                map.AddTBDateTime(ArticleImageAttr.AddTime, null, "添加时间", true, false);
                map.AddTBString(ArticleImageAttr.Path, null, "新闻图片", true, false, 0, 200, 50);

                //map.AddTBString(ArticleImageAttr.ParentNo, null, "父知识树No", true, false, 0, 100, 30);
                //map.AddTBString(ArticleImageAttr.FK_Unit, null, "所在单位", true, false, 0, 50, 30);

                this._enMap = map;
                return this._enMap;
            }
        }
        #endregion
    }
    /// <summary>
    ///得到集合
    /// </summary>
    public class ArticleImages : EntitiesOID
    {
        /// <summary>
        /// 得到一个新实体
        /// </summary>
        public override Entity GetNewEntity
        {
            get
            {
                return new ArticleImage();
            }
        }
        /// <summary>
        /// 通知类别集合
        /// </summary>
        public ArticleImages()
        {
        }
    }
}
