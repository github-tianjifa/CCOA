﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BP.DA;
using BP.En;
using BP.Port;

/************************************************************
   Copyright (C), 2005-2014, manstrosoft Co., Ltd.
   FileName: BMGG.cs
   Author:孙成瑞
   Date:2014-5-26
   Description:部门公告
   ***********************************************************/
namespace BP.OA.Announcement
{
    public enum ISBM { 
        /// <summary>
        /// 是公司公告
        /// </summary>
        yes,
        /// <summary>
        /// 不是公司公告
        /// </summary>
        no

    }
    public class BMGGAttr : EntityOIDAttr
    {
        /// <summary>
        /// 发起人
        /// </summary>
        public const string FQR = "FQR";
        /// <summary>
        /// 所属部门
        /// </summary>
        public const string SSBM = "SSBM";
        /// <summary>
        /// 发起时间
        /// </summary>
        public const string FQSJ = "FQSJ";
        /// <summary>
        /// 标题
        /// </summary>
        public const string BT = "BT";
        /// <summary>
        /// 公告内容
        /// </summary>
        public const string GGNR = "GGNR";
        /// <summary>
        /// 是否为部门公告
        /// </summary>
        public const string ISBM = "ISBM";
        /// <summary>
        /// 部门编号
        /// </summary>
        public const string BMBH = "BMBH";
        /// <summary>
        /// WorkID
        /// </summary>
        public const string WorkId = "WorkId";
    }
    public class BMGG : EntityOID
    {
        #region 属性
        public string FQR
        {
            get {
                return this.GetValStringByKey(BMGGAttr.FQR);
            }
            set {
                this.SetValByKey(BMGGAttr.FQR, value);
            }
        }
        public string SSBM {
            get {
                return this.GetValStringByKey(BMGGAttr.SSBM);
            }
            set {
                this.SetValByKey(BMGGAttr.SSBM,value);
            }
        }
        public string FQSJ {
            get {
                return this.GetValStringByKey(BMGGAttr.FQSJ);
            }
            set {
                this.SetValByKey(BMGGAttr.FQSJ, value);
            }
        }
        public string BMBH
        {
            get
            {
                return this.GetValStringByKey(BMGGAttr.BMBH);
            }
            set
            {
                this.SetValByKey(BMGGAttr.BMBH, value);
            }
        }
        public string GGNR {
            get
            {
                return this.GetValStringByKey(BMGGAttr.GGNR);
            }
            set
            {
                this.SetValByKey(BMGGAttr.GGNR, value);
            }
        }
        public string BT {
            get {
                return this.GetValStringByKey(BMGGAttr.BT);
            }
            set {
                this.SetValByKey(BMGGAttr.BT, value);
            }
        }
        public string ISBM
        {
            get
            {
                return this.GetValStringByKey(BMGGAttr.ISBM);
            }
            set
            {
                this.SetValByKey(BMGGAttr.ISBM, value);
            }
        }
        public Int64 WorkId
        {
            get
            {
                return this.GetValInt64ByKey(BMGGAttr.WorkId);
            }
            set
            {
                this.SetValByKey(BMGGAttr.WorkId, value);
            }
        }
       
        #endregion 属性

        public override UAC HisUAC
        {
            get
            {
                UAC uac = new UAC();
                uac.IsUpdate = false;
                uac.IsDelete = true;
                uac.IsInsert = false;
                uac.IsView = true;
                return uac;
            }
        }


        public override Map EnMap
        {
            get {
                if (this._enMap != null)
                    return this._enMap;
                Map map = new Map("Notice");
                map.EnDesc = "公告";
                map.CodeStruct = "9";
                map.DepositaryOfEntity = Depositary.None;
                //增加OID主键字段。
                map.AddTBIntPKOID();
                map.AddTBString(BMGGAttr.FQR, null, "发起人", true, true, 0, 100, 30);
                map.AddTBString(BMGGAttr.SSBM, null, "所属部门", true, true, 0, 100, 30);
                map.AddTBDate(BMGGAttr.FQSJ, "发起时间", true, true);
                map.AddTBString(BMGGAttr.BT, null, "标题", true, true, 0, 100, 30);
                map.AddTBString(BMGGAttr.GGNR, null, "公告内容", true, true, 0, 1000, 30);
                map.AddDDLSysEnum(BMGGAttr.ISBM, 0, "是否为公司公告", true, true, BMGGAttr.ISBM, "@0 = 否@1 = 是");
                map.AddTBString(BMGGAttr.BMBH, "0", "部门编号", true, true, 0, 5, 30);
                map.AddTBString(BMGGAttr.WorkId, null, "workid", false, true, 0, 100, 30);

                map.AddSearchAttr(BMGGAttr.ISBM);
                this._enMap = map;
                return this._enMap;
            }
        }
    }
    public class BMGGs : EntitiesOID
    {
        /// <summary>
        /// 年检台帐s
        /// </summary>
        public BMGGs() { }
        /// <summary>
        /// 得到它的 Entity 
        /// </summary>
        public override Entity GetNewEntity
        {
            get
            {
                return new BMGG();
            }
        }
    }
}
