﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BP.OA.BH
{
    public static class API
    {
        #region 社情业务流转 API
        /// <summary>
        /// 社情业务流转流程是否启用?
        /// </summary>
        public static bool SqWorkFlow_FlowIsEnable
        {
            get
            {
                return BP.Sys.SystemConfig.GetValByKeyBoolen("SqWorkFlow_FlowIsEnable", false);
            }
        }
        /// <summary>
        /// 社情业务流转的流程标记
        /// </summary>
        public static string SqWorkFlow_FlowMark
        {
            get
            {
                return "SqWorkFlow";
            }
        }

      /// <summary>
      /// 社情业务流转API
      /// </summary>
      /// <param name="sjbt">事件标题</param>
      /// <param name="lb">事件类别</param>
      /// <param name="dw">单位</param>
      /// <param name="xm">姓名</param>
      /// <param name="dh">电话</param>
      /// <param name="pcsj">排查时间</param>
      /// <param name="wtms">问题描述</param>
      /// <param name="fj">多媒体附件</param>
      /// <param name="blfs">办理方式</param>
      /// <returns></returns>
        public static string SqWorkFlow(string sjbt, string lb, string dw, string xm,
            string dh, string  pcsj, string dz,  string wtms,string blfs)
        {
            SqWorkFlow en = new SqWorkFlow();
            en.Titel = sjbt;
            en.Leibie = lb;
            en.FK_Dept = dw;
            en.Name = xm;
            en.Tel = dh;
            en.PCTime = pcsj;
            en.Address = dz;
            en.ProblemSay = wtms;
            en.Mode = blfs;
            en.Insert();
            return "调用api成功...";
        }
        #endregion 社情业务流转api.

        #region 台账业务流转 API
        /// <summary>
        /// 台账业务流转流程是否启用?
        /// </summary>
        public static bool TzWorkFlow_FlowIsEnable
        {
            get
            {
                return BP.Sys.SystemConfig.GetValByKeyBoolen("TzWorkFlow_FlowIsEnable", false);
            }
        }
        /// <summary>
        /// 台账业务流转的流程标记
        /// </summary>
        public static string TzWorkFlow_FlowMark
        {
            get
            {
                return "TzWorkFlow";
            }
        }
       /// <summary>
       /// 台账业务流转
       /// </summary>
       /// <param name="sjbh">事件编号</param>
       /// <param name="yhdwmc">隐患单位名称</param>
       /// <param name="xm">排查人姓名</param>
       /// <param name="tsdw">推送单位</param>
       /// <param name="dh">电话</param>
       /// <param name="pcsj">排查时间</param>
       /// <param name="wtms">问题描述</param>
       /// <param name="tzlx">台账类型</param>
       /// <returns></returns>
        public static string TzWorkFlow(string sjbh, string yhdwmc, string xm,
            string tsdw, string dh, string  pcsj, string wtms, string tzlx)
        {
            TzWorkFlow en = new TzWorkFlow();
            en.Numb = sjbh;
            en.UnitName = yhdwmc;
            en.Name = xm;
            en.PushUnit = tsdw;
            en.Tel = dh;
            en.PcTime = pcsj;
            en.ProblemSay = wtms;
            en.TaizType = tzlx;
            en.Insert();
            return "调用api成功...";
        }
        #endregion 台账业务流转api.

    }
}
