﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BP.En;
namespace BP.OA.BH
{
    class TzWorkFlowAttr : EntityOIDAttr
    {
        /// <summary>
        /// 事件编号
        /// </summary>
        public const string Numb = "Numb";
        /// <summary>
        /// 隐患单位名称
        /// </summary>
        public const string UnitName = "UnitName";
        /// <summary>
        /// 排查人姓名
        /// </summary>
        public const string Name = "Name";
       /// <summary>
       /// 排查人电话
       /// </summary>
        public const string Tel = "Tel";
        /// <summary>
        /// 推送单位
        /// </summary>
        public const string PushUnit = "PushUnit";
        /// <summary>
        /// 排查时间
        /// </summary>
        public const string PcTime = "PcTime";
        /// <summary>
        /// 问题描述
        /// </summary>
        public const string ProblemSay = "ProblemSay";
        /// <summary>
        /// 台账类型
        /// </summary>
        public const string TaizType = "TaizType";
       
    }
    public class TzWorkFlow : EntityOID
    {
        #region 属性
        /// <summary>
        /// 事件编号
        /// </summary>
        public string Numb
        {
            get
            {
                return this.GetValStrByKey(TzWorkFlowAttr.Numb);
            }
            set
            {
                this.SetValByKey(TzWorkFlowAttr.Numb, value);
            }
        }
        /// <summary>
        /// 隐患单位名称
        /// </summary>
        public string UnitName
        {
            get
            {
                return this.GetValStrByKey(TzWorkFlowAttr.UnitName);
            }
            set
            {
                this.SetValByKey(TzWorkFlowAttr.UnitName, value);
            }
        }
        /// <summary>
        /// 排查人姓名
        /// </summary>
        public string Name
        {
            get
            {
                return this.GetValStrByKey(TzWorkFlowAttr.Name);
            }
            set
            {
                this.SetValByKey(TzWorkFlowAttr.Name, value);
            }
        }
        /// <summary>
        /// 排查人电话
        /// </summary>
        public string Tel
        {
            get
            {
                return this.GetValStrByKey(TzWorkFlowAttr.Tel);
            }
            set
            {
                this.SetValByKey(TzWorkFlowAttr.Tel, value);
            }
        }
        /// <summary>
        /// 推送单位
        /// </summary>
        public string PushUnit
        {
            get
            {
                return this.GetValStrByKey(TzWorkFlowAttr.PushUnit);
            }
            set
            {
                this.SetValByKey(TzWorkFlowAttr.PushUnit, value);
            }
        }
        /// <summary>
        /// 问题描述
        /// </summary>
        public string ProblemSay
        {
            get
            {
                return this.GetValStrByKey(TzWorkFlowAttr.ProblemSay);
            }
            set
            {
                this.SetValByKey(TzWorkFlowAttr.ProblemSay, value);
            }
        }
        /// <summary>
        /// 台账类型
        /// </summary>
        public string TaizType
        {
            get
            {
                return this.GetValStrByKey(TzWorkFlowAttr.TaizType);
            }
            set
            {
                this.SetValByKey(TzWorkFlowAttr.TaizType, value);
            }
        }
        /// <summary>
        /// 排查时间
        /// </summary>
        public string PcTime
        {
            get
            {
                return this.GetValStrByKey(TzWorkFlowAttr.PcTime);
            }
            set
            {
                this.SetValByKey(TzWorkFlowAttr.PcTime, value);
            }
        }
     
        #endregion
        public override UAC HisUAC
        {
            get
            {
                UAC uac = new UAC();
                //uac.OpenForSysAdmin();
                uac.IsUpdate = true;
                uac.IsDelete = true;
                uac.IsInsert = true;
                uac.IsView = true;
                return uac;
            }
        }

        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                    return this._enMap;
                Map map = new Map("OA_TzWorkFlow");
                map.EnDesc = "台账业务查询";
                map.CodeStruct = "9";
                //map.DepositaryOfEntity = Depositary.Application;
                //map.DepositaryOfMap = Depositary.Application;
                map.AddTBIntPKOID();
                map.AddTBString(TzWorkFlowAttr.Numb, null, "事件编号", true, false, 0, 10, 30);
                map.AddDDLEntities(TzWorkFlowAttr.UnitName, null, "隐患单位名称", new BP.GPM.Depts(), true);
                map.AddTBString(TzWorkFlowAttr.Name, null, "排查人姓名", true, false, 0, 4000, 500);
                map.AddTBString(TzWorkFlowAttr.Tel, null, "排查人电话", true, false, 0, 4000, 500);
                map.AddDDLEntities(TzWorkFlowAttr.PushUnit, null, "推送单位", new BP.GPM.Depts(), true);
                map.AddTBDate(TzWorkFlowAttr.PcTime, null, "排查时间", true, false);
                map.AddTBStringDoc(TzWorkFlowAttr.ProblemSay, null, "问题描述", true, false, true);
                map.AddDDLSysEnum(TzWorkFlowAttr.TaizType, 0, "台账类型", true, true, TzWorkFlowAttr.TaizType, "@0=安全生产@1=非法生产@2=非法经营@3=非法建设@4=信访稳定@5=基础组织建设@6=数字城管业务@7=城市管理提升@8=党风廉政建设@9=其他");
                //查询
                map.AddSearchAttr(TzWorkFlowAttr.UnitName);//隐患单位名称查询
                ////map.AddSearchAttr(TzWorkFlowAttr.Name);
                map.DTSearchKey = TzWorkFlowAttr.PcTime;//排查时间
                map.DTSearchWay = Sys.DTSearchWay.ByDate;//时间段查询
                this._enMap = map;
                return this._enMap;
            }
        }
    }
    /// <summary>
    /// 预定信息
    /// </summary>
    public class TzWorkFlows : EntitiesOID
    {
        /// <summary>
        /// 预定信息s
        /// </summary>
        public TzWorkFlows() { }
        /// <summary>
        /// 得到它的 Entity 
        /// </summary>
        public override Entity GetNewEntity
        {
            get
            {
                return new TzWorkFlow();
            }
        }
    }
}
