﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BP.En;
using BP.OA;

namespace BP.OA.RCSWGL
{
    /// <summary>
    /// 订票类型
    /// </summary>
    public enum DPType
    { 
        /// <summary>
        /// 飞机票
        /// </summary>
        FJP,
        /// <summary>
        /// 火车票
        /// </summary>
        HCP
    }
    /// <summary>
    /// 票务管理属性
    /// </summary>
    public class PWGLAttr : EntityNoNameAttr {
        /// <summary>
        ///申请人
        /// </summary>
        public const string Name = "Name";
        /// <summary>
        /// 出差日期
        /// </summary>
        public const string CHDate = "CHDate";
        /// <summary>
        /// 订票类型
        /// </summary>
        public const string DPType = "DPType";
        /// <summary>
        /// 目的地
        /// </summary>
        public const string MDD = "MDD";
        /// <summary>
        /// 上级领导
        /// </summary>
        public const string SJLD = "SJLD";
        /// <summary>
        /// 订票管理员
        /// </summary>
        public const string DPAdmin = "DPAdmin";
        /// <summary>
        /// 航班号
        /// </summary>
        public const string HBH = "HBH";
        /// <summary>
        /// 价格
        /// </summary>
        public const string JG = "JG";
        /// <summary>
        /// 起飞日期
        /// </summary>
        public const string QFDate = "QFDate";
        /// <summary>
        /// 订票日期
        /// </summary>
        public const string DPDate = "DPDate";

        public const string FK_NY = "FK_NY";
        /// <summary>
        /// 备注
        /// </summary>
        public const string Note = "Note";
        /// <summary>
        /// 操作
        /// </summary>
        public const string CZ = "CZ";
        /// <summary>
        /// 查看流程
        /// </summary>
        public const string CKLC = "CKLC";
        /// <summary>
        /// workid
        /// </summary>
        public const string WorkId = "WorkId";
        
        
    }
    /// <summary>
    /// 票务管理
    /// </summary>
   public class PWGL:EntityNoName
    {
        #region 构造方法
        /// <summary>
        /// 无参构造方法
        /// </summary>
        public PWGL(){ }
        /// <summary>
        /// 有参构造函数
        /// </summary>
        /// <param name="_No"></param>
        public PWGL(string _No) : base(_No) { }
        #endregion

        #region 属性

        public string FK_NY
        {
            get
            { return this.GetValStrByKey(PWGLAttr.FK_NY); }

            set
            {
                this.SetValByKey(PWGLAttr.FK_NY, value);
            }
        }

        public Int64 WorkId
        {
            get
            {
                return this.GetValInt64ByKey(HQGLAttr.WorkId);
            }
            set
            {
                this.SetValByKey(HQGLAttr.WorkId, value);
            }
        }
        /// <summary>
        /// 票务类型
        /// </summary>
        public int DPType
        {
            get {
                return this.GetValIntByKey(PWGLAttr.DPType);
            }
            set {
                this.SetValByKey(PWGLAttr.DPType,value);
            }
        }
       /// <summary>
       /// 订票类型名称
       /// </summary>
        public string DPTypeText
        {
            get {
                return this.GetValRefTextByKey(PWGLAttr.DPType);
            }
        }

       /// <summary>
       /// 订票日期
       /// </summary>
        public string DPDate {
            get {
                return this.GetValStrByKey(PWGLAttr.DPDate);
            }
            set {
                this.SetValByKey(PWGLAttr.DPDate,value);
            }
        }
       /// <summary>
       /// 申请人
       /// </summary>
        public string Name {
            get {
                return this.GetValStrByKey(PWGLAttr.Name);
            }
            set {
                this.SetValByKey(PWGLAttr.Name, value);
            }
        }
       /// <summary>
       /// 出差日期
       /// </summary>
        public string CHDate
        {
            get
            {
                return this.GetValStrByKey(PWGLAttr.CHDate);
            }
            set
            {
                this.SetValByKey(PWGLAttr.CHDate, value);
            }
        }
       /// <summary>
       /// 目的地
       /// </summary>
        public string MDD
        {
            get
            {
                return this.GetValStrByKey(PWGLAttr.MDD);
            }
            set
            {
                this.SetValByKey(PWGLAttr.MDD, value);
            }
        }
       /// <summary>
       /// 上级领导
       /// </summary>
        public string SJLD
        {
            get
            {
                return this.GetValStrByKey(PWGLAttr.SJLD);
            }
            set
            {
                this.SetValByKey(PWGLAttr.SJLD, value);
            }
        }
       /// <summary>
       /// 订票管理员
       /// </summary>
        public string DPAdmin
        {
            get
            {
                return this.GetValStrByKey(PWGLAttr.DPAdmin);
            }
            set
            {
                this.SetValByKey(PWGLAttr.DPAdmin, value);
            }
        }
       /// <summary>
       /// 航班号
       /// </summary>
        public string HBH
        {
            get
            {
                return this.GetValStrByKey(PWGLAttr.HBH);
            }
            set
            {
                this.SetValByKey(PWGLAttr.HBH, value);
            }
        }
       /// <summary>
       /// 价格
       /// </summary>
        public decimal JG
        {
            get
            {
                return this.GetValDecimalByKey(PWGLAttr.JG);
            }
            set
            {
                this.SetValByKey(PWGLAttr.JG, value);
            }
        }
       /// <summary>
       /// 起飞日期
       /// </summary>
        public string QFDate
        {
            get
            {
                return this.GetValStrByKey(PWGLAttr.QFDate);
            }
            set
            {
                this.SetValByKey(PWGLAttr.QFDate, value);
            }
        }
       /// <summary>
       /// 备注
       /// </summary>
        public string Note
        {
            get
            {
                return this.GetValStrByKey(PWGLAttr.Note);
            }
            set
            {
                this.SetValByKey(PWGLAttr.Note, value);
            }
        }
       /// <summary>
       /// 操作
       /// </summary>
        public string CZ
        {
            get
            {
                return this.GetValStrByKey(PWGLAttr.CZ);
            }
            set
            {
                this.SetValByKey(PWGLAttr.CZ, value);
            }
        }
       /// <summary>
       /// 查看流程
       /// </summary>
        public string CKLC
        {
            get
            {
                return this.GetValStrByKey(PWGLAttr.CKLC);
            }
            set
            {
                this.SetValByKey(PWGLAttr.CKLC, value);
            }
        }


        #endregion

        #region 权限控制
        public override UAC HisUAC
        {
            get
            {
                if(!BP.Web.WebUser.No.Equals("admin")){
                    base.HisUAC.Readonly();
                }
                return base.HisUAC;
            }
        }
        
        #endregion
        public override Map EnMap
        {
            get { 
                if(this._enMap != null)
                return this._enMap;

                Map map = new Map("OA_PWGL");
                map.EnDesc = "票务管理";//描述
                map.CodeStruct = "3";
                //存储位置
                map.DepositaryOfEntity = BP.DA.Depositary.None;
                map.DepositaryOfMap = BP.DA.Depositary.None;

                map.AddTBStringPK(PWGLAttr.No,null,"编号",true,true,0,100,20);
                
                map.AddTBString(PWGLAttr.Name, null, "申请人", true, false, 0, 100, 20);
                
                map.AddTBDate(PWGLAttr.CHDate, null, "出差日期", true, false);
                
                map.AddDDLSysEnum(PWGLAttr.DPType, 0, "订票类型", true, true, PWGLAttr.DPType, "@0=飞机票@1=火车票");
                
                map.AddTBString(PWGLAttr.MDD, null, "目的地", true, false, 0, 100, 20);
                map.AddTBString(PWGLAttr.SJLD, null, "上级领导", true, false, 0, 100, 20);
                map.AddTBString(PWGLAttr.DPAdmin, null, "订票管理员", true, false, 0, 100, 20);
                map.AddTBString(PWGLAttr.HBH, null, "航班号/火车车次", true, false, 0, 100, 20);
                
                map.AddTBMoney(PWGLAttr.JG, 0, "价格", true, false);
                
                map.AddTBDate(PWGLAttr.QFDate, null, "启程日期", true, false);
                map.AddTBDate(PWGLAttr.DPDate, null, "订票日期", true, false);
                map.AddDDLEntities(PWGLAttr.FK_NY, null, "隶属年月", new BP.Pub.NYs(), true);
                
                map.AddTBStringDoc(PWGLAttr.Note, null, "备注", true, false, true);
                
                //map.AddTBString(PWGLAttr.CZ, null, "操作", true, false, 0, 100, 20);
                //map.AddTBString(PWGLAttr.CKLC, null, "查看流程", true, false, 0, 100, 20);

                //设置查询条件
                map.AddSearchAttr(PWGLAttr.DPType);
                map.AddSearchAttr(PWGLAttr.FK_NY);

                this._enMap = map;
                return this._enMap; 
            }
        }

        #region 重写方法
        protected override bool beforeInsert()
        {
            this.DPDate = DateTime.Now.ToString();
           
            return base.beforeInsert();
        }
        protected override bool beforeUpdate()
        {
            this.DPDate = DateTime.Now.ToString();
            return base.beforeUpdate();
        }
        #endregion
    }

    public class PWGLs : EntitiesNoName {
        /// <summary>
        /// 票务管理s
        /// </summary>
        public PWGLs() { }
        /// <summary>
        /// 得到它的entity
        /// </summary>
        public override Entity GetNewEntity
        {
            get {
                return new PWGL();
            }
        }
    }
}
