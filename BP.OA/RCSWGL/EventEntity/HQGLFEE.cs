﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BP.WF;
using BP.Sys;

namespace BP.OA.RCSWGL
{
    public class HQGLFEE:BP.WF.FlowEventBase
    {
        #region 构造方法
        /// <summary>
        /// 后勤管理流程事件实体
        /// </summary>
        public HQGLFEE() { }
        #endregion

        #region 重写属性
        public override string FlowMark 
        {
            get 
            {
                return API.RCSWGL_HQGL_FlowMark;
            }
        }
        #endregion

        #region 重写节点事件方法
        /// <summary>
        /// 删除后
        /// </summary>
        /// <returns></returns>
        public override string AfterFlowDel()
        {

            return null;
        }
        /// <summary>
        /// 删除前
        /// </summary>
        /// <returns></returns>
        public override string BeforeFlowDel()
        {
            return null;
        }
        /// <summary>
        /// 结束后
        /// </summary>
        /// <returns></returns>
        public override string FlowOverAfter()
        {
            API.RCSWGL_HQGL(this.GetValInt("LB"), this.GetValStr("SBR"),
                this.GetValStr("SBRQ"), this.GetValStr("YCQK"), this.GetValStr("DD"), this.GetValStr("DH"),
                this.GetValStr("DJSJ"), this.GetValStr("HQSSMC"), this.GetValStr("HQSSBH"),
                this.GetValStr("WXFZR"), this.GetValStr("YY"), this.GetValStr("ZRHF"),
                this.GetValStr("CLYJ"), this.GetValDecimal("WXSXSC"), this.GetValDecimal("CSFY"),
                this.GetValStr("WCSJ"),this.GetValInt("ZT"), this.GetValStr("BZ"), this.GetValStr("SBBZ"),
               this.GetValInt64("WorkId"));
                
            // 发送提醒消息
            //BP.WF.Dev2Interface.WriteToSMS(this.GetValStr("SBR"),this.GetValStr(""));
            return "写入成功。。。。。";
        }
        /// <summary>
        /// 结束前
        /// </summary>
        /// <returns></returns>
        public override string FlowOverBefore()
        {
            return null;
        }
        #endregion 重写事件完成业务逻辑
    }
}
