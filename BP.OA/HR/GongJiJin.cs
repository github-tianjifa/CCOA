﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BP.En;
using BP.Port;
namespace BP.OA.HR
{
    /// <summary>
    /// 公积金 属性
    /// </summary>
    public class GongJiJinAttr : EntityOIDAttr
    {
        #region 基本属性
        /// <summary>
        /// 部门
        /// </summary>
        public const string FK_Dept = "FK_Dept";
        /// <summary>
        /// 单位
        /// </summary>
        public const string FK_DanWei = "FK_DanWei";
        /// <summary>
        /// 姓名
        /// </summary>
        public const string FK_Name = "FK_Name";
        /// <summary>
        /// 月份
        /// </summary>
        public const string Mouth = "Mouth";
        /// <summary>
        /// 新基数
        /// </summary>
        public const string XinJiShu = "XinJiShu";
        /// <summary>
        /// 月缴存额
        /// </summary>
        public const string YueJiaoCunE = "YueJiaoCunE";
        /// <summary>
        /// 单位缴存
        /// </summary>
        public const string DanWeiJiaoCun = "DanWeiJiaoCun";
        /// <summary>
        /// 个人缴存
        /// </summary>
        public const string GeRenJiaoCun = "GeRenJiaoCun";
        /// <summary>
        /// 合计
        /// </summary>
        public const string HeJi = "HeJi";
        /// <summary>
        /// 个人账号
        /// </summary>
        public const string GeRenZhangHao = "GeRenZhangHao";
        /// <summary>
        /// 记录时间
        /// </summary>
        public const string RDT = "RDT";
        /// <summary>
        /// 备注
        /// </summary>
        public const string BeiZhu = "BeiZhu";
        #endregion
    }
    /// <summary>
    /// 公积金
    /// </summary>
    public class GongJiJin : EntityOID
    {
        /// <summary>
        /// 
        /// </summary>
        public string FK_DeptText
        {
            get { return this.GetValStringByKey(GongJiJinAttr.FK_Dept); }
            set { this.SetValByKey(GongJiJinAttr.FK_Dept, value); }
        }
        /// <summary>
        /// 部门
        /// </summary>
        public string FK_Dept
        {
            get { return this.GetValStringByKey(GongJiJinAttr.FK_Dept); }
            set { this.SetValByKey(GongJiJinAttr.FK_Dept, value); }
        }
        /// <summary>
        /// 单位
        /// </summary>
        public string FK_DanWei
        {
            get { return this.GetValStringByKey(GongJiJinAttr.FK_DanWei); }
            set { this.SetValByKey(GongJiJinAttr.FK_DanWei, value); }
        }
        /// <summary>
        /// 姓名
        /// </summary>
        public string FK_Name
        {
            get { return this.GetValStringByKey(GongJiJinAttr.FK_Name); }
            set { this.SetValByKey(GongJiJinAttr.FK_Name, value); }
        }

        /// <summary>
        /// 月份
        /// </summary>
        public string Mouth
        {
            get { return this.GetValStringByKey(GongJiJinAttr.Mouth); }
            set { this.SetValByKey(GongJiJinAttr.Mouth, value); }
        }
        /// <summary>
        /// 新基数
        /// </summary>
        public float XinJiShu
        {
            get { return this.GetValFloatByKey(GongJiJinAttr.XinJiShu); }
            set { this.SetValByKey(GongJiJinAttr.XinJiShu, value); }
        }
        /// <summary>
        /// 月缴存额
        /// </summary>
        public string GuDingQiXian
        {
            get { return this.GetValStringByKey(GongJiJinAttr.YueJiaoCunE); }
            set { this.SetValByKey(GongJiJinAttr.YueJiaoCunE, value); }
        }
        /// <summary>
        /// 单位缴存
        /// </summary>
        public float DanWeiJiaoCun
        {
            get { return this.GetValFloatByKey(GongJiJinAttr.DanWeiJiaoCun); }
            set { this.SetValByKey(GongJiJinAttr.DanWeiJiaoCun, value); }
        }
        /// <summary>
        /// 个人缴存
        /// </summary>
        public float GeRenJiaoCun
        {
            get { return this.GetValFloatByKey(GongJiJinAttr.GeRenJiaoCun); }
            set { this.SetValByKey(GongJiJinAttr.GeRenJiaoCun, value); }
        }
        /// <summary>
        /// 个人账号
        /// </summary>
        public string GeRenZhangHao
        {
            get { return this.GetValStringByKey(GongJiJinAttr.GeRenZhangHao); }
            set { this.SetValByKey(GongJiJinAttr.GeRenZhangHao, value); }
        }
        /// <summary>
        /// 个人账号
        /// </summary>
        public string Note
        {
            get { return this.GetValStringByKey(GongJiJinAttr.GeRenZhangHao); }
            set { this.SetValByKey(GongJiJinAttr.GeRenZhangHao, value); }
        }
        /// <summary>
        /// 记录时间
        /// </summary>
        public string RDT
        {
            get { return this.GetValStringByKey(GongJiJinAttr.RDT); }
            set { this.SetValByKey(GongJiJinAttr.RDT, value); }
        }
        /// <summary>
        /// 备注
        /// </summary>
        public string BeiZhu
        {
            get { return this.GetValStringByKey(GongJiJinAttr.BeiZhu); }
            set { this.SetValByKey(GongJiJinAttr.BeiZhu, value); }
        }
        /// <summary>
        /// 合计
        /// </summary>
        public float HeJi
        {
            get { return this.GetValFloatByKey(GongJiJinAttr.HeJi); }
            set { this.SetValByKey(GongJiJinAttr.HeJi, value); }
        }
        #region 构造函数
        public GongJiJin()
        {

        }

        #endregion
        #region 重写父类的方法
        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                {
                    return this._enMap;
                }
                Map map = new Map("OA_GongJiJin");
                map.EnDesc = "公积金签订";
                //自动编号
                map.CodeStruct = "4";
                map.IsAutoGenerNo = true;
                map.AddTBIntPKOID();
                //字段
                map.AddTBString(GongJiJinAttr.FK_DanWei, null, "单位", true, false, 1, 50, 100);
                map.AddDDLEntities(GongJiJinAttr.FK_Dept, null, "部门", new Depts(), true);
                map.AddDDLEntities(GongJiJinAttr.FK_Name, null, "姓名", new BP.OA.HR.HRRecords(), true);
                map.AddDDLSysEnum(FuLiAttr.NianFen, 0, "年份", true, true, FuLiAttr.NianFen, "@0=2010@1=2011@2=2012@3=2013@4=2014@5=2015");
                map.AddDDLSysEnum(FuLiAttr.YueFen, 0, "月份", true, true, FuLiAttr.YueFen, "@0=1@1=2@2=3@3=4@4=5@5=6@6=7@7=8@8=9@9=10@10=11@11=12");
                map.AddTBMoney(GongJiJinAttr.XinJiShu, 0, "新基数", true, false);
                map.AddTBMoney(GongJiJinAttr.DanWeiJiaoCun, 0, "单位缴存", true, false);
                map.AddTBMoney(GongJiJinAttr.GeRenJiaoCun, 0, "个人缴存", true, false);
                map.AddTBMoney(GongJiJinAttr.HeJi, 0, "合计", true, true);
                map.AddTBString(GongJiJinAttr.GeRenZhangHao, null, "个人账号", true, false, 1, 50, 100);
                map.AddTBDate(GongJiJinAttr.RDT, null, "记录时间", true, false);
                map.AddTBStringDoc(GongJiJinAttr.BeiZhu, null, "备注", true, false, true);
                //map.AddMyFile("公积金");
                //查询条件
                //map.AddSearchAttr(GongJiJinAttr.FK_Dept);
                map.AddSearchAttr(FuLiAttr.YueFen);
                map.AddSearchAttr(GongJiJinAttr.FK_Name);
                this._enMap = map;
                return this._enMap;
            }
        }
        public override UAC HisUAC
        {
            get
            {
                UAC uac = new UAC();
                return uac.OpenForSysAdmin();
            }
        }
        protected override bool beforeUpdateInsertAction()
        {
            this.HeJi =  this.DanWeiJiaoCun + this.GeRenJiaoCun;
            return base.beforeUpdateInsertAction();
        }
        #endregion
    }
    /// <summary>
    /// 公积金s
    /// </summary>
    public class GongJiJins : EntitiesOID
    {
        #region 构造函数

        public GongJiJins()
        {

        }
        #endregion
        #region 重写父类方法
        public override Entity GetNewEntity
        {
            get { return new GongJiJin(); }
        }
        #endregion
    }
}
