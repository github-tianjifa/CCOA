
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BP.En;
using BP.Port;
using System.Data;
using BP.DA;
namespace BP.OA
{
    //OID， FK_档案登记， 原部门， 原职务， 现在部门， 现在职务， 调用原因， 调动时间， 备注

    /// <summary>
    /// 人员调动 属性
    /// </summary>
    public class HRTransferAttr : EntityNoNameAttr
    {
        #region 基本属性
        /// <summary>
        /// FK_档案登记
        /// </summary>
        public const string FK_HRRecord = "FK_HRRecord";
        /// <summary>
        /// 原部门
        /// </summary>
        public const string FK_OldDept = "FK_OldDept";
        /// <summary>
        /// 原职务
        /// </summary>
        public const string OldZhiWu = "OldZhiWu";
        /// <summary>
        /// 现在部门
        /// </summary>
        public const string FK_NewDept = "FK_NewDept";
        /// <summary>
        /// 现在职务
        /// </summary>
        public const string NewZhiWu = "NewZhiWu";
        /// <summary>
        /// 调用原因
        /// </summary>
        public const string DiaoDongWhy = "DiaoDongWhy";
        /// <summary>
        /// 调动时间
        /// </summary>
        public const string DiaoDongDT = "DiaoDongDT";
        /// <summary>
        /// 备注
        /// </summary>
        public const string Note = "Note";

        #endregion
    }

    /// <summary>
    /// 人员调动
    /// </summary>
    public class HRTransfer : EntityNoName
    {
        #region 基本属性
        /// <summary>
        /// FK_人事档案
        /// </summary>
        public string FK_HRRecord
        {
            get { return this.GetValStringByKey(HRTransferAttr.FK_HRRecord); }
            set { this.SetValByKey(HRTransferAttr.FK_HRRecord, value); }
        }

        /// <summary>
        /// 原部门
        /// </summary>
        public string FK_OldDept
        {
            get { return this.GetValStringByKey(HRTransferAttr.FK_OldDept); }
            set { this.SetValByKey(HRTransferAttr.FK_OldDept, value); }
        }
        /// <summary>
        /// 原职务
        /// </summary>
        public string OldZhiWu
        {
            get { return this.GetValStringByKey(HRTransferAttr.OldZhiWu); }
            set { this.SetValByKey(HRTransferAttr.OldZhiWu, value); }
        }
        /// <summary>
        /// 现部门
        /// </summary>
        public string FK_NewDept
        {
            get { return this.GetValStringByKey(HRTransferAttr.FK_NewDept); }
            set { this.SetValByKey(HRTransferAttr.FK_NewDept, value); }
        }
        /// <summary>
        /// 现职务
        /// </summary>
        public string NewZhiWu
        {
            get { return this.GetValStringByKey(HRTransferAttr.NewZhiWu); }
            set { this.SetValByKey(HRTransferAttr.NewZhiWu, value); }
        }
        /// <summary>
        /// 调动原因
        /// </summary>
        public string DiaoDongWhy
        {
            get { return this.GetValStringByKey(HRTransferAttr.DiaoDongWhy); }
            set { this.SetValByKey(HRTransferAttr.DiaoDongWhy, value); }
        }
        /// <summary>
        /// 调动日期
        /// </summary>
        public string DiaoDongDT
        {
            get { return this.GetValStringByKey(HRTransferAttr.DiaoDongDT); }
            set { this.SetValByKey(HRTransferAttr.DiaoDongDT, value); }
        }
        /// <summary>
        /// 备注
        /// </summary>
        public string Note
        {
            get { return this.GetValStringByKey(HRTransferAttr.Note); }
            set { this.SetValByKey(HRTransferAttr.Note, value); }
        }

        #endregion

        #region 构造函数

        public HRTransfer()
        {
            
        }

        public HRTransfer(string no) : base(no)
        {
            
        }
        #endregion

        #region 重写父类的方法

        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                {
                    return this._enMap;
                }
                Map map = new Map("OA_HRTransfer");
                map.EnDesc = "人员调动";

                //自动编号
                //map.AddTBIntPKOID();
                map.CodeStruct = "4";
                map.IsAutoGenerNo = true;

                //字段
                map.AddTBStringPK(HRTransferAttr.No, null, "编号", true, true, 4, 4, 4);
                map.AddDDLEntities(HRTransferAttr.Name, null, "人员", new BP.Port.Emps(), true);
                map.AddTBDate(HRTransferAttr.DiaoDongDT, null, "调动日期",true, false );
                //map.AddDDLEntities(HRTransferAttr.FK_OldDept, null, "原部门", new Depts(), true);
                map.AddTBString(HRTransferAttr.FK_OldDept, null, "原部门", true, false, 0, 100, 100);
                map.AddTBString(HRTransferAttr.OldZhiWu, null, "原职务", true, false, 0, 100, 100);
                map.AddDDLEntities(HRTransferAttr.FK_NewDept, null, "现在部门", new BP.Port.Depts(), true);
                //map.AddTBString(HRTransferAttr.NewZhiWu, null, "现在职务", true, false, 0, 100, 100);
                map.AddDDLEntities(HRTransferAttr.NewZhiWu, null, "现在职务", new BP.GPM.Dutys(), true);

                map.AddTBStringDoc(HRTransferAttr.DiaoDongWhy, null, "调动原因", true, false, true);
                map.AddTBStringDoc(HRTransferAttr.Note, null, "备注", true, false, true);

                //查询条件
                map.AddSearchAttr(HRTransferAttr.Name);

                this._enMap = map;
                return this._enMap;
            }
        }


      
        protected override void afterInsertUpdateAction()
        {            
            #region
            //HRTransfer hrt = new HRTransfer();
            //string HrSql = "select * from " + hrt.EnMap.PhysicsTable + " where No=" + this.No;
            //DataTable Dsdt = DBAccess.RunSQLReturnTable(HrSql);
            //if (Dsdt.Rows.Count == 0)
            //{
            //hrt.Name = this.Name;
            //hrt.DiaoDongDT = this.DiaoDongDT;
            //hrt.FK_OldDept = this.FK_OldDept;
            //hrt.OldZhiWu = this.OldZhiWu;
            //hrt.FK_NewDept = this.FK_NewDept;
            //hrt.NewZhiWu = this.NewZhiWu;
            //hrt.DiaoDongWhy = this.DiaoDongWhy;
            //hrt.Note = this.Note;
            //hrt.Insert();
            //}
            //HRTransfer hrtr = new HRTransfer(this.Name);
            //hrtr.Name = this.Name;
            //hrtr.DiaoDongDT = this.DiaoDongDT;
            //hrtr.FK_OldDept = this.FK_OldDept;
            //hrtr.Update();
            #endregion
            //插入后跟新
            //BP.GPM.Emp em = new BP.GPM.Emp(this.Name);
            //em.FK_Dept = this.FK_NewDept;
            //em.FK_Duty = this.NewZhiWu;
            //em.Update();
            //base.afterInsertUpdateAction();
            
           
     
           
        }

       
    }

    /// <summary>
    /// 人员调动s
    /// </summary>
    public class HRTransfers : EntitiesNoName
    {
        #region 构造函数

        public HRTransfers()
        {
            
        }
        #endregion

#region 重写父类的方法
        public override Entity GetNewEntity
        {
            get { return new HRTransfer(); }
        } 
        #endregion
    }
}
#endregion

﻿

