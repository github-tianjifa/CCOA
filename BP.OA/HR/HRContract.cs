﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BP.En;
namespace BP.OA.HR
{
    //合同编号， 合同名称， 创建人， 人员名称（FK_人事档案），类型， 签署日期， 登记日期， 固定期限（有、无）， 合同开始日期， 合同截至日期， 备注
    /// <summary>
    /// 劳动合同 属性
    /// </summary>
    public class HRContractAttr : EntityNoNameAttr
    {
        #region 基本属性
        /// <summary>
        /// 创建人
        /// </summary>
        public const string Creater = "Creater";
        /// <summary>
        /// FK_人事档案
        /// </summary>
        public const string FK_HRRecord = "FK_HRRecord";
        /// <summary>
        /// 合同类型
        /// </summary>
        public const string LeiXing = "LeiXing";
        /// <summary>
        /// 签署日期
        /// </summary>
        public const string QianShuDT = "QianShuDT";
        /// <summary>
        /// 登记日期
        /// </summary>
        public const string DengJiDT = "DengJiDT";
        /// <summary>
        /// 固定期限
        /// </summary>
        public const string GuDingQiXian = "GuDingQiXian";
        /// <summary>
        /// 合同开始日期
        /// </summary>
        public const string StartDT = "StartDT";
        /// <summary>
        /// 合同截至日期
        /// </summary>
        public const string EndDT = "EndDT";
        /// <summary>
        /// 备注
        /// </summary>
        public const string Note = "Note";

        #endregion
    }
    /// <summary>
    /// 劳动合同
    /// </summary>
    public class HRContract : EntityNoName
    {
        #region 基本属性
        /// <summary>
        /// 创建人
        /// </summary>
        public string Creater
        {
            get { return this.GetValStringByKey(HRContractAttr.Creater); }
            set { this.SetValByKey(HRContractAttr.Creater, value); }
        }
        /// <summary>
        /// FK_人事档案
        /// </summary>
        public string FK_HRRecord
        {
            get { return this.GetValStringByKey(HRContractAttr.FK_HRRecord); }
            set { this.SetValByKey(HRContractAttr.FK_HRRecord, value); }
        }
        /// <summary>
        /// 合同类型
        /// </summary>
        public string LeiXing
        {
            get { return this.GetValStringByKey(HRContractAttr.LeiXing); }
            set { this.SetValByKey(HRContractAttr.LeiXing, value); }
        }
        /// <summary>
        /// 签署日期
        /// </summary>
        public string QianShuDT
        {
            get { return this.GetValStringByKey(HRContractAttr.QianShuDT); }
            set { this.SetValByKey(HRContractAttr.QianShuDT, value); }
        }
        /// <summary>
        /// 登记日期
        /// </summary>
        public string DengJiDT
        {
            get { return this.GetValStringByKey(HRContractAttr.DengJiDT); }
            set { this.SetValByKey(HRContractAttr.DengJiDT, value); }
        }
        /// <summary>
        /// 固定期限
        /// </summary>
        public string GuDingQiXian
        {
            get { return this.GetValStringByKey(HRContractAttr.GuDingQiXian); }
            set { this.SetValByKey(HRContractAttr.GuDingQiXian, value); }
        }
        /// <summary>
        /// 合同开始日期
        /// </summary>
        public string StartDT
        {
            get { return this.GetValStringByKey(HRContractAttr.StartDT); }
            set { this.SetValByKey(HRContractAttr.StartDT, value); }
        }
        /// <summary>
        /// 合同截至日期
        /// </summary>
        public string EndDT
        {
            get { return this.GetValStringByKey(HRContractAttr.EndDT); }
            set { this.SetValByKey(HRContractAttr.EndDT, value); }
        }
        /// <summary>
        /// 备注
        /// </summary>
        public string Note
        {
            get { return this.GetValStringByKey(HRContractAttr.Note); }
            set { this.SetValByKey(HRContractAttr.Note, value); }
        }
        #endregion

        #region 构造函数

        public HRContract()
        {

        }
        public HRContract(string no)
            : base(no)
        {

        }
        #endregion

        #region 重写父类的方法
        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                {
                    return this._enMap;
                }
                Map map = new Map("OA_HRContract");
                map.EnDesc = "劳动合同签订";
                //自动编号
                map.CodeStruct = "4";
                map.IsAutoGenerNo = true;
                map.AddTBStringPK(HRContractAttr.No, null, "合同编号", true, true, 4, 4, 4);
                //字段
                map.AddDDLEntities(HRContractAttr.FK_HRRecord, null, "姓名", new BP.OA.HR.HRRecords(), true);
                //map.AddTBString(HRContractAttr.Creater, null, "创建人", true, false, 1, 50, 100);
                map.AddTBDate(HRContractAttr.DengJiDT, null, "登记日期", true, false);
                map.AddTBDate(HRContractAttr.QianShuDT, null, "签署日期", true, false);
                map.AddTBDate(HRContractAttr.StartDT, null, "合同开始日期", true, false);
                map.AddTBDate(HRContractAttr.EndDT, null, "合同截至日期", true, false);
                map.AddDDLSysEnum(HRContractAttr.GuDingQiXian, 0, "有无固定期限", true, true, HRContractAttr.GuDingQiXian, "@0=有@1=无");
                //map.AddMyFile("劳动合同");
                map.AddTBStringDoc(HRContractAttr.Note, null, "备注", true, false, true);
                //查询条件
                // map.AddSearchAttr(HRContractAttr.FK_HRRecord);
                //map.AddSearchAttr(HRContractAttr.GuDingQiXian);

                //RefMethod re = new RefMethod();
                this._enMap = map;
                return this._enMap;
            }
        }
        public override UAC HisUAC
        {
            get
            {
                UAC uac = new UAC();
                HRRecord r = new HRRecord(this.FK_HRRecord);
                if (Web.WebUser.No == "admin")
                {
                    uac.OpenAll();
                    return uac;
                }
                if (Web.WebUser.Name == r.Name)
                {
                    uac.IsInsert = false;
                    uac.IsUpdate = true;
                    uac.IsDelete = false;
                    uac.IsView = true;
                }
                else
                {
                    uac.Readonly();
                }
                return uac;
            }
        }
        //protected override bool beforeUpdateInsertAction()
        //{
        //    Port.Emp emp = new Port.Emp(this.FK_HRRecord);
        //    HRContracts hrcs = new HRContracts();
        //    HRContract hrc = (HRContract)hrcs.Filter("FK_HRRecord", emp.No);
        //    this.DengJiDT = hrc.DengJiDT;
        //    this.Creater = hrc.Creater;
        //    this.FK_HRRecord = hrc.No;
        //    this.Name = hrc.Name;
        //    return beforeUpdateInsertAction();
        //}
        protected override void afterInsertUpdateAction()
        {
            HRRecord r = new HRRecord(this.FK_HRRecord);
            r.ZhuangTai = "0";
            r.Update();
        }
        #endregion
        #region 方法


        #endregion


        /// <summary>
        /// 劳动合同s
        /// </summary>

    }
    public class HRContracts : EntitiesNoName
    {
        #region 构造函数
        public HRContracts()
        {

        }
        #endregion
        #region 重写父类方法
        public override Entity GetNewEntity
        {
            get { return new HRContract(); }
        }
        #endregion
    }
}
