﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BP.DA;
using BP.En;
using BP.Port;
namespace BP.OA.Market
{
    
    /// <summary>
    /// 客户档案
    /// </summary>
    public class KeHuDangAnAttr : EntityNoNameAttr
    {
        /// <summary>
        /// 客户名称
        /// </summary>
        public const string KeHuMingCheng = "KeHuMingCheng";
        /// <summary>
        /// 联系人
        /// </summary>
        public const string LianXiRen = "LianXiRen";
        /// <summary>
        /// 联系电话
        /// </summary>
        public const string LianXiDianHua = "LianXiDianHua";
        /// <summary>
        ///联系地址
        /// </summary>
        public const string LianXiDiZhi = "LianXiDiZhi";
        /// <summary>
        ///传真
        /// </summary>
        public const string ChuanZhen = "ChuanZhen";
        /// <summary>
        /// Email
        /// </summary>
        public const string Email = "Email";
        /// <summary>
        /// 董事长
        /// </summary>
        public const string DongShiZhang = "DongShiZhang";
        /// <summary>
        /// 备注
        /// </summary>
        public const string BeiZhu = "BeiZhu";
    }



    /// <summary>
    /// 客户档案实体类
    /// </summary>
    public class KeHuDangAn : EntityNoName
    {
        #region 属性
        /// <summary>
        /// 客户名称
        /// </summary>
        public string KeHuMingCheng
        {
            get
            {
                return this.GetValStrByKey(KeHuDangAnAttr.KeHuMingCheng);
            }
            set
            {
                this.SetValByKey(KeHuDangAnAttr.KeHuMingCheng, value);
            }
        }
        /// <summary>
        /// 联系人
        /// </summary>
        public string LianXiRen
        {
            get
            {
                return this.GetValStrByKey(KeHuDangAnAttr.LianXiRen);
            }
            set
            {
                this.SetValByKey(KeHuDangAnAttr.LianXiRen, value);
            }
        }
        /// <summary>
        /// 联系电话
        /// </summary>
        public string LianXiDianHua
        {
            get
            {
                return this.GetValStrByKey(KeHuDangAnAttr.LianXiDianHua);
            }
            set
            {
                this.SetValByKey(KeHuDangAnAttr.LianXiDianHua, value);
            }
        }
        /// <summary>
        /// 联系地址
        /// </summary>
        public string LianXiDiZhi
        {
            get
            {
                return this.GetValStrByKey(KeHuDangAnAttr.LianXiDiZhi);
            }
            set
            {
                this.SetValByKey(KeHuDangAnAttr.LianXiDiZhi, value);
            }
        }
        /// <summary>
        /// 传真
        /// </summary>
        public string ChuanZhen
        {
            get
            {
                return this.GetValStrByKey(KeHuDangAnAttr.ChuanZhen);
            }
            set
            {
                this.SetValByKey(KeHuDangAnAttr.ChuanZhen, value);
            }
        }
        /// <summary>
        /// Email
        /// </summary>
        public string Email
        {
            get
            {
                return this.GetValStrByKey(KeHuDangAnAttr.Email);
            }
            set
            {
                this.SetValByKey(KeHuDangAnAttr.Email, value);
            }
        }
        /// <summary>
        /// 董事长
        /// </summary>
        public string DongShiZhang
        {
            get
            {
                return this.GetValStrByKey(KeHuDangAnAttr.DongShiZhang);
            }
            set
            {
                this.SetValByKey(KeHuDangAnAttr.DongShiZhang, value);
            }
        }
        /// <summary>
        /// 备注
        /// </summary>
        public string BeiZhu
        {
            get
            {
                return this.GetValStrByKey(KeHuDangAnAttr.BeiZhu);
            }
            set
            {
                this.SetValByKey(KeHuDangAnAttr.BeiZhu, value);
            }
        }
        #endregion 属性
        #region 权限控制属性.

        #endregion 权限控制属性.

        #region 构造方法
        /// <summary>
        /// 投标信息
        /// </summary>
        public KeHuDangAn()
        {
        }
        /// <summary>
        /// 投标信息
        /// </summary>
        /// <param name="_No"></param>
        public KeHuDangAn(string _No) : base(_No) { }
        #endregion

        #region 重写方法
        protected override bool beforeInsert()
        {

            return base.beforeInsert();
        }

        protected override bool beforeUpdate()
        {

            return base.beforeUpdate();
        }
        #endregion 重写方法

        /// <summary>
        /// 投标信息Map
        /// </summary>
        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                    return this._enMap;

                Map map = new Map("OA_KeHuDangAn");
                map.EnDesc = "客户档案登记与维护";
                //map.IsAutoGenerNo = true;
                map.CodeStruct = "3"; //三位编号001开始
                map.AddTBStringPK(KeHuDangAnAttr.No, null, "客户编号", true, true, 0, 100, 40);
                map.AddTBString(KeHuDangAnAttr.KeHuMingCheng, null, "客户名称", true, false, 0, 100, 40);
                map.AddTBString(KeHuDangAnAttr.LianXiRen, null, "联系人", true, false, 0, 100, 40);
                map.AddTBString(KeHuDangAnAttr.LianXiDianHua, null, "联系电话", true, false, 0, 100, 40);
                map.AddTBString(KeHuDangAnAttr.LianXiDiZhi, null, "联系地址", true, false, 0, 100, 40);
                map.AddTBString(KeHuDangAnAttr.ChuanZhen, null, "传真", true, false, 0, 100, 40);
                map.AddTBString(KeHuDangAnAttr.Email, null, "Email", true, false, 0, 100, 40);
                map.AddTBString(KeHuDangAnAttr.DongShiZhang, null, "董事长", true, false, 0, 100, 40);
                map.AddTBStringDoc(GongSiZiZhiAttr.BeiZhu, null, "备注", true, false, true);
                this._enMap = map;
                return this._enMap;
            }
        }

    }
        /// <summary>
        /// 投标信息
        /// </summary>
        public class KeHuDangAns : SimpleNoNames
        {
            /// <summary>
            /// 汽车信息s
            /// </summary>
            public KeHuDangAns() { }
            /// <summary>
            /// 得到它的 Entity 
            /// </summary>
            public override Entity GetNewEntity
            {
                get
                {
                    return new KeHuDangAn();
                }
            }
        }
    }





