using System;
using System.Data;
using System.Collections;
using BP.DA;
using BP.En;
using BP.Port;

namespace BP.DS
{
    /// <summary>
    /// 单位 属性
    /// </summary>
    public class API
    {
        #region 采购 api. 
        /// <summary>
        /// 采购申请流程是否启用?
        /// </summary>
        public static bool DailySupplie_Buy_FlowIsEnable
        {
            get
            {
                return BP.Sys.SystemConfig.GetValByKeyBoolen("DailySupplie_Buy_FlowIsEnable", false);
            }
        }
        /// <summary>
        /// 采购的流程标记
        /// </summary>DailySupplie_Buy_FlowMark
        public static string DailySupplie_Buy_FlowMark
        {
            get
            {
                return "DailySupplieBuy";
            }
        }
        public static string DailySupplieBuy_OneInsert(long WorkID, string fk_dsMain, decimal NumOfPlan, decimal NumOfBuy, 
            decimal UnitPrice,decimal UnitPYCan, decimal JE,decimal LasTJE,string WhoWant, string buyer,string RDT, string DsBuyZT ,string note)
        {
            DSBuy db = new DSBuy();
            db.WorkID=WorkID.ToString();
            db.FK_DSMain = fk_dsMain;
            db.NumOfPlan = NumOfPlan;
            db.NumOfBuy = NumOfBuy;
            db.UnitPrice = UnitPrice;
            db.UnitPYCan = UnitPYCan;
            db.JE = JE;
            db.LasTJE = LasTJE;
            db.WhoWant = WhoWant;
            db.Buyer = buyer;
            db.RDT = RDT;
            db.DsBuyZT = DsBuyZT;
            db.Note = note;
            db.Insert();
            return "调用api成功...";
        }
        public static string DailySupplieBuy_TwoUpdate(int oid, decimal NumOfBuy, decimal UnitPYCan, decimal LasTJE, string DsBuyZT)
        {
            DSBuy ds = new DSBuy(oid);
            ds.NumOfBuy = NumOfBuy;
            ds.UnitPYCan = UnitPYCan;
            ds.LasTJE = LasTJE;
            ds.DsBuyZT = DsBuyZT;
            ds.Update();
            return "调用api成功...";
        }
        public static string DailySupplieBuy_FourUpdate(int oid, string Buyer, string RDT)
        {
            DSBuy ds = new DSBuy(oid);
            ds.Buyer = Buyer;
            ds.RDT = RDT;
            ds.Update();
            return "调用api成功...";
        }
        /// <summary>
        /// 执行采购的API
        /// </summary>
        public static string DailySupplie_Buy(string fk_dsMain, decimal sl, decimal UnitPrice, decimal je, string buyer, string note)
        {
            DSBuy en = new DSBuy();
            en.FK_DSMain = fk_dsMain;
            en.NumOfBuy = sl;
            en.JE = je;
            en.UnitPrice = UnitPrice;
            en.Buyer = buyer;
            en.Note = note;
            en.Insert();
            return "调用api成功...";
        }
        #endregion 采购api.

        #region 办公用品领取 api.
        /// <summary>
        /// 领用申请流程是否启用?
        /// </summary>
        public static bool DailySupplie_Take_FlowIsEnable
        {
            get
            {
                return BP.Sys.SystemConfig.GetValByKeyBoolen("DailySupplie_Take_FlowIsEnable", false);
            }
        }
        /// <summary>
        /// 领用的流程标记
        /// </summary>
        public static string DailySupplie_Take_FlowMark
        {
            get
            {
                return "DailySupplieTake";
            }
        }
        /// <summary>
        /// 办公用品领取
        /// </summary>
        /// <param name="fk_dsMain"></param>
        /// <param name="sl"></param>
        /// <param name="taker"></param>
        /// <param name="note"></param>
        /// <returns></returns>
        public static string DailySupplieTake_OneInsert(long WorkID, string fk_dsMain, decimal NumOfWant, decimal NumOfCan, decimal sl, string taker, string DoIt, string DsTakeZT, string note, string TakerDep)
        {
            DSTake ds = new DSTake();
            ds.FK_DSMain = fk_dsMain;
            ds.NumOfWant = NumOfWant;
            ds.NumOfCan = NumOfCan;
            ds.NumOfTake = sl;
            ds.Taker = taker;
            ds.DoIt = DoIt;
            ds.DsTakeZT = DsTakeZT;
            ds.Note = note;
            ds.WorkID = WorkID.ToString();
            ds.TakerDep = TakerDep;
            ds.Insert();
            return "调用api成功...";
        }
        public static string DailySupplieTake_ThreeUpdate(int oid, decimal NumOfCan, string DsTakeZT)
        {
            DSTake ds = new DSTake(oid);
            ds.NumOfCan = NumOfCan;
            ds.DsTakeZT= DsTakeZT;
            ds.Update();
            return "调用api成功...";
        }
        public static string DailySupplieTake_TwoUpdate(int oid, decimal NumOfTake,string DoIt)
        {
            DSTake ds = new DSTake(oid);
            ds.DoIt = DoIt;
            ds.NumOfTake = NumOfTake;
            ds.Update();
            return "调用api成功...";
        }
        public static string DailySupplieTake_FinallyUpdate(int oid, decimal sl,string DoIt)
        {
            DSTake ds = new DSTake(oid);
            ds.NumOfTake = sl;
            ds.DoIt = DoIt;
            ds.Update();
            return "调用api成功...";
        }

        #endregion 办公用品领取 api.
    }
}
