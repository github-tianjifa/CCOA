using System;
using System.Data;
using System.Collections;
using BP.DA;
using BP.En;
using BP.Port;

namespace BP.DS
{
    /// <summary>
    /// 办公用品库存台帐 属性
    /// </summary>
    public class DSMainAttr : EntityNoNameAttr
    {
        /// <summary>
        /// 类别
        /// </summary>
        public const string FK_Sort = "FK_Sort";
        /// <summary>
        /// 单位
        /// </summary>
        public const string Unit = "Unit";
        /// <summary>
        /// 规格型号
        /// </summary>
        public const string Spec = "Spec";
        /// <summary>
        /// 采购数量
        /// </summary>
        public const string NumOfBuy = "NumOfBuy";
        /// <summary>
        /// 出库数量
        /// </summary>
        public const string NumOfTake = "NumOfTake";
        /// <summary>
        /// 库存数量
        /// </summary>
        public const string NumOfLeft = "NumOfLeft";
        /// <summary>
        /// 当前单价
        /// </summary>
        public const string UnitPrice = "UnitPrice";
        /// <summary>
        /// 库存成本
        /// </summary>
        public const string Cost = "Cost";
    }
    /// <summary>
    ///  办公用品库存台帐
    /// </summary>
    public class DSMain : EntityNoName
    {
        #region 属性
        /// <summary>
        /// 类别
        /// </summary>
        public string FK_Sort
        {
            get
            {
                return this.GetValStringByKey(DSMainAttr.FK_Sort);
            }
            set
            {
                this.SetValByKey(DSMainAttr.FK_Sort, value);
            }
        }
        /// <summary>
        /// 单位
        /// </summary>
        public string Unit
        {
            get
            {
                return this.GetValStringByKey(DSMainAttr.Unit);
            }
            set
            {
                this.SetValByKey(DSMainAttr.Unit, value);
            }
        }
        /// <summary>
        /// 采购数量
        /// </summary>
        public float NumOfBuy
        {
            get
            {
                return this.GetValFloatByKey(DSMainAttr.NumOfBuy);
            }
            set
            {
                this.SetValByKey(DSMainAttr.NumOfBuy, value);
            }
        }
        /// <summary>
        /// 出库数量
        /// </summary>
        public float NumOfTake
        {
            get
            {
                return this.GetValFloatByKey(DSMainAttr.NumOfTake);
            }
            set
            {
                this.SetValByKey(DSMainAttr.NumOfTake, value);
            }
        }
        /// <summary>
        /// 库存数量
        /// </summary>
        public float NumOfLeft
        {
            get
            {
                return this.GetValFloatByKey(DSMainAttr.NumOfLeft);
            }
            set
            {
                this.SetValByKey(DSMainAttr.NumOfLeft, value);
            }
        }
        /// <summary>
        /// 规格型号
        /// </summary>
        public string Spec
        {
            get
            {
                return this.GetValStringByKey(DSMainAttr.Spec);
            }
            set
            {
                this.SetValByKey(DSMainAttr.Spec, value);
            }
        }
        /// <summary>
        /// 当前单价
        /// </summary>
        public float UnitPrice
        {
            get
            {
                return this.GetValFloatByKey(DSMainAttr.UnitPrice);
            }
            set
            {
                this.SetValByKey(DSMainAttr.UnitPrice, value);
            }
        }
        /// <summary>
        /// 库存成本
        /// </summary>
        public float Cost
        {
            get
            {
                return this.GetValFloatByKey(DSMainAttr.Cost);
            }
            set
            {
                this.SetValByKey(DSMainAttr.Cost, value);
            }
        }
        #endregion 属性

        #region 权限控制属性.

        #endregion 权限控制属性.

        #region 构造方法
        /// <summary>
        /// 办公用品库存台帐
        /// </summary>
        public DSMain()
        {
        }
        /// <summary>
        /// 办公用品库存台帐
        /// </summary>
        /// <param name="_No"></param>
        public DSMain(string _No) : base(_No) { }
        #endregion
        public override UAC HisUAC
        {
            get
            {
                UAC uac = new UAC();

              
                uac.IsUpdate = true;
                uac.IsDelete = true;
                uac.IsInsert = true;
                uac.IsView = true;
                return uac;
            }
        }
        /// <summary>
        /// 办公用品库存台帐Map
        /// </summary>
        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                    return this._enMap;

                Map map = new Map("OA_DSMain");
                map.EnDesc = "办公用品库存台帐";

                map.DepositaryOfEntity = Depositary.Application;
                map.DepositaryOfMap = Depositary.Application;

                map.CodeStruct = "4";
                map.IsAutoGenerNo = true;
                map.AddTBStringPK(DSMainAttr.No, null, "编号", true, true, 4, 4, 4);
                map.AddTBString(DSMainAttr.Name, null, "物品名", true, false, 1, 100, 30, true);
                map.AddDDLEntities(DSMainAttr.FK_Sort, null, "类别", new DSSorts(), true);
                map.AddTBString(DSMainAttr.Unit, null, "单位", true,false,1,20,30,true);
                map.AddTBString(DSMainAttr.Spec, null, "规格型号", true, false, 1, 100, 30, true);
                map.AddTBFloat(DSMainAttr.NumOfBuy, 0, "采购数量", true, true);
                map.AddTBFloat(DSMainAttr.NumOfTake, 0, "出库数量", true, true);
                map.AddTBFloat(DSMainAttr.NumOfLeft, 0, "库存数量", true, true);
                map.AddTBMoney(DSMainAttr.UnitPrice, 0, "当前单价(元)", true, true);
                map.AddTBMoney(DSMainAttr.Cost, 0, "库存成本(元)", true,false);

                //查询.
                map.AddSearchAttr(DSMainAttr.FK_Sort);
                //map.AddSearchAttr
                //执行采购
                //RefMethod rm = new RefMethod();
                //rm.Title = "执行采购";
                //rm.ClassMethodName = this.ToString() + ".DoBuy";

                //DoBuy()方法，可以不设置参数，即默认全部

                //rm.HisAttrs.AddTBDecimal("SL", 0, "数量", true, false);
                //rm.HisAttrs.AddTBDecimal("JE", 0, "金额", true, false);
                //rm.HisAttrs.AddTBString("Buyer", null, "采购人", true, false, 0, 4000, 10);
                //rm.HisAttrs.AddTBString("Note", null, "备注", true, false, 0, 4000, 10);
                //map.AddRefMethod(rm);

                //执行领用
                //RefMethod rm2 = new RefMethod();
                //rm2.Title = "执行领用";
                //rm2.ClassMethodName = this.ToString() + ".DoTake";
                //rm2.HisAttrs.AddTBDecimal("SL", 0, "数量", true, false);
                //rm2.HisAttrs.AddTBString("taker", null, "领用人", true, false, 0, 4000, 10);
                //rm2.HisAttrs.AddTBString("Note", null, "备注", true, false, 0, 4000, 10);
                //map.AddRefMethod(rm2);

                this._enMap = map;
                return this._enMap;
            }
        }
        /// <summary>
        /// 执行采购
        /// </summary>
        /// <param name="sl"></param>
        /// <param name="je"></param>
        /// <param name="buyer"></param>
        /// <param name="note"></param>
        /// <returns></returns>
        public string DoBuy()
        {
            if (API.DailySupplie_Buy_FlowIsEnable == true)
                BP.Sys.PubClass.WinOpen("/WF/MyFlow.aspx?FK_Flow=" + API.DailySupplie_Buy_FlowMark, 900, 600);
            else
                BP.Sys.PubClass.WinOpen("UIEn.aspx?EnsName=BP.OA.DS.DSBuys&FK_DSMain=" + this.No + "&EnName=BP.OA.DS.DSBuy", 900, 600);
            return null;
        }
        /// <summary>
        /// 执行领用
        /// </summary>
        /// <returns></returns>
        public string DoTake()
        {
            if (API.DailySupplie_Take_FlowIsEnable == true)
                BP.Sys.PubClass.WinOpen("/WF/MyFlow.aspx?FK_Flow=" + API.DailySupplie_Take_FlowMark + "&OA_DSMain=" + this.No, 900, 600);
            else
                BP.Sys.PubClass.WinOpen("UIEn.aspx?EnsName=BP.OA.DS.DSTakes&FK_DSMain=" + this.No + "&EnName=BP.OA.DS.DSTake", 900, 600);

            return null;
        }

        #region 重写方法
        
        protected override bool beforeInsert()
        {
            return base.beforeInsert();
        }
        #endregion 重写方法
    }
    /// <summary>
    /// 办公用品库存台帐
    /// </summary>
    public class DSMains : EntitiesNoName
    {
        /// <summary>
        /// 办公用品库存台帐
        /// </summary>
        public DSMains() { }
        /// <summary>
        /// 得到它的 Entity 
        /// </summary>S
        public override Entity GetNewEntity
        {
            get
            {
                return new DSMain();
            }
        }
    }
}
