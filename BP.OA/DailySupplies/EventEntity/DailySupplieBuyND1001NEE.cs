using System;
using System.Collections;
using System.Data;
using BP.DA;
using BP.DTS;
using BP.En;
using BP.Web;
using BP.Sys;
using BP.WF;
using BP.Port;

namespace BP.DS
{
    /// <summary>
    /// 购买流程 - 开始节点.
    /// </summary>
    public class DailySupplieBuyND1001NEE : BP.WF.FlowEventBase
    {
        #region 构造.
        /// <summary>
        /// 购买流程事件
        /// </summary>
        public DailySupplieBuyND1001NEE()
        {
        }
        #endregion 属性.

        #region 重写属性.
        /// <summary>
        /// 流程标记
        /// </summary>
        public override string FlowMark
        {
            get { return BP.DS.API.DailySupplie_Buy_FlowMark; }
        }
        ///// <summary>
        ///// 节点标记
        ///// </summary>
        //public override string NodeMarks
        //{
        //    get { return "ND1001"; }
        //}
        #endregion 重写属性.

        #region 重写节点表单事件.
        /// <summary>
        /// 表单载入前
        /// </summary>
        public override string FrmLoadAfter()
        {
            return null;
        }
        /// <summary>
        /// 表单载入后
        /// </summary>
        public override string FrmLoadBefore()
        {
            return null;
        }
        /// <summary>
        /// 表单保存后
        /// </summary>
        public override string SaveAfter()
        {
            return null;
        }
        /// <summary>
        /// 表单保存前
        /// </summary>
        public override string SaveBefore()
        {
            return null;
        }
        #endregion 重写节点表单事件

        #region 重写节点运动事件.
        /// <summary>
        /// 发送前:用于检查业务逻辑是否可以执行发送，不能执行发送就抛出异常.
        /// </summary>
        public override string SendWhen()
        {
            if (this.HisNode.NodeID == 1001)
            {
                //限制多次购买同一物品
                string[] GetName = new string[100];
                int GetNameInd = 0;

                //获取设计器节点表单字段,进行判断
                int JiHuaGouMai = 0;
                decimal DanJia = 0;

                string sql = "select * from ND1001Dtl1 where  RefPK=" + this.WorkID;
                DataTable dt = DBAccess.RunSQLReturnTable(sql);
                foreach (DataRow dr in dt.Rows)
                {
                    //数组赋值
                    GetName[GetNameInd] = dr["OA_DSMain"].ToString();
                    GetNameInd += 1;

                    JiHuaGouMai = int.Parse(dr["JiHuaGouMai"].ToString());
                    DanJia = decimal.Parse(dr["DanJia"].ToString());

                    if (JiHuaGouMai <= 0 || DanJia <= 0)
                    {
                        throw new Exception("@计划数量,单价不可为空或错误的数据状态!");
                    }
                }
                //判断
                for (int i = 0; i < GetNameInd; i++)
                {
                    for (int j = i + 1; j < GetName.Length; j++)
                    {
                        if (GetName[i] == GetName[j])
                        {
                            throw new Exception("@不允许重复购买相同项!");
                        }
                    }
                }
                return "合计已经在发送前事件完成.";
            }

             if (this.HisNode.NodeID == 1002)
            {
                string sql = "select * from ND1001Dtl1 where  RefPK=" + this.WorkID;
                DataTable dt = DBAccess.RunSQLReturnTable(sql);
                foreach (DataRow dr in dt.Rows)
                {
                    string fk_DsMain = dr["OA_DSMain"].ToString();
                    string DsBuyZT = "0";
                    string GetOidsSql = "select OID FROM  OA_DSBuy WHERE WorkID='" + this.WorkID + "' AND  FK_DSMain=" + fk_DsMain;
                    DataTable GetOidDt = DBAccess.RunSQLReturnTable(GetOidsSql);
                    int oid = int.Parse(GetOidDt.Rows[0][0].ToString());

                    if (this.GetValStr("SHZT") == "0")
                    {
                        DsBuyZT = "1";
                        decimal LasTJE = decimal.Parse(dr["JianYiDanJia"].ToString()) * decimal.Parse(dr["PiZhunShuLiang"].ToString());
                        BP.DS.API.DailySupplieBuy_TwoUpdate(oid,
                             decimal.Parse(dr["PiZhunShuLiang"].ToString()),
                            decimal.Parse(dr["JianYiDanJia"].ToString()),
                            LasTJE,
                         DsBuyZT);
                    }
                    else
                    {
                        DsBuyZT = "2";
                        decimal LasTJE = decimal.Parse(dr["JianYiDanJia"].ToString()) * decimal.Parse(dr["PiZhunShuLiang"].ToString());
                        BP.DS.API.DailySupplieBuy_TwoUpdate(oid,
                             0,
                            0,
                            LasTJE,
                         DsBuyZT);
                    }

                    //int LasTJE = int.Parse(dr["JianYiDanJia"].ToString()) * int.Parse(dr["PiZhunShuLiang"].ToString());
                    //BP.DS.API.DailySupplieBuy_TwoUpdate(oid,
                    //     int.Parse(dr["PiZhunShuLiang"].ToString()),
                    //    int.Parse(dr["JianYiDanJia"].ToString()),
                    //    LasTJE,
                    // DsBuyZT);

                }

                return "SendSuccess调用成功....";
            }
            return null;
        }
        /// <summary>
        /// 发送成功后
        /// </summary>
        public override string SendSuccess()
        {
            if (this.HisNode.NodeID == 1001)
            {
                string sql = "select * from ND1001Dtl1 where  RefPK=" + this.WorkID;

                DataTable dt = DBAccess.RunSQLReturnTable(sql);
                foreach (DataRow dr in dt.Rows)
                {
                    decimal JE = decimal.Parse(dr["JiHuaGouMai"].ToString()) * decimal.Parse(dr["DanJia"].ToString());
                    BP.DS.API.DailySupplieBuy_OneInsert(this.WorkID,
                        dr["OA_DSMain"].ToString(),
                        int.Parse(dr["JiHuaGouMai"].ToString()),
                        0,
                       decimal.Parse(dr["DanJia"].ToString()),
                       0,
                       JE,
                       0,
                      BP.Web.WebUser.Name,
                      "",
                      "",
                      "0",
                      this.GetValStr("ShuoMing"));
                }
                return "SendSuccess调用成功....";
            }

            if (this.HisNode.NodeID == 1002)
            {
                //获取设计器节点表单字段,进行判断
                int NumOfPlan = 0;
                int NumOfBuy = 0;
                decimal UnitPrice = 0;
                decimal UnitPYCan = 0;
                decimal LasTJE = 0;
                string sql = "select * from ND1001Dtl1 where  RefPK=" + this.WorkID;
                DataTable dt = DBAccess.RunSQLReturnTable(sql);
                foreach (DataRow dr in dt.Rows)
                {
                    NumOfPlan = int.Parse(dr["JiHuaGouMai"].ToString());
                    UnitPrice = decimal.Parse(dr["DanJia"].ToString());
                    NumOfBuy = int.Parse(dr["PiZhunShuLiang"].ToString());
                    UnitPYCan = decimal.Parse(dr["JianYiDanJia"].ToString());
                    LasTJE = NumOfBuy * UnitPYCan;

                    if (NumOfBuy < 0 || UnitPYCan < 0)
                    {
                        throw new Exception("@批准数量,建议单价不可为空或错误的数据状态!");
                    }
                    if (NumOfBuy > NumOfPlan || UnitPYCan > UnitPrice)
                    {
                        throw new Exception("@批准数量,单价不可大于申请数据!");
                    }
                }

                return "合计已经在发送前事件完成.";
            }

            if (this.HisNode.NodeID == 1004)
            {
                string sql = "select * from ND1001Dtl1 where  RefPK=" + this.WorkID;
                DataTable dt = DBAccess.RunSQLReturnTable(sql);
                foreach (DataRow dr in dt.Rows)
                {
                    string fk_DsMain = dr["OA_DSMain"].ToString();
                    string GetOidsSql = "select OID FROM  OA_DSBuy WHERE WorkID='" + this.WorkID + "' AND  FK_DSMain=" + fk_DsMain;
                    DataTable GetOidDt = DBAccess.RunSQLReturnTable(GetOidsSql);
                    int oid = int.Parse(GetOidDt.Rows[0][0].ToString());

                    BP.DS.API.DailySupplieBuy_FourUpdate(oid,
                        this.GetValStr("CGR"),
                        this.GetValStr("CGRQ"));
                }

                return "SendSuccess调用成功....";
            }

            return null;
        }
        /// <summary>
        /// 发送失败后
        /// </summary>
        public override string SendError()
        {
            return null;
        }
        /// <summary>
        /// 退回前
        /// </summary>
        public override string ReturnBefore()
        {
            return null;
        }
        /// <summary>
        /// 退回后
        /// </summary>
        public override string ReturnAfter()
        {
            return null;
        }
        #endregion 重写事件，完成业务逻辑.
    }
}
